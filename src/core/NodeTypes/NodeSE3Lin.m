%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef NodeSE3Lin < Node
    
    properties
        rotation = [];
    end
    
    methods
        function obj = NodeSE3Lin(input)
            obj = obj@Node(input);
        end
        
        function initializeStatic(obj,nstep)
            obj.position = [obj.position zeros(3,nstep)];
            obj.rotation = [zeros(3,nstep+1)];
        end
        
        function updateStatic(obj,step)
            for i = 1:3
                obj.position(i,step) = obj.position(i,step-1) + obj.listDof{i}.value;
                obj.rotation(i,step) = obj.rotation(i,step-1) + obj.listDof{i+3}.value;
            end
        end
    end  
end

