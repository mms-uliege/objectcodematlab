%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef NodeSE3NL < Node

    properties
        R = eye(3);
        lambda;
    end
    
    methods
        function obj = NodeSE3NL(input)
            obj = obj@Node(input);
        end
        
        function initializeStatic(obj,nstep)
            obj.position = [obj.position zeros(3,nstep)];
            obj.R = zeros(9,nstep+1); obj.R(:,1) = mat3ToVect9(eye(3));
            obj.lambda = zeros(1,nstep+1);
        end
        
        function initializeDynamic(obj,nstep)
            obj.initializeStatic(nstep);
            obj.velocity = zeros(6,nstep+1);
            obj.acceleration = zeros(6,nstep+1);
        end
        
        function updateStatic(obj,step)
            h = zeros(6,1);
            for i = 1:6
                h(i) = obj.listDof{i}.value;
            end
            Hinc = expSE3(h);
            obj.R(:,step) = mat3ToVect9(vect9ToMat3(obj.R(:,step-1))*Hinc(1:3,1:3));
            obj.position(:,step) = obj.position(:,step-1) + vect9ToMat3(obj.R(:,step-1))*Hinc(1:3,4);
            obj.lambda(step) = 1e6*obj.listDof{7}.value;
        end
        
        function updateDynamic(obj,step)
            obj.updateStatic(step);

            for i = 1:6
                obj.velocity(i,step) = obj.listDof{i}.d_value;
                obj.acceleration(i,step) = obj.listDof{i}.dd_value;
            end
        end 
    end  
end

