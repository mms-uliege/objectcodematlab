%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef NodeSE3 < Node

    properties
        R = [1 0 0 0 1 0 0 0 1]';
        R_InitOpti = [];
    end

    methods
        function obj = NodeSE3(input)
            obj = obj@Node(input);
        end

        function initializeStatic(obj,nstep)
            obj.position = [obj.position zeros(3,nstep)];
            obj.R = zeros(9,nstep+1); obj.R(:,1) = mat3ToVect9(eye(3));

            obj.listDof{1}.state = {'position'; 1; obj};
            obj.listDof{2}.state = {'position'; 2; obj};
            obj.listDof{3}.state = {'position'; 3; obj};
            obj.listDof{4}.state = {'R'; 1; obj};
            obj.listDof{5}.state = {'R'; 2; obj};
            obj.listDof{6}.state = {'R'; 3; obj};
        end

        function initializeDynamic(obj,nstep)
            obj.initializeStatic(nstep);
            obj.velocity = zeros(6,nstep+1);
            obj.acceleration = zeros(6,nstep+1);
        end

        function initializeStatic_Opti(obj)
            obj.position_InitOpti = obj.position;
            obj.R_InitOpti = obj.R;
        end

        function initializeDynamic_Opti(obj)
            obj.initializeStatic_Opti();
            obj.velocity_InitOpti = obj.velocity;
            obj.acceleration_InitOpti = obj.acceleration;
        end

        function updateStatic(obj,step,stepUpdate)
           if nargin == 2
                h = zeros(6,1); h_p = zeros(6,1);
                for i = 1:6
                    h(i) = obj.listDof{i}.value;
                    h_p(i) = obj.listDof{i}.value_p;
                end
                h = h_p + computeTSE3inv(h_p)*(h-h_p);
                for i = 1:6
                    obj.listDof{i}.value = h(i);
                end
                Hinc = expSE3(h);
                obj.R(:,step) = mat3ToVect9(vect9ToMat3(obj.R(:,step-1))*Hinc(1:3,1:3));
                obj.position(:,step) = obj.position(:,step-1) + vect9ToMat3(obj.R(:,step-1))*Hinc(1:3,4);
           else
                h = zeros(6,1); h_p = zeros(6,1);
                for i = 1:6
                    h(i) = obj.listDof{i}.value;
                    h_p(i) = obj.listDof{i}.value_p;
                end
                h = h_p + computeTSE3inv(h_p)*(h-h_p);
                for i = 1:6
                    obj.listDof{i}.value = h(i);
                end
                Hinc = expSE3(h);
                obj.R(:,step) = mat3ToVect9(vect9ToMat3(obj.R(:,step+stepUpdate))*Hinc(1:3,1:3));
                obj.position(:,step) = obj.position(:,step+stepUpdate) + vect9ToMat3(obj.R(:,step+stepUpdate))*Hinc(1:3,4);
            end
        end
        
        function updateDynamic(obj,step,stepUpdate)
            if nargin == 2
                h = zeros(6,1);
                for i = 1:6
                    h(i) = obj.listDof{i}.value;
                end
                Hinc = expSE3(h);
                if step == 1 % To allow non-zero initial values
                    obj.R(:,step) = mat3ToVect9(Hinc(1:3,1:3));
                    obj.position(:,step) = Hinc(1:3,4);
                else
                    obj.R(:,step) = mat3ToVect9(vect9ToMat3(obj.R(:,step-1))*Hinc(1:3,1:3));
                    obj.position(:,step) = obj.position(:,step-1) + vect9ToMat3(obj.R(:,step-1))*Hinc(1:3,4);
                end
                for i = 1:6
                    obj.velocity(i,step) = obj.listDof{i}.d_value;
                    obj.acceleration(i,step) = obj.listDof{i}.dd_value;
                end
            else
                h = zeros(6,1);
                for i = 1:6
                    h(i) = obj.listDof{i}.value;
                end
                Hinc = expSE3(h);
                obj.R(:,step) = mat3ToVect9(vect9ToMat3(obj.R(:,step+stepUpdate))*Hinc(1:3,1:3));
                obj.position(:,step) = obj.position(:,step+stepUpdate) + vect9ToMat3(obj.R(:,step+stepUpdate))*Hinc(1:3,4);
                for i = 1:6
                    obj.velocity(i,step) = obj.listDof{i}.d_value;
                    obj.acceleration(i,step) = obj.listDof{i}.dd_value;
                end
            end
        end

        function updateStatic_Opti(obj,step)
            h = zeros(6,1); h_p = zeros(6,1);
            for i = 1:6
                h(i) = obj.listDof{i}.value;
                h_p(i) = obj.listDof{i}.value_p;
            end
            h = h_p + computeTSE3inv(h_p)*(h-h_p);
            for i = 1:6
                obj.listDof{i}.value = h(i);
            end
            Hinc = expSE3(h);
            obj.R(:,step) = mat3ToVect9(vect9ToMat3(obj.R_InitOpti(:,step))*Hinc(1:3,1:3));
            obj.position(:,step) = obj.position_InitOpti(:,step) + vect9ToMat3(obj.R_InitOpti(:,step))*Hinc(1:3,4);
        end

        function updateDynamic_Opti(obj,step)
            h = zeros(6,1);
            for i = 1:6
                h(i) = obj.listDof{i}.value;
            end
            Hinc = expSE3(h);
            obj.R(:,step) = mat3ToVect9(vect9ToMat3(obj.R_InitOpti(:,step))*Hinc(1:3,1:3));
            obj.position(:,step) = obj.position_InitOpti(:,step) + vect9ToMat3(obj.R_InitOpti(:,step))*Hinc(1:3,4);
            for i = 1:6
                obj.velocity(i,step) = obj.listDof{i}.d_value;
                obj.acceleration(i,step) = obj.listDof{i}.dd_value;
            end
        end

        function vec = extractDof(obj,type,nstep)
            if strcmp(type,'q') 
                vec = [obj.position(:,nstep);logSO3(vect9ToMat3(obj.R(:,nstep)))];
            elseif strcmp(type,'dq') 
                vec = obj.velocity(:,nstep);
            elseif strcmp(type,'ddq') 
                vec = obj.acceleration(:,nstep);
            end
        end
    end
end

