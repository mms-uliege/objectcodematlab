%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef Node < Variable

    properties
        numberNode = 0;
        position = [];
        position_InitOpti = [];
        
        velocity = [];
        velocity_InitOpti = [];
        
        acceleration = [];
        acceleration_InitOpti = [];
    end
    
    methods
        function obj = Node(input)
            obj.numberNode = input;
        end
        
        function out = getqmax(obj,i)
            if nargin == 2
                out = max(max(abs(obj.position(i,:))));
            else
                out = max(max(abs(obj.position)));
            end
        end
        
        function initializePosition(obj,timestep)
            if nargin == 1 
                for i = 1:size(obj.position,1)
                    obj.position(i,1) = obj.listDof{i}.value;
                end
            elseif nargin == 2 
                for i = 1:size(obj.position,1)
                    obj.position(i,timestep) = obj.listDof{i}.value;
                end
            end
        end
              
        function initializeVelocity(obj,timestep)
            if nargin == 1 
                for i = 1:size(obj.velocity,1)
                    obj.velocity(i,1) = obj.listDof{i}.d_value;
                end
            elseif nargin == 2 
                for i = 1:size(obj.velocity,1)
                    obj.velocity(i,timestep) = obj.listDof{i}.d_value;
                end
            end
        end
                
        function initializeAcceleration(obj,timestep)
            if nargin == 1 
                for i = 1:size(obj.acceleration,1)
                   obj.acceleration(i,1) = obj.listDof{i}.dd_value;  
                end
            elseif nargin == 2 
                for i = 1:size(obj.acceleration,1)
                   obj.acceleration(i,timestep) = obj.listDof{i}.dd_value;  
                end
            end
        end        
    end    
end

