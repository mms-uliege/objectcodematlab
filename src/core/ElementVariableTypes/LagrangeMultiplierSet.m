%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef LagrangeMultiplierSet < ElementVariable
    
    properties
        value;
        value_InitOpti;
        nL;
    end
    
    properties (Constant = true)
        DofType = 'LagrangeMultiplier' ;
    end
    
    methods
        function obj = LagrangeMultiplierSet(input)
            obj = obj@ElementVariable(input);
        end
        
        function initializeStatic(obj,nstep)
            obj.nL = length(obj.listNumberDof);
            obj.value = zeros(obj.nL,nstep+1);
        end
        
        function initializeDynamic_Opti(obj)
            obj.value_InitOpti = obj.value;
        end
        
        function updateStatic(obj,step)
            for i = 1:obj.nL
                obj.value(i,step) = obj.listDof{i}.value;
            end
        end
        
        function updateDynamic_Opti(obj,step)
            for i = 1:obj.nL
                obj.value(i,step) = obj.listDof{i}.value;
            end
        end
    end
end

