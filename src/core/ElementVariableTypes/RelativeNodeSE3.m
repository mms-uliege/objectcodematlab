%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef RelativeNodeSE3 < ElementVariable
   
    properties
        A = [];
        nDof = [];
        R = [];
        R_InitOpti = [];
        
        x = [];
        x_InitOpti = [];
        
        xI0 = [];
        xI0_InitOpti = [];
        
        velocity = [];
        velocity_InitOpti = [];
        
        acceleration = [];
        acceleration_InitOpti = [];
        
        relCoo = [];
        relCoo_InitOpti = [];
    end
    
    properties (Constant = true)
        DofType = 'MotionDof';
    end
    
    methods
        function obj = RelativeNodeSE3(input)
            obj = obj@ElementVariable(input);
        end
        
        function initializeStatic(obj,nstep)
            obj.nDof = length(obj.listDof);
            obj.A = obj.linkElement.A';
            obj.R = zeros(9,nstep+1); obj.R(:,1) = mat3ToVect9(eye(3));
            obj.x = zeros(3,nstep+1);
            obj.relCoo = zeros(obj.nDof,nstep+1);
            Node1 = obj.linkElement.listNodes{1};
            Node2 = obj.linkElement.listNodes{2};
            obj.xI0 = Node2.position(:,1)-Node1.position(:,1);
            for i = 1:obj.nDof
                obj.listDof{i}.state = {'relCoo'; i; obj};
            end
        end
        
        function initializeDynamic(obj,nstep)
            obj.initializeStatic(nstep);
            obj.velocity = zeros(obj.nDof,nstep+1);
            obj.acceleration = zeros(obj.nDof,nstep+1);
        end
        
        function initializeStatic_Opti(obj)
            obj.R_InitOpti = obj.R;
            obj.x_InitOpti = obj.x;
            obj.relCoo_InitOpti = obj.relCoo;
            obj.xI0_InitOpti = obj.xI0;
        end

        function initializeDynamic_Opti(obj)
            obj.initializeStatic_Opti();
            obj.velocity_InitOpti = obj.velocity;
            obj.acceleration_InitOpti = obj.acceleration;
        end
        
        function initializeVelocity(obj)
           for i = 1:obj.nDof
              obj.velocity(i,1) = obj.listDof{i}.d_value;
           end
        end
        
        function initializeAcceleration(obj)
            for i = 1:obj.nDof
                obj.acceleration(i,1) = obj.listDof{i}.dd_value;
            end
        end
        
        function updateStatic(obj,step)
            h = zeros(obj.nDof,1);
            for i = 1:obj.nDof
                h(i) = obj.listDof{i}.value;
            end
            if step == 1 % To allow non-zero initial values
                H = expSE3(obj.A'*h);
                obj.relCoo(:,step) = h;
            else
                H = [vect9ToMat3(obj.R(:,step-1)) obj.x(:,step-1); 0 0 0 1]*expSE3(obj.A'*h);
                obj.relCoo(:,step) = obj.relCoo(:,step-1) + h;
            end
            obj.R(:,step) = mat3ToVect9(H(1:3,1:3));
            obj.x(:,step) = H(1:3,4);
        end
        
        function updateDynamic(obj,step)
            obj.updateStatic(step);
            v_inc = zeros(obj.nDof,1); vdot_inc = zeros(obj.nDof,1);
            for i = 1:obj.nDof
                v_inc(i) = obj.listDof{i}.d_value;
                vdot_inc(i) = obj.listDof{i}.dd_value;
            end
            obj.velocity(:,step) = v_inc;
            obj.acceleration(:,step) = vdot_inc;
        end
        
        function updateStatic_Opti(obj,step)
            h = zeros(obj.nDof,1);
            for i = 1:obj.nDof
                h(i) = obj.listDof{i}.value;
            end
            H = [vect9ToMat3(obj.R_InitOpti(:,step)) obj.x_InitOpti(:,step); 0 0 0 1]*expSE3(obj.A'*h);
            obj.R(:,step) = mat3ToVect9(H(1:3,1:3));
            obj.x(:,step) = H(1:3,4);
            obj.relCoo(:,step) = obj.relCoo_InitOpti(:,step) + h;
        end

        function updateDynamic_Opti(obj,step)
            obj.updateStatic_Opti(step);
            v_inc = zeros(obj.nDof,1); vdot_inc = zeros(obj.nDof,1);
            for i = 1:obj.nDof
                v_inc(i) = obj.listDof{i}.d_value;
                vdot_inc(i) = obj.listDof{i}.dd_value;
            end

            obj.velocity(:,step) = v_inc;
            obj.acceleration(:,step) = vdot_inc;
        end        
    end    
end

