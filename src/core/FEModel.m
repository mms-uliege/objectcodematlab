%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef FEModel < handle

    properties
        nDof = 0;
        
        listNumberDof = [];
        listNumberNodes = [];
        listNumberElementVariables = [];
        listNumberElements = [];
        
        listDof = {};
        listNodes = {};
        listElementVariables;
        listElements = {};
        
        listMotionDof = [];
        numberMotionDof = 0;
        
        listFixedNodes = [];
        listFixedDof = [];
        listFreeDof = [];
        
        res = [];
        refValForce=0;
        refValPhi=0;
        
        M = [];
        Ct = [];
        Kt = [];
        Phiq = [];
        St = [];  
        
        nEigenFrequencies = 1;
        eigenFrequencies = [];
    end
    
    methods
        
        function obj = FEModel()
        end
        
        function computeElementsTermsStatic(obj, parameters)
            for i = obj.listNumberElements
                obj.listElements{i}.computeTermsStatic(parameters);
            end
        end
        
        function computeElementsTermsDynamic(obj, parameters)
            for i = obj.listNumberElements
                obj.listElements{i}.computeTermsDynamic(parameters);
            end
        end
        
        function removeBlockedDofResidue(obj)
            obj.res(obj.listFixedDof) = [];
        end
        
        function setNumberMotionDof(obj)
            for i = 1 : length(obj.listMotionDof)
                if ismember(obj.listMotionDof(i), obj.listFixedDof)
                    obj.numberMotionDof = obj.numberMotionDof - 1;
                end
            end
        end
        
        function assembleResidueStaticWithBlockedDof(obj, parameters)
            obj.res = zeros(obj.nDof,1);
            obj.refValForce = 0;
            obj.refValPhi = 0;
            
            for i = obj.listNumberElements
                obj.res(obj.listElements{i}.listNumberDof) = obj.listElements{i}.computeResidueStatic(obj.res(obj.listElements{i}.listNumberDof),parameters);
                obj.refValForce = obj.refValForce + obj.listElements{i}.refValForce;
                obj.refValPhi = obj.refValPhi + obj.listElements{i}.refValPhi;
            end
        end
        
        function assembleResidueStatic(obj, timestep)
            obj.assembleResidueStaticWithBlockedDof(timestep)
            obj.removeBlockedDofResidue();
        end
        
        function assembleResidueDynamicWithBlockedDof(obj, parameters)
            obj.res = zeros(obj.nDof,1);
            obj.refValForce = 0;
            obj.refValPhi = 0;
            
            for i = obj.listNumberElements
                obj.res(obj.listElements{i}.listNumberDof) = obj.listElements{i}.computeResidueDynamic(obj.res(obj.listElements{i}.listNumberDof),parameters);
                obj.refValForce = obj.refValForce + obj.listElements{i}.refValForce;
                obj.refValPhi = obj.refValPhi + obj.listElements{i}.refValPhi;
            end
        end

        function assembleResidueDynamic(obj,parameters)
            obj.assembleResidueDynamicWithBlockedDof(parameters);
            obj.removeBlockedDofResidue();
        end
        
        function removeBlockedDofTangentOperator(obj)
            obj.St(obj.listFixedDof,:) = [];
            obj.St(:,obj.listFixedDof) = [];
        end
        
        function assembleTangentOperatorStatic(obj,parameters)
            obj.assembleTangentOperatorStaticWithBlockedDof(parameters);
            obj.removeBlockedDofTangentOperator();
        end
        
        function assembleTangentOperatorStaticWithBlockedDof(obj, parameters)
            obj.St = zeros(obj.nDof,obj.nDof);
            for i = obj.listNumberElements
                obj.St(obj.listElements{i}.listNumberDof,obj.listElements{i}.listNumberDof) = obj.listElements{i}.computeTangentOperatorStatic(obj.St(obj.listElements{i}.listNumberDof,obj.listElements{i}.listNumberDof),parameters);
            end    
        end
        
        function assembleTangentOperatorDynamic(obj,parameters)
            obj.assembleTangentOperatorDynamicWithBlockedDof(parameters);
            obj.removeBlockedDofTangentOperator();
        end
        
        function assembleTangentOperatorDynamicWithBlockedDof(obj,parameters)                  
            obj.St = zeros(obj.nDof,obj.nDof);
            for i = obj.listNumberElements
                obj.St(obj.listElements{i}.listNumberDof,obj.listElements{i}.listNumberDof) = obj.listElements{i}.computeTangentOperatorDynamic(obj.St(obj.listElements{i}.listNumberDof,obj.listElements{i}.listNumberDof),parameters);
            end
        end
        
        function St = getTangentMatrix(obj,type,timestep, complete)
            parameters.scaling = 0;
            parameters.coefK = 0;
            parameters.gammap = 0;
            parameters.betap = 0;
            if strcmp(type,'Stiffness')
                parameters.coefK = 1;
                parameters.scaling = 1e6;
            elseif strcmp(type,'Damping')
                parameters.gammap = 1;
            elseif strcmp(type,'Mass')
                parameters.betap = 1;
            elseif strcmp(type,'Phiq')
                parameters.scaling = 1e6;
            end
            parameters.timestep = timestep;
            if complete
                obj.assembleTangentOperatorDynamicWithBlockedDof(parameters);
            else
                obj.assembleTangentOperatorDynamic(parameters);
            end
            St = obj.St;
        end
 
        function ModalAnalysis(obj,nEF,timestep)
            if nargin == 2
                obj.nEigenFrequencies = nEF;
            end
            Kt = obj.getTangentMatrix('Stiffness',timestep,0);
            M = obj.getTangentMatrix('Mass',timestep,0);
            [~,b] = eig(Kt,M);
            b = sort(diag(b));
            obj.eigenFrequencies = sqrt(b(1:obj.nEigenFrequencies))/(2*pi);
        end
        
        function CreateFEModel(obj,listNodes,listElements)
            
            obj.listNumberNodes = [];
            for i = 1:size(listNodes,1)
                obj.listNumberNodes = [obj.listNumberNodes listNodes(i,1)];
            end
            if size(listNodes,2) == 2
                hyp = 1; %1D
            elseif size(listNodes,2) == 3
                hyp = 2; %2D
            elseif size(listNodes,2) == 4
                hyp = 3; %3D
            end

            obj.listNumberElements = [];
            tabNodalVariables = ListVariables(NodalVariableTypes);
            tabElementVariables = ListVariables(ElementVariableTypes);
            for i = 1:length(listElements)
                if ~isempty(listElements{i})
                    obj.listNumberElements = [obj.listNumberElements i];
                    newElement = eval([listElements{i}.type '(' num2str(i) ')']);
                    obj.listElements{i} = newElement;
                    
                    fieldsi = fields(listElements{i});
                    for j = 1:length(fieldsi)
                        if ~or(strcmp(fieldsi{j},'type'),strcmp(fieldsi{j},'nodes'))
                            newElement.(fieldsi{j}) = listElements{i}.(fieldsi{j});
                        end
                    end
                    
                    ln = []; le = [];
                    if isfield(listElements{i},'nodes')
                        ln = listElements{i}.nodes;
                        newElement.listNumberNodes = listElements{i}.nodes;
                    end
                    if isfield(listElements{i},'elements')
                        le = listElements{i}.elements;
                    end
                    newElement.defineDof(ln,le);
                    tabNodalVariables.addList(obj.listElements{i}.initListNodalDof);
                    tabElementVariables.addList(obj.listElements{i}.initListElementDof);
                end
            end
            
            obj.listNodes = {};
            for i = 1:tabNodalVariables.nVariableTypes
                typeNode = [upper(tabNodalVariables.types{i}(1)) tabNodalVariables.types{i}(2:end)];
                list = unique(round(tabNodalVariables.(tabNodalVariables.types{i})));
                for k = 1:length(list)
                    newNode = eval([typeNode '(' num2str(list(k)) ')']);
                    obj.listNodes{list(k)} = newNode;
                    ndof = min(hyp,length(find(round(tabNodalVariables.(tabNodalVariables.types{i})) == list(k))));
                    newNode.position = listNodes(listNodes(:,1)==list(k),2:(ndof+1))';
                    
                    % link node to elements
                    for l = obj.listNumberElements
                        indexNode = find(obj.listElements{l}.listNumberNodes == list(k));
                        if ~isempty(indexNode)
                            obj.listElements{l}.listNodes{indexNode} = newNode;
                        end
                    end
                end
            end
            
            numDof = 0;
            obj.listDof = {};
            for i = 1:tabNodalVariables.nVariableTypes
                type = tabNodalVariables.types{i};
                for j = 1:length(tabNodalVariables.(type))
                    % create new DOF
                    numDof = numDof+1;
                    newDof = MotionDof(numDof);
                    obj.listDof{numDof} = newDof;
                    
                    % link DOF to nodes
                    indexNode = round(tabNodalVariables.(type)(j));
                    n = length(obj.listNodes{indexNode}.listDof);
                    obj.listNodes{indexNode}.listDof{n+1} = newDof;
                    obj.listNodes{indexNode}.listNumberDof = [obj.listNodes{indexNode}.listNumberDof numDof];
                    
                    % link DOF to elements
                    for k = obj.listNumberElements
                        test = find(obj.listElements{k}.initListNodalDof.(type) == tabNodalVariables.(type)(j));
                        if ~isempty(test)
                            obj.listElements{k}.listNumberDof(test) = numDof;
                        end
                    end
                end
            end
            
            obj.listElementVariables = {};
            numElemVar = 0;
            listElementVariables = ListVariables(ElementVariableTypes);
            for i = 1:tabElementVariables.nVariableTypes
                typeVar = tabElementVariables.types{i};
                list = unique(floor(tabElementVariables.(typeVar))); 

                for k = 1:length(list)
                    %Create new var
                    numElemVar = numElemVar+1;
                    newVar = eval([upper(tabElementVariables.types{i}(1)) tabElementVariables.types{i}(2:end) '(' num2str(list(k)) ')']);
                    obj.listElementVariables{numElemVar} = newVar;
                    listElementVariables.(typeVar){list(k)} = newVar;
                    
                    % link var to elements
                    for l = obj.listNumberElements
                        indexVar = find(obj.listElements{l}.initListElementVar.(typeVar) == list(k));
                        if ~isempty(indexVar)
                            obj.listElements{l}.listElementVariables.(typeVar){indexVar} = newVar;
                        end
                    end
                    
                    % link element to var
                    newVar.linkElement = obj.listElements{list(k)};
                end
            end
            obj.listNumberElementVariables = 1:numElemVar;
            
            for i = 1:tabElementVariables.nVariableTypes
                typeVar = tabElementVariables.types{i};
                typeDof = eval([upper(typeVar(1)) typeVar(2:end) '.DofType']);
                for j = 1:length(tabElementVariables.(typeVar))
                    % create new DOF
                    numDof = numDof+1;
                    newDof = eval([typeDof '(' num2str(numDof) ')']);
                    obj.listDof{numDof} = newDof;
                    
                    % link DOF to var
                    numVar = floor(tabElementVariables.(typeVar)(j));
                    n = length(listElementVariables.(typeVar){numVar}.listDof);
                    listElementVariables.(typeVar){numVar}.listDof{n+1} = newDof;
                    listElementVariables.(typeVar){numVar}.listNumberDof = [listElementVariables.(typeVar){numVar}.listNumberDof numDof];
                    
                    % link DOF to elements
                    for k = obj.listNumberElements
                        test = find(obj.listElements{k}.initListElementDof.(typeVar) == tabElementVariables.(typeVar)(j), 1);
                        if ~isempty(test)
                            obj.listElements{k}.listNumberDof = [obj.listElements{k}.listNumberDof numDof];
                        end
                    end
                end
            end
            
            obj.listFreeDof = 1:numDof;
            obj.listNumberDof = 1:numDof;
            obj.nDof = numDof;
            
            for i = 1 : length(obj.listDof)
                if isa(obj.listDof{i},'MotionDof') || isa(obj.listDof{i}, 'RelativeNodeSE3')
                    obj.listMotionDof = [obj.listMotionDof obj.listDof{i}.numDof];
                    obj.numberMotionDof = obj.numberMotionDof + 1;
                end
            end
        end
        
        function defineBC(obj,BC)
            for i = obj.listNumberNodes
                if find(BC == obj.listNodes{i}.numberNode)
                    obj.listFixedNodes = [obj.listFixedNodes i];
                    obj.listFixedDof = [obj.listFixedDof obj.listNodes{i}.listNumberDof(1:min(6,length(obj.listNodes{i}.listNumberDof)))];
                end
            end
            for i = 1:length(obj.listFixedDof)
                obj.listFreeDof(obj.listFreeDof == obj.listFixedDof(i))= [];
            end
        end

        function initializeMatrices(obj,parameters)
            obj.M = zeros(obj.nDof,obj.nDof);
            obj.Ct = obj.M;
            obj.Kt = obj.M;
            obj.Phiq = obj.M;
            obj.St =  parameters.betap*obj.M+parameters.gammap*obj.Ct+parameters.coefK*obj.Kt+parameters.scaling*obj.Phiq;
        end
        
        function visu(obj,nstepin,nFigure)
            qmax = 0;
            for i = obj.listNumberNodes
                qmax = max(qmax,obj.listNodes{i}.getqmax);
            end
            if nargin >= 3
                for i = obj.listNumberNodes
                    qmax = ones(3,1)*1e-10;
                    qmax(1) = max(qmax(1),obj.listNodes{i}.getqmax(1));qmax(2) = max(qmax(2),obj.listNodes{i}.getqmax(2)); qmax(3) = max(qmax(3),obj.listNodes{i}.getqmax(3));
                end 
               
                nstep = size(obj.listNodes{obj.listNumberNodes(1)}.position,2);
                figure(nFigure);
                for i = 1:nstep
                    for j = obj.listNumberElements
                        obj.listElements{j}.visu(i);
                        if ~isempty(nstepin)    
                              obj.listElements{j}.visu(1);
                        end
                        hold on
                    end
                    if size(qmax,1)==1
                        axis(qmax*[-1 1 -1 1 -1 1])
                    elseif size(qmax,1)==2
                        axis([-1.1*qmax(1) 1.1*qmax(1) -1.1*qmax(2) 1.1*qmax(2)]) 
                    else
                        axis([-1.1*qmax(1) 1.1*qmax(1) -1.1*qmax(2) 1.1*qmax(2) -1.1*qmax(3) 1.1*qmax(3)]) 
                    end
                    view([45 45 45]);
                    pause(0.01)
                    hold off
                end
                hold on
            elseif nargin == 2
                nstep = nstepin;
                for i = nstep
                    for j = obj.listNumberElements
                        obj.listElements{j}.visu(i);
                        hold on
                    end
                    grid on
                end
            else
                nstep = size(obj.listNodes{obj.listNumberNodes(1)}.position,2);
                for i = 1:nstep
                    for j = obj.listNumberElements
                        obj.listElements{j}.visu(i);
                        hold on
                    end
                axis(qmax*[-1 1 -1 1 -1 1])
                grid on
                pause(0.01)
                hold off
                end
            end
        end     
    end   
end