%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef MotionDof < DegreeOfFreedom
    
    properties
        d_value = 0;
        dd_value = 0;
        a_value = 0;
        state;
        numberstate;
    end
    
    methods
        function obj = MotionDof(input)
            obj = obj@DegreeOfFreedom(input);
        end
        
        function staticPrediction(obj)
            obj.value = 0;
        end
        
        function staticCorrection(obj, ~, inc)
            obj.value_p = obj.value;
            obj.value = obj.value + inc;
        end
        
        function obj = initializePosition(obj,init)
            obj.value = init;
        end
        
        function obj = initializeVelocity(obj,init)
            obj.d_value = init;
        end
        
        function obj = initializeAcceleration(obj,init)
            obj.dd_value = init;
            obj.a_value = init;
        end
        
        function dynamicPrediction(obj,parameters)
            obj.value = parameters.predictionParameters(1)*obj.d_value + parameters.predictionParameters(2)*obj.a_value;
            obj.d_value = obj.d_value + parameters.predictionParameters(3)*obj.a_value;
            obj.a_value = parameters.predictionParameters(4)*obj.dd_value - parameters.predictionParameters(5)*obj.a_value;
            obj.value = obj.value + parameters.predictionParameters(6)*obj.a_value;
            obj.d_value = obj.d_value + parameters.predictionParameters(7)*obj.a_value;
            obj.dd_value = 0;
        end
        
        function dynamicCorrection(obj,parameters,inc)
            obj.staticCorrection(parameters, inc);
            obj.d_value = obj.d_value + parameters.gammap*inc;
            obj.dd_value = obj.dd_value + parameters.betap*inc;
        end
        
        function updateA(obj,parameters)
            obj.a_value = obj.a_value + parameters.coefUpdateA*obj.dd_value;
        end
        
        function staticCorrection_Opti(obj,inc)
            obj.value = inc;
        end
        
        function optiCorrection(obj,parameters,step,incQ,incV,incVdot) 
            obj.staticCorrection_Opti(incQ);
            obj.d_value = incV;
            obj.dd_value = incVdot;
        end
        
    end
    
end
