%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef DegreeOfFreedom < handle
    
    properties
        numDof = [];
        value = 0;
        value_p = 0;
    end
    
    methods
        function obj = DegreeOfFreedom(input)
            obj.numDof = input;
        end
        
        function initializeStatic(obj,nstep)
        end
        
        function initializeDynamic(obj,nstep)
            obj.initializeStatic(nstep);
        end
        
        function obj = initializePosition(obj,init)
        end
        
        function obj = initializeVelocity(obj,init)
        end
        
        function obj = initializeAcceleration(obj,init)
        end
        
        function continuationPrediction(obj,step,hv)
            obj.value = hv;
        end
        
        function staticPrediction(obj)
        end
        
        function staticCorrection(obj, parameters, inc)
        end
        
        function obj = dynamicPrediction(obj,parameters)
            obj.staticPrediction();
        end
        
        function obj = dynamicCorrection(obj,parameters,dq)
            obj.staticCorrection(parameters,dq);
        end
        
        function obj = updateA(obj,parameters,step)
        end
    end
    
end

