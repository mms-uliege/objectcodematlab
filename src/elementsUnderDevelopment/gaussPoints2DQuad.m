%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = gaussPoints2DQuad(nGP)

if nGP == 1
    out = [0 2];
elseif nGP == 2
    out = [-1/sqrt(3) 1; 1/sqrt(3) 1];
elseif nGP == 3
    out = [0 8/9; -sqrt(3/5) 5/9; sqrt(3/5) 5/9];
elseif nGP == 4
    out = [sqrt((3-2*sqrt(6/5))/7) (18+sqrt(30))/36;
        -sqrt((3-2*sqrt(6/5))/7) (18+sqrt(30))/36;
        sqrt((3+2*sqrt(6/5))/7) (18-sqrt(30))/36;
        -sqrt((3+2*sqrt(6/5))/7) (18-sqrt(30))/36];
elseif nGP == 5
    out = [0 128/225;
        1/3*sqrt(5-2*sqrt(10/7)) (322+13*sqrt(70))/900;
        -1/3*sqrt(5-2*sqrt(10/7)) (322+13*sqrt(70))/900;
        1/3*sqrt(5+2*sqrt(10/7)) (322-13*sqrt(70))/900;
        -1/3*sqrt(5+2*sqrt(10/7)) (322-13*sqrt(70))/900];
elseif nGP == 6
    out = [0.6612093864662645 0.3607615730481386;
        -0.6612093864662645 0.3607615730481386;
        -0.2386191860831969 0.4679139345726910;
        0.2386191860831969 0.4679139345726910;
        -0.9324695142031521 0.1713244923791704;
        0.9324695142031521 0.1713244923791704];
elseif nGP == 10
    out = [-0.1488743389816312 0.2955242247147529;
        0.1488743389816312 0.2955242247147529;
        -0.4333953941292472 0.2692667193099963;
        0.4333953941292472 0.2692667193099963;
        -0.6794095682990244 0.2190863625159820;
        0.6794095682990244 0.2190863625159820;
        -0.8650633666889845 0.1494513491505806;
        0.8650633666889845 0.1494513491505806
        -0.9739065285171717 0.0666713443086881;
        0.9739065285171717 0.0666713443086881];
end

end
