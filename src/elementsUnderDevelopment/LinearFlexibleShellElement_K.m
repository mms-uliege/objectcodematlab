%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef LinearFlexibleShellElement_K < Element
    %FLEXIBLEBEAMELEMENT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        E; nu; e; Area;
        F; FS; Gravity;
        dPhidq; K;
        nNodes; nCon; nDof;
        nGP = 4; ListGP; FI; dFI;
        
        epsilon = zeros(12,1);
        
        X0;
    end
    
    methods
        function obj = LinearFlexibleShellElement_K(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,listNodes,~)
            obj.nNodes = length(listNodes);
            obj.initListNodalDof.nodeSE3Lin = zeros(1,obj.nNodes*6);
            for i = 1:obj.nNodes
                obj.initListNodalDof.nodeSE3Lin((1:6)+6*(i-1)) = listNodes(i)+(0.01:0.01:0.06);
            end
            obj.initListElementDof.lagrangeMultiplierSet = obj.elementNumber+(0.01:0.01:0.03);
            obj.initListElementVar.lagrangeMultiplierSet = obj.elementNumber;
        end
        
        
        function initializeStatic(obj,nstep)
            obj.nDof = 6*obj.nNodes;
            
            if obj.nNodes == 4
                obj.FI = @(s,t) 0.25*[(1-s)*(1-t)*eye(3) (1+s)*(1-t)*eye(3) (1+s)*(1+t)*eye(3) (1-s)*(1+t)*eye(3)];
                
                obj.dFI{1} = @(s,t) 0.25*[(1-t)*(-1)*eye(3) (1-t)*(+1)*eye(3) (1+t)*(+1)*eye(3) (1+t)*(-1)*eye(3)];
                obj.dFI{2} = @(s,t) 0.25*[(-1)*(1-s)*eye(3) (-1)*(1+s)*eye(3) (+1)*(1+s)*eye(3) (+1)*(1-s)*eye(3)];
                
                obj.ListGP = @(n) ListGPQuad(n);
                
            elseif obj.nNodes == 3
                obj.FI = @(s,t) [(1-s-t)*eye(6) s*eye(6) t*eye(6)];
                
                obj.dFI{1} = @(s,t) [(-1)*eye(6) 1*eye(6) (0)*eye(6)];
                obj.dFI{2} = @(s,t) [(-1)*eye(6) (0)*eye(6) (1)*eye(6)];
                
                obj.ListGP = @(n) ListGPTri(n);
            end
            
            obj.X0 = zeros(6*obj.nNodes,1);
            x0 = zeros(3*obj.nNodes,1);
            for i = 1:obj.nNodes
                obj.X0((1:3)+(i-1)*6) = obj.listNodes{i}.position(:,1);
                x0((1:3)+(i-1)*3) = obj.listNodes{i}.position(:,1);
            end
            x0st = @(s,t) obj.FI(s,t)*x0;
            dx0st = @(s,t,i) obj.dFI{i}(s,t)*x0;
            dX0st = @(s,t,i) [obj.dFI{i}(s,t)*x0; zeros(3,1)];
            
            obj.nCon = 3;
            
            if isempty(obj.F)
                obj.F = @(s,t) zeros(3,1);
            end
            
            GP = obj.ListGP(obj.nGP);
            
            obj.Area = 0;
            obj.K = zeros(6*obj.nNodes,6*obj.nNodes);
            obj.dPhidq = zeros(obj.nCon,6*obj.nNodes);
            obj.FS = zeros(6*obj.nNodes,1);
            
            L1 = norm(obj.listNodes{1}.position(:,1)-obj.listNodes{2}.position(:,1));
            L2 = norm(obj.listNodes{2}.position(:,1)-obj.listNodes{3}.position(:,1));
            
            ind0 = 0;
            for i = 1:size(GP,1)
                s = GP(i,1); t = GP(i,2);
                ind0 = ind0+1;
                
                fU0_1 = [eye(3) zeros(3)]*dX0st(s,t,1); fU0_2 = [eye(3) zeros(3)]*dX0st(s,t,2);
                
                n = tild(fU0_1)*fU0_2; n = n/norm(n);
                
                g = [fU0_1 fU0_2 n];
                [ei,lam] = eig(g'*g); lam = diag(lam);
                U_ru = sqrt(lam(1))*(ei(:,1)*ei(:,1)') + sqrt(lam(2))*(ei(:,2)*ei(:,2)') + sqrt(lam(3))*(ei(:,3)*ei(:,3)');
                R0 = g*U_ru^-1;
                c1 = R0(:,1); c2 = R0(:,2);
                
                FIst = obj.FI(s,t);
                dFI1st = obj.dFI{1}(s,t);
                dFI2st = obj.dFI{2}(s,t);
                
                Q_N1 = zeros(6,6*obj.nNodes);
                Q_N2 = zeros(6,6*obj.nNodes);
                
                for j = 1:obj.nNodes
                    Q_N1(1:3,(1:3)+(j-1)*6) = dFI1st(:,(1:3)+(j-1)*3);
                    Q_N1(1:3,(4:6)+(j-1)*6) = 0.5*((tild(obj.X0((1:3)+(j-1)*6))-tild(x0st(s,t)))*dFI1st(:,(1:3)+(j-1)*3) + tild(dx0st(s,t,1))*FIst(:,(1:3)+(j-1)*3));
                    Q_N1(4:6,(4:6)+(j-1)*6) = dFI1st(:,(1:3)+(j-1)*3);
                    
                    Q_N2(1:3,(1:3)+(j-1)*6) = dFI2st(:,(1:3)+(j-1)*3);
                    Q_N2(1:3,(4:6)+(j-1)*6) = 0.5*((tild(obj.X0((1:3)+(j-1)*6))-tild(x0st(s,t)))*dFI2st(:,(1:3)+(j-1)*3) + tild(dx0st(s,t,2))*FIst(:,(1:3)+(j-1)*3));
                    Q_N2(4:6,(4:6)+(j-1)*6) = dFI2st(:,(1:3)+(j-1)*3);
                end
                Q_N = [Q_N1; Q_N2];
                
                a = GP(i,3)*sqrt(det([fU0_1 fU0_2]'*[fU0_1 fU0_2]));
                
                Ke = a*KPlate(fU0_1,fU0_2,n,obj.E,obj.nu,obj.e);
                Ge = obj.E/(2*(1+obj.nu))*obj.e; EI = obj.E*obj.e^3;
                Ke(3,3) = (1/Ge + L2^2/EI)^-1; Ke(9,9) = (1/Ge + L1^2/EI)^-1;
                
                obj.Area = obj.Area + a;
                obj.K = obj.K + Q_N'*Ke*Q_N;
                obj.dPhidq(1,:) = obj.dPhidq(1,:) + a*obj.e*[c2' zeros(1,3) -c1' zeros(1,3)]*Q_N;
                obj.dPhidq(2,:) = obj.dPhidq(2,:) + a*obj.e*[[0 0 1] zeros(1,3) [0 0 0] zeros(1,3)]*Q_N;
                obj.dPhidq(3,:) = obj.dPhidq(3,:) + a*obj.e*[[0 0 0] zeros(1,3) [0 0 1] zeros(1,3)]*Q_N;
                
                Q = zeros(3,6*obj.nNodes);
                for j = 1:obj.nNodes
                    Q(:,(1:3)+(j-1)*6) = FIst(:,(1:3)+(j-1)*3);
                    Q(:,(4:6)+(j-1)*6) = 0.5*FIst(:,(1:3)+(j-1)*3)*(tild(obj.X0((1:3)+(j-1)*6))-tild(x0st(s,t)));
                end
                
                obj.FS = obj.FS + a*(Q'*obj.F(s,t));
                
            end
            obj.dPhidq = obj.dPhidq;
        end
        
        function obj = initializeDynamic(obj,nstep)
            obj.initializeStatic(nstep);
            obj.MCS = [obj.O0 zeros(3); zeros(3) obj.O0]*obj.MCS*[obj.O0' zeros(3); zeros(3) obj.O0'];
            obj.M = integration1D(@(s) fcnQ(s,obj.h0)'*obj.MCS*fcnQ(s,obj.h0),5,0,1)*obj.L;
        end
        
        function resModel = assembleResidueStatic(obj,resModel,parameters)
            
            X = zeros(6*obj.nNodes,1);
            for i = 1:obj.nNodes
                X((1:3)+(i-1)*6) = obj.listNodes{i}.position(:,parameters.timestep);
                X((4:6)+(i-1)*6) = obj.listNodes{i}.rotation(:,parameters.timestep);
            end
            obj.epsilon = X-obj.X0;
            res = obj.K*(X-obj.X0) + obj.dPhidq'*obj.listElementVariables.lagrangeMultiplierSet{1}.value(:,parameters.timestep);
            res = res - obj.FS*parameters.time;
            
            resModel(1:obj.nDof) = resModel(1:obj.nDof) + res;
            resModel(end-(obj.nCon-1):end) = resModel(end-(obj.nCon-1):end) + parameters.scaling*obj.dPhidq*(X-obj.X0);
            
            obj.refValForce = norm(res);
        end
        
        function resModel = assembleResidueDynamic(obj,resModel,parameters)
            resModel = assembleResidueStatic(obj,resModel,parameters);
            
            %             obj.M = integration1D(@(s) fcnQ(s,obj.h)'*obj.MCS*fcnQ(s,obj.h),4,0,1)*obj.L;
            %             resModel = resModel + obj.M*[obj.listNodes{1}.acceleration(:,parameters.timestep); obj.listNodes{2}.acceleration(:,parameters.timestep)];
            %
            %             v_AB = [obj.listNodes{1}.velocity(:,parameters.timestep);obj.listNodes{2}.velocity(:,parameters.timestep)];
            %             obj.Gyro = integration1D(@(s) fcnQ(s,obj.h)'*(obj.MCS*fcnQdot(s,obj.h,obj.P*v_AB) - hatSE3(fcnQ(s,obj.h)*v_AB)'*obj.MCS*fcnQ(s,obj.h)),4,0,1)*obj.L;
            %             resModel = resModel + obj.Gyro*v_AB;
            %             obj.refValForce = obj.refValForce + norm(obj.M*[obj.listNodes{1}.acceleration(:,parameters.timestep);obj.listNodes{2}.acceleration(:,parameters.timestep)]);
        end
        
        function StModel = assembleTangentOperatorStatic(obj,StModel,parameters)
            StModel(1:obj.nDof,1:obj.nDof) = StModel(1:obj.nDof,1:obj.nDof) + parameters.coefK*obj.K;
            sPhi_q = parameters.scaling*obj.dPhidq;
            StModel(end-(obj.nCon-1):end,1:obj.nDof) = StModel(end-(obj.nCon-1):end,1:obj.nDof) + sPhi_q; StModel(1:obj.nDof,end-(obj.nCon-1):end) = StModel(1:obj.nDof,end-(obj.nCon-1):end) + sPhi_q';
        end
        
        function StModel = assembleTangentOperatorDynamic(obj,StModel,parameters)
            StModel = assembleTangentOperatorStatic(obj,StModel,parameters);
            %             StModel = StModel + parameters.gammap*obj.Gyro + parameters.betap*obj.M;
        end
        
        function visu(obj,step)
            x = cell(obj.nNodes,1);
            for i = 1:obj.nNodes
                x{i} = obj.listNodes{i}.position(:,step);
            end
            for i = 1:(obj.nNodes-1)
                plot3([x{i}(1) x{i+1}(1)],[x{i}(2) x{i+1}(2)],[x{i}(3) x{i+1}(3)],'-*')
                hold on
            end
            plot3([x{1}(1) x{obj.nNodes}(1)],[x{1}(2) x{obj.nNodes}(2)],[x{1}(3) x{obj.nNodes}(3)],'-*')
        end
    end
end

function out = ListGPQuad(nGP)

GP = gaussPoints2DQuad(nGP);

out = zeros(nGP^2,3);
k = 0;
for i = 1:nGP
    for j = 1:nGP
        k = k+1;
        out(k,:) = [GP(i,1) GP(j,1) GP(i,2)*GP(j,2)];
    end
end
end

function out = ListGPTri(nGP)

[x,y,w] = gaussPointsTriangle(nGP);

out = [x y 0.5*w];

end

function K = KPlate(fu0_1,fu0_2,n,E,nu,e)
g_s_ij = [fu0_1 fu0_2 n]'*[fu0_1 fu0_2 n];
g_u_ij = inv(g_s_ij); g = g_u_ij;

L = E*nu/((1+nu)*(1-2*nu)); G = E/(2*(1+nu)); L = 2*L*G/(L+2*G);

HF = zeros(3,3,3,3);
for l = 1:3
    for m = 1:3
        for a = 1:3
            for b = 1:3
                HF(l,m,a,b) = L*g(l,m)*g(a,b) + G*(g(l,a)*g(m,b)+g(l,b)*g(m,a));
            end
        end
    end
end

H2 = zeros(6);
ie = [1 1; 2 2; 1 2; 1 3; 2 3; 3 3];
for i = 1:6
    for j = 1:6
        H2(i,j) = HF(ie(i,1),ie(i,2),ie(j,1),ie(j,2));
    end
end
H = H2(1:5,1:5);

N = @(u) [eye(3) u*[0 n(3) -n(2); -n(3) 0 n(1); n(2) -n(1) 0]];%-tild(u*n)
O = zeros(1,6);
D = @(u) [fu0_1'*N(u) O; O fu0_2'*N(u); fu0_2'*N(u) fu0_1'*N(u); n'*N(u) O; O n'*N(u)];

K = integration1D(@(u) D(u)'*H*D(u),3,-e/2,e/2);
end
