%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef ExternalForceLinShell < Element
    %EXTERNALFORCELINSHELL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        amplitude = 1;
        frequency = 1;
        DOF;
    end
    
    methods
        function obj = ExternalForceLinShell(input)
            obj = obj@Element(input);
        end
        function defineDof(obj,listNodes,~)
            obj.initListNodalDof.nodeSE3Lin = listNodes(1)+(0.01:0.01:0.06);
        end
        
        function resModel = assembleResidueStatic(obj,resModel,parameters)
            F = obj.amplitude*parameters.time;
            resModel(obj.DOF) = resModel(obj.DOF) - F;
            obj.refValForce = norm(F);
        end
        
    end
    
end

