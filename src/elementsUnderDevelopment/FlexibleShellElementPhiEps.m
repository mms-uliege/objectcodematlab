%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef FlexibleShellElementPhiEps < Element
    %FLEXIBLEBEAMELEMENT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        E; nu; e; Area;
        F; Gravity; M;
        Phi_q; KT;
        nNodes; nCon; nDof;
        nGP = 2; ListGP; FI; dFI; LI; FINL;
        hi0; f0; n; R0; Hi; a;
    end
    
    methods
        function obj = FlexibleShellElementPhiEps(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,listNodes,~)
            obj.nNodes = length(listNodes);
            obj.initListNodalDof.nodeSE3NL = zeros(1,obj.nNodes*7);
            for i = 1:obj.nNodes
                obj.initListNodalDof.nodeSE3NL((1:8)+8*(i-1)) = listNodes(i)+(0.01:0.01:0.08);
            end
            obj.initListElementDof.lagrangeMultiplierSet = obj.elementNumber+(0.01:0.01:0.02);
            obj.initListElementVar.lagrangeMultiplierSet = obj.elementNumber;
        end
        
        function [H,hi] = solveH(obj,s,t)
            FIst = obj.FI(s,t);
            hi = zeros(6*obj.nNodes,1);
            x = zeros(3,1); hO = zeros(3,1);
            for i = 1:obj.nNodes
                x = x + FIst(1:3,(1:3)+(i-1)*6)*obj.Hi{i}(1:3,4);
                h = FIst(:,(1:6)+(i-1)*6)*logSE3(obj.Hi{1}\obj.Hi{i});
                hO = hO + h(4:6);
            end
            H = eye(4); H(1:3,4) = x; H(1:3,1:3) = obj.Hi{1}(1:3,1:3)*expSO3(hO);
            for i = 1:obj.nNodes
                hi((1:6)+6*(i-1)) = logSE3(H\obj.Hi{i});
            end
            res = FIst*hi;
            nit = 0;
            while and(norm(res) > 1e-8, nit < 10)
                nit = nit+1;
                S = zeros(6);
                for i = 1:obj.nNodes
                    ind = (1:6)+6*(i-1);
                    S = S - FIst(:,ind)*computeTSE3inv(-hi(ind));
                end
                dh = -S\res;
                H = H*expSE3(dh);
                for i = 1:obj.nNodes
                    hi((1:6)+6*(i-1)) = logSE3(H\obj.Hi{i});
                end
                res = FIst*hi;
            end
            if nit == 10
                error('solveH does not converge')
            end
        end
        
        function initializeStatic(obj,nstep)
            obj.nDof = 6*obj.nNodes;
            
            if obj.nNodes == 4
                obj.FI = @(s,t) 0.25*[(1-s)*(1-t)*eye(6) (1+s)*(1-t)*eye(6) (1+s)*(1+t)*eye(6) (1-s)*(1+t)*eye(6)];
                obj.FINL = @(s,t) 0.25*[(1-s)*(1-t)*eye(1) (1+s)*(1-t)*eye(1) (1+s)*(1+t)*eye(1) (1-s)*(1+t)*eye(1)];
                
                obj.dFI{1} = @(s,t) 0.25*[(1-t)*(-1)*eye(6) (1-t)*(+1)*eye(6) (1+t)*(+1)*eye(6) (1+t)*(-1)*eye(6)];
                obj.dFI{2} = @(s,t) 0.25*[(-1)*(1-s)*eye(6) (-1)*(1+s)*eye(6) (+1)*(1+s)*eye(6) (+1)*(1-s)*eye(6)];
                
                obj.ListGP = @(n) ListGPQuad(n);
                
            elseif obj.nNodes == 3
                obj.FI = @(s,t) [(1-s-t)*eye(6) s*eye(6) t*eye(6)];
                
                obj.dFI{1} = @(s,t) [(-1)*eye(6) 1*eye(6) (0)*eye(6)];
                obj.dFI{2} = @(s,t) [(-1)*eye(6) (0)*eye(6) (1)*eye(6)];
                
                obj.ListGP = @(n) ListGPTri(n);
            end
            
            obj.Hi = cell(obj.nNodes,1);
            for i = 1:obj.nNodes
                Ri = vect9ToMat3(obj.listNodes{i}.R(:,1));
                xi = obj.listNodes{i}.position(:,1);
                obj.Hi{i} = [Ri xi; 0 0 0 1];
            end
            
            %             obj.LI = @(s,t,u) 1;
            obj.nCon = 10;
            
            GP = obj.ListGP(obj.nGP);
            
            obj.hi0 = zeros(6*obj.nNodes,size(GP,1));
            obj.f0 = cell(2,1); obj.f0{1} = zeros(6,size(GP,1)); obj.f0{2} = zeros(6,size(GP,1));
            obj.R0 = cell(size(GP,1),1);
            obj.n = zeros(3,size(GP,1));
            obj.a = zeros(1,size(GP,1)); obj.Area = 0;
            ind0 = 0;
            for i = 1:size(GP,1)
                s = GP(i,1); t = GP(i,2);
                ind0 = ind0+1;
                
                [~,obj.hi0(:,ind0)] = obj.solveH(s,t);
                
                FIst = obj.FI(s,t); dFIst = cell(2,1); dFIst{1} = obj.dFI{1}(s,t); dFIst{2} = obj.dFI{2}(s,t);
                
                A = zeros(6);
                for k = 1:obj.nNodes
                    ind = (1:6)+6*(k-1);
                    A = A + FIst(:,ind)*computeTSE3inv(-obj.hi0(ind));
                end
                A = A^-1;
                
                for l = 1:2
                    for k = 1:obj.nNodes
                        ind = (1:6)+6*(k-1);
                        obj.f0{l}(:,ind0) = obj.f0{l}(:,ind0) + A*dFIst{l}(:,ind)*obj.hi0(ind,ind0);
                    end
                end
                obj.n(:,ind0) = tild(obj.f0{1}(1:3,ind0))*obj.f0{2}(1:3,ind0); obj.n(:,ind0) = obj.n(:,ind0)/norm(obj.n(:,ind0));
                
                F = [obj.f0{1}(1:3,ind0) obj.f0{2}(1:3,ind0) obj.n(:,ind0)];
                [ei,lam] = eig(F'*F); lam = diag(lam);
                U_ru = sqrt(lam(1))*(ei(:,1)*ei(:,1)') + sqrt(lam(2))*(ei(:,2)*ei(:,2)') + sqrt(lam(3))*(ei(:,3)*ei(:,3)');
                obj.R0{ind0} = F*U_ru^-1;
                
                obj.a(ind0) = GP(i,3)*sqrt(det([obj.f0{1}(1:3,ind0) obj.f0{2}(1:3,ind0)]'*[obj.f0{1}(1:3,ind0) obj.f0{2}(1:3,ind0)]));
                obj.Area = obj.Area + obj.a(ind0);
            end
            if isempty(obj.F)
                obj.F = @(s,t) 0;
            end
            if isempty(obj.M)
                obj.M = @(s,t) 0;
            end
            parameters.timestep = 1;
            parameters.scaling = 1;
            assembleResidueStatic(obj,zeros(6*obj.nNodes+obj.nCon,1),parameters);
        end
        
        function obj = initializeDynamic(obj,nstep)
            obj.initializeStatic(nstep);
            obj.MCS = [obj.O0 zeros(3); zeros(3) obj.O0]*obj.MCS*[obj.O0' zeros(3); zeros(3) obj.O0'];
            obj.M = integration1D(@(s) fcnQ(s,obj.h0)'*obj.MCS*fcnQ(s,obj.h0),5,0,1)*obj.L;
        end
        
        function resModel = assembleResidueStatic(obj,resModel,parameters)
            GP = obj.ListGP(obj.nGP);
            
            obj.KT = zeros(obj.nDof);
            obj.Phi_q = zeros(obj.nCon,obj.nDof);
            res = zeros(obj.nDof,1);
            
            lambda = zeros(obj.nCon,1);
            locDof = []; locLam = [];
            for i = 1:obj.nNodes
                Ri = vect9ToMat3(obj.listNodes{i}.R(:,parameters.timestep));
                xi = obj.listNodes{i}.position(:,parameters.timestep);
                obj.Hi{i} = [Ri xi; 0 0 0 1];
                lambda(i) = obj.listNodes{i}.lambda(parameters.timestep);
                locDof = [locDof (1:6)+(i-1)*8]; locLam = [locLam (7:8)+(i-1)*8];
            end
            locLam = [locLam 33 34];
            lambda(9:10) = obj.listElementVariables.lagrangeMultiplierSet{1}.value(:,parameters.timestep);
            
            ind0 = 0;
            for i = 1:size(GP,1)
                s = GP(i,1); t = GP(i,2);
                ind0 = ind0+1;
                
                FIst = obj.FI(s,t); dFIst = cell(2,1); dFIst{1} = obj.dFI{1}(s,t); dFIst{2} = obj.dFI{2}(s,t);
                
                [Hst,hi] = obj.solveH(s,t);
                
                A = zeros(6);
                for k = 1:obj.nNodes
                    ind = (1:6)+6*(k-1);
                    A = A + FIst(:,ind)*computeTSE3inv(-hi(ind));
                end
                A = A^-1;
                
                f = cell(2,1); f{1} = zeros(6,1); f{2} = zeros(6,1);
                Q = zeros(6,6*obj.nNodes);
                
                for k = 1:obj.nNodes
                    ind = (1:6)+6*(k-1);
                    Q(:,ind) = A*FIst(:,ind)*computeTSE3inv(hi(ind));
                    for l = 1:2
                        f{l} = f{l} + A*dFIst{l}(:,ind)*hi(ind);
                    end
                end
                
                Q_N = cell(2,1);
                for l = 1:2
                    Q_N{l} = zeros(6,6*obj.nNodes);
                    for k = 1:obj.nNodes
                        ind = (1:6)+6*(k-1);
                        B_i_alpha = dFIst{l}(:,ind) + FIst(:,ind)*computeDTSE3invC(-hi(ind),f{l});
                        Q_N{l} = Q_N{l} - B_i_alpha*computeTSE3inv(-hi(ind))*Q;
                        Q_N{l}(:,ind) = Q_N{l}(:,ind) + B_i_alpha*computeTSE3inv(hi(ind));
                    end
                    Q_N{l} = A*Q_N{l};
                end
                
                K = obj.a(ind0)*KPlate(obj.f0{1}(1:3,ind0),obj.f0{2}(1:3,ind0),obj.n(:,ind0),obj.E,obj.nu,obj.e);
                obj.KT = obj.KT + [Q_N{1}' Q_N{2}']*K*[Q_N{1}; Q_N{2}];
                
                res = res + [Q_N{1}' Q_N{2}']*K*[f{1}-obj.f0{1}(:,ind0); f{2}-obj.f0{2}(:,ind0)];
                
                if norm(obj.F(s,t))
                    F = obj.a(ind0)*Hst(1:3,1:3)'*obj.F(s,t)*parameters.timestep;
                    res = res + Q(1:3,:)'*F;
                    obj.KT = obj.KT + Q(1:3,:)'*tild(F)*Q(4:6,:);
                end
                
                if norm(obj.M(s,t))
                    M = obj.a(ind0)*obj.M(s,t)*parameters.timestep;
                    res = res + Q(4:6,:)'*M;
                end
                
                %                 if norm(obj.Gravity)
                %                     N = integration1D(@(u) [eye(3) -tild(u*obj.n(:,ind0))],2,-EP.e/2,EP.e/2);
                %                     G = obj.a(ind0)*(Hst(1:3,1:3)'*EP.Gravity(:)*EP.rho);
                %                     res = res - Q'*N'*G;
                %                     obj.KT(1:obj.nDof,1:obj.nDof) = obj.KT(1:obj.nDof,1:obj.nDof) - Q'*(N'*tild(G))*Q(4:6,:);
                %                 end
                
                %                 i1 = [1 0 0]'; i2 = [0 1 0]';
                %                 N = @(u) obj.R0{ind0}'*[eye(3) -tild(u*obj.n(:,ind0))];
                %                 Phi = i2'*N(0)*f{1} - i1'*N(0)*f{2};
                % %                 resModel(end) = resModel(end) + obj.a(ind0)*integration1D(@(u) obj.LI(s,t,u)'*Phi(u),2,-obj.e/2,obj.e/2);
                %                 resModel(locLam) = resModel(locLam) + parameters.scaling*obj.a(ind0)*obj.FINL(s,t)'*Phi;
                %                 Phi_qu = [N(0)'*i2; -N(0)'*i1];
                %                 obj.Phi_q = obj.Phi_q + obj.a(ind0)*obj.FINL(s,t)'*Phi_qu'*[Q_N{1}; Q_N{2}];
                %
                % %                 res = res + obj.a(ind0)*(obj.FINL(s,t)'*Phi_qu'*[Q_N{1}; Q_N{2}])'*lambda;
                %                 res = res + obj.a(ind0)*[Q_N{1}; Q_N{2}]'*Phi_qu*obj.FINL(s,t)*lambda;
                
                mode1 = [zeros(1,9) obj.n(:,ind0)'];
                mode2 = [zeros(1,3) obj.n(:,ind0)' zeros(1,6)];
                tn = tild(obj.n(:,ind0));
                mode3 = [(tn*obj.f0{1}(1:3,ind0))' zeros(1,3) (tn*obj.f0{2}(1:3,ind0))' zeros(1,3)];
                mode4 = [zeros(1,3) (tn*tn*obj.f0{1}(1:3,ind0))' zeros(1,3) (tn*tn*obj.f0{2}(1:3,ind0))'];
                
                mode = [mode1;mode2;mode3;mode4];
                
                FIla = zeros(10,4);
                FIla(1:4,1) = obj.FINL(s,t)';
                FIla(5:8,2) = obj.FINL(s,t)';
                FIla(9,3) = 1;
                FIla(10,4) = 1;
                
                Phi = FIla*mode*[f{1}-obj.f0{1}(:,ind0); f{2}-obj.f0{2}(:,ind0)];
                resModel(locLam) = resModel(locLam) + parameters.scaling*obj.a(ind0)*Phi;
                
                obj.Phi_q = obj.Phi_q + obj.a(ind0)*FIla*mode*[Q_N{1}; Q_N{2};];
                
                res = res + obj.a(ind0)*[Q_N{1}; Q_N{2}]'*mode'*FIla'*lambda;
            end
            resModel(locDof) = resModel(locDof) + res;% + obj.Phi_q'*obj.listElementVariables.lagrangeMultiplierSet{1}.value(:,parameters.timestep);
            %             resModel(locLam) = parameters.scaling*resModel(locLam);
            obj.KT = (obj.KT+obj.KT')/2;
            obj.refValForce = norm(res);
        end
        
        function resModel = assembleResidueDynamic(obj,resModel,parameters)
            resModel = assembleResidueStatic(obj,resModel,parameters);
            
            %             obj.M = integration1D(@(s) fcnQ(s,obj.h)'*obj.MCS*fcnQ(s,obj.h),4,0,1)*obj.L;
            %             resModel = resModel + obj.M*[obj.listNodes{1}.acceleration(:,parameters.timestep); obj.listNodes{2}.acceleration(:,parameters.timestep)];
            %
            %             v_AB = [obj.listNodes{1}.velocity(:,parameters.timestep);obj.listNodes{2}.velocity(:,parameters.timestep)];
            %             obj.Gyro = integration1D(@(s) fcnQ(s,obj.h)'*(obj.MCS*fcnQdot(s,obj.h,obj.P*v_AB) - hatSE3(fcnQ(s,obj.h)*v_AB)'*obj.MCS*fcnQ(s,obj.h)),4,0,1)*obj.L;
            %             resModel = resModel + obj.Gyro*v_AB;
            %             obj.refValForce = obj.refValForce + norm(obj.M*[obj.listNodes{1}.acceleration(:,parameters.timestep);obj.listNodes{2}.acceleration(:,parameters.timestep)]);
        end
        
        function StModel = assembleTangentOperatorStatic(obj,StModel,parameters)
            locDof = []; locLam = [];
            for i = 1:obj.nNodes
                locDof = [locDof (1:6)+(i-1)*8]; locLam = [locLam (7:8)+(i-1)*8];
            end
            locLam = [locLam 33 34];
            StModel(locDof,locDof) = StModel(locDof,locDof) + parameters.coefK*obj.KT;
            sPhi_q = parameters.scaling*obj.Phi_q;
            StModel(locLam,locDof) = StModel(locLam,locDof) + sPhi_q; StModel(locDof,locLam) = StModel(locDof,locLam) + sPhi_q';
        end
        
        function StModel = assembleTangentOperatorDynamic(obj,StModel,parameters)
            StModel = assembleTangentOperatorStatic(obj,StModel,parameters);
            %             StModel = StModel + parameters.gammap*obj.Gyro + parameters.betap*obj.M;
        end
        
        function visu(obj,step)
            x = cell(obj.nNodes,1);
            for i = 1:obj.nNodes
                x{i} = obj.listNodes{i}.position(:,step);
            end
            for i = 1:(obj.nNodes-1)
                plot3([x{i}(1) x{i+1}(1)],[x{i}(2) x{i+1}(2)],[x{i}(3) x{i+1}(3)],'-*')
                hold on
            end
            plot3([x{1}(1) x{obj.nNodes}(1)],[x{1}(2) x{obj.nNodes}(2)],[x{1}(3) x{obj.nNodes}(3)],'-*')
        end
    end
end

function out = ListGPQuad(nGP)

GP = gaussPoints2DQuad(nGP);

out = zeros(nGP^2,3);
k = 0;
for i = 1:nGP
    for j = 1:nGP
        k = k+1;
        out(k,:) = [GP(i,1) GP(j,1) GP(i,2)*GP(j,2)];
    end
end
end

function out = ListGPTri(nGP)

[x,y,w] = gaussPointsTriangle(nGP);

out = [x y 0.5*w];

end

function K = KPlate(fu0_1,fu0_2,n,E,nu,e)
g_s_ij = [fu0_1 fu0_2 n]'*[fu0_1 fu0_2 n];
g_u_ij = inv(g_s_ij); g = g_u_ij;

L = E*nu/((1+nu)*(1-2*nu)); G = E/(2*(1+nu)); %L = 2*L*G/(L+2*G);

HF = zeros(3,3,3,3);
for l = 1:3
    for m = 1:3
        for a = 1:3
            for b = 1:3
                HF(l,m,a,b) = L*g(l,m)*g(a,b) + G*(g(l,a)*g(m,b)+g(l,b)*g(m,a));
            end
        end
    end
end

H2 = zeros(6);
ie = [1 1; 2 2; 1 2; 1 3; 2 3; 3 3];
for i = 1:6
    for j = 1:6
        H2(i,j) = HF(ie(i,1),ie(i,2),ie(j,1),ie(j,2));
    end
end
H = H2(1:5,1:5);

N = @(u) [eye(3) u*[0 n(3) -n(2); -n(3) 0 n(1); n(2) -n(1) 0]];%-tild(u*n)
O = zeros(1,6);
D = @(u) [fu0_1'*N(u) O; O fu0_2'*N(u); fu0_2'*N(u) fu0_1'*N(u); n'*N(u) O; O n'*N(u)];

K = integration1D(@(u) D(u)'*H*D(u),2,-e/2,e/2);
end

% function out = computeTSE3inv(h)
% hH = hatSE3(h);
% out = eye(6) + 0.5*hH + 1/12*hH^2;
% end

% function h = logSE3(H)
% h = zeros(6,1);
% L = (H-eye(4)); L = L -0.5*L^2;
% h(1:3) = L(1:3,4); h(4:6) = itild(L(1:3,1:3));
% end

% function out = computeDTSE3invC(h,v)
%
% hH = hatSE3(h); vH = hatSE3(v);
% out = -0.5*vH - 1/12*(hatSE3(hH*v)+hH*vH);
%
% end
