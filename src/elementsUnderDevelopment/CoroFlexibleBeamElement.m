%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef CoroFlexibleBeamElement < Element
    %FLEXIBLEBEAMELEMENT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        O0;
        MCS;
        K; KCS;
        P;
        h; h0; L;
        alpha=0; beta=0;
        H0A; H0B; W; Z; M1; M2; KT;
    end
    
    methods
        function obj = CoroFlexibleBeamElement(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,listNodes,~)
            obj.initListNodalDof.nodeSE3 = [listNodes(1)+(0.01:0.01:0.06) listNodes(2)+(0.01:0.01:0.06)];
        end
        
        function initializeStatic(obj,nstep)
            obj.h0 = zeros(6,1);
            obj.h0(1:3) = obj.listNodes{2}.position(:,1) - obj.listNodes{1}.position(:,1);
            obj.L = norm(obj.h0(1:3));
            
            t = obj.h0(1:3)/obj.L; [n,b] = vectHooke(t);
            obj.O0 = [t n b];
            O02 = [obj.O0 zeros(3); zeros(3) obj.O0];
            obj.KCS = O02*obj.KCS*O02';
            
            T = computeTSE3inv(obj.h0); obj.P = [hatSE3(obj.h0)-T T];
            obj.K = obj.P'*obj.KCS*obj.P/obj.L;
            
            H0 = eye(4); H0(1:3,4) = (obj.listNodes{2}.position(:,1)+obj.listNodes{1}.position(:,1))/2;
            HA = eye(4); HA(1:3,4) = obj.listNodes{1}.position(:,1);
            HB = eye(4); HB(1:3,4) = obj.listNodes{2}.position(:,1);
            obj.H0A = H0\HA; obj.H0B = H0\HB;
            
            Y0 = [-AdjSE3(obj.H0A^-1); -AdjSE3(obj.H0B^-1)];
            A = Y0';%[eye(6) eye(6)];
            
            obj.Z = -(A*Y0)\A;
            obj.W = eye(12) + Y0*obj.Z;
            
            obj.KT = obj.W'*obj.K*obj.W;
            
            obj.energy = zeros(1,nstep);
        end
        
        function obj = initializeDynamic(obj,nstep)
            obj.initializeStatic(nstep);
            obj.MCS = [obj.O0 zeros(3); zeros(3) obj.O0]*obj.MCS*[obj.O0' zeros(3); zeros(3) obj.O0'];
            M = integration1D(@(s) fcnQ(s,obj.h0)'*obj.MCS*fcnQ(s,obj.h0),5,0,1)*obj.L; M = (M+M')/2;
            
            Y0 = [-AdjSE3(obj.H0A^-1); -AdjSE3(obj.H0B^-1)];
            X = [eye(12) -Y0];
            M0 = X'*M*X;
            obj.M1 = obj.W'*M0(1:12,1:12)*obj.W+obj.Z'*M0(13:18,1:12)*obj.W+obj.W'*M0(1:12,13:18)*obj.Z+obj.Z'*M0(13:18,13:18)*obj.Z;
            obj.M2 = M0(13:18,1:12)*obj.W+M0(13:18,13:18)*obj.Z;
        end
        
        function resModel = assembleResidueStatic(obj,resModel,parameters)
            RA = vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep));
            xA = obj.listNodes{1}.position(:,parameters.timestep);
            HA = [RA xA; 0 0 0 1];
            RB = vect9ToMat3(obj.listNodes{2}.R(:,parameters.timestep));
            xB = obj.listNodes{2}.position(:,parameters.timestep);
            HB = [RB xB; 0 0 0 1];
            
            H0i.H0A = obj.H0A; H0i.H0B = obj.H0B;
            
            [y] = solveH0(HA,HB,H0i);
            
            resModel = resModel + obj.W'*obj.K*y;
            obj.refValForce = norm(obj.W'*obj.K*y);
        end
        
        %         function ComputeResidueS(obj,parameters)
        %             RAT = vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep))';
        %             xA = obj.listNodes{1}.position(:,parameters.timestep);
        %             RB = vect9ToMat3(obj.listNodes{2}.R(:,parameters.timestep));
        %             xB = obj.listNodes{2}.position(:,parameters.timestep);
        %
        %             obj.h = logSE3([RAT*RB RAT*(xB-xA); 0 0 0 1]);
        %
        %             T = computeTSE3inv(obj.h); obj.P = [hatSE3(obj.h)-T T];
        %             obj.res = obj.P'*obj.KCS*((obj.h-obj.h0)/obj.L);
        %         end
        
        function resModel = assembleResidueDynamic(obj,resModel,parameters)
            resModel = assembleResidueStatic(obj,resModel,parameters);
            
            vdot_AB = [obj.listNodes{1}.acceleration(:,parameters.timestep);obj.listNodes{2}.acceleration(:,parameters.timestep)];
            v_AB = [obj.listNodes{1}.velocity(:,parameters.timestep);obj.listNodes{2}.velocity(:,parameters.timestep)];
            
            resModel = resModel + obj.M1*vdot_AB - obj.Z'*hatSE3(obj.Z*v_AB)'*obj.M2*v_AB;
            obj.refValForce = obj.refValForce + norm(obj.M1*vdot_AB - obj.Z'*hatSE3(obj.Z*v_AB)'*obj.M2*v_AB);
        end
        
        %         function ComputeResidueD(obj,parameters)
        %             obj.ComputeResidueS(obj,parameters);
        %
        %             obj.M = integration1D(@(s) fcnQ(s,obj.h)'*obj.MCS*fcnQ(s,obj.h),5,0,1)*obj.L;
        %             obj.res = obj.res + obj.M*[obj.listNodes{1}.acceleration(:,parameters.timestep);obj.listNodes{2}.acceleration(:,parameters.timestep)];
        %
        %             v_AB = [obj.listNodes{1}.velocity(:,parameters.timestep);obj.listNodes{2}.velocity(:,parameters.timestep)];
        %             Gyro = integration1D(@(s) fcnQ(s,obj.h)'*(obj.MCS*fcnQdot(s,obj.h,obj.P*v_AB) - hatSE3(fcnQ(s,obj.h))'*obj.MCS*fcnQ(s,obj.h)),4,0,1)*obj.L;
        %             obj.res = obj.res + Gyro*v_AB;
        %         end
        
        function StModel = assembleTangentOperatorStatic(obj,StModel,parameters)
            StModel = StModel + parameters.coefK*obj.KT;
        end
        
        %         function ComputeTangentOperatorS(obj,parameters)
        %             obj.St = parameters.coefK*(obj.P'*obj.KCS*obj.P)/obj.L;
        %         end
        
        function StModel = assembleTangentOperatorDynamic(obj,StModel,parameters)
            StModel = assembleTangentOperatorStatic(obj,StModel,parameters);
            
            v_AB = [obj.listNodes{1}.velocity(:,parameters.timestep);obj.listNodes{2}.velocity(:,parameters.timestep)];
            CT = -obj.Z'*(hatSE3(obj.Z*v_AB)'*obj.M2 + CheckSE3(obj.M2*v_AB)'*obj.Z);
            StModel = StModel + parameters.gammap*CT + parameters.betap*obj.M1;
        end
        
        %         function ComputeTangentOperatorD(obj,parameters)
        %             obj.ComputeTangentOperatorS(obj,parameters);
        %             obj.St = obj.St + parameters.betap*obj.M;
        %         end
        
        function visu(obj,step)
            RA = vect9ToMat3(obj.listNodes{1}.R(:,step));
            RB = vect9ToMat3(obj.listNodes{2}.R(:,step));
            xA = obj.listNodes{1}.position(:,step);
            xB = obj.listNodes{2}.position(:,step);
            HA  = [RA xA; 0 0 0 1]; HB = [RB xB; 0 0 0 1];
            
            plot3(xA(1),xA(2),xA(3),'r*')
            hold on
            plot3(xB(1),xB(2),xB(3),'r*')
            np = 0;
            hi = logSE3(HA\HB);
            xp = zeros(3,np+2); xp(:,1) = HA(1:3,4);
            for j = 1:np+1
                jj = j/(np+1);
                Hp = expSE3(jj*hi,HA);
                xp(:,j+1) = Hp(1:3,4);
            end
            %             xp = NeglLowTerms(xp);
            plot3(xp(1,:),xp(2,:),xp(3,:),'r','LineWidth',2)
        end
    end
end

function [y,H0] = solveH0(HA,HB,EP)

H0 = HA;
A = [eye(6) eye(6)];
yA = logSE3(EP.H0A^-1*H0^-1*HA);
yB = logSE3(EP.H0B^-1*H0^-1*HB);
y = [yA;yB];

res = A*y;
nit = 0;
while and(norm(res) > 1e-6, nit < 10)
    nit = nit+1;
    
    St = (-computeTSE3inv(-yA)*AdjSE3(EP.H0A^-1)-computeTSE3inv(-yB)*AdjSE3(EP.H0B^-1));
    
    dh0 = -St\res;
    H0 = H0*expSE3(dh0);
    
    yA = logSE3(EP.H0A^-1*H0^-1*HA);
    yB = logSE3(EP.H0B^-1*H0^-1*HB);
    y = [yA;yB];
    
    res = A*y;
end
if nit == 10
    disp('oups')
end

end
