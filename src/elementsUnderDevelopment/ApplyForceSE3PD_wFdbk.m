%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef ApplyForcePD_wFdbk < Element
    % APPLYFORCE 
    % Applies an external force on a joint element
    % The force vector f is applied on the only axis of the joint element
    % in "elements". (used to apply joint torques for example)
    % In this third version, a PD feedback control on the position of the
    % joint (existing in version 2 already) and an additionnal contribution
    % on the actual force being applied to a flexible element (measured using
    % the curvature between TWO nodes) can be added. The flexible element of interest must be
    % defined by providing the two nodes defining it.
    
    
    properties
        f;
        timeref = 0;
        indextime = 1;
        elements;
        ref; % Reference trajectory that should be used (contribution will be added to the f vector/value)
        dref; % Reference velocity of the joint (derivative of dref)
        kp = 100; % proportional gain
        kd = 5; % derivative gain
        cascaded = 1; % say if controler is cascaded (1) or not (0)
        
        kMeas = 0; % proportional gain on the measuring data
        select; % Select the internal force to be measured (1 x 6 vector of 1 or 0, ex: [0 0 0 0 1 0] for torque in y axis (only one obviously).
        % needed for flexible element properties
        h0;
        L;
        yAxis;
        O0;
        KCS;
        P; % used for the derivative of strains
    end
    
    methods
        function obj = ApplyForcePD_wFdbk(input)
            obj = obj@Element(input);
        end
        
%         function defineDof(obj,listNodes,listElements)
        function defineDof(obj,~,listElements)
%             obj.initListNodalDof.nodeSE3 = zeros(1,6);
%             obj.initListNodalDof.nodeSE3 = [listNodes(1)+(0.01:0.01:0.06) listNodes(2)+(0.01:0.01:0.06)];
            
            obj.initListElementDof.lagrangeMultiplierSet = listElements+(0.01:0.01:0.06);
            obj.initListElementVar.lagrangeMultiplierSet = listElements;
            
            obj.initListElementDof.relativeNodeSE3 = listElements+0.01;
            obj.initListElementVar.relativeNodeSE3 = listElements;
        end
        
        function resModel = assembleResidueStatic(obj,resModel,parameters)
            
            if obj.timeref ~= parameters.time
                obj.indextime = obj.indextime+1;
                obj.timeref = parameters.time;
            end
            resModel(1) = resModel(1) - obj.f(obj.indextime);
%             resModel(end) = resModel(end) - obj.f(obj.indextime);
            if obj.kMeas ~= 0                
%                 obj.h0 = zeros(6,1);
%                 obj.h0(1:3) = obj.listNodes{2}.position(:,1) - obj.listNodes{1}.position(:,1);
%                 obj.L = norm(obj.h0(1:3));
% 
%                 t = obj.h0(1:3)/obj.L;
%                 if isempty(obj.yAxis)
%                     [n,b] = vectHooke(t);
%                 else
%                     [n,b] = vectHooke(t,obj.yAxis);
%                 end 
%                 obj.O0 = [t n b];
%                 O02 = [obj.O0 zeros(3); zeros(3) obj.O0];
%                 obj.KCS = O02*obj.KCS*O02';
%                 xA = obj.listNodes{1}.position(:,obj.indextime);
%                 RA = vect9ToMat3(obj.listNodes{1}.R(:,obj.indextime));
% %                 HA = [RA xA; 0 0 0 1];
%                 HAm1 = [RA' -RA'*xA; 0 0 0 1];
%                 
%                 xB = obj.listNodes{2}.position(:,obj.indextime);
%                 RB = vect9ToMat3(obj.listNodes{2}.R(:,obj.indextime));
%                 HB = [RB xB; 0 0 0 1];
% %                 HBm1 = [RB' -RB'*xB; 0 0 0 1];
% 
% %                 hBeam = vectSE3(HAm1*HB);
%                 hBeam = logSE3(HAm1*HB);
%                 
%                 T = computeTSE3inv(hBeam); 
%                 obj.P = [hatSE3(hBeam)-T T];
%                 
%                 epsilon = (hBeam-obj.h0)/obj.L;
% %                 measForce = [obj.select 0 0 0 0 0 0]*obj.P'*obj.KCS*epsilon; % force in element
%                 measForce = obj.select*obj.KCS*epsilon; % force in element
%                 errF = (obj.f(obj.indextime) - measForce); % compute error
%                 resModel(end) = resModel(end) - obj.kMeas*errF; % add correction
                measForce = obj.select*obj.listElementVariables.lagrangeMultiplierSet{1}.value(:,obj.indextime);
                errF = (obj.f(obj.indextime) - measForce);
                resModel(1) = resModel(1) - obj.kMeas*errF; % add correction
            end
            if obj.cascaded
                if ~isempty(obj.dref) && ~isempty(obj.ref)
    %                 err = -(obj.listElementVariables.relativeNodeSE3{1}.relCoo(obj.indextime) - obj.ref(obj.indextime));
    %                 derr = -(obj.listElementVariables.relativeNodeSE3{1}.velocity(obj.indextime) - (obj.dref(obj.indextime)+obj.kp*err));
    %                 resModel(end) = resModel(end) - obj.kd*derr;
                    err = -(obj.listElementVariables.relativeNodeSE3{1}.relCoo(obj.indextime) - obj.ref(obj.indextime));
                    derr = -(obj.listElementVariables.relativeNodeSE3{1}.velocity(obj.indextime) - (obj.dref(obj.indextime)+obj.kp*err));
                    resModel(1) = resModel(1) - obj.kd*derr;
                end
            else
                if ~isempty(obj.ref)
    %                 err = -(obj.listElementVariables.relativeNodeSE3{1}.relCoo(obj.indextime) - obj.ref(obj.indextime));
    %                 resModel(end) = resModel(end) - obj.kp*err;
                    err = -(obj.listElementVariables.relativeNodeSE3{1}.relCoo(obj.indextime) - obj.ref(obj.indextime));
                    resModel(1) = resModel(1) - obj.kp*err;
                end
                if ~isempty(obj.dref)
    %                 derr = -(obj.listElementVariables.relativeNodeSE3{1}.velocity(obj.indextime) - obj.dref(obj.indextime));
    %                 resModel(end) = resModel(end) - obj.kd*derr;
                    derr = -(obj.listElementVariables.relativeNodeSE3{1}.velocity(obj.indextime) - obj.dref(obj.indextime));
                    resModel(1) = resModel(1) - obj.kd*derr;
                end
            end
            
        end
        
        function StModel = assembleTangentOperatorStatic(obj,StModel,parameters)
%             StModel(end,end) = StModel(end,end) + parameters.coefK*(obj.kp) + parameters.gammap*(obj.kd);
%             if obj.kMeas ~= 0 
% %                 StModel(end,1:12) = StModel(end,1:12) + parameters.coefK*obj.kMeas*[obj.select 0 0 0 0 0 0]*obj.P'*obj.KCS*obj.P/obj.L ;
%                 StModel(end,1:12) = StModel(end,1:12) + parameters.coefK*obj.select*obj.kMeas*obj.KCS*obj.P/obj.L ;
%             end
            if obj.cascaded
                StModel(1,1) = StModel(1,1) + parameters.coefK*(obj.kd*obj.kp) + parameters.gammap*(obj.kd);
            else                
                StModel(1,1) = StModel(1,1) + parameters.coefK*(obj.kp) + parameters.gammap*(obj.kd);
            end
            if obj.kMeas ~= 0
                StModel(1,2:7) = StModel(1,2:7) + parameters.scaling*obj.select*(obj.kMeas);
            end
        end
    end
    
end

