%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% I = integration2D(F,nGP,a,b,c,d)
%
% 2D Numerical integration of F, whose output can be of any dimension (scalar,
% vector, matrix)
%
% INPUTS:
%       -F: function handle
%       -nGP: number of Gauss point (from 1 to 5, default: 2)
%       -a: lower bound of the first variable (default: -1)
%       -b: upper bound of the first variable (default: 1)
%       -c: lower bound of the second variable (default: -1)
%       -d: upper bound of the second variable (default: 1)
%
% OUTPUT:
%       -I = 2D numerical integration of F  
%
% Valentin Sonneville (2012)
%

function I = integration2D(F,nGP,a,b,c,d)

GP = cell(5,1);
GP{1} = [0 2];
GP{2} = [-1/sqrt(3) 1; 1/sqrt(3) 1];
GP{3} = [0 8/9; -sqrt(3/5) 5/9; sqrt(3/5) 5/9];
GP{4} = [sqrt((3-2*sqrt(6/5))/7) (18+sqrt(30))/36;
    -sqrt((3-2*sqrt(6/5))/7) (18+sqrt(30))/36;
    sqrt((3+2*sqrt(6/5))/7) (18-sqrt(30))/36;
    -sqrt((3+2*sqrt(6/5))/7) (18-sqrt(30))/36];
GP{5} = [0 128/225;
    1/3*sqrt(5-2*sqrt(10/7)) (322+13*sqrt(70))/900;
    -1/3*sqrt(5-2*sqrt(10/7)) (322+13*sqrt(70))/900;
    1/3*sqrt(5+2*sqrt(10/7)) (322-13*sqrt(70))/900;
    -1/3*sqrt(5+2*sqrt(10/7)) (322-13*sqrt(70))/900];
GP{6} = [0.6612093864662645 0.3607615730481386;
    -0.6612093864662645 0.3607615730481386;
    -0.2386191860831969 0.4679139345726910;
    0.2386191860831969 0.4679139345726910;
    -0.9324695142031521 0.1713244923791704;
    0.9324695142031521 0.1713244923791704];
GP{10} = [-0.1488743389816312 0.2955242247147529;
    0.1488743389816312 0.2955242247147529;
    -0.4333953941292472 0.2692667193099963;
    0.4333953941292472 0.2692667193099963;
    -0.6794095682990244 0.2190863625159820;
    0.6794095682990244 0.2190863625159820;
    -0.8650633666889845 0.1494513491505806;
    0.8650633666889845 0.1494513491505806
    -0.9739065285171717 0.0666713443086881;
    0.9739065285171717 0.0666713443086881];

if nargin < 4
    a = -1;
    b = 1;
    c = -1;
    d = 1;
    if nargin < 2
        nGP = 2;
    end
end
fpx = (b+a)/2;
fmx = (b-a)/2;
fpy = (d+c)/2;
fmy = (d-c)/2;

I = 0;
for i = 1:nGP
    for j = 1:nGP
        I = I + GP{nGP}(i,2)*GP{nGP}(j,2)*F(fmx*GP{nGP}(i,1)+fpx,fmy*GP{nGP}(j,1)+fpy);
    end
end

I = fmx*fmy*I;

end