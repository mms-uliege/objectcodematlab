%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef TrajectoryConstraintWithRotation < Element
    %TRAJECTORYCONSTRAINTWITHROTATION Summary of this class goes here
    %   Element constraining the trajectory (6DOF) of a node, provided some
    %   kinematic elements that would serve as actuators. It is supposed to
    %   work with constraints on position AND rotation level. TO BE TESTED
    %   seems to work up to now...
    
    properties
        Axe; % DOFs to be driven ([1 0 0 0 0 0; 0 1 0 0 0 0] means DOFs "x" and "y")
        nDof;
        dServodq;
%         k = 0;
        alpha0 = 0;
        nCom;
        elements;
        T;
        A;
        active = 1;
    end
    
    methods
        function obj = TrajectoryConstraintWithRotation(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,listNodes,listElements)
            obj.nDof = size(obj.Axe,1); % initialize the number of degree of freedom to be commanded
            obj.initListNodalDof.nodeSE3 = zeros(1,6);
            obj.initListNodalDof.nodeSE3(1:6) = listNodes(1)+(0.01:0.01:0.06);
            
            obj.nCom = length(obj.elements);
            for i = 1:obj.nCom
                obj.initListElementDof.relativeNodeSE3(i) = listElements(i)+0.01;
                obj.initListElementVar.relativeNodeSE3(i) = listElements(i);
            end
            obj.initListElementDof.commandSet = obj.elementNumber+(0.01:0.01:(0.01*obj.nCom)); % Create command objects to control the constrained dof
            obj.initListElementVar.commandSet = obj.elementNumber;
            
        end
        
        function initializeStatic(obj,~)
            if size(obj.Axe,2) == 3
                obj.Axe = [obj.Axe zeros(obj.nDof,3)];
            end
            obj.dServodq = obj.Axe; % Initialize the matrix corresponding to servo constraints to be put in the tangent matrix        
        end
        
        function resModel = assembleResidueStatic(obj,resModel,parameters)
            if obj.active == 1
                RA = vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep));
                xA = obj.listNodes{1}.position(:,parameters.timestep);
%                 HA = [RA xA; 0 0 0 1];
%                 hA = logSE3([RA xA; 0 0 0 1]);
                hd = obj.Axe'*obj.T(:,parameters.timestep);
                Hd = expSE3(hd);
                Rd = expSO3(hd(4:6));

                obj.dServodq(1:obj.nDof,1:6) = obj.Axe*[RA zeros(3); zeros(3) eye(3)];
%                 obj.dServodq(1:obj.nDof,1:6) = obj.Axe*[Rd zeros(3); zeros(3) eye(3)];
%                 obj.dServodq(1:obj.nDof,1:6) = obj.Axe*[eye(3) zeros(3); zeros(3) eye(3)];
                
%                 trajConstraint = obj.Axe*vectSE3([Rd' -Rd'*hd(1:3)]*[RA xA; 0 0 0 1]);
                trajConstraint = obj.Axe*[(xA-hd(1:3)); logSO3(Rd'*RA)];

                resModel(end-(obj.nDof-1):end) = resModel(end-(obj.nDof-1):end) + parameters.scaling*(trajConstraint);  % "Constraint" level
                resModel(6+(1:obj.nCom)) = resModel(6+(1:obj.nCom)) - obj.listElementVariables.commandSet{1}.value(:,parameters.timestep);
            end
        end
        
        
        function StModel = assembleTangentOperatorStatic(obj,StModel,parameters)
            if obj.active == 1
                StModel(end-(obj.nDof-1):end,1:6) = StModel(end-(obj.nDof-1):end,1:6) + parameters.scaling*obj.dServodq;
%                 StModel(end-(obj.nDof-1):end,1:6) = StModel(end-(obj.nDof-1):end,1:6) + obj.dServodq;
            end
            StModel(6+(1:obj.nCom),6+obj.nCom+(1:obj.nCom)) = StModel(6+(1:obj.nCom),6+obj.nCom+(1:obj.nCom)) - eye(obj.nCom);
%             StModel(6+(1:obj.nCom),6+obj.nCom+(1:obj.nCom)) = StModel(6+(1:obj.nCom),6+obj.nCom+(1:obj.nCom)) - parameters.scaling*eye(obj.nCom);

        end

        
    end
    
end

