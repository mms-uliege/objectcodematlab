%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [varargout] = gaussPointsTriangle(n)
% Gives  the coordinates xi and eta and their weights of the n Gauss
% points. Used for triangular integration. Only valid for n = 1, 3, 4, 5, 
% 6, 7, 12 and 13
% In Cartesian coordinates, change of variable:
%
%   x = alpha1 + alpha2 . xi + alpha3 . eta
%   y = beta1  +  beta2 . xi +  beta3 . eta
%
% with
%
%   alpha1 = x1
%   alpha2 = x2 - x1
%   alpha3 = x3 - x1
% and the same for y

nn=[1 3 4 6 7 12 13];

varargout=[];

if ~nargin
    figure('color','w');
    m=sqrt(length(nn));
    m=ceil(m);
    k=0;
    cmap=b2r256;
    set(gcf,'colormap',cmap)
    nbc=size(cmap,1);
    for i=1:m
        for j=1:m
            k=k+1;
            n=nn(k);
            subplot(m,m,k)
            hold on
            [xi,eta,w]=GaussPointsTriangle(n);
            wmin=min(w);
            wmax=max(w);
            patch([0 1 0],[0 0 1],-[1 1 1],[0 0 0],'facecolor','w','linesmoothing','on')
            for l=1:length(w)
                indc=1+(w(l)-wmin)/(eps+wmax-wmin)*(nbc-1);
                indc=round(indc);
                colo=cmap(indc,:);
                plot3(xi(l),eta(l),w(l),'ob','markersize',6,'markerfacecolor',colo,'markeredgecolor',colo, ...
                    'linesmoothing','on')
            end
            axis equal
            axis off
            if k == length(nn)
                return
            end
        end
    end
    return
end

n=round(n);
n=min(n,max(nn));
ind=find(nn-n >= 0);
ind=ind(1);
n=nn(ind);

switch n
    case 1
        xi=1/3;
        eta=1/3;
        w=1;
    case 3
        w=1/3*ones(1,3);
        xi=[1/6 2/3 1/6];
        eta=[1/6 1/6 2/3];
    case 4
        w=[-5/8 13/24*ones(1,3)];
        xi=[1/3 0.2 0.6 0.2];
        eta=[1/3 0.2 0.2 0.6];
    case 6
        w12=0.109951743655322;
        w34=0.223381589678011;
        w=[w12*ones(1,3) w34*ones(1,3)];
        v1=0.091576213509771;
        v2=0.816847572980459;
        v3=0.445948490915965;
        v4=0.108103018168070;
        xi= [v1 v2 v1 v3 v4 v3];
        eta=[v1 v1 v2 v3 v3 v4];
    case 7
        w12=0.125939180544827;
        w34=0.132394152788506;
        w=[9/40 w12*ones(1,3) w34*ones(1,3)];
        v1=0.101286507323456;
        v2=0.797426985353087;
        v3=0.470142064105115;
        v4=0.059715871789770;
        xi= [1/3 v1 v2 v1 v3 v4 v3];
        eta=[1/3 v1 v1 v2 v3 v3 v4];
    case 12
        w12= 0.050844906370207;
        w34= 0.116786275726379;
        w567=0.082851075618374;
        w=[w12*ones(1,3) w34*ones(1,3) w567*ones(1,6)];
        v1=0.063089014491502;
        v2=0.873821971016996;
        v3=0.249286745170910;
        v4=0.501426509658179;
        v5=0.053145049844816;
        v6=0.310352451033785;
        v7=0.636502499121399;
        xi= [v1 v2 v1 v3 v4 v3 v5 v5 v6 v6 v7 v7];
        eta=[v1 v1 v2 v3 v3 v4 v6 v7 v5 v7 v5 v6];
    case 13
        w12= 0.175615257433204;
        w34= 0.053347235608839;
        w567=0.077113760890257;
        w=[-0.149570044467670 w12*ones(1,3) w34*ones(1,3) w567*ones(1,6)];
        v1=0.260345966079038;
        v2=0.479308067841923;
        v3=0.065130102902216;
        v4=0.869739794195568;
        v5=0.048690315425316;
        v6=0.312865496004875;
        v7=0.638444188569809;
        xi= [1/3 v1 v2 v1 v3 v4 v3 v5 v5 v6 v6 v7 v7];
        eta=[1/3 v1 v1 v2 v3 v3 v4 v6 v7 v5 v7 v5 v6];
end

varargout{1}=xi';
varargout{2}=eta';
varargout{3}=w';

return

%------------------------------------------------------------------------------
function [cmap] = b2r256
%
% Blue to red colormap with 256 colors.
%
%  Luc Masset (2009)

cg=(0:2:126)';
n=length(cg);
cmap=[zeros(n,1) cg 255*ones(n,1)];

cg=(129: 4:253)';
cb=(253:-6: 67)';
n=length(cg);
cmap=[cmap;zeros(n,1) cg cb];

cr=(  2: 4:126)';
cb=( 62:-2:  0)';
n=length(cr);
cmap=[cmap;cr 255*ones(n,1) cb];

cr=(130: 4:254)';
n=length(cr);
cmap=[cmap;cr 255*ones(n,1) zeros(n,1)];

cg=(254:-2:128)';
n=length(cg);
cmap=[cmap;255*ones(n,1) cg zeros(n,1)];

cg=(124:-4:  0)';
n=length(cg);
cmap=[cmap;255*ones(n,1) cg zeros(n,1)];

cmap=cmap/255;

return
