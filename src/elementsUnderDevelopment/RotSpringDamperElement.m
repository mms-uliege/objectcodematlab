%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef RotSpringDamperElement < Element
    %DAMPER1DELEMENT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        damping = 0;
        stiffness = 0;
        coulomb = 0; % coulomb friction component (max friction force) assuming that it is positive constant for velocity>0 Arthur Lismonde 11/12/2017
        coulombParam = 100; % parameter of the tanh(coulombParam*velocity) model giving the friction Arthur Lismonde 11/12/2017
        natural_angle;
        A;
    end
    
    methods
        function obj = RotSpringDamperElement(input)
            obj = obj@Element(input);
        end
        function defineDof(obj,listNodes,~)
            choice = [0.04 0.05 0.06]';
            obj.initListNodalDof.nodeSE3 = [listNodes(1)+obj.A*choice listNodes(2)+obj.A*choice];
        end

        function initializeStatic(obj,~)
            if isempty(obj.natural_angle)
                Ang1 = logSO3(vect9ToMat3(obj.listNodes{1}.R(:,1)));
                Ang2 = logSO3(vect9ToMat3(obj.listNodes{2}.R(:,1)));
                obj.natural_angle = obj.A*(Ang2-Ang1);
%                 Ang1 = norm(logSO3(vect9ToMat3(obj.listNodes{1}.R(:,1))));
%                 Ang2 = norm(logSO3(vect9ToMat3(obj.listNodes{2}.R(:,1))));
%                 obj.natural_angle = (Ang2-Ang1);
            end
        end
        
        function resModel = assembleResidueStatic(obj,resModel,parameters)
            Ang1 = logSO3(vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep)));
            Ang2 = logSO3(vect9ToMat3(obj.listNodes{2}.R(:,parameters.timestep)));
            ang = obj.A*(Ang2-Ang1);
%             Ang1 = norm(logSO3(vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep))));
%             Ang2 = norm(logSO3(vect9ToMat3(obj.listNodes{2}.R(:,parameters.timestep))));
%             ang = (Ang2-Ang1);
            resModel = resModel + obj.stiffness*(ang - obj.natural_angle)*[-1;1];
            
            AngVel1 = obj.listNodes{1}.velocity(4:6,parameters.timestep);
            AngVel2 = obj.listNodes{2}.velocity(4:6,parameters.timestep);
            v = obj.A*(AngVel2-AngVel1);
%             AngVel1 = norm(obj.listNodes{1}.velocity(4:6,parameters.timestep));
%             AngVel2 = norm(obj.listNodes{2}.velocity(4:6,parameters.timestep));
%             v = (AngVel2-AngVel1);
            resModel = resModel + obj.damping*v*[-1;1];
            resModel = resModel + obj.coulomb*tanh(obj.coulombParam*v)*[-1;1];
            
            
        end
                
        
        function StModel = assembleTangentOperatorStatic(obj,StModel,parameters)
            StModel = StModel + parameters.coefK*obj.stiffness*[1 -1; -1 1];

            AngVel1 = obj.listNodes{1}.velocity(4:6,parameters.timestep);
            AngVel2 = obj.listNodes{2}.velocity(4:6,parameters.timestep);
            v = obj.A*(AngVel2-AngVel1);
            StModel = StModel + parameters.gammap*obj.damping*[1 -1; -1 1];
            StModel = StModel + parameters.gammap*obj.coulomb*(1-tanh(obj.coulombParam*v))*[1 -1; -1 1];
            
        end
        
        
%         function visu(obj,step)
%             xA = [obj.listNodes{1}.position(step) 0 0]';
%             xB = [obj.listNodes{2}.position(step) 0 0]';
%             plot3([xA(1) xB(1)],[xA(2) xB(2)],[xA(3) xB(3)],'-og')
%         end
        
    end
    
end

