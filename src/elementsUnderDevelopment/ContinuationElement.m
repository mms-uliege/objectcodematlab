%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef ContinuationElement < Element
    %EXTERNALFORCE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        amplitude = 1;
        frequency = 1;
        DOF;
        R;
        F; dF;
    end
    
    methods
        function obj = ContinuationElement(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,listNodes,~)
            obj.initListNodalDof.nodeSE3 = [listNodes(1)+(0.01:0.01:0.06)];
            
            obj.initListElementDof.continuationVariableSet = obj.elementNumber+0.01;
            obj.initListElementVar.continuationVariableSet = obj.elementNumber;
        end
        
        function initializeStatic(obj,nstep)
            obj.R = eye(3);
            obj.F = zeros(3,1); obj.dF = zeros(3,1);
            if obj.DOF <= 3
                obj.F(obj.DOF) = -obj.amplitude;
                obj.dF(obj.DOF) = -obj.amplitude;
            else
                obj.F(obj.DOF-3) = -obj.amplitude;
                obj.dF(obj.DOF-3) = -obj.amplitude;
            end
        end
        
        function resModel = assembleResidueStatic(obj,resModel,parameters)
            obj.R = vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep));
            mu = obj.listElementVariables.continuationVariableSet{1}.value(:,parameters.timestep);
            if obj.DOF <= 3
                %                 obj.R = eye(3);
                obj.F(obj.DOF) = -(1+mu)*obj.amplitude;
                resModel(1:3) = resModel(1:3) + obj.R'*obj.F;
                obj.refValForce = norm(obj.R'*obj.F);
            else
                obj.F(obj.DOF-3) = -(1+mu)*obj.amplitude;
                resModel(4:6) = resModel(4:6) + obj.F;
            end
        end
        
        function StModel = assembleTangentOperatorStatic(obj,StModel,parameters)
            if obj.DOF <= 3
                StModel(1:3,4:6) = StModel(1:3,4:6) + parameters.coefK*tild(obj.R'*obj.F);
                StModel(1:3,7) = StModel(1:3,7) + parameters.coefK*(obj.R'*obj.dF);
            else
                StModel(4:6,7) = StModel(4:6,7) - parameters.coefK*obj.dF;
            end
        end
        
    end
    
end

