%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef Volumic2DElement < Element
    %SPRING1DELEMENT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        N; FI; dFI;
        K; E = 0; nu = 0;
    end
    
    methods
        function obj = Volumic2DElement(input)
            obj = obj@Element(input);
        end
        function defineDof(obj,listNodes,~)
            obj.initListNodalDof.node2D = [];
            for i = 1:4
                obj.initListNodalDof.node2D = [obj.initListNodalDof.node2D listNodes(i)+(0.01:0.01:0.02)];
            end
        end
        
        function initializeStatic(obj,~)
            
            obj.FI = @(s,t) 0.25*[(1-s)*(1-t)*eye(2) (1+s)*(1-t)*eye(2) (1+s)*(1+t)*eye(2) (1-s)*(1+t)*eye(2)];
            
            obj.dFI{1} = @(s,t) 0.25*[(1-t)*(-1)*eye(2) (1-t)*(+1)*eye(2) (1+t)*(+1)*eye(2) (1+t)*(-1)*eye(2)];
            obj.dFI{2} = @(s,t) 0.25*[(-1)*(1-s)*eye(2) (-1)*(1+s)*eye(2) (+1)*(1+s)*eye(2) (+1)*(1-s)*eye(2)];
            
            obj.N = @(s,t) [[1 0]*obj.dFI{1}(s,t); [0 1]*obj.dFI{2}(s,t); 0.5*([0 1]*obj.dFI{1}(s,t) + [1 0]*obj.dFI{2}(s,t))];
            
            L = obj.E*obj.nu/((1+obj.nu)*(1-2*obj.nu)); G = obj.E/(2*(1+obj.nu));
            k = [2*G+L L 0; L 2*G+L 0; 0 0 G];
            obj.K = integration2D(@(s,t) obj.N(s,t)'*k*obj.N(s,t),3,-1,1,-1,1);
            
        end
        
        function resModel = assembleResidueStatic(obj,resModel,parameters)
            uA = obj.listNodes{1}.position(:,parameters.timestep)-obj.listNodes{1}.position(:,1);
            uB = obj.listNodes{2}.position(:,parameters.timestep)-obj.listNodes{2}.position(:,1);
            uC = obj.listNodes{3}.position(:,parameters.timestep)-obj.listNodes{3}.position(:,1);
            uD = obj.listNodes{4}.position(:,parameters.timestep)-obj.listNodes{4}.position(:,1);
            u = [uA;uB;uC;uD];
                        
            internalForces = obj.K*u;
            
            resModel = resModel + internalForces;
        end
        
        %         function ComputeResidueS(obj,parameters)
        %             l = obj.listNodes{2}.position(1,parameters.timestep)-obj.listNodes{1}.position(1,parameters.timestep);
        %             obj.res = obj.stiffness*(l - obj.natural_length)*[-1;1];
        %         end
        
        function StModel = assembleTangentOperatorStatic(obj,StModel,parameters)
            StModel = StModel + parameters.coefK*obj.K;
        end
        
        %         function ComputeTangentOperatorS(obj,parameters)
        %             obj.St = parameters.coefK*[obj.stiffness -obj.stiffness; -obj.stiffness obj.stiffness];
        %         end
        
        function visu(obj,step)
            xA = obj.listNodes{1}.position(:,step);
            xB = obj.listNodes{2}.position(:,step);
            xC = obj.listNodes{3}.position(:,step);
            xD = obj.listNodes{4}.position(:,step);
            plot([xA(1) xB(1)],[xA(2) xB(2)],'-og')
            hold on
            plot([xB(1) xC(1)],[xB(2) xC(2)],'-og')
            plot([xC(1) xD(1)],[xC(2) xD(2)],'-og')
            plot([xA(1) xD(1)],[xA(2) xD(2)],'-og')
        end
        
    end
    
end

