%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef CurvedFlexibleBeamElement < Element
    %FLEXIBLEBEAMELEMENT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        vA1; vA2; vA3; vB1; vB2; vB3;
        OA; OB; AdHA0m1; AdHB0m1;
        M; MCS; Gyro = zeros(12);
        K; KCS;
        P;
        h; h0; L;
        alpha=0; beta=0;
    end
    
    methods
        function obj = CurvedFlexibleBeamElement(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,listNodes,~)
            obj.initListNodalDof.nodeSE3 = [listNodes(1)+(0.01:0.01:0.06) listNodes(2)+(0.01:0.01:0.06)];
        end
        
        function initializeStatic(obj,nstep)
            if ~isempty(obj.vA1)
               obj.OA = [obj.vA1(:) obj.vA2(:) obj.vA3(:)];
            else
               obj.OA = eye(3);
            end
            obj.AdHA0m1 = [obj.OA' zeros(3); zeros(3) obj.OA'];
            if ~isempty(obj.vB1)
               obj.OB = [obj.vB1(:) obj.vB2(:) obj.vB3(:)];
            else
               obj.OB = eye(3);
            end
            obj.AdHB0m1 = [obj.OB' zeros(3); zeros(3) obj.OB'];
            
            obj.h0 = zeros(6,1);
            obj.h0(4:6) = logSO3(obj.OA'*obj.OB);
            obj.h0(1:3) = obj.OA'*TSO3inv(obj.h0(4:6))'*(obj.listNodes{2}.position(:,1) - obj.listNodes{1}.position(:,1));
            obj.L = norm(obj.h0(1:3));
            
%             t = obj.h0(1:3)/obj.L; [n,b] = vectHooke(t);
%             obj.O0 = [t n b];
%             O02 = [obj.O0 zeros(3); zeros(3) obj.O0];
%             obj.KCS = O02*obj.KCS*O02';
            
            T = computeTSE3inv(obj.h0); obj.P = [(hatSE3(obj.h0)-T)*obj.AdHA0m1 T*obj.AdHB0m1];
            obj.K = obj.P'*obj.KCS*obj.P/obj.L;
            
            obj.energy = zeros(1,nstep);
        end
        
        function obj = initializeDynamic(obj,nstep)
            obj.initializeStatic(nstep);
%             obj.MCS = [obj.O0 zeros(3); zeros(3) obj.O0]*obj.MCS*[obj.O0' zeros(3); zeros(3) obj.O0'];
            TT = @(s) s*computeTSE3(s*obj.h0)*computeTSE3inv(obj.h0);
            Q = @(s) [(eye(6)-TT(s))*obj.AdHA0m1 TT(s)*obj.AdHB0m1];
            obj.M = integration1D(@(s) Q(s)'*obj.MCS*Q(s),3,0,1)*obj.L;            
        end
        
        function resModel = assembleResidueStatic(obj,resModel,parameters)
            RAT = vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep))';
            xA = obj.listNodes{1}.position(:,parameters.timestep);
            RB = vect9ToMat3(obj.listNodes{2}.R(:,parameters.timestep));
            xB = obj.listNodes{2}.position(:,parameters.timestep);
            
            
            obj.h = logSE3([obj.OA'*RAT*RB*obj.OB obj.OA'*RAT*(xB-xA); 0 0 0 1]);
            
            T = computeTSE3inv(obj.h); obj.P = [(hatSE3(obj.h)-T)*obj.AdHA0m1 T*obj.AdHB0m1];
%             fint = obj.KCS*((obj.h-obj.h0)/obj.L);
            resModel = resModel + obj.P'*obj.KCS*((obj.h-obj.h0)/obj.L);
            obj.refValForce = norm(obj.P'*obj.KCS*((obj.h-obj.h0)/obj.L));
        end
        
%         function ComputeResidueS(obj,parameters)
%             RAT = vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep))';
%             xA = obj.listNodes{1}.position(:,parameters.timestep);
%             RB = vect9ToMat3(obj.listNodes{2}.R(:,parameters.timestep));
%             xB = obj.listNodes{2}.position(:,parameters.timestep);
%             
%             obj.h = logSE3([RAT*RB RAT*(xB-xA); 0 0 0 1]);
%             
%             T = computeTSE3inv(obj.h); obj.P = [hatSE3(obj.h)-T T];
%             obj.res = obj.P'*obj.KCS*((obj.h-obj.h0)/obj.L);
%         end

        function resModel = assembleResidueDynamic(obj,resModel,parameters)
            resModel = assembleResidueStatic(obj,resModel,parameters);
            
            TT = @(s) s*computeTSE3(s*obj.h)*computeTSE3inv(obj.h);
            Q = @(s) [(eye(6)-TT(s))*obj.AdHA0m1 TT(s)*obj.AdHB0m1];
            
            obj.M = integration1D(@(s) Q(s)'*obj.MCS*Q(s),3,0,1)*obj.L;
            resModel = resModel + obj.M*[obj.listNodes{1}.acceleration(:,parameters.timestep); obj.listNodes{2}.acceleration(:,parameters.timestep)];
            
            v_AB = [obj.listNodes{1}.velocity(:,parameters.timestep);obj.listNodes{2}.velocity(:,parameters.timestep)];
            obj.Gyro = integration1D(@(s) Q(s)'*(obj.MCS*fcnQdot(s,obj.h,obj.P*v_AB)*0 - hatSE3(Q(s)*v_AB)'*obj.MCS*Q(s)),4,0,1)*obj.L;
            resModel = resModel + obj.Gyro*v_AB;
%             obj.refValForce = obj.refValForce + norm(obj.M*[obj.listNodes{1}.acceleration(:,parameters.timestep);obj.listNodes{2}.acceleration(:,parameters.timestep)]);
        end
        
%         function ComputeResidueD(obj,parameters)
%             obj.ComputeResidueS(obj,parameters);
%             
%             obj.M = integration1D(@(s) fcnQ(s,obj.h)'*obj.MCS*fcnQ(s,obj.h),5,0,1)*obj.L;
%             obj.res = obj.res + obj.M*[obj.listNodes{1}.acceleration(:,parameters.timestep);obj.listNodes{2}.acceleration(:,parameters.timestep)];
%             
%             v_AB = [obj.listNodes{1}.velocity(:,parameters.timestep);obj.listNodes{2}.velocity(:,parameters.timestep)];
%             Gyro = integration1D(@(s) fcnQ(s,obj.h)'*(obj.MCS*fcnQdot(s,obj.h,obj.P*v_AB) - hatSE3(fcnQ(s,obj.h))'*obj.MCS*fcnQ(s,obj.h)),4,0,1)*obj.L;
%             obj.res = obj.res + Gyro*v_AB;
%         end

        function StModel = assembleTangentOperatorStatic(obj,StModel,parameters)
            StModel = StModel + parameters.coefK*(obj.P'*obj.KCS*obj.P)/obj.L;
        end
        
%         function ComputeTangentOperatorS(obj,parameters)
%             obj.St = parameters.coefK*(obj.P'*obj.KCS*obj.P)/obj.L;
%         end

        function StModel = assembleTangentOperatorDynamic(obj,StModel,parameters)
            StModel = assembleTangentOperatorStatic(obj,StModel,parameters);
            StModel = StModel + parameters.gammap*obj.Gyro + parameters.betap*obj.M;
        end
        
%         function ComputeTangentOperatorD(obj,parameters)
%             obj.ComputeTangentOperatorS(obj,parameters);
%             obj.St = obj.St + parameters.betap*obj.M;
%         end
        
        function visu(obj,step)
            RA = vect9ToMat3(obj.listNodes{1}.R(:,step));
            RB = vect9ToMat3(obj.listNodes{2}.R(:,step));
            xA = obj.listNodes{1}.position(:,step);
            xB = obj.listNodes{2}.position(:,step);
            HA  = [RA xA; 0 0 0 1]; HB = [RB xB; 0 0 0 1];
            HA0 = [obj.OA zeros(3,1); 0 0 0 1]; HB0 = [obj.OB zeros(3,1); 0 0 0 1];
            
            plot3(xA(1),xA(2),xA(3),'r*')
            hold on
            plot3(xB(1),xB(2),xB(3),'r*')
            np = 3;
            hi = logSE3(HA0\(HA\(HB*HB0)));
            xp = zeros(3,np+2); xp(:,1) = HA(1:3,4);
            for j = 1:np+1
                jj = j/(np+1);
                Hp = expSE3(jj*hi,HA*HA0);
                xp(:,j+1) = Hp(1:3,4);
            end
%             xp = NeglLowTerms(xp);
            plot3(xp(1,:),xp(2,:),xp(3,:),'r','LineWidth',2)
        end
    end
end


