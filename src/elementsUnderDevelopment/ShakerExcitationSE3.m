%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef ShakerExcitationSE3 < Element
    %EXTERNALFORCE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        f;
        timeref = 0;
        indextime = 1;
    end
    
    methods
        function obj = ShakerExcitationSE3(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,listNodes,~)
            obj.initListNodalDof.nodeSE3 = [listNodes(1)+(0.01:0.01:0.06)];
        end
        
        function resModel = assembleResidueStatic(obj,resModel,parameters)
            
            if obj.timeref ~= parameters.time
                obj.indextime = obj.indextime+1;
                obj.timeref = parameters.time;
            end
            
%             resModel(2) = resModel(2) + obj.f(obj.indextime)*10000;
              resModel(2) = resModel(2) - 500*sin(5*parameters.time);
        end
    end
    
end

