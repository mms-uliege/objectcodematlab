%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = computeDTSE3Uinv(h,v)
    h = h(:); v = v(:); 
    O = h(4:6); nO = norm(O); Ud = v(1:3); tUd = tild(Ud); 

    if nO > 1e-8
        U = h(1:3); tO = tild(O); tU = tild(U); 
        Od = v(4:6); tOd = tild(Od);
        s = 2*sin(nO/2)/nO; c = cos(nO/2); alpha = s*c; beta = s*s; gamma = c/s;
        OdTO = Od'*O; OTU = O'*U;

        out = 0.5*tUd + (1-gamma)/nO^2*(Par(tUd,tO)+Par(tU,tOd)) + OdTO/nO^4*(gamma*(gamma+1)+nO^2/4-2)*Par(tU,tO)...
            + (1/beta+gamma-2)/nO^4*((Od'*U+Ud'*O)*tO*tO + OTU*Par(tO,tOd))...
            + OdTO/nO^6*(-2*(alpha+beta)/beta^2 -nO^2/4-gamma*(gamma+3) + 8)*OTU*tO*tO;
    else
        tU = tild(h(1:3)); tOd = tild(v(4:6));
        out = 0.5*tUd + 1/12*Par(tU,tOd);
    end
end

function out = Par(tx,ty)
    out = tx*ty+ty*tx;
end