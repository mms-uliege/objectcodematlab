%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function T = computeDTSO3(h,v)

    O = h(:); U = v(:);
    nO = norm(O); tU = tild(U);

    % if nO > 1e-8
    if nO > 1e-12 %TODESC0
        tO = tild(O);
        s = 2*sin(nO/2)/nO; c = cos(nO/2);
        alpha = s*c; %sin(nO)/nO;
        beta = s*s; %2*(1-cos(nO))/nO^2;
        T = -0.5*beta*tU + (1-alpha)/(nO^2)*(tU*tO+tO*tU) - (alpha-beta)/(nO^2)*(U'*O)*tO...
            +(U'*O)/nO^2*(0.5*beta-3*(1-alpha)/nO^2)*tO*tO;
    else
        T = -0.5*tU;
    end
end