%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = computeDTSE3invCT(h,v) 
    %eq. A.21

    a=h(1:3); b=h(4:6); nb = norm(b); nb2 = nb^2; c=v(1:3);

    alpha = sin(nb)/nb; beta = 2*(1-cos(nb))/nb2; gamma = nb/2*cot(nb/2);
    A = zeros(3);
    if nb > 1e-7
    A = (1-gamma)/nb2*parFloor(a,c) ...
        + 1/nb2^2*((gamma-1)*(gamma+2)+nb2/4) *(parCeil(a,b)*c)*b'...
        + (1/beta+gamma-2)/nb2^2*((tild(b)^2*c)*a' + (b'*a)*parFloor(b,c)) ...
        + (b'*a)/nb2^3*(-2*(alpha+beta)/beta^2-nb2/4-gamma*(gamma+3)+8)*(tild(b)^2*c)*b'; %eq. A.19
    end

    computeDTSO3M1TC = computeDTSO3invTc(h(4:6),v(1:3));
    out = [zeros(3)   computeDTSO3M1TC;...
           computeDTSO3M1TC  A+computeDTSO3invTc(h(4:6),v(4:6))];
end

