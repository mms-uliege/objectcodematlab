%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = tild(x)
    % out = tild(x)
    %
    % Returns the 3x1 vector associated with a skew-symmetric matrix or the
    % other way around
    %
    % INPUT:
    %       -x: 3x1 vector or 3x3 skew-symmetric matrix
    %
    % OUTPUTS:
    %       -out: associated form of x
    %
    % Valentin Sonneville (2012)

    if min(size(x)) == 1
        out = [0 -x(3) x(2); x(3) 0 -x(1); -x(2) x(1) 0];
    else       
        if norm(x'+x) > 1e-3
            error('input is not skewsymmetric')
        else
            x = (x-x');
            out = 0.5*[x(3,2); x(1,3); x(2,1)];
        end
    end