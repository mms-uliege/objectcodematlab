%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [a,delta] = vectSkew(A)
%returns vector a, the vectrial part of a skew-symmetric matrix,
%and delta a, the variation of a

    a = 0.5*[A(3,2) - A(2,3); A(1,3) - A(3,1); A(2,1) - A(1,2)];

    if nargout == 2
        delta = zeros(3,6);
        delta(1,1:3) = 0.5*[A(2,2)+A(3,3) -A(2,1) -A(3,1)];
        delta(1,4:6) = 0.5*[-(A(2,2)+A(3,3)) A(1,2) A(1,3)];
        delta(2,1:3) = 0.5*[-A(1,2) A(1,1)+A(3,3) -A(3,2)];
        delta(2,4:6) = 0.5*[A(2,1) -(A(1,1)+A(3,3)) A(2,3)];
        delta(3,1:3) = 0.5*[-A(1,3) -A(2,3) A(1,1)+A(2,2)];
        delta(3,4:6) = 0.5*[A(3,1) A(3,2) -(A(1,1)+A(2,2))];
    end