%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = computeDTSE3UinvC(h,c)
    a=h(1:3); b=h(4:6); c=c(:);
    nb = norm(b);
    if nb > 1e-4
        nb2 = nb^2;
        alpha = sin(nb)/nb; beta = 2*(1-cos(nb))/nb2; gamma = nb/2*cot(nb/2);
        out = zeros(3,6);
        out(:,1:3) = -0.5*tild(c) + (1-gamma)/nb2*parFloor(b,c) + (1/beta+gamma-2)/nb2^2*(tild(b)^2*c)*b';
        out(:,4:6) = (1-gamma)/nb2*parFloor(a,c)...
            + 1/nb2^2*((gamma-1)*(gamma+2)+nb2/4)*(parCeil(a,b)*c)*b'...
            + (1/beta+gamma-2)/nb2^2*((tild(b)^2*c)*a' + (b'*a)*parFloor(b,c)) ...
            + (b'*a)/nb2^3*(-2*(alpha+beta)/beta^2-nb2/4-gamma*(gamma+3)+8)*(tild(b)^2*c)*b'; %todesco eq A.19
    else
        out = [-0.5*tild(c) 1/12*parFloor(a,c)];
    end
end