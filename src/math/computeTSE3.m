%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Lie group functions

% Tangent application for SE3
function T = computeTSE3(h)
    h_U = h(1:3); h_Omega = h(4:6);
    Tso3 = computeTSO3(h_Omega);
    Tuop = TUOmega_p(h_U,h_Omega);
    T = [Tso3 Tuop; zeros(3) Tso3];
end

function T = TUOmega_p(a,b)
    nb = norm(b); ta = tild(a);
    if nb > 1e-8
        tb = tild(b);
        alpha = sin(nb)/nb;
        beta = 2*(1-cos(nb))/nb^2;
        T = -0.5*beta*ta + (1-alpha)/(nb^2)*(ta*tb+tb*ta) - (alpha-beta)/(nb^2)*(a'*b)*tb...
            +(a'*b)/nb^2*(0.5*beta-3*(1-alpha)/nb^2)*tb*tb;
    else
        T = -0.5*ta;
    end
end