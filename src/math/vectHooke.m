%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [a,b,c] = vectHooke(x,y)
    % Returns three vectors of a local frame compatible with x, y
    %
    % INPUT:
    %       -x: reference vector
    %       or
    %       -x: reference vector
    %       -y: reference vector
    %
    % OUTPUTS:
    %       -a,b,x: the three vectors of the local frame
    %
    % Valentin Sonneville (2012)
    % Arthur Lismonde, Juliano Todesco (28 February 2018)

    c = x(:);
    if nargin==2 
        b = [x(2)*y(3)-x(3)*y(2)  x(3)*y(1)-x(1)*y(3) x(1)*y(2)-x(2)*y(1)]';
        a = [b(2)*c(3)-b(3)*c(2)  b(3)*c(1)-b(1)*c(3) b(1)*c(2)-b(2)*c(1)]';

    else
        if or(x(1),x(2))
            a = [-x(2) x(1) 0]';
            b = [-x(1)*x(3) -x(2)*x(3) (x(1)^2+x(2)^2)]';
        else
            a = [1 0 0]';
            b = [0 1 0]';
        end
    end

    a = a/norm(a);
    b = b/norm(b);
    c = c/norm(c);
