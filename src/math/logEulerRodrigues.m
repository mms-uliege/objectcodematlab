%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = logEulerRodrigues(H)
    if size(H,1) == 4
        R = H(1:3,1:3);
        phi = acos(0.5*(trace(R)-1));
        if norm(phi) > 1e-8
            n = vectSkew(.5/sin(phi)*(R-transpose(R)));
            v = 2*n*sin(phi/2);
            D = sqrt(1-v'*v/4)*eye(3)-0.5*tild(v); % H^-1

            out = [D*H(1:3,4); v];

        else
            out = [H(1:3,4); zeros(3,1)];
        end

    else
        R = H(1:3,1:3);
        phi = acos(0.5*(trace(R)-1));
        if norm(phi) > 1e-8
            n = vectSkew(.5/sin(phi)*(R-transpose(R)));
            out = 2*n*sin(phi/2);        
        else
            out = zeros(3,1);
        end
    end
end