%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = computeDTSE3U(h,v)
    % out is linear in velocity -> if k*h: out = DT(k*h).(k*v) => input v must
    % be multiply by k

    h = h(:); v = v(:);
    O = h(4:6); nO = norm(O); Od = v(4:6);
    U = h(1:3); Ud = v(1:3);  tUd = tild(Ud);

    % if nO > 1e-8
    if nO > 1e-12 %TODESC0
        tU = tild(U);
        tO = tild(O); tOd = tild(Od);
        s = 2*sin(nO/2)/nO; c = cos(nO/2); alpha = s*c; beta = s*s;
        zeta = 1/nO^2*(0.5*beta-3*(1-alpha)/nO^2);
        OdTO = Od'*O; OTU = O'*U;

        out = -0.5*beta*tUd - (alpha-beta)/nO^2*OdTO*tU...
            +(1-alpha)/nO^2*(parCeil(Ud,O) + parCeil(U,Od))-(alpha-beta)/nO^2*((Od'*U+Ud'*O)*tO+OTU*tOd)...
            +zeta*((Od'*U+Ud'*O)*tO*tO + OTU*parCeil(O,Od) + OdTO*parCeil(U,O))...
            -((1-5*alpha-0.5*beta*(nO^2-8))*tO-(alpha-3.5*beta+15*(1-alpha)/nO^2)*tO^2)*OdTO*OTU/nO^4;
    else
        out = -0.5*tUd + 1/6*parCeil(U,Od);
    end
end