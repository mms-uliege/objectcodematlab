%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotVector(H,origin,scale,linewidth)

    if size(H)~=[3 1]
        if size(H)==[1 3]
            H = H';
        else
            disp('Vector dimension is not right')
            return
        end
    end

    if nargin < 2
        origin = [0 0 0]';
    end
    if nargin < 3
        scale = 0.1;
    end
    if nargin < 4
        linewidth = 2;
    end
    Hn = H/norm(H);

    hold on
    plot3([origin(1) origin(1)+Hn(1)*scale],[origin(2) origin(2)+Hn(2)*scale],[origin(3) origin(3)+Hn(3)*scale],'r','Linewidth',linewidth)
    text(origin(1)+Hn(1)*scale,origin(2)+Hn(2)*scale,origin(3)+Hn(3)*scale,[' (',num2str(H(1)),', ',num2str(H(2)),', ',num2str(H(3)),')'])
    grid on
    xlabel('X')
    ylabel('Y')
    zlabel('Z')


end