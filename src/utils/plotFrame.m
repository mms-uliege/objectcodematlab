%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotFrame(H,scale,linewidth)

    if and(size(H,1)~=4,size(H,2)~=4)
        disp('Frame dimension is not right')
        return
    end
    if nargin < 2
        scale = 0.1;
    end
    if nargin < 3
        linewidth = 2;
    end
    hold on
    plot3([H(1,4) H(1,4)+H(1,1)*scale],[H(2,4) H(2,4)+H(2,1)*scale],[H(3,4) H(3,4)+H(3,1)*scale],'g','Linewidth',linewidth)
    plot3([H(1,4) H(1,4)+H(1,2)*scale],[H(2,4) H(2,4)+H(2,2)*scale],[H(3,4) H(3,4)+H(3,2)*scale],'r','Linewidth',linewidth)
    plot3([H(1,4) H(1,4)+H(1,3)*scale],[H(2,4) H(2,4)+H(2,3)*scale],[H(3,4) H(3,4)+H(3,3)*scale],'b','Linewidth',linewidth)
    text(H(1,4),H(2,4),H(3,4),[' (',num2str(H(1,4)),', ',num2str(H(2,4)),', ',num2str(H(3,4)),')'])
    grid on
    xlabel('X')
    ylabel('Y')
    zlabel('Z')


end