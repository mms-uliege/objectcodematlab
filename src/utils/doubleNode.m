%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function newNodes = doubleNode(nodes, numbers)
    % Starting from a set of nodes [# x y z] (n by 4 matrix)
    % Double a selected node (number is a vector of the numbers of the nodes to
    % be doubled)


    nDouble = length(numbers);
    newNodes = zeros(size(nodes,1)+nDouble,4);

    index = 0;
    for i = 1:size(nodes,1)
        node2double = nodes(i,2:4);
        newNodes(i+index,2:4) = node2double;
        newNodes(i+index,1) = i+index;
        if ~isempty(find(numbers==i, 1))
            index = index+1;
            newNodes(i+index,2:4) = node2double;
            newNodes(i+index,1) = i+index;
        end
    end
end