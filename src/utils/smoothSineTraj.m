%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function traj = smoothSineTraj(c1_i,freq,amplitude,timeVector,start_blend1,end_blend1,start_blend2,end_blend2)
    % Creates a smooth (at beginning and end) oscillating trajectory around
    % c1_i (at frequency freq and amplitude amplitude). It creates 2 blending
    % zones at the beginning (from start_blend1 to end_blend1) and at the end 
    % (from start_blend2 to end_blend2)of the sine oscillating trajectory using
    % a 7th or 9th order polynomial parameterized using a parameter sigma that
    % goes from c1_i to a value in the sine at time end_blend1 and from the
    % value in the sine at time start_blend2 to c1_i

    traj = zeros(size(timeVector));

    ti = timeVector(1);
    tf = timeVector(end);
    sine_duration = end_blend2 - start_blend1;
    blend1_duration = end_blend1 - start_blend1;
    blend2_duration = end_blend2 - start_blend2;
    pulse = 2*pi*freq;

    order = 7; % 7 or 9

    ub_1 = sin(pulse*(end_blend1-ti));
    lb_1 = 0;
    d_ub_1 = pulse*cos(pulse*(end_blend1-ti));
    d_lb_1 = 0;
    dd_ub_1 = -pulse^2*sin(pulse*(end_blend1-ti));
    dd_lb_1 = 0;
    ddd_ub_1 = -pulse^3*cos(pulse*(end_blend1-ti));
    ddd_lb_1 = 0;
    dddd_ub_1 = pulse^4*sin(pulse*(end_blend1-ti));
    dddd_lb_1 = 0;

    ub_2 = 0;
    lb_2 = sin(pulse*(start_blend2-ti));
    d_ub_2 = 0;
    d_lb_2 = pulse*cos(pulse*(start_blend2-ti));
    dd_ub_2 = 0;
    dd_lb_2 = -pulse^2*sin(pulse*(start_blend2-ti));
    ddd_ub_2 = 0;
    ddd_lb_2 = -pulse^3*cos(pulse*(start_blend2-ti));
    dddd_ub_2 = 0;
    dddd_lb_2 = pulse^4*sin(pulse*(start_blend2-ti));

    if order == 7
        % Calculation of 1st blending polynomial
        A_1 = [start_blend1^7 start_blend1^6 start_blend1^5 start_blend1^4 start_blend1^3 start_blend1^2 start_blend1 1
             7*start_blend1^6 6*start_blend1^5 5*start_blend1^4 4*start_blend1^3 3*start_blend1^2 2*start_blend1 1 0
             42*start_blend1^5 30*start_blend1^4 20*start_blend1^3 12*start_blend1^2 6*start_blend1 2 0 0
             210*start_blend1^4 120*start_blend1^3 60*start_blend1^2 24*start_blend1 6 0 0 0
             end_blend1^7 end_blend1^6 end_blend1^5 end_blend1^4 end_blend1^3 end_blend1^2 end_blend1 1
             7*end_blend1^6 6*end_blend1^5 5*end_blend1^4 4*end_blend1^3 3*end_blend1^2 2*end_blend1 1 0
             42*end_blend1^5 30*end_blend1^4 20*end_blend1^3 12*end_blend1^2 6*end_blend1 2 0 0
             210*end_blend1^4 120*end_blend1^3 60*end_blend1^2 24*end_blend1 6 0 0 0];

        cpx_1 = A_1\[lb_1 d_lb_1 dd_lb_1 ddd_lb_1 ub_1 d_ub_1 dd_ub_1 ddd_ub_1]';

        % Calculation of 2nd blending polynomial
        A_2 = [start_blend2^7 start_blend2^6 start_blend2^5 start_blend2^4 start_blend2^3 start_blend2^2 start_blend2 1
             7*start_blend2^6 6*start_blend2^5 5*start_blend2^4 4*start_blend2^3 3*start_blend2^2 2*start_blend2 1 0
             42*start_blend2^5 30*start_blend2^4 20*start_blend2^3 12*start_blend2^2 6*start_blend2 2 0 0
             210*start_blend2^4 120*start_blend2^3 60*start_blend2^2 24*start_blend2 6 0 0 0
             end_blend2^7 end_blend2^6 end_blend2^5 end_blend2^4 end_blend2^3 end_blend2^2 end_blend2 1
             7*end_blend2^6 6*end_blend2^5 5*end_blend2^4 4*end_blend2^3 3*end_blend2^2 2*end_blend2 1 0
             42*end_blend2^5 30*end_blend2^4 20*end_blend2^3 12*end_blend2^2 6*end_blend2 2 0 0
             210*end_blend2^4 120*end_blend2^3 60*end_blend2^2 24*end_blend2 6 0 0 0];

        cpx_2 = A_2\[lb_2 d_lb_2 dd_lb_2 ddd_lb_2 ub_2 d_ub_2 dd_ub_2 ddd_ub_2]';
    elseif order == 9
        % Calculation of 1st blending polynomial
        A_1 = [start_blend1^9 start_blend1^8 start_blend1^7 start_blend1^6 start_blend1^5 start_blend1^4 start_blend1^3 start_blend1^2 start_blend1 1;...
             9*start_blend1^8 8*start_blend1^7 7*start_blend1^6 6*start_blend1^5 5*start_blend1^4 4*start_blend1^3 3*start_blend1^2 2*start_blend1 1 0;...
             72*start_blend1^7 56*start_blend1^6 42*start_blend1^5 30*start_blend1^4 20*start_blend1^3 12*start_blend1^2 6*start_blend1 2 0 0;...
             504*start_blend1^6 336*start_blend1^5 210*start_blend1^4 120*start_blend1^3 60*start_blend1^2 24*start_blend1 6 0 0 0;...
             3024*start_blend1^5 1680*start_blend1^4 840*start_blend1^3 360*start_blend1^2 120*start_blend1 24 0 0 0 0;...
             end_blend1^9 end_blend1^8 end_blend1^7 end_blend1^6 end_blend1^5 end_blend1^4 end_blend1^3 end_blend1^2 end_blend1 1;...
             9*end_blend1^8 8*end_blend1^7 7*end_blend1^6 6*end_blend1^5 5*end_blend1^4 4*end_blend1^3 3*end_blend1^2 2*end_blend1 1 0;...
             72*end_blend1^7 56*end_blend1^6 42*end_blend1^5 30*end_blend1^4 20*end_blend1^3 12*end_blend1^2 6*end_blend1 2 0 0;...
             504*end_blend1^6 336*end_blend1^5 210*end_blend1^4 120*end_blend1^3 60*end_blend1^2 24*end_blend1 6 0 0 0;...
             3024*end_blend1^5 1680*end_blend1^4 840*end_blend1^3 360*end_blend1^2 120*end_blend1 24 0 0 0 0];

        cpx_1 = A_1\[lb_1 d_lb_1 dd_lb_1 ddd_lb_1 dddd_lb_1 ub_1 d_ub_1 dd_ub_1 ddd_ub_1 dddd_ub_1]';

        % Calculation of 2nd blending polynomial
        A_2 = [start_blend2^9 start_blend2^8 start_blend2^7 start_blend2^6 start_blend2^5 start_blend2^4 start_blend2^3 start_blend2^2 start_blend2 1;...
             9*start_blend2^8 8*start_blend2^7 7*start_blend2^6 6*start_blend2^5 5*start_blend2^4 4*start_blend2^3 3*start_blend2^2 2*start_blend2 1 0;...
             72*start_blend2^7 56*start_blend2^6 42*start_blend2^5 30*start_blend2^4 20*start_blend2^3 12*start_blend2^2 6*start_blend2 2 0 0;...
             504*start_blend2^6 336*start_blend2^5 210*start_blend2^4 120*start_blend2^3 60*start_blend2^2 24*start_blend2 6 0 0 0;...
             3024*start_blend2^5 1680*start_blend2^4 840*start_blend2^3 360*start_blend2^2 120*start_blend2 24 0 0 0 0;...
             end_blend2^9 end_blend2^8 end_blend2^7 end_blend2^6 end_blend2^5 end_blend2^4 end_blend2^3 end_blend2^2 end_blend2 1;...
             9*end_blend2^8 8*end_blend2^7 7*end_blend2^6 6*end_blend2^5 5*end_blend2^4 4*end_blend2^3 3*end_blend2^2 2*end_blend2 1 0;...
             72*end_blend2^7 56*end_blend2^6 42*end_blend2^5 30*end_blend2^4 20*end_blend2^3 12*end_blend2^2 6*end_blend2 2 0 0;...
             504*end_blend2^6 336*end_blend2^5 210*end_blend2^4 120*end_blend2^3 60*end_blend2^2 24*end_blend2 6 0 0 0;...
             3024*end_blend2^5 1680*end_blend2^4 840*end_blend2^3 360*end_blend2^2 120*end_blend2 24 0 0 0 0];

        cpx_2 = A_2\[lb_2 d_lb_2 dd_lb_2 ddd_lb_2 dddd_lb_2 ub_2 d_ub_2 dd_ub_2 ddd_ub_2 dddd_ub_2]';
    end


    for t = 1:length(timeVector)
        dt = timeVector(t);
        if order == 7
            sigma_1 = cpx_1(1)*dt^7 + cpx_1(2)*dt^6 + cpx_1(3)*dt^5 + cpx_1(4)*dt^4 + cpx_1(5)*dt^3 + cpx_1(6)*dt^2 + cpx_1(7)*dt + cpx_1(8);
            sigma_2 = cpx_2(1)*dt^7 + cpx_2(2)*dt^6 + cpx_2(3)*dt^5 + cpx_2(4)*dt^4 + cpx_2(5)*dt^3 + cpx_2(6)*dt^2 + cpx_2(7)*dt + cpx_2(8);
        elseif order == 9
            sigma_1 = cpx_1(1)*dt^9 + cpx_1(2)*dt^8 + cpx_1(3)*dt^7 + cpx_1(4)*dt^6 + cpx_1(5)*dt^5 + cpx_1(6)*dt^4 + cpx_1(7)*dt^3 + cpx_1(8)*dt^2 + cpx_1(9)*dt + cpx_1(10);
            sigma_2 = cpx_2(1)*dt^9 + cpx_2(2)*dt^8 + cpx_2(3)*dt^7 + cpx_2(4)*dt^6 + cpx_2(5)*dt^5 + cpx_2(6)*dt^4 + cpx_2(7)*dt^3 + cpx_2(8)*dt^2 + cpx_2(9)*dt + cpx_2(10);
        end
        if timeVector(t)<start_blend1 % in constant part 1
            traj(t) = c1_i;
        elseif timeVector(t)>end_blend2 % in constant part 2
            traj(t) = c1_i;
        elseif timeVector(t)>=start_blend1 && timeVector(t)<=end_blend1 % in blending zone 1
            traj(t) = c1_i + amplitude*sigma_1;
        elseif timeVector(t)>=start_blend2 && timeVector(t)<=end_blend2 % in blending zone 2
            traj(t) = c1_i + amplitude*sigma_2;
        else  % in sine part
            traj(t) = c1_i + amplitude*sin(pulse*(dt-ti));
        end
    end
end