%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function trajc = halfCircleTraj(c1_i,c1_end,r,timeVector,type,ti,tf,order)
    % Creates a trajectory that is a 5th or 7th order polynomial between values
    % c1_i and c1_end, along the discretization timeVector. Before ti (after
    % tf) the value should be the initial c1_i (the final c1_end).
    % the type is either a linear interpolation or a sine/cosine (to make a
    % circle, of radius r (not always calculable in case of cosine, becaus finish and start are equal)

    trajc = zeros(size(timeVector));

    if nargin < 6
        ti = timeVector(1);
    end
    if nargin < 7
        tf = timeVector(end);
    end
    if nargin < 8
        order = 7;
    end
    duration = tf-ti;
    step = c1_end-c1_i;

    if strcmp(type,'cos')||strcmp(type,'sin')
        if c1_i > c1_end
            ub = -1;
            lb = 1;
            sig = -1;
        else
            ub = 1;
            lb = -1;
            sig = 1;
        end
    else
        ub = c1_end;
        lb = c1_i;
    end

    if order == 5
        A = [duration^5 duration^4 duration^3 duration^2 duration 1
             5*duration^4 4*duration^3 3*duration^2 2*duration 1 0
             20*duration^3 12*duration^2 6*duration 2 0 0
             0 0 0 0 0 1
             0 0 0 0 1 0
             0 0 0 2 0 0];
        cpx = A\[ub 0 0 lb 0 0]';
        for t = 1:length(timeVector)
            if timeVector(t) >= ti && timeVector(t) <= tf
                dt = timeVector(t)-ti;
                pol = cpx(1)*dt^5 + cpx(2)*dt^4 + cpx(3)*dt^3 + cpx(4)*dt^2 + cpx(5)*dt + cpx(6);
                if strcmp(type,'sin')
                    trajc(t) = r*sin(pol*pi/2)+(r - sig*abs(c1_i));
                elseif strcmp(type,'cos')
                    trajc(t) = r*cos(pol*pi/2)+c1_end;
                else
                    trajc(t) = pol;
                end
            elseif timeVector(t)<ti
                trajc(t) = c1_i;
            elseif timeVector(t)>tf
                trajc(t) = c1_end;
            end
        end

    elseif order == 7
        A = [duration^7 duration^6 duration^5 duration^4 duration^3 duration^2 duration 1
             7*duration^6 6*duration^5 5*duration^4 4*duration^3 3*duration^2 2*duration 1 0
             42*duration^5 30*duration^4 20*duration^3 12*duration^2 6*duration 2 0 0
             210*duration^4 120*duration^3 60*duration^2 24*duration 6 0 0 0
             0 0 0 0 0 0 0 1
             0 0 0 0 0 0 1 0
             0 0 0 0 0 2 0 0
             0 0 0 0 6 0 0 0];
        cpx = A\[ub 0 0 0 lb 0 0 0]';
        for t = 1:length(timeVector)
            if timeVector(t) >= ti && timeVector(t) <= tf
                dt = timeVector(t)-ti;
                pol = cpx(1)*dt^7 + cpx(2)*dt^6 + cpx(3)*dt^5 + cpx(4)*dt^4 + cpx(5)*dt^3 + cpx(6)*dt^2 + cpx(7)*dt + cpx(8);
                if strcmp(type,'sin')
                    trajc(t) = r*sin(pol*pi/2)+(step/2 - sig*abs(c1_i));
                elseif strcmp(type,'cos')
                    trajc(t) = r*cos(pol*pi/2)+c1_end;
                else
                    trajc(t) = pol;
                end
            elseif timeVector(t)<ti
                trajc(t) = c1_i;
            elseif timeVector(t)>tf
                if strcmp(type,'sin')
                    trajc(t) = r*sin(ub*pi/2)+(step/2 - sig*abs(c1_i));
                else
                    trajc(t) = c1_end;
                end
            end
        end
    end   


end