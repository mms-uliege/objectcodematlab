%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function trajc = lineTraj(c1_i,c1_end,timeVector,ti,tf)
    % Creates a trajectory between values
    % c1_i and c1_end, using a parameter sigma that is a 7th or 9th order polynomial
    % and that goes from 0 (c1_i) to 1 (c1_end), trajectory can have a initial
    % time and final time different that the first (vs last) time step in time
    % vector "timeVector".


    trajc = zeros(size(timeVector));

    step = c1_end-c1_i; % jump between initial value and final one

    order = 7; % 7 or 9

    if order == 7
        A = [ti^7 ti^6 ti^5 ti^4 ti^3 ti^2 ti 1
             7*ti^6 6*ti^5 5*ti^4 4*ti^3 3*ti^2 2*ti 1 0
             42*ti^5 30*ti^4 20*ti^3 12*ti^2 6*ti 2 0 0
             210*ti^4 120*ti^3 60*ti^2 24*ti 6 0 0 0
             tf^7 tf^6 tf^5 tf^4 tf^3 tf^2 tf 1
             7*tf^6 6*tf^5 5*tf^4 4*tf^3 3*tf^2 2*tf 1 0
             42*tf^5 30*tf^4 20*tf^3 12*tf^2 6*tf 2 0 0
             210*tf^4 120*tf^3 60*tf^2 24*tf 6 0 0 0];

         ub = 1;
         lb = 0;

        cpx = A\[lb 0 0 0 ub 0 0 0]';
    elseif order == 9
        A = [ti^9 ti^8 ti^7 ti^6 ti^5 ti^4 ti^3 ti^2 ti 1;...
             9*ti^8 8*ti^7 7*ti^6 6*ti^5 5*ti^4 4*ti^3 3*ti^2 2*ti 1 0;...
             72*ti^7 56*ti^6 42*ti^5 30*ti^4 20*ti^3 12*ti^2 6*ti 2 0 0;...
             504*ti^6 336*ti^5 210*ti^4 120*ti^3 60*ti^2 24*ti 6 0 0 0;...
             3024*ti^5 1680*ti^4 840*ti^3 360*ti^2 120*ti 24 0 0 0 0;...
             tf^9 tf^8 tf^7 tf^6 tf^5 tf^4 tf^3 tf^2 tf 1;...
             9*tf^8 8*tf^7 7*tf^6 6*tf^5 5*tf^4 4*tf^3 3*tf^2 2*tf 1 0;...
             72*tf^7 56*tf^6 42*tf^5 30*tf^4 20*tf^3 12*tf^2 6*tf 2 0 0;...
             504*tf^6 336*tf^5 210*tf^4 120*tf^3 60*tf^2 24*tf 6 0 0 0;...
             3024*tf^5 1680*tf^4 840*tf^3 360*tf^2 120*tf 24 0 0 0 0];

         ub = 1;
         lb = 0;

        cpx = A\[lb 0 0 0 0 ub 0 0 0 0]';
    end

    for t = 1:length(timeVector)
        dt = timeVector(t);
        if order == 7
            sigma = cpx(1)*dt^7 + cpx(2)*dt^6 + cpx(3)*dt^5 + cpx(4)*dt^4 + cpx(5)*dt^3 + cpx(6)*dt^2 + cpx(7)*dt + cpx(8);
        elseif order == 9
            sigma = cpx(1)*dt^9 + cpx(2)*dt^8 + cpx(3)*dt^7 + cpx(4)*dt^6 + cpx(5)*dt^5 + cpx(6)*dt^4 + cpx(7)*dt^3 + cpx(8)*dt^2 + cpx(9)*dt + cpx(10);
        end
        if timeVector(t)<ti
            trajc(t) = c1_i;
        elseif timeVector(t)>tf
            trajc(t) = c1_end;
        else
            trajc(t) = c1_i + step*sigma;
        end
    end

end