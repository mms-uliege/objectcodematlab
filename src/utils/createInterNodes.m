%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function NewNodes = createInterNodes(nodes,nElem,Start,End)
    % Starting from a set of nodes [# x y z] (n by 4 matrix)
    % Create new nodes for a given number of elements between
    
    if size(nodes,2)==3
        StartNode = nodes(Start,2:3);
        EndNode = nodes(End,2:3);
        dir = EndNode - StartNode;
        length = norm(dir);
        NewNodes = zeros(size(nodes,1)+nElem-1,3);
        NewNodes(1:Start,:) = nodes(1:Start,:);
        NewNodes(End+nElem-1:end,2:3) = nodes(End:end,2:3);
        NewNodes(End+nElem-1:end,1) = nodes(End:end,1)+nElem-1;
        for i = 1:nElem-1
            k = i/nElem;
            NewNode = StartNode + dir*k;
            NewNodes(Start+i,:) = [Start+i NewNode];
        end
    else

        StartNode = nodes(Start,2:4);
        EndNode = nodes(End,2:4);
        dir = EndNode - StartNode;
        length = norm(dir);
        NewNodes = zeros(size(nodes,1)+nElem-1,4);
        NewNodes(1:Start,:) = nodes(1:Start,:);
        NewNodes(End+nElem-1:end,2:4) = nodes(End:end,2:4);
        NewNodes(End+nElem-1:end,1) = nodes(End:end,1)+nElem-1;
        for i = 1:nElem-1
            k = i/nElem;
            NewNode = StartNode + dir*k;
            NewNodes(Start+i,:) = [Start+i NewNode];
        end
    end
end
