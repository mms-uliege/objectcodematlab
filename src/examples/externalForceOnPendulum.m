%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Exemple using the ImposeNodeMotionRelCoo element.
% The system is a simple pendulum attached, at its top to a sliding joint. 
% A motion in the x and z component is imposed on the sliding joint to make
% the pendulum move (no consideration of extra control object).

clear all
close all

%% Definition of time parameters
finaltime = 2;
timestepsize = 0.01;

timeVector = 0:timestepsize:finaltime;

%% Definition of the nodes of the model (as matrix)
l = 1; % length of the pendulum
grav = [0 0 0];
m_end = 1; % end effector mass

nodes = [1 0 0 0;
         2 0 0 0;
         3 0 0 -l];
 

%% Definition of the elements of the model (as structure)
count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [3 2];
elements{count}.mass = m_end;
elements{count}.acceleration = grav;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.d = 0.1;
elements{count}.A = [0 0 0 0 1 0]';

% Force element on node 3: Constant force along X and constant torque
% around Y. Number of lines in "Axe" property must be equal to number of
% lines in "F".
count = size(elements,2)+1;
elements{count}.type = 'ForceOnNode';
elements{count}.nodes = 3;
elements{count}.Axe = [1 0 0 0 0 0;
                       0 0 0 0 1 0];
elements{count}.following = false;
elements{count}.F = [1 0.1]';

%% Definition of the boundary conditions

BC = [1]; % Node 1 is fixed

%% Definition of the finite element model (FEModel Object)
% Based on previously defined nodes and elements
Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

%% Definition of the solver and its parameters
tic
D = DynamicIntegration(Model);  % Creatind the Dynamic integration object
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.1;
D.parameters.scaling = 1e6;
D.runIntegration(); % Run the integration of the object
compTime = toc;
disp(['Computation time is ',num2str(compTime), ' s'])


%% visualization and plots
% To visualize the model:(uncomment the following line)
% Model.visu

figure
hold on
plot(timeVector,D.model.listElementVariables{1}.relCoo(1,:),'Linewidth',2)
grid on
title('Angular position of the joint of the pendulum')
xlabel('Time (s)')
ylabel('Angle (rad)')
