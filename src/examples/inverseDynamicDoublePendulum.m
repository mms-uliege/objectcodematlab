%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test example for the inverse dynamics of a double pendulum with imposed
% motion on the end-node and see what torque must be appplied to the hinges
% for the given motion (straight line)
clear all
close all

%% Definition of time parameters
finaltime = 1;
timestepsize = 0.01;


%% Definition of the nodes of the model (as matrix)
l1 = 1;
l2 = l1;
angle = 30*pi/180;
nodes = [1 0 0 0;
         2 0 0 0;
         3 l1*cos(angle) 0 l1*sin(angle);
         4 l1*cos(angle) 0 l1*sin(angle);
         5 l1*cos(angle)+l2*cos(angle) 0 0];
  
 
%% Definition of the elements of the model (as structure)
% Element 1: First rigid link
count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [3 2];
elements{count}.mass = 1;
elements{count}.J = 0.01*eye(3);

% Element 2: Second rigid link
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [5 4];
elements{count}.mass = 1;
elements{count}.J = 0.01*eye(3);

% Element 3: First kinematic joint which is a hinge
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 1 0]';

% Element 4: Second kinematic joint which is a hinge
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [3 4];
elements{count}.A = [0 0 0 0 1 0]';


% Element 5: Apply a given trajectory to the last node and 
time = 0:timestepsize:finaltime;
trajx = lineTraj(nodes(end,2),0,time,0,finaltime);
trajz = lineTraj(nodes(end,4),-1,time,0,finaltime);
count = size(elements,2)+1;
elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = nodes(end,1);
elements{count}.elements = [count-2 count-1];
elements{count}.T = [trajx;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 0 1];

%% Definition of the boundary conditions

BC = [1]; % Node 1 is fixed

%% Definition of the finite element model (FEModel Object)
% Based on previously defined nodes and elements

Model = FEModel;
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

%% Definition of the solver and its parameters

D = DynamicIntegration(Model);  % Creatind the Dynamic integration object
D.parameters.timestepsize = timestepsize;
D.parameters.finaltime = finaltime;
D.parameters.rho = 0.2;
D.parameters.scaling = 1e6;
D.runIntegration(); % Run the integration of the object

%% visualization and plots
% To visualize the model:(uncomment the following line)
% Model.visu

% Plots
endNodePos = Model.listNodes{end}.position;
figure
plot(D.parameters.time,endNodePos(1,:),D.parameters.time,endNodePos(2,:),D.parameters.time,endNodePos(3,:),'Linewidth',2)
grid on
title('Tip of the arm position')
xlabel('Time [s]')
ylabel('Poistion [m]')
legend('X','Y','Z')


command = D.model.listElementVariables{end}.value;
figure
plot(D.parameters.time,command(1,:),D.parameters.time,command(2,:),'Linewidth',2)
grid on
title('Hinge torque (command variables)')
xlabel('Time [s]')
ylabel('Torque [Nm]')
legend('Hinge 1','Hinge 2')

command = [D.model.listElementVariables{end-2}.value(5,:);D.model.listElementVariables{end-1}.value(5,:)];
figure
plot(D.parameters.time,command(1,:),D.parameters.time,command(2,:),'Linewidth',2)
grid on
title('Hinge torque (lagrange multipliers)')
xlabel('Time [s]')
ylabel('Torque [Nm]')
legend('Hinge 1','Hinge 2')