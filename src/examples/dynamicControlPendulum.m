%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test example for PD control of a single pendulum which has to move to a reference position "ref"
% with PD gains kp and kd (optional), and a feedforward term "u" (that can be
% optional too)
clear all
close all

%% Definition of time parameters
finaltime = 2;
timestepsize = 0.01;

time = 0:timestepsize:finaltime;

%% Definition of the nodes of the model (as matrix)
l = 1; % length of the pendulum
grav = [0 0 -9.81];
m_end = 1; % end effector mass

nodes = [1 0 0 0;
         2 0 0 0;
         3 l 0 0];
     
%% Control parameters
u = 1*ones(size(time));
ref = -pi/2*ones(size(time)); % reference at 90�
dref = 0*ones(size(time));
kp = 100;
kd = 10;

%% Definition of the elements of the model (as structure)
count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [3 2];
elements{count}.mass = m_end;
elements{count}.acceleration = grav;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = count-1;
elements{count}.kp = kp;
elements{count}.kd = kd;
elements{count}.f = u;
elements{count}.ref = ref;
elements{count}.dref = dref;

%% Definition of the boundary conditions

BC = [1]; % Node 1 is fixed

%% Definition of the finite element model (FEModel Object)
% Based on previously defined nodes and elements
Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

%% Definition of the solver and its parameters
tic
D = DynamicIntegration(Model);  % Creatind the Dynamic integration object
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.1;
D.parameters.scaling = 1e6;
D.runIntegration(); % Run the integration of the object
compTime = toc;
disp(['Computation time is ',num2str(compTime), ' s'])


%% visualization and plots
% To visualize the model:(uncomment the following line)
% Model.visu

figure
plot(time,D.model.listElementVariables{1}.relCoo,time,ref)
grid on
title('Joint angle of the pendulum')
xlabel('Time (s)')
ylabel('Angle (rad)')
legend('Actual angle','Reference')

figure
plot(time,D.model.listElementVariables{1}.velocity,time,dref)
grid on
title('Joint velocity of the pendulum')
xlabel('Time (s)')
ylabel('Angle (rad)')
legend('Actual angle','Reference')