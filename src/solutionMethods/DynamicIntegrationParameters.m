%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef DynamicIntegrationParameters < handle

    properties
        finaltime       = 1;
        timestepsize    = 1;
        timestep        = 1;
        scaling         = 1e6;
        relTolRes       = 1e-3;
        relTolPhi       = 1e-3;
        absTol          = 1e-3;
        itMax           = 15;
        rho             = 1;
        nstep;
        time;
        alpha_m;
        alpha_f;
        gamma;
        beta;
        
        predictionParameters;
        coefUpdateA;
        stepParameters;
        stepParametersInit;
    end
    
    methods
        
        function obj = DynamicIntegrationParameters(varargin)
            if ~isempty(varargin)
                obj.rho = varargin{1};
            end
        end

        function obj = InitialComputation(obj)
            h = obj.timestepsize;
            obj.nstep = round(obj.finaltime/abs(obj.timestepsize));
            obj.time  = 0:h:h*obj.nstep;
            
            obj.alpha_m = (2*obj.rho-1)/(obj.rho+1);
            obj.alpha_f = obj.rho/(obj.rho+1);
            obj.gamma   = 0.5 + obj.alpha_f - obj.alpha_m;
            obj.beta    = (obj.gamma+0.5) * (obj.gamma+0.5) / 4;
            
            obj.stepParameters.predictionParameters = [h h^2*(0.5-obj.beta) h*(1-obj.gamma) obj.alpha_f/(1-obj.alpha_m) obj.alpha_m/(1-obj.alpha_m) h^2*obj.beta h*obj.gamma];
            obj.stepParameters.coefUpdateA = (1-obj.alpha_f)/(1-obj.alpha_m);

            obj.stepParameters.scaling  = obj.scaling; 
            obj.stepParameters.coefK    = 1;
            obj.stepParameters.gammap   = obj.gamma/(obj.beta*h);
            obj.stepParameters.betap    = 1/(h^2*obj.beta)*(1-obj.alpha_m)/(1-obj.alpha_f);
            obj.stepParameters.timestep = 1; 
            
            obj.stepParametersInit.scaling  = 1; 
            obj.stepParametersInit.coefK    = 0; 
            obj.stepParametersInit.gammap   = 0; 
            obj.stepParametersInit.betap    = 1;
        end
    end
    
end