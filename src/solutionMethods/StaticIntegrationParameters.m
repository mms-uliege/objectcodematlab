%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef StaticIntegrationParameters < handle

    properties
        finaltime = 1;
        timestepsize = 1;
        scaling = 1e6;
        relTolRes = 1e-3;
        relTolPhi = 1e-3;
        absTol = 1e-3;
        itMax = 15;
        time;
        nstep;
        stepParameters;
    end
    
    methods
        function obj = InitialComputation(obj)
            h = obj.timestepsize;
            obj.time = 0:h:obj.finaltime;
            obj.nstep = obj.finaltime/obj.timestepsize;
            
            obj.stepParameters.scaling = obj.scaling;
            obj.stepParameters.coefK = 1;
            obj.stepParameters.timestep = 1;
        end
        
    end
    
end

