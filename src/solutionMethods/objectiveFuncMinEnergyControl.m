%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [J, gradJ] = ObjectiveFunc_MinEnergyControl(x, Solution)
    
    scaleU = 0*1e0; % Coeff For torque min
    scaleD = 1e0; % Coeff for node or joint
    scaleE = 1e0; % Coeff for strain energy beam
    
    h = Solution.parameters.finaltime/(Solution.npts-1);
    J = 0;
    [size1 size2] = size(x);
    gradJ = sparse(size1, size2);
        
    for i = 1:Solution.npts
                
        u = [];
        if scaleU ~= 0
            for j = Solution.model.listElementVariables
                if strcmp(j{1}.DofType,'Command')
                    u = [u; j{1}.value(:,Solution.timesteps(i))];
                end
            end
        else
            u = 0;
        end
        
        % Compute the sum of the strain energy of each flexible beam element
        
        FlexBeamsEnergy = 0;
        if ~isempty(Solution.listFlexBeamElem)
            for nE = Solution.listFlexBeamElem
                LocFlexDof = [];
                FlexBeamElem = Solution.model.listElements{nE};
                listDof = FlexBeamElem.listNumberDof;
                for n = listDof
                    LocFlexDof = [LocFlexDof find(Solution.model.listFreeDof==n)];
                end
                Delta_qA = x((i-1)*Solution.nVar+LocFlexDof(1:6));
                Delta_qB = x((i-1)*Solution.nVar+LocFlexDof(7:12));

                R_A = vect9ToMat3(FlexBeamElem.listNodes{1}.R(:,Solution.timesteps(i)));
                x_A = FlexBeamElem.listNodes{1}.position(:,Solution.timesteps(i));
                H_Aoptim1 = [R_A' -R_A'*x_A;0 0 0 1];
                R_B = vect9ToMat3(FlexBeamElem.listNodes{2}.R(:,Solution.timesteps(i)));
                x_B = FlexBeamElem.listNodes{2}.position(:,Solution.timesteps(i));
                H_Bopti = [R_B x_B;0 0 0 1];
                
                h_ABopti = logSE3(H_Aoptim1*H_Bopti);
                h_AB0 = FlexBeamElem.h0;
                
                epsilon = (h_ABopti - h_AB0)/FlexBeamElem.L;
                VarEpsilon = [-computeTSE3inv(h_ABopti)*computeTSE3(Delta_qA) computeTSE3inv(h_ABopti)*computeTSE3(Delta_qB)];
                FlexBeamsEnergy = FlexBeamsEnergy + 0.5*(epsilon'*FlexBeamElem.KCS*epsilon); % epsilon is 6*1 varespilon is 12*1)
                
                % gradient related to current flex beam directly added to total gradient vector
                gradJ((i-1)*Solution.nVar+LocFlexDof) = scaleE*epsilon'*FlexBeamElem.KCS*VarEpsilon*h/Solution.parameters.finaltime;
            end
        end
        
        % Compute the displacement from one time step to the other (for a node)
        listDof = [];
        LocDof = [];
        if ~isempty(Solution.NodeToMinimize)
            for n = 1:length(Solution.NodeToMinimize)
                listDof = [listDof Solution.model.listNodes{Solution.model.listNumberNodes==Solution.NodeToMinimize(n)}.listNumberDof];
            end
            for n = listDof
                LocDof = [LocDof find(Solution.model.listFreeDof==n)];
            end
                
            VarNodeDisp = x((i-1)*Solution.nVar+LocDof);
        else
            VarNodeDisp = 0;
        end         
        
        % Compute the displacement from one time step to another (for a relative node)
        listJointDof = [];
        LocJointDof = [];
        if ~isempty(Solution.JointToMinimize)            
            for n = 1:length(Solution.JointToMinimize)
                LocElem = Solution.model.listNumberElements==Solution.JointToMinimize(n);
                listJointDof = [listJointDof Solution.model.listElements{LocElem}.listElementVariables.relativeNodeSE3{1}.listNumberDof];
            end
            for n = listJointDof
                LocJointDof = [LocJointDof find(Solution.model.listFreeDof==n)];
            end
            
            VarJointDisp = x((i-1)*Solution.nVar+LocJointDof);
        else
            VarJointDisp = 0;
        end
              
        term = (0.5*scaleU*(u' * u) + 0.5*scaleD*(VarNodeDisp' * VarNodeDisp) + 0.5*scaleD*(VarJointDisp' * VarJointDisp) + scaleE*FlexBeamsEnergy)* h;
        J = J + term;

        gradJ((i-1)*Solution.nVar+4*Solution.nCoord+Solution.nLagrangeM+1 : (i-1)*Solution.nVar+4*Solution.nCoord+Solution.nLagrangeM+Solution.nControls) = scaleU*u*h/Solution.parameters.finaltime;
        gradJ((i-1)*Solution.nVar+LocDof) = scaleD*VarNodeDisp*h/Solution.parameters.finaltime;
        gradJ((i-1)*Solution.nVar+LocJointDof) = scaleD*VarJointDisp*h/Solution.parameters.finaltime;
                
    end
    J = J/Solution.parameters.finaltime;

end
