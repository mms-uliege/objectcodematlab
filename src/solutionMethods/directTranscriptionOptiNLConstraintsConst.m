%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [c, ceq, gradc, gradceq] = directTranscriptionOptiNLConstraintsConst(x, Solution)
    %  [c, ceq, gradc, gradceqp] = 
    %   directTranscriptionOptiNLConstraintsConst(x, Solution)
    %
    %  - Compute nonlinear equalities and inequalities constraints of the optimization problem, and its gradients by semi-analytical method
    %  
    %  INPUTS:
    %         - x          = vector of design variables
    %         - Solution    = structure containing all data of the model

    %  OUTPUTS:
    %          - c          = vector of nonlinear inequalities constraints at xp
    %          - ceq        = vector of nonlinear equalities constraints at xp
    %          - gradc      = gradients of the nonlinear inequalities constraints at xp
    %          - gradceqp   = gradients of the nonlinear equalities constraints at xp



    c = [];            %  absence of inequality constraints 
    gradc = [];        %  absence of inequality constraints 

    npts = Solution.npts;

    sizeMotionConst = Solution.nCoord + Solution.nLagrangeM + Solution.nControls;
    
    if Solution.linConst
        sizeIntregrationConst = Solution.nCoord;
    else
        sizeIntregrationConst = 3*Solution.nCoord;
    end

    ceq = sparse(npts*(sizeMotionConst + sizeIntregrationConst) - sizeIntregrationConst, 1);

    gradceq = sparse(Solution.nVar*npts, npts*(sizeMotionConst + sizeIntregrationConst) - sizeIntregrationConst);

    Solution.Update(x)

    LocMotionDof = Solution.LocMotionDof;

    % Take the constant (over time and iteration) mass matrix (not like K,C
    % that can be constant over iter but not along time).
    Mk = Solution.M_Opti;

    for k = 1:npts
        parametersRes.timestep = Solution.timesteps(k);
        parametersRes.scaling = Solution.parameters.scaling;

        % Motion constraints
        startMotionLin = (k-1)*(sizeMotionConst+sizeIntregrationConst)+1;
        startIntegrationLin = startMotionLin + sizeMotionConst;
        startCol = (k-1)*Solution.nVar + 1;

        % Motion Constraint Value
        Solution.model.assembleResidueDynamic(parametersRes,1);

        ceq(startMotionLin:startMotionLin+sizeMotionConst-1) = Solution.model.res;

        % Motion Constraint Gradient
        if Solution.GradConstCalc <= Solution.nPass
            % Definition of constant stiffness and damping matrics along
            % time if not computed at least once
            K = Solution.model.getTangentMatrix('Stiffness',Solution.timesteps(k),1);
            Solution.K_Opti(:,:,k) = K(LocMotionDof,LocMotionDof);
            C = Solution.model.getTangentMatrix('Damping',Solution.timesteps(k),1);
            Solution.C_Opti(:,:,k) = C(LocMotionDof,LocMotionDof);
        end
        Kk = Solution.K_Opti(:,:,k);
        Ck = Solution.C_Opti(:,:,k);
        Bk = Solution.model.getTangentMatrix('Phiq',Solution.timesteps(k),1);
        Ak = Bk(LocMotionDof,Solution.listControls);
        dServodqk = Bk(Solution.listControls,LocMotionDof);
        Bk = Bk(Solution.listLagrangeM,LocMotionDof);

        gradceq(startCol:startCol+Solution.nVar-1, startMotionLin:startMotionLin+sizeMotionConst-1) = [Kk' Bk' dServodqk';...
                                                                                                       Ck' zeros(Solution.nCoord, Solution.nLagrangeM+Solution.nControls);...
                                                                                                       Mk' zeros(Solution.nCoord, Solution.nLagrangeM+Solution.nControls);...
                                                                                                       zeros(Solution.nCoord) zeros(Solution.nCoord, Solution.nLagrangeM+Solution.nControls);...
                                                                                                       Bk  zeros(Solution.nLagrangeM, Solution.nLagrangeM+Solution.nControls);...
                                                                                                       Ak' zeros(Solution.nControls, Solution.nLagrangeM+Solution.nControls)];
        % Integration constraints       
        if k ~= npts % if not at the last time step
            h = Solution.parameters.finaltime/(npts-1);

            TimeDeltaQn = zeros(Solution.nCoord,1);
            vn = zeros(Solution.nCoord,1);
            vn1 = zeros(Solution.nCoord,1);
            vdotn = zeros(Solution.nCoord,1);
            vdotn1 = zeros(Solution.nCoord,1);

            count = 0; % variable to select the bloc of 6 values corresponding to a node or 1 value corresponding to a relative node
            for i = Solution.model.listNodes
                if isempty(find(Solution.model.listFixedNodes==i{1}.numberNode,1))
                    xn = i{1}.position(:,Solution.timesteps(k));
                    xn1 = i{1}.position(:,Solution.timesteps(k+1));
                    RnT = vect9ToMat3(i{1}.R(:,Solution.timesteps(k)))';
                    Rn1 = vect9ToMat3(i{1}.R(:,Solution.timesteps(k+1)));
                    qnm1 = [RnT RnT*(-xn); 0 0 0 1];
                    qn1 = [Rn1 xn1; 0 0 0 1];

                    count = count+6;             
                    TimeDeltaQn((count-5):count) = logSE3(qnm1*qn1); % allocating the variables at the right place
                    vn((count-5):count) = i{1}.velocity(:,Solution.timesteps(k));
                    vn1((count-5):count) = i{1}.velocity(:,Solution.timesteps(k+1));
                    vdotn((count-5):count) = i{1}.acceleration(:,Solution.timesteps(k));
                    vdotn1((count-5):count) = i{1}.acceleration(:,Solution.timesteps(k+1));
                end            
            end
            for i = Solution.model.listElementVariables
                if strcmp(i{1}.DofType,'MotionDof')
                    xn = i{1}.x(:,Solution.timesteps(k));
                    xn1 = i{1}.x(:,Solution.timesteps(k+1));
                    RnT = vect9ToMat3(i{1}.R(:,Solution.timesteps(k)))';
                    Rn1 = vect9ToMat3(i{1}.R(:,Solution.timesteps(k+1)));
                    qnm1 = [RnT RnT*(-xn); 0 0 0 1];
                    qn1 = [Rn1 xn1; 0 0 0 1];

                    count = count+i{1}.nDof;             
                    TimeDeltaQn((count-(i{1}.nDof-1)):count) = i{1}.A*logSE3(qnm1*qn1); % allocating the variables at the right place)
                    vn((count-(i{1}.nDof-1)):count) = i{1}.velocity(:,Solution.timesteps(k));
                    vn1((count-(i{1}.nDof-1)):count) = i{1}.velocity(:,Solution.timesteps(k+1));
                    vdotn((count-(i{1}.nDof-1)):count) = i{1}.acceleration(:,Solution.timesteps(k));
                    vdotn1((count-(i{1}.nDof-1)):count) = i{1}.acceleration(:,Solution.timesteps(k+1));
                end
            end

            accn = x((k-1)*Solution.nVar+3*Solution.nCoord+1 : (k-1)*Solution.nVar+4*Solution.nCoord);
            accn1 = x(k*Solution.nVar+3*Solution.nCoord+1 : k*Solution.nVar+4*Solution.nCoord);

            if Solution.linConst
                IntegrationEqu1 = TimeDeltaQn - h*vn - (0.5 - Solution.parameters.beta)*h^2*accn - Solution.parameters.beta*h^2*accn1; 
                % Integration Constraint Value
                ceq(startIntegrationLin:startIntegrationLin+Solution.nCoord-1) = [IntegrationEqu1];

                % Integration Constraint Gradients
                T = -eye(Solution.nCoord);
                T1 = eye(Solution.nCoord);

                gradceq((k-1)*Solution.nVar+1:k*Solution.nVar, startIntegrationLin:startIntegrationLin+Solution.nCoord-1) = [T; -h*eye(Solution.nCoord); zeros(Solution.nCoord); -(0.5-Solution.parameters.beta)*h^2*eye(Solution.nCoord); zeros(Solution.nLagrangeM+Solution.nControls, Solution.nCoord)];
                gradceq(k*Solution.nVar+1:(k+1)*Solution.nVar, startIntegrationLin:startIntegrationLin+Solution.nCoord-1) = [T1; zeros(Solution.nCoord); zeros(Solution.nCoord); -Solution.parameters.beta*h^2*eye(Solution.nCoord); zeros(Solution.nLagrangeM+Solution.nControls, Solution.nCoord)];

            else
                IntegrationEqu1 = TimeDeltaQn - h*vn - (0.5 - Solution.parameters.beta)*h^2*accn - Solution.parameters.beta*h^2*accn1;
                IntegrationEqu2 = vn1 - vn - (1-Solution.parameters.gamma)*h*accn - Solution.parameters.gamma*h*accn1;
                IntegrationEqu3 = (1 - Solution.parameters.alpha_m)*accn1 + Solution.parameters.alpha_m*accn - (1 - Solution.parameters.alpha_f)*vdotn1 - Solution.parameters.alpha_f*vdotn;   
                % Integration Constraint Value
                ceq(startIntegrationLin:startIntegrationLin+sizeIntregrationConst-1) = [IntegrationEqu1;...
                                                                                      IntegrationEqu2;...
                                                                                      IntegrationEqu3];
                % Integration Constraint Gradients                                                                         
                T = -eye(Solution.nCoord);
                T1 = eye(Solution.nCoord);

                gradceq((k-1)*Solution.nVar+1:k*Solution.nVar, startIntegrationLin:startIntegrationLin+sizeIntregrationConst-1) = [T                               zeros(Solution.nCoord)         zeros(Solution.nCoord);...
                                                                                                                                  -h*eye(Solution.nCoord)          -eye(Solution.nCoord)          zeros(Solution.nCoord);...
                                                                                                                                  zeros(Solution.nCoord)           zeros(Solution.nCoord)        -Solution.parameters.alpha_f*eye(Solution.nCoord);...
                                                                                                                      -(0.5-Solution.parameters.beta)*h^2*eye(Solution.nCoord) -(1-Solution.parameters.gamma)*h*eye(Solution.nCoord) Solution.parameters.alpha_m*eye(Solution.nCoord);...
                                                                                                                     zeros(Solution.nLagrangeM+Solution.nControls, Solution.nCoord)  zeros(Solution.nLagrangeM+Solution.nControls, Solution.nCoord) zeros(Solution.nLagrangeM+Solution.nControls, Solution.nCoord)];
                gradceq(k*Solution.nVar+1:(k+1)*Solution.nVar, startIntegrationLin:startIntegrationLin+sizeIntregrationConst-1) = [T1                              zeros(Solution.nCoord)         zeros(Solution.nCoord);...
                                                                                                                              zeros(Solution.nCoord)               eye(Solution.nCoord)           zeros(Solution.nCoord);...
                                                                                                                              zeros(Solution.nCoord)               zeros(Solution.nCoord)    -(1-Solution.parameters.alpha_f)*eye(Solution.nCoord);...
                                                                                                                   -Solution.parameters.beta*h^2*eye(Solution.nCoord) -Solution.parameters.gamma*h*eye(Solution.nCoord) (1-Solution.parameters.alpha_m)*eye(Solution.nCoord);...
                                                                                                           zeros(Solution.nLagrangeM+Solution.nControls,Solution.nCoord) zeros(Solution.nLagrangeM+Solution.nControls,Solution.nCoord) zeros(Solution.nLagrangeM+Solution.nControls,Solution.nCoord)];
            end
        end        
    end

    if Solution.GradConstCalc <= Solution.nPass
        Solution.GradConstCalc = Solution.GradConstCalc+1; % increment the number of time the Gradient of constraint were computed.
        disp('Matrices were updated...')
    end

end
