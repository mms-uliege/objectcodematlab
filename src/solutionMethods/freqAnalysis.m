%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function analys = freqAnalysis(S,st,ploting)
    % analys = 
    % freqAnalysis(S)
    % 
    % Computes the eigenvalues of the model stored in S.model. 
    %
    %  INPUTS:S
    %         - S    = structure containing all data of the model
    %         - st = time st at which to frequencies

    %  OUTPUTS:
    %          - analys          = vector of eigenvalues at time "st" 

    % Make list of numbers of commands and lagrange multipliers
    listControls = [];
    listLagrangeM = [];
    for nElmVar = 1:size(S.model.listElementVariables,2)
        if strcmp(S.model.listElementVariables{nElmVar}.DofType,'Command')
            listControls = [listControls S.model.listElementVariables{nElmVar}.listNumberDof];
        elseif strcmp(S.model.listElementVariables{nElmVar}.DofType,'LagrangeMultiplier')
            listLagrangeM = [listLagrangeM S.model.listElementVariables{nElmVar}.listNumberDof];
        end
    end
    nLagrangeM = length(listLagrangeM);
    nControls = length(listControls);
    nCoord = length(S.model.listFreeDof) - nLagrangeM - nControls;

    K = S.model.getTangentMatrix('Stiffness',st, 0);
    K = K(1:nCoord,1:nCoord);
    M = S.model.getTangentMatrix('Mass',st, 0);
    M = M(1:nCoord,1:nCoord);
    C = S.model.getTangentMatrix('Damping',st, 0);
    C = C(1:nCoord,1:nCoord);

    B = S.model.getTangentMatrix('Phiq',st, 0);
    B = B(nCoord+1:nCoord+nLagrangeM,1:nCoord);

    E = zeros(2*nCoord+nLagrangeM,2*nCoord+nLagrangeM);
    As = zeros(2*nCoord+nLagrangeM,2*nCoord+nLagrangeM);

    E(1:nCoord,1:nCoord) = eye(nCoord);
    E(nCoord+1:2*nCoord,nCoord+1:2*nCoord) = M;

    As(1:nCoord,nCoord+1:2*nCoord) = eye(nCoord);
    As(nCoord+1:2*nCoord,1:nCoord) = -K;
    As(nCoord+1:2*nCoord,nCoord+1:2*nCoord) = -C;
    As(nCoord+1:2*nCoord,2*nCoord+1:2*nCoord+nLagrangeM) = -B';
    As(2*nCoord+1:2*nCoord+nLagrangeM,1:nCoord) = B;

    [V,D] = eig (As,E);
    freq = sort(diag(D));

    epsilon = 1.e-6;
    largeNumber = 1.e20;
    analys.indexUnstable = find((abs(freq)<largeNumber) & real(freq) > epsilon);
    analys.indexStable = find((abs(freq)<largeNumber) & real(freq) <= epsilon);
    analys.ns = length(analys.indexStable); % number of stable freq
    analys.nu = length(analys.indexUnstable); % number of stable freq

    toU = analys.nu;
    toS = analys.ns;
    if ploting
        figure
        hold on
        plot(real(freq(analys.indexUnstable)),imag(freq(analys.indexUnstable)),'d','MarkerFaceColor','r','MarkerEdgeColor','r')
        plot(real(freq(analys.indexStable)),imag(freq(analys.indexStable)),'o','MarkerFaceColor','b','MarkerEdgeColor','b')
        xlabel('Real part','Fontsize',16)
        ylabel('Imaginary part','Fontsize',16)
        legend('Unstable','Stable', 'Location', 'Best')
        title('Poles of the system','Fontsize',18)
        grid on
    end

    analys.freq = freq;
    analys.sPoles = freq(analys.indexStable);
    analys.usPoles = freq(analys.indexUnstable);
    analys.sNatPulse = abs(freq(analys.indexStable));
    analys.usNatPulse = abs(freq(analys.indexUnstable));
    analys.sNatFreq = abs(freq(analys.indexStable))/(2*pi);
    analys.usNatFreq = abs(freq(analys.indexUnstable))/(2*pi);
    analys.sLowPulse = min(abs(freq(analys.indexStable)));
    analys.usLowPulse = min(abs(freq(analys.indexUnstable)));
    analys.sLowFreq = analys.sLowPulse/(2*pi);
    analys.usLowFreq = analys.usLowPulse/(2*pi);
    analys.Vs = V(:,analys.indexStable);
    analys.Vu = V(:,analys.indexUnstable);

end