%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef StaticIntegration < handle
    
    properties
        model;
        parameters;
    end
    
    methods
        function obj = StaticIntegration(model)
            obj.model = model;
            obj.parameters = StaticIntegrationParameters();
        end
        
        function initializeStaticIntegration(obj)
            obj.parameters.InitialComputation();
            for i = obj.model.listNumberDof
                obj.model.listDof{i}.initializeStatic(obj.parameters.nstep);
            end
            for i = obj.model.listNumberNodes
                obj.model.listNodes{i}.initializeStatic(obj.parameters.nstep);
            end
            for i = obj.model.listNumberElementVariables
                obj.model.listElementVariables{i}.initializeStatic(obj.parameters.nstep);
            end
            for i = obj.model.listNumberElements
                obj.model.listElements{i}.initializeStatic(obj.parameters.nstep);
            end
        end
        
        function condition = checkConvergence(obj)
            conditionRes = norm(obj.model.res(1 : obj.model.numberMotionDof)) ...
                > obj.parameters.relTolRes * (obj.parameters.absTol + obj.model.refValForce);
            if length(obj.model.res) - obj.model.numberMotionDof > 0
                conditionPhi = norm(obj.model.res(obj.model.numberMotionDof + 1 : end)) ...
                    > obj.parameters.relTolPhi * (obj.parameters.absTol + obj.model.refValPhi);
                condition = conditionRes || conditionPhi;
            else
                condition = conditionRes;
            end
        end

        function runIntegration(obj)
            
            obj.initializeStaticIntegration();
            
            obj.model.computeElementsTermsStatic(obj.parameters.stepParameters);
            obj.model.assembleTangentOperatorStatic(obj.parameters.stepParameters);
            IndLine = checkIndLine(obj.model.St);
            
            obj.model.setNumberMotionDof();
            
            for t = 1:obj.parameters.nstep
                
                obj.parameters.stepParameters.timestep = t+1;
                               
                for i = obj.model.listNumberDof
                    obj.model.listDof{i}.staticPrediction();
                end
                for i = obj.model.listNumberNodes
                    obj.model.listNodes{i}.updateStatic(obj.parameters.stepParameters.timestep);
                end
                for i = obj.model.listNumberElementVariables
                    obj.model.listElementVariables{i}.updateStatic(obj.parameters.stepParameters.timestep);
                end
                
                obj.parameters.stepParameters.currentTime = obj.parameters.time(obj.parameters.stepParameters.timestep);
                obj.model.computeElementsTermsStatic(obj.parameters.stepParameters);
                obj.model.assembleResidueStatic(obj.parameters.stepParameters);
                
                nit = 0;
                
                while obj.checkConvergence() && nit < obj.parameters.itMax
                    nit = nit+1;
                    obj.model.assembleTangentOperatorStatic(obj.parameters.stepParameters);
                    
                    dq = zeros(obj.model.nDof,1);
                    dq(obj.model.listFreeDof) = (obj.model.St(IndLine,:))\(-obj.model.res(IndLine));
                    
                    for i = obj.model.listNumberDof
                        obj.model.listDof{i}.staticCorrection(obj.parameters.stepParameters,dq(obj.model.listDof{i}.numDof));
                    end
                    for i = obj.model.listNumberNodes
                        obj.model.listNodes{i}.updateStatic(obj.parameters.stepParameters.timestep);
                    end
                    for i = obj.model.listNumberElementVariables
                        obj.model.listElementVariables{i}.updateStatic(obj.parameters.stepParameters.timestep);
                    end
                    obj.model.computeElementsTermsStatic(obj.parameters.stepParameters);
                    obj.model.assembleResidueStatic(obj.parameters.stepParameters);
                end
                if nit == obj.parameters.itMax
                    disp(['no convergence at step ' num2str(t)])
                end
            end
            
        end
    end
    
end

