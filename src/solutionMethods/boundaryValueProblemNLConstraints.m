%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [c, ceq, gradc, gradceq] = boundaryValueProblemNLConstraints(x, Solution)
    %  - Compute nonlinear equalities and inequalities constraints of the BVP problem, and its gradients by semi-analytical method
    %  
    %  INPUTS:
    %         - x          = vector of design variables
    %         - Solution    = structure containing all data of the model (of
    %         class DirectTranscriptionOpti

    %  OUTPUTS:
    %          - c          = vector of nonlinear inequalities constraints at x
    %          - ceq        = vector of nonlinear equalities constraints at x
    %          - gradc      = gradients of the nonlinear inequalities constraints at x
    %          - gradceqp   = gradients of the nonlinear equalities constraints at x
  
    c = [];            %  absence of inequality constraints 
    gradc = [];        %  absence of inequality constraints 

    npts = Solution.npts;

    sizeMotionConst = Solution.nCoord + Solution.nLagrangeM + Solution.nControls;
    sizeIntregrationConst = 3*Solution.nCoord;
    sizeVelConstraint = 2*(Solution.nLagrangeM + Solution.nControls);
    sizeBV = 2*(Solution.nCoord - Solution.nLagrangeM - Solution.nControls);

    ceq = sparse(npts*(sizeMotionConst + sizeIntregrationConst), 1);

    gradceq = sparse(npts*(sizeMotionConst + sizeIntregrationConst),Solution.nVar*npts);

    % Update all variables with current "state" vector x
    % Solution.Update(x)

    LocMotionDof = Solution.LocMotionDof;

    Mk = Solution.M_Opti;

    %% Additional equation to satisfy initial and final kinematic constraints at velocity level
    velConst_lineIndex = npts*(sizeMotionConst+sizeIntregrationConst)-sizeIntregrationConst+1:npts*(sizeMotionConst+sizeIntregrationConst)-sizeIntregrationConst+sizeVelConstraint;
    velConst_colIndex = [Solution.nCoord+1:Solution.nCoord+Solution.nCoord (npts-1)*(4*Solution.nCoord+Solution.nLagrangeM+Solution.nControls)+Solution.nCoord+1:(npts-1)*(4*Solution.nCoord+Solution.nLagrangeM+Solution.nControls)+2*Solution.nCoord];
    [gradceq_unstable,~,~,velConst_init] = BoundaryValues(Solution,1,'unstable');
    [gradceq_stable,~,~,velConst_fin] = BoundaryValues(Solution,Solution.npts,'stable');
    ceq(velConst_lineIndex) = zeros(sizeVelConstraint,1);

    gradceq(velConst_lineIndex,velConst_colIndex) = [velConst_init zeros(Solution.nLagrangeM+Solution.nControls,Solution.nCoord);...
                                                     zeros(Solution.nLagrangeM+Solution.nControls,Solution.nCoord) velConst_fin];

    %% Boundary values specified at initial and final state of the system
    BV_lineIndex = npts*(sizeMotionConst+sizeIntregrationConst)-sizeIntregrationConst+sizeVelConstraint+1:npts*(sizeMotionConst+sizeIntregrationConst)-sizeIntregrationConst+sizeVelConstraint+sizeBV;
    BV_colIndex = [1:2*Solution.nCoord (npts-1)*(4*Solution.nCoord+Solution.nLagrangeM+Solution.nControls)+1:(npts-1)*(4*Solution.nCoord+Solution.nLagrangeM+Solution.nControls)+2*Solution.nCoord];
    ceq(BV_lineIndex) = zeros(sizeBV,1);

    gradceq(BV_lineIndex,BV_colIndex) = [gradceq_unstable zeros(size(gradceq_unstable,1),2*Solution.nCoord);...
                                         zeros(size(gradceq_stable,1),2*Solution.nCoord) gradceq_stable];

    %% Motion and integration equations
    for k = 1:npts
        parametersRes.timestep = Solution.timesteps(k);
        parametersRes.scaling = Solution.parameters.scaling;

        % Motion constraints
        startMotionLin = (k-1)*(sizeMotionConst+sizeIntregrationConst)+1;
        startIntegrationLin = startMotionLin + sizeMotionConst;
        startCol = (k-1)*Solution.nVar + 1;

        % Motion Constraint Value
        Solution.model.assembleResidueDynamic(parametersRes,1); % when both constraint and
        ceq(startMotionLin:startMotionLin+sizeMotionConst-1) = Solution.model.res;

        % Motion Constraint Gradient
        Kk = Solution.model.getTangentMatrix('Stiffness',Solution.timesteps(k),1);
        Kk = Kk(LocMotionDof,LocMotionDof);
        Ck = Solution.model.getTangentMatrix('Damping',Solution.timesteps(k),1);
        Ck = Ck(LocMotionDof,LocMotionDof);


        Bk = Solution.model.getTangentMatrix('Phiq',Solution.timesteps(k),1);
        Ak = Bk(LocMotionDof,Solution.listControls);
        dServodqk = Bk(Solution.listControls,LocMotionDof);
        Bk = Bk(Solution.listLagrangeM,LocMotionDof);

        gradceq(startMotionLin:startMotionLin+sizeMotionConst-1, startCol:startCol+Solution.nVar-1) = [Kk Ck Mk zeros(Solution.nCoord) Bk' Ak;...
                                                                                                Bk zeros(Solution.nLagrangeM, Solution.nVar-Solution.nCoord);...
                                                                                                dServodqk zeros(Solution.nControls, Solution.nVar-Solution.nCoord)];
        % Integration constraints   
        if k ~= npts
            h = Solution.parameters.finaltime/(npts-1);

            TimeDeltaQn = zeros(Solution.nCoord,1);
            vn = zeros(Solution.nCoord,1);
            vn1 = zeros(Solution.nCoord,1);
            vdotn = zeros(Solution.nCoord,1);
            vdotn1 = zeros(Solution.nCoord,1);

            count = 0; % variable to select the bloc of 6 values corresponding to a node or 1 value corresponding to a relative node
            for i = Solution.model.listNodes
                if isempty(find(Solution.model.listFixedNodes==i{1}.numberNode,1))
                    xn = i{1}.position(:,Solution.timesteps(k));
                    xn1 = i{1}.position(:,Solution.timesteps(k+1));
                    RnT = vect9ToMat3(i{1}.R(:,Solution.timesteps(k)))';
                    Rn1 = vect9ToMat3(i{1}.R(:,Solution.timesteps(k+1)));
                    qnm1 = [RnT RnT*(-xn); 0 0 0 1];
                    qn1 = [Rn1 xn1; 0 0 0 1];

                    count = count+6;             
                    TimeDeltaQn(count-5:count) = logSE3(qnm1*qn1); % allocating the variables at the right place
                    vn(count-5:count) = i{1}.velocity(:,Solution.timesteps(k));
                    vn1(count-5:count) = i{1}.velocity(:,Solution.timesteps(k+1));
                    vdotn(count-5:count) = i{1}.acceleration(:,Solution.timesteps(k));
                    vdotn1(count-5:count) = i{1}.acceleration(:,Solution.timesteps(k+1));
                end            
            end
            for i = Solution.model.listElementVariables
                if strcmp(i{1}.DofType,'MotionDof')
                    xn = i{1}.x(:,Solution.timesteps(k));
                    xn1 = i{1}.x(:,Solution.timesteps(k+1));
                    RnT = vect9ToMat3(i{1}.R(:,Solution.timesteps(k)))';
                    Rn1 = vect9ToMat3(i{1}.R(:,Solution.timesteps(k+1)));
                    qnm1 = [RnT RnT*(-xn); 0 0 0 1];
                    qn1 = [Rn1 xn1; 0 0 0 1];

                    count = count+i{1}.nDof;             
                    TimeDeltaQn(count-(i{1}.nDof-1):count) = i{1}.A*logSE3(qnm1*qn1); % allocating the variables at the right place)
                    vn(count-(i{1}.nDof-1):count) = i{1}.velocity(:,Solution.timesteps(k));
                    vn1(count-(i{1}.nDof-1):count) = i{1}.velocity(:,Solution.timesteps(k+1));
                    vdotn(count-(i{1}.nDof-1):count) = i{1}.acceleration(:,Solution.timesteps(k));
                    vdotn1(count-(i{1}.nDof-1):count) = i{1}.acceleration(:,Solution.timesteps(k+1));
                end
            end

            accn = x((k-1)*Solution.nVar+3*Solution.nCoord+1 : (k-1)*Solution.nVar+4*Solution.nCoord);
            accn1 = x(k*Solution.nVar+3*Solution.nCoord+1 : k*Solution.nVar+4*Solution.nCoord);


            IntegrationEqu1 = TimeDeltaQn - h*vn - (0.5 - Solution.parameters.beta)*h^2*accn - Solution.parameters.beta*h^2*accn1;
            IntegrationEqu2 = vn1 - vn - (1-Solution.parameters.gamma)*h*accn - Solution.parameters.gamma*h*accn1;
            IntegrationEqu3 = (1 - Solution.parameters.alpha_m)*accn1 + Solution.parameters.alpha_m*accn - (1 - Solution.parameters.alpha_f)*vdotn1 - Solution.parameters.alpha_f*vdotn;   
            % Integration Constraint Value
            ceq(startIntegrationLin:startIntegrationLin+sizeIntregrationConst-1) = [IntegrationEqu1;...
                                                                                  IntegrationEqu2;...
                                                                                  IntegrationEqu3];
            % Integration Constraint Gradients                                                                         
            T = -eye(Solution.nCoord); 
            T1 = eye(Solution.nCoord);
            gradceq(startIntegrationLin:startIntegrationLin+sizeIntregrationConst-1,(k-1)*Solution.nVar+1:k*Solution.nVar) = [T -h*eye(Solution.nCoord) zeros(Solution.nCoord) -(0.5-Solution.parameters.beta)*h^2*eye(Solution.nCoord) zeros(Solution.nCoord,Solution.nLagrangeM+Solution.nControls);...
                                                                                                                              zeros(Solution.nCoord) -eye(Solution.nCoord) zeros(Solution.nCoord) -(1-Solution.parameters.gamma)*h*eye(Solution.nCoord) zeros(Solution.nCoord,Solution.nLagrangeM+Solution.nControls);...
                                                                                                                              zeros(Solution.nCoord) zeros(Solution.nCoord) -Solution.parameters.alpha_f*eye(Solution.nCoord) Solution.parameters.alpha_m*eye(Solution.nCoord) zeros(Solution.nCoord,Solution.nLagrangeM+Solution.nControls)];
            gradceq(startIntegrationLin:startIntegrationLin+sizeIntregrationConst-1,k*Solution.nVar+1:(k+1)*Solution.nVar) = [T1 zeros(Solution.nCoord) zeros(Solution.nCoord) -Solution.parameters.beta*h^2*eye(Solution.nCoord) zeros(Solution.nCoord,Solution.nLagrangeM+Solution.nControls);...
                                                                                                                          zeros(Solution.nCoord) eye(Solution.nCoord) zeros(Solution.nCoord) -Solution.parameters.gamma*h*eye(Solution.nCoord) zeros(Solution.nCoord,Solution.nLagrangeM+Solution.nControls);...
                                                                                                                          zeros(Solution.nCoord) zeros(Solution.nCoord) -(1-Solution.parameters.alpha_f)*eye(Solution.nCoord) (1-Solution.parameters.alpha_m)*eye(Solution.nCoord) zeros(Solution.nCoord,Solution.nLagrangeM+Solution.nControls)];

            end        
    end
end
