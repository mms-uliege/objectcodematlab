%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef DirectTranscriptionOpti < handle
    % DirectTranscription optimization solver object that is able to solve
    % dynamic multibody system using an optimization method.
    
    properties
        NodeToMinimize = [];
        JointToMinimize = [];
        model;
        smodel;
        parameters;                     %integrationParameters;
        npts;
        listFlexBeamElem;       % list of element number that are FlexibleBeamElements (for objFunction minimal Energy
        nVar;                   % Number of variable per time step (total number of variable = nVar*npts)
        listLagrangeM;          % list of dof corresponding to Lagrange M
        nControls;              % number of control variables
        nCoord;                 % number of "motion dof"
        listControls;           % list of control elements
        nLagrangeM;             % number of lagrange multipliers
        timeValues = [];
        timesteps = [];
        K_Opti; % Constant matrices to see advantage of local frame approach
        M_Opti; % Constant matrices to see advantage of local frame approach
        C_Opti; % Constant matrices to see advantage of local frame approach
        LocMotionDof;
        linConst = false; % Consider linear constraints
        jointColors; % Colors for plots during optimization process 
        commandColors;
        ConstIter = 'notConstant'; % says if we consider the iteration matrix as constant over the optimization or not.
        GradConstCalc = 0; % counts the number of times the constraint grad have been calculated (when considering const matrices in gradient computation).
        nPass = 0; % number of time the gradients have to be computed before considering constatn
        Traj; % Structure containing trajectory information .A ([1 0 0 0 0 0; 0 1 0 0 0 0] for x,y for example) and .T (corresponding trajectory over time). 
    end
    
    methods
        function obj = DirectTranscriptionOpti(varargin)
            obj.model = varargin{1};
            obj.parameters = DynamicIntegrationParameters();
            
            % Make list of numbers of commands and lagrange multipliers
            obj.listControls = [];
            obj.listLagrangeM = [];
            for nElmVar = 1:size(obj.model.listElementVariables,2)
                if strcmp(obj.model.listElementVariables{nElmVar}.DofType,'Command')
                    obj.listControls = [obj.listControls obj.model.listElementVariables{nElmVar}.listNumberDof];
                elseif strcmp(obj.model.listElementVariables{nElmVar}.DofType,'LagrangeMultiplier')
                    obj.listLagrangeM = [obj.listLagrangeM obj.model.listElementVariables{nElmVar}.listNumberDof];
                end
            end
            obj.nLagrangeM = length(obj.listLagrangeM);
            obj.nControls = length(obj.listControls);
            obj.nCoord = length(obj.model.listFreeDof) - obj.nLagrangeM - obj.nControls;
            obj.nVar = 4*obj.nCoord + obj.nLagrangeM + obj.nControls; % N variable for one timestep
            
            % definition of the joint colors ([R G B]) to plot joint optimisation
            % process (used in the NL constraitns)
            nJoint = 0;
            for i = obj.model.listElementVariables
                if strcmp(i{1}.DofType,'MotionDof')
                    nJoint = nJoint+1;
                end
            end
            obj.jointColors = rand(nJoint,3);   
            obj.commandColors = rand(obj.nControls,3);
        end
        
        function state = runOpti(obj,DynamicIntegrationSol)
            obj.parameters.InitialComputation();
            
            for i = obj.model.listNumberDof
                obj.model.listDof{i}.initializeDynamic(DynamicIntegrationSol.parameters.nstep);
            end

            for i = obj.model.listNumberElementVariables
                obj.model.listElementVariables{i}.initializeDynamic(DynamicIntegrationSol.parameters.nstep);
            end
            
            for i = obj.model.listNumberElements
                obj.model.listElements{i}.initializeDynamic(DynamicIntegrationSol.parameters.nstep);
            end
            
            % Selection of the discrete time steps according to the number of npts
            obj.timeValues = zeros(1,obj.npts);
            obj.timesteps = zeros(1,obj.npts);
            to = 1e-6;
            for k = 1:obj.npts
                obj.timeValues(k) = (k-1)*obj.parameters.finaltime/(obj.npts-1);
                obj.timesteps(k) = find(and(DynamicIntegrationSol.parameters.time>=obj.timeValues(k)-to,DynamicIntegrationSol.parameters.time<=obj.timeValues(k)+to),1);
            end
            
            obj.LocMotionDof = obj.model.listFreeDof(1:obj.nCoord);
            
            % Saves the constant (over time and iteration) mass matrix (not like K,C
            % that are can be constant over iter but not along time).
            M = obj.model.getTangentMatrix('Mass',obj.timesteps(1),1);
            obj.M_Opti = M(obj.LocMotionDof,obj.LocMotionDof);
            
            % Copying every trajectory information into a vector that has
            % the same size in the first guess and in the optimization
            % (inserting 0s between trajectory values)
            for nE = 1:length(obj.model.listElements)
                if isa(obj.model.listElements{nE},'TrajectoryConstraint')
                    T = zeros(size(obj.model.listElements{nE}.T,1),length(DynamicIntegrationSol.parameters.time));
                    T(:,obj.timesteps) = obj.model.listElements{nE}.T(:,1:length(obj.timesteps));
                    obj.model.listElements{nE}.T = T;
                end
            end
            
            % Making a list of flexible elements that will be needed in the
            % objective function.
            obj.listFlexBeamElem = [];
            for nE = 1:length(obj.model.listElements)
                if isa(obj.model.listElements{nE},'FlexibleBeamElement')
                    obj.listFlexBeamElem = [obj.listFlexBeamElem nE];
                end
            end
            
            % Initializing boundaries for variables
            Lb = -inf*ones(obj.nVar*obj.npts,1);
            Ub = inf*ones(obj.nVar*obj.npts,1);          
            
            % Creating initial guess values
            x_0 = zeros(obj.nVar*obj.npts,1);
            nfixDof = length(obj.model.listFixedDof);
            for k = 1:obj.npts
                for var = obj.model.listNodes
                    if isempty(find(obj.model.listFixedNodes==var{1}.numberNode))
                        locV = obj.nCoord + (k-1)*obj.nVar + (var{1}.listNumberDof - nfixDof);
                        locVdot = 2*obj.nCoord + (k-1)*obj.nVar + (var{1}.listNumberDof - nfixDof);
                        locA = 3*obj.nCoord + (k-1)*obj.nVar + (var{1}.listNumberDof - nfixDof);
                        x_0(locV) = var{1}.velocity_InitOpti(:,obj.timesteps(k));
                        x_0(locVdot) = var{1}.acceleration_InitOpti(:,obj.timesteps(k));
                        x_0(locA) = var{1}.acceleration_InitOpti(:,obj.timesteps(k));
                    end
                end
                for var = obj.model.listElementVariables
                    if strcmp(var{1}.DofType,'MotionDof')
                        % restrain joint relative dof to change too much
                        % (might be needed to prevent optimization to
                        % "flip" the system with a great angle.
                        locV = obj.nCoord + (k-1)*obj.nVar + (var{1}.listNumberDof - nfixDof);
                        locVdot = 2*obj.nCoord + (k-1)*obj.nVar + (var{1}.listNumberDof - nfixDof);
                        locA = 3*obj.nCoord + (k-1)*obj.nVar + (var{1}.listNumberDof - nfixDof);
                        x_0(locV) = var{1}.velocity_InitOpti(:,obj.timesteps(k));
                        x_0(locVdot) = var{1}.acceleration_InitOpti(:,obj.timesteps(k));
                        x_0(locA) = var{1}.acceleration_InitOpti(:,obj.timesteps(k));
                    else
                        locValue = 3*obj.nCoord + (k-1)*obj.nVar + (var{1}.listNumberDof - nfixDof);
                        x_0(locValue) = var{1}.value_InitOpti(:,obj.timesteps(k));
                    end
                end
                
            end
               
            A = [];
            b = [];
            if obj.linConst
                sizeIntregrationConst = 2*obj.nCoord;
                h = obj.parameters.finaltime/(obj.npts-1);
                Aeq = zeros(obj.npts*sizeIntregrationConst, obj.nVar*obj.npts);
                beq = zeros(obj.npts*sizeIntregrationConst,1);
                for k = 1:obj.npts-1
                    startIntegrationLin = (k-1)*sizeIntregrationConst+1;
                    Aeq(startIntegrationLin:startIntegrationLin+sizeIntregrationConst-1,(k-1)*obj.nVar+1:k*obj.nVar) = [zeros(obj.nCoord) -eye(obj.nCoord) zeros(obj.nCoord) -(1-obj.parameters.gamma)*h*eye(obj.nCoord) zeros(obj.nCoord,obj.nLagrangeM+obj.nControls);...
                                                                                                                        zeros(obj.nCoord) zeros(obj.nCoord) -obj.parameters.alpha_f*eye(obj.nCoord) obj.parameters.alpha_m*eye(obj.nCoord) zeros(obj.nCoord,obj.nLagrangeM+obj.nControls)];
                    Aeq(startIntegrationLin:startIntegrationLin+sizeIntregrationConst-1,k*obj.nVar+1:(k+1)*obj.nVar) = [zeros(obj.nCoord) eye(obj.nCoord) zeros(obj.nCoord) -obj.parameters.gamma*h*eye(obj.nCoord) zeros(obj.nCoord,obj.nLagrangeM+obj.nControls);...
                                                                                                                        zeros(obj.nCoord) zeros(obj.nCoord) -(1-obj.parameters.alpha_f)*eye(obj.nCoord) (1-obj.parameters.alpha_m)*eye(obj.nCoord) zeros(obj.nCoord,obj.nLagrangeM+obj.nControls)];
                end
                Aeq = sparse(Aeq);
                beq = sparse(beq);
            else
                Aeq = [];
                beq = [];
            end
            
           options = optimset('Algorithm','interior-point',...
               'Diagnostics','off', ...
               'Display','none', ... 
               'GradConstr','on', ...      
               'GradObj','on', ...
               'InitBarrierParam',0.1,... 
               'MaxFunEvals',100,...
               'MaxIter',30,...
               'Hessian',{'lbfgs',10},...
               'LargeScale','on', ...
               'ScaleProblem', 'none',...
               'SubproblemAlgorithm','cg',...
               'TolCon',obj.parameters.relTolRes, ... % 1e-6 tested up to now with 1e-6
               'TolFun',1e-3, ... % 1e-6 tested up to now with 1e-3
               'TolX',1e-6, ... % 1e-10 tested up to now with 1e-6
               'TolGradCon',1e-6, ... % 1e-6 tested up to now with 1e-6
               'TolPCG',0.01, ...
               'TolProjCG',1e-2, ...
               'TolProjCGAbs',1e-6,... % 1e-10 tested up to now with 1e-6
               'UseParallel','never');

            % Default method, considering that the iteration tangent
            % stiffness matrices are not constant over the iteration
            if strcmp(obj.ConstIter, 'notConstant')
                [state,fval,exitflag,output] = fmincon(@(x)objectiveFuncMinEnergyControl(x, obj), x_0, A, b, Aeq, beq, Lb, Ub, @(x)directTranscriptionOptiNLConstraints(x, obj), options);
            elseif strcmp(obj.ConstIter, 'constant')              
                obj.model.initializeMatrices(obj.parameters);
                [state,fval,exitflag,output] = fmincon(@(x)objectiveFuncMinEnergyControl(x, obj), x_0, A, b, Aeq, beq, Lb, Ub, @(x)directTranscriptionOptiNLConstraintsConst(x, obj), options);
            end
        end
        
        function Update(obj,x)
            
            for k = 1:obj.npts
                
                deltaxk = x((k-1)*obj.nVar+1 : (k-1)*obj.nVar+obj.nCoord);

                deltavxk = x((k-1)*obj.nVar+obj.nCoord+1 : (k-1)*obj.nVar+2*obj.nCoord);

                deltavdotxk = x((k-1)*obj.nVar+2*obj.nCoord+1 : (k-1)*obj.nVar+3*obj.nCoord);

                deltaLambdaxk = x((k-1)*obj.nVar+4*obj.nCoord+1 : (k-1)*obj.nVar+4*obj.nCoord+obj.nLagrangeM);

                deltaControlxk = x((k-1)*obj.nVar+4*obj.nCoord+obj.nLagrangeM+1 : (k-1)*obj.nVar+4*obj.nCoord+obj.nLagrangeM+obj.nControls);

                updateDynamiceltaxk = zeros(obj.model.nDof,1);
                updateDynamiceltaxk(obj.model.listFreeDof) = [deltaxk; deltaLambdaxk; deltaControlxk];

                UpdateV = zeros(obj.model.nDof,1);
                UpdateV(obj.model.listFreeDof) = [deltavxk; zeros(obj.nLagrangeM+obj.nControls,1)];

                UpdateVdot = zeros(obj.model.nDof,1);
                UpdateVdot(obj.model.listFreeDof) = [deltavdotxk; zeros(obj.nLagrangeM+obj.nControls,1)];

                for n = obj.model.listNumberDof % Go through all dof
                    % position updates has to contain the lambda and u updates (arbitrary
                    % choice that have been made)
                    obj.model.listDof{n}.optiCorrection(obj.parameters,obj.timesteps(k),updateDynamiceltaxk(obj.model.listDof{n}.numDof),UpdateV(obj.model.listDof{n}.numDof),UpdateVdot(obj.model.listDof{n}.numDof));
                end
                
                for n = obj.model.listNumberNodes
                    obj.model.listNodes{n}.updateDynamic_Opti(obj.timesteps(k));
                end
                
                for n = obj.model.listNumberElementVariables
                    obj.model.listElementVariables{n}.updateDynamic_Opti(obj.timesteps(k));
                end
            end 
                   
        end
        
        function x_0 = InitOpti(obj,DynamicIntegrationSol)
            obj.parameters.InitialComputation();
            
            for i = obj.model.listNumberDof
                obj.model.listDof{i}.initializeDynamic(DynamicIntegrationSol.parameters.nstep);
            end

            for i = obj.model.listNumberElementVariables
                obj.model.listElementVariables{i}.initializeDynamic(DynamicIntegrationSol.parameters.nstep);
            end
            
            for i = obj.model.listNumberElements
                obj.model.listElements{i}.initializeDynamic(DynamicIntegrationSol.parameters.nstep);
            end
            
            % Selection of the discrete time steps according to the number of npts
            obj.timeValues = zeros(1,obj.npts);
            obj.timesteps = zeros(1,obj.npts);
            for k = 1:obj.npts
                obj.timeValues(k) = (k-1)*obj.parameters.finaltime/(obj.npts-1);
                obj.timesteps(k) = find(DynamicIntegrationSol.parameters.time>=obj.timeValues(k),1);
                if k~= 1 && abs(obj.timeValues(k)-DynamicIntegrationSol.parameters.time(obj.timesteps(k)-1))< 1e-6
                    obj.timesteps(k) = obj.timesteps(k)-1;
                end
            end
            
            obj.LocMotionDof = obj.model.listFreeDof(1:obj.nCoord);
            
            % Saves the constant (over time and iteration) mass matrix (not like K,C
            % that are can be constant over iter but not along time).
            M = obj.model.getTangentMatrix('Mass',obj.timesteps(1),1);
            obj.M_Opti = M(obj.LocMotionDof,obj.LocMotionDof);
            
            % Copying every trajectory information into a vector that has
            % the same size in the first guess and in the optimization
            % (inserting 0s between trajectory values)
            for nE = 1:length(obj.model.listElements)
                if isa(obj.model.listElements{nE},'TrajectoryConstraint')
                    T = zeros(size(obj.model.listElements{nE}.T,1),length(DynamicIntegrationSol.parameters.time));
                    T(:,obj.timesteps) = obj.model.listElements{nE}.T(:,1:length(obj.timesteps));
                    obj.model.listElements{nE}.T = T;
                end
            end
            
            % Making a list of flexible elements that will be needed in the
            % objective function.
            obj.listFlexBeamElem = [];
            for nE = 1:length(obj.model.listElements)
                if isa(obj.model.listElements{nE},'FlexibleBeamElement')
                    obj.listFlexBeamElem = [obj.listFlexBeamElem nE];
                end
            end            
                       
            x_0 = zeros(obj.nVar*obj.npts,1);
            nfixDof = length(obj.model.listFixedDof);
            for k = 1:obj.npts
                for var = obj.model.listNodes
                    if isempty(find(obj.model.listFixedNodes==var{1}.numberNode))
                        locV = obj.nCoord + (k-1)*obj.nVar + (var{1}.listNumberDof - nfixDof);
                        locVdot = 2*obj.nCoord + (k-1)*obj.nVar + (var{1}.listNumberDof - nfixDof);
                        x_0(locV) = var{1}.velocity_InitOpti(:,obj.timesteps(k));
                        x_0(locVdot) = var{1}.acceleration_InitOpti(:,obj.timesteps(k));
                    end
                end
                
                for var = obj.model.listElementVariables
                    if strcmp(var{1}.DofType,'MotionDof')
                        locV = obj.nCoord + (k-1)*obj.nVar + (var{1}.listNumberDof - nfixDof);
                        locVdot = 2*obj.nCoord + (k-1)*obj.nVar + (var{1}.listNumberDof - nfixDof);
                        x_0(locV) = var{1}.velocity_InitOpti(:,obj.timesteps(k));
                        x_0(locVdot) = var{1}.acceleration_InitOpti(:,obj.timesteps(k));
                    else
                        locValue = 3*obj.nCoord + (k-1)*obj.nVar + (var{1}.listNumberDof - nfixDof);
                        x_0(locValue) = var{1}.value_InitOpti(:,obj.timesteps(k));
                    end
                end                
            end
        end        
    end
end