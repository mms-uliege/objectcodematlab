%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef Continuation < handle
    
    properties
        model;
        parameters;%integrationParameters;
    end
    
    methods
        function obj = Continuation(model)
            obj.model = model;
            obj.parameters = StaticIntegrationParameters();
        end
        
        function runIntegration(obj)
            
            obj.parameters.InitialComputation();
            
            v = zeros(obj.model.nDof,obj.parameters.nstep);
            
            for i = obj.model.listNumberDof
                obj.model.listDof{i}.initializeStatic(obj.parameters.nstep);
            end
            
            for i = obj.model.listNumberNodes
                obj.model.listNodes{i}.initializeStatic(obj.parameters.nstep);
            end
            
            for i = obj.model.listNumberElementVariables
                obj.model.listElementVariables{i}.initializeStatic(obj.parameters.nstep);
            end
            numContElem = 0;
            
            for i = obj.model.listNumberElements
                obj.model.listElements{i}.initializeStatic(obj.parameters.nstep);
                if and(numContElem == 0,or(isa(obj.model.listElements{i},'ContinuationElement'),isa(obj.model.listElements{i},'Continuation2DElement')))
                    numContElem = i;
                end
            end
            parametersRes.scaling = obj.parameters.scaling;
            
            obj.parameters.coefSt.timestep = 1;
            obj.model.assembleTangentOperatorStatic(obj.parameters.coefSt);
            listFreeDof = 1:length(obj.model.listFreeDof); % list in St after BC []
            listFreeDof2 = listFreeDof;
            numContDof = find(obj.model.listElements{numContElem}.listElementVariables.continuationVariableSet{1}.listNumberDof == obj.model.listFreeDof);
            listFreeDof2(listFreeDof2 == numContDof) = [];
            
            IndLine = obj.model.listFreeDof; % IndLine = checkIndLine(obj.model.St);
            
            v0 = null(obj.model.St(listFreeDof2,listFreeDof));
            v(IndLine,1) = v0(:,end);
            
            for t = 1:obj.parameters.nstep
                
                disp(['step: ' num2str(t)]);
                
                v(:,t+1) = v(:,t);
                
                for i = obj.model.listNumberDof
                    obj.model.listDof{i}.ContinuationPrediction(t+1,obj.parameters.timestepsize*v(i,t+1));
                end
                
                for i = obj.model.listNumberNodes
                    obj.model.listNodes{i}.updateStatic(t+1);
                end
                
                for i = obj.model.listNumberElementVariables
                    obj.model.listElementVariables{i}.updateStatic(t+1);
                end
                
                parametersRes.time = obj.parameters.time(t+1); parametersRes.timestep = t+1;
                obj.model.assembleResidueStatic(parametersRes);
                
                obj.parameters.coefSt.time = parametersRes.time; obj.parameters.coefSt.timestep = t+1;
                dq = zeros(obj.model.nDof,1);
                nit = 0;
                
                while and(norm(obj.model.res) > (obj.parameters.relTolRes*(1+obj.model.refValForce)), nit <= obj.parameters.itMax)
                    nit = nit+1;
                    obj.model.assembleTangentOperatorStatic(obj.parameters.coefSt);
                    
                    H_x = obj.model.St; H_x(numContDof,:) = v(obj.model.listFreeDof,t+1)';
                    H = obj.model.res;
                    dq(obj.model.listFreeDof) = pinv(H_x)*(-H);
                    
                    for i = obj.model.listNumberDof
                        obj.model.listDof{i}.staticCorrection(obj.parameters,t+1,dq(obj.model.listDof{i}.numDof));
                    end
                    
                    for i = obj.model.listNumberNodes
                        obj.model.listNodes{i}.updateStatic(t+1);
                    end
                    
                    for i = obj.model.listNumberElementVariables
                        obj.model.listElementVariables{i}.updateStatic(t+1);
                    end

                    v(obj.model.listFreeDof,t+1) = v(obj.model.listFreeDof,t+1) - H_x\(obj.model.St*v(obj.model.listFreeDof,t+1));
                    v(obj.model.listFreeDof,t+1) = v(obj.model.listFreeDof,t+1)/norm(v(obj.model.listFreeDof,t+1));
                    
                    obj.model.assembleResidueStatic(parametersRes);
                end
                
                if and(nit > obj.parameters.itMax,norm(obj.model.res) > (obj.parameters.relTolRes*(1+obj.model.refValForce)))
                    disp(['no convergence at step ' num2str(t)])
                end
            end
        end
    end 
end

