%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef DynamicIntegration < handle

    properties
        model;
        smodel;
        parameters;
    end
    
    methods
        function obj = DynamicIntegration(varargin)
             obj.model = varargin{1};
            if size(varargin,2) == 2
                obj.smodel = varargin{2};
            end
            obj.parameters = DynamicIntegrationParameters();
        end
        
        function initializeDynamicIntegration(obj)
            for i = obj.model.listNumberDof
                obj.model.listDof{i}.initializeDynamic(obj.parameters.nstep);
            end
            for i = obj.model.listNumberNodes
                obj.model.listNodes{i}.initializeDynamic(obj.parameters.nstep);
            end
            for i = obj.model.listNumberElementVariables
                obj.model.listElementVariables{i}.initializeDynamic(obj.parameters.nstep);
            end
            for i = obj.model.listNumberElements
                obj.model.listElements{i}.initializeDynamic(obj.parameters.nstep);
            end
        end
        
        function initializeVelocityDynamicIntegration(obj)
            if ~isempty(obj.smodel)
                dqV = zeros(obj.model.nDof,1);
                static = StaticIntegration(obj.smodel);
                static.parameters.finaltime = obj.parameters.timestepsize;
                static.parameters.timestepsize = obj.parameters.timestepsize;
                static.runIntegration();
                for i = obj.model.listNumberDof
                    dqV(obj.model.listDof{i}.numDof) = obj.smodel.listDof{i}.value(end)/obj.parameters.timestepsize;
                end
                clear obj.smodel;
                
                for i = obj.model.listNumberDof
                    dqV(obj.model.listDof{i}.numDof);
                    obj.model.listDof{i}.initializeVelocity(dqV(obj.model.listDof{i}.numDof));
                end
                for i = obj.model.listNumberNodes
                    obj.model.listNodes{i}.initializeVelocity();
                end
                for i = obj.model.listNumberElementVariables
                    obj.model.listElementVariables{i}.initializeVelocity();
                end
            end
        end
        
        function initializeAccelerationDynamicIntegration(obj)           
            dq = zeros(obj.model.nDof,1);
            
            for i = obj.model.listNumberDof
                obj.model.listDof{i}.initializeAcceleration(dq(obj.model.listDof{i}.numDof));
            end
            for i = obj.model.listNumberNodes
                obj.model.listNodes{i}.initializeAcceleration();
            end
            for i = obj.model.listNumberElementVariables
                obj.model.listElementVariables{i}.initializeAcceleration();
            end
        end
        
        function applyInitialConditions(obj, initialCondition)
            for n = 1:size(initialCondition,1)
                obj.model.listDof{initialCondition(n,1)}.initializePosition(initialCondition(n,2));
                obj.model.listDof{initialCondition(n,1)}.initializeVelocity(initialCondition(n,3));
                obj.model.listDof{initialCondition(n,1)}.initializeAcceleration(initialCondition(n,4));
                if isa(obj.model.listDof{initialCondition(n,1)},'LagrangeMultiplier')
                    obj.model.listDof{initialCondition(n,1)}.initializeStatic(0,initialCondition(n,3));
                end
            end
            
            for i = obj.model.listNumberNodes
                obj.model.listNodes{i}.updateDynamic(1);
            end
            for i = obj.model.listNumberElementVariables
                obj.model.listElementVariables{i}.updateDynamic(1);
            end
        end
        
        function condition = checkConvergence(obj)
            conditionRes = norm(obj.model.res(1 : obj.model.numberMotionDof)) ...
                > obj.parameters.relTolRes * (obj.parameters.absTol + obj.model.refValForce);
            if length(obj.model.res) - obj.model.numberMotionDof > 0
                conditionPhi = norm(obj.model.res(obj.model.numberMotionDof + 1 : end)) ...
                    > obj.parameters.relTolPhi * (obj.parameters.absTol + obj.model.refValPhi);
                condition = conditionRes || conditionPhi;
            else
                 condition = conditionRes;
            end
        end
        
        function runIntegration(obj, initialCondition)
            % InitialCondition which must be a
            % matrix of m x 4 which states the m initial values (column 2), m initial velocities (column
            % 3) and initial acceleration (column 4) of the m
            % degrees of freedom (numbers given in column 1). If no matrix
            % is given, assumed 0 initial condition on values, vel and acc.
            
            
            obj.parameters.InitialComputation();
            
            obj.initializeDynamicIntegration();
            
            obj.model.initializeMatrices(obj.parameters.stepParameters);

            obj.initializeVelocityDynamicIntegration();
            
            obj.parameters.stepParametersInit.timestep = 1;
            obj.parameters.stepParameters.currentTime = obj.parameters.time(1);
                
            if nargin == 1
                obj.model.computeElementsTermsDynamic(obj.parameters.stepParameters);
                obj.model.assembleResidueDynamic(obj.parameters.stepParameters);
                obj.model.assembleTangentOperatorDynamic(obj.parameters.stepParametersInit);
                IndLine = checkIndLine(obj.model.St);
                
                obj.initializeAccelerationDynamicIntegration();
                
            elseif nargin == 2
                obj.applyInitialConditions(initialCondition);
                obj.model.assembleTangentOperatorDynamic(obj.parameters.stepParametersInit);
                IndLine = checkIndLine(obj.model.St);
            end
            
            obj.model.setNumberMotionDof();
            
            for t = 1:obj.parameters.nstep

                for i = obj.model.listNumberDof
                    obj.model.listDof{i}.dynamicPrediction(obj.parameters.stepParameters);
                end
                
                obj.parameters.stepParameters.timestep = t+1;
                obj.parameters.stepParameters.currentTime = obj.parameters.time(obj.parameters.stepParameters.timestep);
                
                for i = obj.model.listNumberNodes
                    obj.model.listNodes{i}.updateDynamic(obj.parameters.stepParameters.timestep);
                end
                
                for i = obj.model.listNumberElementVariables
                    obj.model.listElementVariables{i}.updateDynamic(obj.parameters.stepParameters.timestep);
                end
                
                obj.model.computeElementsTermsDynamic(obj.parameters.stepParameters);
                obj.model.assembleResidueDynamic(obj.parameters.stepParameters);
                
                nit = 0;
                
                while obj.checkConvergence() && nit < obj.parameters.itMax
                    nit = nit+1;
                    obj.model.assembleTangentOperatorDynamic(obj.parameters.stepParameters); 
                    
                    dq = zeros(obj.model.nDof,1);
                    dq(obj.model.listFreeDof) = (obj.model.St(IndLine,:))\(-obj.model.res(IndLine));
                    
                    for i = obj.model.listNumberDof
                        obj.model.listDof{i}.dynamicCorrection(obj.parameters.stepParameters,dq(obj.model.listDof{i}.numDof));
                    end
                    for i = obj.model.listNumberNodes
                        obj.model.listNodes{i}.updateDynamic(obj.parameters.stepParameters.timestep);
                    end
                    for i = obj.model.listNumberElementVariables
                        obj.model.listElementVariables{i}.updateDynamic(obj.parameters.stepParameters.timestep);
                    end
                    obj.model.computeElementsTermsDynamic(obj.parameters.stepParameters);
                    obj.model.assembleResidueDynamic(obj.parameters.stepParameters);
                    
                end
                if nit == obj.parameters.itMax
                    disp(['no convergence at step ' num2str(t)])
                end
                for i = obj.model.listNumberDof
                    obj.model.listDof{i}.updateA(obj.parameters.stepParameters);
                end
            end
        end
    end
end

