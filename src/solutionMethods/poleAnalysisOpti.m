%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function analys = poleAnalysisOpti(S,step,ploting)
    % freq = 
    % freqAnalysis(S)
    % 
    % Computes the eigenvalues of the model stored in S.model
    %
    %  INPUTS:S
    %         - S    = structure containing all data of the model
    %         - step = time step at which to calculate poles
    %         - ploting = either 1 (plot the poles) or 0 (do not plot the poles)

    %  OUTPUTS:
    %         - analys              = structure containing poles analysis infos
    %         - analys.ns           = number of stable poles
    %         - analys.nu           = number of unstable poles
    %         - analys.poles        = list of all poles
    %         - analys.sPoles       = list of stable poles
    %         - analys.usPoles      = list of unstable poles
    %         - analys.sNatPulse    = list of stable natural pulsation
    %         - analys.usNatPulse   = list of unstable natural pulsation
    %         - analys.sLowPulse    = value of lowest stable natural pulsation
    %         - analys.usLowPulse   = value of lowest unstable natural
    %         pulsation
    %         - analys.sLowFreq     = value of lowest stable natural frequency
    %         - analys.usLowFreq    = value of lowest unstable natural
    %         frequency
    %         - analys.V            = all eigen vectors
    %         - analys.Vs           = stable eigen vectors
    %         - analys.Vu           = unstable eigen vectors
    if nargin < 2
        step = 1;
    end
    if nargin < 3
        ploting = 1;    
    end
    S.LocMotionDof = S.model.listFreeDof(1:S.nCoord);

    tempTime.timestep = step; % need a structure with timestep content to run the Update
    % Motion Constraint Gradient
    for i = S.model.listNumberElements
        S.model.listElements{i}.computeTermsDynamic(tempTime);
    end

    K = S.model.getTangentMatrix('Stiffness',step,1);
    K = K(S.LocMotionDof,S.LocMotionDof);
    M = S.model.getTangentMatrix('Mass',step,1);
    M = M(S.LocMotionDof,S.LocMotionDof);
    C = S.model.getTangentMatrix('Damping',step,1);
    C = C(S.LocMotionDof,S.LocMotionDof);

    B = S.model.getTangentMatrix('Phiq',step,1);
    A = B(S.LocMotionDof,S.listControls);
    dServodq = B(S.listControls,S.LocMotionDof);
    B = B(S.listLagrangeM,S.LocMotionDof);

    E = zeros(2*S.nCoord+S.nControls+S.nLagrangeM,2*S.nCoord+S.nControls+S.nLagrangeM);
    As = zeros(2*S.nCoord+S.nControls+S.nLagrangeM,2*S.nCoord+S.nControls+S.nLagrangeM);

    % As in paper Bruls 2014
    E(1:S.nCoord,1:S.nCoord) = eye(S.nCoord);
    E(S.nCoord+1:2*S.nCoord,S.nCoord+1:2*S.nCoord) = M;

    As(1:S.nCoord,S.nCoord+1:2*S.nCoord) = eye(S.nCoord);
    As(S.nCoord+1:2*S.nCoord,1:S.nCoord) = -K;
    As(S.nCoord+1:2*S.nCoord,S.nCoord+1:2*S.nCoord) = -C;
    As(S.nCoord+1:2*S.nCoord,2*S.nCoord+1:2*S.nCoord+S.nControls+S.nLagrangeM) = -[B' A];
    As(2*S.nCoord+1:2*S.nCoord+S.nControls+S.nLagrangeM,1:S.nCoord) = [B; dServodq];

    [V,D] = eig(As,E);
    poles = sort(diag(D));
    analys.V = V;

    epsilon = 1.e-6;
    largeNumber = 1.e20;
    analys.indexUnstable = find((abs(poles)<largeNumber) & real(poles) > epsilon);
    analys.indexStable = find((abs(poles)<largeNumber) & real(poles) <= epsilon);
    analys.ns = length(analys.indexStable); % number of stable poles
    analys.nu = length(analys.indexUnstable); % number of stable poles

    toU = analys.nu;
    toS = analys.ns;
    if ploting
        figure
        hold on

        plot(real(poles(analys.indexUnstable)),imag(poles(analys.indexUnstable)),'d','MarkerFaceColor','r','MarkerEdgeColor','r')
        plot(real(poles(analys.indexStable)),imag(poles(analys.indexStable)),'o','MarkerFaceColor','b','MarkerEdgeColor','b')
        xlabel('Real part','Fontsize',16)
        ylabel('Imaginary part','Fontsize',16)
        legend('Unstable','Stable', 'Location', 'Best')
        title('Poles of the system','Fontsize',18)
        grid on
    end

    analys.poles = poles;
    analys.sPoles = poles(analys.indexStable); 
    analys.usPoles = poles(analys.indexUnstable);
    analys.sNatPulse = abs(poles(analys.indexStable));
    analys.usNatPulse = abs(poles(analys.indexUnstable));
    analys.sNatFreq = abs(poles(analys.indexStable))/(2*pi);
    analys.usNatFreq = abs(poles(analys.indexUnstable))/(2*pi);
    analys.sLowPulse = min(abs(poles(analys.indexStable)));
    analys.usLowPulse = min(abs(poles(analys.indexUnstable)));
    analys.sLowFreq = analys.sLowPulse/(2*pi);
    analys.usLowFreq = analys.usLowPulse/(2*pi);
    analys.Vs = V(:,analys.indexStable);
    analys.Vu = V(:,analys.indexUnstable);

end