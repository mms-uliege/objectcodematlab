%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Arthur Lismonde (ULiege, Multibody & Mechatronic Systems Lab)
% Author: Javier Galvez (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [actualSolution, expectedSolution] = static3DLoadedBeamTest()

    close all
    clear all

    %% Definition of time parameters
	finaltime = .1;
	timestepsize = .1;

	%% Definition of the nodes of the model (as matrix)
	nodes = [1 0 0 0;...
		     2 2 0 0];
	 
	%% Definition of the elements of the model (as structure)
	% Element 1: Beam
	% beam element parameters
	h = 0.05;
	E = 70e9;
	nu = 0.25;
	G = E/(2*(1+nu));
	rho = 2700;
	A = h^2;
	I = h^4/12;
	J = 2*I;

	KCS = diag([E*A G*A G*A G*J E*I E*I]);
	MCS =  diag(rho*[A A A J I I]);

	count = 1;
	elements{count}.type = 'FlexibleBeamElement';
	elements{count}.nodes = [1 2];
	elements{count}.KCS = KCS;
	elements{count}.MCS = MCS;

	% Element 2: external force
	count = size(elements,2)+1;
	elements{count}.type = 'ForceOnNode';
	elements{count}.nodes = 2;
	elements{count}.F = [3000 3000]';
	elements{count}.Axe = [0 1 0 0 0 0;
		                   0 0 1 0 0 0];
	elements{count}.following = true;

	%% Definition of the boundary conditions

	BC = [1]; % Node 1 is fixed

	%% Definition of the finite element model (FEModel Object)
	% Based on previously defined nodes and elements

	Model = FEModel;
	Model.CreateFEModel(nodes,elements);
	Model.defineBC(BC);

	%% Definition of the solver and its parameters
    tic
	S = StaticIntegration(Model); % Creating StaticIntegration Object
	S.parameters.timestepsize = timestepsize;
	S.parameters.finaltime = finaltime;
	S.runIntegration(); % Run the integration of the Object
    compTime = toc;
	disp(['Static3DLoadedBeam computation time is ',num2str(compTime), ' s.'])

	%% visualization and plots
	% To visualize the model:(uncomment the following line)
	%Model.visu

	% % Plots
	% endNodePos = Model.listNodes{end}.position;
	% figure
	% plot(S.parameters.time,endNodePos(1,:),S.parameters.time,endNodePos(2,:),S.parameters.time,endNodePos(3,:),'Linewidth',2)
	% grid on
	% title('Tip of the beam position')
	% xlabel('Time [s]')
	% ylabel('Poistion [m]')
	% legend('X','Y','Z')

    %% Computed results
	currentPosition = S.model.listNodes{end}.position(:,end);
    currentRotation = S.model.listNodes{end}.R(:,end);
    currentForce = S.model.listElements{1}.refValForce;
    actualSolution.pos = currentPosition; 
    actualSolution.rot = currentRotation;
    actualSolution.force = currentForce;
    
    % Expected results
    positionCheck = [1.982131978392769;0.163195385522295;0.163195385522295];
    rotationCheck = [0.973275231628669;-0.162381223511465;-0.162381223511464;0.162381223511465;0.986637615814335;-0.013362384185665;0.162381223511464;-0.013362384185665;0.986637615814335];
    forceCheck = 1.033223931026559e+04;
    expectedSolution.pos = positionCheck;
    expectedSolution.rot = rotationCheck;
    expectedSolution.force = forceCheck;
end