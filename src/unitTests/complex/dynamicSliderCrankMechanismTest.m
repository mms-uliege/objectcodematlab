%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Arthur Lismonde (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [actualSolution, expectedSolution] = dynamicSliderCrankMechanismTest()

    close all
    clear all
    
    %% Definition of time parameters
    finaltime = 0.8;
    timestepsize = 0.01;


    %% Definition of the nodes of the model (as matrix)
    nodes = [1 0 0 0;...
             2 0 0 0;...
             3 0.5 0 0.5;...
             4 0.5 0 0.5;...
             5 3 0 0;...
             6 3 0 0;...
             7 3.5 0 0];

    %% Definition of the elements of the model (as structure)
    % Element 1: First rigid link
    count = 1;
    elements{count}.type = 'RigidBodyElement';
    elements{count}.nodes = [3 2];
    elements{count}.mass = 1;
    elements{count}.J = 0.01*eye(3);

    % Element 2: third rigid link
    count = size(elements,2)+1;
    elements{count}.type = 'RigidBodyElement';
    elements{count}.nodes = [4 5];
    elements{count}.mass = 1;
    elements{count}.J = 0.01*eye(3);

    % Element 3: third rigid link
    count = size(elements,2)+1;
    elements{count}.type = 'RigidBodyElement';
    elements{count}.nodes = [6 7];
    elements{count}.mass = 1;
    elements{count}.J = 0.01*eye(3);

    % Element 4: First kinematic joint which is a hinge
    count = size(elements,2)+1;
    elements{count}.type = 'KinematicConstraint';
    elements{count}.nodes = [1 2];
    elements{count}.A = [0 0 0 0 1 0]';

    % Element 5: Second kinematic joint which is a hinge
    count = size(elements,2)+1;
    elements{count}.type = 'KinematicConstraint';
    elements{count}.nodes = [3 4];
    elements{count}.A = [0 0 0 0 1 0]';

    % Element 6: third kinematic joint which is a hinge
    count = size(elements,2)+1;
    elements{count}.type = 'KinematicConstraint';
    elements{count}.nodes = [5 6];
    elements{count}.A = [0 0 0 0 1 0]';

    % Element 7: fourth kinematic joint which is a slider
    count = size(elements,2)+1;
    elements{count}.type = 'KinematicConstraint';
    elements{count}.nodes = [6 1];
    elements{count}.A = [1 0 0 0 0 0]';

    % % Element 8: Apply a constant torque on the first hinge joint
    % time = 0:timestepsize:finaltime;
    % motion = 15*ones(size(time));
    % count = size(elements,2)+1;
    % elements{count}.type = 'ForceInKinematicConstraint';
    % elements{count}.elements = count-4;
    % elements{count}.f = motion;

    % Element 8Bis: Apply an imposed motion on the first hinge joint
    time = 0:timestepsize:finaltime;
    traj = lineTraj(0,8*pi,time,0,finaltime);
    count = size(elements,2)+1;
    elements{count}.type = 'TrajectoryConstraintJoint';
    elements{count}.elements = count-4;
    elements{count}.T = traj;

    %% Definition of the boundary conditions

    BC = [1]; % Node 1 is fixed

    %% Definition of the finite element model (FEModel Object)
    % Based on previously defined nodes and elements

    Model = FEModel;
    Model.CreateFEModel(nodes,elements);
    Model.defineBC(BC);

    %% Definition of the solver and its parameters
    tic
    D = DynamicIntegration(Model);  % Creatind the Dynamic integration object
    D.parameters.timestepsize = timestepsize;
    D.parameters.finaltime = finaltime;
    D.parameters.rho = 0.2;
    D.parameters.scaling = 1e6;
    D.runIntegration(); % Run the integration of the object
    elapsed = toc;
    disp(['DynamicSliderCrankMechanism computation lasted ',num2str(elapsed),' s.'])

    %% visualization and plots
    % To visualize the model:(uncomment the following line)
    % Model.visu

    % % Plots
    % endNodePos = Model.listNodes{end}.position;
    % figure
    % plot(D.parameters.time,endNodePos(1,:),D.parameters.time,endNodePos(2,:),D.parameters.time,endNodePos(3,:),'Linewidth',2)
    % grid on
    % title('Tip of the beam position')
    % xlabel('Time [s]')
    % ylabel('Poistion [m]')
    % legend('X','Y','Z')
    % 
    % figure
    % hold on
    % CrankTrajectory = Model.listElementVariables{1}.relCoo;
    % plot(D.parameters.time,CrankTrajectory,'Linewidth',2)
    % plot(D.parameters.time,traj,':','Linewidth',2)
    % grid on
    % title('Trajectory of the crank')
    % xlabel('Time [s]')
    % ylabel('Position [rad]')
    % legend('Effective traj', 'Desired Traj')
    % 
    % figure
    % crankEffort = Model.listElementVariables{end}.value;
    % plot(D.parameters.time,crankEffort,'Linewidth',2)
    % grid on
    % title('Control effort on the crank')
    % xlabel('Time [s]')
    % ylabel('Effort [Nm]')
    % legend('Crank Effort')
    
    %% Computed results
    currentPos = D.model.listNodes{end}.position(:,end);
    currentR = D.model.listNodes{end}.R(:,end);
    currentForce = norm(D.model.listElementVariables{end}.value);
    actualSolution.pos = currentPos; 
    actualSolution.rot = currentR; 
    actualSolution.force = currentForce ;
    
    % Expected results
    posCheck = [3.500000000000106;4.165577239200693e-26;-5.359734497333589e-25];
    RCheck = [1;4.707166603340794e-26;-3.338473851947859e-25;-4.707166603340796e-26;1;1.432458362642024e-25;3.338473851947859e-25;-1.432458362642024e-25;1];
    forceCheck = 6.758845557841428e+03;
    expectedSolution.pos = posCheck;
    expectedSolution.rot = RCheck;
    expectedSolution.force = forceCheck;  
end