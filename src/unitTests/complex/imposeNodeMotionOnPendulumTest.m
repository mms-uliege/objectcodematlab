%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Arthur Lismonde (ULiege, Multibody & Mechatronic Systems Lab)
% Author: Javier Galvez (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [actualSolution, expectedSolution] = imposeNodeMotionOnPendulumTest()

    close all
    clear all

	%% Definition of time parameters
	finaltime = 0.5;
	timestepsize = 0.01;

	timeVector = 0:timestepsize:finaltime;

	%% Definition of the nodes of the model (as matrix)
	l = 1; % length of the pendulum
	grav = [0 0 -9.81];
	m_end = 1; % end effector mass

	nodes = [1 0 0 0;
		     2 0 0 0;
		     3 0 0 0;
		     4 0 0 -l];
		 
	%% Imposed trajectory parameters
	ref = halfCircleTraj(0,0,pi/4,timeVector,'cos');

	%% Definition of the elements of the model (as structure)
	count = 1;
	elements{count}.type = 'RigidBodyElement';
	elements{count}.nodes = [4 3];
	elements{count}.mass = m_end;
	elements{count}.acceleration = grav;

	count = size(elements,2)+1;
	elements{count}.type = 'KinematicConstraint';
	elements{count}.nodes = [1 2];
	elements{count}.A = [1 0 0 0 0 0;
		                 0 0 1 0 0 0]';

	count = size(elements,2)+1;
	elements{count}.type = 'KinematicConstraint';
	elements{count}.nodes = [2 3];
	elements{count}.A = [0 0 0 0 1 0]';

	count = size(elements,2)+1;
	elements{count}.type = 'ImposeNodeMotion';
	elements{count}.nodes = 3;
	elements{count}.Axe = [1 0 0 0 0 0;
		                   0 0 1 0 0 0];
	elements{count}.T = [ref; -ref];

	%% Definition of the boundary conditions

	BC = [1]; % Node 1 is fixed

	%% Definition of the finite element model (FEModel Object)
	% Based on previously defined nodes and elements
	Model = FEModel();
	Model.CreateFEModel(nodes,elements);
	Model.defineBC(BC);

	%% Definition of the solver and its parameters
	tic
	D = DynamicIntegration(Model);  % Creatind the Dynamic integration object
	D.parameters.finaltime = finaltime;
	D.parameters.timestepsize = timestepsize;
	D.parameters.rho = 0.1;
	D.parameters.scaling = 1e6;
	D.runIntegration(); % Run the integration of the object
	compTime = toc;
	disp(['ImposeNodeMotionOnPendulum computation time is ',num2str(compTime), ' s.'])


	%% visualization and plots
	% To visualize the model:(uncomment the following line)
	% Model.visu

	% figure
	% hold on
	% plot(timeVector,D.model.listElementVariables{1}.relCoo(1,:),'Linewidth',2)
	% plot(timeVector,D.model.listElements{end}.T(1,:),':','Linewidth',2)
	% plot(timeVector,D.model.listElementVariables{1}.relCoo(2,:),'Linewidth',2)
	% plot(timeVector,D.model.listElements{end}.T(2,:),':','Linewidth',2)
	% grid on
	% title('Linear position of the node of the pendulum')
	% xlabel('Time (s)')
	% ylabel('Position (m)')
	% legend('Position X','Reference X','Position Z','Reference Z')

	% Check results

    
    
    % Computed results
	currentPos = D.model.listNodes{end}.position(:,end);
	currentR = D.model.listNodes{end}.R(:,end);
    actualSolution.pos = currentPos; 
    actualSolution.rot = currentR; 

    % Expected results
	posCheck = [0.990858191601945;0;0.134907539208041];
	RCheck = [-0.134907539208086;0;-0.990858191602016;0;1;0;0.990858191602016;0;-0.134907539208086];
    expectedSolution.pos = posCheck;
    expectedSolution.rot = RCheck;
end