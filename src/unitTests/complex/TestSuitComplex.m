%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Javier Galvez (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Resources
% https://www.mathworks.com/help/matlab/matlab_prog/create-simple-test-suites.html
% https://www.mathworks.com/videos/matlab-unit-testing-framework-74975.html

classdef TestSuitComplex < matlab.unittest.TestCase
    
    properties (TestParameter)
    end
    
     methods (Test)
        function testDynamic3DBeamLoadedYZ(testCase)
            [actualSolution, expectedSolution] = dynamic3DBeamLoadedYZTest();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.force, expectedSolution.force, 'AbsTol', 1e-4);
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testDynamicControlPendulum(testCase)
            [actualSolution, expectedSolution] = dynamicControlPendulumTest();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testDynamicSliderCrankMechanism(testCase)
            [actualSolution, expectedSolution] = dynamicSliderCrankMechanismTest();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.force, expectedSolution.force, 'AbsTol', 1e-4);
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testDynamicSliderDoublePendulum(testCase)
            [actualSolution, expectedSolution] = dynamicSliderDoublePendulumTest();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testExternalForceOnPendulum(testCase)
            [actualSolution, expectedSolution] = externalForceOnPendulumTest();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testImposeMotionRelCooOnPendulum(testCase)
            [actualSolution, expectedSolution] = imposeMotionRelCooOnPendulumTest();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testImposeNodeMotionOnPendulum(testCase)
            [actualSolution, expectedSolution] = imposeNodeMotionOnPendulumTest();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testInverseDynamicDoublePendulum(testCase)
            [actualSolution, expectedSolution] = inverseDynamicDoublePendulumTest();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.currentTorque, expectedSolution.currentTorque, 'AbsTol', 1e-4);
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testInverseDynamicFlexibleParallelManipulator(testCase)
            [actualSolution, expectedSolution] = inverseDynamicFlexibleParallelManipulatorTest();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', 1.5e-4);
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', 1.5e-4);
            testCase.verifyEqual(actualSolution.currentTorque, expectedSolution.currentTorque, 'AbsTol', 1.5e-4);
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1.5e-4);
        end
        
        function testSpringMassOnInclinedPlane(testCase)
            [actualSolution, expectedSolution] = springMassOnInclinedPlaneTest();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', sqrt(eps));
        end
        
        function testStatic3DLoadedBeam(testCase)
            [actualSolution, expectedSolution] = static3DLoadedBeamTest();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.force, expectedSolution.force, 'AbsTol', 1e-4);
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
    end
end
