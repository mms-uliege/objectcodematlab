%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Arthur Lismonde (ULiege, Multibody & Mechatronic Systems Lab)
% Author: Javier Galvez (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [actualSolution, expectedSolution] = inverseDynamicFlexibleParallelManipulatorTest()

    close all
    clear all

    finaltime = 1;% 1.5 or 1
	timestepsize = 0.1;
	t_i = 0.2;% 0.2
	t_f = 0.8;% 1.2 or 0.7

	rho_num = 0;
	tol = 1e-6;

	%% Arms parameters
	% Dimensions of upper arm _u
	a_u = 0.05;
	b_u = a_u;
	e_u = 0.005;
	rapport = a_u/e_u;
	% Dimensions of fore arm _f
	a_f = 0.0075; % 0.0075 is unstable
	b_f = a_f;
	e_f = a_f/rapport;
	a_fIn = a_f-2*e_f;
	b_fIn = b_f-2*e_f;
	a_uIn = a_u-2*e_u; % inner length
	b_uIn = b_u-2*e_u;

	% material parameters
	E = 70e9; nu = 0.3; G = E/(2*(1+nu));
	rho = 2700;

	alpha = 0.0001; % damping proportional to mass
	beta = 0.01; % damping proportional to stiffness

	% considered gravity?
	grav = [0 0 -9.81];
	% grav = [0 0 0];
	m_end = 0.1; % end effector load [kg]

	%% Creating nodes
	sideTriUp = 0.25; % side length of the upper triangle
	lengthUpperArm = 0.25; % length of the first rigid upper arm (linked to hinge)
	l1 = sideTriUp/(2*cos(pi/6)); % distance between center and a corner of the upper triangle
	l2 = sideTriUp*tan(pi/6)/2; % distance between center and middle of edge of the upper triangle
	sideTriUpperArm = 2*(l1 + lengthUpperArm)*cos(pi/6); % side length of the triangle formed with the outer node of the upper arms
	l2bis = (l1 + lengthUpperArm)*sin(pi/6); % distance between center and midle edge of the triangle formed by the outer node of upper arms.
	sideTriLow = 0.1; % side length of lower triangle
	height = 0.7; % height of the robot 
	nodes = [1 l1 0 0; %                                                UPPER TRIANGLE
		     2 -l2 -sideTriUp/2 0;
		     3 -l2 sideTriUp/2 0;
		     4 l1 0 0; % tips of 1st upper arm                          UPPER ARMS WITH HINGES
		     5 l1+lengthUpperArm 0 0; % tips of 1st upper arm
		     6 -l2 -sideTriUp/2 0; % tips of 2nd upper arm
		     7 -l2bis -sideTriUpperArm/2 0; % tips of 2nd upper arm
		     8 -l2 sideTriUp/2 0; % tips of 3rd upper arm
		     9 -l2bis sideTriUpperArm/2 0; % tips of 3rd upper arm
		     10 l1+lengthUpperArm 0 0; % tips of 1st lower arm          LOWER ARMS WIITH 2DOF JOINTS
		     11 sideTriLow/(2*cos(pi/6)) 0 -height; % tips of 1st lower arm
		     12 -l2bis -sideTriUpperArm/2 0; % tips of 2nd lower arm
		     13 -sideTriLow*tan(pi/6)/2 -sideTriLow/2 -height; % tips of 2nd lower arm
		     14 -l2bis sideTriUpperArm/2 0; % tips of 3rd lower arm
		     15 -sideTriLow*tan(pi/6)/2 sideTriLow/2 -height; % tips of 3rd lower arm
		     16 sideTriLow/(2*cos(pi/6)) 0 -height; %                   LOWER TRIANGLE
		     17 -sideTriLow*tan(pi/6)/2 -sideTriLow/2 -height;
		     18 -sideTriLow*tan(pi/6)/2 sideTriLow/2 -height;
		     19 0 0 -height];

	nElem1 = 4; % even number !
	Start1 = 10;
	End1 = Start1 + 1;
	nodes = createInterNodes(nodes,nElem1,Start1,End1);

	nElem2 = nElem1; % even number !
	Start2 = End1+nElem1;
	End2 = Start2 + 1;
	nodes = createInterNodes(nodes,nElem2,Start2,End2);

	nElem3 = nElem1; % even number !
	Start3 = End2+nElem2;
	End3 = Start3 + 1;
	nodes = createInterNodes(nodes,nElem3,Start3,End3);

	vec1 = nodes(Start1,2:4)/norm(nodes(Start1,2:4));
	vec2 = nodes(Start2,2:4)/norm(nodes(Start2,2:4));
	vec3 = nodes(Start3,2:4)/norm(nodes(Start3,2:4));

	hinge1 = cross(vec1,[0 0 1]);
	hinge2 = cross(vec2,[0 0 1]);
	hinge3 = cross(vec3,[0 0 1]);

	vec1bis = (nodes(Start1,2:4)-nodes(Start1+nElem1-1,2:4))/norm(nodes(Start1,2:4)-nodes(Start1+nElem1-1,2:4));
	vec2bis = (nodes(Start2,2:4)-nodes(Start2+nElem2-1,2:4))/norm(nodes(Start2,2:4)-nodes(Start2+nElem2-1,2:4));
	vec3bis = (nodes(Start3,2:4)-nodes(Start3+nElem3-1,2:4))/norm(nodes(Start3,2:4)-nodes(Start3+nElem3-1,2:4));

	joint2Dof1 = cross(vec1bis,hinge1);
	joint2Dof2 = cross(vec2bis,hinge2);
	joint2Dof3 = cross(vec3bis,hinge3);
	 
	%% Rigid Model
	% End plate dimensions and param

	b = sideTriLow;
	h = sideTriLow*cos(pi/6);
	IxxR = m_end*2*(h*(b/2)^3)/12;
	IyyR = m_end*(b*h^3)/36;
	IzzR = m_end*((-h*b^3/4 + b*h^3)/36);

	a1U = a_u; % first arm param
	a1U_In = a_uIn;
	b1U = b_u;
	b1U_In = b_uIn;
	l1U = lengthUpperArm;
	a1 = a_f;
	a1_In = a_fIn;
	b1 = b_f;
	b1_In = b_fIn;
	l1 = norm(nodes(Start1,2:4)-nodes(Start1+nElem1-1,2:4));
	m1 = (a1U*b1U-a1U_In*b1U_In)*l1U*rho;                     % Upper arm
	IxxU1 = m1*(a1U^2+b1U^2-a1U_In^2-b1U_In^2)/12;
	IyyU1 = m1*(a1U^2+l1U^2)/12;
	IzzU1 = m1*(b1U^2+l1U^2)/12;
	m1bis = (a1*b1-a1_In*b1_In)*l1*rho;                       % Fore arm
	IxxFR1 = m1bis*(a1^2+b1^2-a1_In^2-b1_In^2)/12;
	IyyFR1 = m1bis*(a1^2+l1^2)/12;
	IzzFR1 = m1bis*(b1^2+l1^2)/12;

	a2U = a1U; % second arm param
	a2U_In = a1U_In;
	b2U = b1U;
	b2U_In = b1U_In;
	l2U = lengthUpperArm;
	a2 = a1;
	a2_In = a1_In;
	b2 = b1;
	b2_In = b1_In;
	l2 = norm(nodes(Start2,2:4)-nodes(Start2+nElem2-1,2:4));
	m2 = (a2U*b2U-a2U_In*b2U_In)*l2U*rho;                    % Upper arm
	IxxU2 = m2*(a2U^2+b2U^2-a2U_In^2-b2U_In^2)/12;
	IyyU2 = m2*(a2U^2+l2U^2)/12;
	IzzU2 = m2*(b2U^2+l2U^2)/12;
	m2bis = (a2*b2-a2_In*b2_In)*l2*rho;                      % Fore arm
	IxxFR2 = m2bis*(a2^2+b2^2)/12;
	IyyFR2 = m2bis*(a2^2+l2^2)/12;
	IzzFR2 = m2bis*(b2^2+l2^2)/12;

	a3U = a1U; % third arm param
	a3U_In = a1U_In;
	b3U = b1U;
	b3U_In = b1U_In;
	l3U = lengthUpperArm;
	a3 = a1;
	a3_In = a1_In;
	b3 = b1;
	b3_In = b1_In;
	l3 = norm(nodes(Start3,2:4)-nodes(Start3+nElem3-1,2:4));
	m3 = (a3U*b3U-a3U_In*b3U_In)*l3U*rho;                     % Upper arm
	IxxU3 = m3*(a3U^2+b3U^2-a3U_In^2-b3U_In^2)/12;
	IyyU3 = m3*(a3U^2+l3U^2)/12;
	IzzU3 = m3*(b3U^2+l3U^2)/12;
	m3bis = (a3*b3-a3_In*b3_In)*l3*rho;                       % Fore arm
	IxxFR3 = m3bis*(a3^2+b3^2-a3_In^2-b3_In^2)/12;
	IyyFR3 = m3bis*(a3^2+l3^2)/12;
	IzzFR3 = m3bis*(b3^2+l3^2)/12;

	count = 1;
	elements{count}.type = 'RigidBodyElement'; % end effector
	elements{count}.nodes = [nodes(end,1) nodes(end-3:end-1,1)'];
	elements{count}.mass = m_end;
	elements{count}.J = diag([IxxR IyyR IzzR]);
	elements{count}.acceleration = grav;

	count = size(elements,2)+1;
	elements{count}.type = 'RigidBodyElement'; % 1st upper arm
	elements{count}.nodes = [5 4];
	elements{count}.mass = m1;
	elements{count}.J = diag([IxxU1 IyyU1 IzzU1]);
	elements{count}.acceleration = grav;

	count = size(elements,2)+1;
	elements{count}.type = 'RigidBodyElement'; % 1st lower arm
	elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
	elements{count}.mass = m1bis;
	elements{count}.J = diag([IxxFR1 IyyFR1 IzzFR1]);
	elements{count}.acceleration = grav;

	count = size(elements,2)+1;
	elements{count}.type = 'RigidBodyElement'; % 2nd upper arm
	elements{count}.nodes = [7 6];
	elements{count}.mass = m2;
	elements{count}.J = diag([IxxU2 IyyU2 IzzU2]);
	elements{count}.acceleration = grav;

	count = size(elements,2)+1;
	elements{count}.type = 'RigidBodyElement'; % 2nd lower arm
	elements{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
	elements{count}.mass = m2bis;
	elements{count}.J = diag([IxxFR2 IyyFR2 IzzFR2]);
	elements{count}.acceleration = grav;

	count = size(elements,2)+1;
	elements{count}.type = 'RigidBodyElement'; % 3rd upper arm
	elements{count}.nodes = [9 8];
	elements{count}.mass = m3;
	elements{count}.J = diag([IxxU3 IyyU3 IzzU3]);
	elements{count}.acceleration = grav;

	count = size(elements,2)+1;
	elements{count}.type = 'RigidBodyElement'; % 3rd lower arm
	elements{count}.nodes = [Start3+nElem3/2 Start3+[0:nElem3/2-1] Start3+nElem3/2+1+[0:nElem3/2-1]];
	elements{count}.mass = m3bis;
	elements{count}.J = diag([IxxFR3 IyyFR3 IzzFR3]);
	elements{count}.acceleration = grav;

	count = size(elements,2)+1;
	elements{count}.type = 'KinematicConstraint'; % first upper 2 dof joint
	elements{count}.nodes = [5 Start1];
	elements{count}.A = [0 0 0 joint2Dof1; 0 0 0 hinge1]';

	count = size(elements,2)+1;
	elements{count}.type = 'KinematicConstraint'; % second upper 2 dof joint
	elements{count}.nodes = [7 Start2];
	elements{count}.A = [0 0 0 joint2Dof2; 0 0 0 hinge2]';

	count = size(elements,2)+1;
	elements{count}.type = 'KinematicConstraint'; % third upper 2 dof joint
	elements{count}.nodes = [9 Start3];
	elements{count}.A = [0 0 0 joint2Dof3; 0 0 0 hinge3]';

	count = size(elements,2)+1;
	elements{count}.type = 'KinematicConstraint'; % first lower 2 dof joint
	elements{count}.nodes = [Start1+nElem1 nodes(end-3,1)];
	elements{count}.A = [0 0 0 joint2Dof1; 0 0 0 hinge1]';

	count = size(elements,2)+1;
	elements{count}.type = 'KinematicConstraint'; % second lower 2 dof joint
	elements{count}.nodes = [Start2+nElem2 nodes(end-2,1)];
	elements{count}.A = [0 0 0 joint2Dof2; 0 0 0 hinge2]';

	count = size(elements,2)+1;
	elements{count}.type = 'KinematicConstraint'; % third lower 2 dof joint
	elements{count}.nodes = [Start3+nElem3 nodes(end-1,1)];
	elements{count}.A = [0 0 0 joint2Dof3; 0 0 0 hinge3]';

	count = size(elements,2)+1;
	elements{count}.type = 'KinematicConstraint'; % first hinge joint
	elements{count}.nodes = [1 4];
	elements{count}.A = [0 0 0 hinge1]';

	count = size(elements,2)+1;
	elements{count}.type = 'KinematicConstraint'; % second hinge joint
	elements{count}.nodes = [2 6];
	elements{count}.A = [0 0 0 hinge2]';

	count = size(elements,2)+1;
	elements{count}.type = 'KinematicConstraint'; % third hinge joint
	elements{count}.nodes = [3 8];
	elements{count}.A = [0 0 0 hinge3]';

	% Trajectory

	x_end = nodes(end,2);
	y_end = sideTriLow*2;
	z_end = nodes(end,4);
	r = y_end/2;

	timeVector = 0:timestepsize:finaltime;
	trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'cos',t_i,t_f);
	trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
	trajz = nodes(end,4)*ones(size(timeVector));

	count = size(elements,2)+1;
	elements{count}.type = 'TrajectoryConstraint';
	elements{count}.nodes = [nodes(end,1)];
	elements{count}.T = [trajx;...
		                 trajy;...
		                 trajz];
	elements{count}.Axe = [1 0 0;...
		                   0 1 0;...
		                   0 0 1];
	elements{count}.elements = [count-3 count-2 count-1];
	elements{count}.active = 1;

	% Boundary Condition
	BC = [1 2 3];

	% Solving
	ModelR = FEModel();
	ModelR.CreateFEModel(nodes,elements);
	ModelR.defineBC(BC);

	D = DynamicIntegration(ModelR);
	D.parameters.finaltime = finaltime;
	D.parameters.timestepsize = timestepsize;
	D.parameters.rho = rho_num;
	D.parameters.relTolRes = tol;
	D.parameters.scaling = 1e6;
	D.runIntegration();

	uRigi.u1 = ModelR.listElementVariables{end}.value(1,:);
	uRigi.u2 = ModelR.listElementVariables{end}.value(2,:);
	uRigi.u3 = ModelR.listElementVariables{end}.value(3,:);
	uRigi.time = D.parameters.time;

	%% Flexible Model

	A1 = a1*b1-a1_In*b1_In; IxxF1 = (a1*b1*(a1^2+b1^2)-a1_In*b1_In*(a1_In^2+b1_In^2))/12; IyyF1 = (b1*a1^3-b1_In*a1_In^3)/12;IzzF1 = (a1*b1^3-a1_In*b1_In^3)/12;

	KCS1 = diag([E*A1 G*A1 G*A1 G*IxxF1 E*IyyF1 E*IzzF1]);
	MCS1 = diag(rho*[A1 A1 A1 IxxF1 IyyF1 IzzF1]);

	A2 = a2*b2-a2_In*b2_In; IxxF2 = (a2*b2*(a2^2+b2^2)-a2_In*b2_In*(a2_In^2+b2_In^2))/12; IyyF2 = (b2*a2^3-b2_In*a2_In^3)/12;IzzF2 = (a2*b2^3-a2_In*b2_In^3)/12;

	KCS2 = diag([E*A2 G*A2 G*A2 G*IxxF2 E*IyyF2 E*IzzF2]);
	MCS2 = diag(rho*[A2 A2 A2 IxxF2 IyyF2 IzzF2]);

	A3 = a3*b3-a3_In*b3_In; IxxF3 = (a3*b3*(a3^2+b3^2)-a3_In*b3_In*(a3_In^2+b3_In^2))/12; IyyF3 = (b3*a3^3-b3_In*a3_In^3)/12;IzzF3 = (a3*b3^3-a3_In*b3_In^3)/12;

	KCS3 = diag([E*A3 G*A3 G*A3 G*IxxF3 E*IyyF3 E*IzzF3]);
	MCS3 = diag(rho*[A3 A3 A3 IxxF3 IyyF3 IzzF3]);

	type = 'FlexibleBeamElement';

	count = 1;
	elementsFlex{count}.type = 'RigidBodyElement'; % end effector
	elementsFlex{count}.nodes = [nodes(end,1) nodes(end-3:end-1,1)'];
	elementsFlex{count}.mass = m_end;
	elementsFlex{count}.J = diag([IxxR IyyR IzzR]);
	elementsFlex{count}.acceleration = grav;

	count = size(elementsFlex,2)+1;
	elementsFlex{count}.type = 'RigidBodyElement'; % 1st upper arm
	elementsFlex{count}.nodes = [5 4];
	elementsFlex{count}.mass = m1;
	elementsFlex{count}.J = diag([IxxU1 IyyU1 IzzU1]);
	elementsFlex{count}.acceleration = grav;

	count = size(elementsFlex,2); % 1st lower arm (flexible)
	for i = 1:nElem1
		elementsFlex{count+i}.type = type;
		elementsFlex{count+i}.nodes = [Start1+i-1 Start1+i];
		elementsFlex{count+i}.KCS = KCS1;
		elementsFlex{count+i}.MCS = MCS1;
		elementsFlex{count+i}.alpha = alpha; % damping proportional to Mass
		elementsFlex{count+i}.beta = beta; % damping proportional to Stiffness
		elementsFlex{count+i}.acceleration = grav;
	end

	count = size(elementsFlex,2)+1;
	elementsFlex{count}.type = 'RigidBodyElement'; % 2nd upper arm
	elementsFlex{count}.nodes = [7 6];
	elementsFlex{count}.mass = m2;
	elementsFlex{count}.J = diag([IxxU2 IyyU2 IzzU2]);
	elementsFlex{count}.acceleration = grav;

	count = size(elementsFlex,2); % 2nd lower arm (flexible)
	for i = 1:nElem2
		elementsFlex{count+i}.type = type;
		elementsFlex{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
		elementsFlex{count+i}.KCS = KCS2;
		elementsFlex{count+i}.MCS = MCS2;
		elementsFlex{count+i}.alpha = alpha; % damping proportional to Mass
		elementsFlex{count+i}.beta = beta; % damping proportional to Stiffness
		elementsFlex{count+i}.acceleration = grav;
	end

	count = size(elementsFlex,2)+1;
	elementsFlex{count}.type = 'RigidBodyElement'; % 3rd upper arm
	elementsFlex{count}.nodes = [9 8];
	elementsFlex{count}.mass = m3;
	elementsFlex{count}.J = diag([IxxU3 IyyU3 IzzU3]);
	elementsFlex{count}.acceleration = grav;

	count = size(elementsFlex,2); % 3rd lower arm (flexible)
	for i = 1:nElem3
		elementsFlex{count+i}.type = type;
		elementsFlex{count+i}.nodes = [nodes(Start3+i-1,1) nodes(Start3+i,1)];
		elementsFlex{count+i}.KCS = KCS3;
		elementsFlex{count+i}.MCS = MCS3;
		elementsFlex{count+i}.alpha = alpha; % damping proportional to Mass
		elementsFlex{count+i}.beta = beta; % damping proportional to Stiffness
		elementsFlex{count}.acceleration = grav;
	end

	count = size(elementsFlex,2)+1;
	elementsFlex{count}.type = 'KinematicConstraint'; % first upper 2 dof joint
	elementsFlex{count}.nodes = [5 Start1];
	elementsFlex{count}.A = [0 0 0 joint2Dof1; 0 0 0 hinge1]';

	count = size(elementsFlex,2)+1;
	elementsFlex{count}.type = 'KinematicConstraint'; % second upper 2 dof joint
	elementsFlex{count}.nodes = [7 Start2];
	elementsFlex{count}.A = [0 0 0 joint2Dof2; 0 0 0 hinge2]';

	count = size(elementsFlex,2)+1;
	elementsFlex{count}.type = 'KinematicConstraint'; % third upper 2 dof joint
	elementsFlex{count}.nodes = [9 Start3];
	elementsFlex{count}.A = [0 0 0 joint2Dof3; 0 0 0 hinge3]';

	count = size(elementsFlex,2)+1;
	elementsFlex{count}.type = 'KinematicConstraint'; % first lower 2 dof joint
	elementsFlex{count}.nodes = [Start1+nElem1 nodes(end-3,1)];
	elementsFlex{count}.A = [0 0 0 joint2Dof1; 0 0 0 hinge1]';

	count = size(elementsFlex,2)+1;
	elementsFlex{count}.type = 'KinematicConstraint'; % second lower 2 dof joint
	elementsFlex{count}.nodes = [Start2+nElem2 nodes(end-2,1)];
	elementsFlex{count}.A = [0 0 0 joint2Dof2; 0 0 0 hinge2]';

	count = size(elementsFlex,2)+1;
	elementsFlex{count}.type = 'KinematicConstraint'; % third lower 2 dof joint
	elementsFlex{count}.nodes = [Start3+nElem3 nodes(end-1,1)];
	elementsFlex{count}.A = [0 0 0 joint2Dof3; 0 0 0 hinge3]';

	count = size(elementsFlex,2)+1;
	elementsFlex{count}.type = 'KinematicConstraint'; % first hinge joint
	elementsFlex{count}.nodes = [1 4];
	elementsFlex{count}.A = [0 0 0 hinge1]';

	count = size(elementsFlex,2)+1;
	elementsFlex{count}.type = 'KinematicConstraint'; % second hinge joint
	elementsFlex{count}.nodes = [2 6];
	elementsFlex{count}.A = [0 0 0 hinge2]';

	count = size(elementsFlex,2)+1;
	elementsFlex{count}.type = 'KinematicConstraint'; % third hinge joint
	elementsFlex{count}.nodes = [3 8];
	elementsFlex{count}.A = [0 0 0 hinge3]';

	% Trajectory

	count = size(elementsFlex,2)+1;
	elementsFlex{count}.type = 'TrajectoryConstraint';
	elementsFlex{count}.nodes = [nodes(end,1)];
	elementsFlex{count}.T = [trajx;...
		                 trajy;...
		                 trajz];
	elementsFlex{count}.Axe = [1 0 0;...
		                   0 1 0;...
		                   0 0 1];
	elementsFlex{count}.elements = [count-3 count-2 count-1];
	elementsFlex{count}.active = 1;

	ModelF = FEModel();
	ModelF.CreateFEModel(nodes,elementsFlex);
	ModelF.defineBC(BC);

	% Copying ElementVariables and Node variables from rigid solution
		                       
	for n = ModelF.listNumberNodes
		ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
		ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
		ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
		ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
		ModelF.listNodes{n}.initializeDynamic_Opti();
	end
	skip = 0;
	for n = ModelF.listNumberElementVariables
		if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
		    ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
		    ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
		    ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
		    ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
		    ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
		else
		    while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
		        skip = skip+1;
		    end
		    ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
		end
		ModelF.listElementVariables{n}.initializeDynamic_Opti();
	end


	% Start full optimization with newly computed initial guess
	tic
	S = DirectTranscriptionOpti(ModelF);
	S.parameters.rho = rho_num;
	S.npts = round(finaltime/timestepsize + 1);
	S.parameters.finaltime = finaltime;
	S.parameters.timestepsize = timestepsize;
	S.parameters.scaling = 1e6;
	S.linConst = false;
	S.parameters.relTolRes = tol;
	S.ConstIter = 'notConstant';
	xSol = S.runOpti(D);
	calcTime = toc;
	disp(['InverseDynamicFlexibleParallelManipulator computation lasted ', num2str(calcTime),' s.'])
	% 
	% timeSteps = S.timeValues;
	% timeLoc = S.timesteps;
	% timeRigi = D.parameters.time;
	% 
	% uBeam.u1 = ModelF.listElementVariables{end}.value(1,timeLoc);
	% uBeam.u2 = ModelF.listElementVariables{end}.value(2,timeLoc);
	% uBeam.u3 = ModelF.listElementVariables{end}.value(3,timeLoc);
	% uBeam.time = S.timeValues;
	% 
	% endNode = nodes(end,1);
	% 
	% joint1_init = ModelR.listElementVariables{7};
	% joint2_init = ModelR.listElementVariables{8};
	% joint3_init = ModelR.listElementVariables{9};
	% 
	% joint1 = ModelF.listElementVariables{7};
	% joint2 = ModelF.listElementVariables{8};
	% joint3 = ModelF.listElementVariables{9};
	% 
	% figure
	% hold on
	% plot(timeSteps,joint1.relCoo(timeLoc),timeSteps,joint2.relCoo(timeLoc),timeSteps,joint3.relCoo(timeLoc),'Linewidth',2)
	% plot(timeRigi,joint1_init.relCoo,'--',timeRigi,joint2_init.relCoo,'--',timeRigi,joint3_init.relCoo,'--','Linewidth',2)
	% xlabel('Time (s)','Fontsize',16)
	% ylabel('Joints position (rad)','Fontsize',16)
	% legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
	% % legend('Joint 1','Joint 2','Joint 3','Location', 'Best')
	% grid on
	% 
	% figure
	% hold on
	% plot(timeSteps,joint1.velocity(timeLoc),timeSteps,joint2.velocity(timeLoc),timeSteps,joint3.velocity(timeLoc),'Linewidth',2)
	% plot(timeRigi,joint1_init.velocity,'--',timeRigi,joint2_init.velocity,'--',timeRigi,joint3_init.velocity,'--','Linewidth',2)
	% xlabel('Time (s)','Fontsize',16)
	% ylabel('Joints velocities (rad/s)','Fontsize',16)
	% legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
	% % legend('Joint 1','Joint 2','Joint 3','Location', 'Best')
	% grid on
	% 
	% figure
	% hold on
	% plot(timeSteps,joint1.acceleration(timeLoc),timeSteps,joint2.acceleration(timeLoc),timeSteps,joint3.acceleration(timeLoc),'Linewidth',2)
	% plot(timeRigi,joint1_init.acceleration,'--',timeRigi,joint2_init.acceleration,'--',timeRigi,joint3_init.acceleration,'--','Linewidth',2)
	% xlabel('Time (s)','Fontsize',16)
	% ylabel('Joints acceleration (rad/s^2)','Fontsize',16)
	% legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
	% % legend('Joint 1','Joint 2','Joint 3','Location', 'Best')
	% grid on

	% anl = poleAnalysisOpti(S);%% Direct dynamics computation
  
    %% Computed results
	currentPos = S.model.listNodes{end}.position(:,end);
	currentR = S.model.listNodes{end}.R(:,end);
	currentTorque = S.model.listElementVariables{end}.value(:,end);
    actualSolution.pos = currentPos; 
    actualSolution.rot = currentR; 
    actualSolution.currentTorque = currentTorque ;
    
    % Expected results
	posCheck = [-1.228418812404592e-18;0.200000000000000;-0.700000000000000];
	RCheck = [0.999604845569723;-0.018943711494445;-0.020767486808582;0.019070717750147;0.999800523521034;0.005934719164545;0.020650918575783;-0.006328424913281;0.999766718089822];
	TorqueCheck = [1.702080336007288;1.329924660948716;1.671748454679534];
    expectedSolution.pos = posCheck;
    expectedSolution.rot = RCheck;
    expectedSolution.currentTorque = TorqueCheck;  
end