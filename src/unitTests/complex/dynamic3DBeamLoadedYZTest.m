%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Arthur Lismonde (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [actualSolution, expectedSolution] = dynamic3DBeamLoadedYZTest()

    close all
    clear all
    
    %% Definition of time parameters
    finaltime = 0.3;
    timestepsize = 0.01;

    %% Definition of the nodes of the model (as matrix)
    nodes = [1 0 0 0;
             2 1 0 0];

    grav = [0 -15 -9.81];

    %% Definition of the elements of the model (as structure)
    % Number of beam elements to build the model
    nElem = 4;
    Start = 1;
    End = 2;
    
    % create new nodes to have more beam elements
    nodes = createInterNodes(nodes,nElem,Start,End);

    % Beam parameters
    a = 0.003;
    b = 0.01;
    rho = 2000;

    E = 20e9; nu = 0.3; G = E/(2*(1+nu));
    A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;

    KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
    MCS = diag(rho*[A A A Ixx Iyy Izz]);

    count = 0;
    type = 'FlexibleBeamElement';
    for i = 1:nElem
        elements{count+i}.type = type;
        elements{count+i}.nodes = [nodes(Start+i-1,1) nodes(Start+i,1)];
        elements{count+i}.yAxis = [0 1 0];
        elements{count+i}.KCS = KCS;
        elements{count+i}.MCS = MCS;
        elements{count+i}.alpha = 4.5896e-4; % damping proportional to Mass 4.5896e-4
        elements{count+i}.beta = 0.01329; % damping proportional to Stiffness 0.1329
        elements{count+i}.acceleration = grav;
    end

    %% Definition of the boundary conditions
    BC = [1]; % Node 1 is fixed

    %% Definition of the finite element model (FEModel Object)
    % Based on previously defined nodes and elements
    Model = FEModel();
    Model.CreateFEModel(nodes,elements);
    Model.defineBC(BC);

    %% Definition of the solver and its parameters
    tic
    D = DynamicIntegration(Model);  % Creatind the Dynamic integration object
    D.parameters.finaltime = finaltime;
    D.parameters.timestepsize = timestepsize;
    D.parameters.rho = 0.4;
    D.parameters.scaling = 1e6;
    % D.parameters.relTolRes = 1e-6;
    D.runIntegration(); % Run the integration of the object
    elapsed = toc;
    disp(['Dynamic3DBeamLoadedYZ computation lasted ',num2str(elapsed),' s.'])
    
    %% visualization and plots
    % To visualize the model:(uncomment the following line)
    % Model.visu

    % % Plots
    % endNodePos = Model.listNodes{end}.position;
    % figure
    % plot(D.parameters.time,endNodePos(1,:),D.parameters.time,endNodePos(2,:),D.parameters.time,endNodePos(3,:),'Linewidth',2)
    % grid on
    % title('Tip of the beam position')
    % xlabel('Time [s]')
    % ylabel('Poistion [m]')
    % legend('X','Y','Z')  
      
    %% Computed results
    currentPosition = D.model.listNodes{end}.position(:,end);
    currentRotation = D.model.listNodes{end}.R(:,end);
    currentForce = D.model.listElements{end}.refValForce;
    actualSolution.pos = currentPosition; 
    actualSolution.rot = currentRotation;
    actualSolution.force = currentForce;
    
    % Expected results
    positionCheck = [0.953412403487572;-0.025290057211893;-0.277893685828186];
    rotationCheck = [0.924073427685432;0.031359388801851;0.380926356373082;-0.034636410495860;0.999398449920256;0.001748532227999;-0.380642377191058;-0.014809693817376;0.924603727904758];
    forceCheck = 0.751675456492991;
    expectedSolution.pos = positionCheck;
    expectedSolution.rot = rotationCheck;
    expectedSolution.force = forceCheck;
end