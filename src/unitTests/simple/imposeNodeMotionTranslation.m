%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Arthur Lismonde (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [actualSolution, expectedSolution] = imposeNodeMotionTranslation()
    % Impose node displacement of a 1 nodes rigid body (free in space).
    close all
    clear all

    %% Definition of time parameters
    finaltime = 0.8;
    timestepsize = 0.01;


    %% Definition of the nodes of the model (as matrix)
    nodes = [1 0 0 0];

    %% Definition of the elements of the model (as structure)
    % Element 1: Rigid pendulum
    count = 1;
    elements{count}.type = 'RigidBodyElement';
    elements{count}.nodes = [1];
    elements{count}.mass = 1;
    elements{count}.J = 0.01*eye(3);


    % Element 2: Imposed translation on the first node
    timeV = 0:timestepsize:finaltime;
    traj = lineTraj(0,1,timeV,0,finaltime);
    count = size(elements,2)+1;
    elements{count}.type = 'ImposeNodeMotion';
    elements{count}.nodes = 1;
    elements{count}.Axe = [1 0 0 0 0 0;...
        0 1 0 0 0 0;...
        0 0 1 0 0 0];
    elements{count}.T = [traj;...
        -traj;...
        0.5*traj];

    %% Definition of the boundary conditions

    BC = []; % No node are fixed

    %% Definition of the finite element model (FEModel Object)
    % Based on previously defined nodes and elements

    Model = FEModel;
    Model.CreateFEModel(nodes,elements);
    Model.defineBC(BC);

    %% Definition of the solver and its parameters
    tic
    D = DynamicIntegration(Model);  % Creatind the Dynamic integration object
    D.parameters.timestepsize = timestepsize;
    D.parameters.finaltime = finaltime;
    D.parameters.rho = 0.2;
    D.parameters.scaling = 1e6;
    D.runIntegration(); % Run the integration of the object
    elapsed = toc;
    disp(['ImposeNodeMotionTranslation computation lasted ',num2str(elapsed),' s.'])

    %% visualization and plots
    % To visualize the model:(uncomment the following line)
    % Model.visu

    % % Plots
    % for i = 1:length(timeV)
    %     Hstart(:,:,i) = [vect9ToMat3(Model.listNodes{1}.R(:,i)) Model.listNodes{1}.position(:,i);0 0 0 1];
    % end
    % leg = {};
    % figure
    % hold on
    % plot(D.parameters.time,Model.listNodes{1}.position,'Linewidth',2)
    % leg = [leg 'X_{end}','Y_{end}','Z_{end}'];
    % plot(timeV,elements{end}.T,'k')
    % leg = [leg 'X_{d}','Y_{d}','Z_{d}'];
    % grid on
    % title('Angle of nodes')
    % xlabel('Time [s]')
    % ylabel('Angle [m]')
    % legend(leg)
    %
    % figure
    % for i = 1:length(timeV)
    %     Model.visu(i)
    %     hold on
    %     plotFrame(Hstart(:,:,i))
    %     hold off
    %     axis([-0.1 2 -1.5 2 -1.5 2],'square')
    %     pause(0.1)
    % end

    % Computed results
    currentPos = D.model.listNodes{end}.position(:,end);
    currentR = D.model.listNodes{end}.R(:,end);
    currentForce = norm(D.model.listElementVariables{end}.value);
    actualSolution.pos = currentPos;
    actualSolution.rot = currentR;
    actualSolution.force = currentForce ;

    % Expected results
    posCheck = [1.000000000000007;-1.000000000000007;0.500000000000004];
    RCheck = [1;-2.447276457004514e-16;1.224671802150488e-16;2.447276457004514e-16;1;-1.117498432046728e-16;-1.224671802150488e-16;1.117498432046729e-16;1];
    forceCheck = 1.061279314140727e+02;
    expectedSolution.pos = posCheck;
    expectedSolution.rot = RCheck;
    expectedSolution.force = forceCheck;
end
