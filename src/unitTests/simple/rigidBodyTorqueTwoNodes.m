%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Javier Galvez (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [actualSolution, expectedSolution] = rigidBodyTorqueTwoNodes()

    close all
    clear all
    
    %% Definition of time parameters
    finalTime = 2.1;
    timeStepSize = 0.01;

    %% Definition of the nodes of the model (as matrix)
  nodes = [ 1 0 0 0;
            2 0 0 0;
            3 1 1 1];

    %% Definition of the elements of the model (as structure)
    count = 1;
    elements{count}.type = 'RigidBodyElement';
    elements{count}.nodes = [3 2];
    elements{count}.mass = 1; 
        
    count = size(elements,2)+1;
    elements{count}.type = 'KinematicConstraint';
    elements{count}.nodes = [1 2];
    elements{count}.A = [0 0 0 1 0 0]';
    
    count = size(elements,2)+1;
    elements{count}.type = 'ForceOnNode';
    elements{count}.nodes = 3;
    elements{count}.Axe = [0 0 0 1 1 1];
    elements{count}.following = false;
    elements{count}.F = [10]'; 
    
    %% Definition of the boundary conditions
    boundaryConditions = [1]; % Node 1 is fixed

    %% Definition of the finite element model (FEModel Object)
    Model = FEModel();
    Model.CreateFEModel(nodes,elements);
    Model.defineBC(boundaryConditions);

    %% Definition of the solver and its parameters
    tic % parameters is integration parameters
    dynamicIntegrator = DynamicIntegration(Model);
    dynamicIntegrator.parameters.finaltime = finalTime;
    dynamicIntegrator.parameters.timestepsize = timeStepSize;
    dynamicIntegrator.parameters.rho = 0.4;
    dynamicIntegrator.parameters.scaling = 1e6;
    % D.parameters.relTolRes = 1e-6;
    dynamicIntegrator.runIntegration();
    elapsed = toc;
    disp(['RigidBodyTorqueTwoNodes computation lasted ',num2str(elapsed),...
          ' s.'])
    
    %% visualization and plots
%      Model.visu

    %% Plots
%     endNodePos = Model.listNodes{end}.position;
%     figure
%     plot(dynamicIntegrator.parameters.time,endNodePos(1,:),...
%          dynamicIntegrator.parameters.time,endNodePos(2,:),....
%          dynamicIntegrator.parameters.time,endNodePos(3,:),'Linewidth',2)
%     grid on
%     title('Tip of the beam position')
%     xlabel('Time [s]')
%     ylabel('Poistion [m]')
%     legend('X','Y','Z')  
      
    %% Computed results
    currentPosition = dynamicIntegrator.model.listNodes{end}.position(:,end);
    currentRotation = dynamicIntegrator.model.listNodes{end}.R(:,end);
    actualSolution.pos = currentPosition; 
    actualSolution.rot = currentRotation; 
    
    % Expected results
    positionCheck = [1;0.976859841138501;-1.022616668537562];
    rotationCheck = [1;0;0;0;-0.022878413699530;0.999738254838031;0;-0.999738254838032;-0.022878413699530];

    expectedSolution.pos = positionCheck;
    expectedSolution.rot = rotationCheck;  
end