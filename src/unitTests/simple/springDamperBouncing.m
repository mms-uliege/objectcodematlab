%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Olivier Devigne (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [actualSolution, expectedSolution] = springDamperBouncing()
    % Two nodes attached by a spring. Node 1 is fixed at x1 = (0, 0, 0)
    % and node b is released from x2 = (1, 1, 1). The natural length of the
    % spring is l = 1 m.
    close all
    clear all
    
    %% Definition of time parameters
    finaltime = 1;
    timestepsize = 0.01;

    %% Definition of the nodes of the model (as matrix)
    nodes = [1 0 0 0;...
             2 1 1 1];

    %% Definition of the elements of the model (as structure)
    % Element 1: Rigid body
    count = 1;
    elements{count}.type = 'RigidBodyElement';
    elements{count}.nodes = 1;
    elements{count}.mass = 1;
    elements{count}.J = 0.01*eye(3);

    % Element 2: Rigid body
    count = size(elements,2)+1;
    elements{count}.type = 'RigidBodyElement';
    elements{count}.nodes = 2;
    elements{count}.mass = 1;
    elements{count}.J = 0.01*eye(3);
    elements{count}.acceleration = [0 0 -9.81];
    
    % Element 3: Spring-damper
    count = size(elements,2)+1;
    elements{count}.type = 'SpringDamperElement';
    elements{count}.nodes = [1 2];
    elements{count}.stiffness = 100;
    elements{count}.damping = 10;
    elements{count}.natural_length = 1;
    
    %% Definition of the boundary conditions
    BC = [1]; % Node 1 is fixed

    %% Definition of the finite element model (FEModel Object)
    % Based on previously defined nodes and elements
    Model = FEModel;
    Model.CreateFEModel(nodes,elements);
    Model.defineBC(BC);

    %% Definition of the solver and its parameters
    tic
    D = DynamicIntegration(Model);  % Creatind the Dynamic integration object
    D.parameters.timestepsize = timestepsize;
    D.parameters.finaltime = finaltime;
    D.parameters.rho = 0.2;
    D.parameters.scaling = 1e6;
    D.runIntegration(); % Run the integration of the object
    elapsed = toc;
    disp(['SpringDamperBouncing computation lasted ',num2str(elapsed),' s.'])

    %% visualization and plots
    % To visualize the model:(uncomment the following line)
    % Model.visu

    % % Plots
    % for i = 1:length(timeV)
    %     Hstart(:,:,i) = [vect9ToMat3(Model.listNodes{end}.R(:,i)) Model.listNodes{end}.position(:,i);0 0 0 1];
    % end
    % figure
    % for i = 1:length(timeV)
    %     Model.visu(i)
    %     hold on
    %     plotFrame(Hstart(:,:,i))
    %     hold off
    %     axis([-0.1 1.1 -0.1 1.1 -0.1 1.1],'square')
    %     pause(0.1)    
    % end

    
    % Computed results
    currentPos = D.model.listNodes{end}.position(:,end);
    currentR = D.model.listNodes{end}.R(:,end);
    currentForce = D.model.listElements{end}.refValForce;
    actualSolution.pos = currentPos; 
    actualSolution.rot = currentR; 
    actualSolution.force = currentForce ;
    
    % Expected results
    posCheck = [-0.687372088349049;-0.687372088349049;-0.772742430618280];
    RCheck = [1;-1.281708503535955e-15;-9.584083051525878e-16;1.281708503535955e-15;1;-1.103116378384038e-15;9.584083051525884e-16;1.103116378384037e-15;1];
    forceCheck = 40.334013528394610;
    expectedSolution.pos = posCheck;
    expectedSolution.rot = RCheck;
    expectedSolution.force = forceCheck; 
end