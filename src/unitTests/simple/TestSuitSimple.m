%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Javier Galvez (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Resources
% https://www.mathworks.com/help/matlab/matlab_prog/create-simple-test-suites.html
% https://www.mathworks.com/videos/matlab-unit-testing-framework-74975.html

classdef TestSuitSimple < matlab.unittest.TestCase
    
    properties (TestParameter)
    end
    
    methods (Test)
        function testImposeNodeMotionAngle(testCase)
            [actualSolution, expectedSolution] = imposeNodeMotionAngle();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.force, expectedSolution.force, 'AbsTol', 1e-4);
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testImposeNodeMotionTranslation(testCase)
            [actualSolution, expectedSolution] = imposeNodeMotionTranslation();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.force, expectedSolution.force, 'AbsTol', 1e-4);
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testTrajectoryConstraintJointWithPendulum(testCase)
            [actualSolution, expectedSolution] = trajectoryConstraintJointWithPendulum();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.force, expectedSolution.force, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', sqrt(eps));
        end

        function testTrajectoryConstraintWithPendulum(testCase)
            [actualSolution, expectedSolution] = trajectoryConstraintWithPendulum();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.force, expectedSolution.force, 'AbsTol', 1e-4);
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end

        function testFlexibleBeamTraction(testCase)
            [actualSolution, expectedSolution] = flexibleBeamTraction();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.force, expectedSolution.force, 'AbsTol', 1e-4);
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testFlexibleBeamBending(testCase)
            [actualSolution, expectedSolution] = flexibleBeamBending();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.force, expectedSolution.force, 'AbsTol', 1e-4);
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end        

        function testFlexibleBeamTorsion(testCase)
            [actualSolution, expectedSolution] = flexibleBeamTorsion();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.force, expectedSolution.force, 'AbsTol', 1e-4);
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testRigidBodyAccelerationOneNode(testCase)
            [actualSolution, expectedSolution] = rigidBodyAccelerationOneNode();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testRigidBodyTorqueTwoNodes(testCase)
            [actualSolution, expectedSolution] = rigidBodyTorqueTwoNodes();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testSpringDamperBouncing(testCase)
            [actualSolution, expectedSolution] = springDamperBouncing();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testForceOnNodeRigidBody(testCase)
            [actualSolution, expectedSolution] = forceOnNodeRigidBody();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testKinematicConstraintPrismaticX(testCase)
            [actualSolution, expectedSolution] = kinematicConstraintPrismaticX();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testKinematicConstraintPrismaticY(testCase)
            [actualSolution, expectedSolution] = kinematicConstraintPrismaticY();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testKinematicConstraintPrismaticZ(testCase)
            [actualSolution, expectedSolution] = kinematicConstraintPrismaticZ();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testKinematicConstraintRigidPendulum(testCase)
            [actualSolution, expectedSolution] = kinematicConstraintRigidPendulum();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testForceInKinematicConstraintForce(testCase)
            [actualSolution, expectedSolution] = forceInKinematicConstraintForce();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
        function testForceInKinematicConstraintTorque(testCase)
            [actualSolution, expectedSolution] = forceInKinematicConstraintTorque();
            testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
            testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
        end
        
%                 function xxxxx(testCase)
%                     [actualSolution, expectedSolution] = imposeNodeMotionAngle();
%                     testCase.verifyEqual(actualSolution.pos, expectedSolution.pos, 'AbsTol', sqrt(eps));
%                     testCase.verifyEqual(actualSolution.rot, expectedSolution.rot, 'AbsTol', sqrt(eps));
%                     testCase.verifyEqual(actualSolution.force, expectedSolution.force, 'AbsTol', 1e-4);
%                     testCase.verifyEqual(actualSolution, expectedSolution, 'AbsTol', 1e-4);
%                 end
        
    end
end
