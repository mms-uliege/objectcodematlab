%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Arthur Lismonde (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [actualSolution, expectedSolution] = trajectoryConstraintJointWithPendulum()
    % Trajectory constraint on joint angle of a pendulum (2 nodes rigid body) when
    % actuated by torque at same hinge joint
    close all
    clear all

    %% Definition of time parameters
    finaltime = 0.8;
    timestepsize = 0.01;


    %% Definition of the nodes of the model (as matrix)
    nodes = [1 0 0 0;...
        2 0 0 0;...
        3 1 1 0];

    %% Definition of the elements of the model (as structure)
    % Element 1: Rigid pendulum
    count = 1;
    elements{count}.type = 'RigidBodyElement';
    elements{count}.nodes = [3 2];
    elements{count}.mass = 1;
    elements{count}.J = 0.01*eye(3);

    % Element 2: Rigid pendulum
    count = size(elements,2)+1;
    elements{count}.type = 'KinematicConstraint';
    elements{count}.nodes = [1 2];
    elements{count}.A = [0 0 0 1 -1 0]'/norm([0 0 0 1 -1 0]);

    % Element 3: Imposed rotations on the first node
    timeV = 0:timestepsize:finaltime;
    traj = lineTraj(0,2*pi,timeV,0,finaltime);
    count = size(elements,2)+1;
    elements{count}.type = 'TrajectoryConstraintJoint';
    elements{count}.elements = 2;
    elements{count}.T = [traj];

    %% Definition of the boundary conditions

    BC = [1]; % Node 1 is fixed

    %% Definition of the finite element model (FEModel Object)
    % Based on previously defined nodes and elements

    Model = FEModel;
    Model.CreateFEModel(nodes,elements);
    Model.defineBC(BC);

    %% Definition of the solver and its parameters
    tic
    D = DynamicIntegration(Model);  % Creatind the Dynamic integration object
    D.parameters.timestepsize = timestepsize;
    D.parameters.finaltime = finaltime;
    D.parameters.rho = 0.2;
    D.parameters.scaling = 1e6;
    D.runIntegration(); % Run the integration of the object
    elapsed = toc;
    disp(['TrajectoryConstraintJointWithPendulum computation lasted ',num2str(elapsed),' s.'])

    %% visualization and plots
    % To visualize the model:(uncomment the following line)
    % Model.visu

    %% Computed results
    currentPos = D.model.listNodes{end}.position(:,end);
    currentR = D.model.listNodes{end}.R(:,end);
    currentForce = norm(D.model.listElementVariables{end}.value);
    actualSolution.pos = currentPos; 
    actualSolution.rot = currentR; 
    actualSolution.force = currentForce ;
    
    % Expected results
    posCheck = [1.000000000000001;1.000000000000001;6.494155612709685e-14];
    RCheck = [1.000000000000000;5.281232943723291e-16;-3.254991498244283e-14;5.756800272443208e-16;1.000000000000000;-3.272733450357230e-14;3.236594339151077e-14;3.257559759848336e-14;1.000000000000000];
    forceCheck = 8.935407555186332e+02;
    expectedSolution.pos = posCheck;
    expectedSolution.rot = RCheck;
    expectedSolution.force = forceCheck;
end