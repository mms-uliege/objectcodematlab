%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Olivier Devigne (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [actualSolution, expectedSolution] = kinematicConstraintRigidPendulum()
    % Pendulum with spherical joint under gravity and a torque around z.
    % Initially x1 = (0,0,0) and x2 = (1,1,0)


    close all
    clear all
    
    %% Definition of time parameters
    finaltime = 2;
    timestepsize = 0.01;

    %% Definition of the nodes of the model (as matrix)
    nodes = [1 0 0 0
             2 0 0 0
             3 1 1 0];

    %% Definition of the elements of the model (as structure)
    % Element 1: Rigid pendulum
    count = 1;
    elements{count}.type = 'RigidBodyElement';
    elements{count}.nodes = [3 2];
    elements{count}.mass = 1;
    elements{count}.J = 0.01*eye(3);
    elements{count}.acceleration = [0 0 -9.81];

    % Element 2: Spherical joint
    count = size(elements,2)+1;
    elements{count}.type = 'KinematicConstraint';
    elements{count}.nodes = [1 2];
    elements{count}.A = [0 0 0 1 0 0;
                         0 0 0 0 1 0;
                         0 0 0 0 0 1]';
    
    % Element 3: Force
    count = size(elements,2)+1;
    elements{count}.type = 'ForceOnNode';
    elements{count}.nodes = 2;
    elements{count}.Axe = [0 0 0 0 0 1];
    elements{count}.following = false;
    elements{count}.F = [10]';
    
    %% Definition of the boundary conditions
    BC = [1];

    %% Definition of the finite element model (FEModel Object)
    % Based on previously defined nodes and elements
    Model = FEModel;
    Model.CreateFEModel(nodes,elements);
    Model.defineBC(BC);

    %% Definition of the solver and its parameters
    tic
    D = DynamicIntegration(Model);  % Creating the Dynamic integration object
    D.parameters.timestepsize = timestepsize;
    D.parameters.finaltime = finaltime;
    D.parameters.rho = 0.2;
    D.parameters.scaling = 1e6;
    D.runIntegration(); % Run the integration of the object
    elapsed = toc;
    disp(['KinematicConstraintRigidPendulum computation lasted ',num2str(elapsed),' s.'])

    %% visualization and plots
    % To visualize the model:(uncomment the following line)
    % Model.visu

    % % Plots
    % for i = 1:length(timeV)
    %     Hstart(:,:,i) = [vect9ToMat3(Model.listNodes{end}.R(:,i)) Model.listNodes{end}.position(:,i);0 0 0 1];
    % end
    % figure
    % for i = 1:length(timeV)
    %     Model.visu(i)
    %     hold on
    %     plotFrame(Hstart(:,:,i))
    %     hold off
    %     axis([-0.1 1.1 -0.1 1.1 -0.1 1.1],'square')
    %     pause(0.1)    
    % end

    
    % Computed results
    currentPos = D.model.listNodes{end}.position(:,end);
    currentR = D.model.listNodes{end}.R(:,end);
    actualSolution.pos = currentPos; 
    actualSolution.rot = currentR; 
    
    % Expected results
    posCheck = [-1.048580993975671;-0.085171027890459;-0.945105176729594];
    RCheck = [-0.051305517478455;-0.997275476497217;-0.053003470202597;-0.033865510412001;-0.051305517478457;0.998108646932189;-0.998108646932189;0.053003470202596;-0.031140986909218];
    expectedSolution.pos = posCheck;
    expectedSolution.rot = RCheck;
end