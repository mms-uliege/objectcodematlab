%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Javier Galvez (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [actualSolution, expectedSolution] = flexibleBeamTraction()

    close all
    clear all
    
    %% Definition of time parameters
    finalTime = 0.1;
    timeStepsize = 0.01;

    %% Definition of the nodes of the model (as matrix)
    nodes = [1 0 0 0;
             2 1 1 1];

    %% Definition of the elements of the model (as structure)
    
    % Beam parameters
    a = 0.003;
    b = 0.01;
    rho = 2000;

    E = 200e9;   % Young modulus
    nu = 0.3;   % Poisson modulus
    G = E/(2*(1+nu));
    A = a*b; 
    Ixx = a*b*(a^2+b^2)/12; 
    Iyy = b*a^3/12;
    Izz = a*b^3/12;

    KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
    MCS = diag(rho*[A A A Ixx Iyy Izz]);

    count = 1;
    type = 'FlexibleBeamElement';
    elements{count}.type = type;
    elements{count}.nodes = [1 2];
    elements{count}.yAxis = [0 1 0];
    elements{count}.KCS = KCS;
    elements{count}.MCS = MCS;
    elements{count}.alpha = 4.5896e-4; % damping proportional to Mass 4.5896e-4
    elements{count}.beta = 0.01329; % damping proportional to Stiffness 0.1329

    count = size(elements,2)+1;
    elements{count}.type = 'ForceOnNode';
    elements{count}.nodes = 2;
    elements{count}.Axe = [1 1 1 0 0 0];
    elements{count}.following = false;
    elements{count}.F = 1000;   
    
    %% Definition of the boundary conditions
    boundaryConditions = [1]; % Node 1 is fixed

    %% Definition of the finite element model (FEModel Object)
    Model = FEModel();
    Model.CreateFEModel(nodes,elements);
    Model.defineBC(boundaryConditions);

    %% Definition of the solver and its parameters
    tic % parameters is integration parameters
    dynamicIntegrator = DynamicIntegration(Model);
    dynamicIntegrator.parameters.finaltime = finalTime;
    dynamicIntegrator.parameters.timestepsize = timeStepsize;
    dynamicIntegrator.parameters.rho = 0.4;
    dynamicIntegrator.parameters.scaling = 1e6;
    % D.parameters.relTolRes = 1e-6;
    dynamicIntegrator.runIntegration();
    elapsed = toc;
    disp(['FlexibleBeamTraction computation lasted ',num2str(elapsed),...
          ' s.'])
    
    %% visualization and plots
    %Model.visu

    %% Plots
%     endNodePos = Model.listNodes{end}.position;
%     figure
%     plot(dynamicIntegrator.parameters.time,endNodePos(1,:),...
%          dynamicIntegrator.parameters.time,endNodePos(2,:),....
%          dynamicIntegrator.parameters.time,endNodePos(3,:),'Linewidth',2)
%     grid on
%     title('Tip of the beam position')
%     xlabel('Time [s]')
%     ylabel('Poistion [m]')
%     legend('X','Y','Z')  
      
    %% Computed results
    currentPosition = dynamicIntegrator.model.listNodes{end}.position(:,end);
    currentRotation = dynamicIntegrator.model.listNodes{end}.R(:,end);
    currentForce = dynamicIntegrator.model.listElements{1}.refValForce;
    actualSolution.pos = currentPosition; 
    actualSolution.rot = currentRotation;
    actualSolution.force = currentForce;
    
    % Expected results
    positionCheck = [1.000288365958095;1.000288365958324;1.000288365958254];
    rotationCheck = [1;-1.570246736073967e-13;-1.021784708610597e-13;1.570246736074003e-13;1;4.185517847200552e-14;1.021784708610544e-13;-4.185517847201491e-14;1];
    forceCheck = 2.447027654325832e+03;
    expectedSolution.pos = positionCheck;
    expectedSolution.rot = rotationCheck;
    expectedSolution.force = forceCheck;
end