%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Arthur Lismonde (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [actualSolution, expectedSolution] = imposeNodeMotionAngle()
    % Impose node rotation of a 2 nodes rigid body (free in space).
    close all
    clear all

    %% Definition of time parameters
    finaltime = 0.8;
    timestepsize = 0.01;


    %% Definition of the nodes of the model (as matrix)
    nodes = [1 0 0 0;...
             2 1 1 0];

    %% Definition of the elements of the model (as structure)
    % Element 1: Rigid pendulum
    count = 1;
    elements{count}.type = 'RigidBodyElement';
    elements{count}.nodes = [1 2];
    elements{count}.mass = 1;
    elements{count}.J = 0.01*eye(3);


    % Element 2: Imposed rotations on the first node
    timeV = 0:timestepsize:finaltime;
    traj = lineTraj(0,pi/2,timeV,0,finaltime);
    count = size(elements,2)+1;
    elements{count}.type = 'ImposeNodeMotion';
    elements{count}.nodes = 1;
    elements{count}.Axe = [0 0 0 1 0 0;...
                           0 0 0 0 1 0;...
                           0 0 0 0 0 1];
    elements{count}.T = [traj;...
                         -traj;...
                         0.5*traj];

    %% Definition of the boundary conditions

    BC = []; % No node are fixed

    %% Definition of the finite element model (FEModel Object)
    % Based on previously defined nodes and elements

    Model = FEModel;
    Model.CreateFEModel(nodes,elements);
    Model.defineBC(BC);

    %% Definition of the solver and its parameters
    tic
    D = DynamicIntegration(Model);  % Creatind the Dynamic integration object
    D.parameters.timestepsize = timestepsize;
    D.parameters.finaltime = finaltime;
    D.parameters.rho = 0.2;
    D.parameters.scaling = 1e6;
    D.runIntegration(); % Run the integration of the object
    elapsed = toc;
    disp(['ImposeNodeMotionAngle computation lasted ',num2str(elapsed),' s.'])

    %% visualization and plots
    % To visualize the model:(uncomment the following line)
    % Model.visu

    % % Plots
    % for i = 1:length(timeV)
    %     endNodeAngle(:,i) = logSO3(vect9ToMat3(Model.listNodes{end}.R(:,i)));
    %     startNodeAngle(:,i) = logSO3(vect9ToMat3(Model.listNodes{2}.R(:,i)));
    %     Hend(:,:,i) = [vect9ToMat3(Model.listNodes{end}.R(:,i)) Model.listNodes{end}.position(:,i);0 0 0 1];
    %     Hstart(:,:,i) = [vect9ToMat3(Model.listNodes{2}.R(:,i)) Model.listNodes{2}.position(:,i);0 0 0 1];
    % end
    % leg = {};
    % figure
    % hold on
    % plot(D.parameters.time,endNodeAngle(1,:),D.parameters.time,endNodeAngle(2,:),D.parameters.time,endNodeAngle(3,:),'Linewidth',2)
    % leg = [leg 'RX_{end}','RY_{end}','RZ_{end}'];
    % set(gca,'ColorOrderIndex',1)
    % plot(D.parameters.time,startNodeAngle(1,:),D.parameters.time,startNodeAngle(2,:),D.parameters.time,startNodeAngle(3,:),'Linewidth',2)
    % leg = [leg 'RX_{end}','RY_{end}','RZ_{end}'];
    % plot(timeV,elements{end}.T,'k')
    % leg = [leg 'RX_{d}','RY_{d}','RZ_{d}'];
    % grid on
    % title('Angle of nodes')
    % xlabel('Time [s]')
    % ylabel('Angle [m]')
    % legend(leg)

    % figure
    % for i = 1:length(timeV)
    %     Model.visu(i)
    %     hold on
    %     plotFrame(Hend(:,:,i))
    %     plotFrame(Hstart(:,:,i))
    %     hold off
    %     axis([-0.1 2 -1.5 2 -1.5 2],'square')
    %     pause(0.1)    
    % end

    % Check results
%     posCheck = [-0.942809041582056;-0.471404520791016;0.942809041582080];
%     RCheck = [0.0516073437852579;-0.994416385367313;-0.0920474583051429;-0.523011864576274;0.0516073437852579;-0.850761583276936;0.850761583276937;0.0920474583051430;-0.517428249943587];
% 
%     currentPos = D.model.listNodes{end}.position(:,end);
%     currentR = D.model.listNodes{end}.R(:,end);
%     currentForce = norm(D.model.listElementVariables{end}.value);
%     testCase = TestCase.forInteractiveUse;
%     disp(['Testing imposeNodeMotionAngle for position:'])
%     testCase.verifyThat(currentPos, IsEqualTo(posCheck, 'Within', AbsoluteTolerance(1e-9)))
%     disp('Testing imposeNodeMotionAngle for rotation:')
%     testCase.verifyThat(currentR, IsEqualTo(RCheck, 'Within', AbsoluteTolerance(1e-9)))
    
    % Computed results
    currentPos = D.model.listNodes{end}.position(:,end);
    currentR = D.model.listNodes{end}.R(:,end);
    currentForce = norm(D.model.listElementVariables{end}.value);
    actualSolution.pos = currentPos; 
    actualSolution.rot = currentR; 
    actualSolution.force = currentForce ;
    
    % Expected results
    posCheck = [-0.942809041582070;-0.471404520791047;0.942809041582048];
    RCheck = [0.051607343785245;-0.994416385367315;-0.092047458305122;-0.523011864576292;0.051607343785245;-0.850761583276926;0.850761583276927;0.092047458305122;-0.517428249943609];
    forceCheck = 1.667053649047772;
    expectedSolution.pos = posCheck;
    expectedSolution.rot = RCheck;
    expectedSolution.force = forceCheck;    
end
