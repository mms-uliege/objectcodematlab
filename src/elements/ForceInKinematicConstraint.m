%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef ForceInKinematicConstraint < Element
    % FORCEINKINEMATICCONSTRAINT
    % Applies an external force on a joint element
    % The force vector f is applied on the only axis of the joint element
    % in "elements". (used to apply joint torques for example)
    % In this second version, a PD feedback control on the position of the
    % joint can be added
    
    properties
        f;
        timeref = 0;
        indextime = 1;
        elements;
        ref = []; % Reference trajectory that should be used (contribution will be added to the f vector/value)
        dref = []; % Reference velocity of the joint (derivative of dref)
        kp = 0; % proportional gain
        kd = 0; % derivative gain
    end
    
    methods
        function obj = ForceInKinematicConstraint(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,~,listElements)
            obj.initListElementDof.relativeNodeSE3 = listElements+0.01;
            obj.initListElementVar.relativeNodeSE3 = listElements;
        end
        
        function resModel = computeResidueStatic(obj,resModel,parameters)
            if obj.timeref ~= parameters.currentTime
                obj.indextime = obj.indextime+1;
                obj.timeref = parameters.currentTime;
            end
            resModel(1) = resModel(1) - obj.f(obj.indextime);
            obj.refValForce = norm(obj.f);
            if ~isempty(obj.ref)
                err = -(obj.listElementVariables.relativeNodeSE3{1}.relCoo(obj.indextime) - obj.ref(obj.indextime));
                resModel(1) = resModel(1) - obj.kp*err;
                obj.refValForce = obj.refValForce + norm(obj.kp*err);
            end
            if ~isempty(obj.dref)
                derr = -(obj.listElementVariables.relativeNodeSE3{1}.velocity(obj.indextime) - obj.dref(obj.indextime));
                resModel(1) = resModel(1) - obj.kd*derr;
                obj.refValForce = obj.refValForce + norm(obj.kd*derr);
            end
        end
        
        function StModel = computeTangentOperatorStatic(obj,StModel,parameters)
            StModel = StModel + parameters.coefK*(obj.kp) + parameters.gammap*(obj.kd);
        end
    end
end

