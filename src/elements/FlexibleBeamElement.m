%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef FlexibleBeamElement < Element
    
    properties
        O0;
        M;
        MCS;
        Gyro = zeros(12);
        KCS;
        C; % Damping matrix proportional to M and K by C = alpha*M + beta*K
        P;
        G;
        h;
        h0;
        L;
        A;
        Jxx;
        Iyy;
        Izz;
        E;
        nu;
        rho;
        v_AB;
        dv_AB;
        alpha=0;
        beta=0;
        acceleration = [0 0 0]; % adding an acceleration term, normally used to add gravity
        yAxis=[]; % 3x1 Vector, it is the cross section Y axis in the local frame of the beam to define inertia
    end
    
    methods
        function obj = FlexibleBeamElement(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,listNodes,~)
            obj.initListNodalDof.nodeSE3 = [listNodes(1)+(0.01:0.01:0.06) listNodes(2)+(0.01:0.01:0.06)];
        end
        
        function initializeStatic(obj,nstep)
            obj.h0 = zeros(6,1);
            obj.h0(1:3) = obj.listNodes{2}.position(:,1) - obj.listNodes{1}.position(:,1);
            obj.h = obj.h0;
            obj.L = norm(obj.h0(1:3));
            t = obj.h0(1:3)/obj.L;
            if isempty(obj.yAxis)
                [n,b] = vectHooke(t);
            else
                [n,b] = vectHooke(t,obj.yAxis);
            end
            obj.O0 = [t n b];
            O02 = [obj.O0 zeros(3); zeros(3) obj.O0];
            if isempty(obj.KCS)
                obj.KCS = diag([obj.E*obj.A ...
                    obj.E/(2*(1+obj.nu))*obj.A*5/6 ...
                    obj.E/(2*(1+obj.nu))*obj.A*5/6 ...
                    obj.E/(2*(1+obj.nu))*obj.Jxx ...
                    obj.E*obj.Iyy ...
                    obj.E*obj.Izz]);
            end
            obj.KCS = O02*obj.KCS*O02';
            T = computeTSE3inv(obj.h0);
            obj.P = [hatSE3(obj.h0)-T T];
            if isempty(obj.MCS)
                obj.MCS = diag(obj.rho*[obj.A obj.A obj.A obj.Jxx obj.Iyy obj.Izz]);
            end
            obj.MCS = [obj.O0 zeros(3); zeros(3) obj.O0]*obj.MCS*[obj.O0' zeros(3); zeros(3) obj.O0'];
            gEffA = rotateVect3(obj.acceleration, vect9ToMat3(obj.listNodes{1}.R(:,nstep)), nstep);
            % Acceleration term integrated exactly
            obj.G = integration1D(@(s) fcnQ(s,obj.h)'*obj.MCS*[expSO3(s*obj.h(4:6))'*gEffA;0;0;0],3,0,1)*obj.L;
        end
        
        function obj = initializeDynamic(obj,nstep)
            obj.initializeStatic(nstep);
            obj.M = integration1D(@(s) fcnQ(s,obj.h0)'*obj.MCS*fcnQ(s,obj.h0),3,0,1)*obj.L;
            if obj.alpha || obj.beta
                obj.C = obj.alpha*obj.MCS + obj.beta*obj.KCS;
            end
        end
        
        function computeTermsStatic(obj,parameters)
            RAT       = vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep))';
            xA        = obj.listNodes{1}.position(:,parameters.timestep);
            RB        = vect9ToMat3(obj.listNodes{2}.R(:,parameters.timestep));
            xB        = obj.listNodes{2}.position(:,parameters.timestep);
            obj.h     = logSE3([RAT*RB RAT*(xB-xA); 0 0 0 1]);
            T = computeTSE3inv(obj.h);
            obj.P = [hatSE3(obj.h)-T T];
            gEffA = rotateVect3(obj.acceleration, vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep)), parameters.timestep);
            % Gravity term integrated exactly
            obj.G = integration1D(@(s) fcnQ(s,obj.h)'*obj.MCS*[expSO3(s*obj.h(4:6))'*gEffA;0;0;0],3,0,1)*obj.L;
        end
        
        function computeTermsDynamic(obj, parameters)
            obj.computeTermsStatic(parameters);
            obj.M     = integration1D(@(s) fcnQ(s,obj.h)'*obj.MCS*fcnQ(s,obj.h),3,0,1)*obj.L;
            obj.v_AB  = [obj.listNodes{1}.velocity(:,parameters.timestep);     obj.listNodes{2}.velocity(:,parameters.timestep)];
            obj.dv_AB = [obj.listNodes{1}.acceleration(:,parameters.timestep); obj.listNodes{2}.acceleration(:,parameters.timestep)];
            obj.Gyro  =  integration1D(@(s) fcnQ(s,obj.h)'*(obj.MCS*fcnQdot(s,obj.h,obj.P*obj.v_AB) - hatSE3(fcnQ(s,obj.h)*obj.v_AB)'*obj.MCS*fcnQ(s,obj.h)),3,0,1)*obj.L;
        end
        
        function resModel = computeResidueStatic(obj,resModel,~)
            resModel = resModel - obj.G;
            resModel = resModel + obj.P'*obj.KCS*((obj.h-obj.h0)/obj.L);
            obj.refValForce = norm(obj.P'*obj.KCS*((obj.h-obj.h0)/obj.L));
        end
        
        function resModel = computeResidueDynamic(obj,resModel,parameters)
            resModel = computeResidueStatic(obj,resModel,parameters);
            
            resModel = resModel + obj.M*obj.dv_AB + obj.Gyro*obj.v_AB;
            obj.refValForce = obj.refValForce + norm(obj.M*obj.dv_AB + obj.Gyro*obj.v_AB);
            
            if obj.alpha || obj.beta
                resModel = resModel + (obj.P'*obj.C*obj.P)*obj.v_AB/obj.L;
                obj.refValForce = obj.refValForce + norm((obj.P'*obj.C*obj.P)*obj.v_AB/obj.L);
            end
        end
        
        function StModel = computeTangentOperatorStatic(obj,StModel,parameters)
            StModel = StModel + parameters.coefK*((obj.P'*obj.KCS*obj.P)/obj.L);
        end
        
        function StModel = computeTangentOperatorDynamic(obj,StModel,parameters)
            StModel = computeTangentOperatorStatic(obj,StModel,parameters);
            
            StModel = StModel + parameters.betap*obj.M;
            StModel = StModel + parameters.gammap*obj.Gyro;
            
            if obj.alpha || obj.beta
                StModel = StModel + parameters.gammap*(obj.P'*obj.C*obj.P)/obj.L;
            end
        end
        
        function visu(obj,step)
            RA = vect9ToMat3(obj.listNodes{1}.R(:,step));
            RB = vect9ToMat3(obj.listNodes{2}.R(:,step));
            xA = obj.listNodes{1}.position(:,step);
            xB = obj.listNodes{2}.position(:,step);
            HA  = [RA xA; 0 0 0 1]; HB = [RB xB; 0 0 0 1];
            plot3(xA(1),xA(2),xA(3),'r*')
            hold on
            plot3(xB(1),xB(2),xB(3),'r*')
            np = 10;
            hi = logSE3(HA\HB);
            xp = zeros(3,np+2); xp(:,1) = HA(1:3,4);
            for j = 1:np+1
                jj = j/(np+1);
                Hp = HA*expSE3(jj*hi);
                xp(:,j+1) = Hp(1:3,4);
            end
            
            plot3(xp(1,:),xp(2,:),xp(3,:),'r','LineWidth',obj.MCS(1,1))
        end
    end
end