%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef ImposeRelativeCoordinateMotion < Element
    
    properties
        ImposeMotion;
        elements;
        nDof;
        dServodq;
    end
    
    methods
        function obj = ImposeRelativeCoordinateMotion(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,~,listElements)
            obj.nDof = size(obj.ImposeMotion,1);
            for i = 1:obj.nDof
                obj.initListElementDof.relativeNodeSE3(i) = listElements(i)+0.01;
                obj.initListElementVar.relativeNodeSE3(i) = listElements(i);
            end
            obj.initListElementDof.lagrangeMultiplierSet = obj.elementNumber+(0.01:0.01:(0.01*obj.nDof));
            obj.initListElementVar.lagrangeMultiplierSet = obj.elementNumber;
        end
        
        function initializeStatic(obj,~)
            obj.dServodq = eye(obj.nDof);
        end
        
        function resModel = computeResidueStatic(obj,resModel,parameters)
            trajConstraint = zeros(obj.nDof,1);
            for i = 1:obj.nDof
                trajConstraint(i) = obj.listElementVariables.relativeNodeSE3{i}.relCoo(:,parameters.timestep)-obj.ImposeMotion(i,parameters.timestep);
            end
            resModel(1:obj.nDof) = resModel(1:obj.nDof) + obj.dServodq*obj.listElementVariables.lagrangeMultiplierSet{1}.value(:,parameters.timestep);
            obj.refValForce = norm(obj.dServodq*obj.listElementVariables.lagrangeMultiplierSet{1}.value(:,parameters.timestep));
            resModel(end-(obj.nDof-1):end) = resModel(end-(obj.nDof-1):end) + parameters.scaling*(trajConstraint);
            obj.refValPhi = norm(trajConstraint);
        end
        
        function StModel = computeTangentOperatorStatic(obj,StModel,parameters)
            StModel(1:obj.nDof,end-(obj.nDof-1):end) = StModel(1:obj.nDof,end-(obj.nDof-1):end) + parameters.scaling*obj.dServodq;
            StModel(end-(obj.nDof-1):end,1:obj.nDof) = StModel(end-(obj.nDof-1):end,1:obj.nDof) + parameters.scaling*obj.dServodq;
        end
    end  
end

