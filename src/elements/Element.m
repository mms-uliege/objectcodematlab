%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef Element < handle
    
    properties
        elementNumber = 0;
        
        sizeRes = [];
        energy;
        refValForce = 0;
        refValPhi = 0;
        res = [];
        St = [];
        
        PC = [];      
        
        listNumberNodes = [];
        listNumberDof = [];
                      
        listElementVariables;
        listNodes = {};
    end
    
    properties (Hidden = true)
        initListElementDof;
        initListElementVar;
        initListNodalDof;
    end
    
    properties (Constant = true, Hidden = true)
        refE = 1e-3; %max number of DOF per Element = 1/ref
        refN = 6; %max number of DOF per Node = refN
    end
    
    methods
        function obj = Element(input)
            obj.elementNumber = input;
            obj.initListNodalDof = ListVariables(NodalVariableTypes);
            obj.initListElementDof = ListVariables(ElementVariableTypes);
            obj.initListElementVar = ListVariables(ElementVariableTypes); 
            obj.listElementVariables = ListVariables(ElementVariableTypes);
        end
        
        function defineDof(obj,listNodes,listElements)
        end
        
        function initializeStatic(obj,nstep)
        end
        
        function initializeDynamic(obj,nstep)
            obj.initializeStatic(nstep);
        end
        
        function resModel = computeResidueStatic(obj,resModel,parameters)
        end
        
        function resModel = computeResidueDynamic(obj,resModel,parameters)
            resModel = computeResidueStatic(obj,resModel,parameters);
        end

        function StModel = computeTangentOperatorStatic(obj,StModel,parameters)
        end
        
        function StModel = computeTangentOperatorDynamic(obj,StModel,parameters)
            StModel = computeTangentOperatorStatic(obj,StModel,parameters);
        end
        
        function computeTermsStatic(obj,parameters)
        end
        
        function computeTermsDynamic(obj, parameters)
            computeTermsStatic(obj, parameters);
        end
        
        function visu(obj,step)          
        end
    end
end

