%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Arthur Lismonde (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef ImposeNodeMotion < Element
    % Element constraining the trajectory T (6DOF) of a node, without specifying any element as atuator.
    
    properties
        Axe; % DOFs to be driven ([1 0 0 0 0 0; 0 1 0 0 0 0] means DOFs "x" and "y")
        nDof;
        dPhidq;
        alpha0 = 0;
        T;
    end
    
    methods
        function obj = ImposeNodeMotion(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,listNodes,~)
            obj.nDof = size(obj.Axe,1); % initialize the number of degree of freedom to be commanded
            obj.initListNodalDof.nodeSE3 = zeros(1,6);
            obj.initListNodalDof.nodeSE3(1:6) = listNodes(1)+(0.01:0.01:0.06);
            
            obj.initListElementDof.lagrangeMultiplierSet = obj.elementNumber+(0.01:0.01:(0.01*obj.nDof));
            obj.initListElementVar.lagrangeMultiplierSet = obj.elementNumber;  
        end
        
        function initializeStatic(obj,~)
            if size(obj.Axe,2) == 3
                obj.Axe = [obj.Axe zeros(obj.nDof,3)];
            end
            obj.dPhidq = obj.Axe;
        end
        
        function resModel = computeResidueStatic(obj,resModel,parameters)
            RA = vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep));
            xA = obj.listNodes{1}.position(:,parameters.timestep);
            HA = [RA xA; 0 0 0 1];
            hd = obj.Axe'*obj.T(:,parameters.timestep);
            Hd = [expSO3(hd(4:6)) hd(1:3);0 0 0 1];
            xd = Hd(1:3,4);
            Rd = Hd(1:3,1:3);
            HdInv = [Rd' -Rd'*xd; 0 0 0 1];
            
            obj.dPhidq(1:obj.nDof,1:6) = obj.Axe*[Rd'*RA zeros(3); zeros(3) Rd'*RA];
            motionConstraint = obj.Axe*vectSE3(HdInv*HA);
            resModel(6+(1:obj.nDof)) = resModel(6+(1:obj.nDof)) + parameters.scaling*(motionConstraint);  % "Constraint" level
            obj.refValPhi = norm(motionConstraint);
            resModel(1:6) = resModel(1:6) + obj.dPhidq'*obj.listElementVariables.lagrangeMultiplierSet{1}.value(:,parameters.timestep);
            obj.refValForce = norm(obj.dPhidq'*obj.listElementVariables.lagrangeMultiplierSet{1}.value(:,parameters.timestep));
        end
        
        function StModel = computeTangentOperatorStatic(obj,StModel,parameters)
            StModel(1:6,6+(1:obj.nDof)) = StModel(1:6,6+(1:obj.nDof)) + parameters.scaling*obj.dPhidq';
            StModel(6+(1:obj.nDof),1:6) = StModel(6+(1:obj.nDof),1:6) + parameters.scaling*obj.dPhidq;
        end
    end
end

