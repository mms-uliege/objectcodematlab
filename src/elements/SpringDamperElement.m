%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef SpringDamperElement < Element
    
    properties
        damping;
        stiffness;
        natural_length;
        h0;
        h;
        L;
        BT;
        xA;
        xB;
        e1;
    end
    
    methods
        function obj = SpringDamperElement(input)
            obj = obj@Element(input);
        end
        function defineDof(obj,listNodes,~)
            obj.initListNodalDof.nodeSE3 = [listNodes(1)+(0.01:0.01:0.06) listNodes(2)+(0.01:0.01:0.06)];
        end

        function initializeStatic(obj,~)
            obj.h0 = zeros(6,1);
            obj.h0(1:3) = obj.listNodes{2}.position(:,1) - obj.listNodes{1}.position(:,1);
            if isempty(obj.natural_length)
                obj.natural_length = norm(obj.h0(1:3));
            else
                obj.h0(1:3) = obj.natural_length*obj.h0(1:3)/norm(obj.h0(1:3));
            end
        end
        
        function obj = initializeDynamic(obj,nstep)
            obj.initializeStatic(nstep);
        end
        
        function resModel = computeResidueStatic(obj,resModel,~)            
            resModel = resModel + obj.BT*obj.stiffness*(norm(obj.xB-obj.xA)-obj.natural_length);
            obj.refValForce = norm(obj.BT*obj.stiffness*(norm(obj.xB-obj.xA)-obj.natural_length));
        end
        
        function resModel = computeResidueDynamic(obj,resModel,parameters)
            resModel = computeResidueStatic(obj,resModel,parameters);
            RA = vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep));
            RBT = vect9ToMat3(obj.listNodes{2}.R(:,parameters.timestep))';
            dv_AB = blkdiag(RA, RA) * blkdiag(RBT, RBT) * obj.listNodes{2}.velocity(:,parameters.timestep) - obj.listNodes{1}.velocity(:,parameters.timestep);
            dv_AB = obj.e1'*dv_AB(1:3);
            resModel = resModel + obj.BT*obj.damping*dv_AB;
            obj.refValForce = obj.refValForce + norm(obj.BT*obj.damping*dv_AB);
        end
        
        function StModel = computeTangentOperatorStatic(obj,StModel,parameters)
            StModel = StModel + parameters.coefK*obj.BT*obj.stiffness*obj.BT';
        end

        function StModel = computeTangentOperatorDynamic(obj,StModel,parameters)
            StModel = computeTangentOperatorStatic(obj,StModel,parameters);
            StModel = StModel + parameters.gammap*obj.BT*obj.damping*obj.BT';
        end
        
        function computeTermsStatic(obj,parameters)
            obj.xA = obj.listNodes{1}.position(:,parameters.timestep);
            obj.xB = obj.listNodes{2}.position(:,parameters.timestep);
            obj.e1 = (obj.xB-obj.xA)/norm(obj.xB-obj.xA);
            RAT = vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep))';
            RBT = vect9ToMat3(obj.listNodes{2}.R(:,parameters.timestep))';
            obj.BT = [-RAT*obj.e1; 0; 0; 0 ; RBT*obj.e1; 0; 0; 0];     
        end
        
        function visu(obj,step)
            x1 = obj.listNodes{1}.position(:,step);
            x2 = obj.listNodes{2}.position(:,step);
            plot3([x1(1) x2(1)],[x1(2) x2(2)], [x1(3) x2(3)],'-ok','Linewidth',1.5)
        end 
    end
end

