%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef RigidBodyElement < Element

    properties
        mass = 0; 
        J = zeros(3);
        M = zeros(6);
        sizePhi; 
        dPhidq; 
        X;
        acceleration = [0 0 0]; % adding an acceleration term, normally used to add gravity
    end
    
    methods
        function obj = RigidBodyElement(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,listNodes,~)
            nDof = 6*length(listNodes);
            obj.initListNodalDof.nodeSE3 = zeros(1,nDof);
            for i = 1:length(listNodes)
                obj.initListNodalDof.nodeSE3((1:6)+(i-1)*6) = listNodes(i)+(0.01:0.01:0.06);
            end
            obj.initListElementDof.lagrangeMultiplierSet = obj.elementNumber+(0.01:0.01:(0.01*(nDof-6)));
            obj.initListElementVar.lagrangeMultiplierSet = obj.elementNumber;
        end
        
        function initializeStatic(obj,nstep)
            obj.sizeRes = length(obj.listNodes)*6; obj.sizePhi = obj.sizeRes-6;
            
            obj.dPhidq = zeros(obj.sizePhi,obj.sizeRes);
            if obj.sizePhi
                x0 = obj.listNodes{1}.position(:,1);
                obj.X = zeros(3,obj.sizePhi/6);
                for i = 1:(obj.sizePhi/6)
                    obj.X(:,i) = obj.listNodes{i+1}.position(:,1)-x0;
                    obj.dPhidq((1:6)+6*(i-1),[1:6 (1:6)+6*i]) = [[eye(3) tild(-obj.X(:,i)); zeros(3) eye(3)] -eye(6)];
                end
            end
            obj.energy = zeros(1,nstep+1);
        end
        
        function obj = initializeDynamic(obj,nstep)
            obj.initializeStatic(nstep);
            obj.M(1:3,1:3) = obj.mass*eye(3);
            obj.M(4:6,4:6) = obj.J;
        end
        
        function resModel = computeResidueStatic(obj,resModel,parameters)
            obj.refValForce = 0;
            if obj.sizePhi
                R0 = vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep));
                x0 = obj.listNodes{1}.position(:,parameters.timestep);
                H0 = [R0 x0; 0 0 0 1];
                for i = 1:(obj.sizePhi/6)
                    RT = vect9ToMat3(obj.listNodes{i+1}.R(:,parameters.timestep))';
                    x = obj.listNodes{i+1}.position(:,parameters.timestep);
                    Hm1 = [RT RT*(-x); 0 0 0 1];
                    Hi = [eye(3) obj.X(:,i); 0 0 0 1];
                    resModel(obj.sizeRes+(1:6)+6*(i-1)) = resModel(obj.sizeRes+(1:6)+6*(i-1)) + parameters.scaling*vectSE3(Hm1*H0*Hi);
                end
                obj.refValPhi = norm(vectSE3(Hm1*H0*Hi));
                resModel(1:obj.sizeRes) = resModel(1:obj.sizeRes) + obj.dPhidq'*obj.listElementVariables.lagrangeMultiplierSet{1}.value(:,parameters.timestep);
                obj.refValForce = norm(obj.dPhidq'*obj.listElementVariables.lagrangeMultiplierSet{1}.value(:,parameters.timestep));
            end

            gEff = rotateVect3(obj.acceleration, vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep)), parameters.timestep);
            resModel(1:6) = resModel(1:6) - obj.M*[gEff' 0 0 0]';
            obj.refValForce = obj.refValForce + norm(obj.M*[gEff' 0 0 0]');
        end

        function resModel = computeResidueDynamic(obj,resModel,parameters)
            resModel = computeResidueStatic(obj,resModel,parameters);
            v = obj.listNodes{1}.velocity(:,parameters.timestep);
            resModel(1:6) = resModel(1:6) + obj.M*obj.listNodes{1}.acceleration(:,parameters.timestep) - hatSE3(v)'*obj.M*v;
            obj.refValForce = obj.refValForce + norm(obj.M*obj.listNodes{1}.acceleration(:,parameters.timestep) - hatSE3(v)'*obj.M*v);
        end        

        function StModel = computeTangentOperatorStatic(obj,StModel,parameters)
            gEff = rotateVect3(obj.acceleration, vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep)), parameters.timestep);
            StModel(1:3,4:6) = StModel(1:3,4:6) - parameters.coefK*tild(obj.M(1 : 3, 1 : 3) * gEff);
            if obj.sizePhi
                StModel(obj.sizeRes+1:end,1:obj.sizeRes) = StModel(obj.sizeRes+1:end,1:obj.sizeRes) + parameters.scaling*obj.dPhidq;
                StModel(1:obj.sizeRes,obj.sizeRes+1:end) = StModel(1:obj.sizeRes,obj.sizeRes+1:end) + parameters.scaling*obj.dPhidq';
            end
        end

        function StModel = computeTangentOperatorDynamic(obj,StModel,parameters)
            StModel = computeTangentOperatorStatic(obj,StModel,parameters);
            StModel(1:6,1:6) = StModel(1:6,1:6) + parameters.betap*obj.M;
        end
        
        function visu(obj,step)
            xA = obj.listNodes{1}.position(:,step);
            plot3(xA(1),xA(2),xA(3),'r*','LineWidth',2)
            hold on
            for j = 1:(length(obj.listNodes)-1)
                xB = obj.listNodes{j+1}.position(:,step);
                plot3([xA(1) xB(1)],[xA(2) xB(2)],[xA(3) xB(3)],'-*')
            end
        end       
    end
end

