%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef KinematicConstraint < Element
    
    properties
        A;
        nDof;
        dPhidq;
        xI0;
        k = 0; % stiffness
        d = 0; % damping parameter
        coulomb = 0; % coulomb friction component (max friction force) assuming that it is positive constant for velocity>0 
        coulombParam = 100; % parameter of the tanh(coulombParam*velocity) model giving the friction
        alpha0 = [];
    end
    
    methods
        function obj = KinematicConstraint(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,listNodes,~)
            if any(strcmp(properties(obj), 'A'))
                obj.nDof = size(obj.A,2);
            else
                obj.nDof = 0;
            end
            if isempty(obj.alpha0)
                obj.alpha0 = zeros(obj.nDof,1);
            end
            obj.initListNodalDof.nodeSE3 = zeros(1,12);
            for i = 1:2
                obj.initListNodalDof.nodeSE3((1:6)+(i-1)*6) = listNodes(i)+(0.01:0.01:0.06);
            end
            
            obj.initListElementDof.lagrangeMultiplierSet = obj.elementNumber+(0.01:0.01:0.06);
            obj.initListElementVar.lagrangeMultiplierSet = obj.elementNumber;    
            obj.initListElementDof.relativeNodeSE3 = obj.elementNumber+(0.01:0.01:(0.01*obj.nDof));
            obj.initListElementVar.relativeNodeSE3 = obj.elementNumber;
        end
        
        function initializeStatic(obj,~)
            obj.dPhidq = [eye(6) -eye(6)];
            obj.xI0 = obj.listNodes{2}.position(:,1)-obj.listNodes{1}.position(:,1);
            obj.dPhidq(1:3,4:6) = tild(-obj.xI0);
            if obj.nDof
                obj.dPhidq = [obj.dPhidq obj.A];
            end
        end
        
        function resModel = computeResidueStatic(obj,resModel,parameters)
            RA = vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep));
            xA = obj.listNodes{1}.position(:,parameters.timestep);
            HA = [RA xA; 0 0 0 1];
            RBT = vect9ToMat3(obj.listNodes{2}.R(:,parameters.timestep))';
            xB = obj.listNodes{2}.position(:,parameters.timestep);
            HBm1 = [RBT RBT*(-xB); 0 0 0 1];
            
            if obj.nDof
                RI = vect9ToMat3(obj.listElementVariables.relativeNodeSE3{1}.R(:,parameters.timestep));
                xI = obj.listElementVariables.relativeNodeSE3{1}.x(:,parameters.timestep) + obj.xI0;
                HI = [RI xI; 0 0 0 1];
                obj.dPhidq(:,1:6) = [RI' RI'*tild(-xI); zeros(3) RI'];
                resModel(12+obj.nDof+(1:6)) = resModel(12+obj.nDof+(1:6)) + parameters.scaling*vectSE3(HBm1*HA*HI);
                obj.refValPhi = norm(vectSE3(HBm1*HA*HI));
            else
                HI0 = [eye(3) obj.xI0; 0 0 0 1];
                resModel(12+obj.nDof+(1:6)) = resModel(12+obj.nDof+(1:6)) + parameters.scaling*vectSE3(HBm1*HA*HI0);
                obj.refValPhi = norm(vectSE3(HBm1*HA*HI0));
            end
            resModel(1:(12+obj.nDof)) = resModel(1:(12+obj.nDof)) + obj.dPhidq'*obj.listElementVariables.lagrangeMultiplierSet{1}.value(:,parameters.timestep);
            obj.refValForce = norm(obj.dPhidq'*obj.listElementVariables.lagrangeMultiplierSet{1}.value(:,parameters.timestep));
            
            if obj.k ~= 0 % if kinematic constraint is coupled with spring element
                resModel(12+[1:obj.nDof]) = resModel(12+[1:obj.nDof]) + obj.k*(obj.listElementVariables.relativeNodeSE3{1}.relCoo(:,parameters.timestep)-obj.alpha0);
                obj.refValForce = obj.refValForce + norm(obj.k*(obj.listElementVariables.relativeNodeSE3{1}.relCoo(:,parameters.timestep)-obj.alpha0));
            end
        end
        
        function resModel = computeResidueDynamic(obj,resModel,parameters)
            resModel = computeResidueStatic(obj, resModel, parameters);
            
            if obj.d ~= 0 % if kinematic constraint is coupled with damper element
                resModel(12+[1:obj.nDof]) = resModel(12+[1:obj.nDof]) + obj.d*(obj.listElementVariables.relativeNodeSE3{1}.velocity(:,parameters.timestep));
                obj.refValForce = obj.refValForce + norm(obj.d*obj.listElementVariables.relativeNodeSE3{1}.velocity(:,parameters.timestep));
            end

            if obj.coulomb ~= 0 % if kinematic constraint is coupled with coulomb friction
                resModel(12+[1:obj.nDof]) = resModel(12+[1:obj.nDof]) + obj.coulomb*tanh(obj.coulombParam*(obj.listElementVariables.relativeNodeSE3{1}.velocity(:,parameters.timestep)));
                obj.refValForce = obj.refValForce + norm(obj.coulomb*tanh(obj.coulombParam*(obj.listElementVariables.relativeNodeSE3{1}.velocity(:,parameters.timestep))));
            end
        end

        function StModel = computeTangentOperatorStatic(obj,StModel,parameters)
            StModel(end-5:end,1:(12+obj.nDof)) = StModel(end-5:end,1:(12+obj.nDof)) + parameters.scaling*obj.dPhidq;
            StModel(1:(12+obj.nDof),end-5:end) = StModel(1:(12+obj.nDof),end-5:end) + parameters.scaling*obj.dPhidq';
            
            if obj.k ~= 0
                StModel(12+[1:obj.nDof],12+[1:obj.nDof]) = StModel(12+[1:obj.nDof],12+[1:obj.nDof]) + parameters.coefK*obj.k;
            end
        end
        
        function StModel = computeTangentOperatorDynamic(obj,StModel,parameters) 
            StModel = computeTangentOperatorStatic(obj, StModel, parameters);
            
            if obj.d ~= 0 && isfield(parameters,'gammap') % if kinematic constraint is coupled with damper element
                StModel(12+[1:obj.nDof],12+[1:obj.nDof]) = StModel(12+[1:obj.nDof],12+[1:obj.nDof]) + parameters.gammap*obj.d;
            end
            
            if obj.coulomb ~= 0 && isfield(parameters,'gammap') % if kinematic constraint is coupled with coulomb friction
                StModel(12+[1:obj.nDof],12+[1:obj.nDof]) = StModel(12+[1:obj.nDof],12+[1:obj.nDof]) + parameters.gammap*obj.coulombParam*(1-tanh(obj.coulombParam*(obj.listElementVariables.relativeNodeSE3{1}.velocity(:,parameters.timestep))));
            end
        end
    end
end

