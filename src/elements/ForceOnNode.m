%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef ForceOnNode < Element
    
    properties
        Axe = [1 0 0 0 0 0]; % Axes along which the forces are applied (local or global), one line per axis
        following = true; % is the force following the local frame (true) or defined in the intertial frame (false)
        F = 0; % vector of forces (one value only means constant over time), one line per axis
    end
    
    methods
        function obj = ForceOnNode(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,listNodes,~)
            obj.initListNodalDof.nodeSE3 = listNodes(1)+(0.01:0.01:0.06);
        end
        
        function resModel = computeResidueStatic(obj,resModel,parameters)
            if obj.following
                R = eye(3);
            else
                R = vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep));
            end
            
            if size(obj.F,2)==1
                timestep = 1;
            else
                timestep = parameters.timestep;
            end
            
            force = [R' zeros(3); zeros(3) eye(3)]*obj.Axe'*obj.F(:,timestep);
            resModel = resModel - force;
            obj.refValForce = norm(force);
        end
        
        function StModel = computeTangentOperatorStatic(obj,StModel,parameters)
            if size(obj.F,2)==1
                timestep = 1;
            else
                timestep = parameters.timestep;
            end
            if ~obj.following && norm(obj.Axe(:,1:3))~=0
                R = vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep));
                StModel(4:6,4:6) = StModel(4:6,4:6) + parameters.coefK*tild(R'*obj.Axe(:,1:3)'*obj.F(:,timestep)); 
            end
        end        
    end
end

