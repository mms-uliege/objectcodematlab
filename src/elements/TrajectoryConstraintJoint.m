%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef TrajectoryConstraintJoint < Element
    
    properties
        nDof;
        dServodq;
        nCom;
        elements;
        T;
        active = 1;
    end
    
    methods
        function obj = TrajectoryConstraintJoint(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,~,listElements)
            obj.nDof = size(obj.T,1); % initialize the number of degrees of freedom to be commanded (number of lines in the trajectory)

            obj.nCom = size(obj.T,1); % command a joint according to the numbers of trajectories given
            for i = 1:obj.nCom
                obj.initListElementDof.relativeNodeSE3(i) = listElements(i)+0.01;
                obj.initListElementVar.relativeNodeSE3(i) = listElements(i);
            end
            obj.initListElementDof.commandSet = obj.elementNumber+(0.01:0.01:(0.01*obj.nCom)); % Create command objects to control the constrained dof
            obj.initListElementVar.commandSet = obj.elementNumber;
            
        end
        
        function initializeStatic(obj,~)            
            obj.dServodq = eye(obj.nDof); % Initialize the matrix corresponding to servo constraints to be put in the tangent matrix
        end
        
        function resModel = computeResidueStatic(obj,resModel,parameters)
            if obj.active == 1
                relCoordinate = obj.listElementVariables.relativeNodeSE3{1}.relCoo(:,parameters.timestep);
                
                trajConstraint = relCoordinate - obj.T(:,parameters.timestep);
                resModel(end-(obj.nDof-1):end) = resModel(end-(obj.nDof-1):end) + parameters.scaling*(trajConstraint);
                obj.refValPhi = norm(trajConstraint);
                resModel(1:obj.nCom) = resModel(1:obj.nCom) - obj.listElementVariables.commandSet{1}.value(:,parameters.timestep);
                obj.refValForce = norm(obj.listElementVariables.commandSet{1}.value(:,parameters.timestep));
            end
        end
           
        function StModel = computeTangentOperatorStatic(obj,StModel,parameters)
            if obj.active == 1
                StModel(end-(obj.nDof-1):end,1:obj.nDof) = StModel(end-(obj.nDof-1):end,1:obj.nDof) + parameters.scaling*obj.dServodq;
                StModel(1:obj.nCom,obj.nCom+(1:obj.nCom)) = StModel(1:obj.nCom,obj.nCom+(1:obj.nCom)) - eye(obj.nCom);
            end
        end
    end   
end

