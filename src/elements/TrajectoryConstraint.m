%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Arthur Lismonde (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef TrajectoryConstraint < Element
    
    properties
        Axe; % Axes to be commanded ([1 0 0 0 0 0; 0 1 0 0 0 0] means axe "x" and "y")
        nDof;
        dServodq;
        nCom;
        elements;
        T;
        active = 1;
    end
    
    methods
        function obj = TrajectoryConstraint(input)
            obj = obj@Element(input);
        end
        
        function defineDof(obj,listNodes,listElements)
            obj.nDof = size(obj.Axe,1); % initialize the number of degrees of freedom to be commanded
            obj.initListNodalDof.nodeSE3 = zeros(1,6);
            obj.initListNodalDof.nodeSE3(1:6) = listNodes(1)+(0.01:0.01:0.06);
            
            obj.nCom = length(obj.elements);
            for i = 1:obj.nCom
                obj.initListElementDof.relativeNodeSE3(i) = listElements(i)+0.01;
                obj.initListElementVar.relativeNodeSE3(i) = listElements(i);
            end
            obj.initListElementDof.commandSet = obj.elementNumber+(0.01:0.01:(0.01*obj.nCom)); % Create command objects to control the constrained dof
            obj.initListElementVar.commandSet = obj.elementNumber;
        end
        
        function initializeStatic(obj,~)
            if size(obj.Axe,2) == 3
                obj.Axe = [obj.Axe zeros(obj.nDof,3)];
            end
            obj.dServodq = obj.Axe; % Initialize the matrix corresponding to servo constraints to be put in the tangent matrix        
        end
        
        function resModel = computeResidueStatic(obj,resModel,parameters)
            if obj.active == 1
                RA = vect9ToMat3(obj.listNodes{1}.R(:,parameters.timestep));
                xA = obj.listNodes{1}.position(:,parameters.timestep);
                HA = [RA xA; 0 0 0 1];
                hd = obj.Axe'*obj.T(:,parameters.timestep);
                Hd = [expSO3(hd(4:6)) hd(1:3);0 0 0 1];
                xd = Hd(1:3,4);
                Rd = Hd(1:3,1:3);
                HdInv = [Rd' -Rd'*xd; 0 0 0 1];
                
                obj.dServodq(1:obj.nDof,1:6) = obj.Axe*[Rd'*RA zeros(3); zeros(3) Rd'*RA];
                trajConstraint = obj.Axe*vectSE3(HdInv*HA);
                resModel(end-(obj.nDof-1):end) = resModel(end-(obj.nDof-1):end) + parameters.scaling*(trajConstraint);  % "Constraint" level
                obj.refValPhi = norm(trajConstraint);
                resModel(6+(1:obj.nCom)) = resModel(6+(1:obj.nCom)) - obj.listElementVariables.commandSet{1}.value(:,parameters.timestep);
                obj.refValForce = norm(obj.listElementVariables.commandSet{1}.value(:,parameters.timestep));
            end
        end
        
        function StModel = computeTangentOperatorStatic(obj,StModel,parameters)
            if obj.active == 1
                StModel(end-(obj.nDof-1):end,1:6) = StModel(end-(obj.nDof-1):end,1:6) + parameters.scaling*obj.dServodq;
                StModel(6+(1:obj.nCom),6+obj.nCom+(1:obj.nCom)) = StModel(6+(1:obj.nCom),6+obj.nCom+(1:obj.nCom)) - eye(obj.nCom);
            end
        end 
    end    
end

