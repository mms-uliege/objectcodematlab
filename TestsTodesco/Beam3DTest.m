%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test example for a uniform 3D beam loaded dynamically
clear all
% close all

%% Definition of time parameters
finaltime = 1;
timestepsize = 0.01;

%% Definition of the nodes of the model (as matrix)
nodes = [1 0 0 0;
         2 0 0 1];
     
grav = [0 0 -9.81 ];

%% Definition of the elements of the model (as structure)
% Number of beam elements to build the model
nElem = 15;
Start = 1;
End = 2;
% create new nodes to have more beam elements
nodes = createInterNodes(nodes,nElem,Start,End);

% Beam parameters
a = 0.008;
b = 0.01;
rho = 7800;

E = 21e9; nu = 0.3; G = E/(2*(1+nu));
A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy =b*a^3/12;Izz = a*b^3/12;

KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

count = 0;
type = 'FlexibleBeamElement';
for i = 1:nElem
    elements{count+i}.type  = type;
    elements{count+i}.nodes = [nodes(Start+i-1,1) nodes(Start+i,1)];
    elements{count+i}.yAxis = [0 1 0];
    elements{count+i}.KCS   = KCS;
    elements{count+i}.MCS   = MCS;
    elements{count+i}.alpha = 4.5896e-4; % damping proportional to Mass 4.5896e-4
    elements{count+i}.beta  = 0.01329; % damping proportional to Stiffness 0.1329
    elements{count+i}.g     = grav;
end

%% Definition of the boundary conditions

BC = [1]; % Node 1 is fixed

%% Definition of the finite element model (FEModel Object)
% Based on previously defined nodes and elements
Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

%% Definition of the solver and its parameters
tic
D = DynamicIntegration(Model);  % Creatind the Dynamic integration object
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 1;
D.parameters.scaling = 1e6;
D.runIntegration(); % Run the integration of the object
elapsed = toc;
disp(['Computation lasted ',num2str(elapsed/60),' min.'])
%% Visualization and plots
% To visualize the model:(uncomment the following line)
% Model.Visu

% Plots
endNodePos = Model.listNodes{end}.position;
figure(3)
plot(D.parameters.time,endNodePos(1,:)-nodes(end,2),D.parameters.time,endNodePos(2,:)-nodes(end,3),D.parameters.time,endNodePos(3,:)-nodes(end,4),'Linewidth',1)
grid on; hold on;
title('Tip of the beam position')
xlabel('Time [s]')
ylabel('Poistion [m]')
legend('X','Y','Z')

% timeOld = [ 0.23553 ] ; 
