%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Syntax: 
%       m2uml.call_local_PlantUML               ...
%               (   'Title'         , title_str     ... required
%               ,   'Classes'       , class_list    ... required
%               ,   'Arrows'        , other_arrows  ... required, possible empty
%               ,   'TempFolder'    , temp_folder   ... optional
%               ,   'PlantUmlJar'   , plantuml_jar  ... optional
%               ); 
%
    %%
    FILES: 
    
    []src
        []Elements
            ForceInKinematicConstraint < Element
            ForceInKinematicConstraintPD < Element
            Continuation2DElement < Element
            ContinuationElement < Element
            CoroFlexibleBeam2DElement < Element
            CoroFlexibleBeamElement < Element
            CurvedFlexibleBeamElement < Element
            Damper1DElement < Element
            Element < handle
            ExternalForce < Element
            ExternalForce2D < Element
            ExternalForceLinShell < Element
            ExternalForceSE2 < Element
            ExternalForce < Element
            FlexibleBeam2DElement < Element
            FlexibleBeamElement < Element
            FlexibleShellElement < Element
            FlexibleShellElement2LM < Element
            FlexibleShellElementNL < Element
            FlexibleShellElementPhiEps < Element
            FlexibleShellElementPhiEps2 < Element
            ImposeRelativeCoordinateMotion < Element
            KinematicConstraint < Element
            KinematicConstraint2D < Element
            LinearFlexibleShellElement < Element
            LinearFlexibleShellElement_Fin < Element
            LinearFlexibleShellElement_K < Element
            LinSpringDamperElement < Element
            LumpedMass1DElement < Element
            punctualForce < Element
            RigidBody2DElement < Element
            RigidBodyElement < Element
            RotSpringDamperElement < Element
            ShakerExcitationSE2 < Element
            ShakerExcitationSE3 < Element
            Spring1DElement < Element
            TrajectoryConstraint < Element
            TrajectoryConstraint < Element
            TrajectoryConstraint3 < Element
            TrajectoryConstraintJoint < Element
            Volumic2DElement < Element
        []Examples 
        []Functions
        []Math
        []Model 
            FEModel < handle
        []SolutionMethods
            Continuation < handle
            DirectTranscriptionOpti < handle
            DynamicIntegration < handle
            DynamicIntegrationParameters < handle
            taticIntegration < handle
            StaticIntegrationParameters < handle
        []Variables
            []Doftypes
               Command < DegreeOfFreedom
               ContinuationDof < DegreeOfFreedom
               LagrangeMultiplier < DegreeOfFreedom
               MotionDof < DegreeOfFreedom
            []ElementVariableTypes
               CommandSet < ElementVariable
               ContinuationVariableSet < ElementVariable
               ElementVariable < Variable
               ElementVariableTypes < handle
               LagrangeMultiplierSet < ElementVariable
               RelativeNodeSE2 < ElementVariable
               RelativeNodeSE3 < ElementVariable
            []NodesTypes: 
                NodalVariableTypes < handle
                Node < Variable
                Node1D < Node
                Node2D < Node
                NodeSE2 < Node
                NodeSE3 < Node
                NodeSE3Lin < Node
                NodeSE3NL < Node
            ListVariables < handle & dynamicprops
            Variable < handle
%%
m2uml.call_local_PlantUML                               ...
        (   'Title'     ,   'local Ltas Code'   ...
        ,   'Classes'   ,   {
                                'NodalVariableTypes'
                                'Node'
                                'Node1D'
                                'Node2D'
                                'NodeSE2'
                                'NodeSE3'
                                'NodeSE3Lin'
                                'NodeSE3NL'
                            }   ...
        ,   'Arrows'    ,   {
                                'NodalVariableTypes --> Node'
                                'NodalVariableTypes --> Node1D'
                                'NodalVariableTypes --> Node2D'
                                'NodalVariableTypes --> NodeSE2'
                                'NodalVariableTypes --> NodeSE3'
                                'NodalVariableTypes --> NodeSE3Lin'
                                'NodalVariableTypes --> NodeSE3NL'
                            }   ...
        );


%%
m2uml.call_local_PlantUML                               ...
        (   'Title'     ,   'local Ltas Code'   ...
        ,   'Classes'   ,   {
                    'ForceInKinematicConstraint'
					'ForceInKinematicConstraintPD'
					'Continuation2DElement'
					'ContinuationElement'
					'CoroFlexibleBeam2DElement'
					'CoroFlexibleBeamElement'
					'CurvedFlexibleBeamElement'
					'Damper1DElement'
					'Element'
					'ExternalForce'
					'ExternalForce2D'
					'ExternalForceLinShell'
					'ExternalForceSE2'
					'ExternalForce'
					'FlexibleBeam2DElement'
					'FlexibleBeamElement'
					'FlexibleShellElement'
					'FlexibleShellElement2LM'
					'FlexibleShellElementNL'
					'FlexibleShellElementPhiEps'
					'FlexibleShellElementPhiEps2'
					'ImposeRelativeCoordinateMotion'
					'KinematicConstraint'
					'KinematicConstraint2D'
					'LinearFlexibleShellElement'
					'LinearFlexibleShellElement_Fin'
					'LinearFlexibleShellElement_K'
					'LinSpringDamperElement'
					'LumpedMass1DElement'
					'punctualForce'
					'RigidBody2DElement'
					'RigidBodyElement'
					'RotSpringDamperElement'
					'ShakerExcitationSE2'
					'ShakerExcitationSE3'
					'Spring1DElement'
					'TrajectoryConstraint'
					'TrajectoryConstraint'
					'TrajectoryConstraint3'
					'TrajectoryConstraintJoint'
					'Volumic2DElement'     
                            }   ...
        ,   'Arrows'    ,   {
                    'Element --> ForceInKinematicConstraint'
					'Element --> ForceInKinematicConstraintPD'
					'Element --> Continuation2DElement'
					'Element --> ContinuationElement'
					'Element --> CoroFlexibleBeam2DElement'
					'Element --> CoroFlexibleBeamElement'
					'Element --> CurvedFlexibleBeamElement'
					'Element --> Damper1DElement'
					'Element --> ExternalForce'
					'Element --> ExternalForce2D'
					'Element --> ExternalForceLinShell'
					'Element --> ExternalForceSE2'
					'Element --> ExternalForce'
					'Element --> FlexibleBeam2DElement'
					'Element --> FlexibleBeamElement'
					'Element --> FlexibleShellElement'
					'Element --> FlexibleShellElement2LM'
					'Element --> FlexibleShellElementNL'
					'Element --> FlexibleShellElementPhiEps'
					'Element --> FlexibleShellElementPhiEps2'
					'Element --> ImposeRelativeCoordinateMotion'
					'Element --> KinematicConstraint'
					'Element --> KinematicConstraint2D'
					'Element --> LinearFlexibleShellElement'
					'Element --> LinearFlexibleShellElement_Fin'
					'Element --> LinearFlexibleShellElement_K'
					'Element --> LinSpringDamperElement'
					'Element --> LumpedMass1DElement'
					'Element --> punctualForce'
					'Element --> RigidBody2DElement'
					'Element --> RigidBodyElement'
					'Element --> RotSpringDamperElement'
					'Element --> ShakerExcitationSE2'
					'Element --> ShakerExcitationSE3'
					'Element --> Spring1DElement'
					'Element --> TrajectoryConstraint'
					'Element --> TrajectoryConstraint'
					'Element --> TrajectoryConstraint3'
					'Element --> TrajectoryConstraintJoint'
					'Element --> Volumic2DElement'
                            }   ...
        );

%%
       [~] = m2uml.run(  'local Ltas Code' , ...
        {    
					'ForceInKinematicConstraint'
					'ForceInKinematicConstraintPD'
					'Continuation2DElement'
					'ContinuationElement'
					'CoroFlexibleBeam2DElement'
					'CoroFlexibleBeamElement'
					'CurvedFlexibleBeamElement'
					'Damper1DElement'
					'Element'
					'ExternalForce'
					'ExternalForce2D'
					'ExternalForceLinShell'
					'ExternalForceSE2'
					'ExternalForce'
					'FlexibleBeam2DElement'
					'FlexibleBeamElement'
					'FlexibleShellElement'
					'FlexibleShellElement2LM'
					'FlexibleShellElementNL'
					'FlexibleShellElementPhiEps'
					'FlexibleShellElementPhiEps2'
					'ImposeRelativeCoordinateMotion'
					'KinematicConstraint'
					'KinematicConstraint2D'
					'LinearFlexibleShellElement'
					'LinearFlexibleShellElement_Fin'
					'LinearFlexibleShellElement_K'
					'LinSpringDamperElement'
					'LumpedMass1DElement'
					'punctualForce'
					'RigidBody2DElement'
					'RigidBodyElement'
					'RotSpringDamperElement'
					'ShakerExcitationSE2'
					'ShakerExcitationSE3'
					'Spring1DElement'
					'TrajectoryConstraint'
					'TrajectoryConstraint'
					'TrajectoryConstraint3'
					'TrajectoryConstraintJoint'
					'Volumic2DElement'     
             },{
					'Element --> ForceInKinematicConstraint'
					'Element --> ForceInKinematicConstraintPD'
					'Element --> Continuation2DElement'
					'Element --> ContinuationElement'
					'Element --> CoroFlexibleBeam2DElement'
					'Element --> CoroFlexibleBeamElement'
					'Element --> CurvedFlexibleBeamElement'
					'Element --> Damper1DElement'
					'Element --> ExternalForce'
					'Element --> ExternalForce2D'
					'Element --> ExternalForceLinShell'
					'Element --> ExternalForceSE2'
					'Element --> ExternalForce'
					'Element --> FlexibleBeam2DElement'
					'Element --> FlexibleBeamElement'
					'Element --> FlexibleShellElement'
					'Element --> FlexibleShellElement2LM'
					'Element --> FlexibleShellElementNL'
					'Element --> FlexibleShellElementPhiEps'
					'Element --> FlexibleShellElementPhiEps2'
					'Element --> ImposeRelativeCoordinateMotion'
					'Element --> KinematicConstraint'
					'Element --> KinematicConstraint2D'
					'Element --> LinearFlexibleShellElement'
					'Element --> LinearFlexibleShellElement_Fin'
					'Element --> LinearFlexibleShellElement_K'
					'Element --> LinSpringDamperElement'
					'Element --> LumpedMass1DElement'
					'Element --> punctualForce'
					'Element --> RigidBody2DElement'
					'Element --> RigidBodyElement'
					'Element --> RotSpringDamperElement'
					'Element --> ShakerExcitationSE2'
					'Element --> ShakerExcitationSE3'
					'Element --> Spring1DElement'
					'Element --> TrajectoryConstraint'
					'Element --> TrajectoryConstraint'
					'Element --> TrajectoryConstraint3'
					'Element --> TrajectoryConstraintJoint'
					'Element --> Volumic2DElement'
			 }  );     
            
            
            
            
            
%%             
     [~] = m2uml.run(  'local Ltas Code' , ...
        {    
					'FEModel'
					'Continuation'
					'DirectTranscriptionOpti'
					'DynamicIntegration'
					'DynamicIntegrationParameters'
					'taticIntegration'
					'StaticIntegrationParameters'
					'Command'
					'ContinuationDof'
					'LagrangeMultiplier'
					'MotionDof'
					'CommandSet'
					'ContinuationVariableSet'
					'ElementVariable'
					'ElementVariableTypes'
					'LagrangeMultiplierSet'
					'RelativeNodeSE2'
					'RelativeNodeSE3'
					'NodalVariableTypes'
					'Node'
					'Node1D'
					'Node2D'
					'NodeSE2'
					'NodeSE3'
					'NodeSE3Lin'
					'NodeSE3NL'
					'ListVariables'
					'Variable'       
             },{
					'DegreeOfFreedom --> Command'
					'DegreeOfFreedom --> ContinuationDof'
					'DegreeOfFreedom --> LagrangeMultiplier'
					'DegreeOfFreedom --> MotionDof'
					'DegreeOfFreedom --> CommandSet'
					'Variable --> ElementVariable'
					'Variable --> Node'
					'ElementVariable --> ContinuationVariableSet'
					'ElementVariable --> LagrangeMultiplierSet'
					'ElementVariable --> RelativeNodeSE2'
					'ElementVariable --> RelativeNodeSE3'
					'ElementVariable --> NodalVariableTypes'
					'Node --> Node1D'
					'Node --> Node2D'
					'Node --> NodeSE2'
					'Node --> NodeSE3'
					'Node --> NodeSE3Lin'
					'Node --> NodeSE3NL'
			 }  );     
            
%%    
       [~] = m2uml.run(  'local Ltas Code' , ...
        {    
					'ForceInKinematicConstraint'
					'ForceInKinematicConstraintPD'
					'Continuation2DElement'
					'ContinuationElement'
					'CoroFlexibleBeam2DElement'
					'CoroFlexibleBeamElement'
					'CurvedFlexibleBeamElement'
					'Damper1DElement'
					'Element'
					'ExternalForce'
					'ExternalForce2D'
					'ExternalForceLinShell'
					'ExternalForceSE2'
					'ExternalForce'
					'FlexibleBeam2DElement'
					'FlexibleBeamElement'
					'FlexibleShellElement'
					'FlexibleShellElement2LM'
					'FlexibleShellElementNL'
					'FlexibleShellElementPhiEps'
					'FlexibleShellElementPhiEps2'
					'ImposeRelativeCoordinateMotion'
					'KinematicConstraint'
					'KinematicConstraint2D'
					'LinearFlexibleShellElement'
					'LinearFlexibleShellElement_Fin'
					'LinearFlexibleShellElement_K'
					'LinSpringDamperElement'
					'LumpedMass1DElement'
					'punctualForce'
					'RigidBody2DElement'
					'RigidBodyElement'
					'RotSpringDamperElement'
					'ShakerExcitationSE2'
					'ShakerExcitationSE3'
					'Spring1DElement'
					'TrajectoryConstraint'
					'TrajectoryConstraint'
					'TrajectoryConstraint3'
					'TrajectoryConstraintJoint'
					'Volumic2DElement'
					'FEModel'
					'Continuation'
					'DirectTranscriptionOpti'
					'DynamicIntegration'
					'DynamicIntegrationParameters'
					'StaticIntegration'
					'StaticIntegrationParameters'
					'Command'
					'ContinuationDof'
					'LagrangeMultiplier'
					'MotionDof'
					'CommandSet'
					'ContinuationVariableSet'
					'ElementVariable'
					'ElementVariableTypes'
					'LagrangeMultiplierSet'
					'RelativeNodeSE2'
					'RelativeNodeSE3'
					'NodalVariableTypes'
					'Node'
					'Node1D'
					'Node2D'
					'NodeSE2'
					'NodeSE3'
					'NodeSE3Lin'
					'NodeSE3NL'
					'ListVariables'
					'Variable'       
             },{
					'Element --> ForceInKinematicConstraint'
					'Element --> ForceInKinematicConstraintPD'
					'Element --> Continuation2DElement'
					'Element --> ContinuationElement'
					'Element --> CoroFlexibleBeam2DElement'
					'Element --> CoroFlexibleBeamElement'
					'Element --> CurvedFlexibleBeamElement'
					'Element --> Damper1DElement'
					'Element --> ExternalForce'
					'Element --> ExternalForce2D'
					'Element --> ExternalForceLinShell'
					'Element --> ExternalForceSE2'
					'Element --> ExternalForce'
					'Element --> FlexibleBeam2DElement'
					'Element --> FlexibleBeamElement'
					'Element --> FlexibleShellElement'
					'Element --> FlexibleShellElement2LM'
					'Element --> FlexibleShellElementNL'
					'Element --> FlexibleShellElementPhiEps'
					'Element --> FlexibleShellElementPhiEps2'
					'Element --> ImposeRelativeCoordinateMotion'
					'Element --> KinematicConstraint'
					'Element --> KinematicConstraint2D'
					'Element --> LinearFlexibleShellElement'
					'Element --> LinearFlexibleShellElement_Fin'
					'Element --> LinearFlexibleShellElement_K'
					'Element --> LinSpringDamperElement'
					'Element --> LumpedMass1DElement'
					'Element --> punctualForce'
					'Element --> RigidBody2DElement'
					'Element --> RigidBodyElement'
					'Element --> RotSpringDamperElement'
					'Element --> ShakerExcitationSE2'
					'Element --> ShakerExcitationSE3'
					'Element --> Spring1DElement'
					'Element --> TrajectoryConstraint'
					'Element --> TrajectoryConstraint'
					'Element --> TrajectoryConstraint3'
					'Element --> TrajectoryConstraintJoint'
					'Element --> Volumic2DElement'
					'DegreeOfFreedom --> Command'
					'DegreeOfFreedom --> ContinuationDof'
					'DegreeOfFreedom --> LagrangeMultiplier'
					'DegreeOfFreedom --> MotionDof'
					'DegreeOfFreedom --> CommandSet'
					'Variable --> ElementVariable'
					'Variable --> Node'
					'ElementVariable --> ContinuationVariableSet'
					'ElementVariable --> LagrangeMultiplierSet'
					'ElementVariable --> RelativeNodeSE2'
					'ElementVariable --> RelativeNodeSE3'
					'ElementVariable --> NodalVariableTypes'
					'Node --> Node1D'
					'Node --> Node2D'
					'Node --> NodeSE2'
					'Node --> NodeSE3'
					'Node --> NodeSE3Lin'
					'Node --> NodeSE3NL'
			 }  );     
%%             
    
     [~] = m2uml.run(  'local Ltas Code' , ...
        {                       'Variable'
                                'NodalVariableTypes'  
                                'Node'
                                'Node1D'
                                'Node2D'
                                'NodeSE2'
                                'NodeSE3'
                                'NodeSE3Lin'
                                'NodeSE3NL'
                                
             },{
                                'Variable --> Node'
                                'Node --> Node1D'
                                'Node --> Node2D'
                                'Node --> NodeSE2'
                                'Node --> NodeSE3'
                                'Node --> NodeSE3Lin'
                                'Node --> NodeSE3NL'
            }  );
        