%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%
%
%   Juliano Todesco
%   19/03/2018
%-------------------------------------------------------------------------%
% Test example for a uniform 3D beam loaded dynamically 
 clear all
 close all
 clc

%% Definition of time parameters
finaltime       = 0.5;
timestepsize	= 1e-3;
relTolRes    	= 1e-6;
relTolPhi    	= 1e-6;
scaling      	= 1e5;
rhoIP        	= 0.6;
itMax       	= 100;

%% Definition of the nodes of the model (as matrix)
nodes = [1 0 0 0;
         2 1 0 0];
     
grav = [0 0 0*-9.81 ];

%% Definition of the elements of the model (as structure)
% Number of beam elements to build the model
nElem = 1;
Start = 1;
End = 2;
% create new nodes to have more beam elements
nodes = createInterNodes(nodes,nElem,Start,End);

% Beam parameters
a   = 0.02;
b   = 0.02;
rho = 7800;
E   = 2.0e11; 
nu  = 0.3; 

%%
count = 0;
type = 'FlexibleBeamElement';
i=1
    elements{count+i}.type  = type;
    elements{count+i}.nodes = [nodes(Start+i-1,1) nodes(Start+i,1)];
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.A     = a*b;
    elements{count+i}.Izz   = b*a^3/12;
    elements{count+i}.Iyy   = a*b^3/12;
    elements{count+i}.Jxx   = a*b*(a^2+b^2)/12;
    elements{count+i}.E     = E;
    elements{count+i}.nu    = nu;
    elements{count+i}.rho   = rho;
%     elements{count+i}.alpha = 5e-4;%1e-5;  % damping proportional to Mass 4.5896e-4
%     elements{count+i}.beta  = 1e-2; %1e-5; % damping proportional to Stiffness 0.1329
    elements{count+i}.g     = grav;
    elements{count+i}.listM = 3;
%     elements{count+i}.listCt= 3;
%     elements{count+i}.listKt= 3; 

%% Definition of the boundary conditions

BC = [1]; % Node 1 is fixed
%% Definition of the finite element model (FEModel Object)
% Based on previously defined nodes and elements
Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);
parameters.nstep = 10;
for i = Model.listNumberDof
        Model.listDof{i}.InitializeD(parameters.nstep);
end
for i = Model.listNumberNodes
    Model.listNodes{i}.InitializeD(parameters.nstep);
end
for i = Model.listNumberElementVariables
    Model.listElementVariables{i}.InitializeD(parameters.nstep);
end
for i = Model.listNumberElements
    Model.listElements{i}.InitializeD(parameters.nstep);
end

step = 1;
parameters.timestep = step ;
resModel = zeros(12,1); 
Model.listElements{1}.UpDate(parameters);
res0 = Model.listElements{1}.AssembleResidueE(resModel, parameters);
StModel = zeros(12,12); 
parameters.gammap = 0;
parameters.betap = 0;
parameters.coefK = 1;
Kt = Model.listElements{1}.AssembleTangentOperatorD(StModel, parameters);

Model.listElements{1}.Visu(step) ; grid on; view(2); 
axis([0 2.5 -2.5 0.5 ])

%%
step = 2;
hinc = 1e-8*[0.01 0.02 0.03 0.04 0.05 0.06 0.01 0.02 0.03 0.04 0.05 0.06]';

Hinc = expSE3(hinc(1:6));
nod = Model.listNodes{1};
nod.R(:,step) = dimR(dimR(nod.R(:,step-1))*Hinc(1:3,1:3));
nod.position(:,step) = nod.position(:,step-1) + dimR(nod.R(:,step-1))*Hinc(1:3,4);

Hinc = expSE3(hinc(7:12));
nod = Model.listNodes{2};
nod.R(:,step) = dimR(dimR(nod.R(:,step-1))*Hinc(1:3,1:3));
nod.position(:,step) = nod.position(:,step-1) + dimR(nod.R(:,step-1))*Hinc(1:3,4);

parameters.timestep = step;
Model.listElements{1}.UpDate(parameters);
res1 = Model.listElements{1}.AssembleResidueE(resModel, parameters);

(res1 - Kt * hinc)/norm(res1)

Model.listElements{1}.Visu(step) ; grid on; view(2); 
axis([0 2.5 -2.5 0.5 ])
