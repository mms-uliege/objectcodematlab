%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
A = [0.7701    0.8094     0.8830     0.4544     0.5888     0.7704     0.8821     1.1970     1.2867]*1.e-2;
a0 = [ 1.e-2    1.e-2      1.e-2      1.e-2      1.e-2      1.e-2      1.e-2      1.e-2      1.e-2]'; 
b = A*a0*.8;
Aeq = [];
beq = [];
lb = a0*0.25;
ub = a0*1e+1;
nonlcon = [];
%%
disp('WingBeam3DSensiTest(a0,1)')

[norm_q] = WingBeam3DSensiDimTest(a0,1)

open('WorkspaceDimref.mat');
    Model.Visu([],1)
    grid on
%%
sensi = zeros(9,1);
for i=1:9
    epson = 1e-12;
    nBeam       = i;
    a(nBeam)     = a(nBeam)-epson;    [norm_qb] = WingBeam3DSensiTest(a,0);
    a(nBeam)     = a(nBeam)+epson;    [norm_qf] = WingBeam3DSensiTest(a,0);
    sensi(i)=(norm_qf-norm_qb)/2/epson;
end
 sensi


%%
disp('WingBeam3DSensiTest')
tic
options = optimoptions('fmincon','Display','iter','Algorithm','sqp','ConstraintTolerance',1e-2); 
a = fmincon(@(a)WingBeam3DSensiTest(a,0),a0,A,b,Aeq,beq,lb,ub,nonlcon,options)
[norm_q] = WingBeam3DSensiTest(a,1)
b = A*a
toc


% % Elapsed time is 138.306441 seconds.
%%
disp('WingBeam3DSensiGradTest')
tic
options = optimoptions('fmincon','Display','iter','Algorithm','sqp','SpecifyObjectiveGradient',true,'ConstraintTolerance',1e-2);
a = fmincon(@(a)WingBeam3DSensiGradTest(a,0),a0,A,b,Aeq,beq,lb,ub,nonlcon,options)
[norm_q, sensi] = WingBeam3DSensiGradTest(a,1)
b = A*a
toc


%%
disp('WingBeam3DSensiDimTest(a0,1)')
[norm_q] = WingBeam3DSensiDimTest(a0,1)


%%
sensi = zeros(9,1);
a =a0;
for i=1:9
    epson = 1e-12;
    nBeam       = i;
    a(nBeam)     = a(nBeam)-epson;    [norm_qb] = WingBeam3DSensiDimTest(a,0);
    a(nBeam)     = a(nBeam)+epson;    [norm_qf] = WingBeam3DSensiDimTest(a,0);
    sensi(i)=(norm_qf-norm_qb)/2/epson;
end
 sensi

%%
disp('WingBeam3DSensiDimTest')
tic
options = optimoptions('fmincon','Display','iter','Algorithm','sqp','ConstraintTolerance',1e-2);
a = fmincon(@(a)WingBeam3DSensiDimTest(a,0),a0,A,b,Aeq,beq,lb,ub,nonlcon,options)
[norm_q] = WingBeam3DSensiDimTest(a,1)
b = A*a
toc


%%
disp('WingBeam3DSensiDimGradTest')
tic
options = optimoptions('fmincon','Display','iter','Algorithm','sqp','SpecifyObjectiveGradient',true,'ConstraintTolerance',1e-2);
a = fmincon(@(a)WingBeam3DSensiDimGradTest(a,0),a0,A,b,Aeq,beq,lb,ub,nonlcon,options)
[norm_q] = WingBeam3DSensiDimTest(a,1)
b = A*a
toc

