%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Beam Parameters 
nElem = 4;
Ltot  = 1.0;
rho_  = 7500*ones(nElem,1);    % (kg/m�)
r1    = 0.009*ones(nElem,1);   % (m) 
r2    = 0.01*ones(nElem,1);    % (m) 
E     = 2e+11*ones(nElem,1);   % (Pa) Young's modulus
nu    = 0.30*ones(nElem,1);    % Poisson's ratio
sensi = [0 0 0 1];             % [r1 r2 E rho]    

% Definition of time parameters
finaltime       = 0.01;
timestepsize    = 1e-5;
if 1
    SensiFD = zeros(2,nElem);
    eps = 1e-3*rho_(1);
    for i = 1:nElem 
        rho = rho_;
        rho(i) = rho(i) +eps; 
        [~,~,~,CriteDM_P] = CantileverBeam3DSensiDynamicIntegration_Test1(nElem,Ltot,r1,r2,E,nu,rho,sensi,finaltime,timestepsize,1,1,ratioStep);
        rho = rho_;
        rho(i) = rho(i) -eps; 
        [~,~,~,CriteDM_L] = CantileverBeam3DSensiDynamicIntegration_Test1(nElem,Ltot,r1,r2,E,nu,rho,sensi,finaltime,timestepsize,1,1,ratioStep);
        SensiFD(:,i) =  (CriteDM_P-CriteDM_L)/(2*eps);
    end
end
rho = rho_;

DM_FD_G = zeros(4,4);
DM_FD_F = zeros(4,4);
AV_FD_G = zeros(4,4);
AV_FD_F = zeros(4,4);

figure(3); subplot(2,1,1); 
    plot(bar,SensiFD(1,:),'-*','DisplayName',['FD ', num2str(timestepsize, '%1.1e'),'s'] );hold on;
    grid on ;legend('-DynamicLegend');legend('show');title('Gradient G');xlabel('m');ylabel('Sensitivity')
    subplot(2,1,2);
    plot(bar,SensiFD(2,:),'-*','DisplayName',['FD ', num2str(timestepsize, '%1.1e'),'s']);hold on;
    grid on ;legend('-DynamicLegend');legend('show'); title('Gradient F'); xlabel('m'); ylabel('Sensitivity')
    i=0;
for ratioStep = [1,2,4,10]
    i=i+1;
[~,~,SensiDM,CriteDM] = CantileverBeam3DSensiDynamicIntegration_Test1(nElem,Ltot,r1,r2,E,nu,rho,sensi,finaltime,timestepsize,0,1,ratioStep);
[~,~,SensiAV,CriteAV] = CantileverBeam3DSensiDynamicIntegration_Test1(nElem,Ltot,r1,r2,E,nu,rho,sensi,finaltime,timestepsize,0,0,ratioStep);
bar=0:Ltot/(nElem-1):Ltot;

figure(3); subplot(2,1,1); 
    plot(bar,SensiDM(1,:),'-*','DisplayName',['DM ', num2str(ratioStep*timestepsize, '%1.1e'),'s'] );hold on;
    plot(bar,SensiAV(1,:),'-*','DisplayName',['AV ', num2str(ratioStep*timestepsize, '%1.1e'),'s'] );hold on;
    grid on ;legend('-DynamicLegend');legend('show');title('Gradient G');xlabel('m');ylabel('Sensitivity')
    subplot(2,1,2);
    plot(bar,SensiDM(2,:),'-*','DisplayName',['DM ', num2str(ratioStep*timestepsize, '%1.1e'),'s']);hold on;
    plot(bar,SensiAV(2,:),'-*','DisplayName',['AV ', num2str(ratioStep*timestepsize, '%1.1e'),'s']);hold on;grid on;
    legend('-DynamicLegend');legend('show'); title('Gradient F'); xlabel('m'); ylabel('Sensitivity')

    disp('MAxRelativeSensi DM AV ');
        disp(['   G = [' num2str( max( abs(SensiDM(1,:)-SensiAV(1,:))./abs(SensiAV(1,:)), abs(SensiDM(1,:)-SensiAV(1,:))./abs(SensiDM(1,:)))) '];']);
        disp(['   F = [' num2str( max( abs(SensiDM(2,:)-SensiAV(2,:))./abs(SensiAV(2,:)), abs(SensiDM(2,:)-SensiAV(2,:))./abs(SensiDM(2,:)))) '];']);

    disp('MAxRelativeSensi DM_FD');
        disp(['   G = [' num2str(  abs(SensiDM(1,:)-SensiFD(1,:))./abs(SensiFD(1,:))) '];']);
        disp(['   F = [' num2str(  abs(SensiDM(2,:)-SensiFD(2,:))./abs(SensiFD(2,:))) '];']);

    disp('MAxRelativeSensi AV_FD');
        disp(['   G = [' num2str(  abs(SensiAV(1,:)-SensiFD(1,:))./abs(SensiFD(1,:))) '];']);
        disp(['   F = [' num2str(  abs(SensiAV(2,:)-SensiFD(2,:))./abs(SensiFD(2,:))) '];']);

    DM_FD_G(i,:) = [ abs(SensiDM(1,:)-SensiFD(1,:))./abs(SensiFD(1,:))];
    DM_FD_F(i,:) = [ abs(SensiDM(2,:)-SensiFD(2,:))./abs(SensiFD(2,:))];  

    AV_FD_G(i,:) = [ abs(SensiAV(1,:)-SensiFD(1,:))./abs(SensiFD(1,:))];
    AV_FD_F(i,:) = [ abs(SensiAV(2,:)-SensiFD(2,:))./abs(SensiFD(2,:))];  

end


figure(4)
subplot(2,1,1);
loglog([1,2,4,10]*timestepsize,[norm(DM_FD_G(1,:),2),norm(DM_FD_G(2,:),2),norm(DM_FD_G(3,:),2),norm(DM_FD_G(4,:),2)],'-*','DisplayName','Direct Differentiation Method');hold on;grid on;
loglog([1,2,4,10]*timestepsize,[norm(AV_FD_G(1,:),2),norm(AV_FD_G(2,:),2),norm(AV_FD_G(3,:),2),norm(AV_FD_G(4,:),2)],'-*','DisplayName','Adjoint Variable Method');hold on;grid on;
legend('show');
title('Relative error of the sensitivity calculated at the end of the simulation (Static)')
xlabel('Time Step Size[s]')
ylabel('Relative error')
subplot(2,1,2);
loglog([1,2,4,10]*timestepsize,[norm(DM_FD_F(1,:),2),norm(DM_FD_F(2,:),2),norm(DM_FD_F(3,:),2),norm(DM_FD_F(4,:),2)],'-*','DisplayName','Direct Differentiation Method');hold on;grid on;
loglog([1,2,4,10]*timestepsize,[norm(AV_FD_F(1,:),2),norm(AV_FD_F(2,:),2),norm(AV_FD_F(3,:),2),norm(AV_FD_F(4,:),2)],'-*','DisplayName','Adjoint Variable Method');hold on;grid on;
legend('show');
title('Relative error of the sensitivity calculated during all simullation (Dynamic)')
xlabel('Time Step Size[s]')
ylabel('Relative error')

% disp('MAxRelativeSensi DM AV ');
%     disp(['   G = [' num2str( max( abs(SensiDM(1,:)-SensiAV(1,:))./abs(SensiAV(1,:)), abs(SensiDM(1,:)-SensiAV(1,:))./abs(SensiDM(1,:)))) '];']);
%     disp(['   F = [' num2str( max( abs(SensiDM(2,:)-SensiAV(2,:))./abs(SensiAV(2,:)), abs(SensiDM(2,:)-SensiAV(2,:))./abs(SensiDM(2,:)))) '];']);
% %
% disp('MAxRelativeSensi DM_FD');
%     disp(['   G = [' num2str( max( abs(SensiDM(1,:)-SensiFD(1,:))./abs(SensiFD(1,:)), abs(SensiDM(1,:)-SensiFD(1,:))./abs(SensiDM(1,:)))) '];']);
%     disp(['   F = [' num2str( max( abs(SensiDM(2,:)-SensiFD(2,:))./abs(SensiFD(2,:)), abs(SensiDM(2,:)-SensiFD(2,:))./abs(SensiDM(2,:)))) '];']);
% %
% disp('MAxRelativeSensi AV_FD');
%     disp(['   G = [' num2str( max( abs(SensiAV(1,:)-SensiFD(1,:))./abs(SensiFD(1,:)), abs(SensiAV(1,:)-SensiFD(1,:))./abs(SensiAV(1,:)))) '];']);
%     disp(['   F = [' num2str( max( abs(SensiAV(2,:)-SensiFD(2,:))./abs(SensiFD(2,:)), abs(SensiAV(2,:)-SensiFD(2,:))./abs(SensiAV(2,:)))) '];']);
