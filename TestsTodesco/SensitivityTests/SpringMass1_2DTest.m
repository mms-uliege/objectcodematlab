%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%
%                           Code Matlab Ltas
%      
%   Test: Spring 1D Element 
%
%-------------------------------------------------------------------------%
clear all 
% close all 
clc 
format LONGE
%
finaltime      = 3;
timestepsize   = 1.e-3;
relTolRes      = 1.e-12;
relTolPhi      = 1.e-12;
scaling        = 1.e+5; 
rho            = 0.9;
itMax          = 10;
LinConst       = 0;
% V�rifi� avec testanal pour param�tre m avec CI q0, sans force ext
%
L = 1;
%%
nodes = [1  0;
         2  L ];
%%%
k = 1;
elements{k}.type            ='Spring1DElement';
elements{k}.nodes           = [2 1];
elements{k}.stiffness       = 1e+3;
elements{k}.damping         = 5e+1;
% elements{k}.natural_length  = L;
dElementdp.sensi            = [1 1 ];% [stiffness damping]         
dElementdp.stiffness        = [1 0 ];          
dElementdp.damping          = [0 1 ];    
dElementdp.step_fd          = [1e+0 1e-2 ];
elements{k}.Sensi           = dElementdp;

k = k+1;
elements{k}.type            = 'LumpedMass1DElement';
elements{k}.nodes           = 2;
elements{k}.mass            = 10;

k = k+1;
elements{k}.type            = 'ExternalForce';
elements{k}.nodes           = 2;
elements{k}.amplitude       = 9.81*elements{2}.mass *sin(pi/4);
elements{k}.frequency       = 0;
%--------- 
% BC = [1];
% 
% Model = FEModel();
% Model.CreateFEModel(nodes,elements);
% Model.defineBC(BC);
% 
% G                   = DynamicIntegration(Model);
% G.parameters.finaltime      = finaltime;
% G.parameters.timestepsize   = timestepsize;
% G.parameters.relTolRes      = relTolRes;
% G.parameters.relTolPhi      = relTolPhi;
% G.parameters.scaling        = scaling; 
% G.parameters.rho            = rho;
% G.parameters.itMax          = itMax;
% G.runIntegration();
% % G.model.Visu()
% figure(1);
% plot(G.parameters.time,Model.listElements{2}.listNodes{1}.position);hold on;

%----------
BC = [1];
%%%%
Model = FEModelSensi();
Model.CreateFEModelSensi(nodes,elements);
Model.defineBC(BC);
Model.defineCreteria(@Criteria)
G                   = SensitivityAV(Model);
G.parameters.finaltime      = finaltime;
G.parameters.timestepsize   = timestepsize;
G.parameters.relTolRes      = relTolRes;
G.parameters.relTolPhi      = relTolPhi;
G.parameters.scaling        = scaling; 
G.parameters.rho            = rho;
G.parameters.itMax          = itMax;
G.runIntegration();
G.sIP.LinConst      = LinConst; 
% G.model.Visu()

figure(1);
plot(G.parameters.time,Model.listElements{2}.listNodes{1}.position);hold on;


figure(4); hold on
plot(Model.listElements{3}.listNodes{1}.position(1,:),'--');hold on;grid on; 
legend('x1','x2');

G.runSensi();

SensiAV = G.model.sensi;
CriteAV = G.model.crite;

% disp('Sensi');
% G.model.sensi
% disp('Crite');
% G.model.crite

% format LONGENG
% if G.sIP.method
%     disp ('Adjoint Variable Method:' );
% else
%     disp ('Direct Differentiation Method:' );
% end
% format LONGENG
% G.model.sensi;
% G.model.crite';

% disp(['SensiDM G = [' num2str(G.model.sensi(1,:)) '];']);
% disp(['        F = [' num2str(G.model.sensi(2,:)) '];']);
% 
% disp(['Crite G = [' num2str(G.model.crite(1,:)) '];']);
% disp(['      F = [' num2str(G.model.crite(2,:)) '];']);
clear Model
%%
Model = FEModelSensi();
Model.CreateFEModelSensi(nodes,elements);
Model.defineBC(BC);
Model.defineCreteria(@Criteria)
H                   = SensitivityDM(Model);
H.parameters.finaltime      = G.parameters.finaltime  ;
H.parameters.timestepsize   = G.parameters.timestepsize;
H.parameters.relTolRes      = G.parameters.relTolRes;
H.parameters.relTolPhi      = G.parameters.relTolPhi;
H.parameters.scaling        = G.parameters.scaling; 
H.parameters.rho            = G.parameters.rho;
H.parameters.itMax          = G.parameters.itMax ;
H.runIntegration();
H.sIP.LinConst      = LinConst; 
H.runSensi();

% format LONGE
% if H.sIP.method
%     disp ('Adjoint Variable Method:');
% else
%     disp ('Direct Differentiation Method:');
% end
% 
% H.model.sensi
% H.model.crite'
% format SHORT
% disp('MAxRelativeSensi')
% disp([['Log10( err_G ):';'Log10( err_F ):'] , num2str(max(log10(abs((G.model.sensi-H.model.sensi)./H.model.sensi)),log10(abs((G.model.sensi-H.model.sensi)./G.model.sensi))),3)])

SensiDM = H.model.sensi;
CriteDM = H.model.crite;

disp(' ');
disp('CriteDM :');  
disp(['   G = [' num2str(CriteDM(1,:)) '];']);
disp(['   F = [' num2str(CriteDM(2,:)) '];']);
disp('CriteAV :'); 
disp(['   G = [' num2str(CriteAV(1,:)) '];']);
disp(['   F = [' num2str(CriteAV(2,:)) '];']); 
disp('SensiDM :'); 
disp(['   G = [' num2str(SensiDM(1,:)) '];']);
disp(['   F = [' num2str(SensiDM(2,:)) '];']);
disp('SensiAV :');  
disp(['   G = [' num2str(SensiAV(1,:)) '];']);
disp(['   F = [' num2str(SensiAV(2,:)) '];']);
disp(' ');
disp('MAxRelativeSensi DM AV ');
disp(['   G = [' num2str( max( abs(SensiDM(1,:)-SensiAV(1,:))./abs(SensiAV(1,:)), abs(SensiDM(1,:)-SensiAV(1,:))./abs(SensiDM(1,:)))) '];']);
disp(['   F = [' num2str( max( abs(SensiDM(2,:)-SensiAV(2,:))./abs(SensiAV(2,:)), abs(SensiDM(2,:)-SensiAV(2,:))./abs(SensiDM(2,:)))) '];']);


function [F,dF,G,dG] = Criteria(model,parameters,timestep)
nf = 2;
if nargin == 0
    F = nf;
    return
end

F  = cell(nf,1);
G  = cell(nf,1);
dF = cell(nf,1);
dG = cell(nf,1);

dL = sqrt( model.listElements{1}.listNodes{1}.position(timestep)^2 ...
              +model.listElements{1}.listNodes{2}.position(timestep)^2);

qSensi = zeros(1,model.nDof);
qSensi(model.listElements{1}.listNodes{2}.listNumberDof) = model.listElements{1}.listNodes{2}.position(timestep)...
    /dL;
qSensi(model.listElements{1}.listNodes{1}.listNumberDof) = model.listElements{1}.listNodes{1}.position(timestep)...
    /dL;
    
dF{2}.q = qSensi;
dG{1}.q = qSensi;

qCrite = dL -abs(model.listElements{1}.natural_length) ;
F{2}.q = qCrite;
G{1}.q = qCrite;


end
