%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%
%                           Code Matlab Ltas
%      
%   Test: Spring 1D Element 
%
%-------------------------------------------------------------------------%
clear all 
% close all 
% clc 
%
% V�rifi� avec testanal pour param�tre m avec CI q0, sans force ext
L = 1;
alpha = 0;
nodes = [1  0   0  ;
         2  0   0  ;
         3  L*cos(alpha*pi/180)   L*sin(alpha*pi/180);
         4  L*cos(alpha*pi/180)   L*sin(alpha*pi/180);
         5  L*cos(alpha*pi/180)   L*sin(alpha*pi/180)];

%%%
k = 1;
elements{k}.type            = 'FlexibleBeam2DElement'; %'LinSpringDamperElement2D';
elements{k}.nodes           = [2 3];
elements{k}.KCS             = [1e7 0 0; 0 1e7 0; 0 0 1e4];
elements{k}.MCS             = [0.1 0 0; 0 10 0; 0 0 10];
elements{k}.betaK           = 0.1;
elements{k}.betaM           = 0.1;
% elements{k}.A               = 1;
% elements{k}.natural_length  = 1;
% elements{k}.natural_teta    = 10*pi/180;
% elements{k}.stiffness       = 2;
% elements{k}.stiffnessTeta   = 0;
% elements{k}.damping         = 10;
% elements{k}.dampingTeta     = 2;
% dElementdp.sensi            = [1 1 1];% [stiffness damping natural_length]         
% dElementdp.stiffness        = [1 0 0];          
% dElementdp.damping          = [0 1 0];          
% dElementdp.natural_length   = [0 0 1]; 
% dElementdp.step_fd          = [1e+1 1e-2 1e-3];
% elements{k}.Sensi           = dElementdp;

% k = k+1;
% elements{k}.type            = 'RigidBody2DElement';
% elements{k}.nodes           = [2];
% elements{k}.mass            = 0.1;
% elements{k}.J               = 1;
% 
% k = k+1;
% elements{k}.type            = 'RigidBody2DElement';
% elements{k}.nodes           = [3];
% elements{k}.mass            = 0.1;
% elements{k}.J               = 1;
% 
% k = k+1;
% elements{k}.type            = 'RigidBody2DElement';
% elements{k}.nodes           = [4];
% elements{k}.mass            = 0.1;
% elements{k}.J               = 1;

k = k+1;
elements{k}.type            = 'ExternalForceSE2';
elements{k}.nodes           = [3];
elements{k}.DOF             = 2;
elements{k}.amplitude       = 100;
elements{k}.frequency       = 0;

k = k+1;
elements{k}.type            = 'KinematicConstraint2D';
elements{k}.nodes           = [1 2 ];
elements{k}.A               = [0 0 1]';

k = k+1;
elements{k}.type            = 'KinematicConstraint2D';
elements{k}.nodes           = [3 4];
elements{k}.A               = [0 0 1]';

k = k+1;
elements{k}.type            = 'KinematicConstraint2D';
elements{k}.nodes           = [4 5];
elements{k}.A               = [0 1 0]';

BC = [1,5];

Model = FEModelSensi();
Model.CreateFEModelSensi(nodes,elements);
Model.defineBC(BC);
Model.defineCreteria(@Criteria)

G                   = DynamicIntegration(Model);
G.parameters.finaltime      = 10;
G.parameters.timestepsize   = 1.e-2;
G.parameters.relTolRes      = 1.e-8;
G.parameters.relTolPhi      = 1.e-8;
G.parameters.scaling        = 1.e+8; 
G.parameters.rho            = 0.8; 
G.parameters.itMax          = 10;

G.runIntegration();
% G.sIP.LinConst      = 1; 
% G.model.Visu()


starttime= 100;
hip = sqrt(Model.listElements{1}.listNodes{2}.position(1,starttime:end).^2 +Model.listElements{1}.listNodes{2}.position(2,starttime:end).^2);
sinTeta = Model.listElements{1}.listNodes{2}.position(1,starttime:end)./hip;        
cosTeta = Model.listElements{1}.listNodes{2}.position(2,starttime:end)./hip;
tanTeta = sinTeta./cosTeta;
cotanTeta = cosTeta./sinTeta;

figure;
plot(sinTeta);hold on;grid on;
plot(cosTeta );hold on;grid on;
plot(sqrt(sinTeta.^2+cosTeta.^2));hold on;grid on;
legend('sin','cos','sqrt(sin2+cos2)');

figure;
plot(Model.listElements{1}.listNodes{1}.position(1,:),'--');hold on;grid on; 
plot(Model.listElements{1}.listNodes{1}.position(2,:),'--');
legend('x1','y1');

figure;
plot(Model.listElements{1}.listNodes{2}.position(1,:),'--');hold on;grid on; 
plot(Model.listElements{1}.listNodes{2}.position(2,:),'--');
plot(sqrt(Model.listElements{1}.listNodes{2}.position(1,:).^2 +Model.listElements{1}.listNodes{2}.position(2,:).^2)-L,'--');
legend('x2','y2','dL');

figure;
plot(abs(elements{2}.amplitude)*ones(1,size(Model.listElements{1}.listNodes{2}.position(1,starttime:end),2)));hold on;grid on;
plot( elements{1}.KCS(1,1)*abs(hip-L).*cosTeta,'--');hold on;grid on; 
legend('Fel','K*dL*cos(teta)');

figure;
plot(abs(elements{2}.amplitude)./abs(cosTeta));hold on;grid on;
plot( elements{1}.KCS(1,1) *abs( hip-L ),'--');hold on;grid on; 
legend('F/cos(Teta)','K*dL');

G.runSensi();
format LONGE
G.model.sensi
G.model.crite



function [F,dF,G,dG] = Criteria(model,parameters,timestep)
nf = 2;
if nargin == 0
    F = nf;
    return
end

F  = cell(nf,1);
G  = cell(nf,1);
dF = cell(nf,1);
dG = cell(nf,1);

qSensi = zeros(1,model.nDof);
qSensi(model.listElements{1}.listNodes{2}.listNumberDof) = model.listElements{1}.listNodes{2}.position(timestep);
    
dF{2}.q = qSensi;
dG{1}.q = qSensi;

qCrite = model.listElements{1}.listNodes{2}.position(timestep)-model.listElements{1}.natural_length;
F{2}.q = qCrite;
G{1}.q = qCrite;


end
