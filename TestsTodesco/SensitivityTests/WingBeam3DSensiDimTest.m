%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Test wing 3D 
%
% Juliano 2018-02-26
%

function [q] = WingBeam3DSensiDimTest(b_,plots)
alpha = 4.5896e-3;
beta  = 0.02;
a_   = 0.01*ones(9,1);
% b_   = 0.01*ones(9,1);
rho_ = 2000*ones(9,1);
E_   = 20e9*ones(9,1); 
nu_  = 0.3*ones(9,1); 

%%
Nodes_AGARD = [0.000000000000000E+000  0.000000000000000E+000  0.000000000000000E+000
  5.577839932590724E-002  0.000000000000000E+000  0.000000000000000E+000
  0.111556798651814       0.000000000000000E+000  0.000000000000000E+000
  0.167335197977722       0.000000000000000E+000  0.000000000000000E+000
  0.223113597303629       0.000000000000000E+000  0.000000000000000E+000
  0.273049996700138       0.000000000000000E+000  0.000000000000000E+000
  0.334670395955443       0.000000000000000E+000  0.000000000000000E+000
  0.390448795281351       0.000000000000000E+000  0.000000000000000E+000
  0.446227194607258       0.000000000000000E+000  0.000000000000000E+000
  0.502005593933165       0.000000000000000E+000  0.000000000000000E+000
  0.557783993259072       0.000000000000000E+000  0.000000000000000E+000
  8.093963902182877E-002  0.000000000000000E+000  7.619999907910824E-002
  0.134822182370648       0.000000000000000E+000  7.619999907910824E-002
  0.188704725719467       0.000000000000000E+000  7.619999907910824E-002
  0.242587269068286       0.000000000000000E+000  7.619999907910824E-002
  0.296468796417117       0.000000000000000E+000  7.619999907910824E-002
  0.346709995809942       0.000000000000000E+000  7.619999907910824E-002
  0.404235915114731       0.000000000000000E+000  7.619999907910824E-002
  0.458116934463568       0.000000000000000E+000  7.619999907910824E-002
  0.512000493812375       0.000000000000000E+000  7.619999907910824E-002
  0.565881513161212       0.000000000000000E+000  7.619999907910824E-002
  0.619765072510019       0.000000000000000E+000  7.619999907910824E-002
  0.161879278043658       0.000000000000000E+000  0.152399998158216     
  0.213865965415388       0.000000000000000E+000  0.152399998158216     
  0.265851636787131       0.000000000000000E+000  0.152399998158216     
  0.317840356158838       0.000000000000000E+000  0.152399998158216     
  0.369826535530575       0.000000000000000E+000  0.152399998158216     
  0.421639994904399       0.000000000000000E+000  0.152399998158216     
  0.473798894274049       0.000000000000000E+000  0.152399998158216     
  0.525785073645785       0.000000000000000E+000  0.152399998158216     
  0.577773793017492       0.000000000000000E+000  0.152399998158216     
  0.629759972389229       0.000000000000000E+000  0.152399998158216     
  0.681746151760966       0.000000000000000E+000  0.152399998158216     
  0.242818917065486       0.000000000000000E+000  0.228599997237325     
  0.292910256460123       0.000000000000000E+000  0.228599997237325     
  0.343001595854759       0.000000000000000E+000  0.228599997237325     
  0.393090395249426       0.000000000000000E+000  0.228599997237325     
  0.443181734644063       0.000000000000000E+000  0.228599997237325     
  0.495299994014204       0.000000000000000E+000  0.228599997237325     
  0.543364413433336       0.000000000000000E+000  0.228599997237325     
  0.593455752827972       0.000000000000000E+000  0.228599997237325     
  0.643544552222639       0.000000000000000E+000  0.228599997237325     
  0.693635891617276       0.000000000000000E+000  0.228599997237325     
  0.743727231011912       0.000000000000000E+000  0.228599997237325     
  0.323758556087315       0.000000000000000E+000  0.304799996316433     
  0.371952515504882       0.000000000000000E+000  0.304799996316433     
  0.420149014922418       0.000000000000000E+000  0.304799996316433     
  0.468342974339984       0.000000000000000E+000  0.304799996316433     
  0.516539473757520       0.000000000000000E+000  0.304799996316433     
  0.566419993154705       0.000000000000000E+000  0.304799996316433     
  0.612927392592654       0.000000000000000E+000  0.304799996316433     
  0.661123892010190       0.000000000000000E+000  0.304799996316433     
  0.709317851427756       0.000000000000000E+000  0.304799996316433     
  0.757514350845292       0.000000000000000E+000  0.304799996316433     
  0.805708310262859       0.000000000000000E+000  0.304799996316433     
  0.404698195109144       0.000000000000000E+000  0.380999995395541     
  0.450997314549610       0.000000000000000E+000  0.380999995395541     
  0.497296433990076       0.000000000000000E+000  0.380999995395541     
  0.543595553430542       0.000000000000000E+000  0.380999995395541     
  0.589894672871008       0.000000000000000E+000  0.380999995395541     
  0.640079992264509       0.000000000000000E+000  0.380999995395541     
  0.682492911751941       0.000000000000000E+000  0.380999995395541     
  0.728792031192407       0.000000000000000E+000  0.380999995395541     
  0.775091150632873       0.000000000000000E+000  0.380999995395541     
  0.821390270073339       0.000000000000000E+000  0.380999995395541     
  0.867689389513806       0.000000000000000E+000  0.380999995395541     
  0.485637834130973       0.000000000000000E+000  0.457199994474649     
  0.530042113594338       0.000000000000000E+000  0.457199994474649     
  0.574443853057735       0.000000000000000E+000  0.457199994474649     
  0.618848132521100       0.000000000000000E+000  0.457199994474649     
  0.663252411984466       0.000000000000000E+000  0.457199994474649     
  0.713739991374314       0.000000000000000E+000  0.457199994474649     
  0.752058430911228       0.000000000000000E+000  0.457199994474649     
  0.796460170374624       0.000000000000000E+000  0.457199994474649     
  0.840864449837990       0.000000000000000E+000  0.457199994474649     
  0.885268729301356       0.000000000000000E+000  0.457199994474649     
  0.929670468764752       0.000000000000000E+000  0.457199994474649     
  0.566577473152801       0.000000000000000E+000  0.533399993553758     
  0.609084372639097       0.000000000000000E+000  0.533399993553758     
  0.651593812125362       0.000000000000000E+000  0.533399993553758     
  0.694100711611658       0.000000000000000E+000  0.533399993553758     
  0.736607611097954       0.000000000000000E+000  0.533399993553758     
  0.784859990514815       0.000000000000000E+000  0.533399993553758     
  0.821621410070546       0.000000000000000E+000  0.533399993553758     
  0.864130849556811       0.000000000000000E+000  0.533399993553758     
  0.906637749043107       0.000000000000000E+000  0.533399993553758     
  0.949144648529403       0.000000000000000E+000  0.533399993553758     
  0.991651548015699       0.000000000000000E+000  0.533399993553758     
  0.647517112174630       0.000000000000000E+000  0.609599992632866     
  0.688129171683826       0.000000000000000E+000  0.609599992632866     
  0.728741231193021       0.000000000000000E+000  0.609599992632866     
  0.769353290702216       0.000000000000000E+000  0.609599992632866     
  0.809962810211442       0.000000000000000E+000  0.609599992632866     
  0.855979989655316       0.000000000000000E+000  0.609599992632866     
  0.891186929229833       0.000000000000000E+000  0.609599992632866     
  0.931798988739029       0.000000000000000E+000  0.609599992632866     
  0.972411048248224       0.000000000000000E+000  0.609599992632866     
   1.01302056775745       0.000000000000000E+000  0.609599992632866     
   1.05363262726665       0.000000000000000E+000  0.609599992632866     
  0.728456751196459       0.000000000000000E+000  0.685799991711974     
  0.767173970728554       0.000000000000000E+000  0.685799991711974     
  0.805888650260679       0.000000000000000E+000  0.685799991711974     
  0.844603329792805       0.000000000000000E+000  0.685799991711974     
  0.883320549324900       0.000000000000000E+000  0.685799991711974     
  0.932179988734424       0.000000000000000E+000  0.685799991711974     
  0.960752448389120       0.000000000000000E+000  0.685799991711974     
  0.999467127921246       0.000000000000000E+000  0.685799991711974     
   1.03818180745337       0.000000000000000E+000  0.685799991711974     
   1.07689902698547       0.000000000000000E+000  0.685799991711974     
   1.11561370651759       0.000000000000000E+000  0.685799991711974     
  0.809396390218288       0.000000000000000E+000  0.761999990791082     
  0.846216229773313       0.000000000000000E+000  0.761999990791082     
  0.883036069328338       0.000000000000000E+000  0.761999990791082     
  0.919855908883363       0.000000000000000E+000  0.761999990791082     
  0.956675748438388       0.000000000000000E+000  0.761999990791082     
   1.00329998787493       0.000000000000000E+000  0.761999990791082     
   1.03031542754844       0.000000000000000E+000  0.761999990791082     
   1.06713526710346       0.000000000000000E+000  0.761999990791082     
   1.10395510665849       0.000000000000000E+000  0.761999990791082     
   1.14077494621351       0.000000000000000E+000  0.761999990791082     
   1.17759478576854       0.000000000000000E+000  0.761999990791082];    

% figure(1)
% plot(Nodes_AGARD(:,1),Nodes_AGARD(:,3), 'c*'),hold on;
% grid on;
% Node_list = [3 6 9 36 39 42 69 72 75 113 116 119];
% plot(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,3), 'r*','MarkerSize',10),hold on;
% Node_list = [3 36 69 113 ];
% plot(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,3), 'g','LineWidth',2),hold on;
% Node_list = [6 39 72 116];
% plot(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,3), 'g','LineWidth',2),hold on;
% Node_list = [9 42 75 119];
% plot(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,3), 'g','LineWidth',2),hold on;
% Node_list = [ 36 39];
% plot(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,3), 'm','LineWidth',2),hold on;
% Node_list = [ 39 42];
% plot(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,3), 'b','LineWidth',2),hold on;
% Node_list = [ 69 72];
% plot(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,3), 'm','LineWidth',2),hold on;
% Node_list = [72 75];
% plot(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,3), 'b','LineWidth',2),hold on;
% Node_list = [ 113 116];
% plot(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,3), 'm','LineWidth',2),hold on;
% Node_list = [ 116 119];
% plot(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,3), 'b','LineWidth',2),hold on;
% Node_list = [3 6 9 ];
% plot(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,3), 'bs','MarkerSize',20),hold on;
% figure(10)
% plot3(Nodes_AGARD(:,1),Nodes_AGARD(:,2),Nodes_AGARD(:,3), 'c*'),hold on;    grid on;
% Node_list = [3 6 9 36 39 42 69 72 75 113 116 119];
% plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'r*','MarkerSize',10),hold on;
% Node_list = [3 36 69 113 ];
% plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'g','LineWidth',1e+4*a_(1)*b_(1)),hold on;
% Node_list = [6 39 72 116];
% plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'g','LineWidth',1e+4*a_(2)*b_(2)),hold on;
% Node_list = [9 42 75 119];
% plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'g','LineWidth',1e+4*a_(3)*b_(3)),hold on;
% Node_list = [ 36 39];
% plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'm','LineWidth',1e+4*a_(4)*b_(4)),hold on;
% Node_list = [ 39 42];
% plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'b','LineWidth',1e+4*a_(5)*b_(5)),hold on;
% Node_list = [ 69 72];
% plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'm','LineWidth',1e+4*a_(6)*b_(6)),hold on;
% Node_list = [72 75];
% plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'b','LineWidth',1e+4*a_(7)*b_(7)),hold on;
% Node_list = [ 113 116];
% plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'm','LineWidth',1e+4*a_(8)*b_(8)),hold on;
% Node_list = [ 116 119];
% plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'b','LineWidth',1e+4*a_(9)*b_(9)),hold on;
% Node_list = [3 6 9 ];
% plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'bs','MarkerSize',10),hold on;

%%

grav = [0 -9.81*0 0];

% Nodes's Beam 1
Start1  = 1;
nodes(Start1,:)   =  [Start1   Nodes_AGARD(3,:)];
nodes(Start1+1,:) =  [Start1+1 Nodes_AGARD(113,:)];
nElem1  = 10; 
nodes   = createInterNodes(nodes,nElem1,Start1,Start1+1);
End1    = Start1+nElem1;
% Element 1: Beam
nBeam =1;
a   = a_(nBeam);
b   = b_(nBeam);
rho = rho_(nBeam);
E   = E_(nBeam); 
nu  = nu_(nBeam);

A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;
G = E/(2*(1+nu));
KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

type = 'FlexibleBeamElement';
count = 0;
for i = 1:nElem1
    elements{count+i}.type  = type;
    elements{count+i}.nodes = [Start1+i-1 Start1+i];
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.KCS   = KCS;
    elements{count+i}.MCS   = MCS;
    elements{count+i}.alpha = alpha; % damping proportional to Mass 4.5896e-4
    elements{count+i}.beta  = beta ; % damping proportional to Stiffness 0.1329
    elements{count+i}.g     = grav;
end

%%

% Nodes's Beam 2
Start2  = End1+1;
nodes(Start2,:) =  [Start2   Nodes_AGARD(6,:)];
nodes(Start2+1,:) =  [Start2+1 Nodes_AGARD(116,:)];
nElem2  = nElem1; 
nodes   = createInterNodes(nodes,nElem2,Start2,Start2+1 );
End2    = Start2+nElem2;
% Element 2: Beam
nBeam =2;
a   = a_(nBeam);
b   = b_(nBeam);
rho = rho_(nBeam);
E   = E_(nBeam); 
nu  = nu_(nBeam);

A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;
G = E/(2*(1+nu));
KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

type = 'FlexibleBeamElement';
count = size(elements,2);
for i = 1:nElem2
    elements{count+i}.type  = type;
    elements{count+i}.nodes = [Start2+i-1 Start2+i];
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.KCS   = KCS;
    elements{count+i}.MCS   = MCS;
    elements{count+i}.alpha = alpha; % damping proportional to Mass 4.5896e-4
    elements{count+i}.beta  = beta; % damping proportional to Stiffness 0.1329
    elements{count+i}.g     = grav;
end

% %%
% 
% % Nodes's Beam 3
Start3  = End2+1;
nodes(Start3,:) =  [Start3   Nodes_AGARD(9,:)];
nodes(Start3+1,:) =  [Start3+1 Nodes_AGARD(119,:)];
nElem3  = nElem1; 
nodes   = createInterNodes(nodes,nElem3,Start3,Start3+1 );
End3    = Start3+nElem3;
% Element 3: Beam
nBeam =3;
a   = a_(nBeam);
b   = b_(nBeam);
rho = rho_(nBeam);
E   = E_(nBeam); 
nu  = nu_(nBeam);

A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;
G = E/(2*(1+nu));
KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

type = 'FlexibleBeamElement';
count = size(elements,2);
for i = 1:nElem3
    elements{count+i}.type  = type;
    elements{count+i}.nodes = [Start3+i-1 Start3+i];
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.KCS   = KCS;
    elements{count+i}.MCS   = MCS;
    elements{count+i}.alpha = alpha; % damping proportional to Mass 4.5896e-4
    elements{count+i}.beta  = beta; % damping proportional to Stiffness 0.1329
    elements{count+i}.g     = grav;
end

%%

% Nodes's Beam 4
Start4  = End3+1;
nodes(Start4,:)   =  [Start4 nodes(round(Start1+(3*nElem1/11)),2:4)];
nodes(Start4+1,:) =  [Start4+1 nodes(round(Start2+(3*nElem2/11)),2:4)];
nElem4  = 3; 
nodes   = createInterNodes(nodes,nElem4,Start4,Start4+1);
nodes(Start4+nElem4,:)   =[];
nodes(Start4,:) =[];
End4    = Start4+nElem4-2;
nodes(Start4:End4,1) = Start4:End4; 
% Element 4: Beam
nBeam =4;
a   = a_(nBeam);
b   = b_(nBeam);
rho = rho_(nBeam);
E   = E_(nBeam); 
nu  = nu_(nBeam);

A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;
G = E/(2*(1+nu));
KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

type = 'FlexibleBeamElement';
count = size(elements,2);
for i = 1:nElem4
    elements{count+i}.type  = type;
    elements{count+i}.nodes                  = [Start4+i-2                  Start4+i-1];
    if(i==1)      elements{count+i}.nodes    = [round(Start1+(3*nElem1/11)) Start4+i-1]; end
    if(i==nElem4) elements{count+i}.nodes    = [Start4+i-2                  round(Start2+(3*nElem2/11))]; end
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.KCS   = KCS;
    elements{count+i}.MCS   = MCS;
    elements{count+i}.alpha = alpha; % damping proportional to Mass 4.5896e-4
    elements{count+i}.beta  = beta; % damping proportional to Stiffness 0.1329
    elements{count+i}.g     = grav;
end

%%

% Nodes's Beam 5
Start5  = End4+1;
nodes(Start5,:)   =  [Start5 nodes(round(Start2+(3*nElem2/11)),2:4)];
nodes(Start5+1,:) =  [Start5+1 nodes(round(Start3+(3*nElem3/11)),2:4)];
nElem5  = nElem4 ; 
nodes   = createInterNodes(nodes,nElem5,Start5,Start5+1);
nodes(Start5+nElem5,:)   =[];
nodes(Start5,:) =[];
End5    = Start5+nElem5-2;
nodes(Start5:End5,1) = Start5:End5; 
% Element 5: Beam
nBeam =5;
a   = a_(nBeam);
b   = b_(nBeam);
rho = rho_(nBeam);
E   = E_(nBeam); 
nu  = nu_(nBeam);

A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;
G = E/(2*(1+nu));
KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

type = 'FlexibleBeamElement';
count = size(elements,2);
for i = 1:nElem5
    elements{count+i}.type  = type;
    elements{count+i}.nodes                  = [Start5+i-2                  Start5+i-1];
    if(i==1)      elements{count+i}.nodes    = [round(Start2+(3*nElem2/11)) Start5+i-1]; end
    if(i==nElem5) elements{count+i}.nodes    = [Start5+i-2                  round(Start3+(3*nElem3/11))]; end
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.KCS   = KCS;
    elements{count+i}.MCS   = MCS;
    elements{count+i}.alpha = alpha; % damping proportional to Mass 4.5896e-4
    elements{count+i}.beta  = beta; % damping proportional to Stiffness 0.1329
    elements{count+i}.g     = grav;
end

%%

% Nodes's Beam 6
Start6  = End5+1;
nodes(Start6,:)   =  [Start6 nodes(round(Start1+(7*nElem1/11)),2:4)];
nodes(Start6+1,:) =  [Start6+1 nodes(round(Start2+(7*nElem2/11)),2:4)];
nElem6  = nElem4; 
nodes   = createInterNodes(nodes,nElem6,Start6,Start6+1);
nodes(Start6+nElem6,:)   =[];
nodes(Start6,:) =[];
End6    = Start6+nElem6-2;
nodes(Start6:End6,1) = Start6:End6; 
% Element 6: Beam
nBeam =6;
a   = a_(nBeam);
b   = b_(nBeam);
rho = rho_(nBeam);
E   = E_(nBeam); 
nu  = nu_(nBeam);

A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;
G = E/(2*(1+nu));
KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

type = 'FlexibleBeamElement';
count = size(elements,2);
for i = 1:nElem6
    elements{count+i}.type  = type;
    elements{count+i}.nodes                  = [Start6+i-2                  Start6+i-1];
    if(i==1)      elements{count+i}.nodes    = [round(Start1+(7*nElem1/11)) Start6+i-1]; end
    if(i==nElem6) elements{count+i}.nodes    = [Start6+i-2                  round(Start2+(7*nElem2/11))]; end
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.KCS   = KCS;
    elements{count+i}.MCS   = MCS;
    elements{count+i}.alpha = alpha; % damping proportional to Mass 4.5896e-4
    elements{count+i}.beta  = beta; % damping proportional to Stiffness 0.1329
    elements{count+i}.g     = grav;
end

%%

% Nodes's Beam 7
Start7  = End6+1;
nodes(Start7,:)   =  [Start7 nodes(round(Start2+(7*nElem2/11)),2:4)];
nodes(Start7+1,:) =  [Start7+1 nodes(round(Start3+(7*nElem3/11)),2:4)];
nElem7  = nElem4 ; 
nodes   = createInterNodes(nodes,nElem7,Start7,Start7+1);
nodes(Start7+nElem7,:)   =[];
nodes(Start7,:) =[];
End7    = Start7+nElem7-2;
nodes(Start7:End7,1) = Start7:End7; 
% Element 7: Beam
nBeam =7;
a   = a_(nBeam);
b   = b_(nBeam);
rho = rho_(nBeam);
E   = E_(nBeam); 
nu  = nu_(nBeam);

A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;
G = E/(2*(1+nu));
KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

type = 'FlexibleBeamElement';
count = size(elements,2);
for i = 1:nElem7
    elements{count+i}.type  = type;
    elements{count+i}.nodes                  = [Start7+i-2                  Start7+i-1];
    if(i==1)      elements{count+i}.nodes    = [round(Start2+(7*nElem2/11)) Start7+i-1]; end
    if(i==nElem7) elements{count+i}.nodes    = [Start7+i-2                  round(Start3+(7*nElem3/11))]; end
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.KCS   = KCS;
    elements{count+i}.MCS   = MCS;
    elements{count+i}.alpha = alpha; % damping proportional to Mass 4.5896e-4
    elements{count+i}.beta  = beta; % damping proportional to Stiffness 0.1329
    elements{count+i}.g     = grav;
end

%%

% Nodes's Beam 8
Start8  = End7+1;
nodes(Start8,:)   =  [Start8 nodes(End1,2:4)];
nodes(Start8+1,:) =  [Start8+1 nodes(End2,2:4)];
nElem8  = nElem4; 
nodes   = createInterNodes(nodes,nElem8,Start8,Start8+1);
nodes(Start8+nElem8,:)   =[];
nodes(Start8,:) =[];
End8    = Start8+nElem8-2;
nodes(Start8:End8,1) = Start8:End8; 
% Element 8: Beam
nBeam =8;
a   = a_(nBeam);
b   = b_(nBeam);
rho = rho_(nBeam);
E   = E_(nBeam); 
nu  = nu_(nBeam);

A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;
G = E/(2*(1+nu));
KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

type = 'FlexibleBeamElement';
count = size(elements,2);
for i = 1:nElem8
    elements{count+i}.type  = type;
    elements{count+i}.nodes                  = [Start8+i-2 Start8+i-1];
    if(i==1)      elements{count+i}.nodes    = [End1       Start8+i-1]; end
    if(i==nElem8) elements{count+i}.nodes    = [Start8+i-2 End2      ]; end
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.KCS   = KCS;
    elements{count+i}.MCS   = MCS;
    elements{count+i}.alpha = alpha; % damping proportional to Mass 4.5896e-4
    elements{count+i}.beta  = beta; % damping proportional to Stiffness 0.1329
    elements{count+i}.g     = grav;
end

%%

% Nodes's Beam 9
Start9  = End8+1;
nodes(Start9,:)   =  [Start9 nodes(End2,2:4)];
nodes(Start9+1,:) =  [Start9+1 nodes(End3,2:4)];
nElem9  = nElem4 ; 
nodes   = createInterNodes(nodes,nElem9,Start9,Start9+1);
nodes(Start9+nElem9,:)   =[];
nodes(Start9,:) =[];
End9    = Start9+nElem9-2;
nodes(Start9:End9,1) = Start9:End9; 
% Element 9: Beam
nBeam =9;
a   = a_(nBeam);
b   = b_(nBeam);
rho = rho_(nBeam);
E   = E_(nBeam); 
nu  = nu_(nBeam);

A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;
G = E/(2*(1+nu));
KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

type = 'FlexibleBeamElement';
count = size(elements,2);
for i = 1:nElem9
    elements{count+i}.type  = type;
    elements{count+i}.nodes                  = [Start9+i-2 Start9+i-1];
    if(i==1)      elements{count+i}.nodes    = [End2       Start9+i-1]; end
    if(i==nElem9) elements{count+i}.nodes    = [Start9+i-2 End3      ]; end
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.KCS   = KCS;
    elements{count+i}.MCS   = MCS;
    elements{count+i}.alpha = alpha; % damping proportional to Mass 4.5896e-4
    elements{count+i}.beta  = beta; % damping proportional to Stiffness 0.1329
    elements{count+i}.g     = grav;
end

%% Definition of time parameters
finaltime = 0.15;
timestepsize = 0.005;

f = zeros(1,round(finaltime/timestepsize)+1);
% f = 100*sin(2*pi*2*(0:timestepsize:finaltime));
f(2) = 20;
f(3) = 200;
f(4) = 20;
count = size(elements,2)+1;
elements{count}.type = 'punctualForce';
elements{count}.nodes = 33;
elements{count}.DOF = 2;
elements{count}.f = f;

f = zeros(1,round(finaltime/timestepsize)+1);
% f = 100*sin(2*pi*2*(0:timestepsize:finaltime));
f(2) = -20;
f(3) = -250;
f(4) = -20;
count = size(elements,2)+1;
elements{count}.type = 'punctualForce';
elements{count}.nodes = 11;
elements{count}.DOF = 2;
elements{count}.f = f;

%% %-----------------------------------------------------------------------

%% Definition of the boundary conditions

BC = [Start1 Start2 Start3]; % Node 1 is fixed

%% Definition of the finite element model (FEModel Object)
% Based on previously defined nodes and elements
Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

%% Definition of the solver and its parameters
% tic
D = DynamicIntegration(Model);  % Creatind the Dynamic integration object
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.0;
D.parameters.scaling = 1e6;
D.runIntegration(); % Run the integration of the object

% S = StaticIntegration(Model);  % Creatind the Dynamic integration object
% S.parameters.finaltime = finaltime;
% S.parameters.timestepsize = timestepsize;
% % S.parameters.rho = 0.0;
% S.parameters.scaling = 1e6;
% S.runIntegration(); % Run the integration of the object

q = zeros(6*size(Model.listNodes,2),size(Model.listNodes{i}.position,2));
Nodes = zeros(size(Model.listNodes,2),3);
for i = 1:size(Model.listNodes,2)
    q(i*6-5:i*6-3,:) = Model.listNodes{i}.position;
    Nodes(i,:) = Model.listNodes{i}.position(:,end)';
end

% elapsed = toc;
% disp(['Computation lasted ',num2str(elapsed/60),' min.'])
% % Visualization and plots
% To visualize the model:(uncomment the following line)
% Model.Visu
% 
% 
if plots
    
    Model.Visu([],1)
    grid on
    
%     figure
%     plot(q(33*6-5,:));hold on
    figure
    plot(q(33*6-4,:));hold on
%     figure
%     plot(q(33*6-3,:));hold on
 
    figure
    Node_list = [3 36 69 113 ];
    plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'g','LineWidth',1e+3*a_(1)*b_(1)),hold on;
    Node_list = [6 39 72 116];
    plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'g','LineWidth',1e+3*a_(2)*b_(2)),hold on;
    Node_list = [9 42 75 119];
    plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'g','LineWidth',1e+3*a_(3)*b_(3)),hold on;
    Node_list = [ 36 39];
    plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'm','LineWidth',1e+3*a_(4)*b_(4)),hold on;
    Node_list = [ 39 42];
    plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'b','LineWidth',1e+3*a_(5)*b_(5)),hold on;
    Node_list = [ 69 72];
    plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'm','LineWidth',1e+3*a_(6)*b_(6)),hold on;
    Node_list = [72 75];
    plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'b','LineWidth',1e+3*a_(7)*b_(7)),hold on;
    Node_list = [ 113 116];
    plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'm','LineWidth',1e+3*a_(8)*b_(8)),hold on;
    Node_list = [ 116 119];
    plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'b','LineWidth',1e+3*a_(9)*b_(9)),hold on;
    Node_list = [3 6 9 ];
    plot3(Nodes_AGARD(Node_list,1),Nodes_AGARD(Node_list,2),Nodes_AGARD(Node_list,3), 'bs','MarkerSize',10),hold on;
    
    Node_list = [1  2  3  4  5  6  7  8  9  10 11];
    plot3(Nodes(Node_list,1),Nodes(Node_list,2),Nodes(Node_list,3), 'g','LineWidth',1e+3*a_(1)*b_(1)),hold on;
    Node_list = [12 13 14 15 16 17 18 19 20 21 22];
    plot3(Nodes(Node_list,1),Nodes(Node_list,2),Nodes(Node_list,3), 'g','LineWidth',1e+3*a_(2)*b_(2)),hold on;
    Node_list = [23 24 25 26 27 28 29 30 31 32 33];
    plot3(Nodes(Node_list,1),Nodes(Node_list,2),Nodes(Node_list,3), 'g','LineWidth',1e+3*a_(3)*b_(3)),hold on;
    Node_list = [ 4 34 35 15];
    plot3(Nodes(Node_list,1),Nodes(Node_list,2),Nodes(Node_list,3), 'm','LineWidth',1e+3*a_(4)*b_(4)),hold on;
    Node_list = [ 15 36 37 26];
    plot3(Nodes(Node_list,1),Nodes(Node_list,2),Nodes(Node_list,3), 'b','LineWidth',1e+3*a_(5)*b_(5)),hold on;
    Node_list = [ 7 38 39 18];
    plot3(Nodes(Node_list,1),Nodes(Node_list,2),Nodes(Node_list,3), 'm','LineWidth',1e+3*a_(6)*b_(6)),hold on;
    Node_list = [ 18 40 41 29];
    plot3(Nodes(Node_list,1),Nodes(Node_list,2),Nodes(Node_list,3), 'b','LineWidth',1e+3*a_(7)*b_(7)),hold on;
    Node_list = [ 11 42 43 22];
    plot3(Nodes(Node_list,1),Nodes(Node_list,2),Nodes(Node_list,3), 'm','LineWidth',1e+3*a_(8)*b_(8)),hold on;
    Node_list = [ 22 44 45 33];
    plot3(Nodes(Node_list,1),Nodes(Node_list,2),Nodes(Node_list,3), 'b','LineWidth',1e+3*a_(9)*b_(9)),hold on;
    Node_list = [ Start1 Start2 Start3];
    plot3(Nodes(Node_list,1),Nodes(Node_list,2),Nodes(Node_list,3), 'bs','MarkerSize',10),hold on;
    axis([0 2 -0.025 0.025 0 1]);grid on

%     figure
%     plot(nodes(:,2),nodes(:,4), '.','MarkerSize',20);hold on;axis('equal');grid on
%     plot(q(1:6:end,end),q(3:6:end,end), '.','MarkerSize',20);axis('equal');grid on
%     
%     figure
%     plot(nodes(:,2),nodes(:,3), '.','MarkerSize',20);hold on;axis('equal');grid on
%     plot(q(1:6:end,end),q(2:6:end,end), '.','MarkerSize',20);axis('equal');grid on
%     
%     figure
%     plot(nodes(:,3),nodes(:,4), '.','MarkerSize',20);hold on;axis('equal');grid on
%     plot(q(2:6:end,end),q(3:6:end,end), '.','MarkerSize',20);axis('equal');grid on

end 
% figure(1);hold on
% VisuMatlab(model,q,numericalParameters,0); 

q =norm(q(2:6:end,end));

end