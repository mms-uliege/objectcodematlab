%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%

%   Sensitivity calculation of a Cantilever beam 3D 
%
%   Juliano Todesco
%   02/08/2018
%-------------------------------------------------------------------------%

% Test example for a uniform 3D beam loaded dynamically 
clear all
% close all

%% Definition of time parameters
finaltime       = 0.01;
timestepsize    = 0.001;

%% Definition of the nodes of the model (as matrix)
nodes = [1 0 0 0;
         2 1 0 0];
     
grav = [0 0 -9.81 ];

%% Definition of the elements of the model (as structure)
% Number of beam elements to build the model
nElem = 6;
Start = 1;
End = 2;
% create new nodes to have more beam elements
nodes = createInterNodes(nodes,nElem,Start,End);

% Beam parameters
a = 0.01;
b = 0.01;
rho = 7500;
E = 2e11; 
nu = 0.3; 

%%
% G = E/(2*(1+nu));
% A = a*b; 
% Jxx = a*b*(a^2+b^2)/12; 
% Iyy = b*a^3/12;
% Izz = a*b^3/12;
% KCS = diag( [E*A G*A*5/6 G*A*5/6 G*Jxx E*Iyy E*Izz] );
% MCS = diag( rho*[A A A Jxx Iyy Izz] );
% count = 0;
% type = 'FlexibleBeamElement';
% for i = 1:nElem
%     elements{count+i}.type  = type;
%     elements{count+i}.nodes = [nodes(Start+i-1,1) nodes(Start+i,1)];
%     elements{count+i}.yAxis = [0 1 0];
%     elements{count+i}.KCS   = KCS;
%     elements{count+i}.MCS   = MCS;
%     elements{count+i}.alpha = 5e-3; % damping proportional to Mass 4.5896e-4
%     elements{count+i}.beta  = 5e-3; % damping proportional to Stiffness 0.1329
%     elements{count+i}.g     = grav;
%     elements{count+i}.listM  = [1 0 0];%[1 0 0];%[1 1 0];%[1 1 1]
%     elements{count+i}.listCt = [1 0 0];%[1 0 0];%[1 1 0];
%     elements{count+i}.listKt = [1 0 0];%[1 0 0];%[1 1 0];
%     
%     dElementdp.sensi        = [0 0 0 1];% [d t E rho]         
%     dElementdp.A            = [a b 0 0]; 
%     dElementdp.I            = [a^3/12 b*a^2/4 0 0 ];      
%     dElementdp.Jt           = [(b^2*a/4+a^3/12) (b^3/12+b*a^2/4) 0 0];
%     dElementdp.E            = [1 0 0 0];     
%     dElementdp.nu           = [0 0 0 0]; 
%     dElementdp.rho          = [0 0 0 1];
%     dElementdp.betaM        = [0 0 0 0];
%     dElementdp.betaK        = [0 0 0 0];
%     dElementdp.step_fd      = [1e-8 1e-8 0 0];
%     elements{count+i}.Sensi  = dElementdp;
% end

%%
count = 0;
type = 'FlexibleBeamElement';
for i = 1:nElem
    elements{count+i}.type  = type;
    elements{count+i}.nodes = [nodes(Start+i-1,1) nodes(Start+i,1)];
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.A     = a*b;
    elements{count+i}.Izz   = b*a^3/12;
    elements{count+i}.Iyy   = a*b^3/12;
    elements{count+i}.Jxx   = a*b*(a^2+b^2)/12;
    elements{count+i}.E     = E;
    elements{count+i}.nu    = nu;
    elements{count+i}.rho   = rho;
    elements{count+i}.alpha = 5e-3; % damping proportional to Mass 4.5896e-4
    elements{count+i}.beta  = 5e-3; % damping proportional to Stiffness 0.1329
    elements{count+i}.g     = grav;
    elements{count+i}.listM = [1 0 0];%[1 0 0];%[1 1 0];%[1 1 1]
    elements{count+i}.listCt= [1 0 0];%[1 0 0];%[1 1 0];
    elements{count+i}.listKt= [1 0 0];%[1 0 0];%[1 1 0];
    dElementdp.sensi        = [1 0 0 0];% [a b E rho]         
    dElementdp.A            = [b a 0 0]; 
    dElementdp.Izz          = [b*a^2/4 a^3/12 0 0 ];      
    dElementdp.Iyy          = [a^3/12 b*a^2/4 0 0 ];      
    dElementdp.Jxx          = [(b^3/12+b*a^2/4) (b^2*a/4+a^3/12) 0 0];
    dElementdp.E            = [0 0 1 0];     
    dElementdp.nu           = [0 0 0 0]; 
    dElementdp.rho          = [0 0 0 1];
    dElementdp.alpha        = [0 0 0 0];
    dElementdp.beta         = [0 0 0 0];
    dElementdp.step_fd      = [1e-8 1e-8 1e+6 1e+1];
    elements{count+i}.Sensi = dElementdp;
end

%% Definition of the boundary conditions

BC = [1]; % Node 1 is fixed

%% Definition of the finite element model (FEModel Object)
% Based on previously defined nodes and elements
Model = FEModelSensi();
Model.CreateFEModelSensi(nodes,elements);
Model.defineBC(BC);
Model.defineCreteria(@Criteria)

%% Definition of the solver and its parameters
% tic
% D                 = DynamicIntegration(Model);  % Creatind the Dynamic integration object
% D.parameters.finaltime    = finaltime;
% D.parameters.timestepsize = timestepsize;
% D.parameters.rho          = 1;
% D.parameters.scaling      = 1e0;
% D.runIntegration(); % Run the integration of the object
% elapsed = toc;
% disp(['Computation lasted ',num2str(elapsed/60),' min.'])

D                       = SensitivityDM(Model);  % Creatind the Dynamic integration object
D.parameters.finaltime          = finaltime;
D.parameters.timestepsize       = timestepsize;
D.parameters.relTolRes          = 1.e-7;
D.parameters.relTolPhi          = 1.e-7;
D.parameters.scaling            = 1e0;
D.parameters.rho                = 0.9;
D.parameters.itMax              = 20;
D.runIntegration(); % Run the integration of the object
D.sIP.LinConst          = 0; 
D.runSensi();
D.model.sensi
D.model.crite

%% Visualization and plots
% To visualize the model:(uncomment the following line)
% Model.Visu

% Plots
endNodePos = Model.listNodes{end}.position;
figure(3)
plot(D.parameters.time,endNodePos(1,:)-nodes(end,2),D.parameters.time,endNodePos(2,:)-nodes(end,3),D.parameters.time,endNodePos(3,:)-nodes(end,4),'Linewidth',1)
grid on; hold on;
title('Tip of the beam position')
xlabel('Time [s]')
ylabel('Poistion [m]')
legend('X','Y','Z')

% timeOld = [ 0.23553 ] ; 

function [F,dF,G,dG] = Criteria(model,parameters,timestep)
nf = 2;
if nargin == 0
    F = nf;
    return
end

f = zeros(length(model.listNumberNodes));
for i = model.listNumberElements
    f(i:i+1,i:i+1) = f(i:i+1,i:i+1) + [2 1;1 2].*(model.listElements{i}.L)/6;
end

q = zeros(length(model.listNumberNodes),1);
for i = model.listNumberNodes
    q(i) = model.listNodes{i}.position(3,timestep);
end

dFdW   = 2*q'*f;
qSensi = zeros(1,model.nDof);
for i=  model.listNumberNodes
    qSensi(model.listNodes{i}.listDof{2}.numDof) = dFdW(i);
end

dG = cell(nf,1);dG{1}.q = qSensi;
dF = cell(nf,1);dF{2}.q = qSensi;

qCrite = q'*f*q;

F  = cell(nf,1); F{2}.q = qCrite;
G  = cell(nf,1); G{1}.q = qCrite;
end
