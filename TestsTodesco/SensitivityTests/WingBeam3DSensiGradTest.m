%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Test wing 3D 
%
% Juliano 2018-02-26
%

function [q,sensi] = WingBeam3DSensiGradTest(b_,plots)

q = WingBeam3DSensiTest(b_,plots);

%%
sensi = zeros(9,1);
bb_   = 0.01*ones(9,1);
for i=1:9
    epson = 1e-8;
    nBeam       = i;
    bb_(nBeam)     = b_(nBeam)-epson;    [norm_qb] = WingBeam3DSensiTest(bb_,0);
    bb_(nBeam)     = b_(nBeam)+epson;    [norm_qf] = WingBeam3DSensiTest(bb_,0);
    sensi(i)= (norm_qf-norm_qb)/2/epson;
end
end