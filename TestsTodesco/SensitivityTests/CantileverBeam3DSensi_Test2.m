%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%
%   Sensitivity calculation of a Cantilever beam 3D 
%
%   Juliano Todesco
%   20/11/2018
%-------------------------------------------------------------------------%
% Test example for a uniform 3D beam loaded dynamically 
 clear all
% close all
%  clc

%% Definition of time parameters
finaltime       = 0.2;
timestepsize	= 1e-3;
relTolRes    	= 1e-8;
relTolPhi    	= 1e-8;
scaling      	= 1e5;
rhoIP        	= 0.9;
itMax       	= 10;

%% Definition of the nodes of the model (as matrix)
nodes = [1 0 0 0;
         2 1 0 0];
     
grav = [0 0 0*-9.81 ];

%% Definition of the elements of the model (as structure)
% Number of beam elements to build the model
nElem = 4;
Start = 1;
End = 2;
% create new nodes to have more beam elements
nodes = createInterNodes(nodes,nElem,Start,End);

% Beam parameters
a   = 0.02;
b   = 0.02;
rho = 7800;
E   = 2.0e11; 
nu  = 0.3; 
damping =0.0097; 
freqBeam = 20; %rad/s
%%
count = 0;
type = 'FlexibleBeamElement';
for i = 1:nElem
    elements{count+i}.type  = type;
    elements{count+i}.nodes = [nodes(Start+i-1,1) nodes(Start+i,1)];
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.A     = a*b;
    elements{count+i}.Izz   = b*a^3/12;
    elements{count+i}.Iyy   = a*b^3/12;
    elements{count+i}.Jxx   = a*b*(a^2+b^2)/12;
    elements{count+i}.E     = E;
    elements{count+i}.nu    = nu;
    elements{count+i}.rho   = rho;
    elements{count+i}.alpha = 2*damping*freqBeam;  % damping proportional to Mass 
    elements{count+i}.beta  = 2*damping/freqBeam;  % damping proportional to Stiffness 
    elements{count+i}.g     = grav;
%     elements{count+i}.listM = 3;
%     elements{count+i}.listCt= 3;
%     elements{count+i}.listKt= 3; 
%     dElementdp.sensi        = [1 0 0 0];% [a b E rho]         
%     dElementdp.A            = [b a 0 0]; 
%     dElementdp.Izz          = [b*a^2/4 a^3/12 0 0 ];      
%     dElementdp.Iyy          = [a^3/12 b*a^2/4 0 0 ];      
%     dElementdp.Jxx          = [(b^3/12+b*a^2/4) (b^2*a/4+a^3/12) 0 0];
%     dElementdp.E            = [0 0 1 0];     
%     dElementdp.nu           = [0 0 0 0]; 
%     dElementdp.rho          = [0 0 0 1];
%     dElementdp.alpha        = [0 0 0 0];
%     dElementdp.beta         = [0 0 0 0];
%     dElementdp.step_fd      = [1e-8 1e-8 1e+6 1e+1];
%     elements{count+i}.Sensi = dElementdp;
end

k = count+i+1;
k_Force = k;
Periodo = 0.05;
if Periodo/timestepsize>finaltime/timestepsize
   nStepsForce = finaltime/timestepsize;
   nFinalForce = finaltime;
else
   nStepsForce = Periodo/timestepsize;
   nFinalForce = Periodo;
end
elements{k}.type            = 'ExternalForce';
elements{k}.nodes           = [nElem+1];
elements{k}.DOF             = 3;
elements{k}.amplitude       = zeros(1,finaltime/timestepsize +1);
elements{k}.amplitude([int32(1:1:nStepsForce+1)]) = 1000*cos(pi/2 + 2*pi*(1/Periodo)*[0:timestepsize:nFinalForce]);
elements{k}.frequency       = 0;

%% Definition of the boundary conditions
BC = [1]; % Node 1 is fixed
%
%% Definition of the finite element model (FEModel Object)
% Based on previously defined nodes and elements
Model = FEModelSensi();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);
Model.defineCreteria(@Criteria)
% Definition of the solver and its parameters
tic;disp('Dynamic Integration');
D                 = DynamicIntegration(Model);  % Creatind the Dynamic integration object
D.parameters.finaltime    = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.relTolRes    = 1.e-8;
D.parameters.relTolPhi    = 1.e-8;
D.parameters.rho          = 0.9;
D.parameters.scaling      = 1e3;
D.parameters.itMax        = 20;
D.runIntegration(); % Run the integration of the object
elapsed = toc;
disp(['   Computation lasted ',num2str(elapsed/60),' min.'])
% Visualization and plots
% To visualize the model:(uncomment the following line)
% Model.Visu
%Plots
endNodePos = Model.listNodes{end}.position;

figure(1)
plot(D.parameters.time,endNodePos(1,:)-nodes(end,2),D.parameters.time,endNodePos(2,:)-nodes(end,3),D.parameters.time,endNodePos(3,:)-nodes(end,4),'Linewidth',1)
grid on; hold on;
title('Tip of the beam position')
xlabel('Time [s]')
ylabel('Poistion [m]')
legend('X','Y','Z')

figure(2)
plot(D.parameters.time,elements{k_Force}.amplitude,'Linewidth',1)
grid on; hold on;
title('Punctual force on tip of the beam')
xlabel('Time [s]')
ylabel('Force [N]')
legend('Force')

%% Direct Differentiation Method
format LONG
disp ('Direct Differentiation Method:' );
ModelDM = FEModelSensi();
ModelDM.CreateFEModelSensi(nodes,elements);
ModelDM.defineBC(BC);
ModelDM.defineCreteria(@Criteria)
D                       = SensitivityDM(ModelDM);  % Creatind the Dynamic integration object
D.parameters.finaltime          = finaltime;
D.parameters.timestepsize       = timestepsize;
D.parameters.relTolRes          = relTolRes;
D.parameters.relTolPhi          = relTolPhi;
D.parameters.scaling            = scaling;
D.parameters.rho                = rhoIP;
D.parameters.itMax              = itMax;
tic;
D.runIntegration(); % Run the integration of the object
elapsedInt = toc;
%Plots
endNodePos = ModelDM.listNodes{end}.position;
figure(1)
plot(D.parameters.time,endNodePos(1,:)-nodes(end,2),D.parameters.time,endNodePos(2,:)-nodes(end,3),D.parameters.time,endNodePos(3,:)-nodes(end,4),'Linewidth',1)
grid on; hold on;
title('Tip of the beam position')
xlabel('Time [s]')
ylabel('Poistion [m]')
legend('X','Y','Z')

figure(2)
plot(D.parameters.time,elements{k_Force}.amplitude,'Linewidth',1)
grid on; hold on;
title('Punctual force on tip of the beam')
xlabel('Time [s]')
ylabel('Force [N]')
legend('Force')


disp ('   Run Direct Differentiation Method:' );
D.sIP.LinConst          = 1;
D.parameters.rho                = rhoIP;   %<<<========= 
tic;
D.runSensi();
elapsedDM = toc;

SensiDM = D.model.sensi;
CriteDM = D.model.crite;
disp(['   Computation lasted Run Integration ',num2str(elapsedInt/60),' min.'])
disp(['   Computation lasted Run DM ',num2str(elapsedDM/60),' min.'])
disp(['   Computation lasted ',num2str((elapsedDM-elapsedInt)/elapsedInt),'%.'])
%% Adjoint Variable Method:
disp ('Adjoint Variable Method:' );
% ModelAV = FEModelSensi();
% ModelAV.CreateFEModelSensi(nodes,elements);
% ModelAV.defineBC(BC);
% ModelAV.defineCreteria(@Criteria)
ModelAV = ModelDM;
A                       = SensitivityAV(ModelAV);  % Creatind the Dynamic integration object
A.parameters.finaltime          = finaltime;
A.parameters.timestepsize       = timestepsize;
A.parameters.relTolRes          = relTolRes;
A.parameters.relTolPhi          = relTolPhi;
D.parameters.scaling            = scaling;
A.parameters.rho                = rhoIP;
A.parameters.itMax              = itMax ;

% tic;
% A.runIntegration(); % Run the integration of the object
% elapsedInt = toc;

disp ('   Run Adjoint Variable Method:' );
A.sIP.LinConst          = 1;
A.parameters.rho                = 1; %<<<========= 
tic;
A.runSensi();
elapsedAV = toc;

format LONGENG
SensiAV = A.model.sensi;
CriteAV = A.model.crite;
disp(['   Computation lasted Run Integration ',num2str(elapsedInt/60),' min.'])
disp(['   Computation lasted Run AV ',num2str(elapsedAV/60),' min.'])
disp(['   Computation lasted ',num2str((elapsedAV-elapsedInt)/elapsedInt),'%.'])
%%
disp(' ');
disp('CriteDM :');  
disp(['   G = [' num2str(CriteDM(1,:)) '];']);
disp(['   F = [' num2str(CriteDM(2,:)) '];']);
disp('CriteAV :'); 
disp(['   G = [' num2str(CriteAV(1,:)) '];']);
disp(['   F = [' num2str(CriteAV(2,:)) '];']); 
disp('SensiDM :'); 
disp(['   G = [' num2str(SensiDM(1,:)) '];']);
disp(['   F = [' num2str(SensiDM(2,:)) '];']);
disp('SensiAV :');  
disp(['   G = [' num2str(SensiAV(1,:)) '];']);
disp(['   F = [' num2str(SensiAV(2,:)) '];']);
disp(' ');
disp('MAxRelativeSensi DM AV ');
disp(['   G = [' num2str( max( abs(SensiDM(1,:)-SensiAV(1,:))./abs(SensiAV(1,:)), abs(SensiDM(1,:)-SensiAV(1,:))./abs(SensiDM(1,:)))) '];']);
disp(['   F = [' num2str( max( abs(SensiDM(2,:)-SensiAV(2,:))./abs(SensiAV(2,:)), abs(SensiDM(2,:)-SensiAV(2,:))./abs(SensiDM(2,:)))) '];']);
%
Ltot = nodes(end,2)-nodes(1,2);
 bar=0:Ltot/(nElem-1):Ltot;
    figure(3); subplot(2,1,1); legend('off')
    plot(bar,SensiAV(1,:),'-*','DisplayName','AV - 3D');hold on;plot(bar,SensiDM(1,:),'-*','DisplayName','DM - 3D');hold on;grid on 
    title('Gradient G');
    %xlabel('5 eleme');
    ylabel('Sensitivity')
    subplot(2,1,2); legend('off')
    plot(bar,SensiAV(2,:),'-*','DisplayName','AV - 3D');hold on;plot(bar,SensiDM(2,:),'-*','DisplayName','DM - 3D');hold on;grid on 
    title('Gradient F');
    %xlabel('5 eleme');
    ylabel('Sensitivity')
%%
function [F,dF,G,dG] = Criteria(model,parameters,timestep)
nf = 2;
if nargin == 0
    F = nf;
    return
end


qSensi = zeros(1,model.nDof);
qSensi(model.listNodes{end}.listDof{3}.numDof) = 1;


dG = cell(nf,1);dG{1}.q = qSensi;
dF = cell(nf,1);dF{2}.q = qSensi;

qCrite = model.listNodes{end}.position(3,timestep);

F  = cell(nf,1); F{2}.q = qCrite;
G  = cell(nf,1); G{1}.q = qCrite;
end
%%
function [F,dF,G,dG] = Criteria2(model,parameters,timestep)
nf = 2;
if nargin == 0
    F = nf;
    return
end

f = zeros(length(model.listNumberNodes));
for i = 1 : size(model.listNumberNodes,2)-1
    f(i:i+1,i:i+1) = f(i:i+1,i:i+1) + [2 1;1 2].*(model.listElements{i}.L)/6;
end

q = zeros(length(model.listNumberNodes),1);
for i = model.listNumberNodes
    q(i) = model.listNodes{i}.position(3,timestep);
end

dFdW   = 2*q'*f;
qSensi = zeros(1,model.nDof);
for i=  model.listNumberNodes
    qSensi(model.listNodes{i}.listDof{3}.numDof) = dFdW(i);
end

dG = cell(nf,1);dG{1}.q = qSensi;
dF = cell(nf,1);dF{2}.q = qSensi;

qCrite = q'*f*q;

F  = cell(nf,1); F{2}.q = qCrite;
G  = cell(nf,1); G{1}.q = qCrite;
end
