%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ModelDM,q,SensiDM,CriteDM] = CantileverBeam3DSensiStaticIntegration_Test3(nElem,Ltot,r1,r2,E, nu,rho,sensi,finaltime,timestepsize) %[ CriteDM,CriteAV,SensiAV,SensiDM]=

%-------------------------------------------------------------------------%
%   Sensitivity calculation of a Cantilever beam 3D 
%
%   Juliano Todesco
%   28/03/2019
%-------------------------------------------------------------------------%
if nargin == 0
%      clear all
%      close all
%      clc
%     
    % Beam parameters
    nElem   = 5;
    Ltot    = 1.0;
    rho     = 7500*ones(nElem,1);    % (kg/m�)
    r1      = 0.009*ones(nElem,1);   % (m) height
    r2      = 0.01*ones(nElem,1);    % (m) thickness (Initial value of design variable)
    E       = 2e+11*ones(nElem,1);   % (Pa) Young's modulus
    nu      = 0.30*ones(nElem,1);    % Poisson's ratio
    sensi   = [0 0 1 0];             % [r1 r2 E rho]    
    %% Definition of time parameters
    finaltime       = 2;
    timestepsize	= 1;
end
relTolRes    	= 1e-7;
relTolPhi    	= 1e-7;
scaling      	= 1e5;
itMax       	= 100;
%% Definition of the nodes of the model (as matrix)
nodes = [1 0 0 0;
         2 Ltot 0 0];
     
grav = [0 0 0*-9.81 ];
%% Definition of the elements of the model (as structure)
% Number of beam elements to build the model
Start = 1;
End = 2;
% create new nodes to have more beam elements
nodes = createInterNodes(nodes,nElem,Start,End);

%%
count = 0;
for k = 1:nElem
    elements{count+k}.type  =  'FlexibleBeamElement';
    elements{count+k}.nodes = [nodes(Start+k-1,1) nodes(Start+k,1)];
    elements{count+k}.yAxis = sensi;
    elements{count+k}.A     = pi*(r2(k)^2-r1(k)^2);
    elements{count+k}.Izz   = (pi/4)*(r2(k)^4 -r1(k)^4);  
    elements{count+k}.Iyy   = (pi/4)*(r2(k)^4 -r1(k)^4);  
    elements{count+k}.Jxx   = (pi/2)*(r2(k)^4 -r1(k)^4); 
    elements{count+k}.E     = E(k);
    elements{count+k}.nu    = nu(k); 
    elements{count+k}.rho   = rho(k);
    elements{count+k}.g     = grav;
    elements{count+k}.listM = 3;
    elements{count+k}.listCt= 3;
    elements{count+k}.listKt= 3; 
    dElementdp.sensi        = [0 0 1 0];% [a b E rho]         
    dElementdp.A            = [-pi*2*r1(k) pi*2*r2(k) 0 0];   
    dElementdp.Izz          = [(pi/4)*(-4*r1(k)^3) (pi/4)*(4*r2(k)^3) 0 0];   
    dElementdp.Iyy          = [(pi/4)*(-4*r1(k)^3) (pi/4)*(4*r2(k)^3) 0 0];    
    dElementdp.Jxx          = [(pi/2)*(-4*r1(k)^3) (pi/2)*(4*r2(k)^3) 0 0];
    dElementdp.E            = [0 0 1 0];     
    dElementdp.nu           = [0 0 0 0]; 
    dElementdp.rho          = [0 0 0 1];
    dElementdp.alpha        = [0 0 0 0];
    dElementdp.beta         = [0 0 0 0];
    dElementdp.step_fd      = [1e-8 1e-8 1e+6 1e+1];
    elements{count+k}.Sensi = dElementdp;
end

k = count+k+1; k_Force = k;
elements{k}.type            = 'ExternalForce';
elements{k}.nodes           = nElem+1;
elements{k}.DOF             = 3;
elements{k}.amplitude       = 100*ones(1,finaltime/timestepsize +1);
% elements{k}.amplitude(1:9)   = [100 200 ];
elements{k}.frequency       = 0;

%% Definition of the boundary conditions
BC = [1]; % Node 1 is fixed
%%
    % %% Definition of the finite element model (FEModel Object)
    % % Based on previously defined nodes and elements
    % Model = FEModelSensi();
    % Model.CreateFEModel(nodes,elements);
    % Model.defineBC(BC);
    % Model.defineCreteria(@Criteria)
    % % Definition of the solver and its parameters
    % tic;disp('Static Integration');
    % D                 = StaticIntegration(Model);  % Creatind the Dynamic integration object
    % D.parameters.finaltime    = finaltime;
    % D.parameters.timestepsize = timestepsize;
    % D.parameters.relTolRes    = relTolRes;
    % D.parameters.relTolPhi    = relTolPhi;
    % % D.parameters.rho          = rho;
    % D.parameters.scaling      = scaling;
    % D.parameters.itMax        = itMax;
    % D.runIntegration(); % Run the integration of the object
    % elapsed = toc;
    % disp(['   Computation lasted ',num2str(elapsed/60),' min.'])
    % % Visualization and plots
    % % To visualize the model:(uncomment the following line)
    % % Model.Visu
    % %Plots
    % endNodePos = Model.listNodes{end}.position;
    % 
    % figure(1)
    % plot(D.parameters.time,endNodePos(1,:)-nodes(end,2),D.parameters.time,endNodePos(2,:)-nodes(end,3),D.parameters.time,endNodePos(3,:)-nodes(end,4),'Linewidth',1)
    % grid on; hold on;
    % title('Tip of the beam position')
    % xlabel('Time [s]')
    % ylabel('Poistion [m]')
    % legend('X','Y','Z')
    % 
    % figure(2)
    % plot(D.parameters.time,elements{k_Force}.amplitude,'Linewidth',1)
    % grid on; hold on;
    % title('Punctual force on tip of the beam')
    % xlabel('Time [s]')
    % ylabel('Force [N]')
    % legend('Force')

%% Direct Differentiation Method
ModelDM = FEModelSensi();
ModelDM.CreateFEModelSensi(nodes,elements);
ModelDM.defineBC(BC);
ModelDM.defineCreteria(@Criteria)
D                       = SensitivityStaticDM(ModelDM);  % Creatind the Dynamic integration object
D.parameters.finaltime          = finaltime;
D.parameters.timestepsize       = timestepsize;
D.parameters.relTolRes          = relTolRes;
D.parameters.relTolPhi          = relTolPhi;
D.parameters.scaling            = scaling;
D.parameters.itMax              = itMax;

disp ('   Run Integration:' );
D.runIntegration(); % Run the integration of the object
%Plots
q = ModelDM.listNodes{end}.position;
if nargin == 0
    figure(1)
    plot(D.parameters.time,q(1,:)-nodes(end,2),D.parameters.time,q(2,:)-nodes(end,3),D.parameters.time,q(3,:)-nodes(end,4),'Linewidth',1)
    grid on; hold on;
    title('Tip of the beam position')
    xlabel('Time [s]')
    ylabel('Poistion [m]')
    legend('X','Y','Z')
    
    figure(2)
    plot(D.parameters.time,elements{k_Force}.amplitude,'Linewidth',1)
    grid on; hold on;
    title('Punctual force on tip of the beam')
    xlabel('Time [s]')
    ylabel('Force [N]')
    legend('Force')
end
disp ('   Run Direct Differentiation Method:' );
D.sIP.LinConst          = 1;
D.runSensi();
SensiDM = D.model.sensi;
CriteDM = D.model.crite;
% %% Adjoint Variable Method:
% disp ('Adjoint Variable Method:' );
% % ModelAV = FEModelSensi();
% % ModelAV.CreateFEModelSensi(nodes,elements);
% % ModelAV.defineBC(BC);
% % ModelAV.defineCreteria(@Criteria)
% ModelAV = ModelDM;
% A                       = SensitivityAV(ModelAV);  % Creatind the Dynamic integration object
% A.parameters.finaltime          = finaltime;
% A.parameters.timestepsize       = timestepsize;
% A.parameters.relTolRes          = relTolRes;
% A.parameters.relTolPhi          = relTolPhi;
% D.parameters.scaling            = scaling;
% A.parameters.rho                = rhoIP;
% A.parameters.itMax              = itMax ;
% 
% % tic;
% % A.runIntegration(); % Run the integration of the object
% % elapsedInt = toc;
% 
% disp ('   Run Adjoint Variable Method:' );
% A.sIP.LinConst          = 1;
% A.parameters.rho                = 1; %<<<========= 
% tic;
% A.runSensi();
% elapsedAV = toc;
% 
% format LONGENG
% SensiAV = A.model.sensi;
% CriteAV = A.model.crite;
% disp(['   Computation lasted Run Integration ',num2str(elapsedInt/60),' min.'])
% disp(['   Computation lasted Run AV ',num2str(elapsedAV/60),' min.'])
% disp(['   Computation lasted ',num2str((elapsedAV-elapsedInt)/elapsedInt),'%.'])
% %%
% disp(' ');
% disp('CriteDM :');  
% disp(['   G = [' num2str(CriteDM(1,:)) '];']);
% disp(['   F = [' num2str(CriteDM(2,:)) '];']);
% disp('CriteAV :'); 
% disp(['   G = [' num2str(CriteAV(1,:)) '];']);
% disp(['   F = [' num2str(CriteAV(2,:)) '];']); 
% disp('SensiDM :'); 
% disp(['   G = [' num2str(SensiDM(1,:)) '];']);
% disp(['   F = [' num2str(SensiDM(2,:)) '];']);
% disp('SensiAV :');  
% disp(['   G = [' num2str(SensiAV(1,:)) '];']);
% disp(['   F = [' num2str(SensiAV(2,:)) '];']);
% disp(' ');
% disp('MAxRelativeSensi DM AV ');
% disp(['   G = [' num2str( max( abs(SensiDM(1,:)-SensiAV(1,:))./abs(SensiAV(1,:)), abs(SensiDM(1,:)-SensiAV(1,:))./abs(SensiDM(1,:)))) '];']);
% disp(['   F = [' num2str( max( abs(SensiDM(2,:)-SensiAV(2,:))./abs(SensiAV(2,:)), abs(SensiDM(2,:)-SensiAV(2,:))./abs(SensiDM(2,:)))) '];']);
% %
% Ltot = nodes(end,2)-nodes(1,2);
%  bar=0:Ltot/(nElem-1):Ltot;
%     figure(3); subplot(2,1,1); legend('off')
%     plot(bar,SensiAV(1,:),'-*','DisplayName','AV - 3D');hold on;plot(bar,SensiDM(1,:),'-*','DisplayName','DM - 3D');hold on;grid on 
%     title('Gradient G');
%     %xlabel('5 eleme');
%     ylabel('Sensitivity')
%     subplot(2,1,2); legend('off')
%     plot(bar,SensiAV(2,:),'-*','DisplayName','AV - 3D');hold on;plot(bar,SensiDM(2,:),'-*','DisplayName','DM - 3D');hold on;grid on 
%     title('Gradient F');
%     %xlabel('5 eleme');
%     ylabel('Sensitivity')
% %%
end

function [F,dF,G,dG] = Criteria(model,parameters,timestep)
nf = 2;
if nargin == 0
    F = nf;
    return
end


qSensi = zeros(1,model.nDof);
qSensi(model.listNodes{end}.listDof{3}.numDof) = 1;

dG = cell(nf,1);dG{1}.q = qSensi;
dF = cell(nf,1);dF{2}.q = qSensi;

qCrite = model.listNodes{end}.position(3,timestep);

F  = cell(nf,1); F{2}.q = qCrite;
G  = cell(nf,1); G{1}.q = qCrite;
end
%%
function [F,dF,G,dG] = Criteria2(model,parameters,timestep)
nf = 2;
if nargin == 0
    F = nf;
    return
end

f = zeros(length(model.listNumberNodes));
for i = 1 : size(model.listNumberNodes,2)-1
    f(i:i+1,i:i+1) = f(i:i+1,i:i+1) + [2 1;1 2].*(model.listElements{i}.L)/6;
end

q = zeros(length(model.listNumberNodes),1);
for i = model.listNumberNodes
    q(i) = model.listNodes{i}.position(3,timestep);
end

dFdW   = 2*q'*f;
qSensi = zeros(1,model.nDof);
for i=  model.listNumberNodes
    qSensi(model.listNodes{i}.listDof{3}.numDof) = dFdW(i);
end

dG = cell(nf,1);dG{1}.q = qSensi;
dF = cell(nf,1);dF{2}.q = qSensi;

qCrite = q'*f*q;

F  = cell(nf,1); F{2}.q = qCrite;
G  = cell(nf,1); G{1}.q = qCrite;
end
