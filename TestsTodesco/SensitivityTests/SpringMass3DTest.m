%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%
%                           Code Matlab Ltas
%      
%   Test: Spring 3D Element 
%
%-------------------------------------------------------------------------%
clear all 
% close all 
% clc 
%
finaltime      = 3;
timestepsize   = 1.e-3;
relTolRes      = 1.e-12;
relTolPhi      = 1.e-12;
scaling        = 1.e+5; 
rho            = 0.9; 
itMax          = 20;
LinConst       = 0;
% V�rifi� avec testanal pour param�tre m avec CI q0, sans force ext
%
L = 1;
alpha = -pi/4;
%%
nodes = [1  0   0  0 ;
         2  L*cos(alpha)   L*sin(alpha) 0;
         3  L*cos(alpha)   L*sin(alpha) 0;
         4  L*cos(alpha)   L*sin(alpha) 0];
%%%
k = 1;
elements{k}.type            ='SpringDamperElement';
elements{k}.nodes           = [1 2];
elements{k}.natural_length  = 1;
elements{k}.stiffness       = 1e+3;  
elements{k}.damping         = 5e+1;
dElementdp.sensi            = [1 1 ];% [stiffness damping natural_length]         
dElementdp.stiffness        = [1 0 ];          
dElementdp.damping          = [0 1 ];          
dElementdp.natural_length   = [0 0 ]; 
dElementdp.step_fd          = [1e+0 1e-2];
elements{k}.Sensi           = dElementdp;

k = k+1;
elements{k}.type            = 'RigidBodyElement';
elements{k}.nodes           = [2];
elements{k}.mass            = 10;
elements{k}.J               = 1;
dElementdp.sensi            = [0 0 ];% [mass J]         
dElementdp.mass             = [1 0 ];          
dElementdp.J                = [0 1 ];   
dElementdp.step_fd          = [1e+1 1e-2];
elements{k}.Sensi           = dElementdp;

k = k+1;
elements{k}.type            = 'ExternalForce';
elements{k}.nodes           = [2];
elements{k}.DOF             = 2;
elements{k}.amplitude       = -9.81*elements{2}.mass;%*sin(pi/4);
elements{k}.frequency       = 0;

k = k+1;
elements{k}.type            = 'KinematicConstraint';
elements{k}.nodes           = [2 3];
elements{k}.A               = [0 0 0 0 0 1]';

k = k+1;
elements{k}.type            = 'KinematicConstraint';
elements{k}.nodes           = [3 4];
elements{k}.A               = [sin(alpha) cos(alpha) 0 0 0 0]';

BC = [1,4];
%%
Model = FEModelSensi();
Model.CreateFEModelSensi(nodes,elements);
Model.defineBC(BC);
Model.defineCreteria(@Criteria)
%%
ModelDM = Model;
G                   = SensitivityDM(Model);
G.parameters.finaltime      = finaltime;
G.parameters.timestepsize   = timestepsize;
G.parameters.relTolRes      = relTolRes;
G.parameters.relTolPhi      = relTolPhi;
G.parameters.scaling        = scaling; 
G.parameters.rho            = rho; 
G.parameters.itMax          = itMax;
G.runIntegration();
G.sIP.LinConst      = LinConst; 
% G.model.Visu()

figure(1);
plot(G.parameters.time, sqrt((Model.listElements{1}.listNodes{2}.position(1,:)-Model.listElements{1}.listNodes{1}.position(1,:)).^2  ...
                    +(Model.listElements{1}.listNodes{2}.position(2,:)-Model.listElements{1}.listNodes{1}.position(2,:)).^2  ...
                    +(Model.listElements{1}.listNodes{2}.position(3,:)-Model.listElements{1}.listNodes{1}.position(3,:)).^2)  );hold on;
 
G.runSensi();
format LONGE
SensiDM = G.model.sensi;
CriteDM = G.model.crite;
%%
ModelAV = Model;
G                   = SensitivityAV(ModelAV);
G.parameters.finaltime      = finaltime;
G.parameters.timestepsize   = timestepsize;
G.parameters.relTolRes      = relTolRes ;
G.parameters.relTolPhi      = relTolPhi;
G.parameters.scaling        = scaling; 
G.parameters.rho            = 1;  %<<<====
G.parameters.itMax          = itMax;
G.sIP.LinConst      = LinConst; 
G.runSensi();
format LONGE
SensiAV = G.model.sensi;
CriteAV = G.model.crite;
%%
disp(' ');
disp('CriteDM :');  
disp(['   G = [' num2str(CriteDM(1,:)) '];']);
disp(['   F = [' num2str(CriteDM(2,:)) '];']);
disp('CriteAV :'); 
disp(['   G = [' num2str(CriteAV(1,:)) '];']);
disp(['   F = [' num2str(CriteAV(2,:)) '];']); 
disp('SensiDM :'); 
disp(['   G = [' num2str(SensiDM(1,:)) '];']);
disp(['   F = [' num2str(SensiDM(2,:)) '];']);
disp('SensiAV :');  
disp(['   G = [' num2str(SensiAV(1,:)) '];']);
disp(['   F = [' num2str(SensiAV(2,:)) '];']);
disp(' ');
disp('MAxRelativeSensi DM AV ');
disp(['   G = [' num2str( max( abs(SensiDM(1,:)-SensiAV(1,:))./abs(SensiAV(1,:)), abs(SensiDM(1,:)-SensiAV(1,:))./abs(SensiDM(1,:)))) '];']);
disp(['   F = [' num2str( max( abs(SensiDM(2,:)-SensiAV(2,:))./abs(SensiAV(2,:)), abs(SensiDM(2,:)-SensiAV(2,:))./abs(SensiDM(2,:)))) '];']);

function [F,dF,G,dG] = Criteria(model,parameters,timestep)
nf = 2;
if nargin == 0
    F = nf;
    return
end

F  = cell(nf,1);
G  = cell(nf,1);
dF = cell(nf,1);
dG = cell(nf,1);

dL = sqrt((model.listElements{1}.listNodes{2}.position(1,timestep)-model.listElements{1}.listNodes{1}.position(1,timestep))^2 ...
         +(model.listElements{1}.listNodes{2}.position(2,timestep)-model.listElements{1}.listNodes{1}.position(2,timestep))^2 ...
         +(model.listElements{1}.listNodes{2}.position(3,timestep)-model.listElements{1}.listNodes{1}.position(3,timestep))^2);
          
qSensi = zeros(1,model.nDof);
qSensi(model.listElements{1}.listNodes{2}.listNumberDof(1:3)) = ...
       model.listElements{1}.listNodes{2}.position(:,timestep)./dL;
    
dF{2}.q = qSensi;
dG{1}.q = qSensi;

qCrite = dL-abs(model.listElements{1}.natural_length);
F{2}.q = qCrite;
G{1}.q = qCrite;
end



% starttime= 10;
% hip = sqrt(Model.listElements{1}.listNodes{2}.position(1,starttime:end).^2 +Model.listElements{1}.listNodes{2}.position(2,starttime:end).^2);
% sinTeta = Model.listElements{1}.listNodes{2}.position(1,starttime:end)./hip;        
% cosTeta = Model.listElements{1}.listNodes{2}.position(2,starttime:end)./hip;
% tanTeta = sinTeta./cosTeta;
% cotanTeta = cosTeta./sinTeta;

% figure(4);
% plot(Model.listElements{1}.listNodes{2}.position(1,:),'--');hold on;grid on; 
% plot(Model.listElements{1}.listNodes{2}.position(2,:),'--');hold on;grid on; 
% plot(Model.listElements{1}.listNodes{2}.position(3,:),'--');hold on;grid on; 
% legend('x','y','z');
% 
% 
% figure(1);
% plot(sinTeta);hold on;grid on;
% plot(cosTeta );
% plot(sqrt(sinTeta.^2+cosTeta.^2));
% legend('sin','cos','sqrt(sin2+cos2)');
% 
% figure(2);
% plot(Model.listElements{1}.listNodes{1}.position(1,:),'--');hold on;grid on; 
% plot(Model.listElements{1}.listNodes{1}.position(2,:),'--');
% legend('x1','y1');
% 
% figure(3);
% plot(Model.listElements{1}.listNodes{2}.position(1,:),'--');hold on;grid on; 
% plot(Model.listElements{1}.listNodes{2}.position(2,:),'--');
% plot(sqrt(Model.listElements{1}.listNodes{2}.position(1,:).^2 +Model.listElements{1}.listNodes{2}.position(2,:).^2)-L,'--');
% legend('x2','y2','dL');
% 
% figure(4);
% plot(abs(elements{3}.amplitude)*ones(1,size(Model.listElements{1}.listNodes{2}.position(1,starttime:end),2)));hold on;grid on;
% plot( elements{1}.stiffness*abs(hip-L).*cosTeta,'--');
% legend('Fel','K*dL*cos(teta)');
% 
% figure(5);
% plot(abs(elements{3}.amplitude)*ones(1,size(Model.listElements{1}.listNodes{2}.position(1,starttime:end),2)));hold on;grid on;
% plot(abs(elements{3}.amplitude)*abs(tanTeta));
% plot(abs(elements{3}.amplitude)./abs(cosTeta));
% plot( elements{1}.stiffness *abs(Model.listElements{1}.listNodes{2}.position(2,starttime:end)) ,'--'); 
% plot( elements{1}.stiffness *abs( hip-L ),'--'); 
% legend('F','Ftan(Teta)','F/cos(Teta)','K*dy','K*dL');
