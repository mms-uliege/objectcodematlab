#!/bin/bash

# $1 = old name
# $2 = new name

grep -rl $1 * | xargs -i@ sed -i 's/'$1/$2'/g' @

