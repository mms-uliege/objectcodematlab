%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Comparaison of multiple serial manipulator with varying cross section
% from a2start to a2end and a ration outer dimension to inner thickness
% with a perturbation (1/100 grav force) at the end effector

clear all
close all

tic
finaltime = 1.5;
timestepsize = 0.01;
t_i = 0.2;
t_f = 1.3;

a1 = 0.05;
b1 = a1;
l1 = 1;
e1 = 0.01; % thiskness of tube 1 (if 0, then not tube but plain beam)
if e1 == 0
    a1In = 0;
    b1In = 0;
else
    a1In = a1-2*e1; % inner length
    b1In = b1-2*e1;
    rapport = a1/e1;
end

rho = 2700;
m1 = (a1*b1-a1In*b1In)*l1*rho;
IxxR1 = m1*(a1^2+b1^2)/12;
IyyR1 = m1*(a1^2+l1^2)/12;
IzzR1 = m1*(b1^2+l1^2)/12;

l2 = l1;
m_end = 0.1; % end-effector mass

% Variation of the cross section value from a2start to a2end

a2start = 0.055;
step = 0.025;
a2end = 0.005;

% rangeEdge = (a2start:-step:a2end);
rangeEdge = [0.1 0.075 0.06 0.05 0.04 0.03 0.02 0.01 0.0075];
% Creating nodes
angle = 45*pi/180;
nodes = [1 0 0 0;
         2 0 0 0;
         3 0 0 0;
         4 l1*cos(angle) 0 l1*sin(angle);
         5 l1*cos(angle) 0 l1*sin(angle);
         6 l1*cos(angle)+l2*cos(angle) 0 0];

nElem1 = 2;
Start1 = 3;
End1 = 4;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

nElem2 = 4;
Start2 = End1+nElem1;
End2 = Start2+1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);

% nodes = doubleNode(nodes,size(nodes,1));

% perturbation of the system (as a grav force on the end-effector)
% pert = [0 0 0.05];.
f = 1; % frequency of the sine perturbation
ampl = 0.3; % amplitude of the sine perturbation
grav = [0 0 -9.81];
pert = [-ampl -ampl ampl];
t = 0:timestepsize:finaltime;
% pert = [zeros(1,length(t)); ampl*sin(2*pi*f.*t); zeros(1,length(t))]';
% pert = [zeros(1,length(t)); zeros(1,length(t));
% awgn(ampl*sin(2*pi*f.*t),25)]'; % with white gaussian noise

%% Starting the loop that varies the parameter
RMSFlex = zeros(1,length(rangeEdge));
RMSRigi = zeros(1,length(rangeEdge));
for nEdge = 1:length(rangeEdge)
    elements = [];
    elementsFlex = [];
    elementsDirDyn = [];
%     DR = [];
%     DD = [];
%     DRD = [];
%     S = [];
    
    a2 = rangeEdge(nEdge);
    b2 = a2;
    if e1 == 0
        e2 = 0;
        a2In = 0;
        b2In = 0;
    else
        e2 = a2/rapport;
        a2In = a2-2*e2;
        b2In = b2-2*e2;
    end
    %% Rigid Model

    m2 = (a2*b2-a2In*b2In)*l2*rho;
    IxxR2 = m2*(a2^2+b2^2)/12;
    IyyR2 = m2*(a2^2+l2^2)/12;
    IzzR2 = m2*(b2^2+l2^2)/12;

    count = 1;
    elements{count}.type = 'RigidBodyElement';
    elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
    elements{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
    elements{count}.J = diag([IxxR1 IyyR1 IzzR1]);
    elements{count}.g = grav;
    count = count +1;

    elements{count}.type = 'RigidBodyElement';
    elements{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
    elements{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
    elements{count}.J = diag([IxxR2 IyyR2 IzzR2]);
    elements{count}.g = grav;
    count = count +1;

    count = size(elements,2)+1;
    elements{count}.type = 'RigidBodyElement';
    elements{count}.nodes = [nodes(end,1)];
    elements{count}.mass = m_end; % 0.07
    elements{count}.g = grav;

%     count = size(elements,2)+1;
%     elements{count}.type = 'KinematicConstraint';
%     elements{count}.nodes = [nodes(end,1) nodes(end,1)-1];

    count = size(elements,2)+1;
    elements{count}.type = 'KinematicConstraint';
    elements{count}.nodes = [1 2];
    elements{count}.A = [0 0 0 0 0 1]';

    count = size(elements,2)+1;
    elements{count}.type = 'KinematicConstraint';
    elements{count}.nodes = [2 3];
    elements{count}.A = [0 0 0 0 1 0]';

    count = size(elements,2)+1;
    elements{count}.type = 'KinematicConstraint';
    elements{count}.nodes = [Start2-1 Start2];
    elements{count}.A = [0 0 0 0 1 0]';
    count = count+1;

    % Trajectory

    y_end = l2;
    z_end = nodes(end,4);
    r = y_end/2;

    timeVector = 0:timestepsize:finaltime;
    trajx = nodes(end,2)*ones(size(timeVector));
    trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
    trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

    elements{count}.type = 'TrajectoryConstraint';
    elements{count}.nodes = [nodes(end,1)];
    elements{count}.T = [trajx;...
                         trajy;...
                         trajz];
    elements{count}.Axe = [1 0 0;...
                           0 1 0;...
                           0 0 1];
    elements{count}.elements = [count-3 count-2 count-1];
    elements{count}.active = 1;

    % Boundary Condition
    BC = [1];

    % Solving
    ModelR = FEModel();
    ModelR.CreateFEModel(nodes,elements);
    ModelR.defineBC(BC);

    DR = DynamicIntegration(ModelR);
    DR.parameters.finaltime = finaltime;
    DR.parameters.timestepsize = timestepsize;
    DR.parameters.rho = 0.01;
    % DR.parameters.relTolRes = 1e-10;
    DR.parameters.scaling = 1e6;
    DR.runIntegration();

    uRigi.u1 = ModelR.listElementVariables{end}.value(1,:);
    uRigi.u2 = ModelR.listElementVariables{end}.value(2,:);
    uRigi.u3 = ModelR.listElementVariables{end}.value(3,:);
    uRigi.time = DR.parameters.time;

    %% Flexible Model

    E = 70e9; nu = 0.3; G = E/(2*(1+nu));
    A2 = a2*b2-a2In*b2In; Ixx2 = (a2*b2*(a2^2+b2^2)-a2In*b2In*(a2In^2+b2In^2))/12; Iyy2 = (b2*a2^3-b2In*a2In^3)/12;Izz2 = (a2*b2^3-a2In*b2In^3)/12;

    KCS2 = diag([E*A2 G*A2 G*A2 G*Ixx2 E*Iyy2 E*Izz2]);
    MCS2 = diag(rho*[A2 A2 A2 Ixx2 Iyy2 Izz2]);

    type = 'FlexibleBeamElement';
    count = 1;
    elementsFlex{count}.type = 'RigidBodyElement';   % first link (rigid)
    elementsFlex{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
    elementsFlex{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
    elementsFlex{count}.J = diag([IxxR1 IyyR1 IzzR1]);
    elementsFlex{count}.g = grav;
    
    count = size(elementsFlex,2); % Second link (flexible)
    for i = 1:nElem2
        elementsFlex{count+i}.type = type;
        elementsFlex{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
        elementsFlex{count+i}.KCS = KCS2;
        elementsFlex{count+i}.MCS = MCS2;
        elementsFlex{count+i}.g = grav;
    end

    count = size(elementsFlex,2)+1;
    elementsFlex{count}.type = 'RigidBodyElement';
    elementsFlex{count}.nodes = [nodes(end,1)];
    elementsFlex{count}.mass = m_end; % 0.07
    elementsFlex{count}.g = grav;

%     count = size(elementsFlex,2)+1;
%     elementsFlex{count}.type = 'KinematicConstraint';
%     elementsFlex{count}.nodes = [nodes(end,1) nodes(end,1)-1];

    count = size(elementsFlex,2)+1;
    elementsFlex{count}.type = 'KinematicConstraint';
    elementsFlex{count}.nodes = [1 2];
    elementsFlex{count}.A = [0 0 0 0 0 1]';

    count = size(elementsFlex,2)+1;
    elementsFlex{count}.type = 'KinematicConstraint';
    elementsFlex{count}.nodes = [2 3];
    elementsFlex{count}.A = [0 0 0 0 1 0]';

    count = size(elementsFlex,2)+1;
    elementsFlex{count}.type = 'KinematicConstraint';
    elementsFlex{count}.nodes = [Start2-1 Start2];
    elementsFlex{count}.A = [0 0 0 0 1 0]';
    count = count+1;

    % Trajectory

    npts = finaltime/timestepsize + 1;

    timeVector = 0:finaltime/(npts-1):finaltime;
    trajx = nodes(end,2)*ones(size(timeVector));
    trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
    trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

    elementsFlex{count}.type = 'TrajectoryConstraint';
    elementsFlex{count}.nodes = [nodes(end,1)];
    elementsFlex{count}.T = [trajx;...
                             trajy;...
                             trajz];
    elementsFlex{count}.Axe = [1 0 0;...
                               0 1 0;...
                               0 0 1];
    elementsFlex{count}.elements = [count-3 count-2 count-1];
    elementsFlex{count}.active = 1;

    % Solving

    ModelF = FEModel();
    ModelF.CreateFEModel(nodes,elementsFlex);
    ModelF.defineBC(BC);

    % Copying ElementVariables and Node variables from rigid solution

    for n = ModelF.listNumberNodes
        ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
        ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
        ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
        ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
        ModelF.listNodes{n}.InitializeD_Opti();
    end
    skip = 0;
    for n = ModelF.listNumberElementVariables
        if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
    %         ModelF.listElementVariables{n}.A = ModelR.listElementVariables{n}.A;
    %         ModelF.listElementVariables{n}.nDof = ModelR.listElementVariables{n}.nDof;
            ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
            ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
    %         ModelF.listElementVariables{n}.xI0 = ModelR.listElementVariables{n}.xI0;
            ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
            ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
            ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
        else
            while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
                skip = skip+1;
            end
            ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
        end
        ModelF.listElementVariables{n}.InitializeD_Opti();
    end
    % First part optimization to compute the equilibrium solution for the first
    % preactuation phase. Then copy this preactuation phase, to have full initial guess equilibrium to compute full
    % optimization.
 
%     EndPreActStep = t_i/(finaltime/(npts-1))+1;
    EndPreTime = t_i*0.75;
    EndPreActStep = round(EndPreTime/(finaltime/(npts-1))+1);
    S = DirectTranscriptionOpti(ModelF);
    S.parameters.rho = 0.01;
    S.npts = EndPreActStep+1;
    S.parameters.finaltime = EndPreTime;
    S.parameters.timestepsize = finaltime/(npts-1);
    S.parameters.scaling = 1e6;
    S.linConst = false;
    xSol = S.runOpti(DR);   
    
    % Copy the state at the end of preactuation phase to start as new initial
    % guess.
    % Redefine the trajectory that was cut to only the preactuation phase
    ModelF.listElements{end}.T = [trajx;...
                                  trajy;...
                                  trajz];

    for n = ModelF.listNumberNodes
        for step = 1:EndPreActStep-1
            ModelF.listNodes{n}.R_InitOpti(:,step) = ModelF.listNodes{n}.R(:,EndPreActStep);
            ModelF.listNodes{n}.position_InitOpti(:,step) = ModelF.listNodes{n}.position(:,EndPreActStep);
            ModelF.listNodes{n}.velocity_InitOpti(:,step) = ModelF.listNodes{n}.velocity(:,EndPreActStep);
            ModelF.listNodes{n}.acceleration_InitOpti(:,step) = ModelF.listNodes{n}.acceleration(:,EndPreActStep);
        end
    end
    for n = ModelF.listNumberElementVariables
        if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
            for step = 1:EndPreActStep-1
                ModelF.listElementVariables{n}.R_InitOpti(:,step) = ModelF.listElementVariables{n}.R(:,EndPreActStep);
                ModelF.listElementVariables{n}.x_InitOpti(:,step) = ModelF.listElementVariables{n}.x(:,EndPreActStep);
                ModelF.listElementVariables{n}.velocity_InitOpti(:,step) = ModelF.listElementVariables{n}.velocity(:,EndPreActStep);
                ModelF.listElementVariables{n}.acceleration_InitOpti(:,step) = ModelF.listElementVariables{n}.acceleration(:,EndPreActStep);
                ModelF.listElementVariables{n}.relCoo_InitOpti(:,step) = ModelF.listElementVariables{n}.relCoo(:,EndPreActStep);
            end
        else
            for step = 1:EndPreActStep-1
                ModelF.listElementVariables{n}.value_InitOpti(:,step) = ModelF.listElementVariables{n}.value(:,EndPreActStep); 
            end
        end
    end

    S = DirectTranscriptionOpti(ModelF);
    S.parameters.rho = 0.01;
    S.npts = npts;
    S.parameters.finaltime = finaltime;
    S.parameters.timestepsize = finaltime/(S.npts-1);
    % S.NodeToMinimize = [Start2+[1:nElem2-1]];
    % S.JointToMinimize = []; 
    S.parameters.scaling = 1e6;
    S.linConst = false;
    xSol = S.runOpti(DR);

    timeSteps = S.timeValues;
    timeLoc = S.timesteps;
    uBeam.u1 = ModelF.listElementVariables{end}.value(1,timeLoc);
    uBeam.u2 = ModelF.listElementVariables{end}.value(2,timeLoc);
    uBeam.u3 = ModelF.listElementVariables{end}.value(3,timeLoc);
    uBeam.time = S.timeValues;

    %% Direct dynamic with both solutions
    
    count = 1;
    elementsDirDyn{count}.type = 'RigidBodyElement';
    elementsDirDyn{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
    elementsDirDyn{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
    elementsDirDyn{count}.J = diag([IxxR1 IyyR1 IzzR1]);
    elementsDirDyn{count}.g = grav;

    type = 'FlexibleBeamElement';
    count = size(elementsDirDyn,2);
    for i = 1:nElem2
        elementsDirDyn{count+i}.type = type;
        elementsDirDyn{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
        elementsDirDyn{count+i}.KCS = KCS2;
        elementsDirDyn{count+i}.MCS = MCS2;
        elementsDirDyn{count+i}.g = grav;
    end

    count = size(elementsDirDyn,2)+1;
    elementsDirDyn{count}.type = 'RigidBodyElement';
    elementsDirDyn{count}.nodes = [nodes(end,1)];
    elementsDirDyn{count}.mass = m_end; % 0.07
    elementsDirDyn{count}.g = grav + pert;

%     count = size(elementsDirDyn,2)+1;
%     elementsDirDyn{count}.type = 'KinematicConstraint';
%     elementsDirDyn{count}.nodes = [nodes(end,1) nodes(end,1)-1];

    count = size(elementsDirDyn,2)+1;
    elementsDirDyn{count}.type = 'KinematicConstraint';
    elementsDirDyn{count}.nodes = [1 2];
    elementsDirDyn{count}.A = [0 0 0 0 0 1]';

    count = size(elementsDirDyn,2)+1;
    elementsDirDyn{count}.type = 'KinematicConstraint';
    elementsDirDyn{count}.nodes = [2 3];
    elementsDirDyn{count}.A = [0 0 0 0 1 0]';
    count = count+1;

    elementsDirDyn{count}.type = 'KinematicConstraint';
    elementsDirDyn{count}.nodes = [Start2-1 Start2];
    elementsDirDyn{count}.A = [0 0 0 0 1 0]';
    count = count+1;
    
    timeVector = 0:timestepsize:finaltime;
    trajx = nodes(end,2)*ones(size(timeVector));
    trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
    trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

    u1_init = interp1(uRigi.time,uRigi.u1,timeVector,'linear');
    u2_init = interp1(uRigi.time,uRigi.u2,timeVector,'linear');
    u3_init = interp1(uRigi.time,uRigi.u3,timeVector,'linear');

    u1 = interp1(uBeam.time,uBeam.u1,timeVector,'linear');
    u2 = interp1(uBeam.time,uBeam.u2,timeVector,'linear');
    u3 = interp1(uBeam.time,uBeam.u3,timeVector,'linear');
     % Trick to make result better (seems like the big, uneven jump makes the simulation bad.
    u1(1:5) = u1(12)*ones(1,5);
    u2(1:5) = u2(12)*ones(1,5);
    u3(1:5) = u3(12)*ones(1,5);

    elementsDirDyn{count}.type = 'ForceInKinematicConstraint';
    elementsDirDyn{count}.elements = [count-3];
    elementsDirDyn{count}.f = u1;
    count = count+1;

    elementsDirDyn{count}.type = 'ForceInKinematicConstraint';
    elementsDirDyn{count}.elements = [count-3];
    elementsDirDyn{count}.f = u2;
    count = count+1;

    elementsDirDyn{count}.type = 'ForceInKinematicConstraint';
    elementsDirDyn{count}.elements = [count-3];
    elementsDirDyn{count}.f = u3;
    
    % Boundary Condition
    BC = [1];

    % Model Def
    ModelD = FEModel();% Flex model
    ModelD.CreateFEModel(nodes,elementsDirDyn);
    ModelD.defineBC(BC);

    ModelRD = FEModel();% Rigid model
    ModelRD.CreateFEModel(nodes,elementsDirDyn);
    ModelRD.defineBC(BC);

    % Solving Flexible
    DD = DynamicIntegration(ModelD);
    DD.parameters.finaltime = finaltime;
    DD.parameters.timestepsize = timestepsize;
    DD.parameters.rho = 0.01;
    % D.parameters.relTolRes = 1e-10; 
    DD.parameters.scaling = 1e6;
    DD.runIntegration();

    % Solving Rigid
    ModelRD.listElements{count-2}.f = u1_init;
    ModelRD.listElements{count-1}.f = u2_init;
    ModelRD.listElements{count}.f = u3_init;

    DRD = DynamicIntegration(ModelRD);
    DRD.parameters.finaltime = finaltime;
    DRD.parameters.timestepsize = timestepsize;
    DRD.parameters.rho = 0.01;
    % DR.parameters.relTolRes = 1e-10;
    DRD.parameters.scaling = 1e6;
    DRD.runIntegration();
    
    x = ModelD.listNodes{end}.position(1,:);
    y = ModelD.listNodes{end}.position(2,:);
    z = ModelD.listNodes{end}.position(3,:);

    xR = ModelRD.listNodes{end}.position(1,:);
    yR = ModelRD.listNodes{end}.position(2,:);
    zR = ModelRD.listNodes{end}.position(3,:);
    
    RelError = zeros(size(timeVector));
    RelErrorR = zeros(size(timeVector));
    for ij = 1:length(RelError)
        RelError(ij) = 100*(norm([trajx(ij)-x(ij) trajy(ij)-y(ij) trajz(ij)-z(ij)])/norm([trajx(ij) trajy(ij) trajz(ij)]));
        RelErrorR(ij) = 100*(norm([trajx(ij)-xR(ij) trajy(ij)-yR(ij) trajz(ij)-zR(ij)])/norm([trajx(ij) trajy(ij) trajz(ij)]));
    end

    RMSFlex(nEdge) = sqrt(mean(RelError.^2));
    RMSRigi(nEdge) = sqrt(mean(RelErrorR.^2));
    
end
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])
%% Plot

figure
plot(rangeEdge(1,:),RMSFlex(1,:),rangeEdge(1,:),RMSRigi(1,:),'Linewidth',2)
xlabel('a_2','Fontsize',16)
ylabel('RMS relative error','Fontsize',16)
legend('With u','With u_{rigid}','Location','Best')
grid on
% title('Impact of edge length on RMS error','Fontsize',13)

