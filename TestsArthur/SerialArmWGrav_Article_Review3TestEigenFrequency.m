%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test inverse dynamics of a serial manipulator (2 links, 3 dof).
% considering gravity, with a initial guess that is the deformed state.
% Initial guess computed staticaly and nor rigid model to start with
% With possibility to add some PD feedback on joint position level

% Model used for Multibody system Dynamics journal with some tests to
% improve direct dynamics result

clear all
% close all

computeOpti = true;
computeDirectDyn = true;
% computeDirectDyn = false;

finaltime = 1;
timestepsize = 0.01;

hypForPreComp = 'notConstant';
hypForComp = 'notConstant';
% hypForComp = 'constant';

a1 = 0.05;
b1 = a1;
l1 = 1;
l2 = l1; 
e1 = 0.01;
rapport = a1/e1;
a2 = 0.0075;
b2 = a2;
e2 = a2/rapport;
a2In = a2-2*e2;
b2In = b2-2*e2;
a1In = a1-2*e1; % inner length
b1In = b1-2*e1;

alpha = 0*0.0001; % Damping proportionnal to mass
beta = 0*0.01; % Damping proportionnal to stiffness
rho_num = 0.9;
scaling = 1e6;
tol = 1e-6;

nPass = 0;

% grav = [0 0 -9.81];
grav = [0 0 0];
time = 0:timestepsize:finaltime;
% grav = [zeros(1,length(t)); zeros(1,length(t)); [0 0 0 -9.81*ones(1,length(t)-3)]]';

m_end = 0.1; % end-effector mass

%% Creating nodes
angle = 45*pi/180;
nodes = [1 0 0 0;
         2 0 0 0;
         3 0 0 0;
         4 l1*cos(angle) 0 l1*sin(angle);
         5 l1*cos(angle) 0 l1*sin(angle);
         6 l1*cos(angle)+l2*cos(angle) 0 0];
     
nElem1 = 2;
Start1 = 3;
End1 = 4;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

nElem2 = 10;
Start2 = End1+nElem1;
End2 = Start2+1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);
 
%% Rigid Model

rho = 2700;
m1 = (a1*b1-a1In*b1In)*l1*rho;
% IxxR1 = m1*(a1^2+b1^2)/12;
IxxR1 = m1*(a1^2+b1^2-a1In^2-b1In^2)/12;
IyyR1 = m1*(a1^2+l1^2)/12;
IzzR1 = m1*(b1^2+l1^2)/12;

m2 = (a2*b2-a2In*b2In)*l2*rho;
IxxR2 = m2*(a2^2+b2^2-a2In^2-b2In^2)/12;
IyyR2 = m2*(a2^2+l2^2)/12;
IzzR2 = m2*(b2^2+l2^2)/12;

E = 70e9; nu = 0.3; G = E/(2*(1+nu));
% if computeOpti ~= true
A2 = a2*b2-a2In*b2In; Ixx2 = (a2*b2*(a2^2+b2^2)-a2In*b2In*(a2In^2+b2In^2))/12; Iyy2 = (b2*a2^3-b2In*a2In^3)/12;Izz2 = (a2*b2^3-a2In*b2In^3)/12;

KCS2 = diag([E*A2 G*A2 G*A2 G*Ixx2 E*Iyy2 E*Izz2]);
MCS2 = diag(rho*[A2 A2 A2 Ixx2 Iyy2 Izz2]);

for mode = 1:10
    ks = ((mode-0.5)*pi)^2;
    I = Iyy2;
    freq_theo(mode) = ks*(1/l2^2)*sqrt(E*I/rho)/(2*pi);
end

count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elements{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR1 IyyR1 IzzR1]);
elements{count}.g = grav;

type = 'FlexibleBeamElement';
count = size(elements,2);
for i = 1:nElem2
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
    elements{count+i}.KCS = KCS2;
    elements{count+i}.MCS = MCS2;
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;
    elements{count+i}.g = grav;
end

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = m_end;
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = 0*[0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.A = 0*[0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.A = 0*[0 0 0 0 1 0]';
count = count+1;

s = round(finaltime*0.05/timestepsize);
e = round(s+10);
A = 5;
f = zeros(size(time));
f(s:e) = A*ones(1,e-s+1);
elements{count}.type = 'punctualForce';
elements{count}.nodes = [nodes(end,1)];
elements{count}.f = f;
elements{count}.DOF = [3];

% Boundary Condition
BC = [1];

% Solving
Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
D.parameters.relTolRes = tol;
D.parameters.scaling = scaling;
D.runIntegration();

uBeam.u1 = Model.listElementVariables{end}.value(1,:);
uBeam.u2 = Model.listElementVariables{end}.value(2,:);
uBeam.u3 = Model.listElementVariables{end}.value(3,:);
uBeam.jointPos1 = Model.listElementVariables{1}.relCoo;
uBeam.jointPos2 = Model.listElementVariables{2}.relCoo;
uBeam.jointPos3 = Model.listElementVariables{3}.relCoo;
uBeam.jointVel1 = Model.listElementVariables{1}.velocity;
uBeam.jointVel2 = Model.listElementVariables{2}.velocity;
uBeam.jointVel3 = Model.listElementVariables{3}.velocity;
uBeam.time = time;


%% Plots

co = [    0    0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840];
set(groot,'defaultAxesColorOrder',co)

endNode = Model.listNodes{end};

figure
subplot(311)
plot(time,endNode.position(1,:),'Linewidth',2)
ylabel('x [m]','Fontsize',16)
grid on
subplot(312)
plot(time,endNode.position(2,:),'Linewidth',2)
ylabel('y [m]','Fontsize',16)
grid on
subplot(313)
plot(time,endNode.position(3,:),'Linewidth',2)
title('Tip position')
xlabel('Time [s]','Fontsize',16)
ylabel('z [m]','Fontsize',16)
grid on

analysis = freqAnalysis(D,1,0);
analysis.sNatFreq(1:10)
analysis.usNatFreq(1:10)
