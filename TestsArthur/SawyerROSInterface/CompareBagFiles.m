%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script to read bag file containing commands sent to Sawyer and compare them. This script
% will read the data and plot them in parallel to the computed ones.

close all

loadedBag = rosbag('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\sentCommands.bag');
sentData = readMessages(loadedBag);

nMess = loadedBag.NumMessages;
StartCounter = loadedBag.StartTime;
EndCounter = loadedBag.EndTime;
[time,col] = timeseries(loadedBag);

disp(['Total sending time is ' num2str(EndCounter-StartCounter) ' sec']);

values2Plot.Position = [];
values2Plot.Velocity = [];
values2Plot.Effort = [];
values2Plot.time = zeros(nMess,1);

isPos = false;
isVel = false;
isTorque = false;
if ~isempty(sentData{1}.Position)
    isPos = true;
end
if ~isempty(sentData{1}.Velocity)
    isVel = true;
end
if ~isempty(sentData{1}.Effort)
    isTorque = true;
end

for i = 1:nMess
    values2Plot.time(i) = time.Time(i)-time.Time(1);
    if isPos
        values2Plot.Position(i,:) = sentData{i}.Position;
        posFile = 'TestPoseCircleFlexJoint2.txt';
%         posFile = 'TestPoseCircle.txt';
    end
    if isVel
        values2Plot.Velocity(i,:) = sentData{i}.Velocity;
        velFile = 'TestVelocityCircleFlexJoint2.txt';
    end
    if isTorque
        values2Plot.Effort(i,:) = sentData{i}.Effort;
        torqueFile = 'TestTorqueCircleFlexJoint2.txt';
%         torqueFile = 'TestTorqueCircle.txt';
    end
end

if isPos
    computedPos = fopen(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\' posFile]);
    posContent = textscan(computedPos,'%n %n %n %n','HeaderLines',1,'Delimiter',' ');
    fclose(computedPos);
end
if isVel
    computedVel = fopen(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\' velFile]);
    velContent = textscan(computedVel,'%n %n %n %n','HeaderLines',1,'Delimiter',' ');
    fclose(computedVel);
end

if isTorque
    computedTorque = fopen(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\' torqueFile]);
    torqueContent = textscan(computedTorque,'%n %n %n %n','HeaderLines',1,'Delimiter',' ');
    fclose(computedTorque);
end

field = fieldnames(values2Plot);
nField = length(field);
for i = 1:nField-1
    leg = {};
    if ~isempty(values2Plot.(field{i})) && ~strcmp(field{i},'time')
        figure
        nVal = size(values2Plot.(field{i}),2);
        for j = 1:nVal
            hold on
            plot(values2Plot.time,values2Plot.(field{i})(:,j))
        end
        if strcmp(field{i},'Position')
            time = posContent{1};
            u1 = posContent{2};
            u2 = posContent{3};
            u3 = posContent{4};
        elseif strcmp(field{i},'Velocity')
            time = velContent{1};
            u1 = velContent{2};
            u2 = velContent{3};
            u3 = velContent{4};
        elseif strcmp(field{i},'Effort')
            time = torqueContent{1};
            u1 = torqueContent{2};
            u2 = torqueContent{3};
            u3 = torqueContent{4};
        end
        plot(time,u1,time,-u2,time,-u3)
        leg = num2cell(strrep(num2str(1:nVal),' ',''));
        leg = [leg {'u1' 'u2' 'u3'}];
        legend(leg, 'Location', 'Best')
        title([field{i} ' of Sawyer robot'])
        xlabel('Time (s)')
        ylabel([field{i}])
        hold off
    end
end


