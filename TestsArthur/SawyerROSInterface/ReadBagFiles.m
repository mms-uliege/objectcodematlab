%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script to read bag file containing commands sent to Sawyer. This script
% will read the data and plot them
close all

loadedBag = rosbag('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\sentCommands.bag');
sentData = readMessages(loadedBag);

nMess = loadedBag.NumMessages;
StartCounter = loadedBag.StartTime;
EndCounter = loadedBag.EndTime;
[time,col] = timeseries(loadedBag);

disp(['Total sending time is ' num2str(EndCounter-StartCounter) ' sec']);

values2Plot.Position = [];
values2Plot.Velocity = [];
values2Plot.Effort = [];
values2Plot.time = zeros(nMess,1);

for i = 1:nMess
    values2Plot.time(i) = time.Time(i)-time.Time(1);
    if ~isempty(sentData{1}.Position)
        values2Plot.Position(i,:) = sentData{i}.Position;
    elseif ~isempty(sentData{1}.Position)
        values2Plot.Velocity(i,:) = sentData{i}.Velocity;
    elseif ~isempty(sentData{1}.Effort)
        values2Plot.Effort(i,:) = sentData{i}.Effort;
    else
        disp('nothing to plot... ')
        break
    end
end

field = fieldnames(values2Plot);
nField = length(field);
for i = 1:nField-1
    if ~isempty(values2Plot.(field{i})) && ~strcmp(field{i},'time')
        figure
        nVal = size(values2Plot.(field{i}),2);
        for j = 1:nVal
            hold on
            plot(values2Plot.time,values2Plot.(field{i})(:,j))
        end
        legend(num2cell(strrep(num2str(1:nVal),' ','')))
        title([field{i} ' of Sawyer robot'])
        xlabel('Time (s)')
        ylabel([field{i}])
        hold off
    end
end