%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test to see if the analytical gradients are the same as those computed
% using the finite difference
clear

finaltime = 1;
timestepsize = 0.01;
t_i = 0.1;
t_f = finaltime-t_i;

computeOpti = true;
computeDirectDyn = true;
hypForComp = 'constant';
% hypForComp = 'notConstant';
scaling = 1e6;

a1 = 0.05;
b1 = a1;
l1 = 1;
l2 = l1;
e1 = 0.01;
a1In = a1-2*e1; % inner length
b1In = b1-2*e1;

alpha = 0*0.0001; % damping proportional to mass
beta = 0*0.01; % damping proportional to stiffness

grav = [0 0 -9.81];
% grav = [0 0 0];

nPass = 0;

m_end = 0.1; % end-effector mass

%% Creating nodes
nodes = [1 0 0 0;
         2 0 0 0;
         3 l1 0 0;];
     
nElem1 = 4;
Start1 = 2;
End1 = 3;
nodes = createInterNodes(nodes,nElem1,Start1,End1);
 
%% Rigid Model

rho = 2700;
m1 = (a1*b1-a1In*b1In)*l1*rho;
% IxxR1 = m1*(a1^2+b1^2)/12;
IxxR1 = m1*(a1^2+b1^2-a1In^2-b1In^2)/12;
IyyR1 = m1*(a1^2+l1^2)/12;
IzzR1 = m1*(b1^2+l1^2)/12;

count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elements{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR1 IyyR1 IzzR1]);
elements{count}.g = grav;

E = 70e9; nu = 0.3; G = E/(2*(1+nu));
% E = 70e9; nu = 0.3; G = E/(2*(1+nu));

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = m_end; % 0.07
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 1 0 0 0]';

% Trajectory
z_end = l1;
r=2;

timeVector = 0:timestepsize:finaltime;
trajx = nodes(end,2)*ones(size(timeVector));
trajy = nodes(end,3)*ones(size(timeVector));
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'lin',t_i,t_f);
count = size(elements,2)+1;
elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajz];
elements{count}.Axe = [0 0 1];
elements{count}.elements = [count-1];
elements{count}.active = 1;

% count = size(elements,2)+1;
% elements{count}.type = 'TrajectoryConstraintJoint';
% elements{count}.elements = count-1;
% elements{count}.T = trajz;

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.1;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = scaling;
D.runIntegration();

%% Flexible Model

% A2 = a2*b2; Ixx2 = a2*b2*(a2^2+b2^2)/12; Iyy2 = b2*a2^3/12;Izz2 = a2*b2^3/12;
A1 = a1*b1-a1In*b1In; Ixx1 = (a1*b1*(a1^2+b1^2)-a1In*b1In*(a1In^2+b1In^2))/12; Iyy1 = (b1*a1^3-b1In*a1In^3)/12;Izz1 = (a1*b1^3-a1In*b1In^3)/12;

KCS1 = diag([E*A1 G*A1 G*A1 G*Ixx1 E*Iyy1 E*Izz1]);
MCS1 = diag(rho*[A1 A1 A1 Ixx1 Iyy1 Izz1]);

type = 'FlexibleBeamElement';
count = 0; % Second link (flexible)
for i = 1:nElem1
    elementsFlex{count+i}.type = type;
    elementsFlex{count+i}.nodes = [nodes(Start1+i-1,1) nodes(Start1+i,1)];
    elementsFlex{count+i}.KCS = KCS1;
    elementsFlex{count+i}.MCS = MCS1;
    elementsFlex{count+i}.alpha = alpha;
    elementsFlex{count+i}.beta = beta;
    elementsFlex{count+i}.g = grav;
end
% count = 1;
% elementsFlex{count}.type = 'RigidBodyElement';
% elementsFlex{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
% elementsFlex{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
% elementsFlex{count}.J = diag([IxxR1 IyyR1 IzzR1]);
% elementsFlex{count}.g = grav;

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'RigidBodyElement';
elementsFlex{count}.nodes = [nodes(end,1)];
elementsFlex{count}.mass = m_end; % 0.07
elementsFlex{count}.g = grav;

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [1 2];
elementsFlex{count}.A = [0 0 1 0 0 0]';

% Trajectory

npts = finaltime/timestepsize + 1;

timeVector = 0:finaltime/(npts-1):finaltime;
trajx = nodes(end,2)*ones(size(timeVector));
trajy = nodes(end,3)*ones(size(timeVector));
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'lin',t_i,t_f);

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'TrajectoryConstraint';
elementsFlex{count}.nodes = [nodes(end,1)];
elementsFlex{count}.T = [trajz];
elementsFlex{count}.Axe = [0 0 1];
elementsFlex{count}.elements = [count-1];
elementsFlex{count}.active = 1;

% count = size(elementsFlex,2)+1;
% elements{count}.type = 'TrajectoryConstraintJoint';
% elements{count}.elements = count-1;
% elements{count}.T = trajz;

% Solving

ModelF = FEModel();
ModelF.CreateFEModel(nodes,elementsFlex);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from rigid solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = 0.1;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.parameters.scaling = scaling;
S.linConst = false;
S.nPass = nPass;

if computeOpti == true
    S.ConstIter = hypForComp;
    xsol = S.runOpti(D);
    
    figure
    plot(S.parameters.time, ModelF.listElementVariables{end}.value(1,:), D.parameters.time, ModelR.listElementVariables{end}.value(1,:))
    xlabel('Time (s)')
    ylabel('Command (N)')
    legend('u_{flex}','u_{rigi}')
    
    figure
    plot(S.parameters.time, ModelF.listNodes{end}.position(3,:), D.parameters.time, ModelR.listNodes{end}.position(3,:), timeVector, trajz)
    xlabel('Time (s)')
    ylabel('Position (m)')
    legend('u_{flex}','u_{rigi}','presc')
else
    S.parameters.InitialComputation();

    for i = S.model.listNumberDof
        S.model.listDof{i}.InitializeD(D.parameters.nstep);
    end
    for i = S.model.listNumberElementVariables
        S.model.listElementVariables{i}.InitializeD(D.parameters.nstep);
    end
    for i = S.model.listNumberElements
        S.model.listElements{i}.InitializeD(D.parameters.nstep);
    end

    % Selection of the discrete time steps according to the number of npts
    S.timeValues = zeros(1,S.npts);
    S.timesteps = zeros(1,S.npts);
    for k = 1:S.npts
        S.timeValues(k) = (k-1)*S.parameters.finaltime/(S.npts-1);
        S.timesteps(k) = find(D.parameters.time>=S.timeValues(k),1);
        if k~= 1 && abs(S.timeValues(k)-D.parameters.time(S.timesteps(k)-1))< 1e-6
            S.timesteps(k) = S.timesteps(k)-1;
        end
    end

    S.LocMotionDof = S.model.listFreeDof(1:S.nCoord);

    % Saves the constant (over time and iteration) mass matrix (not like K,C
    % that are can be constant over iter but not along time).
    M = S.model.getTangentMatrix_Opti('Mass',S.timesteps(1),S.parameters.scaling);
    S.M_Opti = M(S.LocMotionDof,S.LocMotionDof);

    % Copying every trajectory information into a vector that has
    % the same size in the first guess and in the optimization
    % (inserting 0s between trajectory values)
    for nE = 1:length(S.model.listElements)
        if isa(S.model.listElements{nE},'TrajectoryConstraint')
            T = zeros(size(S.model.listElements{nE}.T,1),length(D.parameters.time));
            T(:,S.timesteps) = S.model.listElements{nE}.T(:,1:length(S.timesteps));
    %                     T(:,S.timesteps) = S.model.listElements{nE}.T;
            S.model.listElements{nE}.T = T;
        end
    end

    % Making a list of flexible elements that will be needed in the
    % Sective function.
    S.listFlexBeamElem = [];
    for nE = 1:length(S.model.listElements)
        if isa(S.model.listElements{nE},'FlexibleBeamElement')
            S.listFlexBeamElem = [S.listFlexBeamElem nE];
        end
    end            

    x_0 = zeros(S.nVar*S.npts,1);
    nfixDof = length(S.model.listFixedDof);
    for k = 1:S.npts
        for var = S.model.listNodes
            if isempty(find(S.model.listFixedNodes==var{1}.numberNode))
                locV = S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
                locVdot = 2*S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
                x_0(locV) = var{1}.velocity_InitOpti(:,S.timesteps(k));
                x_0(locVdot) = var{1}.acceleration_InitOpti(:,S.timesteps(k));
            end
        end
        for var = S.model.listElementVariables
            if strcmp(var{1}.DofType,'MotionDof')
                locV = S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
                locVdot = 2*S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
                x_0(locV) = var{1}.velocity_InitOpti(:,S.timesteps(k));
                x_0(locVdot) = var{1}.acceleration_InitOpti(:,S.timesteps(k));
            else
                locValue = 3*S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
                x_0(locValue) = var{1}.value_InitOpti(:,S.timesteps(k));
            end
        end

    end

    sizeMotionConst = S.nCoord + S.nLagrangeM + S.nControls;
    sizeIntregrationConst = 3*S.nCoord;

    delta = 1e-3; % Perturbation size
    x_i = x_0;
    Diff_gradceq = zeros(S.nVar*S.npts, S.npts*(sizeMotionConst + sizeIntregrationConst) - sizeIntregrationConst);
    [c, ceq, gradc, gradceq] = directTranscriptionOptiNLConstraints(x_i, S);
    for i = 1:length(x_i)
        x_pert = zeros(size(x_i));
        x_pert(i) = delta;
    %     [c_pertm, ceq_pertm] = NLConstraints(x_i-x_pert, S);
    %     [c_pert, ceq_pert] = NLConstraints(x_i+x_pert, S);
    %     line=(ceq_pertm-ceq_pert)'/(2*delta);
        [c_pert, ceq_pert] = NLConstraints(x_i+x_pert, S);
        line=(ceq-ceq_pert)'/delta;
        Diff_gradceq(i,:) = -line;
        if i==floor(length(x_i)/2)
            disp('halfway there...')
        end
        if i==floor(3*length(x_i)/4)
            disp('almost there...')
        end
        if i==length(x_i)
            disp('Done !')
        end
        spy(Diff_gradceq',10)
        drawnow
    end
    % figure
    % spy(Diff_gradceq,10)
    % figure
    % spy(gradceq,10)
    Diff = Diff_gradceq'-gradceq';
    tol = 1e-6;
    figure
    spy(abs(Diff)>tol,10)

    Tb = 2;
    sE = (sizeMotionConst + sizeIntregrationConst);
    bloc0 = Diff(1:sE,1:2*S.nVar);
    blocM = Diff(Tb*(sE)+1:(Tb+1)*(sE),Tb*S.nVar+1:(Tb+2)*S.nVar);

    figure
    spy(abs(bloc0)>tol,10)
    title('Differences du Gradient de contrainte au temps initial','Fontsize',14)
    [l,c] = find(abs(bloc0)>tol);
    disp('Pour le bloc au temps 0')
    for equLin = 1:length(l)
        if l(equLin)<=S.nCoord
            if c(equLin)<=S.nCoord
                disp('Problem dans K...')
            elseif c(equLin)<=2*S.nCoord && c(equLin)>S.nCoord
                disp('Problem dans C...')
            elseif c(equLin)<=3*S.nCoord && c(equLin)>2*S.nCoord
                disp('Problem dans M...')
            elseif c(equLin)<=(3*S.nCoord+S.nLagrangeM) && c(equLin)>3*S.nCoord
                disp('Problem dans LagrangeM...')
            elseif c(equLin)<=(3*S.nCoord+S.nLagrangeM+S.nControls) && c(equLin)>(3*S.nCoord+S.nLagrangeM)
                disp('Problem dans Controls...')
            else
                disp('oubli� qqch dans equ mouv...')
            end
        elseif l(equLin)<=(S.nCoord+S.nLagrangeM) && l(equLin)>S.nCoord
            disp('Problem dans contrainte cin...')
        elseif l(equLin)<=(sizeMotionConst) && l(equLin)>(S.nCoord+S.nLagrangeM)
            disp('Problem dans contrainte servo...')
        elseif l(equLin)<=(sizeMotionConst+S.nCoord) && l(equLin)>(sizeMotionConst)
            disp('Problem dans 1er equ integ...')
        elseif l(equLin)<=(sizeMotionConst+2*S.nCoord) && l(equLin)>(sizeMotionConst+S.nCoord)
            disp('Problem dans 2eme equ integ...')
        elseif l(equLin)<=(sizeMotionConst+3*S.nCoord) && l(equLin)>(sizeMotionConst+2*S.nCoord)
            disp('Problem dans 3eme equ integ...')
        else
            disp('oubli� qqch')
        end
    end
    figure
    spy(abs(blocM)>tol,10)
    title(['Differences du Gradient de contrainte au pas de temps ' num2str(Tb)],'Fontsize',14)

    [l,c] = find(abs(blocM)>tol);
    disp(['Pour le bloc au temps ',num2str(Tb)])
    for equLin = 1:length(l)
        if l(equLin)<=S.nCoord
            if c(equLin)<=S.nCoord
                disp('Problem dans K...')
            elseif c(equLin)<=2*S.nCoord && c(equLin)>S.nCoord
                disp('Problem dans C...')
            elseif c(equLin)<=3*S.nCoord && c(equLin)>2*S.nCoord
                disp('Problem dans M...')
            elseif c(equLin)<=4*S.nCoord && c(equLin)>3*S.nCoord
                disp('Problem dans alpha...')
            elseif c(equLin)<=(4*S.nCoord+S.nLagrangeM) && c(equLin)>4*S.nCoord
                disp('Problem dans LagrangeM...')
            elseif c(equLin)<=(4*S.nCoord+S.nLagrangeM+S.nControls) && c(equLin)>(4*S.nCoord+S.nLagrangeM)
                disp('Problem dans Controls...')
            else
                disp(['oubli� qqch dans equ mouv...',num2str(equLin)])
            end
        elseif l(equLin)<=(S.nCoord+S.nLagrangeM) && l(equLin)>S.nCoord
            disp('Problem dans contrainte cin...')
        elseif l(equLin)<=(sizeMotionConst) && l(equLin)>(S.nCoord+S.nLagrangeM)
            disp('Problem dans contrainte servo...')
        elseif l(equLin)<=(sizeMotionConst+S.nCoord) && l(equLin)>(sizeMotionConst)
            disp('Problem dans 1er equ integ...')
        elseif l(equLin)<=(sizeMotionConst+2*S.nCoord) && l(equLin)>(sizeMotionConst+S.nCoord)
            disp('Problem dans 2eme equ integ...')
        elseif l(equLin)<=(sizeMotionConst+3*S.nCoord) && l(equLin)>(sizeMotionConst+2*S.nCoord)
            disp('Problem dans 3eme equ integ...')
        else
            disp('oubli� qqch')
        end
    end
end

if computeDirectDyn == true && computeOpti == true;
    timeVector = 0:finaltime/(npts-1):finaltime;
    timeLoc = S.timesteps;
    u1 = ModelF.listElementVariables{end}.value(1,timeLoc);
    u1_init = ModelR.listElementVariables{end}.value(1,:);
    
    u1_init = interp1(D.parameters.time,u1_init,timeVector,'linear');
    u1 = interp1(S.parameters.time,u1,timeVector,'linear');
    
    type = 'FlexibleBeamElement';
    count = 0; % Second link (flexible)
    for i = 1:nElem1
        elementsDirDyn{count+i}.type = type;
        elementsDirDyn{count+i}.nodes = [nodes(Start1+i-1,1) nodes(Start1+i,1)];
        elementsDirDyn{count+i}.KCS = KCS1;
        elementsDirDyn{count+i}.MCS = MCS1;
        elementsDirDyn{count+i}.alpha = alpha;
        elementsDirDyn{count+i}.beta = beta;
        elementsDirDyn{count+i}.g = grav;
    end
    % count = 1;
    % elementsDirDyn{count}.type = 'RigidBodyElement';
    % elementsDirDyn{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
    % elementsDirDyn{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
    % elementsDirDyn{count}.J = diag([IxxR1 IyyR1 IzzR1]);
    % elementsDirDyn{count}.g = grav;

    count = size(elementsDirDyn,2)+1;
    elementsDirDyn{count}.type = 'RigidBodyElement';
    elementsDirDyn{count}.nodes = [nodes(end,1)];
    elementsDirDyn{count}.mass = m_end; % 0.07
    elementsDirDyn{count}.g = grav;

    count = size(elementsDirDyn,2)+1;
    elementsDirDyn{count}.type = 'KinematicConstraint';
    elementsDirDyn{count}.nodes = [1 2];
    elementsDirDyn{count}.A = [0 0 1 0 0 0]';
    count = count+1;
    
    elementsDirDyn{count}.type = 'ForceInKinematicConstraint';
    elementsDirDyn{count}.elements = [count-1];
    elementsDirDyn{count}.f = u1;

    % Boundary Condition
    BC = [1];

    % Model Def
    ModelFD = FEModel();% Flex model
    ModelFD.CreateFEModel(nodes,elementsDirDyn);
    ModelFD.defineBC(BC);

    ModelRD = FEModel();% Rigid model
    ModelRD.CreateFEModel(nodes,elementsDirDyn);
    ModelRD.defineBC(BC);

    % Solving Flexible
    D = DynamicIntegration(ModelFD);
    D.parameters.finaltime = finaltime;
    D.parameters.timestepsize = timestepsize;
    D.parameters.rho = 0.01;
    D.parameters.scaling = scaling;
    D.runIntegration();

    % Solving Rigid
    ModelRD.listElements{count}.f = u1_init;

    DR = DynamicIntegration(ModelRD);
    DR.parameters.finaltime = finaltime;
    DR.parameters.timestepsize = timestepsize;
    DR.parameters.rho = 0.01;
    DR.parameters.scaling = scaling;
    DR.runIntegration();
    
    % plots 
    x = ModelFD.listNodes{end}.position(1,:);
    y = ModelFD.listNodes{end}.position(2,:);
    z = ModelFD.listNodes{end}.position(3,:);

    xR = ModelRD.listNodes{end}.position(1,:);
    yR = ModelRD.listNodes{end}.position(2,:);
    zR = ModelRD.listNodes{end}.position(3,:);
    
    figure
    hold on
    plot3(x,y,z, '-o','Linewidth',2, 'Color','r')
    plot3(xR,yR,zR,'--', 'Linewidth',2, 'Color','b')
    plot3(trajx,trajy,trajz, 'Linewidth',2, 'Color','k')
    legend('With u','With u_{rigid}','Prescribed')
    xlabel('X [m]','Fontsize',16)
    ylabel('Y [m]','Fontsize',16)
    zlabel('Z [m]','Fontsize',16)
    % title('Trajectory of flexible arm','Fontsize',18)
    grid on
    
    RelError = zeros(size(timeVector));
    RelErrorR = zeros(size(timeVector));
    for i = 1:length(RelError)
%         RelError(i) = 100*(norm([trajz(i)-z(i)])/norm([trajz(i)]));
%         RelErrorR(i) = 100*(norm([trajz(i)-zR(i)])/norm([trajz(i)]));
        RelError(i) = (norm([trajz(i)-z(i)]));
        RelErrorR(i) = (norm([trajz(i)-zR(i)]));
    end
%     figure
%     plot(timeVector,RelError,timeVector,RelErrorR)
%     title('Relative error of the direct dynamic trajectory','Fontsize',13)
%     xlabel('Time')
%     ylabel('Relative Error (%)')
%     legend('Flex','Rigid')
%     grid on

    RMSRelError = sqrt(mean(RelError.^2))
    MaxRelError = max(RelError)
    RMSRelErrorR = sqrt(mean(RelErrorR.^2))
    MaxRelErrorR = max(RelErrorR)
    
end

