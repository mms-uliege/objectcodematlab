%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Specifications bras robot

g = 9.81; % [m/s^2]

% Moteur 1 (base)
m_1 = 0.15; % [kg]
m_art1 = 0.05; % [kg]
m_tot1 = m_1 + m_art1; % [kg]

omega_max1 = 4.5; % [rad/s^2]

% Moteur 2 (epaule)
m_2 = 0.15; % [kg]
m_art2 = 0.05; % [kg]
m_tot2 = m_2 + m_art2; % [kg]

omega_max2 = 7; % [rad/s^2]

% Moteur 3 (coude)
m_3 = 0.15; % [kg]
m_art3 = 0.05; % [kg]
m_tot3 = m_3 + m_art3; % [kg]

omega_max3 = 4.5; % [rad/s^2]

% Materiau bras 1 (rigide)
rho_r = 2700; % [kg/m^3]
L = 0.25; % [m]
diam_ext = 0.03; % [m]
r_ext = diam_ext/2;
diam_int = 0.02; % [m]
r_int = diam_int/2;
A_ext = pi*(r_ext)^2; % [m^2]
A_int = pi*(r_int)^2; % [m^2]
m_ext = A_ext*rho_r*L;
m_int = A_int*rho_r*L;
m_r = m_ext - m_int; % [kg]
I_r = (m_ext/4 * (r_ext^2+(L^2)/3)) - (m_int/4 * (r_int^2+(L^2)/3)); % [kg.m^2] selon axe perpendiculaire axe cylindre au centre
% I_r2 = (m_r/12)*(3*(r_ext^2 + r_int^2) + L^2);

% Materiau bras 2 (souple)

rho_f = rho_r; % [kg/m^3]
L_f = L; % [m]
m_f = m_r; % [kg]
I_f = I_r; % [kg.m^2]

% Masse en bout

m = 0.2; % [kg]
% a_maxxy = 2.5; % [m/s^2]
% a_maxz = 2; % [m/s^2]

I_1 = I_r + m_r*L^2/4;
I_2 = I_f + m_f*(3*L/2)^2;
I_3 = I_f + m_f*(L/2)^2;

% Couple min T3 du moteur 3

T3_grav = g*m_f*L/2 + g*m*L;
T3_inertie = omega_max3*I_3 + omega_max3*L*m;
T3 = T3_grav + T3_inertie;

% Couple min T2 du moteur 2

T2_grav = g*m_r*L/2 + g*m_tot3*L;
T2_inertie = omega_max2*I_1 + omega_max2*L*m_tot3;
% T2 = T2_grav + T2_inertie + T3;
T2 = T2_grav + T2_inertie + omega_max2*I_2 + g*m_f*3*L/2 + g*m*2*L + omega_max3*2*L*m;

% Couple min T1 du moteur 1

T1_grav = 0;
T1_inertie = omega_max1*(I_1 + I_2) + omega_max1*L*m_tot3 + omega_max1*2*L*m;
T1 = T1_grav + T1_inertie;

disp(['Avec une gravit� g = ',num2str(g),' m/s^2'])
disp(['Avec une masse en bout m = ',num2str(m),' kg'])
disp(['Couple min au moteur 1 (base): ', num2str(T1),' Nm'])
disp(['Couple min au moteur 2 (�paule): ', num2str(T2),' Nm'])
disp(['Couple min au moteur 3 (coude): ', num2str(T3),' Nm'])



