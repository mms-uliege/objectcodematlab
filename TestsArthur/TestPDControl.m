%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test of the PD control on a single pendulum using the Apply Force SE3PD
clear all

finalTime = 3; % duration of simulation
timeStepSize = 0.01;
time = 0:timeStepSize:finalTime;

l = 1;
m_end = 1;

grav = [0 0 -9.81];
% grav = [0 0 0];

nodes = [1 0 0 0;
         2 0 0 0;
         3 l 0 0];
     
u = 1*ones(size(time));
ref = -pi/2*ones(size(time));
dref = 0*ones(size(time));
kp = 100;
kd = 10;

count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [3 2];
elements{count}.mass = m_end;
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
% elements{count}.coulomb = 2;
% elements{count}.d = 1;
elements{count}.A = [0 0 0 0 1 0]';

% count = size(elements,2)+1;
% elements{count}.type = 'RotSpringDamperElement';
% elements{count}.nodes = [1 2];
% elements{count}.coulomb = 2;
% % elements{count}.damping = 10;
% % elements{count}.natural_angle = 45*pi/180;
% % elements{count}.stiffness = 100;
% elements{count}.A = [0 1 0];

count = size(elements,2)+1;
elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = count-1;
elements{count}.kp = kp;
elements{count}.kd = kd;
elements{count}.f = u;
elements{count}.ref = ref;
elements{count}.dref = dref;

BC = 1;

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

tic
D = DynamicIntegration(Model);
D.parameters.finaltime = finalTime;
D.parameters.timestepsize = timeStepSize;
D.parameters.rho = 0.1;
% D.parameters.scaling = 1e6;
D.runIntegration();
compTime = toc;
disp(['Computation time is ',num2str(compTime), ' s'])

figure
plot(time,D.model.listElementVariables{1}.relCoo)
title('Joint Angle')
xlabel('Time (s)')
ylabel('Angle (rad)')

figure
plot(time,D.model.listElementVariables{1}.velocity)
title('Joint Velocity')
xlabel('Time (s)')
ylabel('Angle (rad)')
