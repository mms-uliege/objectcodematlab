%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Test of lambda kinemtaic manipulator with beam, for optimisation
clear

finaltime = 1;
timestepsize = 0.01;

%% Creation of the model

% Nodes
nodes = [1 0 0.5 0;
         2 0 0.5 0;
         3 0 0.5 0;
         4 sqrt(3)/2 0 0;
         5 0 -0.5 0;
         6 0 -0.5 0;
         7 sqrt(3)/2 0 0;
         8 sqrt(3) 0.5 0];
     
nElem = 2;
Start = 3;
End = 4;
nodes = createInterNodes(nodes,nElem,Start,End);
     
nElem = 2;
Start = 7;
End = 8;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 9;
End = 10;
nodes = createInterNodes(nodes,nElem,Start,End);

% Elements
elements = [];
elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2];
elements{1}.mass = .5;

elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [6];
elements{2}.mass = .5;

% type = 'RigidBodyElement';
% Start = 3;
% End = 7;
% nElem = (End-Start)/2;
% mass = 6.875/nElem;
% J = 0.57/nElem*eye(3);
% count = size(elements,2);
% for i = 1:nElem
%     elements{count+i}.type = type;
%     elements{count+i}.nodes = [nodes(Start+i+1,1) nodes(Start+i-1,1) nodes(Start+i,1)];
%     elements{count+i}.mass = mass;
%     elements{count+i}.J = J;
% end

a = 0.005;
b = 0.08;
E = 120000e9; nu = 0.33; G = E/(2*(1+nu)); rho = 7600;
A = a*b; I1 = (a*b^3)/12; I2  = (b*a^3)/12;J = (a^2 + b^2)*a*b/12;
KCS = diag([E*A G*A G*A G*J E*I1 E*I2]);
MCS = diag(rho*[A A A J I1 I2]);

type = 'FlexibleBeamElement';
Start = 3;
End = 5;
nElem = End-Start;
count = size(elements,2);
for i = 1:nElem
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start+i-1,1) nodes(Start+i,1)];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
end

type = 'FlexibleBeamElement';
Start = 7;
End = nodes(end,1);
nElem = End-Start;
count = size(elements,2);
for i = 1:nElem
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start+i-1,1) nodes(Start+i,1)];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
end

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % Element 9
elements{count}.nodes = [1 2];
elements{count}.A = [1 0 0 0 0 0]';
count = count+1;

elements{count}.type = 'KinematicConstraint'; % Element 10
elements{count}.nodes = [2 3];
elements{count}.A = [0 0 0 0 0 1]';
count = count+1;

elements{count}.type = 'KinematicConstraint'; % Element 11
elements{count}.nodes = [1 6];
elements{count}.A = [1 0 0 0 0 0]';
count = count+1;

elements{count}.type = 'KinematicConstraint'; % Element 12
elements{count}.nodes = [6 7];
elements{count}.A = [0 0 0 0 0 1]';
count = count+1;

elements{count}.type = 'KinematicConstraint'; % Element 13
elements{count}.nodes = [5 9];
elements{count}.A = [0 0 0 0 0 1]';
count = count+1;

% Trajectory

TrajParam.points = [sqrt(3) 0.5 0;...
                    3 0 0];
                    
TrajParam.timeVector = 0:timestepsize:finaltime;
TrajParam.intervals = [0.3 0.7];

[trajx trajy trajz] = PointTraj(TrajParam);

elements{count}.type = 'TrajectoryConstraint'; % Element 14
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;
                     trajy];
elements{count}.Axe = [1 0 0;
                       0 1 0];
elements{count}.elements = [9 11];
elements{count}.active = 1;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

%% Dynamic integration for initial guess

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.2;
% D.parameters.relTolRes = 1e-12;
D.parameters.scaling = 1e6;
D.runIntegration();


%% Optimization modifications

for n = Model.listNumberNodes
    Model.listNodes{n}.InitializeD_Opti();
end
for n = Model.listNumberElementVariables
    Model.listElementVariables{n}.InitializeD_Opti();
end

a = 0.005;
b = 0.08;
E = 120e9; nu = 0.33; G = E/(2*(1+nu));
A = a*b; I1 = (a*b^3)/12; I2  = (b*a^3)/12;J = (a^2 + b^2)*a*b/12;
KCS = diag([E*A G*A G*A G*J E*I1 E*I2]);
for elem = Model.listElements
   if isa(elem{1},'FlexibleBeamElement')
       elem{1}.KCS = KCS;
   end
end

npts = 80;
TrajParam.timeVector = 0:finaltime/(npts-1):finaltime;
[trajx trajy trajz] = PointTraj(TrajParam);
Model.listElements{14}.T = [trajx;...
                            trajy];

tic
S = DirectTranscriptionOpti(Model);
S.parameters.rho = 0.35;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.NodeToMinimize = [4 8 10];
% S.JointToMinimize = [];
S.parameters.scaling = 1e6;
S.linConst = false;
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min'])
%% Post-Process
% Plots

u1_init = Model.listElementVariables{end}.value_InitOpti(1,:);
u2_init = Model.listElementVariables{end}.value_InitOpti(2,:);

uLambdaRigid.u1_init = u1_init;
uLambdaRigid.u2_init = u2_init;
uLambdaRigid.time = D.parameters.time;

save('uLambdaRigid','uLambdaRigid')

timeSteps = S.timeValues;
timeLoc = S.timesteps;

u1 = Model.listElementVariables{end}.value(1,timeLoc);
u2 = Model.listElementVariables{end}.value(2,timeLoc);

uLambdaBeam.u1 = u1;
uLambdaBeam.u2 = u2;
uLambdaBeam.time = timeSteps;

save('uLambdaBeam','uLambdaBeam')

endNode = 11;

xEff = Model.listNodes{endNode}.position(1,timeLoc);
yEff = Model.listNodes{endNode}.position(2,timeLoc);

figure
title('Trajectory of lambda manipulator effector')
hold on
plot(xEff,yEff, 'Linewidth',3)
plot(trajx,trajy, 'Linewidth',1, 'Color','r')
xlabel('x')
ylabel('y')
grid on

figure
hold on
plot(timeSteps,xEff,timeSteps,yEff)
plot(timeSteps,trajx,'--',timeSteps,trajy,'--')
legend('X', 'Y','Xd','Yd')
xlabel('time')
ylabel('position')
grid on

figure
hold on
plot(timeSteps,u1,timeSteps,u2,'Linewidth',3)
% plot(timeSteps,u1_init(timeLoc),':',timeSteps,u2_init(timeLoc),':','Linewidth',3)
plot(D.parameters.time,u1_init,':',D.parameters.time,u2_init,':','Linewidth',3)
xlabel('Time (s)','Fontsize',16)
ylabel('Commands','Fontsize',16)
legend('u1','u2','u1_{rigid}','u2_{rigid}','Location','Best')
title('Commands of a flexible lambda manipulator','Fontsize',18)
grid on

% save('CartBeamD','D')
% save('CartBeamS','S')

