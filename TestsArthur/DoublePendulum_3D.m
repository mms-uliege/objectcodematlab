%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test example for a dynamic rigid double pendulum on a slider falling under gravity
clear all
% close all

%% Definition of time parameters
finaltime = 2;
timestepsize = 0.01;
grav = [0 0 -9.81];

%% Definition of the nodes of the model (as matrix)
nodes = [1 0 0 0;...
         2 0 0 0;...
         3 0 0 0;...
         4 1 0 0;...
         5 1 0 0;
         6 2 0 0];
 
%% Definition of the elements of the model (as structure)
% Element 1: First rigid link
count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [4 3];
elements{count}.mass = 1;
elements{count}.J = eye(3);
elements{count}.g = grav;


% Element 2: Second link
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [6 5];
elements{count}.mass = 1;
elements{count}.J = eye(3);
elements{count}.g = grav;

% Element 3: First kinematic joint which is a slider
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [1 0 0 0 0 0]';

% Element 4: Second kinematic joint which is a hinge
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.d = 0.5; % damping coefficient inside the hinge
elements{count}.A = [0 0 0 0 1 0;0 0 0 0 0 1]';

% Element 4: Second kinematic joint which is a hinge
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [4 5];
elements{count}.k = 2; % spring-like stiffness force inside the hinge
elements{count}.A = [0 0 0 0 1 0;0 0 0 0 0 1]';

%% Definition of the boundary conditions

BC = [1]; % Node 1 is fixed

%% Definition of the finite element model (FEModel Object)
% Based on previously defined nodes and elements

Model = FEModel;
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

%% Definition of the solver and its parameters

D = DynamicIntegration(Model);  % Creatind the Dynamic integration object
D.parameters.timestepsize = timestepsize;
D.parameters.finaltime = finaltime;
D.parameters.rho = 0.2;
D.parameters.scaling = 1e6;
D.runIntegration(); % Run the integration of the object

%% Visualization and plots
% To visualize the model:(uncomment the following line)
% Model.Visu

% Plots
endNodePos = Model.listNodes{end}.position;
figure
plot(D.parameters.time,endNodePos(1,:),D.parameters.time,endNodePos(2,:),D.parameters.time,endNodePos(3,:),'Linewidth',2)
grid on
title('Tip of the beam position')
xlabel('Time [s]')
ylabel('Poistion [m]')
legend('X','Y','Z')