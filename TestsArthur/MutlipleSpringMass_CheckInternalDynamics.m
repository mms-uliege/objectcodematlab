%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test of multiple spring mass system connected together.
% The input is the force applied to the first mass and the output, the
% position of the last mass. Such problem should have some internal
% dynamics, is it solvable by direct time integration or optimization?

clear all
close all

finaltime = 4; % [s]
h = 0.005; % [s]
initPhaseLength = 0.2; % [s]
t_i = 0+initPhaseLength; 
t_f = finaltime-initPhaseLength;
timeVector = 0:h:finaltime;

rho_num = 0.0;
scaling = 1e6;
tol_num = 1e-6;

grav = 0*[0 9.81 0];

% Masses properties
m = 0.5; % [kg]
J = 0.0*eye(3);
% mas = [0 1];
mas = [0 2 1];

n_mass = length(mas)-1; % Number of masses to be connected
distance = 0.5; % Distance between each masses (natural lenght of each spring)
dir = [0 1 0]; % direction of the masses ([1 0 0] corresponds to x)
joint_axis = [0 1 0 0 0 0]';
travelDist = 1;

% Spring damper properties
k = 10;
d = 0.1;

% Trajectory that is to be followed
freq = 1;
amplitude = 0.1;
start_blend1 = t_i - initPhaseLength/2;
end_blend1 = t_i + initPhaseLength/2;
start_blend2 = t_f - initPhaseLength/2;
end_blend2 = t_f + initPhaseLength/2;

nodes = [1 0 0 0]; % initializing list of nodes
for i = 1:n_mass
    nodes = [nodes; size(nodes,1)+1 nodes(end,2:4)+dir*distance];
end

%% Rigid model

for i = 1:n_mass+1
    elements{i}.type = 'RigidBodyElement';
    elements{i}.nodes = [nodes(i,1)];
    elements{i}.mass = mas(i);
    elements{i}.J = J;
    elements{i}.g = grav;
end

count = size(elements,2);
elements{count+1}.type = 'KinematicConstraint';
elements{count+1}.nodes = [1 2];
elements{count+1}.A = joint_axis;
for i = 2:n_mass
    elements{count+i}.type = 'KinematicConstraint';
    elements{count+i}.nodes = [nodes(i,1) nodes(i+1,1)];
    elements{count+i}.A = 0*joint_axis;
    elements{count+i}.k = k;
    elements{count+i}.d = d;
end

% traj = smoothSineTraj(n_mass*distance,freq,amplitude,timeVector,start_blend1,end_blend1,start_blend2,end_blend2);
% traj = lineTraj(n_mass*distance,n_mass*distance+distance/2,timeVector,t_i,t_f);
traj = lineTraj(n_mass*distance,n_mass*distance+travelDist,timeVector,t_i,t_f);

count = size(elements,2)+1;
elements{count}.type = 'TrajectoryConstraint4';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [traj];
elements{count}.Axe = dir;
elements{count}.elements = [n_mass+2];
elements{count}.active = 1;

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = h;
D.parameters.rho = rho_num;
D.parameters.itMax = 30;
D.parameters.relTolRes = tol_num;
D.parameters.scaling = scaling;
D.runIntegration();

%% Flexible model

for i = 1:n_mass+1
    elementsFlex{i}.type = 'RigidBodyElement';
    elementsFlex{i}.nodes = [nodes(i,1)];
    elementsFlex{i}.mass = mas(i);
    elementsFlex{i}.J = J;
    elementsFlex{i}.g = grav;
end

count = size(elementsFlex,2);
elementsFlex{count+1}.type = 'KinematicConstraint';
elementsFlex{count+1}.nodes = [1 2];
elementsFlex{count+1}.A = joint_axis;
for i = 2:n_mass
    elementsFlex{count+i}.type = 'KinematicConstraint';
    elementsFlex{count+i}.nodes = [nodes(i,1) nodes(i+1,1)];
    elementsFlex{count+i}.A = joint_axis;
    elementsFlex{count+i}.k = k;
    elementsFlex{count+i}.d = d;
end

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'TrajectoryConstraint4';
elementsFlex{count}.nodes = [nodes(end,1)];
elementsFlex{count}.T = [traj];
elementsFlex{count}.Axe = dir;
elementsFlex{count}.elements = [n_mass+2];
elementsFlex{count}.active = 1;

% Boundary Condition
BC = [1];

% Solving
ModelF = FEModel();
ModelF.CreateFEModel(nodes,elementsFlex);
ModelF.defineBC(BC);

for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
        ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

npts = round(finaltime/h + 1); % number of points for optimization
S = DirectTranscriptionOpti(ModelF);
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = h;
S.parameters.rho = rho_num;
S.parameters.relTolRes = tol_num;
S.parameters.scaling = scaling;
S.npts = npts;
S.JointToMinimize = [n_mass + 2 + [1:n_mass-1]];
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
tic
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])
anl = poleAnalysisOpti(S,1,0)
         
%%
% Plots
nodeDispR = [];
nodeVelR = [];
nodeDispF = [];
nodeVelF = [];
leg = {};
for i = 1:n_mass
    nodeDispR = [nodeDispR; dir*ModelR.listNodes{i+1}.position];
    nodeVelR = [nodeVelR; dir*ModelR.listNodes{i+1}.velocity(1:3,:)];
    nodeDispF = [nodeDispF; dir*ModelF.listNodes{i+1}.position];
    nodeVelF = [nodeVelF; dir*ModelF.listNodes{i+1}.velocity(1:3,:)];
    leg = [leg, num2str(i)];
end
figure
hold on
plot(timeVector,nodeDispR,'Linewidth',2)
plot(timeVector,traj,'Linewidth',2)
xlabel('Time [s]')
ylabel('Displacment [m]')
legend([leg 'traj'],'Location','Best')
title('Rigid model')
grid on

figure
hold on
plot(timeVector,ModelR.listElementVariables{end}.value,'Linewidth',2)
plot(timeVector,ModelF.listElementVariables{end}.value,'Linewidth',2)
xlabel('Time [s]')
ylabel('Force [N]')
legend('Rigid','Flexible','Location','Best')
title('Actuation force comparison')
grid on

figure
hold on
plot(timeVector,nodeDispF,'Linewidth',2)
plot(timeVector,traj,'Linewidth',2)
xlabel('Time [s]')
ylabel('Displacment [m]')
legend(leg,'Location','Best')
title('Flexible model')
grid on

figure
hold on
plot(timeVector,nodeVelF,'Linewidth',2)
% plot(timeVector,traj,'Linewidth',2)
xlabel('Time [s]')
ylabel('Velocity [m/2]')
legend(leg,'Location','Best')
title('Flexible model')
grid on

relDispR = [];
relDispF = [];
relVelR = [];
relVelF = [];
for i = 1:n_mass
    relDispR = [relDispR; ModelR.listElementVariables{i}.relCoo];
    relDispF = [relDispF; ModelF.listElementVariables{i}.relCoo];
    relVelR = [relVelR; ModelR.listElementVariables{i}.velocity];
    relVelF = [relVelF; ModelF.listElementVariables{i}.velocity];
end
figure
hold on
plot(timeVector,relDispR,'--','Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(timeVector,relDispF,'Linewidth',2)
xlabel('Time [s]')
ylabel('Joints displacment [m]')
legend([leg leg],'Location','Best')
title('Comparison of rigid and flexible models joint displacements')
grid on

figure
hold on
plot(timeVector,relVelR,'--','Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(timeVector,relVelF,'Linewidth',2)
xlabel('Time [s]')
ylabel('Joints velocity [m/s]')
legend([leg leg],'Location','Best')
title('Comparison of rigid and flexible models joint velocities')
grid on
