%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear

finaltime = 2;
timestepsize = 0.01;
t_i = 0.25;
t_f = finaltime - t_i;

% nodes = [1 -1 0 0;
%          2 -1 0 0;
%          3 -1 0 0;
%          4 -1.6614 -0.75 0;
%          5 -1.6614 -0.75 0;
%          6 -1.3307 -1.125 0;
%          7 -1.3307 -1.125 0;
%          8 -1 -1.5 0];
% nodes = createInterNodes(nodes,2,3,4);
% nodes = createInterNodes(nodes,2,6,7);
% nodes = createInterNodes(nodes,2,9,10);

nodes = [1 -1 0 0;
         2 -1 0 0;
         3 -1 0 0;
         4 -1.6614 -0.75 0;
         5 -1.6614 -0.75 0;
         6 -1 -1.5 0];

nodes = createInterNodes(nodes,2,3,4);
nodes = createInterNodes(nodes,6,6,7);
nodes = doubleNode(nodes,[8 10]);


elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2];
elements{1}.mass = 3;
% elements{1}.J = eye(3);
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [4 3 5];
elements{2}.mass = 6.875;
elements{2}.J = 0.57*eye(3);

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [7 6 8];
elements{3}.mass = 6.875/3;
elements{3}.J = 0.0217*eye(3);

elements{4}.type = 'RigidBodyElement';
elements{4}.nodes = [10 9 11];
elements{4}.mass = 6.875/3;
elements{4}.J = 0.0217*eye(3);

elements{5}.type = 'RigidBodyElement';
elements{5}.nodes = [13 12 14];
elements{5}.mass = 6.875/3;
elements{5}.J = 0.0217*eye(3);

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [1 2];
elements{6}.A = [1 0 0 0 0 0]';

elements{7}.type = 'KinematicConstraint';
elements{7}.nodes = [2 3];
elements{7}.A = [0 0 0 0 0 1]';

elements{8}.type = 'KinematicConstraint';
elements{8}.nodes = [5 6];
elements{8}.A = [0 0 0 0 0 1]';

elements{9}.type = 'KinematicConstraint';
elements{9}.nodes = [8 9];
elements{9}.A = 0*[0 0 0 0 0 1]';

elements{10}.type = 'KinematicConstraint';
elements{10}.nodes = [11 12];
elements{10}.A = 0*[0 0 0 0 0 1]';

elements{11}.type = 'RotSpringDamperElement';
elements{11}.damping = 200; % 200 
elements{11}.stiffness = 100; % 100
elements{11}.nodes = [8 9];
elements{11}.A = [0 0 1];

elements{12}.type = 'RotSpringDamperElement';
elements{12}.damping = 200; % 200
elements{12}.stiffness = 100; % 100
elements{12}.nodes = [11 12];
elements{12}.A = [0 0 1];

% Trajectory

cart_end = 1;
cart_i = -1;

x_end = 1;
x_i = -1;

y_end = -1.5;
y_i = -1.5;

r = 1;

% 0.2,1.7    0,1.5    0.5,2     0.8,2.3
timeVector = 0:timestepsize:finaltime;
trajcart = halfCircleTraj(cart_i,cart_end,r,timeVector,'lin',t_i,t_f);
trajx = halfCircleTraj(x_i,x_end,r,timeVector,'sin',t_i,t_f);
trajy = halfCircleTraj(y_i,y_end,r,timeVector,'cos',t_i,t_f);

elements{13}.type = 'TrajectoryConstraint';
elements{13}.nodes = [2];
elements{13}.T = [trajcart];
elements{13}.Axe = [1 0 0];
elements{13}.elements = [6];
elements{13}.active = 1;

elements{14}.type = 'TrajectoryConstraint';
elements{14}.nodes = [nodes(end,1)];
elements{14}.T = [trajx;...
                 trajy];
elements{14}.Axe = [1 0 0;...
                   0 1 0];
elements{14}.elements = [7 8];
elements{14}.active = 1;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

compute = 1;
if compute
    D = DynamicIntegration(Model);
    D.parameters.finaltime = finaltime;
    D.parameters.timestepsize = timestepsize;
    D.parameters.rho = 0.8;
    % D.parameters.relTolRes = 1e-12;
    D.runIntegration();
else
    load CartFlexD
    Model = D.model;
end

% creating Flexible model
ModelF = FEModel();
ModelF.CreateFEModel(nodes,elements);
ModelF.defineBC(BC);
% Copying ElementVariables and Node variables from rigid solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = Model.listNodes{n}.R;
    ModelF.listNodes{n}.position = Model.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = Model.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = Model.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
%         ModelF.listElementVariables{n}.A = Model.listElementVariables{n}.A;
%         ModelF.listElementVariables{n}.nDof = Model.listElementVariables{n}.nDof;
        ModelF.listElementVariables{n}.R = Model.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = Model.listElementVariables{n}.x;
%         ModelF.listElementVariables{n}.xI0 = Model.listElementVariables{n}.xI0;
        ModelF.listElementVariables{n}.velocity = Model.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = Model.listElementVariables{n}.acceleration;
        ModelF.listElementVariables{n}.relCoo = Model.listElementVariables{n}.relCoo;
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(Model.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = Model.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end
ModelF.listElements{9}.A = [0 0 0 0 0 1]';
ModelF.listElements{10}.A = [0 0 0 0 0 1]';

ModelF.listElements{11}.stiffness = 100; % 125 to make it unstable
ModelF.listElements{11}.damping = 2.25; % 0.75
ModelF.listElements{12}.stiffness = 100; % 125 to make it unstable
ModelF.listElements{12}.damping = 2.25; % 0.75

for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.InitializeD_Opti();
end
for n = ModelF.listNumberElementVariables
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

npts = finaltime/timestepsize + 1;
timeVector = 0:finaltime/(npts-1):finaltime;
trajcart = halfCircleTraj(cart_i,cart_end,r,timeVector,'lin',t_i,t_f);
trajx = halfCircleTraj(x_i,x_end,r,timeVector,'sin',t_i,t_f);
trajy = halfCircleTraj(y_i,y_end,r,timeVector,'cos',t_i,t_f);
Model.listElements{13}.T = [trajcart];
Model.listElements{14}.T = [trajx;...
                            trajy];
Model.listElements{13}.active = 1;
Model.listElements{14}.active = 1;

tic
S = DirectTranscriptionOpti(ModelF);
% S.parameters.relTolRes = 1e-12; 
S.parameters.rho = 0.8;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
% S.NodeToMinimize = [7 8 9 10];
S.JointToMinimize = [9 10];
S.parameters.scaling = 1e6;
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])

DeplCoudeOpti = ModelF.listElementVariables{4}.relCoo;
DeplCoude0 = ModelF.listElementVariables{4}.relCoo_InitOpti;
u1_init = ModelF.listElementVariables{end-1}.value_InitOpti;
u2_init = ModelF.listElementVariables{end}.value_InitOpti(1,:);
u3_init = ModelF.listElementVariables{end}.value_InitOpti(2,:);

% Plots

uCartDoubleRigid.u1_init = u1_init;
uCartDoubleRigid.u2_init = u2_init;
uCartDoubleRigid.u3_init = u3_init;
uCartDoubleRigid.time = D.parameters.time;

save('uCartDoubleRigid','uCartDoubleRigid')

timeSteps = S.timeValues;
timeLoc = S.timesteps;

uCartDoubleFlex.u1 = ModelF.listElementVariables{end-1}.value(timeLoc);
uCartDoubleFlex.u2 = ModelF.listElementVariables{end}.value(1,timeLoc);
uCartDoubleFlex.u3 = ModelF.listElementVariables{end}.value(2,timeLoc);
uCartDoubleFlex.time = timeSteps;

save('uCartDoubleFlex','uCartDoubleFlex')

figure
hold on
plot(Model.listNodes{2}.position(1,timeLoc),Model.listNodes{2}.position(2,timeLoc),Model.listNodes{end}.position(1,timeLoc),Model.listNodes{end}.position(2,timeLoc), 'Linewidth',3)
plot(trajcart,zeros(size(trajcart)),trajx,trajy, 'Linewidth',1, 'Color','r')
grid on


figure
hold on
plot(timeSteps,ModelF.listNodes{2}.position(1,timeLoc),timeSteps,ModelF.listNodes{end}.position(1,timeLoc),timeSteps,ModelF.listNodes{end}.position(2,timeLoc))
plot(timeSteps,trajcart,'--',timeSteps,trajx,'--',timeSteps,trajy,'--')
legend('cart', 'X', 'Y','cartd','Xd','Yd')
grid on

figure
hold on
plot(timeSteps,ModelF.listElementVariables{end-1}.value(timeLoc),timeSteps,ModelF.listElementVariables{end}.value(1,timeLoc),timeSteps,ModelF.listElementVariables{end}.value(2,timeLoc),'Linewidth',3)
plot(timeSteps,u1_init(timeLoc),':',timeSteps,u2_init(timeLoc),':',timeSteps,u3_init(timeLoc),':','Linewidth',3)
xlabel('Time (s)','Fontsize',16)
ylabel('Commands','Fontsize',16)
legend('u1','u2','u3','u1_{rigid}','u2_{rigid}','u3_{rigid}','Location','Best')
title('Commands of a flexible cart system','Fontsize',18)
grid on

figure
plot(timeSteps,DeplCoude0(timeLoc),timeSteps,DeplCoudeOpti(timeLoc))
legend('Beta init','Beta Opti')
grid on

beta1 = ModelF.listElementVariables{4}.relCoo(timeLoc);
beta1dot = ModelF.listElementVariables{4}.velocity(timeLoc);
beta2 = ModelF.listElementVariables{5}.relCoo(timeLoc);
beta2dot = ModelF.listElementVariables{5}.velocity(timeLoc);
figure
axis([min([beta1,beta2]) max([beta1,beta2]) min([beta1dot,beta2dot]) max([beta1dot,beta2dot])])
grid on
title('Phase plot','FontSize',18)
xlabel('\beta [rad]','FontSize',16)
ylabel('d\beta/dt [rad/s]','FontSize',16)
hold on
for i = 1:length(beta1)
    if i==1
        plot(beta1(i),beta1dot(i),'-o','MarkerFaceColor','g','MarkerSize',12)
        plot(beta2(i),beta2dot(i),'-o','MarkerFaceColor','g','MarkerSize',12)
    elseif i==length(beta1)
        plot(beta1(i),beta1dot(i),'-o','MarkerFaceColor','r','MarkerSize',12)
        plot(beta2(i),beta2dot(i),'-o','MarkerFaceColor','r','MarkerSize',12)
    else
        plot(beta1(i),beta1dot(i),'-o','MarkerFaceColor','y')
        plot(beta2(i),beta2dot(i),'-o','MarkerFaceColor','y')
    end
    drawnow
    pause(.1)
end
plot(beta1,beta1dot,'-b',beta2,beta2dot,'-r','Linewidth',2)
legend('\beta_1','\beta_2')

xcart = ModelF.listNodes{2}.position(1,timeLoc);
ycart = ModelF.listNodes{2}.position(2,timeLoc);
x = ModelF.listNodes{end}.position(1,timeLoc);
y = ModelF.listNodes{end}.position(2,timeLoc);


% save('cartDoubleFlex','S')

% RelError = zeros(size(timeSteps));
% for i = 1:length(RelError)
%     RelError(i) = 100*(norm([trajx(i)-x(i) trajy(i)-y(i)]))/norm([trajx(i) trajy(i)]);
% end
% figure
% plot(timeSteps,RelError)
% title('Relative error of the direct dynamic trajectory','Fontsize',13)
% xlabel('time')
% ylabel('Relative Error (%)')
% grid on

