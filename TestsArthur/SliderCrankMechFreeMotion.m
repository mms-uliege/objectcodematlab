%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% slider crank mechanism on a spring free motion

clear
close all

finaltime = 2;
timestepsize = 0.01;
t_i = 0.1;
t_f = 1.9;

a1 = 0.02;
b1 = a1;
l1 = 1;
l2 = l1;
e1 = 0.002;
rapport = a1/e1;
a2 = 0.02;
b2 = a2;
e2 = a2/rapport;
a2In = a2-2*e2;
b2In = b2-2*e2;
a1In = a1-2*e1; % inner length
b1In = b1-2*e1;
angle = 45*pi/180;
mass = 0.5;

grav = [0 0 -0];

nodes = [1 0 0 0;
         2 0 0 0;
         3 l1*cos(angle) 0 l1*sin(angle);
         4 l1*cos(angle) 0 l1*sin(angle);
         5 l1*cos(angle)+l2*cos(angle) 0 0];
     
nElem1 = 2;
Start1 = 2;
End1 = 3;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

nElem2 = 2;
Start2 = End1+nElem1;
End2 = Start2+1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);

nodes = doubleNode(nodes,nodes(end,1));

%% Rigid Model

rho = 2700;
m1 = (a1*b1-a1In*b1In)*l1*rho;
% IxxR1 = m1*(a1^2+b1^2)/12;
IxxR1 = m1*(a1^2+b1^2-a1In^2-b1In^2)/12;
IyyR1 = m1*(a1^2+l1^2)/12;
IzzR1 = m1*(b1^2+l1^2)/12;

m2 = (a2*b2-a2In*b2In)*l2*rho;
IxxR2 = m2*(a2^2+b2^2-a2In^2-b2In^2)/12;
IyyR2 = m2*(a2^2+l2^2)/12;
IzzR2 = m2*(b2^2+l2^2)/12;

count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elements{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR1 IyyR1 IzzR1]);
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
elements{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR2 IyyR2 IzzR2]);
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = mass;
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'RotSpringDamperElement';
elements{count}.nodes = [1 Start1];
elements{count}.stiffness = 100;
elements{count}.damping = 5;
elements{count}.natural_angle = pi/2;
elements{count}.A = [0 1 0];

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 Start1];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start1+nElem1 Start2];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [nodes(end-1,1) nodes(end,1)];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [nodes(end,1) 1];
elements{count}.A = [1 0 0 0 0 0]';

% % Trajectory
% r=l1/4;
% x_end = nodes(end,2);
% 
% timeVector = 0:timestepsize:finaltime;
% trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'cos',t_i,t_f);
% count = count+1;
% elements{count}.type = 'TrajectoryConstraint';
% elements{count}.nodes = [nodes(end,1)];
% elements{count}.T = [trajx];
% elements{count}.Axe = [1 0 0];
% elements{count}.elements = [count-3];
% elements{count}.active = 1;

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.3;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e3;
D.runIntegration();

endPoint = ModelR.listNodes{end};
figure
plot(D.parameters.time,endPoint.position(1,:),D.parameters.time,endPoint.position(2,:),D.parameters.time,endPoint.position(3,:))
legend('x','y','z')

command = ModelR.listElementVariables{end};
figure
plot(D.parameters.time,command.value(1,:))
legend('torque')

commandJoint = ModelR.listElementVariables{1};
figure
plot(D.parameters.time,commandJoint.relCoo(1,:))
legend('Command Angle')

