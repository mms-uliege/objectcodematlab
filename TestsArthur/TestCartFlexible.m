%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control

finaltime = 2;
timestepsize = 0.01;

nodes = [1 -1 0 0;
         2 -1 0 0;
         3 -1 0 0;
         4 -1.6614 -0.75 0;
         5 -1.6614 -0.75 0;
         6 -1.3307 -1.125 0;
         7 -1.3307 -1.125 0;
         8 -1 -1.5 0];

elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2];
elements{1}.mass = 3;
% elements{1}.J = eye(3);
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [3 4];
elements{2}.mass = 6.875;
elements{2}.J = 0.57*eye(3);

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [5 6];
elements{3}.mass = 6.875/2;
elements{3}.J = 0.0723*eye(3);

elements{4}.type = 'RigidBodyElement';
elements{4}.nodes = [7 8];
elements{4}.mass = 6.875/2;
elements{4}.J = 0.0723*eye(3);


% elements{2}.type = 'ExternalForce';
% elements{2}.nodes = 3;
% elements{2}.DOF = 2;
% elements{2}.amplitude = -10;
% elements{2}.frequency = 0;

elements{5}.type = 'KinematicConstraint';
elements{5}.nodes = [1 2];
elements{5}.A = [1 0 0 0 0 0]';

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [2 3];
elements{6}.A = [0 0 0 0 0 1]';

elements{7}.type = 'KinematicConstraint';
elements{7}.nodes = [4 5];
elements{7}.A = [0 0 0 0 0 1]';

elements{8}.type = 'KinematicConstraint';
elements{8}.nodes = [6 7];
elements{8}.A = [0 0 0 0 0 1]';

elements{9}.type = 'RotSpringDamperElement';
elements{9}.damping = 0.25;
elements{9}.stiffness = 3; % Minimum Phase
% elements{9}.stiffness = 3; % Non-Minimum Phase
elements{9}.nodes = [6 7];
elements{9}.A = [0 0 1];

% Trajectory

cart_end = 1;
cart_i = -1;

x_end = 1;
x_i = -1;

y_end = -1.5;
y_i = -1.5;

trajcart = halfCircleTraj(cart_i,cart_end, finaltime,timestepsize,'lin',0.2,1.7);
trajx = halfCircleTraj(x_i,x_end, finaltime,timestepsize,'sin',0.2,1.7);
trajy = halfCircleTraj(y_i,y_end, finaltime,timestepsize,'cos',0.2,1.7);

elements{10}.type = 'TrajectoryConstraint';
elements{10}.nodes = [2];
elements{10}.T = [trajcart];
elements{10}.Axe = [1 0 0];
elements{10}.elements = [5];

elements{11}.type = 'TrajectoryConstraint';
elements{11}.nodes = [8];
elements{11}.T = [trajx;...
                 trajy];
elements{11}.Axe = [1 0 0;...
                   0 1 0];
elements{11}.elements = [6 7];


% load uflex
% elements{11}.type = 'ForceInKinematicConstraint';
% elements{11}.elements = [5];
% elements{11}.f = u1f;
% 
% elements{12}.type = 'ForceInKinematicConstraint';
% elements{12}.elements = [6];
% elements{12}.f = u2f;
% 
% elements{13}.type = 'ForceInKinematicConstraint';
% elements{13}.elements = [7];
% elements{13}.f = u3f;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

S = DynamicIntegration(Model);
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = timestepsize;
S.parameters.relTolRes = 1e-12;
S.parameters.rho = 0.7;
S.runIntegration();

% Plots

figure
hold on
plot(Model.listNodes{2}.position(1,:),Model.listNodes{2}.position(2,:),Model.listNodes{8}.position(1,:),Model.listNodes{8}.position(2,:), 'Linewidth',3)
plot(trajcart,zeros(size(trajcart)),trajx,trajy, 'Linewidth',1, 'Color','r')
grid on


figure
hold on
plot(S.parameters.time,Model.listNodes{2}.position(1,:),S.parameters.time,Model.listNodes{8}.position(1,:),S.parameters.time,Model.listNodes{8}.position(2,:))
plot(S.parameters.time,trajcart,'--',S.parameters.time,trajx,'--',S.parameters.time,trajy,'--')
legend('cart', 'X', 'Y','cartd','Xd','Yd')
grid on


figure
plot(S.parameters.time,Model.listElementVariables{end-1}.value,S.parameters.time,Model.listElementVariables{end}.value(1,:),S.parameters.time,Model.listElementVariables{end}.value(2,:))
legend('u1','u2','u3')
grid on

u1f = Model.listElementVariables{end-1}.value;
u2f = Model.listElementVariables{end}.value(1,:);
u3f = Model.listElementVariables{end}.value(2,:);
save('uflex','u1f','u2f','u3f')


