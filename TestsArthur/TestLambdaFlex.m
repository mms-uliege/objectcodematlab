%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test of rigid lambda kinematic manipulator
clear

finaltime = 2;
timestepsize = 0.01;

%% Creation of the model

% Nodes
nodes = [1 0 0.5 0;
         2 0 0.5 0;
         3 0 0.5 0;
         4 sqrt(3)/2 0 0;
         5 0 -0.5 0;
         6 0 -0.5 0;
         7 sqrt(3)/2 0 0;
         8 3*sqrt(3)/4 0.25 0;
         9 3*sqrt(3)/4 0.25 0;
         10 sqrt(3) 0.5 0];
     
nElem = 2;
Start = 3;
End = 4;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 7;
End = 8;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 9;
End = 10;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 12;
End = 13;
nodes = createInterNodes(nodes,nElem,Start,End);
     
% Elements
elements = [];
elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2];
elements{1}.mass = 0.5;

elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [6];
elements{2}.mass = 0.5;

J = 0.57*eye(3);
mass = 0.5;

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [4 3 5];
elements{3}.mass = mass;
elements{3}.J = J;

elements{4}.type = 'RigidBodyElement';
elements{4}.nodes = [8 7 9];
elements{4}.mass = mass;
elements{4}.J = J;

elements{5}.type = 'RigidBodyElement';
elements{5}.nodes = [10 9 11];
elements{5}.mass = mass/2;
elements{5}.J = J;

elements{6}.type = 'RigidBodyElement';
elements{6}.nodes = [13 12 14];
elements{6}.mass = mass/2;
elements{6}.J = J;

elements{7}.type = 'KinematicConstraint';
elements{7}.nodes = [1 2];
elements{7}.A = [1 0 0 0 0 0]';

elements{8}.type = 'KinematicConstraint';
elements{8}.nodes = [2 3];
elements{8}.A = [0 0 0 0 0 1]';

elements{9}.type = 'KinematicConstraint';
elements{9}.nodes = [1 6];
elements{9}.A = [1 0 0 0 0 0]';

elements{10}.type = 'KinematicConstraint';
elements{10}.nodes = [6 7];
elements{10}.A = [0 0 0 0 0 1]';

elements{11}.type = 'KinematicConstraint';
elements{11}.nodes = [5 9];
elements{11}.A = [0 0 0 0 0 1]';

elements{12}.type = 'KinematicConstraint';
elements{12}.nodes = [11 12];
elements{12}.A = [0 0 0 0 0 1]';

elements{13}.type = 'RotSpringDamperElement';
elements{13}.damping = 100; %
elements{13}.stiffness = 100; %
elements{13}.nodes = [11 12];
elements{13}.A = [0 0 1];

% Trajectory

TrajParam.points = [sqrt(3) 0.5 0;...
                    3 0 0];
                    
TrajParam.timeVector = 0:timestepsize:finaltime;
TrajParam.intervals = [0.3 0.7];

[trajx trajy trajz] = PointTraj(TrajParam);

elements{14}.type = 'TrajectoryConstraint'; % Element 14
elements{14}.nodes = [nodes(end,1)];
elements{14}.T = [trajx;
                 trajy];
elements{14}.Axe = [1 0 0;
                   0 1 0];
elements{14}.elements = [7 9];
elements{14}.active = 1;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

%% Dynamic integration for initial guess

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.0;
% D.parameters.relTolRes = 1e-12;
% D.parameters.scaling = 1e6;
D.runIntegration();

%% Optimization

for n = Model.listNumberNodes
    Model.listNodes{n}.InitializeD_Opti();
end
for n = Model.listNumberElementVariables
    Model.listElementVariables{n}.InitializeD_Opti();
end

Model.listElements{13}.damping = 0.1;
Model.listElements{13}.stiffness = 2;

npts = 60;
TrajParam.timeVector = 0:finaltime/(npts-1):finaltime;
[trajx trajy trajz] = PointTraj(TrajParam);
Model.listElements{14}.T = [trajx;...
                            trajy];

tic
S = DirectTranscriptionOpti(Model);
S.parameters.rho = 0.0;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
% S.NodeToMinimize = [];
S.JointToMinimize = [12];
S.parameters.scaling = 1e6;
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min'])


%% Post process

timeSteps = S.timeValues;
timeLoc = S.timesteps;

EndNode = 14;

u1_init = Model.listElementVariables{end}.value_InitOpti(1,:);
u2_init = Model.listElementVariables{end}.value_InitOpti(2,:);

uLambdaRigid.u1_init = u1_init;
uLambdaRigid.u2_init = u2_init;
uLambdaRigid.time = D.parameters.time;

save('uLambdaRigid','uLambdaRigid')

u1 = Model.listElementVariables{end}.value(1,timeLoc);
u2 = Model.listElementVariables{end}.value(2,timeLoc);

uLambdaFlex.u1 = u1;
uLambdaFlex.u2 = u2;
uLambdaFlex.time = timeSteps;

save('uLambdaFlex','uLambdaFlex')

u1_init = Model.listElementVariables{end}.value_InitOpti(1,:);
u2_init = Model.listElementVariables{end}.value_InitOpti(2,:);

xEff = Model.listNodes{EndNode}.position(1,timeLoc);
yEff = Model.listNodes{EndNode}.position(2,timeLoc);
xEffdot = Model.listNodes{EndNode}.velocity(1,timeLoc);
yEffdot = Model.listNodes{EndNode}.velocity(2,timeLoc);

figure
title('Commands of flexible lambda manipulator')
hold on
plot(timeSteps,u1,timeSteps,u2,'Linewidth',3)
plot(D.parameters.time,u1_init,':',D.parameters.time,u2_init,':','Linewidth',3)
legend('u1','u2','u1_{rigid}','u2_{rigid}','Location','Best')
xlabel('time')
ylabel('command')
grid on

figure
title('X and Y of lambda manipulator effector')
hold on
plot(timeSteps,xEff,timeSteps,yEff)
plot(timeSteps,trajx,':',timeSteps,trajy,':')
legend('x','y','x_d','y_d')
xlabel('time')
ylabel('position')
grid on

% figure
% title('Xdot and Ydot of lambda manipulator effector')
% hold on
% plot(timeSteps,xEffdot,timeSteps,yEffdot)
% legend('x','y')
% xlabel('time')
% ylabel('velocity')
% grid on

figure
title('Trajectory of lambda manipulator effector')
hold on
plot(xEff,yEff,'Linewidth',3)
plot(trajx,trajy, 'Linewidth',1, 'Color','r')
xlabel('x')
ylabel('y')
grid on

beta = Model.listElementVariables{6}.relCoo(timeLoc);
betadot = Model.listElementVariables{6}.velocity(timeLoc);
figure
axis([min(beta) max(beta) min(betadot) max(betadot)])
grid on
title('Phase plot','FontSize',18)
xlabel('\beta [rad]','FontSize',16)
ylabel('d\beta/dt [rad/s]','FontSize',16)
hold on
for i = 1:length(beta)
    if i==1
        plot(beta(i),betadot(i),'-o','MarkerFaceColor','g','MarkerSize',12)
    elseif i==length(beta)
        plot(beta(i),betadot(i),'-o','MarkerFaceColor','r','MarkerSize',12)
    else
        plot(beta(i),betadot(i),'-o','MarkerFaceColor','y')
    end
    drawnow
    pause(.1)
end
plot(beta,betadot,'-')


