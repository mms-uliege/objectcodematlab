%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Test of flexible cart with beam, for optimisation
clear

finaltime = 2.0;
timestepsize = 0.01;
t_i = 0.2;
t_f = 1.7;

%% Creation of the nodes

nodes = [1 -1 0 0;
         2 -1 0 0;
         3 -1 0 0;
         4 -1.6614 -0.75 0;
         5 -1.6614 -0.75 0;
         6 -1 -1.5 0];
     
nElem1 = 8;
Start1 = 3;
End1 = 4;
nodes = createInterNodes(nodes,nElem1,Start1,End1);
     
nElem2 = 8;
Start2 = End1+nElem1;
End2 = Start2+1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);

%% Rigid Model
% Elements
a = 0.005;
b = a;
l = 1;
rho = 8400;
m = a*b*l*rho;
Ixx = m*(a^2+b^2)/12;
Iyy = m*(a^2+l^2)/12;
Izz = m*(b^2+l^2)/12;

count = 1;
elements = [];
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [2];
elements{count}.mass = 3;
% elements{count}.J = eye(3);
count = count +1;

elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elements{count}.mass = m; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([Ixx Iyy Izz]);
% elements{count}.g = [0 0 -9.81];
count = count +1;

elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem1/2+1+[0:nElem1/2-1]];
elements{count}.mass = m; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([Ixx Iyy Izz]);
% elements{count}.g = [0 0 -9.81];
count = count +1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [1 0 0 0 0 0]';
count = count+1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.A = [0 0 0 0 0 1]';
count = count+1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.A = [0 0 0 0 0 1]';
count = count+1;

% Trajectory
cart_end = 1;
cart_i = -1;

x_end = 1;
x_i = -1;

y_end = -1.5;
y_i = -1.5;

r = 1;

timeVector = 0:timestepsize:finaltime;
trajcart = halfCircleTraj(cart_i,cart_end,r,timeVector,'lin',t_i,t_f);
trajx = halfCircleTraj(x_i,x_end,r,timeVector,'sin',t_i,t_f);
trajy = halfCircleTraj(y_i,y_end,r,timeVector,'cos',t_i,t_f);

elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [2];
elements{count}.T = [trajcart];
elements{count}.Axe = [1 0 0];
elements{count}.elements = [count-3];
elements{count}.active = 1;
count = count+1;

elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                 trajy];
elements{count}.Axe = [1 0 0;...
                   0 1 0];
elements{count}.elements = [count-3 count-2];
elements{count}.active = 1;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

% Solving

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.1;
% D.parameters.relTolRes = 1e-12;
D.parameters.scaling = 1e6;
D.runIntegration();

uRigi.u1 = Model.listElementVariables{end-1}.value;
uRigi.u2 = Model.listElementVariables{end}.value(1,:);
uRigi.u3 = Model.listElementVariables{end}.value(2,:);
uRigi.time = D.parameters.time;


%% Flexible Model

E = 120e9; nu = 0.33; G = E/(2*(1+nu));
A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;
KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

count = 1;
elementsFlex = [];
elementsFlex{count}.type = 'RigidBodyElement';
elementsFlex{count}.nodes = [2];
elementsFlex{count}.mass = 3;
% elementsFlex{count}.J = eye(3);

type = 'FlexibleBeamElement';
% count = size(elementsFlex,2);
for i = 1:nElem1
    elementsFlex{count+i}.type = type;
    elementsFlex{count+i}.nodes = [nodes(Start1+i-1,1) nodes(Start1+i,1)];
    elementsFlex{count+i}.KCS = KCS;
    elementsFlex{count+i}.MCS = MCS;
end

count = size(elementsFlex,2);
for i = 1:nElem2
    elementsFlex{count+i}.type = type;
    elementsFlex{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
    elementsFlex{count+i}.KCS = KCS;
    elementsFlex{count+i}.MCS = MCS;
end

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [1 2];
elementsFlex{count}.A = [1 0 0 0 0 0]';
count = count+1;

elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [2 3];
elementsFlex{count}.A = [0 0 0 0 0 1]';
count = count+1;

elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [Start2-1 Start2];
elementsFlex{count}.A = [0 0 0 0 0 1]';
count = count+1;

% Trajectory
npts = 101;

timeVector = 0:finaltime/(npts-1):finaltime;
trajcart = halfCircleTraj(cart_i,cart_end,r,timeVector,'lin',t_i,t_f);
trajx = halfCircleTraj(x_i,x_end,r,timeVector,'sin',t_i,t_f);
trajy = halfCircleTraj(y_i,y_end,r,timeVector,'cos',t_i,t_f);

elementsFlex{count}.type = 'TrajectoryConstraint';
elementsFlex{count}.nodes = [2];
elementsFlex{count}.T = [trajcart];
elementsFlex{count}.Axe = [1 0 0];
elementsFlex{count}.elements = [count-3];
elementsFlex{count}.active = 1;
count = count+1;

elementsFlex{count}.type = 'TrajectoryConstraint';
elementsFlex{count}.nodes = [nodes(end,1)];
elementsFlex{count}.T = [trajx;...
                        trajy];
elementsFlex{count}.Axe = [1 0 0;...
                        0 1 0];
elementsFlex{count}.elements = [count-3 count-2];
elementsFlex{count}.active = 1;

ModelF = FEModel();
ModelF.CreateFEModel(nodes,elementsFlex);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from rigid solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = Model.listNodes{n}.R;
    ModelF.listNodes{n}.position = Model.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = Model.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = Model.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
%         ModelF.listElementVariables{n}.A = Model.listElementVariables{n}.A;
%         ModelF.listElementVariables{n}.nDof = Model.listElementVariables{n}.nDof;
        ModelF.listElementVariables{n}.R = Model.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = Model.listElementVariables{n}.x;
%         ModelF.listElementVariables{n}.xI0 = Model.listElementVariables{n}.xI0;
        ModelF.listElementVariables{n}.velocity = Model.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = Model.listElementVariables{n}.acceleration;
        ModelF.listElementVariables{n}.relCoo = Model.listElementVariables{n}.relCoo;
    else
        while Model.listElementVariables{n+skip}.nL>6
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = Model.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

tic
S = DirectTranscriptionOpti(ModelF);
% S.parameters.relTolRes = 1e-12;
S.parameters.rho = 0.1;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.NodeToMinimize = [Start1+[1:nElem1-1] Start2+[1:nElem2-1]];
% S.JointToMinimize = [];
S.parameters.scaling = 1e6;
S.linConst = false;
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])


%% Post-Process
% Plots

u1_init = Model.listElementVariables{end-1}.value;
u2_init = Model.listElementVariables{end}.value(1,:);
u3_init = Model.listElementVariables{end}.value(2,:);

uCartRigid.u1_init = u1_init;
uCartRigid.u2_init = u2_init;
uCartRigid.u3_init = u3_init;
uCartRigid.time = D.parameters.time;

save('uCartRigid','uCartRigid')

timeSteps = S.timeValues;
timeLoc = S.timesteps;

u1 = ModelF.listElementVariables{end-1}.value(timeLoc);
u2 = ModelF.listElementVariables{end}.value(1,timeLoc);
u3 = ModelF.listElementVariables{end}.value(2,timeLoc);

uCartBeam.u1 = u1;
uCartBeam.u2 = u2;
uCartBeam.u3 = u3;
uCartBeam.time = timeSteps;

save('uCartBeam','uCartBeam')

figure
hold on
plot(ModelF.listNodes{2}.position(1,timeLoc),ModelF.listNodes{2}.position(2,timeLoc),ModelF.listNodes{end}.position(1,timeLoc),ModelF.listNodes{end}.position(2,timeLoc), 'Linewidth',3)
plot(trajcart,zeros(size(trajcart)),trajx,trajy, 'Linewidth',1, 'Color','r')
grid on


figure
hold on
plot(timeSteps,ModelF.listNodes{2}.position(1,timeLoc),timeSteps,ModelF.listNodes{end}.position(1,timeLoc),timeSteps,ModelF.listNodes{end}.position(2,timeLoc))
plot(timeSteps,trajcart,'--',timeSteps,trajx,'--',timeSteps,trajy,'--')
legend('cart', 'X', 'Y','cartd','Xd','Yd')
grid on

figure
hold on
plot(timeSteps,u1,timeSteps,u2,timeSteps,u3,'Linewidth',3)
plot(timeSteps,u1_init(timeLoc),':',timeSteps,u2_init(timeLoc),':',timeSteps,u3_init(timeLoc),':','Linewidth',3)
xlabel('Time (s)','Fontsize',16)
ylabel('Commands','Fontsize',16)
legend('u1','u2','u3','u1_{rigid}','u2_{rigid}','u3_{rigid}','Location','Best')
title('Commands of a flexible cart system','Fontsize',18)
grid on

% save('CartBeamD','D')
% save('CartBeamS','S')

