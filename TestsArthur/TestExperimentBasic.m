%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear

finaltime = 2;
timestepsize = 0.01;
t_i = 0.2;
t_f = 1.8;

nodes = [1 0 0 0;
         2 0 0 0;
         3 -0.3 0 0];
     
nElem = 5;
Start = 2;
End = 3;
nodes = createInterNodes(nodes,nElem,Start,End);
% nodes = doubleNode(nodes,nodes(end,1));


a = 0.002;
b = 0.01;
l = 0.3;
rho = 2700;
m = a*b*l*rho;
Ixx = m*(a^2+b^2)/12;
Iyy = m*(a^2+l^2)/12;
Izz = m*(b^2+l^2)/12;
count = 1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [3 2 4];
% elements{count}.mass = m; % 0.01*0.002*0.4*2700 = 0.0216
% % elements{count}.J = diag([Ixx Iyy Izz]);
% elements{count}.J = 0.0001*eye(3);
% elements{count}.g = [0 0 -9.81];
% count = count +1;

circular = false;
if ~circular
% square
%     a = 0.002;
%     b = 0.01;
    E = 70000e9; nu = 0.3; G = E/(2*(1+nu)); rho = 2700;
    A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;
else
% circular
    d = 0.005;
    E = 70000e9; nu = 0.3; G = E/(2*(1+nu)); rho = 2700;
    A = pi*d^2/4; I = pi*d^4/64; J = 2*I;
end

KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

type = 'FlexibleBeamElement';
Start = 2;
End = nodes(end,1);
nElem = End-Start;
if exist('elements')== 1
    count = size(elements,2);
else
    count = 0;
end
for i = 1:nElem
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start+i-1,1) nodes(Start+i,1)];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
end

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = 0.0616; % 0.03*0.019*0.04*2700
elements{count}.g = [0 0 -9.81];
count = count +1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 1 0]';
count = count+1;

% Trajectory

x_end = 0.3;
z_end = nodes(end,4);
r = 0.3;

timeVector = 0:timestepsize:finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'sin',t_i,t_f);
trajy = nodes(end,3)*ones(size(timeVector));
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);


elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 0 1];
elements{count}.elements = [count-1];
elements{count}.active = 1;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

compute = 1;
if compute
    D = DynamicIntegration(Model);
    D.parameters.finaltime = finaltime;
    D.parameters.timestepsize = timestepsize;
    D.parameters.rho = 0.0;
    % D.parameters.relTolRes = 1e-10;
    D.parameters.scaling = 1e6;
    D.runIntegration();
else
    load('ArmBeamD')
    Model = D.model;
end

uRigiBeam.u1 = Model.listElementVariables{end}.value(1,:);
uRigiBeam.time = D.parameters.time;

% save('uArmRigiBeam','uRigiBeam')

% count = length(Model.listElements);
% Model.listElements{count}.active = 1;

for n = Model.listNumberNodes
    Model.listNodes{n}.InitializeD_Opti();
end
for n = Model.listNumberElementVariables
    Model.listElementVariables{n}.InitializeD_Opti();
end

if ~circular
% square
    E = 70e9; G = E/(2*(1+nu));
else
% circular
    E = 70e9; nu = 0.3; G = E/(2*(1+nu));
end
KCS = diag([E*A G*A G*A G*Ixx E*Ixx E*Ixx]);
for elem = Model.listElements
   if isa(elem{1},'FlexibleBeamElement')
       elem{1}.KCS = KCS;
   end
end

npts = 101;

timeVector = 0:finaltime/(npts-1):finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'sin',t_i,t_f);
trajy = nodes(end,3)*ones(size(timeVector));
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

Model.listElements{count}.T = [trajx;...
                               trajz];
tic
S = DirectTranscriptionOpti(Model);
% S.parameters.relTolRes = 1e-12;
S.parameters.rho = 0.0;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.NodeToMinimize = [3 4 5 6];
% S.JointToMinimize = [];
S.parameters.scaling = 1e6;
S.linConst = false;
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])

% Plots

timeSteps = S.timeValues;
timeLoc = S.timesteps;

uBeam.u1 = Model.listElementVariables{end}.value(1,timeLoc);
uBeam.time = S.timeValues;

% save('uArmBeam','uBeam')

endNode = nodes(end,1);
figure
hold on
plot3(Model.listNodes{endNode}.position(1,timeLoc),Model.listNodes{endNode}.position(2,timeLoc),Model.listNodes{endNode}.position(3,timeLoc), 'Linewidth',3)
plot3(trajx,trajy,trajz, 'Linewidth',1, 'Color','r')
grid on

figure
hold on
plot(timeSteps,Model.listNodes{endNode}.position(1,timeLoc),timeSteps,Model.listNodes{endNode}.position(2,timeLoc),timeSteps,Model.listNodes{endNode}.position(3,timeLoc))
plot(timeSteps,trajx,'--',timeSteps,trajy,'--',timeSteps,trajz,'--')
legend('X', 'Y', 'Z','Xd','Yd','Zd')
grid on

figure
hold on
plot(timeSteps,Model.listElementVariables{end}.value(1,timeLoc),'Linewidth',2)
plot(D.parameters.time,Model.listElementVariables{end}.value_InitOpti(1,:),':','Linewidth',2)
% plot(timeSteps,u1_init(timeLoc),':',timeSteps,u2_init(timeLoc),':',timeSteps,u3_init(timeLoc),':','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Commands','Fontsize',16)
legend('u1','u1_{rigid}','Location', 'Best')
title('Commands of a flexible Robot','Fontsize',18)
grid on

joint1_init = Model.listElementVariables{1}.relCoo_InitOpti;

joint1 = Model.listElementVariables{1}.relCoo(timeLoc);

figure
hold on
plot(timeSteps,joint1,'Linewidth',2)
plot(D.parameters.time,joint1_init,':','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints angle (rad)','Fontsize',16)
legend('Joint 1','Joint 1 rigid')
title('Joints angle of a flexible Robot','Fontsize',18)
grid on

joint1 = Model.listElementVariables{1};

figure
hold on
plot(timeSteps,joint1.velocity(timeLoc),'Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints velocity (rad/s)','Fontsize',16)
legend('Joint 1')
title('Joints velocity','Fontsize',18)
grid on

figure
hold on
plot(timeSteps,joint1.acceleration(timeLoc),'Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints accelerations (rad/s^2)','Fontsize',16)
legend('Joint 1')
title('Joints accelerations','Fontsize',18)
grid on

u1 = Model.listElementVariables{end}.value(1,timeLoc);
v1 = Model.listElementVariables{1}.velocity(timeLoc);
pos1 = Model.listElementVariables{1}.relCoo(timeLoc);

% file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestServo\TestServo\posIn.txt','w');
% file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestControl1\TestControl1\posIn.txt','w');
file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestControl2\TestControl2\posIn.txt','w');
for i = 1:length(pos1)
    fprintf(file,'%10.10f \n',pos1(i));
end
fclose(file);

% file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestServo\TestServo\velocityIn.txt','w');
% file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestControl1\TestControl1\velocityIn.txt','w');
file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestControl2\TestControl2\velocityIn.txt','w');
for i = 1:length(v1)
    fprintf(file,'%10.10f \n',v1(i));
end
fclose(file);

% file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestServo\TestServo\torqueIn.txt','w');
% file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestControl1\TestControl1\torqueIn.txt','w');
file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestControl2\TestControl2\torqueIn.txt','w');
for i = 1:length(u1)
    fprintf(file,'%10.10f \n',u1(i));
end
fclose(file);
% dlmwrite('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestServo\TestServo\torqueIn.txt',u1);
% dlmwrite('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestServo\TestServo\velocityIn.txt',v1);
% dlmwrite('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestServo\TestServo\posIn.txt',pos1);



