%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear

finaltime = 1.5;
timestepsize = 0.01;
t_i = 0.2;
t_f = 1.2;

nodes = [1 0 0 -1;
         2 0 0 -1;
         3 0 0 0;
         4 0 0 0;
         5 0 sqrt(2)/2 sqrt(2)/2;
         6 0 sqrt(2)/2 sqrt(2)/2;
         7 0 sqrt(2) 0];
     
nElem = 2;
Start = 2;
End = 3;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 5;
End = 6;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 6;
Start = 8;
End = 9;
nodes = createInterNodes(nodes,nElem,Start,End);

% elements{1}.type = 'RigidBodyElement';
% elements{1}.nodes = [3 2 4];
% elements{1}.mass = 2;
% elements{2}.J = 0.6*eye(3);
%      
% elements{2}.type = 'RigidBodyElement';
% elements{2}.nodes = [6 5 7];
% elements{2}.mass = 2;
% elements{2}.J = 0.6*eye(3);
% 
% elements{3}.type = 'RigidBodyElement';
% elements{3}.nodes = [9 8 10];
% elements{3}.mass = 0.5;
% elements{3}.J = 0.2*eye(3);

d = 0.05;
m1 = 2;
l1 = 1;
I1 = (m1/4)*(d^2/4 + l1^2/3);
J1 = 0.5*m1*(d/2)^2;

m2 = m1;
l2 = l1;
I2 = (m2/4)*(d^2/4 + l2^2/3);
J2 = 0.5*m2*(d/2)^2;

m3 = 0.5;
l3 = 0.25;
I3 = (m3/4)*(d^2/4 + l3^2/3);
J3 = 0.5*m3*(d/2)^2;

elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [3 2 4];
elements{1}.mass = 2;
elements{1}.J = diag([J1 I1 I1]);
% elements{1}.g = [0 0 -9.81];
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [6 5 7];
elements{2}.mass = m2;
elements{2}.J = diag([J2 I2 I2]);
% elements{2}.g = [0 0 -9.81];

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [9 8 10];
elements{3}.mass = m3;
elements{3}.J = diag([J3 I3 I3]);
% elements{3}.g = [0 0 -9.81];

circular = true;
if ~circular
% square
    a = 0.005;
    E = 210e9; nu = 0.33; G = E/(2*(1+nu)); rho = 7900;
    A = a^2; I = a^4/12; J = 2*I;
else
% circular
    d = 0.005;
    E = 20e9; nu = 0.3; G = E/(2*(1+nu)); rho = 2700;
    A = pi*d^2/4; I = pi*d^4/64; J = 2*I;
end

KCS = diag([E*A G*A G*A G*J E*I E*I]);
MCS = diag(rho*[A A A J I I]);

type = 'FlexibleBeamElement';
Start = 10;
End = nodes(end,1);
nElem = End-Start;
count = size(elements,2);
for i = 1:nElem
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start+i-1,1) nodes(Start+i,1)];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
end

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 0 1]';
count = count+1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [4 5];
elements{count}.A = [0 0 0 1 0 0]';
count = count+1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [7 8];
elements{count}.A = [0 0 0 1 0 0]';
count = count+1;

t = 0:timestepsize:finaltime;

load('uArmRigiBeam')
u1_init = interp1(uRigiBeam.time,uRigiBeam.u1,t,'linear');
u2_init = interp1(uRigiBeam.time,uRigiBeam.u2,t,'linear');
u3_init = interp1(uRigiBeam.time,uRigiBeam.u3,t,'linear');
% u1 = u1_init;
% u2 = u2_init;
% u3 = u3_init;

load('uArmBeam')
u1 = interp1(uBeam.time,uBeam.u1,t,'linear');
u2 = interp1(uBeam.time,uBeam.u2,t,'linear');
u3 = interp1(uBeam.time,uBeam.u3,t,'linear');
% u1 = smooth(u1);
% u2 = smooth(u2);
% u3 = smooth(u3);

% Input torques
elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = [count-3];
elements{count}.f = u1;
count = count+1;

elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = [count-3];
elements{count}.f = u2;
count = count+1;

elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = [count-3];
elements{count}.f = u3;

% % TrajParam.points = [0 sqrt(2) 0;...
% %                     0 sqrt(2) 0.2;...
% %                     sqrt(2)/4 3*sqrt(2)/4 0.5;...
% %                     sqrt(2)/2 sqrt(2)/2 0.2;...
% %                     sqrt(2)/2 sqrt(2)/2 -.1];
%                 
% TrajParam.points = [0 sqrt(2) 0;...
%                     0 sqrt(2) 0.2;...
%                     1 1 0.4;...
%                     sqrt(2) 0 0.2;...
%                     sqrt(2) 0 -.1];
% TrajParam.tau = 0.05;                    
% TrajParam.timeVector = 0:timestepsize:finaltime;
% % TrajParam.intervals = [0.2 0.5 1.2 1.8 2.3];
% TrajParam.intervals = [0.2 0.4 1 1.6 1.8];
% 
% % [trajx trajy trajz] = PointTraj(TrajParam);
% [trajx trajy trajz] = PointTrajCurve(TrajParam);

x_end = 1;
z_end = nodes(end,4);
r = 0.5;

timeVector = 0:timestepsize:finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'sin',t_i,t_f);
trajy = nodes(end,3)*ones(size(timeVector));
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

Modelr = FEModel();
Modelr.CreateFEModel(nodes,elements);
Modelr.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.1;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

Modelr.listElements{11}.f = u1_init;
Modelr.listElements{12}.f = u2_init;
Modelr.listElements{13}.f = u3_init;

Dr = DynamicIntegration(Modelr);
Dr.parameters.finaltime = finaltime;
Dr.parameters.timestepsize = timestepsize;
Dr.parameters.rho = 0.1;
% Dr.parameters.relTolRes = 1e-12;
Dr.runIntegration();

% Plots

timeSteps = D.parameters.time;

endNode = nodes(end,1);

x = Model.listNodes{endNode}.position(1,:);
y = Model.listNodes{endNode}.position(2,:);
z = Model.listNodes{endNode}.position(3,:);

xr = Modelr.listNodes{endNode}.position(1,:);
yr = Modelr.listNodes{endNode}.position(2,:);
zr = Modelr.listNodes{endNode}.position(3,:);

% figure
% hold on
% plot3(x,y,z, 'Linewidth',3)
% plot3(trajx,trajy,trajz, 'Linewidth',1, 'Color','r')
% grid on

figure
hold on
plot3(x,y,z, '-o','Linewidth',2, 'Color','r')
plot3(xr,yr,zr,'--', 'Linewidth',2, 'Color','b')
plot3(trajx,trajy,trajz, 'Linewidth',2, 'Color','k')
xlabel('X [m]','Fontsize',16)
ylabel('Y [m]','Fontsize',16)
zlabel('Z [m]','Fontsize',16)
view([0 -1 0])
legend('With u','With u_{rigid}','Prescribed','Location','South')
% title('Trajectory of the cart','Fontsize',18)
grid on

figure
hold on
plot(timeSteps,x,timeSteps,y,timeSteps,z)
plot(timeSteps,trajx,'--',timeSteps,trajy,'--',timeSteps,trajz,'--')
legend('X', 'Y', 'Z','Xd','Yd','Zd')
grid on

figure
hold on
% title('Commands of a 3D flexible pick & place manipulator')
plot(timeSteps,u1,timeSteps,u2,timeSteps,u3,'Linewidth',2)
plot(timeSteps,u1_init,'--',timeSteps,u2_init,'--',timeSteps,u3_init,'--','Linewidth',2)
legend('u1','u2','u3','u1_{rigid}','u2_{rigid}','u3_{rigid}','Location', 'Best')
xlabel('Time [s]','Fontsize',16)
ylabel('Commands [Nm]','Fontsize',16)
grid on

% Error plot

RelError = zeros(size(timeSteps));
RelErrorR = zeros(size(timeSteps));
for i = 1:length(RelError)
    RelError(i) = 100*(norm([trajx(i)-x(i) trajy(i)-y(i) trajz(i)-z(i)]))/norm([trajx(i) trajy(i) trajz(i)]);
    RelErrorR(i) = 100*(norm([trajx(i)-xr(i) trajy(i)-yr(i) trajz(i)-zr(i)]))/norm([trajx(i) trajy(i) trajz(i)]);
end
figure
plot(timeSteps,RelError,timeSteps,RelErrorR)
title('Relative error of the direct dynamic trajectory','Fontsize',13)
xlabel('time')
ylabel('Relative Error (%)')
legend('with u','with u_{rigid}')
grid on

RMSRelError = sqrt(mean(RelError.^2))
MaxRelError = max(RelError)
RMSRelErrorR = sqrt(mean(RelErrorR.^2))
MaxRelErrorR = max(RelErrorR)

% figure
% hold on
% title('Joints angle of 3D flexible arm')
% plot(timeSteps,Model.listElementVariables{1}.relCoo,timeSteps,Model.listElementVariables{2}.relCoo,timeSteps,Model.listElementVariables{3}.relCoo)
% plot(timeSteps,Modelr.listElementVariables{1}.relCoo,'--',timeSteps,Modelr.listElementVariables{2}.relCoo,'--',timeSteps,Modelr.listElementVariables{3}.relCoo,'--')
% xlabel('Time [s]')
% ylabel('Angle [rad]')

joint1_init = Modelr.listElementVariables{1}.relCoo;
joint2_init = Modelr.listElementVariables{2}.relCoo;
joint3_init = Modelr.listElementVariables{3}.relCoo;

joint1 = Model.listElementVariables{1}.relCoo;
joint2 = Model.listElementVariables{2}.relCoo;
joint3 = Model.listElementVariables{3}.relCoo;

figure
hold on
plot(timeSteps,joint1,timeSteps,joint2,timeSteps,joint3,'Linewidth',2)
plot(timeSteps,joint1_init,':',timeSteps,joint2_init,':',timeSteps,joint3_init,':','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints angle (rad)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid')
title('Joints angle of a flexible Robot','Fontsize',18)
grid on


