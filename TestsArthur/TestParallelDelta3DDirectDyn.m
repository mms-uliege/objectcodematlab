%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test parallel manipulator with hinge joints
clear all
% close all

finaltime = 1.5;% 1.5 or 1
timestepsize = 0.01;
t_i = 0.2;% 0.2
t_f = 1.3;% 1.2 or 0.7

%% Creating nodes
sideTriUp = 0.25; % side length of the upper triangle
lengthUpperArm = 0.25; % length of the first rigid upper arm (linked to hinge)
l1 = sideTriUp/(2*cos(pi/6)); % distance between center and a corner of the upper triangle
l2 = sideTriUp*tan(pi/6)/2; % distance between center and middle of edge of the upper triangle
sideTriUpperArm = 2*(l1 + lengthUpperArm)*cos(pi/6); % side length of the triangle formed with the outer node of the upper arms
l2bis = (l1 + lengthUpperArm)*sin(pi/6); % distance between center and midle edge of the triangle formed by the outer node of upper arms.
sideTriLow = 0.1; % side length of lower triangle
height = 0.7; % height of the robot
nodes = [1 l1 0 0; %                                                UPPER TRIANGLE
         2 -l2 -sideTriUp/2 0;
         3 -l2 sideTriUp/2 0;
         4 l1 0 0; % tips of 1st upper arm                          UPPER ARMS WITH HINGES
         5 l1+lengthUpperArm 0 0; % tips of 1st upper arm
         6 -l2 -sideTriUp/2 0; % tips of 2nd upper arm
         7 -l2bis -sideTriUpperArm/2 0; % tips of 2nd upper arm
         8 -l2 sideTriUp/2 0; % tips of 3rd upper arm
         9 -l2bis sideTriUpperArm/2 0; % tips of 3rd upper arm
         10 l1+lengthUpperArm 0 0; % tips of 1st lower arm          LOWER ARMS WIITH 2DOF JOINTS
         11 sideTriLow/(2*cos(pi/6)) 0 -height; % tips of 1st lower arm
         12 -l2bis -sideTriUpperArm/2 0; % tips of 2nd lower arm
         13 -sideTriLow*tan(pi/6)/2 -sideTriLow/2 -height; % tips of 2nd lower arm
         14 -l2bis sideTriUpperArm/2 0; % tips of 3rd lower arm
         15 -sideTriLow*tan(pi/6)/2 sideTriLow/2 -height; % tips of 3rd lower arm
         16 sideTriLow/(2*cos(pi/6)) 0 -height; %                   LOWER TRIANGLE
         17 -sideTriLow*tan(pi/6)/2 -sideTriLow/2 -height;
         18 -sideTriLow*tan(pi/6)/2 sideTriLow/2 -height;
         19 0 0 -height];

nElem1 = 4; % even number !
Start1 = 10;
End1 = Start1 + 1;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

nElem2 = nElem1; % even number !
Start2 = End1+nElem1;
End2 = Start2 + 1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);

nElem3 = nElem1; % even number !
Start3 = End2+nElem2;
End3 = Start3 + 1;
nodes = createInterNodes(nodes,nElem3,Start3,End3);

vec1 = nodes(Start1,2:4)/norm(nodes(Start1,2:4));
vec2 = nodes(Start2,2:4)/norm(nodes(Start2,2:4));
vec3 = nodes(Start3,2:4)/norm(nodes(Start3,2:4));

hinge1 = cross(vec1,[0 0 1]);
hinge2 = cross(vec2,[0 0 1]);
hinge3 = cross(vec3,[0 0 1]);

vec1bis = (nodes(Start1,2:4)-nodes(Start1+nElem1-1,2:4))/norm(nodes(Start1,2:4)-nodes(Start1+nElem1-1,2:4));
vec2bis = (nodes(Start2,2:4)-nodes(Start2+nElem2-1,2:4))/norm(nodes(Start2,2:4)-nodes(Start2+nElem2-1,2:4));
vec3bis = (nodes(Start3,2:4)-nodes(Start3+nElem3-1,2:4))/norm(nodes(Start3,2:4)-nodes(Start3+nElem3-1,2:4));

joint2Dof1 = cross(vec1bis,hinge1);
joint2Dof2 = cross(vec2bis,hinge2);
joint2Dof3 = cross(vec3bis,hinge3);
 
%% Flexible Model
m_end = 0.1; % end effector load [kg]
b = sideTriLow;
h = sideTriLow*cos(pi/6);
IxxR = m_end*2*(h*(b/2)^3)/12;
IyyR = m_end*(b*h^3)/36;
IzzR = m_end*((-h*b^3/4 + b*h^3)/36);

rho = 2700;

a1R = 0.02; % first arm param
b1R = 0.02;
l1R = lengthUpperArm;
a1 = 0.005;
b1 = 0.005;
l1 = norm(nodes(Start1,2:4)-nodes(Start1+nElem1-1,2:4));
m1 = a1R*b1R*l1R*rho;
IxxR1 = m1*(a1R^2+b1R^2)/12;
IyyR1 = m1*(a1R^2+l1R^2)/12;
IzzR1 = m1*(b1R^2+l1R^2)/12;
m1bis = a1*b1*l1*rho;
Ixx1 = m1bis*(a1^2+b1^2)/12;
Iyy1 = m1bis*(a1^2+l1^2)/12;
Izz1 = m1bis*(b1^2+l1^2)/12;

a2R = a1R; % second arm param
b2R = b1R;
l2R = lengthUpperArm;
a2 = a1;
b2 = b1;
l2 = norm(nodes(Start2,2:4)-nodes(Start2+nElem2-1,2:4));
m2 = a2R*b2R*l2R*rho;
IxxR2 = m2*(a2R^2+b2R^2)/12;
IyyR2 = m2*(a2R^2+l2R^2)/12;
IzzR2 = m2*(b2R^2+l2R^2)/12;
m2bis = a2*b2*l2*rho;
Ixx2 = m2bis*(a2^2+b2^2)/12;
Iyy2 = m2bis*(a2^2+l2^2)/12;
Izz2 = m2bis*(b2^2+l2^2)/12;

a3R = a1R; % third arm param
b3R = b1R;
l3R = lengthUpperArm;
a3 = a1;
b3 = b1;
l3 = norm(nodes(Start3,2:4)-nodes(Start3+nElem3-1,2:4));
m3 = a3R*b3R*l3R*rho;
IxxR3 = m3*(a3R^2+b3R^2)/12;
IyyR3 = m3*(a3R^2+l3R^2)/12;
IzzR3 = m3*(b3R^2+l3R^2)/12;
m3bis = a3*b3*l3*rho;
Ixx3 = m3bis*(a3^2+b3^2)/12;
Iyy3 = m3bis*(a3^2+l3^2)/12;
Izz3 = m3bis*(b3^2+l3^2)/12;

E = 70e9; nu = 0.3; G = E/(2*(1+nu));
A1 = a1*b1; Ixx1 = a1*b1*(a1^2+b1^2)/12; Iyy1 = b1*a1^3/12;Izz1 = a1*b1^3/12;

KCS1 = diag([E*A1 G*A1 G*A1 G*Ixx1 E*Iyy1 E*Izz1]);
MCS1 = diag(rho*[A1 A1 A1 Ixx1 Iyy1 Izz1]);

A2 = a2*b2; Ixx2 = a2*b2*(a2^2+b2^2)/12; Iyy2 = b2*a2^3/12;Izz2 = a2*b2^3/12;

KCS2 = diag([E*A2 G*A2 G*A2 G*Ixx2 E*Iyy2 E*Izz2]);
MCS2 = diag(rho*[A2 A2 A2 Ixx2 Iyy2 Izz2]);

A3 = a3*b3; Ixx3 = a3*b3*(a3^3+b3^2)/12; Iyy3 = b3*a3^3/12;Izz3 = a3*b3^3/12;

KCS3 = diag([E*A3 G*A3 G*A3 G*Ixx3 E*Iyy3 E*Izz3]);
MCS3 = diag(rho*[A3 A3 A3 Ixx3 Iyy3 Izz3]);

type = 'FlexibleBeamElement';


count = 1;
elements{count}.type = 'RigidBodyElement'; % end effector
elements{count}.nodes = [nodes(end,1) nodes(end-3:end-1,1)'];
elements{count}.mass = m_end;
elements{count}.J = diag([IxxR IyyR IzzR]);
% elements{count}.g = [0 0 -9.81];

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement'; % 1st upper arm
elements{count}.nodes = [5 4];
elements{count}.mass = m1;
elements{count}.J = diag([IxxR1 IyyR1 IzzR1]);
% elements{count}.g = [0 0 -9.81];

count = size(elements,2); % 1st lower arm (flexible)
for i = 1:nElem1
    elements{count+i}.type = type;
    elements{count+i}.nodes = [Start1+i-1 Start1+i];
    elements{count+i}.KCS = KCS1;
    elements{count+i}.MCS = MCS1;
end
% count = size(elements,2)+1;
% elements{count}.type = 'RigidBodyElement'; % 1st lower arm
% elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
% elements{count}.mass = m1bis;
% elements{count}.J = diag([Ixx1 Iyy1 Izz1]);
% % elements{count}.g = [0 0 -9.81];

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement'; % 2nd upper arm
elements{count}.nodes = [7 6];
elements{count}.mass = m2;
elements{count}.J = diag([IxxR2 IyyR2 IzzR2]);
% elements{count}.g = [0 0 -9.81];

count = size(elements,2); % 2nd lower arm (flexible)
for i = 1:nElem2
    elements{count+i}.type = type;
    elements{count+i}.nodes = [Start2+i-1 Start2+i];
    elements{count+i}.KCS = KCS2;
    elements{count+i}.MCS = MCS2;
end
% count = size(elements,2)+1;
% elements{count}.type = 'RigidBodyElement'; % 2nd lower arm
% elements{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
% elements{count}.mass = m2bis;
% elements{count}.J = diag([Ixx2 Iyy2 Izz2]);
% % elements{count}.g = [0 0 -9.81];

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement'; % 3rd upper arm
elements{count}.nodes = [9 8];
elements{count}.mass = m3;
elements{count}.J = diag([IxxR3 IyyR3 IzzR3]);
% elements{count}.g = [0 0 -9.81];

count = size(elements,2); % 3rd lower arm (flexible)
for i = 1:nElem3
    elements{count+i}.type = type;
    elements{count+i}.nodes = [Start3+i-1 Start3+i];
    elements{count+i}.KCS = KCS3;
    elements{count+i}.MCS = MCS3;
end
% count = size(elements,2)+1;
% elements{count}.type = 'RigidBodyElement'; % 3rd lower arm
% elements{count}.nodes = [Start3+nElem3/2 Start3+[0:nElem3/2-1] Start3+nElem3/2+1+[0:nElem3/2-1]];
% elements{count}.mass = m3bis;
% elements{count}.J = diag([Ixx3 Iyy3 Izz3]);
% % elements{count}.g = [0 0 -9.81];

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % first upper 2 dof joint
elements{count}.nodes = [5 Start1];
elements{count}.A = [0 0 0 joint2Dof1; 0 0 0 hinge1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % second upper 2 dof joint
elements{count}.nodes = [7 Start2];
elements{count}.A = [0 0 0 joint2Dof2; 0 0 0 hinge2]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % third upper 2 dof joint
elements{count}.nodes = [9 Start3];
elements{count}.A = [0 0 0 joint2Dof3; 0 0 0 hinge3]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % first lower 2 dof joint
elements{count}.nodes = [Start1+nElem1 nodes(end-3,1)];
elements{count}.A = [0 0 0 joint2Dof1; 0 0 0 hinge1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % second lower 2 dof joint
elements{count}.nodes = [Start2+nElem2 nodes(end-2,1)];
elements{count}.A = [0 0 0 joint2Dof2; 0 0 0 hinge2]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % third lower 2 dof joint
elements{count}.nodes = [Start3+nElem3 nodes(end-1,1)];
elements{count}.A = [0 0 0 joint2Dof3; 0 0 0 hinge3]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % first hinge joint
elements{count}.nodes = [1 4];
elements{count}.A = [0 0 0 hinge1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % second hinge joint
elements{count}.nodes = [2 6];
elements{count}.A = [0 0 0 hinge2]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % third hinge joint
elements{count}.nodes = [3 8];
elements{count}.A = [0 0 0 hinge3]';

% Trajectory

x_end = nodes(end,2);
y_end = sideTriLow*2;
z_end = nodes(end,4);
r = y_end/2;

timeVector = 0:timestepsize:finaltime;
% trajx = nodes(end,2)*ones(size(timeVector));
% trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
% trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'cos',t_i,t_f);
trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
trajz = nodes(end,4)*ones(size(timeVector));

load('uRigiDelta3D')
u1_init = interp1(uRigi.time,uRigi.u1,timeVector,'linear');
u2_init = interp1(uRigi.time,uRigi.u2,timeVector,'linear');
u3_init = interp1(uRigi.time,uRigi.u3,timeVector,'linear');

load('uDelta3D')
u1 = interp1(uBeam.time,uBeam.u1,timeVector,'linear');
u2 = interp1(uBeam.time,uBeam.u2,timeVector,'linear');
u3 = interp1(uBeam.time,uBeam.u3,timeVector,'linear');

count = size(elements,2)+1;
elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = [count-3];
elements{count}.f = u1;

count = size(elements,2)+1;
elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = [count-3];
elements{count}.f = u2;

count = size(elements,2)+1;
elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = [count-3];
elements{count}.f = u3;

% Boundary Condition
BC = [1 2 3];

% Model Def
Model = FEModel();% Flex model
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

ModelR = FEModel();% Rigid model
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

% Solving Flexible
D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.01;
D.parameters.scaling = 1e6;
D.runIntegration();

% Solving Rigid
ModelR.listElements{count-2}.f = u1_init;
ModelR.listElements{count-1}.f = u2_init;
ModelR.listElements{count}.f = u3_init;

DR = DynamicIntegration(ModelR);
DR.parameters.finaltime = finaltime;
DR.parameters.timestepsize = timestepsize;
DR.parameters.rho = 0.01;
DR.parameters.scaling = 1e6;
DR.runIntegration();


%% Plots
x = Model.listNodes{end}.position(1,:);
y = Model.listNodes{end}.position(2,:);
z = Model.listNodes{end}.position(3,:);

xR = ModelR.listNodes{end}.position(1,:);
yR = ModelR.listNodes{end}.position(2,:);
zR = ModelR.listNodes{end}.position(3,:);


figure
hold on
plot3(x,y,z, '-o','Linewidth',2, 'Color','r')
plot3(xR,yR,zR,'--', 'Linewidth',2, 'Color','b')
plot3(trajx,trajy,trajz, 'Linewidth',2, 'Color','k')
legend('With u','With u_{rigid}','Prescribed')
xlabel('X [m]','Fontsize',16)
ylabel('Y [m]','Fontsize',16)
zlabel('Z [m]','Fontsize',16)
% title('Trajectory of flexible arm','Fontsize',18)
grid on

figure
hold on
plot(timeVector,x,timeVector,y,timeVector,z)
plot(timeVector,trajx,'--',timeVector,trajy,'--',timeVector,trajz,'--')
legend('X', 'Y', 'Z','Xd','Yd','Zd')
grid on

joint1_init = ModelR.listElementVariables{7};
joint2_init = ModelR.listElementVariables{8};
joint3_init = ModelR.listElementVariables{9};

joint1 = Model.listElementVariables{7};
joint2 = Model.listElementVariables{8};
joint3 = Model.listElementVariables{9};

figure
hold on
plot(timeVector,joint1.relCoo,timeVector,joint2.relCoo,timeVector,joint3.relCoo,'Linewidth',2)
plot(timeVector,joint1_init.relCoo,'--',timeVector,joint2_init.relCoo,'--',timeVector,joint3_init.relCoo,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints angle (rad)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% title('Joints angle of a flexible Robot','Fontsize',18)
grid on

figure
hold on
plot(timeVector,u1,timeVector,u2,timeVector,u3,'Linewidth',2)
plot(timeVector,u1_init(:),'--',timeVector,u2_init(:),'--',timeVector,u3_init(:),'--','Linewidth',2)
xlabel('Time [s]','Fontsize',16)
ylabel('Commands [Nm]','Fontsize',16)
legend('u_1','u_2','u_3','u_{1,rigid}','u_{2,rigid}','u_{3,rigid}','Location','Best')
% title('Commands of a flexible arm system','Fontsize',18)
grid on

RelError = zeros(size(timeVector));
RelErrorR = zeros(size(timeVector));
for i = 1:length(RelError)
    RelError(i) = 100*(norm([trajx(i)-x(i) trajy(i)-y(i) trajz(i)-z(i)])/norm([trajx(i) trajy(i) trajz(i)]));
    RelErrorR(i) = 100*(norm([trajx(i)-xR(i) trajy(i)-yR(i) trajz(i)-zR(i)])/norm([trajx(i) trajy(i) trajz(i)]));
end
figure
plot(timeVector,RelError,timeVector,RelErrorR)
title('Relative error of the direct dynamic trajectory','Fontsize',13)
xlabel('Time')
ylabel('Relative Error (%)')
legend('Flex','Rigid')
grid on

RMSRelError = sqrt(mean(RelError.^2))
MaxRelError = max(RelError)
RMSRelErrorR = sqrt(mean(RelErrorR.^2))
MaxRelErrorR = max(RelErrorR)

figure
hold on
plot(timeVector,joint1.velocity,timeVector,joint2.velocity,timeVector,joint3.velocity,'Linewidth',2)
plot(timeVector,joint1_init.velocity,'--',timeVector,joint2_init.velocity,'--',timeVector,joint3_init.velocity,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints velocity (rad/s)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid')
title('Joints velocity','Fontsize',18)
grid on

figure
hold on
plot(timeVector,joint1.acceleration,timeVector,joint2.acceleration,timeVector,joint3.acceleration,'Linewidth',2)
plot(timeVector,joint1_init.acceleration,'--',timeVector,joint2_init.acceleration,'--',timeVector,joint3_init.acceleration,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints accelerations (rad/s^2)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid')
title('Joints accelerations','Fontsize',18)
grid on



