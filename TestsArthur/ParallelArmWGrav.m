%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test parallel manipulator with hinge joints
clear
% close all

computeOpti = true; % see if we want to compute the inverse dynamics of flexible syst or not (if not, then the direct computation is then to see if, considering flexibility, the time integration works?)
computeDirectDyn = true;

finaltime = 1;% 1.5 or 1
timestepsize = 0.01;
t_i = 0.2;% 0.2
t_f = 0.8;% 1.2 or 0.7
rho_num = 0.01;
% hypForComp = 'notConstant';
hypForComp = 'constant';
listM = [1 1 1];
listKt = [1 1 1];
listCt = [1 1 1];

%% Arms parameters
% Dimensions of upper arm _u
a_u = 0.05;
b_u = a_u;
e_u = 0.005;
rapport = a_u/e_u;
% Dimensions of fore arm _f
a_f = 0.0075; % 0.0075 is unstable and works for timestepsize of 0.02s
b_f = a_f;
e_f = a_f/rapport;
a_fIn = a_f-2*e_f;
b_fIn = b_f-2*e_f;
a_uIn = a_u-2*e_u; % inner length
b_uIn = b_u-2*e_u;

% material parameters
E = 70e9; nu = 0.3; G = E/(2*(1+nu));
rho = 2700;

alpha = 0.0005; % damping proportional to mass
beta = 0.01; % damping proportional to stiffness

% considered gravity?
grav = [0 0 -9.81];
% grav = [0 0 0];
m_end = 0.1; % end effector load [kg]

%% Creating nodes
sideTriUp = 0.25; % side length of the upper triangle
lengthUpperArm = 0.25; % length of the first rigid upper arm (linked to hinge)
l1 = sideTriUp/(2*cos(pi/6)); % distance between center and a corner of the upper triangle
l2 = sideTriUp*tan(pi/6)/2; % distance between center and middle of edge of the upper triangle
sideTriUpperArm = 2*(l1 + lengthUpperArm)*cos(pi/6); % side length of the triangle formed with the outer node of the upper arms
l2bis = (l1 + lengthUpperArm)*sin(pi/6); % distance between center and midle edge of the triangle formed by the outer node of upper arms.
sideTriLow = 0.1; % side length of lower triangle
height = 0.7; % height of the robot 
nodes = [1 l1 0 0; %                                                UPPER TRIANGLE
         2 -l2 -sideTriUp/2 0;
         3 -l2 sideTriUp/2 0;
         4 l1 0 0; % tips of 1st upper arm                          UPPER ARMS WITH HINGES
         5 l1+lengthUpperArm 0 0; % tips of 1st upper arm
         6 -l2 -sideTriUp/2 0; % tips of 2nd upper arm
         7 -l2bis -sideTriUpperArm/2 0; % tips of 2nd upper arm
         8 -l2 sideTriUp/2 0; % tips of 3rd upper arm
         9 -l2bis sideTriUpperArm/2 0; % tips of 3rd upper arm
         10 l1+lengthUpperArm 0 0; % tips of 1st lower arm          LOWER ARMS WIITH 2DOF JOINTS
         11 sideTriLow/(2*cos(pi/6)) 0 -height; % tips of 1st lower arm
         12 -l2bis -sideTriUpperArm/2 0; % tips of 2nd lower arm
         13 -sideTriLow*tan(pi/6)/2 -sideTriLow/2 -height; % tips of 2nd lower arm
         14 -l2bis sideTriUpperArm/2 0; % tips of 3rd lower arm
         15 -sideTriLow*tan(pi/6)/2 sideTriLow/2 -height; % tips of 3rd lower arm
         16 sideTriLow/(2*cos(pi/6)) 0 -height; %                   LOWER TRIANGLE
         17 -sideTriLow*tan(pi/6)/2 -sideTriLow/2 -height;
         18 -sideTriLow*tan(pi/6)/2 sideTriLow/2 -height;
         19 0 0 -height];

nElem1 = 4; % even number !
Start1 = 10;
End1 = Start1 + 1;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

nElem2 = nElem1; % even number !
Start2 = End1+nElem1;
End2 = Start2 + 1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);

nElem3 = nElem1; % even number !
Start3 = End2+nElem2;
End3 = Start3 + 1;
nodes = createInterNodes(nodes,nElem3,Start3,End3);

vec1 = nodes(Start1,2:4)/norm(nodes(Start1,2:4));
vec2 = nodes(Start2,2:4)/norm(nodes(Start2,2:4));
vec3 = nodes(Start3,2:4)/norm(nodes(Start3,2:4));

hinge1 = cross(vec1,[0 0 1]);
hinge2 = cross(vec2,[0 0 1]);
hinge3 = cross(vec3,[0 0 1]);

vec1bis = (nodes(Start1,2:4)-nodes(Start1+nElem1-1,2:4))/norm(nodes(Start1,2:4)-nodes(Start1+nElem1-1,2:4));
vec2bis = (nodes(Start2,2:4)-nodes(Start2+nElem2-1,2:4))/norm(nodes(Start2,2:4)-nodes(Start2+nElem2-1,2:4));
vec3bis = (nodes(Start3,2:4)-nodes(Start3+nElem3-1,2:4))/norm(nodes(Start3,2:4)-nodes(Start3+nElem3-1,2:4));

joint2Dof1 = cross(vec1bis,hinge1);
joint2Dof2 = cross(vec2bis,hinge2);
joint2Dof3 = cross(vec3bis,hinge3);
 
%% Rigid Model
% End plate dimensions and param

b = sideTriLow;
h = sideTriLow*cos(pi/6);
IxxR = m_end*2*(h*(b/2)^3)/12;
IyyR = m_end*(b*h^3)/36;
IzzR = m_end*((-h*b^3/4 + b*h^3)/36);

a1U = a_u; % first arm param
a1U_In = a_uIn;
b1U = b_u;
b1U_In = b_uIn;
l1U = lengthUpperArm;
a1 = a_f;
a1_In = a_fIn;
b1 = b_f;
b1_In = b_fIn;
l1 = norm(nodes(Start1,2:4)-nodes(Start1+nElem1-1,2:4));
m1 = (a1U*b1U-a1U_In*b1U_In)*l1U*rho;                     % Upper arm
IxxU1 = m1*(a1U^2+b1U^2-a1U_In^2-b1U_In^2)/12;
IyyU1 = m1*(a1U^2+l1U^2)/12;
IzzU1 = m1*(b1U^2+l1U^2)/12;
m1bis = (a1*b1-a1_In*b1_In)*l1*rho;                       % Fore arm
IxxFR1 = m1bis*(a1^2+b1^2-a1_In^2-b1_In^2)/12;
IyyFR1 = m1bis*(a1^2+l1^2)/12;
IzzFR1 = m1bis*(b1^2+l1^2)/12;

a2U = a1U; % second arm param
a2U_In = a1U_In;
b2U = b1U;
b2U_In = b1U_In;
l2U = lengthUpperArm;
a2 = a1;
a2_In = a1_In;
b2 = b1;
b2_In = b1_In;
l2 = norm(nodes(Start2,2:4)-nodes(Start2+nElem2-1,2:4));
m2 = (a2U*b2U-a2U_In*b2U_In)*l2U*rho;                    % Upper arm
IxxU2 = m2*(a2U^2+b2U^2-a2U_In^2-b2U_In^2)/12;
IyyU2 = m2*(a2U^2+l2U^2)/12;
IzzU2 = m2*(b2U^2+l2U^2)/12;
m2bis = (a2*b2-a2_In*b2_In)*l2*rho;                      % Fore arm
IxxFR2 = m2bis*(a2^2+b2^2)/12;
IyyFR2 = m2bis*(a2^2+l2^2)/12;
IzzFR2 = m2bis*(b2^2+l2^2)/12;

a3U = a1U; % third arm param
a3U_In = a1U_In;
b3U = b1U;
b3U_In = b1U_In;
l3U = lengthUpperArm;
a3 = a1;
a3_In = a1_In;
b3 = b1;
b3_In = b1_In;
l3 = norm(nodes(Start3,2:4)-nodes(Start3+nElem3-1,2:4));
m3 = (a3U*b3U-a3U_In*b3U_In)*l3U*rho;                     % Upper arm
IxxU3 = m3*(a3U^2+b3U^2-a3U_In^2-b3U_In^2)/12;
IyyU3 = m3*(a3U^2+l3U^2)/12;
IzzU3 = m3*(b3U^2+l3U^2)/12;
m3bis = (a3*b3-a3_In*b3_In)*l3*rho;                       % Fore arm
IxxFR3 = m3bis*(a3^2+b3^2-a3_In^2-b3_In^2)/12;
IyyFR3 = m3bis*(a3^2+l3^2)/12;
IzzFR3 = m3bis*(b3^2+l3^2)/12;

count = 1;
elements{count}.type = 'RigidBodyElement'; % end effector
elements{count}.nodes = [nodes(end,1) nodes(end-3:end-1,1)'];
elements{count}.mass = m_end;
elements{count}.J = diag([IxxR IyyR IzzR]);
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement'; % 1st upper arm
elements{count}.nodes = [5 4];
elements{count}.mass = m1;
elements{count}.J = diag([IxxU1 IyyU1 IzzU1]);
elements{count}.g = grav;

if computeOpti == true
    count = size(elements,2)+1;
    elements{count}.type = 'RigidBodyElement'; % 1st lower arm
    elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
    elements{count}.mass = m1bis;
    elements{count}.J = diag([IxxFR1 IyyFR1 IzzFR1]);
    elements{count}.g = grav;
end
if computeOpti ~= true
    A1 = a1*b1-a1_In*b1_In; IxxF1 = (a1*b1*(a1^2+b1^2)-a1_In*b1_In*(a1_In^2+b1_In^2))/12; IyyF1 = (b1*a1^3-b1_In*a1_In^3)/12;IzzF1 = (a1*b1^3-a1_In*b1_In^3)/12;

    KCS1 = diag([E*A1 G*A1 G*A1 G*IxxF1 E*IyyF1 E*IzzF1]);
    MCS1 = diag(rho*[A1 A1 A1 IxxF1 IyyF1 IzzF1]);
type = 'FlexibleBeamElement';
count = size(elements,2);
for i = 1:nElem1
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start1+i-1,1) nodes(Start1+i,1)];
    elements{count+i}.KCS = KCS1;
    elements{count+i}.MCS = MCS1;
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.alpha = alpha; % damping proportional to Mass
    elements{count+i}.beta = beta; % damping proportional to Stiffness
    elements{count+i}.g = grav;
end
end

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement'; % 2nd upper arm
elements{count}.nodes = [7 6];
elements{count}.mass = m2;
elements{count}.J = diag([IxxU2 IyyU2 IzzU2]);
elements{count}.g = grav;

if computeOpti == true
    count = size(elements,2)+1;
    elements{count}.type = 'RigidBodyElement'; % 2nd lower arm
    elements{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
    elements{count}.mass = m2bis;
    elements{count}.J = diag([IxxFR2 IyyFR2 IzzFR2]);
    elements{count}.g = grav;
end
if computeOpti ~= true
    A2 = a2*b2-a2_In*b2_In; IxxF2 = (a2*b2*(a2^2+b2^2)-a2_In*b2_In*(a2_In^2+b2_In^2))/12; IyyF2 = (b2*a2^3-b2_In*a2_In^3)/12;IzzF2 = (a2*b2^3-a2_In*b2_In^3)/12;

    KCS2 = diag([E*A2 G*A2 G*A2 G*IxxF2 E*IyyF2 E*IzzF2]);
    MCS2 = diag(rho*[A2 A2 A2 IxxF2 IyyF1 IzzF1]);
type = 'FlexibleBeamElement';
count = size(elements,2);
for i = 1:nElem2
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
    elements{count+i}.KCS = KCS2;
    elements{count+i}.MCS = MCS2;
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.alpha = alpha; % damping proportional to Mass
    elements{count+i}.beta = beta; % damping proportional to Stiffness
    elements{count+i}.g = grav;
end
end

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement'; % 3rd upper arm
elements{count}.nodes = [9 8];
elements{count}.mass = m3;
elements{count}.J = diag([IxxU3 IyyU3 IzzU3]);
elements{count}.g = grav;

if computeOpti == true
    count = size(elements,2)+1;
    elements{count}.type = 'RigidBodyElement'; % 3rd lower arm
    elements{count}.nodes = [Start3+nElem3/2 Start3+[0:nElem3/2-1] Start3+nElem3/2+1+[0:nElem3/2-1]];
    elements{count}.mass = m3bis;
    elements{count}.J = diag([IxxFR3 IyyFR3 IzzFR3]);
    elements{count}.g = grav;
end
if computeOpti ~= true
    A3 = a3*b3-a3_In*b3_In; IxxF3 = (a3*b3*(a3^2+b3^2)-a3_In*b3_In*(a3_In^2+b3_In^2))/12; IyyF3 = (b3*a3^3-b3_In*a3_In^3)/12;IzzF3 = (a3*b3^3-a3_In*b3_In^3)/12;

    KCS3 = diag([E*A3 G*A3 G*A3 G*IxxF3 E*IyyF3 E*IzzF3]);
    MCS3 = diag(rho*[A3 A3 A3 IxxF3 IyyF3 IzzF3]);
type = 'FlexibleBeamElement';
count = size(elements,2);
for i = 1:nElem2
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start3+i-1,1) nodes(Start3+i,1)];
    elements{count+i}.KCS = KCS3;
    elements{count+i}.MCS = MCS3;
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.alpha = alpha; % damping proportional to Mass
    elements{count+i}.beta = beta; % damping proportional to Stiffness
    elements{count}.g = grav;
end
end

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % first upper 2 dof joint
elements{count}.nodes = [5 Start1];
elements{count}.A = [0 0 0 joint2Dof1; 0 0 0 hinge1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % second upper 2 dof joint
elements{count}.nodes = [7 Start2];
elements{count}.A = [0 0 0 joint2Dof2; 0 0 0 hinge2]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % third upper 2 dof joint
elements{count}.nodes = [9 Start3];
elements{count}.A = [0 0 0 joint2Dof3; 0 0 0 hinge3]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % first lower 2 dof joint
elements{count}.nodes = [Start1+nElem1 nodes(end-3,1)];
elements{count}.A = [0 0 0 joint2Dof1; 0 0 0 hinge1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % second lower 2 dof joint
elements{count}.nodes = [Start2+nElem2 nodes(end-2,1)];
elements{count}.A = [0 0 0 joint2Dof2; 0 0 0 hinge2]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % third lower 2 dof joint
elements{count}.nodes = [Start3+nElem3 nodes(end-1,1)];
elements{count}.A = [0 0 0 joint2Dof3; 0 0 0 hinge3]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % first hinge joint
elements{count}.nodes = [1 4];
elements{count}.A = [0 0 0 hinge1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % second hinge joint
elements{count}.nodes = [2 6];
elements{count}.A = [0 0 0 hinge2]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % third hinge joint
elements{count}.nodes = [3 8];
elements{count}.A = [0 0 0 hinge3]';

% Trajectory

x_end = nodes(end,2);
y_end = sideTriLow*2;
z_end = nodes(end,4);
r = y_end/2;

timeVector = 0:timestepsize:finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'cos',t_i,t_f);
trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
trajz = nodes(end,4)*ones(size(timeVector));

count = size(elements,2)+1;
elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajy;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elements{count}.elements = [count-3 count-2 count-1];
elements{count}.active = 1;

% Boundary Condition
BC = [1 2 3];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

uRigi.u1 = ModelR.listElementVariables{end}.value(1,:);
uRigi.u2 = ModelR.listElementVariables{end}.value(2,:);
uRigi.u3 = ModelR.listElementVariables{end}.value(3,:);
uRigi.time = D.parameters.time;

if computeOpti~=true
    timeVector = 0:timestepsize:finaltime;
    u1_init = interp1(uRigi.time,uRigi.u1,timeVector,'linear');
    u2_init = interp1(uRigi.time,uRigi.u2,timeVector,'linear');
    u3_init = interp1(uRigi.time,uRigi.u3,timeVector,'linear');
    
    figure
    plot(timeVector,u1_init(:),'--',timeVector,u2_init(:),'--',timeVector,u3_init(:),'--','Linewidth',2)
    xlabel('Time [s]','Fontsize',16)
    ylabel('Commands [Nm]','Fontsize',16)
    legend('u_{1,rigid}','u_{2,rigid}','u_{3,rigid}','Location','Best')
    % title('Commands of a flexible arm system','Fontsize',18)
    grid on
end

%% Flexible Model
if computeOpti == true

A1 = a1*b1-a1_In*b1_In; IxxF1 = (a1*b1*(a1^2+b1^2)-a1_In*b1_In*(a1_In^2+b1_In^2))/12; IyyF1 = (b1*a1^3-b1_In*a1_In^3)/12;IzzF1 = (a1*b1^3-a1_In*b1_In^3)/12;

KCS1 = diag([E*A1 G*A1 G*A1 G*IxxF1 E*IyyF1 E*IzzF1]);
MCS1 = diag(rho*[A1 A1 A1 IxxF1 IyyF1 IzzF1]);

A2 = a2*b2-a2_In*b2_In; IxxF2 = (a2*b2*(a2^2+b2^2)-a2_In*b2_In*(a2_In^2+b2_In^2))/12; IyyF2 = (b2*a2^3-b2_In*a2_In^3)/12;IzzF2 = (a2*b2^3-a2_In*b2_In^3)/12;

KCS2 = diag([E*A2 G*A2 G*A2 G*IxxF2 E*IyyF2 E*IzzF2]);
MCS2 = diag(rho*[A2 A2 A2 IxxF2 IyyF2 IzzF2]);

A3 = a3*b3-a3_In*b3_In; IxxF3 = (a3*b3*(a3^2+b3^2)-a3_In*b3_In*(a3_In^2+b3_In^2))/12; IyyF3 = (b3*a3^3-b3_In*a3_In^3)/12;IzzF3 = (a3*b3^3-a3_In*b3_In^3)/12;

KCS3 = diag([E*A3 G*A3 G*A3 G*IxxF3 E*IyyF3 E*IzzF3]);
MCS3 = diag(rho*[A3 A3 A3 IxxF3 IyyF3 IzzF3]);

type = 'FlexibleBeamElement';

count = 1;
elementsFlex{count}.type = 'RigidBodyElement'; % end effector
elementsFlex{count}.nodes = [nodes(end,1) nodes(end-3:end-1,1)'];
elementsFlex{count}.mass = m_end;
elementsFlex{count}.J = diag([IxxR IyyR IzzR]);
elementsFlex{count}.g = grav;

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'RigidBodyElement'; % 1st upper arm
elementsFlex{count}.nodes = [5 4];
elementsFlex{count}.mass = m1;
elementsFlex{count}.J = diag([IxxU1 IyyU1 IzzU1]);
elementsFlex{count}.g = grav;

count = size(elementsFlex,2); % 1st lower arm (flexible)
for i = 1:nElem1
    elementsFlex{count+i}.type = type;
    elementsFlex{count+i}.nodes = [Start1+i-1 Start1+i];
    elementsFlex{count+i}.KCS = KCS1;
    elementsFlex{count+i}.MCS = MCS1;
    elements{count+i}.alpha = alpha; % damping proportional to Mass
    elements{count+i}.beta = beta; % damping proportional to Stiffness
    elementsFlex{count+i}.g = grav;
end

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'RigidBodyElement'; % 2nd upper arm
elementsFlex{count}.nodes = [7 6];
elementsFlex{count}.mass = m2;
elementsFlex{count}.J = diag([IxxU2 IyyU2 IzzU2]);
elementsFlex{count}.g = grav;

count = size(elementsFlex,2); % 2nd lower arm (flexible)
for i = 1:nElem2
    elementsFlex{count+i}.type = type;
    elementsFlex{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
    elementsFlex{count+i}.KCS = KCS2;
    elementsFlex{count+i}.MCS = MCS2;
    elements{count+i}.alpha = alpha; % damping proportional to Mass
    elements{count+i}.beta = beta; % damping proportional to Stiffness
    elementsFlex{count+i}.g = grav;
end

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'RigidBodyElement'; % 3rd upper arm
elementsFlex{count}.nodes = [9 8];
elementsFlex{count}.mass = m3;
elementsFlex{count}.J = diag([IxxU3 IyyU3 IzzU3]);
elementsFlex{count}.g = grav;

count = size(elementsFlex,2); % 3rd lower arm (flexible)
for i = 1:nElem3
    elementsFlex{count+i}.type = type;
    elementsFlex{count+i}.nodes = [nodes(Start3+i-1,1) nodes(Start3+i,1)];
    elementsFlex{count+i}.KCS = KCS3;
    elementsFlex{count+i}.MCS = MCS3;
    elements{count+i}.alpha = alpha; % damping proportional to Mass
    elements{count+i}.beta = beta; % damping proportional to Stiffness
    elementsFlex{count}.g = grav;
end

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint'; % first upper 2 dof joint
elementsFlex{count}.nodes = [5 Start1];
elementsFlex{count}.A = [0 0 0 joint2Dof1; 0 0 0 hinge1]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint'; % second upper 2 dof joint
elementsFlex{count}.nodes = [7 Start2];
elementsFlex{count}.A = [0 0 0 joint2Dof2; 0 0 0 hinge2]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint'; % third upper 2 dof joint
elementsFlex{count}.nodes = [9 Start3];
elementsFlex{count}.A = [0 0 0 joint2Dof3; 0 0 0 hinge3]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint'; % first lower 2 dof joint
elementsFlex{count}.nodes = [Start1+nElem1 nodes(end-3,1)];
elementsFlex{count}.A = [0 0 0 joint2Dof1; 0 0 0 hinge1]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint'; % second lower 2 dof joint
elementsFlex{count}.nodes = [Start2+nElem2 nodes(end-2,1)];
elementsFlex{count}.A = [0 0 0 joint2Dof2; 0 0 0 hinge2]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint'; % third lower 2 dof joint
elementsFlex{count}.nodes = [Start3+nElem3 nodes(end-1,1)];
elementsFlex{count}.A = [0 0 0 joint2Dof3; 0 0 0 hinge3]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint'; % first hinge joint
elementsFlex{count}.nodes = [1 4];
elementsFlex{count}.A = [0 0 0 hinge1]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint'; % second hinge joint
elementsFlex{count}.nodes = [2 6];
elementsFlex{count}.A = [0 0 0 hinge2]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint'; % third hinge joint
elementsFlex{count}.nodes = [3 8];
elementsFlex{count}.A = [0 0 0 hinge3]';

% Trajectory

npts = finaltime/timestepsize + 1;

timeVector = 0:finaltime/(npts-1):finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'cos',t_i,t_f);
trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
trajz = nodes(end,4)*ones(size(timeVector));

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'TrajectoryConstraint';
elementsFlex{count}.nodes = [nodes(end,1)];
elementsFlex{count}.T = [trajx;...
                     trajy;...
                     trajz];
elementsFlex{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elementsFlex{count}.elements = [count-3 count-2 count-1];
elementsFlex{count}.active = 1;

ModelF = FEModel();
ModelF.CreateFEModel(nodes,elementsFlex);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from rigid solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

% First part optimization to compute the equilibrium solution for the first
% preactuation phase. Then copy this preactuation phase, to have full initial guess equilibrium to compute full
% optimization.
tic
% EndPreActStep = t_i/(finaltime/(npts-1))+1;
% EndPreActStep = round((t_i/(finaltime/(npts-1))+1)*0.47);
EndPreTime = t_i*0.75;%0.75
EndPreActStep = round(EndPreTime/(finaltime/(npts-1))+1);
S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = rho_num;
S.npts = EndPreActStep+1;
S.parameters.finaltime = EndPreTime;
S.parameters.timestepsize = finaltime/(npts-1);
S.parameters.scaling = 1e6;
S.ConstIter = hypForComp;
S.linConst = false;
xSol = S.runOpti(D);

% Copy the state at the end of preactuation phase to start as new initial
% guess.
% Redefine the trajectory that was cut to only the preactuation phase
ModelF.listElements{end}.T = [trajx;...
                              trajy;...
                              trajz];

for n = ModelF.listNumberNodes
    for step = 1:EndPreActStep-1
        ModelF.listNodes{n}.R_InitOpti(:,step) = ModelF.listNodes{n}.R(:,EndPreActStep);
        ModelF.listNodes{n}.position_InitOpti(:,step) = ModelF.listNodes{n}.position(:,EndPreActStep);
        ModelF.listNodes{n}.velocity_InitOpti(:,step) = ModelF.listNodes{n}.velocity(:,EndPreActStep);
        ModelF.listNodes{n}.acceleration_InitOpti(:,step) = ModelF.listNodes{n}.acceleration(:,EndPreActStep);
    end
%     ModelF.listNodes{n}.InitializeD_Opti();
end
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        for step = 1:EndPreActStep-1
            ModelF.listElementVariables{n}.R_InitOpti(:,step) = ModelF.listElementVariables{n}.R(:,EndPreActStep);
            ModelF.listElementVariables{n}.x_InitOpti(:,step) = ModelF.listElementVariables{n}.x(:,EndPreActStep);
            ModelF.listElementVariables{n}.velocity_InitOpti(:,step) = ModelF.listElementVariables{n}.velocity(:,EndPreActStep);
            ModelF.listElementVariables{n}.acceleration_InitOpti(:,step) = ModelF.listElementVariables{n}.acceleration(:,EndPreActStep);
            ModelF.listElementVariables{n}.relCoo_InitOpti(:,step) = ModelF.listElementVariables{n}.relCoo(:,EndPreActStep);
        end
    else
        for step = 1:EndPreActStep-1
            ModelF.listElementVariables{n}.value_InitOpti(:,step) = ModelF.listElementVariables{n}.value(:,EndPreActStep); 
        end
    end
%     ModelF.listElementVariables{n}.InitializeD_Opti();
end

% Start second full optimization with newly computed initial guess

S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = rho_num;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.parameters.scaling = 1e6;
S.ConstIter = hypForComp;
S.linConst = false;
S.ConstIter = 'notConstant';
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])

%% Plots

timeSteps = S.timeValues;
timeLoc = S.timesteps;
timeRigi = D.parameters.time;

uBeam.u1 = ModelF.listElementVariables{end}.value(1,timeLoc);
uBeam.u2 = ModelF.listElementVariables{end}.value(2,timeLoc);
uBeam.u3 = ModelF.listElementVariables{end}.value(3,timeLoc);
uBeam.time = S.timeValues;

endNode = nodes(end,1);

joint1_init = ModelR.listElementVariables{7};
joint2_init = ModelR.listElementVariables{8};
joint3_init = ModelR.listElementVariables{9};

joint1 = ModelF.listElementVariables{7};
joint2 = ModelF.listElementVariables{8};
joint3 = ModelF.listElementVariables{9};

figure
hold on
plot(timeSteps,joint1.relCoo(timeLoc),timeSteps,joint2.relCoo(timeLoc),timeSteps,joint3.relCoo(timeLoc),'Linewidth',2)
plot(timeRigi,joint1_init.relCoo,'--',timeRigi,joint2_init.relCoo,'--',timeRigi,joint3_init.relCoo,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints position (rad)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% legend('Joint 1','Joint 2','Joint 3','Location', 'Best')
grid on

figure
hold on
plot(timeSteps,joint1.velocity(timeLoc),timeSteps,joint2.velocity(timeLoc),timeSteps,joint3.velocity(timeLoc),'Linewidth',2)
plot(timeRigi,joint1_init.velocity,'--',timeRigi,joint2_init.velocity,'--',timeRigi,joint3_init.velocity,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints velocities (rad/s)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% legend('Joint 1','Joint 2','Joint 3','Location', 'Best')
grid on

figure
hold on
plot(timeSteps,joint1.acceleration(timeLoc),timeSteps,joint2.acceleration(timeLoc),timeSteps,joint3.acceleration(timeLoc),'Linewidth',2)
plot(timeRigi,joint1_init.acceleration,'--',timeRigi,joint2_init.acceleration,'--',timeRigi,joint3_init.acceleration,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints acceleration (rad/s^2)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% legend('Joint 1','Joint 2','Joint 3','Location', 'Best')
grid on

anl = poleAnalysisOpti(S)
end
%% Direct dynamics computation
if computeDirectDyn == true

count = 1;
elementsDirDyn{count}.type = 'RigidBodyElement'; % end effector
elementsDirDyn{count}.nodes = [nodes(end,1) nodes(end-3:end-1,1)'];
elementsDirDyn{count}.mass = m_end;
elementsDirDyn{count}.J = diag([IxxR IyyR IzzR]);
elementsDirDyn{count}.g = grav;

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'RigidBodyElement'; % 1st upper arm
elementsDirDyn{count}.nodes = [5 4];
elementsDirDyn{count}.mass = m1;
elementsDirDyn{count}.J = diag([IxxU1 IyyU1 IzzU1]);
elementsDirDyn{count}.g = grav;

count = size(elementsDirDyn,2); % 1st lower arm (flexible)
for i = 1:nElem1
    elementsDirDyn{count+i}.type = type;
    elementsDirDyn{count+i}.nodes = [Start1+i-1 Start1+i];
    elementsDirDyn{count+i}.KCS = KCS1;
    elementsDirDyn{count+i}.MCS = MCS1;
    elements{count+i}.alpha = alpha; % damping proportional to Mass
    elements{count+i}.beta = beta; % damping proportional to Stiffness
    elementsDirDyn{count+i}.g = grav;
end

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'RigidBodyElement'; % 2nd upper arm
elementsDirDyn{count}.nodes = [7 6];
elementsDirDyn{count}.mass = m2;
elementsDirDyn{count}.J = diag([IxxU2 IyyU2 IzzU2]);
elementsDirDyn{count}.g = grav;

count = size(elementsDirDyn,2); % 2nd lower arm (flexible)
for i = 1:nElem2
    elementsDirDyn{count+i}.type = type;
    elementsDirDyn{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
    elementsDirDyn{count+i}.KCS = KCS2;
    elementsDirDyn{count+i}.MCS = MCS2;
    elements{count+i}.alpha = alpha; % damping proportional to Mass
    elements{count+i}.beta = beta; % damping proportional to Stiffness
    elementsDirDyn{count+i}.g = grav;
end

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'RigidBodyElement'; % 3rd upper arm
elementsDirDyn{count}.nodes = [9 8];
elementsDirDyn{count}.mass = m3;
elementsDirDyn{count}.J = diag([IxxU3 IyyU3 IzzU3]);
elementsDirDyn{count}.g = grav;

count = size(elementsDirDyn,2); % 3rd lower arm (flexible)
for i = 1:nElem3
    elementsDirDyn{count+i}.type = type;
    elementsDirDyn{count+i}.nodes = [nodes(Start3+i-1,1) nodes(Start3+i,1)];
    elementsDirDyn{count+i}.KCS = KCS3;
    elementsDirDyn{count+i}.MCS = MCS3;
    elements{count+i}.alpha = alpha; % damping proportional to Mass
    elements{count+i}.beta = beta; % damping proportional to Stiffness
    elementsDirDyn{count}.g = grav;
end

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'KinematicConstraint'; % first upper 2 dof joint
elementsDirDyn{count}.nodes = [5 Start1];
elementsDirDyn{count}.A = [0 0 0 joint2Dof1; 0 0 0 hinge1]';

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'KinematicConstraint'; % second upper 2 dof joint
elementsDirDyn{count}.nodes = [7 Start2];
elementsDirDyn{count}.A = [0 0 0 joint2Dof2; 0 0 0 hinge2]';

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'KinematicConstraint'; % third upper 2 dof joint
elementsDirDyn{count}.nodes = [9 Start3];
elementsDirDyn{count}.A = [0 0 0 joint2Dof3; 0 0 0 hinge3]';

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'KinematicConstraint'; % first lower 2 dof joint
elementsDirDyn{count}.nodes = [Start1+nElem1 nodes(end-3,1)];
elementsDirDyn{count}.A = [0 0 0 joint2Dof1; 0 0 0 hinge1]';

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'KinematicConstraint'; % second lower 2 dof joint
elementsDirDyn{count}.nodes = [Start2+nElem2 nodes(end-2,1)];
elementsDirDyn{count}.A = [0 0 0 joint2Dof2; 0 0 0 hinge2]';

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'KinematicConstraint'; % third lower 2 dof joint
elementsDirDyn{count}.nodes = [Start3+nElem3 nodes(end-1,1)];
elementsDirDyn{count}.A = [0 0 0 joint2Dof3; 0 0 0 hinge3]';

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'KinematicConstraint'; % first hinge joint
elementsDirDyn{count}.nodes = [1 4];
elementsDirDyn{count}.A = [0 0 0 hinge1]';

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'KinematicConstraint'; % second hinge joint
elementsDirDyn{count}.nodes = [2 6];
elementsDirDyn{count}.A = [0 0 0 hinge2]';

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'KinematicConstraint'; % third hinge joint
elementsDirDyn{count}.nodes = [3 8];
elementsDirDyn{count}.A = [0 0 0 hinge3]';
count = count+1;

timeVector = 0:timestepsize:finaltime;
u1_init = interp1(uRigi.time,uRigi.u1,timeVector,'linear');
u2_init = interp1(uRigi.time,uRigi.u2,timeVector,'linear');
u3_init = interp1(uRigi.time,uRigi.u3,timeVector,'linear');

u1 = interp1(uBeam.time,uBeam.u1,timeVector,'linear');
u2 = interp1(uBeam.time,uBeam.u2,timeVector,'linear');
u3 = interp1(uBeam.time,uBeam.u3,timeVector,'linear');

elementsDirDyn{count}.type = 'ForceInKinematicConstraint';
elementsDirDyn{count}.elements = [count-3];
elementsDirDyn{count}.f = u1;
count = count+1;

elementsDirDyn{count}.type = 'ForceInKinematicConstraint';
elementsDirDyn{count}.elements = [count-3];
elementsDirDyn{count}.f = u2;
count = count+1;

elementsDirDyn{count}.type = 'ForceInKinematicConstraint';
elementsDirDyn{count}.elements = [count-3];
elementsDirDyn{count}.f = u3;

% Boundary Condition
BC = [1 2 3];

% Model Def
ModelFD = FEModel();% Flex model
ModelFD.CreateFEModel(nodes,elementsDirDyn);
ModelFD.defineBC(BC);

ModelRD = FEModel();% Rigid model
ModelRD.CreateFEModel(nodes,elementsDirDyn);
ModelRD.defineBC(BC);

% Solving Flexible
D = DynamicIntegration(ModelFD);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
D.parameters.scaling = 1e6;
D.runIntegration();

% Solving Rigid
ModelRD.listElements{count-2}.f = u1_init;
ModelRD.listElements{count-1}.f = u2_init;
ModelRD.listElements{count}.f = u3_init;

DR = DynamicIntegration(ModelRD);
DR.parameters.finaltime = finaltime;
DR.parameters.timestepsize = timestepsize;
DR.parameters.rho = rho_num;
DR.parameters.scaling = 1e6;
DR.runIntegration();
    
x = ModelFD.listNodes{end}.position(1,:);
y = ModelFD.listNodes{end}.position(2,:);
z = ModelFD.listNodes{end}.position(3,:);

xR = ModelRD.listNodes{end}.position(1,:);
yR = ModelRD.listNodes{end}.position(2,:);
zR = ModelRD.listNodes{end}.position(3,:);
   
joint1_init = ModelRD.listElementVariables{7};
joint2_init = ModelRD.listElementVariables{8};
joint3_init = ModelRD.listElementVariables{9};

joint1 = ModelFD.listElementVariables{7};
joint2 = ModelFD.listElementVariables{8};
joint3 = ModelFD.listElementVariables{9};

figure
hold on
plot(timeVector,joint1.relCoo,timeVector,joint2.relCoo,timeVector,joint3.relCoo,'Linewidth',2)
plot(timeVector,joint1_init.relCoo,'--',timeVector,joint2_init.relCoo,'--',timeVector,joint3_init.relCoo,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints angle (rad)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% title('Joints angle of a flexible Robot','Fontsize',18)
grid on

figure
hold on
plot(timeVector,u1,timeVector,u2,timeVector,u3,'Linewidth',2)
plot(timeVector,u1_init(:),'--',timeVector,u2_init(:),'--',timeVector,u3_init(:),'--','Linewidth',2)
xlabel('Time [s]','Fontsize',16)
ylabel('Commands [Nm]','Fontsize',16)
legend('u_1','u_2','u_3','u_{1,rigid}','u_{2,rigid}','u_{3,rigid}','Location','Best')
% title('Commands of a flexible arm system','Fontsize',18)
grid on

figure
hold on
plot3(x,y,z, '-o','Linewidth',2, 'Color','r')
plot3(xR,yR,zR,'--', 'Linewidth',2, 'Color','b')
plot3(trajx,trajy,trajz, 'Linewidth',2, 'Color','k')
legend('With u','With u_{rigid}','Prescribed')
xlabel('X [m]','Fontsize',16)
ylabel('Y [m]','Fontsize',16)
zlabel('Z [m]','Fontsize',16)
% title('Trajectory of flexible arm','Fontsize',18)
grid on

RelError = zeros(size(timeVector));
RelErrorR = zeros(size(timeVector));
for i = 1:length(RelError)
    RelError(i) = 100*(norm([trajx(i)-x(i) trajy(i)-y(i) trajz(i)-z(i)])/norm([trajx(i) trajy(i) trajz(i)]));
    RelErrorR(i) = 100*(norm([trajx(i)-xR(i) trajy(i)-yR(i) trajz(i)-zR(i)])/norm([trajx(i) trajy(i) trajz(i)]));
end
%     figure
%     plot(timeVector,RelError,timeVector,RelErrorR)
%     title('Relative error of the direct dynamic trajectory','Fontsize',13)
%     xlabel('Time')
%     ylabel('Relative Error (%)')
%     legend('Flex','Rigid')
%     grid on

RMSRelError = sqrt(mean(RelError.^2))
MaxRelError = max(RelError)
RMSRelErrorR = sqrt(mean(RelErrorR.^2))
MaxRelErrorR = max(RelErrorR)
    

figure
hold on
plot(timeVector,joint1.velocity,timeVector,joint2.velocity,timeVector,joint3.velocity,'Linewidth',2)
plot(timeVector,joint1_init.velocity,'--',timeVector,joint2_init.velocity,'--',timeVector,joint3_init.velocity,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints velocity (rad/s)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid')
title('Joints velocity','Fontsize',18)
grid on

figure
hold on
plot(timeVector,joint1.acceleration,timeVector,joint2.acceleration,timeVector,joint3.acceleration,'Linewidth',2)
plot(timeVector,joint1_init.acceleration,'--',timeVector,joint2_init.acceleration,'--',timeVector,joint3_init.acceleration,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints accelerations (rad/s^2)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid')
title('Joints accelerations','Fontsize',18)
grid on
    
    
end
