%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% direct dynamics of the flexible cart system to check the eigen frequency
% of the system. An "impulse" is applied to the end effector to check the
% response of the system.

clear all
close all

finaltime = 2;
timestepsize = 0.01;
t_i = 0.2;
t_f = 1.7;
time = 0:timestepsize:finaltime;

rho_num = 0.3;
scaling = 1e6;
tol = 1e-6;

nodes = [1 -1 0 0;
         2 -1 0 0;
         3 -1 0 0;
         4 -1.6614 -0.75 0;
         5 -1.6614 -0.75 0;
         6 -1.3307 -1.125 0;
         7 -1.3307 -1.125 0;
         8 -1 -1.5 0];

nodes = createInterNodes(nodes,2,3,4);
nodes = createInterNodes(nodes,2,6,7);
nodes = createInterNodes(nodes,2,9,10);

elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2];
elements{1}.mass = 3;
% elements{1}.J = eye(3);

elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [4 3 5];
elements{2}.mass = 6.875;
elements{2}.J = 0.57*eye(3);
% elements{2}.g = [0 9.81 0];

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [7 6 8];
elements{3}.mass = 6.875/2;
elements{3}.J = 0.0723*eye(3);
% elements{3}.g = [0 9.81 0];

elements{4}.type = 'RigidBodyElement';
elements{4}.nodes = [10 9 11];
elements{4}.mass = 6.875/2;
elements{4}.J = 0.0723*eye(3);
% elements{4}.g = [0 9.81 0];

elements{5}.type = 'KinematicConstraint';
elements{5}.nodes = [1 2];
elements{5}.A = 0*[1 0 0 0 0 0]';

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [2 3];
elements{6}.A = 0*[0 0 0 0 0 1]';

elements{7}.type = 'KinematicConstraint';
elements{7}.nodes = [5 6];
elements{7}.A = 0*[0 0 0 0 0 1]';

elements{8}.type = 'KinematicConstraint';
elements{8}.nodes = [8 9];
% elements{8}.d = 0*0.25; % 40 (31 commence a instable)
% elements{8}.k = 50; % 50
elements{8}.A = [0 0 0 0 0 1]';

elements{9}.type = 'RotSpringDamperElement';
elements{9}.damping = 0.25; % 40 (31 commence a instable)
elements{9}.stiffness = 50; % 50
elements{9}.nodes = [8 9];
elements{9}.A = [0 0 1];

s = round(finaltime*0.05/timestepsize);
e = round(s+5);
A = 30;
f = zeros(size(time));
f(s:e) = A*ones(1,e-s+1);
elements{10}.type = 'punctualForce';
elements{10}.nodes = [nodes(end,1)];
elements{10}.f = f;
elements{10}.DOF = [1];


BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
D.parameters.scaling = scaling;
D.parameters.relTolRes = tol;
D.runIntegration();

% Plot
figure
subplot(311)
plot(time, Model.listNodes{end}.position(1,:))
ylabel('x')
grid on
subplot(312)
plot(time, Model.listNodes{end}.position(2,:))
ylabel('y')
grid on
subplot(313)
plot(time, f)
ylabel('force')
xlabel('Time [s]')
grid on



