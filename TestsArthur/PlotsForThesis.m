%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plots for thesis
close all

h = 0.01;
ti = 0;
tf = 1;
timeVector = ti:h:tf;

% 7th order polynomial
% traj7pol = halfCircleTraj(0,1,0.5,timeVector,'sin',ti,tf);
traj7pol = lineTraj(0,1,timeVector,ti,tf);

figure
plot(timeVector,traj7pol,'-o','Linewidth',2)
grid on
xlabel('Time','Fontsize',16)
ylabel('$\rm\bf{s}$','Interpreter','Latex','Fontsize',16)
% axis([0 1 -0.1 1.1])


figure
plot(timeVector,deriveVal(traj7pol,h),'-o','Linewidth',2)
grid on
xlabel('Time','Fontsize',16)
ylabel('$\rm\bf\dot{s}$','Interpreter','Latex','Fontsize',16)
% axis([0 1 0 2.3])

figure
plot(timeVector,deriveVal(deriveVal(traj7pol,h),h),'-o','Linewidth',2)
grid on
xlabel('Time','Fontsize',16)
ylabel('$\rm\bf\ddot{s}$','Interpreter','Latex','Fontsize',16)
% axis([0 1 -8 8])

figure
plot(timeVector,deriveVal(deriveVal(deriveVal(traj7pol,h),h),h),'-o','Linewidth',2)
grid on
xlabel('Time','Fontsize',16)
ylabel('$\rm\bf{s}^{[3]}$','Interpreter','Latex','Fontsize',16)
% axis([0 1 -16 16])

figure
plot(timeVector,deriveVal(deriveVal(deriveVal(deriveVal(traj7pol,h),h),h),h),'-o','Linewidth',2)
grid on
xlabel('Time','Fontsize',16)
ylabel('$s^{[4]}$','Interpreter','Latex','Fontsize',16)
% axis([0 1 -16 16])

figure
plot(timeVector,deriveVal(deriveVal(deriveVal(deriveVal(deriveVal(traj7pol,h),h),h),h),h),'-o','Linewidth',2)
grid on
xlabel('Time','Fontsize',16)
ylabel('$s^{[5]}$','Interpreter','Latex','Fontsize',16)
% axis([0 1 -16 16])
