%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test inverse dynamics of a serial manipulator (2 links, 3 dof).
% considering gravity, with a initial guess that is the deformed state.
% Initial guess computed staticaly and nor rigid model to start with
% With possibility to add some PD feedback on joint position level

% Model used for Multibody system Dynamics journal with some tests to
% improve direct dynamics result

clear all
% close all

computeOpti = true;
computeDirectDyn = true;
% computeDirectDyn = false;

finaltime = 2.1;
timestepsize = 0.01;
t_i = 0.8;
t_f = finaltime-0.2;
timeShift = 0.6;
    
% kp1 = 10;
% kd1 = 1;
% kp2 = 10;
% kd2 = 1;
% kp3 = 10;
% kd3 = 1;
kp1 = 0;
kd1 = 0;
kp2 = 0;
kd2 = 0;
kp3 = 0;
kd3 = 0;

hypForPreComp = 'notConstant';
hypForComp = 'notConstant';
% hypForComp = 'constant';

a1 = 0.05;
b1 = a1;
l1 = 1;
l2 = l1; 
e1 = 0.01;
rapport = a1/e1;
a2 = 0.0075;
b2 = a2;
e2 = a2/rapport;
a2In = a2-2*e2;
b2In = b2-2*e2;
a1In = a1-2*e1; % inner length
b1In = b1-2*e1;

alpha = 0*0.0001; % Damping proportionnal to mass
beta = 0*0.01; % Damping proportionnal to stiffness
rho_num = 0.0;
scaling = 1e6;
tol = 1e-6;
% listM = [1 0 0];
% listKt = [1 0 0];
% listCt = [1 0 0];
% listPhiq = [1 0 0];
% listM = [1 1 1];
% listKt = [1 1 1];
% listCt = [1 1 1];
% listPhiq = [1 1 1];

nPass = 0;

grav = [0 0 -9.81];
% grav = [0 0 0];
% t = 0:timestepsize:finaltime;
% grav = [zeros(1,length(t)); zeros(1,length(t)); [0 0 0 -9.81*ones(1,length(t)-3)]]';

m_end = 0.1; % end-effector mass

%% Creating nodes
angle = 45*pi/180;
nodes = [1 0 0 0;
         2 0 0 0;
         3 0 0 0;
         4 l1*cos(angle) 0 l1*sin(angle);
         5 l1*cos(angle) 0 l1*sin(angle);
         6 l1*cos(angle)+l2*cos(angle) 0 0];
     
nElem1 = 2;
Start1 = 3;
End1 = 4;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

nElem2 = 4;
Start2 = End1+nElem1;
End2 = Start2+1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);
 
%% Rigid Model

rho = 2700;
m1 = (a1*b1-a1In*b1In)*l1*rho;
% IxxR1 = m1*(a1^2+b1^2)/12;
IxxR1 = m1*(a1^2+b1^2-a1In^2-b1In^2)/12;
IyyR1 = m1*(a1^2+l1^2)/12;
IzzR1 = m1*(b1^2+l1^2)/12;

m2 = (a2*b2-a2In*b2In)*l2*rho;
IxxR2 = m2*(a2^2+b2^2-a2In^2-b2In^2)/12;
IyyR2 = m2*(a2^2+l2^2)/12;
IzzR2 = m2*(b2^2+l2^2)/12;

count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elements{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR1 IyyR1 IzzR1]);
elements{count}.g = grav;
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;
count = count +1;

% if computeOpti == true
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
elements{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR2 IyyR2 IzzR2]);
elements{count}.g = grav;
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;
% end

E = 70e9; nu = 0.3; G = E/(2*(1+nu));
% if computeOpti ~= true
A2 = a2*b2-a2In*b2In; Ixx2 = (a2*b2*(a2^2+b2^2)-a2In*b2In*(a2In^2+b2In^2))/12; Iyy2 = (b2*a2^3-b2In*a2In^3)/12;Izz2 = (a2*b2^3-a2In*b2In^3)/12;

KCS2 = diag([E*A2 G*A2 G*A2 G*Ixx2 E*Iyy2 E*Izz2]);
MCS2 = diag(rho*[A2 A2 A2 Ixx2 Iyy2 Izz2]);

% type = 'FlexibleBeamElement';
% count = size(elements,2);
% for i = 1:nElem2
%     elements{count+i}.type = type;
%     elements{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
%     elements{count+i}.KCS = KCS2;
%     elements{count+i}.MCS = MCS2;
%     elements{count+i}.yAxis = [0 1 0];
%     elements{count+i}.listM = listM;
%     elements{count+i}.listCt = listCt;
%     elements{count+i}.listKt = listKt;
%     elements{count+i}.listPhiq = listPhiq;
%     elements{count+i}.alpha = alpha;
%     elements{count+i}.beta = beta;
%     elements{count+i}.g = grav;
% end
% % end

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = m_end;
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.A = [0 0 0 0 1 0]';
count = count+1;

% Trajectory

y_end = l2;
z_end = nodes(end,4);
r = y_end/2;

timeVector = 0:timestepsize:finaltime;
trajx = nodes(end,2)*ones(size(timeVector));
trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);
% trajy = nodes(end,3)*ones(size(timeVector));
% trajz = nodes(end,4)*ones(size(timeVector));


elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajy;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elements{count}.elements = [count-3 count-2 count-1];
elements{count}.active = 1;

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
% D = StaticIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
D.parameters.relTolRes = tol;
D.parameters.scaling = scaling;
D.runIntegration();

t = find(D.parameters.time==timeShift,1);
uRigi.u1 = ModelR.listElementVariables{end}.value(1,t:end);
uRigi.u2 = ModelR.listElementVariables{end}.value(2,t:end);
uRigi.u3 = ModelR.listElementVariables{end}.value(3,t:end);
uRigi.jointPos1 = ModelR.listElementVariables{1}.relCoo(t:end);
uRigi.jointPos2 = ModelR.listElementVariables{2}.relCoo(t:end);
uRigi.jointPos3 = ModelR.listElementVariables{3}.relCoo(t:end);
uRigi.jointVel1 = ModelR.listElementVariables{1}.velocity(t:end);
uRigi.jointVel2 = ModelR.listElementVariables{2}.velocity(t:end);
uRigi.jointVel3 = ModelR.listElementVariables{3}.velocity(t:end);
uRigi.time = 0:timestepsize:finaltime-timeShift;

%% Flexible Model
if computeOpti == true
count = 1;
elementsF{count}.type = 'RigidBodyElement';
elementsF{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elementsF{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
elementsF{count}.J = diag([IxxR1 IyyR1 IzzR1]);
elementsF{count}.g = grav;

type = 'FlexibleBeamElement';
count = size(elementsF,2);
for i = 1:nElem2
    elementsF{count+i}.type = type;
    elementsF{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
    elementsF{count+i}.KCS = KCS2;
    elementsF{count+i}.MCS = MCS2;
    elementsF{count+i}.yAxis = [0 0 1];
%     elementsF{count+i}.listM = listM;
%     elementsF{count+i}.listCt = listCt;
%     elementsF{count+i}.listKt = listKt;
%     elementsF{count+i}.listPhiq = listPhiq;
    elementsF{count+i}.alpha = alpha;
    elementsF{count+i}.beta = beta;
    elementsF{count+i}.g = grav;
end

count = size(elementsF,2)+1;
elementsF{count}.type = 'RigidBodyElement';
elementsF{count}.nodes = [nodes(end,1)];
elementsF{count}.mass = m_end;
elementsF{count}.g = grav;

count = size(elementsF,2)+1;
elementsF{count}.type = 'KinematicConstraint';
elementsF{count}.nodes = [1 2];
elementsF{count}.A = [0 0 0 0 0 1]';

count = size(elementsF,2)+1;
elementsF{count}.type = 'KinematicConstraint';
elementsF{count}.nodes = [2 3];
elementsF{count}.A = [0 0 0 0 1 0]';

count = size(elementsF,2)+1;
elementsF{count}.type = 'KinematicConstraint';
elementsF{count}.nodes = [Start2-1 Start2];
elementsF{count}.A = [0 0 0 0 1 0]';
count = count+1;

% Trajectory

elementsF{count}.type = 'TrajectoryConstraint';
elementsF{count}.nodes = [nodes(end,1)];
elementsF{count}.T = [trajx;...
                     trajy;...
                     trajz];
elementsF{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elementsF{count}.elements = [count-3 count-2 count-1];
elementsF{count}.active = 1;

% Solving
npts = finaltime/timestepsize + 1;
ModelF = FEModel();
ModelF.CreateFEModel(nodes,elementsF);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from rigid solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
        ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

% Start second full optimization with newly computed initial guess
tic
S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = rho_num;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.parameters.scaling = scaling;
S.linConst = false;
S.ConstIter = hypForComp;
S.nPass = nPass;
S.parameters.relTolRes = tol;
% analys = S.runPoleAnalysisOpti(D)
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])
% save('C:\ObjectCodeMatlab\TestsArthur\D_SerialArm_wGrav_Article.mat','D')
% save('C:\ObjectCodeMatlab\TestsArthur\S_SerialArm_wGrav_Article.mat','S')

%% Plots

% timeSteps = S.timeValues;
% timeLoc = S.timesteps;
t = find(S.timeValues==timeShift,1);
timeSteps = S.timeValues(t:end);
timeLoc = S.timesteps(t:end);

uBeam.u1 = ModelF.listElementVariables{end}.value(1,timeLoc);
uBeam.u2 = ModelF.listElementVariables{end}.value(2,timeLoc);
uBeam.u3 = ModelF.listElementVariables{end}.value(3,timeLoc);
uBeam.jointPos1 = S.model.listElementVariables{1}.relCoo(t:end);
uBeam.jointPos2 = S.model.listElementVariables{2}.relCoo(t:end);
uBeam.jointPos3 = S.model.listElementVariables{3}.relCoo(t:end);
uBeam.jointVel1 = S.model.listElementVariables{1}.velocity(t:end);
uBeam.jointVel2 = S.model.listElementVariables{2}.velocity(t:end);
uBeam.jointVel3 = S.model.listElementVariables{3}.velocity(t:end);
uBeam.time = 0:timestepsize:finaltime-timeShift;

% save('uExperiment3D','uBeam')

joint1_init = ModelR.listElementVariables{1};
joint2_init = ModelR.listElementVariables{2};
joint3_init = ModelR.listElementVariables{3};

joint1 = ModelF.listElementVariables{1};
joint2 = ModelF.listElementVariables{2};
joint3 = ModelF.listElementVariables{3};

% co = [0 0 1;
%       0 0.5 0;
%       1 0 0;
%       0 0.75 0.75;
%       0.75 0 0.75;
%       0.75 0.75 0;
%       0.25 0.25 0.25];
co = [    0    0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840];
set(groot,'defaultAxesColorOrder',co)

figure
hold on
plot(timeSteps,joint1.relCoo(timeLoc),timeSteps,joint2.relCoo(timeLoc),timeSteps,joint3.relCoo(timeLoc),'Linewidth',2)
plot(D.parameters.time(t:end),joint1_init.relCoo(timeLoc),'--',D.parameters.time(t:end),joint2_init.relCoo(timeLoc),'--',D.parameters.time(t:end),joint3_init.relCoo(timeLoc),'--','Linewidth',2)
title('Joint positions: initial guess vs. optimized solution')
xlabel('Time [s]','Fontsize',16)
ylabel('Joints angle [rad]','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% title('Joints angle of a flexible Robot','Fontsize',18)
grid on

endNodeF = ModelF.listNodes{end};
endNodeR = ModelR.listNodes{end};

figure
hold on
plot(timeSteps,endNodeF.position(1,timeLoc),timeSteps,endNodeF.position(2,timeLoc),timeSteps,endNodeF.position(3,timeLoc),'Linewidth',2)
plot(D.parameters.time(t:end),endNodeR.position(1,timeLoc),'--',D.parameters.time(t:end),endNodeR.position(2,timeLoc),'--',D.parameters.time(t:end),endNodeR.position(3,timeLoc),'--','Linewidth',2)
plot(timeVector,trajx,':',timeVector,trajy,':',timeVector,trajz,':','Linewidth',2)
title('Tip position: initial guess vs. optimized solution')
xlabel('Time [s]','Fontsize',16)
ylabel('Tip position [m]','Fontsize',16)
legend('X','Y','Z','X rigid','Y rigid','Z rigid','X des','Y des','Z des','Location', 'Best')
grid on


figure
hold on
plot(uBeam.time,uBeam.jointVel1,uBeam.time,uBeam.jointVel2,uBeam.time,uBeam.jointVel3,'Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(uRigi.time,uRigi.jointVel1,'--',uRigi.time,uRigi.jointVel2,'--',uRigi.time,uRigi.jointVel3,'--','Linewidth',2)
xlabel('Time [s]','Fontsize',16)
ylabel('Joints velocity [rad/s]','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% title('Joints velocity','Fontsize',18)
grid on

figure
hold on
plot(uBeam.time,joint1.acceleration(timeLoc),uBeam.time,joint2.acceleration(timeLoc),uBeam.time,joint3.acceleration(timeLoc),'Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(uRigi.time,joint1_init.acceleration(timeLoc),'--',uRigi.time,joint2_init.acceleration(timeLoc),'--',uRigi.time,joint3_init.acceleration(timeLoc),'--','Linewidth',2)
xlabel('Time [s]','Fontsize',16)
ylabel('Joints accelerations [rad/s^2]','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% title('Joints accelerations','Fontsize',18)
grid on

figure
hold on
plot(uBeam.time,[uBeam.u1;uBeam.u2;uBeam.u3],'Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(uRigi.time,[uRigi.u1;uRigi.u2;uRigi.u3],'--','Linewidth',2)
xlabel('Time [s]','Fontsize',16)
ylabel('Joints torques [Nm]','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% title('Joints torques','Fontsize',18)
grid on

anlFlex = poleAnalysisOpti(S,1,0);
anlFlexEnd = poleAnalysisOpti(S,211,0);

figure
hold on
plot(real(anlFlex.poles(anlFlex.indexUnstable)),imag(anlFlex.poles(anlFlex.indexUnstable)),'d','MarkerFaceColor','r','MarkerEdgeColor','r')
plot(real(anlFlex.poles(anlFlex.indexStable)),imag(anlFlex.poles(anlFlex.indexStable)),'o','MarkerFaceColor','b','MarkerEdgeColor','b')
xlabel('Real part','FontSize',16)
ylabel('Imaginary part','FontSize',16)
title('Poles of the system','FontSize',18)
legend('Unstable','Stable', 'Location', 'Best')
grid on
axis([-700 700 -700 700])
% axis([-1000 1000 -1000 1000])
viscircles([0 0],100*2*pi,'Color','k');

end

if computeDirectDyn == true

    clear elementsDirDyn
    newNodes = nodes;
%     for l = 1:size(newNodes,1)
%         newNodes(l,2:4) = ModelF.listNodes{l}.position(:,1)';
% %         for c = 2:size(newNodes,2)
% %             if newNodes(l,c)<= 1e-6
% %                 newNodes(l,c)=0;
% %             end
% %         end
%     end
%     figure
%     hold on
%     plot3(newNodes(:,2),newNodes(:,3),newNodes(:,4),'*r')
%     plot3(nodes(:,2),nodes(:,3),nodes(:,4),'ob')
    in = t; % time step choosen for initial condition
    initialCondition = [];
    for n = ModelF.listNumberNodes
        x = ModelF.listNodes{n}.position(:,in);
        R = dimR(ModelF.listNodes{n}.R(:,in));
        H = [R x; 0 0 0 1];
        h = logSE3(H);
        initialCondition = [initialCondition; [ModelF.listNodes{n}.listNumberDof' h ModelF.listNodes{n}.velocity(:,in) ModelF.listNodes{n}.acceleration(:,in)]];
    end
    for n = ModelF.listNumberElementVariables
        if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
            initialCondition = [initialCondition; [ModelF.listElementVariables{n}.listNumberDof' ModelF.listElementVariables{n}.relCoo(:,in) ModelF.listElementVariables{n}.velocity(:,in) ModelF.listElementVariables{n}.acceleration(:,in)]];
        elseif strcmp(ModelF.listElementVariables{n}.DofType,'LagrangeMultiplier')
            initialCondition = [initialCondition; [ModelF.listElementVariables{n}.listNumberDof' zeros(ModelF.listElementVariables{n}.nL,1) ModelF.listElementVariables{n}.value(:,in) zeros(size(ModelF.listElementVariables{n}.value(:,in)))]];
        end
    end
    
    count = 1;
    elementsDirDyn{count}.type = 'RigidBodyElement';
    elementsDirDyn{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
    elementsDirDyn{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
    elementsDirDyn{count}.J = diag([IxxR1 IyyR1 IzzR1]);
    elementsDirDyn{count}.g = grav;
    
%     count = size(elementsDirDyn,2)+1;
%     elementsDirDyn{count}.type = 'RigidBodyElement';
%     elementsDirDyn{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
%     elementsDirDyn{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
%     elementsDirDyn{count}.J = diag([IxxR2 IyyR2 IzzR2]);
%     elementsDirDyn{count}.g = grav;

    type = 'FlexibleBeamElement';
    count = size(elementsDirDyn,2);
    for i = 1:nElem2
        elementsDirDyn{count+i}.type = type;
        elementsDirDyn{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
        elementsDirDyn{count+i}.KCS = KCS2;
        elementsDirDyn{count+i}.MCS = MCS2;
        elementsDirDyn{count+i}.yAxis = [0 0 1];
%         elementsDirDyn{count+i}.listM = listM;
%         elementsDirDyn{count+i}.listCt = listCt;
%         elementsDirDyn{count+i}.listKt = listKt;
%         elementsDirDyn{count+i}.listPhiq = listPhiq;
        elementsDirDyn{count+i}.alpha = alpha;
        elementsDirDyn{count+i}.beta = beta;
        elementsDirDyn{count+i}.g = grav;
    end

    count = size(elementsDirDyn,2)+1;
    elementsDirDyn{count}.type = 'RigidBodyElement';
    elementsDirDyn{count}.nodes = [nodes(end,1)];
    elementsDirDyn{count}.mass = m_end;
    elementsDirDyn{count}.g = grav;

    count = size(elementsDirDyn,2)+1;
    elementsDirDyn{count}.type = 'KinematicConstraint';
    elementsDirDyn{count}.nodes = [1 2];
    elementsDirDyn{count}.A = [0 0 0 0 0 1]';

    count = size(elementsDirDyn,2)+1;
    elementsDirDyn{count}.type = 'KinematicConstraint';
    elementsDirDyn{count}.nodes = [2 3];
    elementsDirDyn{count}.A = [0 0 0 0 1 0]';
    count = count+1;

    elementsDirDyn{count}.type = 'KinematicConstraint';
    elementsDirDyn{count}.nodes = [Start2-1 Start2];
    elementsDirDyn{count}.A = [0 0 0 0 1 0]';
    count = count+1;

    % Trajectory

    y_end = l2;
    z_end = newNodes(end,4);
    r = y_end/2;

    timeVector = 0:timestepsize:finaltime-timeShift;
    trajx = newNodes(end,2)*ones(size(timeVector));
    trajy = halfCircleTraj(newNodes(end,3),y_end,r,timeVector,'sin',t_i-timeShift,t_f-timeShift);
    trajz = halfCircleTraj(newNodes(end,4),z_end,r,timeVector,'cos',t_i-timeShift,t_f-timeShift);

%     load('uRigiExperiment3D')
%     u1_init = interp1(uRigi.time,uRigi.u1,timeVector,'linear');
%     u2_init = interp1(uRigi.time,uRigi.u2,timeVector,'linear');
%     u3_init = interp1(uRigi.time,uRigi.u3,timeVector,'linear');
%     u1_init(1) = u1_init(2);
%     u2_init(1) = u2_init(2);
%     u3_init(1) = u3_init(2);

%     load('uExperiment3D')
%     u1 = interp1(uBeam.time,uBeam.u1,timeVector,'linear');
%     u2 = interp1(uBeam.time,uBeam.u2,timeVector,'linear');
%     u3 = interp1(uBeam.time,uBeam.u3,timeVector,'linear');
%     u1(1) = u1(2);
%     u2(1) = u2(2);
%     u3(1) = u3(2);

    % u1(1:5) = u1(12)*ones(1,5);
    % u2(1:5) = u2(12)*ones(1,5);
    % u3(1:5) = u3(12)*ones(1,5);
    
    u1_init = uRigi.u1;
    u2_init = uRigi.u2;
    u3_init = uRigi.u3;

    u1 = uBeam.u1;
    u2 = uBeam.u2;
    u3 = uBeam.u3;
    
    count = size(elementsDirDyn,2)+1;
    elementsDirDyn{count}.type = 'ForceInKinematicConstraint';
    elementsDirDyn{count}.elements = [count-3];
    elementsDirDyn{count}.f = uBeam.u1;
    count = count+1;

    elementsDirDyn{count}.type = 'ForceInKinematicConstraint';
    elementsDirDyn{count}.elements = [count-3];
    elementsDirDyn{count}.f = uBeam.u2;
    count = count+1;

    elementsDirDyn{count}.type = 'ForceInKinematicConstraint';
    elementsDirDyn{count}.elements = [count-3];
    elementsDirDyn{count}.f = uBeam.u3;
    
%     count = size(elementsDirDyn,2)+1;
%     elementsDirDyn{count}.type = 'ForceInKinematicConstraintPD'; % try with PD control
%     elementsDirDyn{count}.elements = [count-3];
%     elementsDirDyn{count}.ref = uBeam.jointPos1;
%     elementsDirDyn{count}.dref = uBeam.jointVel1;
%     elementsDirDyn{count}.kp = kp1;
%     elementsDirDyn{count}.kd = kd1;
%     elementsDirDyn{count}.f = u1;
% 
%     count = size(elementsDirDyn,2)+1;
%     elementsDirDyn{count}.type = 'ForceInKinematicConstraintPD'; % try with PD control
%     elementsDirDyn{count}.elements = [count-3];
%     elementsDirDyn{count}.ref = uBeam.jointPos2;
%     elementsDirDyn{count}.dref = uBeam.jointVel2;
%     elementsDirDyn{count}.kp = kp2;
%     elementsDirDyn{count}.kd = kd2;
%     elementsDirDyn{count}.f = u2;
% 
%     count = size(elementsDirDyn,2)+1;
%     elementsDirDyn{count}.type = 'ForceInKinematicConstraintPD'; % try with PD control
%     elementsDirDyn{count}.elements = [count-3];
%     elementsDirDyn{count}.ref = uBeam.jointPos3;
%     elementsDirDyn{count}.dref = uBeam.jointVel3;
%     elementsDirDyn{count}.kp = kp3;
%     elementsDirDyn{count}.kd = kd3;
%     elementsDirDyn{count}.f = u3;

    % Boundary Condition
    BC = [1];

    % Model Def
    ModelFD = FEModel();% Flex model
    ModelFD.CreateFEModel(newNodes,elementsDirDyn);
    ModelFD.defineBC(BC);

    ModelRD = FEModel();% Rigid model
    ModelRD.CreateFEModel(newNodes,elementsDirDyn);
    ModelRD.defineBC(BC);

    % Solving Flexible
    DF = DynamicIntegration(ModelFD);
    DF.parameters.finaltime = finaltime-timeShift;
    DF.parameters.timestepsize = timestepsize;
    DF.parameters.rho = rho_num;
    DF.parameters.scaling = scaling;
    DF.parameters.relTolRes = tol;
    DF.runIntegration(initialCondition);
%     DF.runIntegration();

    % Solving Rigid
%     ModelRD.listElements{count-2}.f = u1_init;
%     ModelRD.listElements{count-2}.ref = uRigi.jointPos3;
%     ModelRD.listElements{count-2}.dref = uRigi.jointVel3;
%     ModelRD.listElements{count-1}.f = u2_init;
%     ModelRD.listElements{count-1}.ref = uRigi.jointPos3;
%     ModelRD.listElements{count-1}.dref = uRigi.jointVel3;
%     ModelRD.listElements{count}.f = u3_init;
%     ModelRD.listElements{count}.ref = uRigi.jointPos3;
%     ModelRD.listElements{count}.dref = uRigi.jointVel3;
    
    ModelRD.listElements{count-2}.f = uRigi.u1;
    ModelRD.listElements{count-1}.f = uRigi.u2;
    ModelRD.listElements{count}.f = uRigi.u3;

    DR = DynamicIntegration(ModelRD);
    DR.parameters.finaltime = finaltime-timeShift;
    DR.parameters.timestepsize = timestepsize;
    DR.parameters.rho = rho_num;
    DR.parameters.scaling = scaling;
    DR.parameters.relTolRes = tol;
%     DR.runIntegration(initialCondition);
    DR.runIntegration();

    %% Plots

    x = ModelFD.listNodes{end}.position(1,:);
    y = ModelFD.listNodes{end}.position(2,:);
    z = ModelFD.listNodes{end}.position(3,:);

    xR = ModelRD.listNodes{end}.position(1,:);
    yR = ModelRD.listNodes{end}.position(2,:);
    zR = ModelRD.listNodes{end}.position(3,:);
%     nPlot = 1:6:300;
    figure
    hold on
%     plot3(x(nPlot),y(nPlot),z(nPlot), ':o','Linewidth',2, 'Color','r')
%     plot3(xR(nPlot),yR(nPlot),zR(nPlot),'--', 'Linewidth',2, 'Color','b')
%     plot3(trajx(nPlot),trajy(nPlot),trajz(nPlot), 'Linewidth',2, 'Color','k')
    plot3(x,y,z, '-o','Linewidth',2, 'Color','r')
    plot3(xR,yR,zR,'--', 'Linewidth',2, 'Color','b')
    plot3(trajx,trajy,trajz, 'Linewidth',2, 'Color','k')
    legend('With u','With u_{rigid}','Prescribed', 'Location', 'Best')
    xlabel('X [m]','Fontsize',16)
    ylabel('Y [m]','Fontsize',16)
    zlabel('Z [m]','Fontsize',16)
    % title('Trajectory of flexible arm','Fontsize',18)
    grid on
    
    radius = [];
    radiusR = [];
    radiusDesired = 0.5*ones(size(timeVector));
    for i = 1:length(timeVector)
        radius(i) = norm([x(i)-trajx(1) y(i)-0.5 z(i)]);
        radiusR(i) = norm([xR(i)-trajx(1) yR(i)-0.5 zR(i)]); 
    end
    figure
    hold on
    plot(timeVector, [radius; radiusR; radiusDesired],'Linewidth',2)
    xlabel('Time [s]','Fontsize',16)
    ylabel('Radius [m]','Fontsize',16)
    legend('With u','With u_{rigid}','Prescribed', 'Location', 'Best')
    title('Radius of the circle described by the end-effector')
    grid on

    figure
    hold on
    plot(timeVector,trajx,'k',timeVector,trajy,'k',timeVector,trajz,'k')
    set(gca,'ColorOrderIndex',1)
    plot(timeVector,x,timeVector,y,timeVector,z)
    set(gca,'ColorOrderIndex',1)
    plot(timeVector,xR,'--',timeVector,yR,'--',timeVector,zR,'--')
    xlabel('Time [s]','Fontsize',16)
    ylabel('Tip position [m]','Fontsize',16)
    legend('Xd','Yd','Zd','X', 'Y', 'Z','XR', 'YR', 'ZR')
    title('Tip position after direct dynamics','Fontsize',16)
    grid on

    joint1_init = ModelRD.listElementVariables{1};
    joint2_init = ModelRD.listElementVariables{2};
    joint3_init = ModelRD.listElementVariables{3};

    joint1 = ModelFD.listElementVariables{1};
    joint2 = ModelFD.listElementVariables{2};
    joint3 = ModelFD.listElementVariables{3};

    figure
    hold on
    plot(timeVector,joint1.relCoo,timeVector,joint2.relCoo,timeVector,joint3.relCoo,'Linewidth',2)
    plot(timeVector,joint1_init.relCoo,'--',timeVector,joint2_init.relCoo,'--',timeVector,joint3_init.relCoo,'--','Linewidth',2)
    xlabel('Time [s]','Fontsize',16)
    ylabel('Joints angle [rad]','Fontsize',16)
    legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
    title('Joints angles after direct dynamics','Fontsize',18)
    grid on

    figure
    hold on
    plot(timeVector,u1,timeVector,u2,timeVector,u3,'Linewidth',2)
    plot(timeVector,u1_init(:),'--',timeVector,u2_init(:),'--',timeVector,u3_init(:),'--','Linewidth',2)
    xlabel('Time [s]','Fontsize',16)
    ylabel('Commands [Nm]','Fontsize',16)
    legend('u_1','u_2','u_3','u_{1,rigid}','u_{2,rigid}','u_{3,rigid}','Location','Best')
    % title('Commands of a flexible arm system','Fontsize',18)
    grid on

    RelError = zeros(size(timeVector));
    RelErrorR = zeros(size(timeVector));
    for i = 1:length(RelError)
        RelError(i) = 100*(norm([trajx(i)-x(i) trajy(i)-y(i) trajz(i)-z(i)])/norm([trajx(i) trajy(i) trajz(i)]));
        RelErrorR(i) = 100*(norm([trajx(i)-xR(i) trajy(i)-yR(i) trajz(i)-zR(i)])/norm([trajx(i) trajy(i) trajz(i)]));
    end
%     figure
%     plot(timeVector,RelError,timeVector,RelErrorR)
%     title('Relative error of the direct dynamic trajectory','Fontsize',13)
%     xlabel('Time')
%     ylabel('Relative Error (%)')
%     legend('Flex','Rigid')
%     grid on

    RMSRelError = sqrt(mean(RelError.^2))
    MaxRelError = max(RelError)
    RMSRelErrorR = sqrt(mean(RelErrorR.^2))
    MaxRelErrorR = max(RelErrorR)

    figure
    hold on
    plot(timeVector,joint1.velocity,timeVector,joint2.velocity,timeVector,joint3.velocity,'Linewidth',2)
    plot(timeVector,joint1_init.velocity,'--',timeVector,joint2_init.velocity,'--',timeVector,joint3_init.velocity,'--','Linewidth',2)
    xlabel('Time [s]','Fontsize',16)
    ylabel('Joints velocity [rad/s]','Fontsize',16)
    legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid')
    title('Joints velocity','Fontsize',18)
    grid on

    figure
    hold on
    plot(timeVector,joint1.acceleration,timeVector,joint2.acceleration,timeVector,joint3.acceleration,'Linewidth',2)
    plot(timeVector,joint1_init.acceleration,'--',timeVector,joint2_init.acceleration,'--',timeVector,joint3_init.acceleration,'--','Linewidth',2)
    xlabel('Time [s]','Fontsize',16)
    ylabel('Joints accelerations [rad/s^2]','Fontsize',16)
    legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid')
    title('Joints accelerations','Fontsize',18)
    grid on
    
    
end

% figure
% for i = 1:length(timeVector)
% % for i = 1:2
% %     hold on
%     ModelFD.Visu(i)
% %     ModelF.Visu(i)
%     plot3(trajx,trajy,trajz,'k')
%     axis([1.4 1.42 0 1 -0.1 0.55])
%     view([30 10])
%     hold off
%     drawnow
% end

% analysis of the deformation measure inside the flexible beams
% Beam of interest
% beam = [2 3 4 5 6 7];
beam = [];
for i = S.model.listNumberElements
    if isa(S.model.listElements{i},'FlexibleBeamElement')
        beam = [beam i];
    end
end
epsilon = zeros(6,length(S.timeValues),length(beam)); % initialize deformation vectors
depsilon = zeros(6,length(S.timeValues),length(beam)); % initialize deformation rate vectors
for i = 1:length(beam)
    B = S.model.listElements{beam(i)};
    for t = 1:length(S.timeValues)
        xA = B.listNodes{1}.position(:,t);
        RA = dimR(B.listNodes{1}.R(:,t));
        HAm1 = [RA' -RA'*xA; 0 0 0 1];
        
        xB = B.listNodes{2}.position(:,t);
        RB = dimR(B.listNodes{2}.R(:,t));
        HB = [RB xB; 0 0 0 1];
        
        hBeam = logSE3(HAm1*HB);
        epsilon(:,t,i) = (hBeam-B.h0)/B.L;
    end
end
h = S.timeValues(2)-S.timeValues(1);
for i = 1:length(beam)
    depsilon(:,:,i) = deriveVal(epsilon(:,:,i),h);
end
compo = 6;
leg = {};
figure
hold on
title(['Phase plot of strain (compo ',num2str(compo),') in beams of the system'])
% for i = 1:length(beam)
for i = 1:1
    plot(epsilon(compo,:,i),depsilon(compo,:,i))
    leg = [leg,num2str(i)];
end
xlabel('$\epsilon$','interpreter','latex','FontSize',18)
ylabel('$\dot{\epsilon}$','interpreter','latex','FontSize',18)
legend(leg)
grid on
numDof = 36;
numDofDot = 93;
unstab = plot(10*[-anlFlex.V(numDof,100) anlFlex.V(numDof,100)], 10*[-anlFlex.V(numDofDot,100) anlFlex.V(numDofDot,100)],'Linewidth',2); % 64 corresponds to the eta value and 128 corresponds to \dot{eta}
% stab = plot(10*[-anlFlex.V(numDof,102) anlFlex.V(numDof,102)], 10*[-anlFlex.V(numDofDot,102) anlFlex.V(numDofDot,102)],'Linewidth',2); % 60 is the column number that corresponds to the unstable eigen vector and 61 is the stable eigen vector 
stab = plot(10*[-anlFlexEnd.V(numDof,102) anlFlexEnd.V(numDof,102)], 10*[-anlFlexEnd.V(numDofDot,102) anlFlexEnd.V(numDofDot,102)],'Linewidth',2); % 60 is the column number that corresponds to the unstable eigen vector and 61 is the stable eigen vector 
legend([unstab, stab],'Unstable','Stable')

compo = 4;
leg = {};
figure
hold on
title(['Phase plot of strain (compo ',num2str(compo),') in beams of the system'])
% for i = 1:length(beam)
for i = 1:1
    plot(epsilon(compo,:,i),depsilon(compo,:,i))
    leg = [leg,num2str(i)];
end
xlabel('$\epsilon$','interpreter','latex','FontSize',18)
ylabel('$\dot{\epsilon}$','interpreter','latex','FontSize',18)
legend(leg)
grid on
numDof = 34;
numDofDot = 91;
unstab = plot(10*[-anlFlex.V(numDof,100) anlFlex.V(numDof,100)], 10*[-anlFlex.V(numDofDot,100) anlFlex.V(numDofDot,100)],'Linewidth',2); % 64 corresponds to the eta value and 128 corresponds to \dot{eta}
% stab = plot(10*[-anlFlex.V(numDof,102) anlFlex.V(numDof,102)], 10*[-anlFlex.V(numDofDot,102) anlFlex.V(numDofDot,102)],'Linewidth',2); % 60 is the column number that corresponds to the unstable eigen vector and 61 is the stable eigen vector 
stab = plot(10*[-anlFlexEnd.V(numDof,102) anlFlexEnd.V(numDof,102)], 10*[-anlFlexEnd.V(numDofDot,102) anlFlexEnd.V(numDofDot,102)],'Linewidth',2); % 60 is the column number that corresponds to the unstable eigen vector and 61 is the stable eigen vector 
legend([unstab, stab],'Unstable','Stable')

compo = 5;
leg = {};
figure
hold on
title(['Phase plot of strain (compo ',num2str(compo),') in beams of the system'])
% for i = 1:length(beam)
for i = 1:1
    plot(epsilon(compo,:,i),depsilon(compo,:,i))
    leg = [leg,num2str(i)];
end
xlabel('$\epsilon$','interpreter','latex','FontSize',18)
ylabel('$\dot{\epsilon}$','interpreter','latex','FontSize',18)
legend(leg)
grid on
numDof = 35;
numDofDot = 92;
unstab = plot(10*[-anlFlex.V(numDof,100) anlFlex.V(numDof,100)], 10*[-anlFlex.V(numDofDot,100) anlFlex.V(numDofDot,100)],'Linewidth',2); % 64 corresponds to the eta value and 128 corresponds to \dot{eta}
% stab = plot(10*[-anlFlex.V(numDof,102) anlFlex.V(numDof,102)], 10*[-anlFlex.V(numDofDot,102) anlFlex.V(numDofDot,102)],'Linewidth',2); % 60 is the column number that corresponds to the unstable eigen vector and 61 is the stable eigen vector 
stab = plot(10*[-anlFlexEnd.V(numDof,102) anlFlexEnd.V(numDof,102)], 10*[-anlFlexEnd.V(numDofDot,102) anlFlexEnd.V(numDofDot,102)],'Linewidth',2); % 60 is the column number that corresponds to the unstable eigen vector and 61 is the stable eigen vector 
legend([unstab, stab],'Unstable','Stable')

% Plot deformation over time
leg = {};
figure
hold on
for i = 1:length(beam)
    plot(S.timeValues,epsilon(compo,:,i))
    leg = [leg,num2str(i)];
end
xlabel('Time [s]','interpreter','latex','FontSize',18)
ylabel('$\epsilon$','interpreter','latex','FontSize',18)
legend(leg)
plot([t_i t_i],[-0.05 0.05],'k')
plot([t_f t_f],[-0.05 0.05],'k')
grid on

% Plot deformation rate over time
leg = {};
figure
hold on
for i = 1:length(beam)
    plot(S.timeValues,depsilon(compo,:,i))
    leg = [leg,num2str(i)];
end
xlabel('Time [s]','interpreter','latex','FontSize',18)
ylabel('$\dot{\epsilon}$','interpreter','latex','FontSize',18)
legend(leg)
plot([t_i t_i],[-0.5 0.5],'k')
plot([t_f t_f],[-0.5 0.5],'k')
grid on


