%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear

finaltime = 2.5;
timestepsize = 0.01;

nodes = [1 0 0 -1;
         2 0 0 -1;
         3 0 0 0;
         4 0 0 0;
         5 0 sqrt(2)/2 sqrt(2)/2;
         6 0 sqrt(2)/2 sqrt(2)/2;
         7 0 sqrt(2)*(3/4) sqrt(2)/4;
         8 0 sqrt(2)*(3/4) sqrt(2)/4;
         9 0 sqrt(2) 0];
     
nElem = 2;
Start = 2;
End = 3;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 5;
End = 6;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 8;
End = 9;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 11;
End = 12;
nodes = createInterNodes(nodes,nElem,Start,End);

elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [3 2 4];
elements{1}.mass = 6.875;
elements{2}.J = 0.57*eye(3);
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [6 5 7];
elements{2}.mass = 6.875;
elements{2}.J = 0.57*eye(3);

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [9 8 10];
elements{3}.mass = 6.875/2;
elements{3}.J = 0.0723*eye(3);

elements{4}.type = 'RigidBodyElement';
elements{4}.nodes = [12 11 13];
elements{4}.mass = 6.875/2;
elements{4}.J = 0.0723*eye(3);

elements{5}.type = 'KinematicConstraint';
elements{5}.nodes = [1 2];
elements{5}.A = [0 0 0 0 0 1]';

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [4 5];
elements{6}.A = [0 0 0 1 0 0]';

elements{7}.type = 'KinematicConstraint';
elements{7}.nodes = [7 8];
elements{7}.A = [0 0 0 1 0 0]';

elements{8}.type = 'KinematicConstraint';
elements{8}.nodes = [10 11];
elements{8}.A = [0 0 0 1 0 0]';

elements{9}.type = 'RotSpringDamperElement';
elements{9}.damping = 0.25; %
elements{9}.stiffness = 50; %
elements{9}.nodes = [10 11];
elements{9}.A = [1 0 0];

t = 0:timestepsize:finaltime;

% load('uArmRigi')
% u1 = interp1(uRigi.time,uRigi.u1,t);
% u2 = interp1(uRigi.time,uRigi.u2,t);
% u3 = interp1(uRigi.time,uRigi.u3,t);

load('uArmFlex')
u1 = interp1(uFlex.time,uFlex.u1,t,'linear');
u2 = interp1(uFlex.time,uFlex.u2,t,'linear');
u3 = interp1(uFlex.time,uFlex.u3,t,'linear');

% Input torques
elements{10}.type = 'ForceInKinematicConstraint';
elements{10}.elements = [5];
elements{10}.f = u1;

elements{11}.type = 'ForceInKinematicConstraint';
elements{11}.elements = [6];
elements{11}.f = u2;

elements{12}.type = 'ForceInKinematicConstraint';
elements{12}.elements = [7];
elements{12}.f = u3;

TrajParam.points = [0 sqrt(2) 0;...
                    0 sqrt(2) 0.2;...
                    sqrt(2)/4 3*sqrt(2)/4 0.5;...
                    sqrt(2)/2 sqrt(2)/2 0.2;...
                    sqrt(2)/2 sqrt(2)/2 -.1];
                    
TrajParam.timeVector = t;
TrajParam.intervals = [0.3 0.7 1.3 1.8 2.2];

[trajx trajy trajz] = PointTraj(TrajParam);

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.0;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

% Plots

timeSteps = D.parameters.time;

endNode = 13;
x = Model.listNodes{endNode}.position(1,:);
y = Model.listNodes{endNode}.position(2,:);
z = Model.listNodes{endNode}.position(3,:);

figure
hold on
plot3(x,y,z, 'Linewidth',3)
plot3(trajx,trajy,trajz, 'Linewidth',1, 'Color','r')
grid on

figure
hold on
plot(timeSteps,x,timeSteps,y,timeSteps,z)
plot(timeSteps,trajx,'--',timeSteps,trajy,'--',timeSteps,trajz,'--')
legend('X', 'Y', 'Z','Xd','Yd','Zd')
grid on

% figure
% hold on
% title('Velocity of end effector of a flexible Robot','Fontsize',18)
% plot(timeSteps,Model.listNodes{endNode}.velocity(1,:),...
%     timeSteps,Model.listNodes{endNode}.velocity(2,:),...
%     timeSteps,Model.listNodes{endNode}.velocity(3,:),...
%     timeSteps,Model.listNodes{endNode}.velocity(4,:),...
%     timeSteps,Model.listNodes{endNode}.velocity(5,:),...
%     timeSteps,Model.listNodes{endNode}.velocity(6,:))
% xlabel('Time (s)','Fontsize',16)
% ylabel('Velocity','Fontsize',16)
% legend('1','2','3','4','5','6')
% grid on
% 
% figure
% hold on
% title('Acceleration of end effector of a flexible Robot','Fontsize',18)
% plot(timeSteps,Model.listNodes{endNode}.acceleration(1,:),...
%     timeSteps,Model.listNodes{endNode}.acceleration(2,:),...
%     timeSteps,Model.listNodes{endNode}.acceleration(3,:),...
%     timeSteps,Model.listNodes{endNode}.acceleration(4,:),...
%     timeSteps,Model.listNodes{endNode}.acceleration(5,:),...
%     timeSteps,Model.listNodes{endNode}.acceleration(6,:))
% xlabel('Time (s)','Fontsize',16)
% ylabel('Acceleration','Fontsize',16)
% legend('1','2','3','4','5','6')
% grid on

% Error plot

RelError = zeros(size(timeSteps));
for i = 1:length(RelError)
    RelError(i) = 100*(norm([trajx(i)-x(i) trajy(i)-y(i) trajz(i)-z(i)]))/norm([trajx(i) trajy(i) trajz(i)]);
end
figure
plot(timeSteps,RelError)
title('Relative error of the direct dynamic trajectory','Fontsize',13)
xlabel('time')
ylabel('Relative Error (%)')
grid on

MeanRelError = mean(RelError)
MaxRelError = max(RelError)
