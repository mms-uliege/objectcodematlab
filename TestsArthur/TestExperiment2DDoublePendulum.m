%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control 2D double pendul control
clear

finaltime = 2.5;
timestepsize = 0.01;
t_i = 0.5;
t_f = 2.0;

%% Creating nodes
angle = 30*pi/180;
nodes = [1 0 0 0;
         2 0 0 0;
         3 0.3*cos(angle) 0 0.3*sin(angle);
         4 0.3*cos(angle) 0 0.3*sin(angle);
         5 0.3*cos(angle)*2 0 0];
     
nElem1 = 4;
Start1 = 2;
End1 = 3;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

nElem2 = 4;
Start2 = End1+nElem1;
End2 = Start2+1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);

%% Rigid Model

a1 = 0.002;
a2 = 0.004;
b = 0.01;
l = 0.3;
rho = 2700;
m1 = a1*b*l*rho;
m2 = a2*b*l*rho;
Ixx1 = m1*(a1^2+b^2)/12;
Iyy1 = m1*(a1^2+l^2)/12;
Izz1 = m1*(b^2+l^2)/12;
Ixx2 = m2*(a2^2+b^2)/12;
Iyy2 = m2*(a2^2+l^2)/12;
Izz2 = m2*(b^2+l^2)/12;

count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elements{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([Ixx1 Iyy1 Izz1]);
% elements{count}.g = [0 0 -9.81];
count = count +1;

elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem1/2+1+[0:nElem1/2-1]];
elements{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([Ixx2 Iyy2 Izz2]);
% elements{count}.g = [0 0 -9.81];
count = count +1;

elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2];
elements{count}.mass = 0.07; % 0.03*0.019*0.04*2700
% elements{count}.g = [0 0 -9.81];
count = count +1;

elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = 0.05; % 0.03*0.019*0.04*2700
% elements{count}.g = [0 0 -9.81];
count = count +1;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.A = [0 0 0 0 1 0]';
count = count+1;

% Trajectory

x_end = -nodes(end,2);
z_end = nodes(end,4);
r = abs(x_end);

timeVector = 0:timestepsize:finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'sin',t_i,t_f);
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);


elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 0 1];
elements{count}.elements = [count-2 count-1];
elements{count}.active = 1;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.0;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

uRigiDoublePendul.u1 = Model.listElementVariables{end}.value(1,:);
uRigiDoublePendul.u2 = Model.listElementVariables{end}.value(2,:);
uRigiDoublePendul.time = D.parameters.time;

% save('uRigiDoublePendul','uRigiDoublePendul')

%% Flexible Model
count = 0;

E = 70e9; nu = 0.3; G = E/(2*(1+nu)); rho = 2700;
A1 = a1*b; Ixx1 = a1*b*(a1^2+b^2)/12; Iyy1 = b*a1^3/12;Izz1 = a1*b^3/12;
A2 = a2*b; Ixx2 = a2*b*(a2^2+b^2)/12; Iyy2 = b*a2^3/12;Izz2 = a2*b^3/12;

KCS1 = diag([E*A1 G*A1 G*A1 G*Ixx1 E*Iyy1 E*Izz1]);
MCS1 = diag(rho*[A1 A1 A1 Ixx1 Iyy1 Izz1]);
KCS2 = diag([E*A2 G*A2 G*A2 G*Ixx2 E*Iyy2 E*Izz2]);
MCS2 = diag(rho*[A2 A2 A2 Ixx2 Iyy2 Izz2]);

type = 'FlexibleBeamElement';
% count = size(elements,2);
for i = 1:nElem1
    elementsFlex{count+i}.type = type;
    elementsFlex{count+i}.nodes = [nodes(Start1+i-1,1) nodes(Start1+i,1)];
    elementsFlex{count+i}.KCS = KCS1;
    elementsFlex{count+i}.MCS = MCS1;
end

count = size(elementsFlex,2);
for i = 1:nElem2
    elementsFlex{count+i}.type = type;
    elementsFlex{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
    elementsFlex{count+i}.KCS = KCS2;
    elementsFlex{count+i}.MCS = MCS2;
end

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'RigidBodyElement';
elementsFlex{count}.nodes = [Start2];
elementsFlex{count}.mass = 0.07; % 0.03*0.019*0.04*2700
% elementsFlex{count}.g = [0 0 -9.81];
count = count +1;

elementsFlex{count}.type = 'RigidBodyElement';
elementsFlex{count}.nodes = [nodes(end,1)];
elementsFlex{count}.mass = 0.05; % 0.03*0.019*0.04*2700
% elementsFlex{count}.g = [0 0 -9.81];

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [1 2];
elementsFlex{count}.A = [0 0 0 0 1 0]';
count = count+1;

elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [Start2-1 Start2];
elementsFlex{count}.A = [0 0 0 0 1 0]';
count = count+1;

% Trajectory

npts = 126;

x_end = -nodes(end,2);
z_end = nodes(end,4);
r = abs(x_end);

timeVector = 0:finaltime/(npts-1):finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'sin',t_i,t_f);
trajy = ones(size(trajx))*nodes(end,3);
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

elementsFlex{count}.type = 'TrajectoryConstraint';
elementsFlex{count}.nodes = [nodes(end,1)];
elementsFlex{count}.T = [trajx;...
                     trajz];
elementsFlex{count}.Axe = [1 0 0;...
                       0 0 1];
elementsFlex{count}.elements = [count-2 count-1];
elementsFlex{count}.active = 1;

ModelF = FEModel();
ModelF.CreateFEModel(nodes,elementsFlex);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from rigid solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = Model.listNodes{n}.R;
    ModelF.listNodes{n}.position = Model.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = Model.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = Model.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
%         ModelF.listElementVariables{n}.A = Model.listElementVariables{n}.A;
%         ModelF.listElementVariables{n}.nDof = Model.listElementVariables{n}.nDof;
        ModelF.listElementVariables{n}.R = Model.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = Model.listElementVariables{n}.x;
%         ModelF.listElementVariables{n}.xI0 = Model.listElementVariables{n}.xI0;
        ModelF.listElementVariables{n}.velocity = Model.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = Model.listElementVariables{n}.acceleration;
        ModelF.listElementVariables{n}.relCoo = Model.listElementVariables{n}.relCoo;
    else
        while Model.listElementVariables{n+skip}.nL>6
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = Model.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

tic
S = DirectTranscriptionOpti(ModelF);
% S.parameters.relTolRes = 1e-12;
S.parameters.rho = 0.1;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.NodeToMinimize = [Start1+[1:nElem1-1] Start2+[1:nElem2-1]];
% S.JointToMinimize = [];
S.parameters.scaling = 1e0;
S.linConst = false;
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])

%% Plots

timeSteps = S.timeValues;
timeLoc = S.timesteps;

uBeamDoublePendul.u1 = ModelF.listElementVariables{end}.value(1,timeLoc);
uBeamDoublePendul.u2 = ModelF.listElementVariables{end}.value(2,timeLoc);
uBeamDoublePendul.time = S.timeValues;

% save('uBeamDoublePendul','uBeamDoublePendul')

endNode = nodes(end,1);
figure
hold on
plot3(ModelF.listNodes{endNode}.position(1,timeLoc),ModelF.listNodes{endNode}.position(2,timeLoc),ModelF.listNodes{endNode}.position(3,timeLoc), 'Linewidth',3)
plot3(trajx,trajy,trajz, 'Linewidth',1, 'Color','r')
grid on

figure
hold on
plot(timeSteps,ModelF.listNodes{endNode}.position(1,timeLoc),timeSteps,ModelF.listNodes{endNode}.position(2,timeLoc),timeSteps,ModelF.listNodes{endNode}.position(3,timeLoc))
plot(timeSteps,trajx,'--',timeSteps,trajy,'--',timeSteps,trajz,'--')
legend('X', 'Y', 'Z','Xd','Yd','Zd')
grid on

figure
hold on
plot(timeSteps,uBeamDoublePendul.u1,timeSteps,uBeamDoublePendul.u2,'Linewidth',2)
plot(uRigiDoublePendul.time,uRigiDoublePendul.u1,':',uRigiDoublePendul.time,uRigiDoublePendul.u2,':','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Commands','Fontsize',16)
legend('u1','u2','u1_{rigid}','u2_{rigid}','Location', 'Best')
title('Commands of an experimental Robot','Fontsize',18)
grid on

joint1_init = Model.listElementVariables{1};
joint2_init = Model.listElementVariables{2};

joint1 = ModelF.listElementVariables{1};
joint2 = ModelF.listElementVariables{2};

figure
hold on
plot(timeSteps,joint1.relCoo(timeLoc),timeSteps,joint2.relCoo(timeLoc),'Linewidth',2)
plot(D.parameters.time,joint1_init.relCoo,':',D.parameters.time,joint2_init.relCoo,':','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints angle (rad)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 1 rigid','Joint 2 rigid')
title('Joints angle of a flexible Robot','Fontsize',18)
grid on


figure
hold on
plot(timeSteps,joint1.velocity(timeLoc),timeSteps,joint2.velocity(timeLoc),'Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints velocity (rad/s)','Fontsize',16)
legend('Joint 1','Joint 2')
title('Joints velocity','Fontsize',18)
grid on

figure
hold on
plot(timeSteps,joint1.acceleration(timeLoc),timeSteps,joint2.acceleration(timeLoc),'Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints accelerations (rad/s^2)','Fontsize',16)
legend('Joint 1','Joint 2')
title('Joints accelerations','Fontsize',18)
grid on

MX_init_angle = 30*pi/180;
RX_init_angle = 150*pi/180;
MX_pos = MX_init_angle - joint1.relCoo(timeLoc);
RX_pos = RX_init_angle + joint2.relCoo(timeLoc);
MX_pos_rigi = MX_init_angle - joint1_init.relCoo(timeLoc);
RX_pos_rigi = RX_init_angle + joint2_init.relCoo(timeLoc);

% write in positions for MX servo
file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\DoublePendul2D\DoublePendul2D\DoublePendul2D\MX_posIn.txt','w');
for i = 1:length(MX_pos)
    fprintf(file,'%10.10f \n',MX_pos(i));
end
fclose(file);

% write in positions for RX servo
file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\DoublePendul2D\DoublePendul2D\DoublePendul2D\RX_posIn.txt','w');
for i = 1:length(RX_pos)
    fprintf(file,'%10.10f \n',RX_pos(i));
end
fclose(file);

% write in positions for MX servo when considered rigid
file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\DoublePendul2D\DoublePendul2D\DoublePendul2D\MX_posIn_rigi.txt','w');
for i = 1:length(MX_pos_rigi)
    fprintf(file,'%10.10f \n',MX_pos_rigi(i));
end
fclose(file);

% write in positions for RX servo when considered rigid
file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\DoublePendul2D\DoublePendul2D\DoublePendul2D\RX_posIn_rigi.txt','w');
for i = 1:length(RX_pos_rigi)
    fprintf(file,'%10.10f \n',RX_pos_rigi(i));
end
fclose(file);




