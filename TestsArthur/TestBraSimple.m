%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
close all
clear
clc

finaltime = 3.5;
timestepsize = 0.01;

nodes = [1 0 0 0;
         2 0 0 0;
         3 0 1 0];
     
elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2 3];
elements{1}.mass = 1;
elements{1}.J = ones(3);

% elements{10}.type = 'ExternalForce';
% elements{10}.nodes = 5;
% elements{10}.DOF = 3;
% elements{10}.amplitude = -10;
% elements{10}.frequency = 0;
% 
% elements{20}.type = 'ExternalForce';
% elements{20}.nodes = 7;
% elements{20}.DOF = 3;
% elements{20}.amplitude = -10;
% elements{20}.frequency = 0;

elements{2}.type = 'KinematicConstraint';
elements{2}.nodes = [1 2];
elements{2}.A = [0 0 0 0 0 1]';
% elements{2}.k = 1e2;


% Trajectory

x_end = 1;
x_i = nodes(end,2);

y_end = 0;
y_i = nodes(end,3);

z_end = 0;
z_i = nodes(end,4);

A = [finaltime^5 finaltime^4 finaltime^3 0 0 1
     5*finaltime^4 4*finaltime^3 3*finaltime^2 0 0 0
     20*finaltime^3 12*finaltime^2 6*finaltime 0 0 0
     0 0 0 0 0 1
     0 0 0 0 1 0
     0 0 0 1 0 0];

cpx = A\[x_end 0 0 x_i 0 0]';
cpy = A\[y_end 0 0 y_i 0 0]';
cpz = A\[z_end 0 0 z_i 0 0]';

temps = 0:timestepsize:finaltime;
trajx = cpx(1)*temps.^5 + cpx(2)*temps.^4 + cpx(3)*temps.^3 + cpx(4)*temps.^2 + cpx(5)*temps + cpx(6);
trajy = cpy(1)*temps.^5 + cpy(2)*temps.^4 + cpy(3)*temps.^3 + cpy(4)*temps.^2 + cpy(5)*temps + cpy(6);
trajz = cpz(1)*temps.^5 + cpz(2)*temps.^4 + cpz(3)*temps.^3 + cpz(4)*temps.^2 + cpz(5)*temps + cpz(6);


% elements{7}.type = 'TrajectoryConstraint';
% elements{7}.nodes = [7];
% elements{7}.T = [trajx;...
%                  trajy;...
%                  trajz];
% elements{7}.Axe = [1 0 0;...
%                  0 1 0;...
%                  0 0 1];
% % elements{7}.finaltime = finaltime;


elements{3}.type = 'TrajectoryConstraint';
elements{3}.nodes = [3];
elements{3}.T = [trajx];
elements{3}.Axe = [1 0 0];
elements{3}.elements = [2];

% elements{4}.type = 'RotSpringDamperElement';
% elements{4}.damping = 1;
% elements{4}.stiffness = 10;
% elements{4}.natural_angle = -pi/6;
% elements{4}.nodes = [1 2];
% elements{4}.A = [0 0 1];

% load ComRot
% elements{11}.type = 'ForceInKinematicConstraint';
% elements{11}.elements = [2];
% elements{11}.f = Rot;


BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

S = DynamicIntegration(Model);
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = timestepsize;
S.parameters.rho = 0.9;
S.runIntegration();

figure
hold on
plot3(Model.listNodes{3}.position(1,:),Model.listNodes{3}.position(2,:),Model.listNodes{3}.position(3,:), 'Linewidth',2)
plot3(Model.listNodes{3}.position(1,:),Model.listNodes{3}.position(2,:),-3*ones(1,finaltime/timestepsize + 1), 'Linewidth',2,'Color',[.8 .8 .8])
axis(3*[-1 1 -1 1 -1 1])
grid on

figure
plot(S.parameters.time,trajx)
legend('Xd')

figure
plot(S.parameters.time,Model.listNodes{3}.position(1,:),S.parameters.time,Model.listNodes{3}.position(2,:))
legend('X', 'Y')


figure
plot(S.parameters.time,Model.listElementVariables{end}.value)
grid on

figure
plot(S.parameters.time,asin(Model.listNodes{3}.position(1,:))*180/pi)
legend('angle')

