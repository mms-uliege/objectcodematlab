%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Double Pendulum example to see if the optimization process works for a
% traditionnal dynamic integration scheme (but will obviously take longer
% time
clear Model
clear elements
clear nodes

finaltime = 3;
timestepsize = 0.01;
time = 0:timestepsize:finaltime;
npts = finaltime/timestepsize +1;

grav = [0 0 -9.81];

nodes = [1 0 0 0;...
         2 0 0 0;...
         3 1 0 0;...
         4 1 0 0;...
         5 2 0 0];
     
%  nodes = [1 0 0 0;...
%      2 0 0 0;...
%      3 1 0 0];

nElem1 = 2;
Start1 = 2;
nodes = createInterNodes(nodes,nElem1,Start1,Start1+1);

nElem2 = 2;
Start2 = Start1+nElem1+1;
nodes = createInterNodes(nodes,nElem2,Start2,Start2+1);

stiff = 0;
damp = 0;
coulomb = 0;

kp = 200;
kd = 5;

E = 210e9; % link modulus in N/m�
l = 0.9340; % link length in m
w = 0.015; % link width in m
h = 0.004; % link height in m
rho = 7800; % mass density in kg/m�
A = 2.09e-4; % cross section area of the link in m�
m = l * A * rho; % mass of link in kg
% J = diag([m*(w^2 + h^2)/12 m*(l^2 + h^2)/12 m*(l^2 + w^2)/12]); % inertia in kg*m�
J = diag([2.18e-4 0.08374 0.08374]); % inertia in kg*m�
nu = 0.27; % poisson coef
G = E/(2*(1+nu)); % compression modulus
% Ixx = (w*h)*(w^2 + h^2)/12; % area moment inertia in case of beam in m^4
% Iyy = (w*h^3)/12; % area moment inertia in case of beam in m^4
% Izz = (w^3*h)/12; % area moment inertia in case of beam in m^4
Ixx = 1.76e-8; % area moment inertia in case of beam in m^4
Iyy = 1.01e-8; % area moment inertia in case of beam in m^4
Izz = 0.75e-8; % area moment inertia in case of beam in m^4
KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

count = 0;
for i = 1:nElem1
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start1-1+i Start1+i];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
    elements{count+i}.g = grav;
end

count = size(elements,2);
for i = 1:nElem2
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start2-1+i Start2+i];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
    elements{count+i}.g = grav;
end

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = 3;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.k = stiff;
elements{count}.d = damp;
elements{count}.coulomb = coulomb;
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.k = stiff;
elements{count}.d = damp;
elements{count}.coulomb = coulomb;
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = count-2;
elements{count}.elements = count-1;
elements{count}.f = 0*ones(size(time));
elements{count}.ref = pi/6*ones(size(time));
elements{count}.dref = 0*ones(size(time));

% BC
BC = [1];

% Create Model

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

% Solving
 
D = DynamicIntegration(Model);
% D = StaticIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.01;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

for n = Model.listNumberNodes
%     dh = zeros(6,npts);
%     for i = 1:npts
%         dh(:,i) = logSE3([dimR(Model.listNodes{n}.R(:,i)) Model.listNodes{n}.position(:,i); 0 0 0 1]);
%     end
%     Model.listNodes{n}.velocity = deriveVal(dh,timestepsize);
%     Model.listNodes{n}.acceleration = deriveVal(Model.listNodes{n}.velocity,timestepsize);
    Model.listNodes{n}.InitializeD_Opti();
end
for n = Model.listNumberElementVariables
%     if strcmp(Model.listElementVariables{n}.DofType,'MotionDof')
%         Model.listElementVariables{n}.velocity = deriveVal(Model.listElementVariables{n}.relCoo,timestepsize);
%         Model.listElementVariables{n}.acceleration = deriveVal(Model.listElementVariables{n}.velocity,timestepsize);
%     end
    Model.listElementVariables{n}.InitializeD_Opti();
end

S = DirectTranscriptionOpti(Model);
S.npts = finaltime/timestepsize +1;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = timestepsize;
S.parameters.rho = 0.01;
% S.parameters.relTolRes = 1e-10;
S.parameters.scaling = 1e6;
xSol = S.runOpti(D);



