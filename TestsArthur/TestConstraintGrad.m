%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% test of gradients
close all
clear
clc

finaltime = 3.5;
timestepsize = 0.01;

nodes = [1 -1 0 0;
         2 -1 0 0;
         3 -1 0 0;
         4 -1.6614 -0.75 0;
         5 -1.6614 -0.75 0;
         6 -1 -1.5 0];

elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2];
elements{1}.mass = 3;
% elements{1}.J = eye(3);
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [3 4];
elements{2}.mass = 6.875;
elements{2}.J = 0.57*eye(3);

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [5 6];
elements{3}.mass = 6.875;
elements{3}.J = 0.57*eye(3);

% elements{20}.type = 'ExternalForce';
% elements{20}.nodes = [3 4 5 6];
% elements{20}.DOF = 2;
% elements{20}.amplitude = -10;
% elements{20}.frequency = 0;

elements{4}.type = 'KinematicConstraint';
elements{4}.nodes = [1 2];
elements{4}.A = [1 0 0 0 0 0]';

elements{5}.type = 'KinematicConstraint';
elements{5}.nodes = [2 3];
elements{5}.A = [0 0 0 0 0 1]';

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [4 5];
elements{6}.A = [0 0 0 0 0 1]';

% Trajectory

cart_end = 1;
cart_i = -1;

x_end = 1;
x_i = -1;

y_end = -1.5;
y_i = -1.5;

trajcart = halfCircleTraj(cart_i,cart_end, finaltime,timestepsize,'lin',1,2.5);
trajx = halfCircleTraj(x_i,x_end, finaltime,timestepsize,'sin',1,2.5);
trajy = halfCircleTraj(y_i,y_end, finaltime,timestepsize,'cos',1,2.5);

elements{8}.type = 'TrajectoryConstraint';
elements{8}.nodes = [2];
elements{8}.T = [trajcart];
elements{8}.Axe = [1 0 0];
elements{8}.elements = [4];

elements{9}.type = 'TrajectoryConstraint';
elements{9}.nodes = [6];
elements{9}.T = [trajx;...
                 trajy];
elements{9}.Axe = [1 0 0;...
                   0 1 0];
elements{9}.elements = [5 6];

BC = [1];

Model0 = FEModel();
Model0.CreateFEModel(nodes,elements);
Model0.defineBC(BC);

Model1 = FEModel();
Model1.CreateFEModel(nodes,elements);
Model1.defineBC(BC);

Solution0 = DirectTranscriptionOpti(Model0);
Solution0.parameters.finaltime = finaltime;
Solution0.parameters.timestepsize = timestepsize;
Solution0.parameters.rho = 0.8;
Solution0.npts = 11;
% Solution0.parameters.scaling = 1e0;

Solution0.parameters.InitialComputation();
            
for i = Solution0.model.listNumberDof
    Solution0.model.listDof{i}.InitializeD(Solution0.parameters.nstep);
end
for i = Solution0.model.listNumberNodes
    Solution0.model.listNodes{i}.InitializeD(Solution0.parameters.nstep);
end
for i = Solution0.model.listNumberElementVariables
    Solution0.model.listElementVariables{i}.InitializeD(Solution0.parameters.nstep);
end
for i = Solution0.model.listNumberElements
    Solution0.model.listElements{i}.InitializeD(Solution0.parameters.nstep);
end

Solution1 = DirectTranscriptionOpti(Model1);
Solution1.parameters.finaltime = finaltime;
Solution1.parameters.timestepsize = timestepsize;
Solution1.parameters.rho = 0.8;
Solution1.npts = 11;
% Solution1.parameters.scaling = 1e0;

Solution1.parameters.InitialComputation();
            
for i = Solution1.model.listNumberDof
    Solution1.model.listDof{i}.InitializeD(Solution1.parameters.nstep);
end
for i = Solution1.model.listNumberNodes
    Solution1.model.listNodes{i}.InitializeD(Solution1.parameters.nstep);
end
for i = Solution1.model.listNumberElementVariables
    Solution1.model.listElementVariables{i}.InitializeD(Solution1.parameters.nstep);
end
for i = Solution1.model.listNumberElements
    Solution1.model.listElements{i}.InitializeD(Solution1.parameters.nstep);
end

% computation of some values by finite difference

savedModelName = 'DynInt_RigidCart';

% Total number of variable per time step
% Solution.nVar = 165;
% Solution.nCoord = 33;
% Solution.nLagrangeMult = 30;
% Solution.nControls = 3;


% for Solution.npts
npts = Solution1.npts;
% at the kth time step
k = 11;
shiftTimeStep = (k-1)*Solution1.nVar;

% perturbation eta of the variable (type 1 is dq, 2 is v, 3 is v_dot, 4 is acc,
% 5 is lambda, 6 is u)

type = 6;
if type<=5
    shiftType = (type-1)*Solution1.nCoord;
elseif type==6
    shiftType = 4*Solution1.nCoord+Solution1.nLagrangeM;
end
number = 1; % value number of variable type "type" (1-33 if type dq,v,vdot or a, 1-30 if type lambda, 1-3 if type u)

VarNum = shiftTimeStep + shiftType + number; % location number of the variable

% Location of the chosen equation line
sizeMotionConst = Solution1.nCoord + Solution1.nLagrangeM + Solution1.nControls;
sizeIntregrationConst = 3*Solution1.nCoord;

startMotionLin = (k-1)*(sizeMotionConst+sizeIntregrationConst);
startIntegrationLin = startMotionLin + sizeMotionConst;

% Gradient for equation of motion OR equation of integration
EquMotion = 1;
if EquMotion
    EquLin = [startMotionLin+1:startMotionLin+sizeMotionConst];
else
    EquLin = [startIntegrationLin+1:startIntegrationLin+sizeIntregrationConst];
end

% in equation number (choice between 1-165, same number as variable within
% one time step)
eta = 1e-4;
% x0 = initValFromModel(savedModelName,npts);
eps = 1e-6;
x0 = zeros(Solution1.nVar*Solution1.npts,1);
x1 = x0;
x1(VarNum) = x0(VarNum)+eta;

% Compute the constraint values (watch out that gradceq is
% number of variable x number of equation
[c, ceq1, gradc, gradceq1] = directTranscriptionOptiNLConstraints(x1, Solution1);
[c, ceq0, gradc, gradceq0] = directTranscriptionOptiNLConstraints(x0, Solution0);


% Gradient at selected variable
% *Solution.parameters.scaling
FiniteDiff = (ceq1(EquLin) - ceq0(EquLin))/(eta);
gradceq0(VarNum,EquLin)';
gradceq1(VarNum,EquLin)';

check = abs(gradceq0(VarNum,EquLin)' - FiniteDiff);
if check<=1e-8
    disp('Les gradient de contraintes sont semblables')
else
    disp('Les gradient de contraintes ne sont pas semblables !!!!')
end

% Compute objective function gradients

[J1, gradJ1] = objectiveFuncMinControl(x1, Solution1);
[J0, gradJ0] = objectiveFuncMinControl(x0, Solution0);
J1
J0
normgrad0 = norm(gradJ0)

FiniteDiffObj = (J1 - J0)/eta;
checkObj = abs(gradJ0(VarNum) - FiniteDiffObj);
if checkObj<=1e-8
    disp('Les gradient de l''objectif sont semblables')
else
    disp('Les gradient de l''objectif ne sont pas semblables !!!!')
end

% check if other columns of gradient matrix are alike

% spy(gradceq0'-gradceq1',10)
% 
% diff = gradceq0(1:200,1:200)'-gradceq1(1:200,1:200)';


