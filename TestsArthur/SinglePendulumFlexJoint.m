%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Example of a flexible joint robot with one degree of freedom
% single pendulum like robot
clear all
% close all

finaltime = 5; % 2 seconds
timestepsize = 0.01;

% grav = [0 0 -9.81];
grav = [0 0 0];
stif = 1.2*1e5/10;
damp = 2.2*1e-3;
m = 1;

% nodes = [1 0 0 0;...
%          2 0 0 0;...
%          3 0 0 0;...
%          4 1 0 0;...
%          5 0.3 0 0];
     
nodes = [1 0 0 0;...
         2 0 0 0;...
         3 0 0 0;...
         4 1 0 0];
     
count = 1;
% creating first rigid body (rotating aroud y)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [2];
elements{count}.mass = 0.001;
elements{count}.J = 0.0001*eye(3);
elements{count}.g = grav;

count = size(elements,2)+1;
% creating first rigid body (rotating aroud y)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [4 3];
elements{count}.mass = m;
elements{count}.J = 0.1*eye(3);
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'RotSpringDamperElement';
elements{count}.nodes = [2 3];
elements{count}.stiffness = stif;
elements{count}.damping = damp;
elements{count}.A = [0 1 0];

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.A = [0 0 0 0 1 0]';

t = 0:timestepsize:finaltime;
A = 10; % Nm max amplitude of the sine torque
fq = 2; % Hz frequency of the sinewave torque   sqrt(stif/m)
u1 = A*cos(t.*2*pi*fq);
% u1 = zeros(size(t));
% u1(1:10) = A*ones(1,10);
count = size(elements,2)+1;
elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = [count-2];
elements{count}.f = u1;

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
% D = StaticIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.9;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

figure
plot(D.parameters.time,ModelR.listElementVariables{1}.relCoo,D.parameters.time,ModelR.listElementVariables{2}.relCoo)
xlabel('time')
ylabel('angular position')
legend('first joint','second joint')
