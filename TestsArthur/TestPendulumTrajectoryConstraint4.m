%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test a simple 2DOF pendulum see if TrajectoryConstraint4 can work for
% rotations

clear all

initialtime = 0;
finaltime = 1;
h = 0.01; % time step size
time = initialtime:h:finaltime;
rho_num = 0.5; % numerical damping

% first link
ang1 = 0;
l1 = 2; % length of the pendulum
m1 = 1;
% second link
ang2 = pi/2;
l2 = 1; % length of the pendulum
m2 = 1;

% third link
ang3 = pi/2;
l3 = 1; % length of the pendulum
m3 = 1;

% gravity
grav = [0 -9.81 0];
% grav = [0 0 0];

% nodes = [1 0 0 0;...
%          2 0 0 0;...
%          3 l1*cos(ang1) l1*sin(ang1) 0
%          4 l1*cos(ang1) l1*sin(ang1) 0;...
%          5 l1*cos(ang1)+l2*cos(ang2) l1*sin(ang1)+l2*sin(ang2) 0];
     
nodes = [1 0 0 0;...
         2 0 0 0;...
         3 l1*cos(ang1) l1*sin(ang1) 0
         4 l1*cos(ang1) l1*sin(ang1) 0;...
         5 l1*cos(ang1)+l2*cos(ang2) l1*sin(ang1)+l2*sin(ang2) 0;...
         6 l1*cos(ang1)+l2*cos(ang2) l1*sin(ang1)+l2*sin(ang2) 0;...
         7 l1*cos(ang1)+l2*cos(ang2)+l3*cos(ang3) l1*sin(ang1)+l2*sin(ang2)+l3*sin(ang3) 0];
     
elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [3 2];
elements{1}.mass = m1;
elements{1}.g = grav;

elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [5 4];
elements{2}.mass = m2;
elements{2}.g = grav;

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [6 7];
elements{3}.mass = m3;
elements{3}.g = grav;

elements{4}.type = 'KinematicConstraint';
elements{4}.nodes = [1 2];
elements{4}.A = [0 0 0 0 0 1]';

elements{5}.type = 'KinematicConstraint';
elements{5}.nodes = [3 4];
elements{5}.A = [0 0 0 0 0 1]';

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [5 6];
elements{6}.A = [0 0 0 0 0 1]';

start_ang = 0;
end_ang = -pi/6;
xend = nodes(end,2)+l3/3;
yend = 0;
traja = lineTraj(start_ang,end_ang,time,initialtime,finaltime);
trajx = lineTraj(nodes(end,2),xend,time,initialtime,finaltime);
trajy = lineTraj(nodes(end,3),yend,time,initialtime,finaltime);
elements{7}.type = 'TrajectoryConstraint4';
elements{7}.nodes = [nodes(end,1)];
elements{7}.T = [trajx;
                 trajy;
                 traja];
elements{7}.Axe = [1 0 0 0 0 0;...
                   0 1 0 0 0 0;...
                   0 0 0 0 0 1];
elements{7}.elements = [4 5 6];
elements{7}.active = 1;

% boundary condition
BC = [1];

% Solving model
Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = h;
D.parameters.rho = rho_num;
D.parameters.scaling = 1e6;
D.runIntegration();

% figures
figure
plot(time,Model.listNodes{end}.position)
title('Pendulum tip position')
xlabel('Time')
ylabel('Position')
legend('x','y','z')
grid on

figure
plot(time,Model.listElementVariables{end}.value)
title('Pendulum actuator torque')
xlabel('Time')
ylabel('torque')
grid on
