%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create double pendulum
close all
clear all

% first link
l1 = 1;
m1 = 1;
I1 = eye(3);
theta10 = 10*45*pi/180;

% second link
l2 = 1;
m2 = 1;
I2 = eye(3);
theta20 = -10*-45*pi/180;

% gravity
grav = [0 -9.81 0];

% numerical parameters
ti = 0;
tf = 5;
h = 0.01;
time = ti:h:tf;
rho_num = 0.5; % numerical damping

% Define my nodes
nodes = [1 0 0 0;...
         2 0 0 0;...
         3 (l1/2)*cos(theta10) (l1/2)*sin(theta10) 0;...
         4 (l1)*cos(theta10) (l1)*sin(theta10) 0;...
         5 (l1)*cos(theta10) (l1)*sin(theta10) 0;...
         6 (l1)*cos(theta10)+(l2/2)*cos(theta10 + theta20) (l1)*sin(theta10)+(l2/2)*sin(theta10 + theta20) 0;...
         7 (l1)*cos(theta10)+(l2)*cos(theta10 + theta20) (l1)*sin(theta10)+(l2)*sin(theta10 + theta20) 0;...
         8 (l1)*cos(theta10)+(l2)*cos(theta10 + theta20) (l1)*sin(theta10)+(l2)*sin(theta10 + theta20) 0];

     % Create the elements
elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [3 2 4];
elements{1}.mass = m1;
elements{1}.J = I1;
elements{1}.g = grav;

elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [6 5 7];
elements{2}.mass = m1;
elements{2}.J = I1;
elements{2}.g = grav;

elements{3}.type = 'KinematicConstraint';
elements{3}.nodes = [1 2];
elements{3}.A = [0 0 0 0 0 1]';

elements{4}.type = 'KinematicConstraint';
elements{4}.nodes = [4 5];
elements{4}.A = [0 0 0 0 0 1]';

f = 5.*sin(2*pi*1*time);
% elements{5}.type = 'ForceInKinematicConstraint';
% elements{5}.elements = 3;
% elements{5}.f = f;

f = 0*5.*sin(2*pi*1*time);
elements{5}.type = 'ForceInKinematicConstraintPD';
elements{5}.elements = 3;
elements{5}.ref = zeros(1,length(time))';
elements{5}.dref = zeros(1,length(time))';
elements{5}.kd = 10;
elements{5}.f = f;

elements{6}.type = 'ForceInKinematicConstraintPD';
elements{6}.elements = 4;
elements{6}.ref = zeros(1,length(time))';
elements{6}.dref = zeros(1,length(time))';
elements{6}.kd = 10;
elements{6}.f = f;

% Boundary condition
BC = [1 8];

% Create model
Model = FEModel();
Model.CreateFEModel(nodes,elements)
Model.defineBC(BC);

% Solve the system
D = DynamicIntegration(Model);
D.parameters.finaltime = tf;
D.parameters.timestepsize = h;
D.parameters.rho = rho_num;
D.runIntegration();

% plots
figure
plot(time,Model.listNodes{end}.position(1,:),time,Model.listNodes{end}.position(2,:))
title('Position of tip')
xlabel('Time')
ylabel('Position')
legend('x','y')

figure
plot(time,Model.listNodes{end}.velocity(1,:),time,Model.listNodes{end}.velocity(2,:))
title('Velocity of tip')
xlabel('Time')
ylabel('Velocity')
legend('x','y')

figure
plot(time,Model.listNodes{end}.acceleration(1,:),time,Model.listNodes{end}.acceleration(2,:))
title('Acceleration of tip')
xlabel('Time')
ylabel('Acceleration')
legend('x','y')


figure
plot(time,Model.listElementVariables{1}.relCoo,time,Model.listElementVariables{2}.relCoo)
title('Joint Position of the arm')
xlabel('Time')
ylabel('Angle')
legend('joint1','joint2')

figure
plot(time,Model.listElementVariables{1}.relCoo,time,Model.listElementVariables{2}.relCoo)
title('Joint Position of the arm')
xlabel('Time')
ylabel('Angle')
legend('joint1','joint2')




