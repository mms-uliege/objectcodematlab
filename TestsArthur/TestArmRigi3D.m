%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear

finaltime = 2.5;
timestepsize = 0.01;

nodes = [1 0 0 -1;
         2 0 0 -1;
         3 0 0 0;
         4 0 0 0;
         5 0 sqrt(2)/2 sqrt(2)/2;
         6 0 sqrt(2)/2 sqrt(2)/2;
         7 0 sqrt(2) 0];
     
nElem = 2;
Start = 2;
End = 3;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 5;
End = 6;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 8;
End = 9;
nodes = createInterNodes(nodes,nElem,Start,End);

elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [3 2 4];
elements{1}.mass = 6.875;
elements{2}.J = 0.57*eye(3);
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [6 5 7];
elements{2}.mass = 6.875;
elements{2}.J = 0.57*eye(3);

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [9 8 10];
elements{3}.mass = 6.875;
elements{3}.J = 0.57*eye(3);

elements{5}.type = 'KinematicConstraint';
elements{5}.nodes = [1 2];
elements{5}.A = [0 0 0 0 0 1]';

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [4 5];
elements{6}.A = [0 0 0 1 0 0]';

elements{7}.type = 'KinematicConstraint';
elements{7}.nodes = [7 8];
elements{7}.A = [0 0 0 1 0 0]';

% elements{8}.type = 'KinematicConstraint';
% elements{8}.nodes = [6 7];
% elements{8}.A = [0 0 0 0 0 1]';

% elements{9}.type = 'RotSpringDamperElement';
% elements{9}.damping = 200; % 1
% elements{9}.stiffness = 1000; % Minimum Phase 7.5
% % elements{9}.stiffness = 3; % Non-Minimum Phase
% elements{9}.nodes = [6 7];
% elements{9}.A = [0 0 1];

% Trajectory

TrajParam.points = [0 sqrt(2) 0;...
                    0 sqrt(2) 0.2;...
                    sqrt(2)/4 3*sqrt(2)/4 0.5;...
                    sqrt(2)/2 sqrt(2)/2 0.2;...
                    sqrt(2)/2 sqrt(2)/2 -.1];
                    
TrajParam.timeVector = 0:timestepsize:finaltime;
TrajParam.intervals = [0.3 0.7 1.3 1.8 2.2];

[trajx trajy trajz] = PointTraj(TrajParam);

elements{11}.type = 'TrajectoryConstraint';
elements{11}.nodes = [10];
elements{11}.T = [trajx;...
                 trajy;...
                 trajz];
elements{11}.Axe = [1 0 0;...
                   0 1 0;...
                   0 0 1];
elements{11}.elements = [5 6 7];
elements{11}.active = 1;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

tic
D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.0;
% D.parameters.relTolRes = 1e-12;
D.runIntegration();
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])

uRigi.u1 = Model.listElementVariables{end}.value(1,:);
uRigi.u2 = Model.listElementVariables{end}.value(2,:);
uRigi.u3 = Model.listElementVariables{end}.value(3,:);
uRigi.time = D.parameters.time;

save('uArmRigi','uRigi')
% Plots

endNode = 10;
figure
hold on
plot3(Model.listNodes{endNode}.position(1,:),Model.listNodes{endNode}.position(2,:),Model.listNodes{endNode}.position(3,:), 'Linewidth',3)
plot3(trajx,trajy,trajz, 'Linewidth',1, 'Color','r')
grid on


figure
hold on
plot(D.parameters.time,Model.listNodes{endNode}.position(1,:),D.parameters.time,Model.listNodes{endNode}.position(2,:),D.parameters.time,Model.listNodes{endNode}.position(3,:))
plot(D.parameters.time,trajx,'--',D.parameters.time,trajy,'--',D.parameters.time,trajz,'--')
legend('X', 'Y', 'Z','Xd','Yd','Zd')
grid on

figure
hold on
plot(D.parameters.time,Model.listElementVariables{end}.value(1,:),D.parameters.time,Model.listElementVariables{end}.value(2,:),D.parameters.time,Model.listElementVariables{end}.value(3,:),'Linewidth',2)
% plot(timeSteps,u1_init(timeLoc),':',timeSteps,u2_init(timeLoc),':',timeSteps,u3_init(timeLoc),':','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Commands','Fontsize',16)
legend('u1','u2','u3')
title('Commands of a rigid 3D Robot','Fontsize',18)
grid on


