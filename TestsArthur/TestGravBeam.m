%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test gravity in beam element
clear all
close all

% Simulation time

finaltime = 2;
timestepsize = 0.005;

l = 0.5;
m_end = 1;
f = 0.5; % frequency of the sine perturbation
ampl = -0*9.81; % amplitude of the sine perturbation
grav = [0 0 ampl];
t = 0:timestepsize:finaltime;
% grav = [zeros(1,length(t)); zeros(1,length(t)); ampl*sin(2*pi*f.*t)]';
rho_num = 0.1;

% Creating nodes

nodes = [1 0 0 0;
         2 l 0 0];
     
nElem1 = 5;
Start1 = 1;
End1 = 2;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

% Beam parameters
a1 = 0.001;
b1 = 0.01;
rho = 2000;

E = 20e9; nu = 0.3; G = E/(2*(1+nu));
A1 = a1*b1; Ixx1 = a1*b1*(a1^2+b1^2)/12; Iyy1 = b1*a1^3/12;Izz1 = a1*b1^3/12;

KCS1 = diag([E*A1 G*A1 G*A1 G*Ixx1 E*Iyy1 E*Izz1]);
MCS1 = diag(rho*[A1 A1 A1 Ixx1 Iyy1 Izz1]);
listM = [1 0 0];
listKt = [1 0 0];
listCt = [1 0 0];
alpha = 0*0.0001;
beta = 0*0.01;

% Creating list of elements
count = 0;

type = 'FlexibleBeamElement';
for i = 1:nElem1
    elementsFlex{count+i}.type = type;
    elementsFlex{count+i}.nodes = [nodes(Start1+i-1,1) nodes(Start1+i,1)];
    elementsFlex{count+i}.KCS = KCS1;
    elementsFlex{count+i}.MCS = MCS1;
    elementsFlex{count+i}.alpha = alpha;
    elementsFlex{count+i}.beta = beta;
    elementsFlex{count+i}.listM = listM;
    elementsFlex{count+i}.listKt = listKt;
    elementsFlex{count+i}.listCt = listCt;
    elementsFlex{count+i}.g = grav;
end

% count = size(elementsFlex,2)+1;
% elementsFlex{count}.type = 'RigidBodyElement';
% elementsFlex{count}.nodes = [nodes(end,1)];
% elementsFlex{count}.mass = m_end; % 0.07
% elementsFlex{count}.g = grav;

% count = size(elementsFlex,2)+1;
% elementsFlex{count}.type = 'ExternalForce';
% elementsFlex{count}.nodes = nodes(end,1);
% elementsFlex{count}.DOF = 2;
% elementsFlex{count}.amplitude = 100;
% elementsFlex{count}.frequency = 1;


% Boundary Condition
BC = [1];

% Building model
Model = FEModel();
Model.CreateFEModel(nodes,elementsFlex);
Model.defineBC(BC);

% Solving

D = DynamicIntegration(Model);
% D = StaticIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration([33 0.5 1]);
% D.runIntegration();

EndNode = Model.listNodes{end};
xEnd = EndNode.position(1,:);
yEnd = EndNode.position(2,:);
zEnd = EndNode.position(3,:);
dxEnd = EndNode.velocity(1,:);
dyEnd = EndNode.velocity(2,:);
dzEnd = EndNode.velocity(3,:);
ddxEnd = EndNode.acceleration(1,:);
ddyEnd = EndNode.acceleration(2,:);
ddzEnd = EndNode.acceleration(3,:);

figure
plot(D.parameters.time,xEnd,D.parameters.time,yEnd,D.parameters.time,zEnd)
title('Tip position (m)')
ylabel('Position (m)')
xlabel('Time (s)')
legend('x','y','z')

figure
plot(D.parameters.time,dxEnd,D.parameters.time,dyEnd,D.parameters.time,dzEnd)
title('Tip velocity (m)')
ylabel('Velocity (m/s)')
xlabel('Time (s)')
legend('x','y','z')

figure
plot(D.parameters.time,ddxEnd,D.parameters.time,ddyEnd,D.parameters.time,ddzEnd)
title('Tip acceleration (m)')
ylabel('Acceleration (m/s^2)')
xlabel('Time (s)')
legend('x','y','z')

