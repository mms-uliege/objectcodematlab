%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% test for the joint trajectory tracking element,
% try to move a simple pendulum with a sine wave motion
clear

finaltime = 1; % simulation time in sec
timestepsize = 0.001; % time step size in sec

grav = [0 0 -9.87]; % gravity in -z direction
% grav = [0 0 0]; % gravity in -z direction

timeVector = 0:timestepsize:finaltime;
f = 3;
jointTraj = (pi/4)*sin(2*pi*f*timeVector);

nodes = [1 0 0 0;...
         2 0 0 0;...
         3 1 0 0];
     
count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [3 2];
elements{count}.mass = 1;
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 1 0 0 0]';

count = size(elements,2)+1;
elements{count}.type = 'TrajectoryConstraintJoint';
elements{count}.elements = count-1;
elements{count}.T = jointTraj;

% boundary condition
BC = [1];

% create FEM model
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);


% solve dynamic problem
D = DynamicIntegration(ModelR);
% D = StaticIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.0;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

commands = D.model.listElementVariables{end}.value;
joint = D.model.listElementVariables{1}.relCoo;
% plots
figure
plot(D.parameters.time, commands)
xlabel('Time (s)')
ylabel('Commands (Nm)')
grid on

figure
hold on
plot(D.parameters.time, joint)
plot(timeVector,jointTraj,'--')
grid on
xlabel('Time (s)')
ylabel('Joint angle (rad)')
legend('eff','desired')