%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DIRECT DYNAMIC, controlling the joints in position.
% Test to try to get a control of 3 joints of the Ella robot of JKU Linz. WITH
% Visc JOINTS (ALL coulomb and viscous) and a dynamic Rigid computation at the beginning
% With line trajectory as for the actual Ella robot USING Cartesian TRAJECTORY.
% And end-effector mass is not exactly at the tip (6cm before)

% parameters comming from https://robfile.mechatronik.uni-linz.ac.at/doku.php?id=start:robots:ella:mechsetup
% close all
clear all
clear elements

finaltime = 2.15; % 2 seconds
timestepsize = 0.01;
duration = '115'; % duration of the trajectory (WITHOUT PRE- POST- DURATION)

t_i = 0.8; % time at end of pre-actuation
t_f = finaltime - 0.2; % time at begining of post-actuation

% kp1 = 100;
% kd1 = 5;
% kp2 = 100;
% kd2 = 5;
% kp3 = 100;
% kd3 = 5;
kp1 = 0;
kd1 = 0;
kp2 = 0;
kd2 = 0;
kp3 = 0;
kd3 = 0;

grav = [0 0 -9.81]; % gravity in m/s�
% grav = [0 0 0]; % gravity in m/s�

endPoseInit = [1.4 0.75 -0.5]; % changed in the coordinate we have in our model
angleLink0 = 0.4918; % rad, angle at the base (0.4918)
angleLink1 = -0.1840; % angle of the first joint in rad (0.1840)
angleLink2 = 0.9926; % angle of the second joint in rad (-0.9926)
% angleLink0 = 0; % rad, angle at the base (0) test when robot is facing front
% angleLink1 = -0.1840; % angle of the first joint in rad (0.1840)
% angleLink2 = 0.9926; % angle of the second joint in rad (-0.9926)

gear1 = 100;
gear2 = 100;
gear3 = 160;

% Joint damping, friction and stiffness
damp1 = 1.95*1e-3*gear1^2;
damp2 = 2.2*1e-3*gear2^2;
damp3 = 4.7699*1e-4*gear3^2;
% stif1 = 1.2*1e5;
% stif2 = 1.2*1e5;
% stif3 = 2.9*1e4;
coul1 = 0.2*gear1*0; % no consideration of the coulomb friction (adds a 10^-6 contribution but still)
coul2 = 0.14*gear2*0;
coul3 = 0.07*gear3*0;
% damp1 = 0;
% damp2 = 0;
% damp3 = 0;
% coul1 = 0;
% coul2 = 0;
% coul3 = 0;
stif1 = 0;
stif2 = 0;
stif3 = 0;
coulombParam = 100; % steepness of the tanh in coulomb friction (initially at 100)

alpha = 0.0001;  % Damping coeff proportional to mass of the beams
beta = 0.015; % Damping coeff proportional to stiffness of the beams

rho_num = 0; % 0.01 before
scaling = 1e6;
tol = 1e-6;
% listM = [1 0 0];
% listKt = [1 0 0];
% listCt = [1 0 0];
% listPhiq = [1 0 0];
% listM = [1 1 1];
% listKt = [1 1 1];
% listCt = [1 1 1];
% listPhiq = [1 1 1];
% listM = 3;
% listKt = 3;
% listCt = 3;
% listM = 1;
% listKt = 1;
% listCt = 1;
% listPhiq = 1;
%%
link1.E = 210e9; % link modulus in N/m�
link1.l = 0.9595; % link length in m
link1.rho = 7800; % mass density in kg/m�
link1.massCorr = 1; % Correction factor on the mass 1.7*
link1.inertiaCorr = 1; % Correction factor on the inertia 2.5*
link1.A = 3.01e-4; % cross section area of the link in m� previously 3.01e-4
link1.m = link1.l * link1.A * link1.rho; % mass of link in kg
% link1.m = 2.25; % mass of link in kg
link1.J = diag([5.1800e-4 0.1625 0.1625]); % inertia in kg*m�
link1.nu = 0.27; % poisson coef
link1.G = link1.E/(2*(1+link1.nu)); % compression modulus
link1.Ixx = 5.9e-8; % area moment inertia in case of beam in m^4 previously 3.85e-8 
link1.Iyy = 1.75e-8; % area moment inertia in case of beam in m^4 previously 1.75e-8
link1.Izz = 2.1e-8; % area moment inertia in case of beam in m^4 previously 2.1e-8
link1.KCS = diag([link1.E*link1.A link1.G*link1.A link1.G*link1.A link1.G*link1.Ixx link1.E*link1.Iyy link1.E*link1.Izz]);
link1.MCS = diag(link1.rho*[link1.A link1.A link1.A link1.Ixx link1.Iyy link1.Izz]);
link1.nodes = [0 0 0;...
               link1.l*cos(angleLink1)*cos(angleLink0) link1.l*cos(angleLink1)*sin(angleLink0) link1.l*sin(-angleLink1)]; % nodes of the body, starting with bottom to top (center of mass in the middle)
link1.nElem = 2; % Number of elements composing the link
% adding necessary nodes
link1.nodes = CreateInterCoord(link1.nodes,link1.nElem,1,2);
%%
link2.E = 210e9; % link modulus in N/m�
link2.l = 0.9340; % link length in m (from motor to tip)
link2.lc = 0.8740; % link length in m (from motor to center of mass at the tip)
link2.rho = 7800; % mass density in kg/m�
link2.A = 2.09e-4; % cross section area of the link in m� previously 2.09e-4
link2.m = link2.lc * link2.A * link2.rho; % mass of link in kg
% link2.m = 1.4; % mass of link in kg
link2.J = diag([2.18e-4 0.08374 0.08374]); % inertia in kg*m�
link2.nu = 0.27; % poisson coef
link2.G = link2.E/(2*(1+link2.nu)); % compression modulus
link2.Ixx = 2.85e-8; % area moment inertia in case of beam in m^4 previously 1.76e-8 
link2.Iyy = 0.75e-8; % area moment inertia in case of beam in m^4 previously 0.75e-8 
link2.Izz = 1.01e-8; % area moment inertia in case of beam in m^4 previously 1.01e-8 
link2.KCS = diag([link2.E*link2.A link2.G*link2.A link2.G*link2.A link2.G*link2.Ixx link2.E*link2.Iyy link2.E*link2.Izz]);
link2.MCS = diag(link2.rho*[link2.A link2.A link2.A link2.Ixx link2.Iyy link2.Izz]);
link2.nodes = [link1.nodes(end,1) link1.nodes(end,2) link1.nodes(end,3);...
               (link2.lc*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*cos(angleLink0) (link2.lc*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*sin(angleLink0) link1.nodes(end,3)-link2.lc*sin(angleLink2+angleLink1);...
               (link2.l*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*cos(angleLink0) (link2.l*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*sin(angleLink0) link1.nodes(end,3)-link2.l*sin(angleLink2+angleLink1)]; % nodes of the body, starting with bottom to top (center of mass in the middle)
link2.nElem = 2; % Number of elements composing the link
% adding necessary nodes
link2.nodes = CreateInterCoord(link2.nodes,link2.nElem,1,2);
%%
joint1.axis = [0 0 0 0 0 1]';
joint1.stiff = stif1;
joint1.coul = coul1; % 1.4*
joint1.damp = damp1;

%%
joint2.axis = [0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]'/norm([0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]);
joint2.stiff = stif2;
joint2.coul = coul2;
joint2.damp = damp2; % 0.5*

%%
joint3.axis = [0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]'/norm([0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]);
joint3.stiff = stif3;
joint3.coul = coul3;
joint3.damp = damp3; % 1.3*

%% Link1 rotation
X1 = (link1.nodes(end,:)-link1.nodes(1,:))/norm(link1.nodes(end,:)-link1.nodes(1,:));
Y1 = joint2.axis(4:6)';
Z1 = cross(X1,Y1);
A1 = [X1; Y1; Z1];

% Motor - Brak - gear inertia
J_motor1 = (0.33+0.011+1.69)*10^-4;
J_motor2 = (0.33+0.011+1.69)*10^-4;
J_motor3 = (0.33+0.011+0.193)*10^-4;

% Base inertia
% J_base = 1*diag([0 0 0.3+(J_motor1)*gear1^2]); % 0.05
% J_base = 1*diag([0 0 0.3+(J_motor1)]); % 0.05
J_baseM = diag([0 0 (J_motor1)*gear1]);
J_base = diag([0 0 0.3]);

% Shoulder inertia
% J_shoulder = diag([0 (J_motor2)*gear2^2 0]);
% J_shoulder = diag([0 (J_motor2)*gear2 0]);
J_shoulderM = diag([0 (J_motor2)*gear2 0]);
J_shoulder = diag([0 0 0]);

% Elbow mass
m_elbow = (3.3+3); % previously 3.3+3     0.85*
% J_elbow = 1*diag([0.0299 0.0177+(J_motor3)*gear3^2 0.0299]); % 1.5*   then 1.3
% J_elbow = 1*diag([0.0299 0.0177+(J_motor3)*gear3 0.0299]); % 1.5*   then 1.3
J_elbowM = diag([0 (J_motor3)*gear3 0]);
J_elbow = diag([0.0299 0.0177 0.0299]); % 1.5*   then 1.3

% End effector mass
m_end = 2.1; % previously 2.1    then 1.4*     then 1.2
J_end = diag([2.066e-3 4.378e-3 3.3e-3]);
Xend = (link2.nodes(end,:)-link2.nodes(1,:))/norm(link2.nodes(end,:)-link2.nodes(1,:));
Yend = joint3.axis(4:6)';
Zend = cross(Xend,Yend);
Aend = [Xend; Yend; Zend];

%%
% put all nodes' coord into one matrix only     
nodes = [link1.nodes(1,:);...
         link1.nodes(1,:);...
         link1.nodes;...
         link2.nodes]; % List of nodes to build the model

% number all nodes properly so they can be used by FEMmodel
nodes = [1:size(nodes,1);nodes']';
     
% creating first flexible link with link.nElem number of elements (rotating aroud z)
count = 0;
Start1 = 3; % first node of the 1st link
for i = 1:link1.nElem
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start1-1+i Start1+i];
    elements{count+i}.KCS = link1.KCS;
    elements{count+i}.MCS = diag([link1.massCorr*ones(1,3) link1.inertiaCorr*ones(1,3)])*link1.MCS;
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;
%     elements{count+i}.listM = listM;
%     elements{count+i}.listCt = listCt;
%     elements{count+i}.listKt = listKt;
%     elements{count+i}.listPhiq = listPhiq;
    elements{count+i}.g = grav;
end

% creating first flexible link with link.nElem number of elements (rotating aroud y)
count = size(elements,2);
Start2 = Start1+link1.nElem+1; % first node of the 2nd link
for i = 1:link2.nElem + 1 % +1 is to take into account that the end mass is not actually at the end...
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start2-1+i Start2+i];
    elements{count+i}.KCS = link2.KCS;
    elements{count+i}.MCS = link2.MCS;
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;    
%     elements{count+i}.listM = listM;
%     elements{count+i}.listCt = listCt;
%     elements{count+i}.listKt = listKt;
%     elements{count+i}.listPhiq = listPhiq;
    elements{count+i}.g = grav;
end

% creating rigid body joint(rotating aroud Z) Base
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [2];
% elements{count}.mass = 0.3;
elements{count}.J = J_base;
elements{count}.g = grav;
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;

% creating rigid body joint(rotating aroud y) inertia of motor 2 (shoulder)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [3];
% elements{count}.mass = 0.3;
elements{count}.J = A1*J_shoulder*A1'; % Inertia in right frame
elements{count}.g = grav;
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;

% creating rigid body joint(rotating aroud y) Elbow
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2];
elements{count}.mass = m_elbow;
% elements{count}.J = A1*diag([0.0299 0.0177+((0.33+0.011+0.193)*10^-4)*gear3^2 0.0299])*A1';
elements{count}.J = A1*J_elbow*A1';
elements{count}.g = grav;
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;

% creating rigid body end-effector
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end-1,1)];
elements{count}.mass = m_end;
elements{count}.J = Aend*J_end*Aend';
elements{count}.g = grav;
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;

% Creating the kinematic constraints
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.k = joint1.stiff;
elements{count}.d = joint1.damp;
elements{count}.coulomb = joint1.coul;
elements{count}.coulombParam = coulombParam;
elements{count}.A = joint1.axis;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.k = joint2.stiff;
elements{count}.d = joint2.damp;
elements{count}.coulomb = joint2.coul;
elements{count}.coulombParam = coulombParam;
elements{count}.A = joint2.axis;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.k = joint3.stiff;
elements{count}.d = joint3.damp;
elements{count}.coulomb = joint3.coul;
elements{count}.coulombParam = coulombParam;
elements{count}.A = joint3.axis;

% End effector Trajectory
timeVector = 0:timestepsize:finaltime;
% --------- different trajectories ----------
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\trajxSimulink_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\trajySimulink_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\trajzSimulink_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\timeSimulink_',duration])
trajx = zeros(size(timeVector));
trajy = zeros(size(timeVector));
trajz = zeros(size(timeVector));
indexI = find(timeVector==t_i,1);
indexF = find(timeVector==t_f,1);
lengthTime = length(timeVector);
trajx(1:indexI) = trajxSimulink(1)*ones(1,indexI);
trajz(1:indexI) = trajySimulink(1)*ones(1,indexI);
trajy(1:indexI) = -trajzSimulink(1)*ones(1,indexI);
trajx(indexF:lengthTime) = trajxSimulink(end)*ones(1,lengthTime-indexF+1);
trajz(indexF:lengthTime) = trajySimulink(end)*ones(1,lengthTime-indexF+1);
trajy(indexF:lengthTime) = -trajzSimulink(end)*ones(1,lengthTime-indexF+1);
trajx(indexI:indexF) = interp1(timeSimulink,trajxSimulink,0:timestepsize:t_f-t_i,'linear');
trajz(indexI:indexF) = interp1(timeSimulink,trajySimulink,0:timestepsize:t_f-t_i,'linear');
trajy(indexI:indexF) = -interp1(timeSimulink,trajzSimulink,0:timestepsize:t_f-t_i,'linear');
% plot(timeVector,[trajx;trajy;trajz])

load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\S_',duration,'_testArticle'])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\D_',duration,'_testArticle'])
ImposedJoint1_flex = S.model.listElementVariables{1}.relCoo;
ImposedJoint2_flex = S.model.listElementVariables{2}.relCoo;
ImposedJoint3_flex = S.model.listElementVariables{3}.relCoo;
ImposedJoint1_rigid = D.model.listElementVariables{1}.relCoo;
ImposedJoint2_rigid = D.model.listElementVariables{2}.relCoo;
ImposedJoint3_rigid = D.model.listElementVariables{3}.relCoo;
ImposedJointVel1_flex = S.model.listElementVariables{1}.velocity;
ImposedJointVel2_flex = S.model.listElementVariables{2}.velocity;
ImposedJointVel3_flex = S.model.listElementVariables{3}.velocity;
ImposedJointVel1_rigid = D.model.listElementVariables{1}.velocity;
ImposedJointVel2_rigid = D.model.listElementVariables{2}.velocity;
ImposedJointVel3_rigid = D.model.listElementVariables{3}.velocity;

ImposedTorque1_flex = S.model.listElementVariables{end}.value(1,:);
ImposedTorque2_flex = S.model.listElementVariables{end}.value(2,:);
ImposedTorque3_flex = S.model.listElementVariables{end}.value(3,:);
ImposedTorque1_rigid = D.model.listElementVariables{end}.value(1,:);
ImposedTorque2_rigid = D.model.listElementVariables{end}.value(2,:);
ImposedTorque3_rigid = D.model.listElementVariables{end}.value(3,:);

% ImposedJoint1_rigid = lineTraj(0,-1,timeVector,0,finaltime);
% ImposedJoint2_rigid = lineTraj(0,-0.5,timeVector,0,finaltime);
% ImposedJoint3_rigid = lineTraj(0,-0.5,timeVector,0,finaltime);

% count = size(elements,2)+1;
% elements{count}.type = 'ImposeRelativeCoordinateMotion';
% elements{count}.elements = [count-3 count-2 count-1];
% elements{count}.ImposeMotion = [ImposedJoint1_rigid;ImposedJoint2_rigid;ImposedJoint3_rigid];

% count = size(elements,2)+1;
% elements{count}.type = 'ForceInKinematicConstraint';
% elements{count}.elements = [count-3];
% elements{count}.f = [ImposedTorque1_rigid];
% 
% count = size(elements,2)+1;
% elements{count}.type = 'ForceInKinematicConstraint';
% elements{count}.elements = [count-3];
% elements{count}.f = [ImposedTorque2_rigid];
% 
% count = size(elements,2)+1;
% elements{count}.type = 'ForceInKinematicConstraint';
% elements{count}.elements = [count-3];
% elements{count}.f = [ImposedTorque3_rigid];

kp = 0;
kd = 0;
count = size(elements,2)+1;
elements{count}.type = 'ForceInKinematicConstraintPD';
elements{count}.elements = [count-3];
elements{count}.ref = ImposedJoint1_rigid;
elements{count}.dref = ImposedJointVel1_rigid;
elements{count}.kp = kp;
elements{count}.kd = kd;
elements{count}.f = [ImposedTorque1_rigid];

count = size(elements,2)+1;
elements{count}.type = 'ForceInKinematicConstraintPD';
elements{count}.elements = [count-3];
elements{count}.ref = ImposedJoint2_rigid;
elements{count}.dref = ImposedJointVel2_rigid;
elements{count}.kp = kp;
elements{count}.kd = kd;
elements{count}.f = [ImposedTorque2_rigid];

count = size(elements,2)+1;
elements{count}.type = 'ForceInKinematicConstraintPD';
elements{count}.elements = [count-3];
elements{count}.ref = ImposedJoint3_rigid;
elements{count}.dref = ImposedJointVel3_rigid;
elements{count}.kp = kp;
elements{count}.kd = kd;
elements{count}.f = [ImposedTorque3_rigid];

% Initial conditions
in = 1; % time step choosen for initial condition
initialCondition = [];
for n = S.model.listNumberNodes
    x = S.model.listNodes{n}.position(:,in);
    R = dimR(S.model.listNodes{n}.R(:,in));
    H = [R x; 0 0 0 1];
    h = logSE3(H);
    initialCondition = [initialCondition; [S.model.listNodes{n}.listNumberDof' h S.model.listNodes{n}.velocity(:,in) S.model.listNodes{n}.acceleration(:,in)]];
end
for n = S.model.listNumberElementVariables
    if strcmp(S.model.listElementVariables{n}.DofType,'MotionDof')
        initialCondition = [initialCondition; [S.model.listElementVariables{n}.listNumberDof' S.model.listElementVariables{n}.relCoo(:,in) S.model.listElementVariables{n}.velocity(:,in) S.model.listElementVariables{n}.acceleration(:,in)]];
    elseif strcmp(S.model.listElementVariables{n}.DofType,'LagrangeMultiplier')
        initialCondition = [initialCondition; [S.model.listElementVariables{n}.listNumberDof' zeros(S.model.listElementVariables{n}.nL,1) S.model.listElementVariables{n}.value(:,in) zeros(size(S.model.listElementVariables{n}.value(:,in)))]];
    end
end

clear S
clear D
% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
D.parameters.relTolRes = tol;
D.parameters.scaling = scaling;
D.runIntegration();

ModelF = FEModel();
ModelF.CreateFEModel(nodes,elements);
ModelF.defineBC(BC);

% ModelF.listElements{end}.ImposeMotion = [ImposedJoint1_flex; ImposedJoint2_flex; ImposedJoint3_flex];

ModelF.listElements{end-2}.f = ImposedTorque1_flex;
ModelF.listElements{end-1}.f = ImposedTorque2_flex;
ModelF.listElements{end}.f = ImposedTorque3_flex;

ModelF.listElements{end-2}.ref = ImposedJoint1_flex;
ModelF.listElements{end-1}.ref = ImposedJoint2_flex;
ModelF.listElements{end}.ref = ImposedJoint3_flex;
ModelF.listElements{end-2}.dref = ImposedJointVel1_flex;
ModelF.listElements{end-1}.dref = ImposedJointVel2_flex;
ModelF.listElements{end}.dref = ImposedJointVel3_flex;

S = DynamicIntegration(ModelF);
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = timestepsize;
S.parameters.rho = rho_num;
S.parameters.relTolRes = tol;
S.parameters.scaling = scaling;
S.runIntegration();
% S.runIntegration(initialCondition);

%% plots

% analys = poleAnalysisOpti(S,1,0)

EndEffector_rigid = D.model.listNodes{end};
EndEffector_flex = S.model.listNodes{end};
figure
hold on
plot(timeVector, EndEffector_rigid.position,'--','Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(timeVector, EndEffector_flex.position, '-','Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(timeVector, [trajx; trajy;trajz],':','Linewidth',2)
xlabel('Time [s]','Fontsize',14)
ylabel('Position [m]','Fontsize',14)
legend('Rigid_{x}','Rigid_{y}','Rigid_{z}','FEM_{x}','FEM_{y}','FEM_{z}','Desired_x','Desired_y','Desired_z')
line([t_i t_i],[-2 2],'color','k','LineWidth',2)
line([t_i+str2double(duration)/100 t_i+str2double(duration)/100],[-2 2],'color','k','LineWidth',2)
grid on

RelError = zeros(size(timeVector));
RelErrorR = zeros(size(timeVector));
for i = 1:length(RelError)
    RelError(i) = 100*(norm([trajx(i)-EndEffector_flex.position(1,i) trajy(i)-EndEffector_flex.position(2,i) trajz(i)-EndEffector_flex.position(3,i)])/norm([trajx(i) trajy(i) trajz(i)]));
    RelErrorR(i) = 100*(norm([trajx(i)-EndEffector_rigid.position(1,i) trajy(i)-EndEffector_rigid.position(2,i) trajz(i)-EndEffector_rigid.position(3,i)])/norm([trajx(i) trajy(i) trajz(i)]));
end
RMSRelError = sqrt(mean(RelError.^2))
MaxRelError = max(RelError)
RMSRelErrorR = sqrt(mean(RelErrorR.^2))
MaxRelErrorR = max(RelErrorR)

joint1_rigid = D.model.listElementVariables{1};
joint1_flex = S.model.listElementVariables{1};
joint2_rigid = D.model.listElementVariables{2};
joint2_flex = S.model.listElementVariables{2};
joint3_rigid = D.model.listElementVariables{3};
joint3_flex = S.model.listElementVariables{3};
figure
hold on
plot(timeVector, [joint1_rigid.relCoo;joint2_rigid.relCoo;joint3_rigid.relCoo],'--','Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(timeVector, [joint1_flex.relCoo;joint2_flex.relCoo;joint3_flex.relCoo], '-','Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(timeVector, [ImposedJoint1_rigid; ImposedJoint2_rigid; ImposedJoint3_rigid],':','Linewidth',2)
xlabel('Time [s]','Fontsize',14)
ylabel('Joint angle [m]','Fontsize',14)
legend('Rigid_{x}','Rigid_{y}','Rigid_{z}','FEM_{x}','FEM_{y}','FEM_{z}','Desired_x','Desired_y','Desired_z')
line([t_i t_i],[-2 2],'color','k','LineWidth',2)
line([t_i+str2double(duration)/100 t_i+str2double(duration)/100],[-2 2],'color','k','LineWidth',2)
grid on

joint1_rigidlagrange = D.model.listElementVariables{4}.value(6,:);
joint1_flexlagrange = S.model.listElementVariables{4}.value(6,:);
joint2_rigidlagrange = D.model.listElementVariables{5}.value(5,:);
joint2_flexlagrange = S.model.listElementVariables{5}.value(5,:);
joint3_rigidlagrange = D.model.listElementVariables{6}.value(5,:);
joint3_flexlagrange = S.model.listElementVariables{6}.value(5,:);
figure
hold on
plot(timeVector, [joint1_rigidlagrange;joint2_rigidlagrange;joint3_rigidlagrange],'--','Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(timeVector, [joint1_flexlagrange;joint2_flexlagrange;joint3_flexlagrange], '-','Linewidth',2)
xlabel('Time [s]','Fontsize',14)
ylabel('Joint effort (lagrange) [Nm]','Fontsize',14)
legend('Rigid_{x}','Rigid_{y}','Rigid_{z}','FEM_{x}','FEM_{y}','FEM_{z}')
line([t_i t_i],[-2 2],'color','k','LineWidth',2)
line([t_i+str2double(duration)/100 t_i+str2double(duration)/100],[-2 2],'color','k','LineWidth',2)
grid on

figure
hold on
plot(timeVector, [ImposedTorque1_flex ;ImposedTorque2_flex;ImposedTorque3_flex],'-','Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(timeVector, [ImposedTorque1_rigid;ImposedTorque2_rigid;ImposedTorque3_rigid], '--','Linewidth',2)
xlabel('Time [s]','Fontsize',14)
ylabel('Joint effort (imposed) [Nm]','Fontsize',14)
legend('FEM_{x}','FEM_{y}','FEM_{z}','Rigid_{x}','Rigid_{y}','Rigid_{z}')
line([t_i t_i],[-200 50],'color','k','LineWidth',2)
line([t_i+str2double(duration)/100 t_i+str2double(duration)/100],[-200 50],'color','k','LineWidth',2)
grid on


