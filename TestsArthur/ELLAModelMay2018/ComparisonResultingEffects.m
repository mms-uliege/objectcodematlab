%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Comparison of Rigid and Flexible assumptions on Simulink model for
% different scaling time trajectories
%
%% Wanted result
duration = '115'; % '11' '115' '12' '125' or '13'

listMeasured = {'115'}; % list of measured datas {'115','xx',...,'yy'}
if strcmp(duration,listMeasured)
    measured = 1;
else
    measured = 0;
end
%%
%
% -------     Flexible   -------
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingTorqueFlex_',duration],'resultingTorque')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingMotorAnglesFlex_',duration],'resultingMotorAngles')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAnglesFlex_',duration],'resultingLinkAngles')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc1Flex_',duration],'resultingLinkAcc1')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc2Flex_',duration],'resultingLinkAcc2')
% -------      Rigid     -------
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingTorqueRigid_',duration],'resultingTorque')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingMotorAnglesRigid_',duration],'resultingMotorAngles')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAnglesRigid_',duration],'resultingLinkAngles')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc1Rigid_',duration],'resultingLinkAcc1')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc2Rigid_',duration],'resultingLinkAcc2')
% -------   Flexible FF  -------
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingTorqueFlexFF_',duration],'resultingTorque')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingMotorAnglesFlexFF_',duration],'resultingMotorAngles')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAnglesFlexFF_',duration],'resultingLinkAngles')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc1FlexFF_',duration],'resultingLinkAcc1')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc2FlexFF_',duration],'resultingLinkAcc2')

%% Loading resulting behavior from simulink or actual

% -------     Flexible   -------

load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingTorqueFlex_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingMotorAnglesFlex_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAnglesFlex_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc1Flex_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc2Flex_',duration])

torqueFlex = resultingTorque;
motorAnglesFlex = resultingMotorAngles;
linkAnglesFlex = resultingLinkAngles;
linkAcc1Flex = resultingLinkAcc1;
linkAcc2Flex = resultingLinkAcc2;

% -------      Rigid     -------

load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingTorqueRigid_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingMotorAnglesRigid_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAnglesRigid_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc1Rigid_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc2Rigid_',duration])

torqueRigid = resultingTorque;
motorAnglesRigid = resultingMotorAngles;
linkAnglesRigid = resultingLinkAngles;
linkAcc1Rigid = resultingLinkAcc1;
linkAcc2Rigid = resultingLinkAcc2;

% -------   Flexible FF  -------

% load('C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingTorqueFlexFF_',duration])
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingMotorAnglesFlexFF_',duration])
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAnglesFlexFF_',duration])
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc1FlexFF_',duration])
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc2FlexFF_',duration])
% 
% torqueFlexFF = resultingTorque;
% motorAnglesFlexFF = resultingMotorAngles;
% linkAnglesFlexFF = resultingLinkAngles;
% linkAcc1FlexFF = resultingLinkAcc1;
% linkAcc2FlexFF = resultingLinkAcc2;

%% Measured values on real Ella
% -------    Reference   -------
virtualExtraTimeActual = 10;
virtualExtraTimeRef = 10.2;
if measured
    load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Measurements\refTraj_',duration])

    tRef = TARGET_DATA____measuredData_MMot_0_(1,:)+virtualExtraTimeRef;
    torqueRef(1,:) = TARGET_DATA____measuredData_MMot_0_(2,:);
    torqueRef(2,:) = TARGET_DATA____measuredData_MMot_1_(2,:);
    torqueRef(3,:) = TARGET_DATA____measuredData_MMot_2_(2,:);
    linkAcc2Ref(1,:) = TARGET_DATA____measuredData_aIMU2_0_(2,:);
    linkAcc2Ref(2,:) = TARGET_DATA____measuredData_aIMU2_1_(2,:);
    linkAcc2Ref(3,:) = TARGET_DATA____measuredData_aIMU2_2_(2,:);

    % -------    Reference qM  -------

    load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Measurements\flat_qM_',duration])

    tqM = TARGET_DATA____measuredData_MMot_0_(1,:)+virtualExtraTimeRef;
    torqueqM(1,:) = TARGET_DATA____measuredData_MMot_0_(2,:);
    torqueqM(2,:) = TARGET_DATA____measuredData_MMot_1_(2,:);
    torqueqM(3,:) = TARGET_DATA____measuredData_MMot_2_(2,:);
    linkAcc2qM(1,:) = TARGET_DATA____measuredData_aIMU2_0_(2,:);
    linkAcc2qM(2,:) = TARGET_DATA____measuredData_aIMU2_1_(2,:);
    linkAcc2qM(3,:) = TARGET_DATA____measuredData_aIMU2_2_(2,:);

    % -------    Reference qM FF -------

    load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Measurements\flat_qM_torque_',duration])

    tqMFF = TARGET_DATA____measuredData_MMot_0_(1,:)+virtualExtraTimeRef;
    torqueqMFF(1,:) = TARGET_DATA____measuredData_MMot_0_(2,:);
    torqueqMFF(2,:) = TARGET_DATA____measuredData_MMot_1_(2,:);
    torqueqMFF(3,:) = TARGET_DATA____measuredData_MMot_2_(2,:);
    linkAcc2qMFF(1,:) = TARGET_DATA____measuredData_aIMU2_0_(2,:);
    linkAcc2qMFF(2,:) = TARGET_DATA____measuredData_aIMU2_1_(2,:);
    linkAcc2qMFF(3,:) = TARGET_DATA____measuredData_aIMU2_2_(2,:);

    % -------    FE actual   -------

    load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Measurements\fe_',duration])

    tActual = TARGET_DATA____measuredData_MMot_0_(1,:)+virtualExtraTimeActual;
    torqueActual(1,:) = TARGET_DATA____measuredData_MMot_0_(2,:);
    torqueActual(2,:) = TARGET_DATA____measuredData_MMot_1_(2,:);
    torqueActual(3,:) = TARGET_DATA____measuredData_MMot_2_(2,:);
    linkAcc2Actual(1,:) = TARGET_DATA____measuredData_aIMU2_0_(2,:);
    linkAcc2Actual(2,:) = TARGET_DATA____measuredData_aIMU2_1_(2,:);
    linkAcc2Actual(3,:) = TARGET_DATA____measuredData_aIMU2_2_(2,:);

    % -------    FE actual FF  -------

    load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Measurements\fe_torque_',duration])

    tActualFF = TARGET_DATA____measuredData_MMot_0_(1,:)+virtualExtraTimeActual;
    torqueActualFF(1,:) = TARGET_DATA____measuredData_MMot_0_(2,:);
    torqueActualFF(2,:) = TARGET_DATA____measuredData_MMot_1_(2,:);
    torqueActualFF(3,:) = TARGET_DATA____measuredData_MMot_2_(2,:);
    linkAcc2ActualFF(1,:) = TARGET_DATA____measuredData_aIMU2_0_(2,:);
    linkAcc2ActualFF(2,:) = TARGET_DATA____measuredData_aIMU2_1_(2,:);
    linkAcc2ActualFF(3,:) = TARGET_DATA____measuredData_aIMU2_2_(2,:);
end
%% Loading FE computed results

% -------     FE Flex    -------
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\EndPoseCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointPoseCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointTorqueCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointVelCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\TimeCutActual_',duration])
FlexTime = Time+virtualExtraTimeActual;
FlexTorques = JointTorque;

% -------    FE Rigid    -------
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\EndPoseRCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointPoseRCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointTorqueRCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointVelRCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\TimeRCutActual_',duration])
RigidTime = Time+virtualExtraTimeActual;
RigidTorques = JointTorque;


%% Plotting graphs
% close all
% Torques
% figure
% subplot(3,1,1) % first plot
% hold on
% plot(torqueRigid.Time,torqueRigid.Data(:,1))
% plot(torqueFlex.Time,torqueFlex.Data(:,1))
% plot(torqueFlexFF.Time,torqueFlexFF.Data(:,1))
% plot(tRef,torqueRef(1,:))
% plot(tActual,torqueActual(1,:))
% plot(FlexTime,FlexTorques(1,:))
% plot(RigidTime,RigidTorques(1,:))
% if exist('TrajTorqueMotor')
%     plot(TrajTorqueMotor.Time+virtualExtraTimeRef,TrajTorqueMotor.Data(:,1))
% end
% title('Torque comparison from simulink')
% ylabel('Torque 1 [Nm]')
% axis([10 15 -1.5 1.5])
% grid on
% subplot(3,1,2) % second plot
% hold on
% plot(torqueRigid.Time,torqueRigid.Data(:,2))
% plot(torqueFlex.Time,torqueFlex.Data(:,2))
% plot(torqueFlexFF.Time,torqueFlexFF.Data(:,2))
% plot(tRef,torqueRef(2,:))
% plot(tActual,torqueActual(2,:))
% plot(FlexTime,FlexTorques(2,:))
% plot(RigidTime,RigidTorques(2,:))
% if exist('TrajTorqueMotor')
%     plot(TrajTorqueMotor.Time+virtualExtraTimeRef,TrajTorqueMotor.Data(:,2))
% end
% ylabel('Torque 2 [Nm]')
% axis([10 15 -0.2 2.7])
% grid on
% subplot(3,1,3) % third plot
% hold on
% plot(torqueRigid.Time,torqueRigid.Data(:,3))
% plot(torqueFlex.Time,torqueFlex.Data(:,3))
% plot(torqueFlexFF.Time,torqueFlexFF.Data(:,3))
% plot(tRef,torqueRef(3,:))
% plot(tActual,torqueActual(3,:))
% plot(FlexTime,FlexTorques(3,:))
% plot(RigidTime,RigidTorques(3,:))
% if exist('TrajTorqueMotor')
%     plot(TrajTorqueMotor.Time+virtualExtraTimeRef,TrajTorqueMotor.Data(:,3))
% end
% xlabel('Time [s]')
% ylabel('Torque 3 [Nm]')
% axis([10 15 -0.3 0.8])
% legend('u_{rigid}','u_{flex}','u_{FF}','u_{ref}','u_{actual}','u_{FE_F}','u_{FE_R}')
% grid on

figure % first plot
hold on
plot(torqueRigid.Time,torqueRigid.Data(:,1),'Linewidth',2)
plot(torqueFlex.Time,torqueFlex.Data(:,1),'Linewidth',2)
% plot(torqueFlexFF.Time,torqueFlexFF.Data(:,1),'Linewidth',2)
if measured
    plot(tRef,torqueRef(1,:),'Linewidth',2)
    plot(tqM,torqueqM(1,:),'Linewidth',2)
    plot(tqMFF,torqueqMFF(1,:),'Linewidth',2)
    plot(tActual,torqueActual(1,:),'Linewidth',2)
    plot(tActualFF,torqueActualFF(1,:),'Linewidth',2)
end
plot(FlexTime,FlexTorques(1,:),'Linewidth',2)
plot(RigidTime,RigidTorques(1,:),'Linewidth',2)
if exist('TrajTorqueMotor')
    plot(TrajTorqueMotor.Time+virtualExtraTimeRef,TrajTorqueMotor.Data(:,1),'Linewidth',2)
end
title(['Torque 1 comparison from simulink (',duration,' s traj)'])
xlabel('Time [s]','Fontsize',14)
ylabel('Torque 1 [Nm]','Fontsize',14)
axis([10 12 -1.6 0.7])
% axis([10 12 -inf inf])
if measured
    legend('u_{rigid}','u_{flex}','u_{ref}','u_{qM}','u_{qMFF}','u_{actual}','u_{actualFF}','u_{FE_F}','u_{FE_R}','Location','Best')
else
    legend('u_{rigid}','u_{flex}','u_{FE_F}','u_{FE_R}','Location','Best')
end
line([virtualExtraTimeRef virtualExtraTimeRef],[-15 15],'color','k','LineWidth',2)
line([virtualExtraTimeRef+str2double(duration)/100 virtualExtraTimeRef+str2double(duration)/100],[-15 15],'color','k','LineWidth',2)
grid on

figure % second plot
hold on
plot(torqueRigid.Time,torqueRigid.Data(:,2),'Linewidth',2)
plot(torqueFlex.Time,torqueFlex.Data(:,2),'Linewidth',2)
% plot(torqueFlexFF.Time,torqueFlexFF.Data(:,2),'Linewidth',2)
if measured
    plot(tRef,torqueRef(2,:),'Linewidth',2)
    plot(tqM,torqueqM(2,:),'Linewidth',2)
    plot(tqMFF,torqueqMFF(2,:),'Linewidth',2)
    plot(tActual,torqueActual(2,:),'Linewidth',2)
    plot(tActualFF,torqueActualFF(2,:),'Linewidth',2)
end
plot(FlexTime,FlexTorques(2,:),'Linewidth',2)
plot(RigidTime,RigidTorques(2,:),'Linewidth',2)
if exist('TrajTorqueMotor')
    plot(TrajTorqueMotor.Time+virtualExtraTimeRef,TrajTorqueMotor.Data(:,2),'Linewidth',2)
end
title(['Torque 2 comparison from simulink (',duration,' s traj)'])
xlabel('Time [s]','Fontsize',14)
ylabel('Torque 2 [Nm]','Fontsize',14)
axis([10 12 -2.4 3.8])
% axis([10 12 -inf inf])
if measured
    legend('u_{rigid}','u_{flex}','Rigid_{ref}','LEM','LEM_{FF}','FEM','FEM_{FF}','u_{FE_F}','u_{FE_R}','Location','Best')
else
    legend('u_{rigid}','u_{flex}','u_{FE_F}','u_{FE_R}','Location','Best')
end
line([virtualExtraTimeRef virtualExtraTimeRef],[-15 15],'color','k','LineWidth',2)
line([virtualExtraTimeRef+str2double(duration)/100 virtualExtraTimeRef+str2double(duration)/100],[-15 15],'color','k','LineWidth',2)
grid on

figure % third plot
hold on
plot(torqueRigid.Time,torqueRigid.Data(:,3),'Linewidth',2)
plot(torqueFlex.Time,torqueFlex.Data(:,3),'Linewidth',2)
% plot(torqueFlexFF.Time,torqueFlexFF.Data(:,3),'Linewidth',2)
if measured
    plot(tRef,torqueRef(3,:),'Linewidth',2)
    plot(tqM,torqueqM(3,:),'Linewidth',2)
    plot(tqMFF,torqueqMFF(3,:),'Linewidth',2)
    plot(tActual,torqueActual(3,:),'Linewidth',2)
    plot(tActualFF,torqueActualFF(3,:),'Linewidth',2)
end
plot(FlexTime,FlexTorques(3,:),'Linewidth',2)
plot(RigidTime,RigidTorques(3,:),'Linewidth',2)
if exist('TrajTorqueMotor')
    plot(TrajTorqueMotor.Time+virtualExtraTimeRef,TrajTorqueMotor.Data(:,3),'Linewidth',2)
end
title(['Torque 3 comparison from simulink (',duration,' s traj)'])
xlabel('Time [s]','Fontsize',14)
ylabel('Torque 3 [Nm]','Fontsize',14)
axis([10 12 -0.3 1.1])
% axis([10 12 -inf inf])
if measured
    legend('u_{rigid}','u_{flex}','Rigid_{ref}','LEM','LEM_{FF}','FEM','FEM_{FF}','u_{FE_F}','u_{FE_R}','Location','Best')
else
    legend('u_{rigid}','u_{flex}','u_{FE_F}','u_{FE_R}','Location','Best')
end
line([virtualExtraTimeRef virtualExtraTimeRef],[-15 15],'color','k','LineWidth',2)
line([virtualExtraTimeRef+str2double(duration)/100 virtualExtraTimeRef+str2double(duration)/100],[-15 15],'color','k','LineWidth',2)
grid on


% Motor angles
figure
hold on
plot(motorAnglesRigid.Time,motorAnglesRigid.Data,'--','Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(motorAnglesFlex.Time,motorAnglesFlex.Data,'Linewidth',2)
% set(gca,'ColorOrderIndex',1)
% plot(motorAnglesFlexFF.Time,motorAnglesFlexFF.Data,':','Linewidth',2)
xlabel('Time [s]','Fontsize',14)
ylabel('Angles [rad]','Fontsize',14)
axis([10 12 -inf inf])
title(['Motor Angles comparison from simulink (',duration,' s traj)'])
legend('Rigid_{M1}','Rigid_{M2}','Rigid_{M3}','LEM_{M1}','LEM_{M2}','LEM_{M3}')
% legend('qM1_{rigid}','qM2_{rigid}','qM3_{rigid}','qM1_{flex}','qM2_{flex}','qM3_{flex}','qM1_{FF}','qM2_{FF}','qM3_{FF}','Location','Best')
line([virtualExtraTimeRef virtualExtraTimeRef],[-2 1],'color','k','LineWidth',2)
line([virtualExtraTimeRef+str2double(duration)/100 virtualExtraTimeRef+str2double(duration)/100],[-2 1],'color','k','LineWidth',2)
grid on

% Motor torques
figure
hold on
plot(RigidTime,RigidTorques,'Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(FlexTime,FlexTorques,'--','Linewidth',2)
% set(gca,'ColorOrderIndex',1)
% plot(motorAnglesFlexFF.Time,motorAnglesFlexFF.Data,':','Linewidth',2)
xlabel('Time [s]','Fontsize',14)
ylabel('Torque [Nm]','Fontsize',14)
axis([10 12 -inf inf])
title(['Motor Torques comparison from FE (',duration,' s traj)'])
legend('Rigid_{M1}','Rigid_{M2}','Rigid_{M3}','FEM_{M1}','FEM_{M2}','FEM_{M3}')
% legend('qM1_{rigid}','qM2_{rigid}','qM3_{rigid}','qM1_{flex}','qM2_{flex}','qM3_{flex}','qM1_{FF}','qM2_{FF}','qM3_{FF}','Location','Best')
line([virtualExtraTimeRef virtualExtraTimeRef],[-2 1],'color','k','LineWidth',2)
line([virtualExtraTimeRef+str2double(duration)/100 virtualExtraTimeRef+str2double(duration)/100],[-2 1],'color','k','LineWidth',2)
grid on


% Link angles
figure
hold on
plot(linkAnglesRigid.Time,linkAnglesRigid.Data,'--','Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(linkAnglesFlex.Time,linkAnglesFlex.Data,'Linewidth',2)
% set(gca,'ColorOrderIndex',1)
% plot(linkAnglesFlexFF.Time,linkAnglesFlexFF.Data,':','Linewidth',2)
xlabel('Time [s]','Fontsize',14)
ylabel('Angles [rad]','Fontsize',14)
axis([10 12 -inf inf])
title(['Link Angles comparison from simulink (',duration,' s traj)'])
legend('Rigid_{A1}','Rigid_{A2}','Rigid_{A3}','LEM_{A1}','LEM_{A2}','LEM_{A3}')
% legend('qA1_{rigid}','qA2_{rigid}','qA3_{rigid}','qA1_{flex}','qA2_{flex}','qA3_{flex}','qA1_{FF}','qA2_{FF}','qA3_{FF}','Location','Best')
line([virtualExtraTimeRef virtualExtraTimeRef],[-2 1],'color','k','LineWidth',2)
line([virtualExtraTimeRef+str2double(duration)/100 virtualExtraTimeRef+str2double(duration)/100],[-2 1],'color','k','LineWidth',2)
grid on


% Link acceleration IMU 1
figure
hold on
plot(linkAcc1Rigid.Time,linkAcc1Rigid.Data,'--','Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(linkAcc1Flex.Time,linkAcc1Flex.Data,'Linewidth',2)
% set(gca,'ColorOrderIndex',1)
% plot(linkAcc1FlexFF.Time,linkAcc1FlexFF.Data,':','Linewidth',2)
xlabel('Time [s]','Fontsize',14)
ylabel('Acceleration [m/s^2]','Fontsize',14)
axis([10 12 -inf inf])
title(['Link acceleration comparison from simulink IMU 1 (',duration,' s traj)'])
legend('Rigid_{x}','Rigid_{x}','Rigid_{z}','LEM_{x}','LEM_{y}','LEM_{z}')
% legend('x_{rigid}','y_{rigid}','z_{rigid}','x_{flex}','y_{flex}','z_{flex}','x_{FF}','y_{FF}','z_{FF}','Location','Best')
line([virtualExtraTimeRef virtualExtraTimeRef],[-15 20],'color','k','LineWidth',2)
line([virtualExtraTimeRef+str2double(duration)/100 virtualExtraTimeRef+str2double(duration)/100],[-15 20],'color','k','LineWidth',2)
grid on


% Link acceleration IMU 2

% figure
% subplot(3,1,1) % first plot
% hold on
% plot(linkAcc2Rigid.Time,linkAcc2Rigid.Data(:,1))
% plot(linkAcc2Flex.Time,linkAcc2Flex.Data(:,1))
% plot(linkAcc2FlexFF.Time,linkAcc2FlexFF.Data(:,1))
% plot(tRef,linkAcc2Ref(1,:))
% plot(tActual,linkAcc2Actual(1,:))
% title('Acceleration comparison from simulink')
% ylabel('Acc 2 x [m/s^2]')
% axis([10 12 -inf inf])
% grid on
% subplot(3,1,2) % second plot
% hold on
% plot(linkAcc2Rigid.Time,linkAcc2Rigid.Data(:,2))
% plot(linkAcc2Flex.Time,linkAcc2Flex.Data(:,2))
% plot(linkAcc2FlexFF.Time,linkAcc2FlexFF.Data(:,2))
% plot(tRef,linkAcc2Ref(2,:))
% plot(tActual,linkAcc2Actual(2,:))
% ylabel('Acc 2 y [m/s^2]')
% axis([10 12 -inf inf])
% grid on
% subplot(3,1,3) % third plot
% hold on
% plot(linkAcc2Rigid.Time,linkAcc2Rigid.Data(:,3))
% plot(linkAcc2Flex.Time,linkAcc2Flex.Data(:,3))
% plot(linkAcc2FlexFF.Time,linkAcc2FlexFF.Data(:,3))
% plot(tRef,linkAcc2Ref(3,:))
% plot(tActual,linkAcc2Actual(3,:))
% xlabel('Time [s]')
% ylabel('Acc 2 z [m/s^2]')
% axis([10 12 -inf inf])
% % legend('u1_{rigid}','u2_{rigid}','u3_{rigid}','u1_{flex}','u2_{flex}','u3_{flex}')
% legend('acc_{rigid}','acc_{flex}','acc_{FF}','acc_{ref}','acc_{actual}')
% grid on
leg = {};
figure
hold on
% plot(linkAcc2Rigid.Time,linkAcc2Rigid.Data(:,1),'Linewidth',2); leg = [leg 'acc_{rigid}'];
% plot(linkAcc2Flex.Time,linkAcc2Flex.Data(:,1),'Linewidth',2); leg = [leg 'acc_{flex}'];
% plot(linkAcc2FlexFF.Time,linkAcc2FlexFF.Data(:,1),'Linewidth',2)
if measured
    plot(tRef,linkAcc2Ref(1,:),'Linewidth',2); leg = [leg 'Rigid_{ref}'];
    plot(tActual,linkAcc2Actual(1,:),'Linewidth',2); leg = [leg 'FEM'];
%     plot(tActualFF,linkAcc2ActualFF(1,:),'Linewidth',2); leg = [leg 'FEM_{FF}'];
    plot(tqM,linkAcc2qM(1,:),'Linewidth',2); leg = [leg 'LEM'];
%     plot(tqMFF,linkAcc2qMFF(1,:),'Linewidth',2); leg = [leg 'LEM_{FF}'];
end
% title(['Acceleration comparison from simulink (',duration,' s traj)'])
xlabel('Time [s]','Fontsize',14)
ylabel('Acc 2 x [m/s^2]','Fontsize',14)
axis([10 12 -inf inf])
legend(leg,'Location','Best')
line([virtualExtraTimeRef virtualExtraTimeRef],[-20 15],'color','k','LineWidth',2)
line([virtualExtraTimeRef+str2double(duration)/100 virtualExtraTimeRef+str2double(duration)/100],[-20 15],'color','k','LineWidth',2)
grid on

leg = {};
figure
hold on
% plot(linkAcc2Rigid.Time,linkAcc2Rigid.Data(:,2),'Linewidth',2); leg = [leg 'acc_{rigid}'];
% plot(linkAcc2Flex.Time,linkAcc2Flex.Data(:,2),'Linewidth',2); leg = [leg 'acc_{flex}'];
% plot(linkAcc2FlexFF.Time,linkAcc2FlexFF.Data(:,2),'Linewidth',2)
if measured
    plot(tRef,linkAcc2Ref(2,:),'Linewidth',2); leg = [leg 'Rigid_{ref}'];
    plot(tActual,linkAcc2Actual(2,:),'Linewidth',2); leg = [leg 'FEM'];
%     plot(tActualFF,linkAcc2ActualFF(2,:),'Linewidth',2); leg = [leg 'FEM_{FF}'];
    plot(tqM,linkAcc2qM(2,:),'Linewidth',2); leg = [leg 'LEM'];
%     plot(tqMFF,linkAcc2qMFF(2,:),'Linewidth',2); leg = [leg 'LEM_{FF}'];
end
% title(['Acceleration comparison from simulink (',duration,' s traj)'])
xlabel('Time [s]','Fontsize',14)
ylabel('Acc 2 y [m/s^2]','Fontsize',14)
axis([10 12 -inf inf])
legend(leg,'Location','Best')
line([virtualExtraTimeRef virtualExtraTimeRef],[-20 25],'color','k','LineWidth',2)
line([virtualExtraTimeRef+str2double(duration)/100 virtualExtraTimeRef+str2double(duration)/100],[-20 25],'color','k','LineWidth',2)
grid on

leg = {};
figure
hold on
% plot(linkAcc2Rigid.Time,linkAcc2Rigid.Data(:,3),'Linewidth',2); leg = [leg 'acc_{rigid}'];
% plot(linkAcc2Flex.Time,linkAcc2Flex.Data(:,3),'Linewidth',2); leg = [leg 'acc_{flex}'];
% plot(linkAcc2FlexFF.Time,linkAcc2FlexFF.Data(:,3),'Linewidth',2)
if measured
    plot(tRef,linkAcc2Ref(3,:),'Linewidth',2); leg = [leg 'Rigid_{ref}'];
    plot(tActual,linkAcc2Actual(3,:),'Linewidth',2); leg = [leg 'FEM'];
%     plot(tActualFF,linkAcc2ActualFF(3,:),'Linewidth',2); leg = [leg 'FEM_{FF}'];
    plot(tqM,linkAcc2qM(3,:),'Linewidth',2); leg = [leg 'LEM'];
%     plot(tqMFF,linkAcc2qMFF(3,:),'Linewidth',2); leg = [leg 'LEM_{FF}'];
end
% title(['Acceleration comparison from simulink (',duration,' s traj)'])
xlabel('Time [s]','Fontsize',14)
ylabel('Acc 2 z [m/s^2]','Fontsize',14)
axis([10 12 -inf inf])
legend(leg,'Location','Best')
line([virtualExtraTimeRef virtualExtraTimeRef],[-15 15],'color','k','LineWidth',2)
line([virtualExtraTimeRef+str2double(duration)/100 virtualExtraTimeRef+str2double(duration)/100],[-15 15],'color','k','LineWidth',2)
grid on
