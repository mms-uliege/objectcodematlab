%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plots for Second paper
close all
% clear all
%% Wanted result
duration = '115'; % '11' '115' '12' '125' or '13'

listMeasured = {'115'}; % list of measured datas {'115','xx',...,'yy'}
if strcmp(duration,listMeasured)
    measured = 1;
else
    measured = 0;
end

virtualExtraTimeActual = 10;
virtualExtraTimeRef = 10.2;
initialTime = 0.6; % [s] assumed initial time of the trajectory
finalTime = 2.15; % [s] time of the trajectory + pre/post
timeStepSize = 0.0004; % [s] wanted sampling time for the control
nStep = (finalTime-initialTime)/timeStepSize;
theoTime = virtualExtraTimeActual:timeStepSize:virtualExtraTimeActual+(finalTime-initialTime);

%% Loading FE computed results

% -------     FE Flex    -------
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\EndPoseCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointPoseCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointTorqueCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointVelCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\TimeCutActual_',duration])
FlexTime = Time+virtualExtraTimeActual;
FlexPose = JointPose;
FlexVel = JointVel;
FlexTorques = JointTorque;

% -------    FE Rigid    -------
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\EndPoseRCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointPoseRCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointTorqueRCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointVelRCutActual_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\TimeRCutActual_',duration])
RigidTime = Time+virtualExtraTimeActual;
RigidPose = JointPose;
RigidVel = JointVel;
RigidTorques = JointTorque;

%% Loading resulting behavior from simulink or actual

% -------     Flexible   -------

load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingTorqueFlex_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingMotorAnglesFlex_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAnglesFlex_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc1Flex_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc2Flex_',duration])

torqueFlex = resultingTorque;
motorAnglesFlex = resultingMotorAngles;
linkAnglesFlex = resultingLinkAngles;
linkAcc1Flex = resultingLinkAcc1;
linkAcc2Flex = resultingLinkAcc2;

% -------      Rigid     -------

load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingTorqueRigid_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingMotorAnglesRigid_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAnglesRigid_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc1Rigid_',duration])
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\resultingLinkAcc2Rigid_',duration])

torqueRigid = resultingTorque;
motorAnglesRigid = resultingMotorAngles;
linkAnglesRigid = resultingLinkAngles;
linkAcc1Rigid = resultingLinkAcc1;
linkAcc2Rigid = resultingLinkAcc2;

% interpolating to be at same time steps



%% Plots
leg = {};
% Motor angles
figure
hold on
plot(RigidTime,RigidPose,':','Linewidth',2)
% plot(motorAnglesRigid.Time,motorAnglesRigid.Data,':','Linewidth',2)
leg = [leg 'Rigid_{M1}' 'Rigid_{M2}' 'Rigid_{M3}'];
set(gca,'ColorOrderIndex',1)
plot(motorAnglesFlex.Time,motorAnglesFlex.Data,'-','Linewidth',2)
leg = [leg 'LEM_{M1}' 'LEM_{M2}' 'LEM_{M3}'];
set(gca,'ColorOrderIndex',1)
plot(FlexTime,FlexPose,'-.','Linewidth',2)
leg = [leg 'FEM_{M1}' 'FEM_{M2}' 'FEM_{M3}'];
xlabel('Time [s]','Fontsize',14)
ylabel('Input joint position [rad]','Fontsize',14)
axis([10 11.55 -inf inf])
% title(['Motor Angles comparison(',duration,' s traj)'])
legend(leg,'Location','Best')
line([virtualExtraTimeRef virtualExtraTimeRef],[-1.5 1],'color','k','LineWidth',2)
line([virtualExtraTimeRef+str2double(duration)/100 virtualExtraTimeRef+str2double(duration)/100],[-1.5 1],'color','k','LineWidth',2)
grid on

leg = {};
% difference in motor angles
ind = 10/0.0004;
figure
hold on
% plot(motorAnglesRigid.Time,motorAnglesRigid.Data-motorAnglesFlex.Data,'--','Linewidth',2)
% leg = [leg 'Rigid_{M1}' 'Rigid_{M2}' 'Rigid_{M3}'];
% set(gca,'ColorOrderIndex',1)
% plot(theoTime,motorAnglesRigid.Data(ind:ind+nStep,:)-motorAnglesFlex.Data(ind:ind+nStep,:),'-','Linewidth',2)
plot(theoTime,RigidPose'-motorAnglesFlex.Data(ind:ind+nStep,:),'-','Linewidth',2)
leg = [leg 'LEM_{M1}' 'LEM_{M2}' 'LEM_{M3}'];
set(gca,'ColorOrderIndex',1)
% plot(theoTime,motorAnglesRigid.Data(ind:ind+nStep,:)-FlexPose','-.','Linewidth',2)
plot(theoTime,RigidPose'-FlexPose','-.','Linewidth',2)
leg = [leg 'FEM_{M1}' 'FEM_{M2}' 'FEM_{M3}'];
% set(gca,'ColorOrderIndex',1)
% plot(theoTime,motorAnglesRigid.Data(ind:ind+nStep,:)-RigidPose','-.','Linewidth',2)
% leg = [leg 'Rigid_{M1}' 'Rigid_{M2}' 'Rigid_{M3}'];
xlabel('Time [s]','Fontsize',14)
ylabel('Input joint position difference [rad]','Fontsize',14)
axis([10 11.55 -inf inf])
% title(['Motor Angles comparison(',duration,' s traj)'])
legend(leg,'Location','Best')
line([virtualExtraTimeRef virtualExtraTimeRef],[-0.03 0.03],'color','k','LineWidth',2)
line([virtualExtraTimeRef+str2double(duration)/100 virtualExtraTimeRef+str2double(duration)/100],[-0.03 0.03],'color','k','LineWidth',2)
grid on

leg = {};
% Motor torques
figure
hold on
plot(RigidTime,RigidTorques,':','Linewidth',2)
% plot(torqueRigid.Time,torqueRigid.Data,':','Linewidth',2)
leg = [leg 'Rigid_{M1}' 'Rigid_{M2}' 'Rigid_{M3}'];
set(gca,'ColorOrderIndex',1)
plot(torqueFlex.Time,torqueFlex.Data,'-','Linewidth',2)
leg = [leg 'LEM_{M1}' 'LEM_{M2}' 'LEM_{M3}'];
set(gca,'ColorOrderIndex',1)
plot(FlexTime,FlexTorques,'-.','Linewidth',2)
leg = [leg 'FEM_{M1}' 'FEM_{M2}' 'FEM_{M3}'];
xlabel('Time [s]','Fontsize',14)
ylabel('Input joint torques [Nm]','Fontsize',14)
axis([10 11.55 -inf inf])
% title(['Motor Angles comparison(',duration,' s traj)'])
legend(leg,'Location','Best')
line([virtualExtraTimeRef virtualExtraTimeRef],[-1.5 1],'color','k','LineWidth',2)
line([virtualExtraTimeRef+str2double(duration)/100 virtualExtraTimeRef+str2double(duration)/100],[-1.5 1],'color','k','LineWidth',2)
grid on

gear1 = 100;
gear2 = 100;
gear3 = 160;

jointLoc = [1 2 3];
simTime = S.timeValues;
simEndPose = S.model.listNodes{end}.position;
j = 1;
for i = jointLoc
    simJointPose(j,:) = S.model.listElementVariables{i}.relCoo;
    simJointVel(j,:) = S.model.listElementVariables{i}.velocity;
    simJointAcc(j,:) = S.model.listElementVariables{i}.acceleration;
    j = j+1;
end

i = 1;
for nElemVar = S.model.listElementVariables
    if strcmp(nElemVar{1}.DofType,'Command')
        simJointTorque(i:i+nElemVar{1}.nL-1,:) = nElemVar{1}.value;
        i = i+nElemVar{1}.nL;
    end
end

% Extrapolate inputs based on loaded data
Time = initialTime:timeStepSize:finalTime;
for i = 1:size(simEndPose,1)
    EndPose(i,:) = interp1(simTime,simEndPose(i,:),Time,'spline');
end
for i = 1:size(simJointPose,1)
    JointPose(i,:) = interp1(simTime,simJointPose(i,:),Time,'spline');
    JointVel(i,:) = interp1(simTime,simJointVel(i,:),Time,'spline');
    JointAcc(i,:) = interp1(simTime,simJointAcc(i,:),Time,'spline');
    JointTorque(i,:) = interp1(simTime,simJointTorque(i,:),Time,'spline');
end
J_motor1 = (0.33+0.011+1.69)*10^-4;
J_motor2 = (0.33+0.011+1.69)*10^-4;
J_motor3 = (0.33+0.011+0.193)*10^-4;
% Adapt torque values according to gear ratios
if size(JointTorque,1) == 3;    
    JointTorque(1,:) = JointTorque(1,:)/gear1 + JointAcc(1,:)*J_motor1*gear1;
    JointTorque(2,:) = -JointTorque(2,:)/gear2 - JointAcc(2,:)*J_motor2*gear2;
    JointTorque(3,:) = -JointTorque(3,:)/gear3 - JointAcc(3,:)*J_motor3*gear3;
    
    % Adapting relative joint position to absolute joint position
    initJ0 = 0.4918; % rad, angle at the base (0.4918)
%     initJ0 = 0; % rad, angle at the base (0.4918)
    initJ1 = 0.1840; % angle of the first joint in rad (0.1840)
    initJ2 = -0.9926; % angle of the second joint in rad (-0.9926)
    JointPose(1,:) = JointPose(1,:)+initJ0;
    JointPose(2,:) = -JointPose(2,:)+initJ1;
    JointPose(3,:) = -JointPose(3,:)+initJ2;
end
Time = Time - initialTime + virtualExtraTimeActual;
% torque = timeseries(JointTorque',Time);
ExtraTime = 0:timeStepSize:virtualExtraTimeActual;
torque = timeseries([ones(length(ExtraTime),1)*JointTorque(:,1)';JointTorque'],[ExtraTime Time+virtualExtraTimeActual]);

leg = {};
figure
hold on
plot(Time,JointTorque(1,:),Time,JointTorque(2,:),Time,JointTorque(3,:),'Linewidth',2)
leg = [leg 'FEM_{M1}' 'FEM_{M2}' 'FEM_{M3}'];
set(gca,'ColorOrderIndex',1)
plot(RigidTime,RigidTorques,':','Linewidth',2)
leg = [leg 'Rigid_{M1}' 'Rigid_{M2}' 'Rigid_{M3}'];
xlabel('Time [s]','Fontsize',14)
ylabel('Input joint torques [Nm]','Fontsize',14)
legend(leg,'Location','Best')
line([virtualExtraTimeRef virtualExtraTimeRef],[-1 2],'color','k','LineWidth',2)
line([virtualExtraTimeRef+str2double(duration)/100 virtualExtraTimeRef+str2double(duration)/100],[-1 2],'color','k','LineWidth',2)
grid on

AF = [S.model.listElementVariables{1}.acceleration; S.model.listElementVariables{2}.acceleration; S.model.listElementVariables{3}.acceleration];
AR = [D.model.listElementVariables{1}.acceleration; D.model.listElementVariables{2}.acceleration; D.model.listElementVariables{3}.acceleration];
TF = S.model.listElementVariables{end}.value;
TR = D.model.listElementVariables{end}.value;
J_motor1 = (0.33+0.011+1.69)*10^-4;
J_motor2 = (0.33+0.011+1.69)*10^-4;
J_motor3 = (0.33+0.011+0.193)*10^-4;
% Adapt torque values according to gear ratios
TF(1,:) = TF(1,:)/gear1 + AF(1,:)*J_motor1*gear1;
TF(2,:) = -TF(2,:)/gear2 - AF(2,:)*J_motor2*gear2;
TF(3,:) = -TF(3,:)/gear3 - AF(3,:)*J_motor3*gear3;
TR(1,:) = TR(1,:)/gear1 + AR(1,:)*J_motor1*gear1;
TR(2,:) = -TR(2,:)/gear2 - AR(2,:)*J_motor2*gear2;
TR(3,:) = -TR(3,:)/gear3 - AR(3,:)*J_motor3*gear3;
leg = {};
figure
hold on
plot(S.timeValues,TF,'Linewidth',2)
leg = [leg 'FEM_{M1}' 'FEM_{M2}' 'FEM_{M3}'];
set(gca,'ColorOrderIndex',1)
plot(D.parameters.time,TR,':','Linewidth',2)
leg = [leg 'Rigid_{M1}' 'Rigid_{M2}' 'Rigid_{M3}'];
% plot(torqueFlex.Time,torqueFlex.Data,'.-','Linewidth',2)
% leg = [leg 'LEM_{M1}' 'LEM_{M2}' 'LEM_{M3}'];
% set(gca,'ColorOrderIndex',1)
xlabel('Time [s]','Fontsize',14)
ylabel('Input joint torques [Nm]','Fontsize',14)
axis([0.6 2.15 -inf inf])
legend(leg,'Location','Best')
line([initialTime+0.2 initialTime+0.2],[-1 2],'color','k','LineWidth',2)
line([initialTime+str2double(duration)/100+0.2 initialTime+str2double(duration)/100+0.2],[-1 2],'color','k','LineWidth',2)
% line([initialTime+0.2 initialTime+0.2],[-200 50],'color','k','LineWidth',2)
% line([initialTime+str2double(duration)/100+0.2 initialTime+str2double(duration)/100+0.2],[-200 50],'color','k','LineWidth',2)
grid on



%% Plots for sawyer robot
target_freq = 0.3; % [Hz] frequency at which arm should vibrate
amplitude = 0.12; % amplitude of the vibration 0.05 initialy
dur = '20';
preAct = 0.3; % duration of pre actuation phase
postAct = 0.2; % duration of pre actuation phase
finaltime = str2double(dur)/10 + preAct + postAct; % sec
timestepsize = 0.01; % sec
initial_time = 0.1; % Time to cut out at the beginning of the simulation

% t_i = preAct; % time at end of pre-actuation
% t_f = finaltime - postAct; % time at begining of post-actuation

timeVector = 0:timestepsize:finaltime;

blending_time1 = 0.45;
blending_time2 = 0.45;
waiting_time_1 = 0.1;
waiting_time_2 = 0.1;
approach_time = round(finaltime/4,1);
dist_x = amplitude*3/5; % tested for 0.05 initially
section1_ti = initial_time + waiting_time_1; % end of 1st waiting step (before going towards body)
section1_tf = section1_ti + approach_time; % end of 1st phase (then comes some waiting time 1)
%     section2_ti = section1_tf + waiting_time_1; % start of 2nd phase
section2_ti = section1_ti + approach_time/2; % start of 2nd phase
section2_tf = finaltime - waiting_time_2; % end of 2nd phase, then comes post actuation
section1_i_index = 1; % global starting time index
section1_f_index = round(section1_tf/timestepsize); % global ending time index of the 1st phase
section2_i_index = round(section2_ti/timestepsize); % global starting time index of the 2nd phase
section2_f_index = round(finaltime/timestepsize + 1); % global ending time index of whole traj
% section 1 (some pre actuation phase and a post actuation/waiting
% time)
trajy = smoothSineTraj(0.1833,target_freq,amplitude,timeVector,section2_ti,section2_ti+blending_time1,section2_tf-blending_time2,section2_tf);
%     trajz = nodes(end,4)*ones(size(timeVector));
trajz = smoothSineTraj(-0.1603,target_freq,amplitude,timeVector,section2_ti,section2_ti+blending_time1,section2_tf-blending_time2,section2_tf);
trajx = lineTraj(0.8810,0.8810-dist_x,timeVector,section1_ti,section1_tf);


leg = {};
figure
plot(timeVector,[trajx;trajy;trajz],'Linewidth',2)
leg = [leg 'X', 'Y', 'Z'];
xlabel('Time [s]','Fontsize',14)
ylabel('Cartesian position [m]','Fontsize',14)
legend(leg,'Location','Best')
% line([virtualExtraTimeRef virtualExtraTimeRef],[-0.03 0.03],'color','k','LineWidth',2)
% line([virtualExtraTimeRef+str2double(duration)/100 virtualExtraTimeRef+str2double(duration)/100],[-0.03 0.03],'color','k','LineWidth',2)
grid on


