%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compute the frames
C1 = eye(4);C2 = eye(4);C3 = eye(4);C4 = eye(4);Cc = eye(4);
Cc(1:3,4) = [1 1 0]';
C2(1:3,4) = [2 0 0]';
C3(1:3,4) = [2 2 0]';
C4(1:3,4) = [0 2 0]';

figure
plotFrame(C1)
plotFrame(C2)
plotFrame(C3)
plotFrame(C4)
plotFrame(Cc)
% compute the transformation and apply to initial frames
T = trotz(45)*trotx(45)*transl(4,2,0);
C1s = T*C1;
C2s = T*C2;
C3s = T*C3;
C4s = T*C4;
Ccs = T*Cc;
plotFrame(C1s)
plotFrame(C2s)
plotFrame(C3s)
plotFrame(C4s)
plotFrame(Ccs)
text(C1(1,4),C1(2,4),C1(3,4),['C1 (',num2str(C1(1,4)),', ',num2str(C1(2,4)),', ',num2str(C1(3,4)),')'])
text(C2(1,4),C2(2,4),C2(3,4),['C2 (',num2str(C2(1,4)),', ',num2str(C2(2,4)),', ',num2str(C2(3,4)),')'])
text(C3(1,4),C3(2,4),C3(3,4),['C3 (',num2str(C3(1,4)),', ',num2str(C3(2,4)),', ',num2str(C3(3,4)),')'])
text(C4(1,4),C4(2,4),C4(3,4),['C4 (',num2str(C4(1,4)),', ',num2str(C4(2,4)),', ',num2str(C4(3,4)),')'])
text(Cc(1,4),Cc(2,4),Cc(3,4),['Cc (',num2str(Cc(1,4)),', ',num2str(Cc(2,4)),', ',num2str(Cc(3,4)),')'])
text(C1s(1,4),C1s(2,4),C1s(3,4),['C1s (',num2str(C1s(1,4)),', ',num2str(C1s(2,4)),', ',num2str(C1s(3,4)),')'])
text(C2s(1,4),C2s(2,4),C2s(3,4),['C2s (',num2str(C2s(1,4)),', ',num2str(C2s(2,4)),', ',num2str(C2s(3,4)),')'])
text(C3s(1,4),C3s(2,4),C3s(3,4),['C3s (',num2str(C3s(1,4)),', ',num2str(C3s(2,4)),', ',num2str(C3s(3,4)),')'])
text(C4s(1,4),C4s(2,4),C4s(3,4),['C4s (',num2str(C4s(1,4)),', ',num2str(C4s(2,4)),', ',num2str(C4s(3,4)),')'])
text(Ccs(1,4),Ccs(2,4),Ccs(3,4),['Ccs (',num2str(Ccs(1,4)),', ',num2str(Ccs(2,4)),', ',num2str(Ccs(3,4)),')'])
axis([-0.1 3.5 -0.1 6.5 -0.1 3])
axis equal

% compute the grasp matrix
G1s = [C1s(1:3,1:3) zeros(3);tild(C1s(1:3,4))*C1s(1:3,1:3) C1s(1:3,1:3)];
G2s = [C2s(1:3,1:3) zeros(3);tild(C2s(1:3,4))*C2s(1:3,1:3) C2s(1:3,1:3)];
G3s = [C3s(1:3,1:3) zeros(3);tild(C3s(1:3,4))*C3s(1:3,1:3) C3s(1:3,1:3)];
G4s = [C4s(1:3,1:3) zeros(3);tild(C4s(1:3,4))*C4s(1:3,1:3) C4s(1:3,1:3)];
G = [G1s'; G2s'; G3s'; G4s']';
% G = [G1s']';

% compute the selection matrices
B1 = [1 0 0 0 0 0];
% B1 = eye(5);
% B1 = [B1 [0 0 0 0 0]'];
B2 = [0 1 0 0 0 0];
B3 = [0 0 1 0 0 0];
% B4 = [1 0 0 0 0 0];
B4 = [0 0 0 1 0 0;...
      0 0 0 0 1 0;...
      0 0 0 0 0 1];
B = blkdiag(B1,B2,B3,B4);
% B = blkdiag(B1);

% Real grasp matrix
G_real = (B*G')';

% Compute GG^T
GGT = G_real*G_real';
l = sqrt(eig(GGT));
