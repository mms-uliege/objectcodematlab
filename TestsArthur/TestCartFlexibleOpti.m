%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear all

finaltime = 2;
timestepsize = 0.01;
t_i = 0.2;
t_f = 1.7;

rho_num = 0.3;
scaling = 1e6;
tol = 1e-6;

nodes = [1 -1 0 0;
         2 -1 0 0;
         3 -1 0 0;
         4 -1.6614 -0.75 0;
         5 -1.6614 -0.75 0;
         6 -1.3307 -1.125 0;
         7 -1.3307 -1.125 0;
         8 -1 -1.5 0];

nodes = createInterNodes(nodes,2,3,4);
nodes = createInterNodes(nodes,2,6,7);
nodes = createInterNodes(nodes,2,9,10);


elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2];
elements{1}.mass = 3;
% elements{1}.J = eye(3);
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [4 3 5];
elements{2}.mass = 6.875;
elements{2}.J = 0.57*eye(3);
% elements{2}.g = [0 9.81 0];

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [7 6 8];
elements{3}.mass = 6.875/2;
elements{3}.J = 0.0723*eye(3);
% elements{3}.g = [0 9.81 0];

elements{4}.type = 'RigidBodyElement';
elements{4}.nodes = [10 9 11];
elements{4}.mass = 6.875/2;
elements{4}.J = 0.0723*eye(3);
% elements{4}.g = [0 9.81 0];


% elements{2}.type = 'ExternalForce';
% elements{2}.nodes = 3;
% elements{2}.DOF = 2;
% elements{2}.amplitude = -10;
% elements{2}.frequency = 0;

elements{5}.type = 'KinematicConstraint';
elements{5}.nodes = [1 2];
elements{5}.A = [1 0 0 0 0 0]';

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [2 3];
elements{6}.A = [0 0 0 0 0 1]';

elements{7}.type = 'KinematicConstraint';
elements{7}.nodes = [5 6];
elements{7}.A = [0 0 0 0 0 1]';

elements{8}.type = 'KinematicConstraint';
elements{8}.nodes = [8 9];
elements{8}.k = 50;
elements{8}.d = 0.25;
elements{8}.A = 0*[0 0 0 0 0 1]';

% Trajectory

cart_end = 1;
cart_i = -1;

x_end = 1;
x_i = -1;

y_end = -1.5;
y_i = -1.5;

r = 1;

timeVector = 0:timestepsize:finaltime;
trajcart = halfCircleTraj(cart_i,cart_end,r,timeVector,'lin',t_i,t_f);
trajx = halfCircleTraj(x_i,x_end,r,timeVector,'sin',t_i,t_f);
trajy = halfCircleTraj(y_i,y_end,r,timeVector,'cos',t_i,t_f);

elements{9}.type = 'TrajectoryConstraint';
elements{9}.nodes = [2];
elements{9}.T = [trajcart];
elements{9}.Axe = [1 0 0];
elements{9}.elements = [5];
elements{9}.active = 1;

elements{10}.type = 'TrajectoryConstraint';
elements{10}.nodes = [11];
elements{10}.T = [trajx;...
                 trajy];
elements{10}.Axe = [1 0 0;...
                   0 1 0];
elements{10}.elements = [6 7];
elements{10}.active = 1;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);


D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
D.parameters.scaling = scaling;
D.parameters.relTolRes = tol;
D.runIntegration();


%%

npts = finaltime/timestepsize + 1;
timeVector = 0:finaltime/(npts-1):finaltime;
trajcart = halfCircleTraj(cart_i,cart_end,r,timeVector,'lin',t_i,t_f);
trajx = halfCircleTraj(x_i,x_end,r,timeVector,'sin',t_i,t_f);
trajy = halfCircleTraj(y_i,y_end,r,timeVector,'cos',t_i,t_f);
Model.listElements{10}.T = [trajcart];
Model.listElements{10}.T = [trajx;...
                            trajy];
Model.listElements{10}.active = 1;
Model.listElements{10}.active = 1;

elements{8}.A = [0 0 0 0 0 1]'; % Allows motion of passive joint

ModelF = FEModel();
ModelF.CreateFEModel(nodes,elements);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from rigid solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = Model.listNodes{n}.R;
    ModelF.listNodes{n}.position = Model.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = Model.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = Model.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
%         ModelF.listElementVariables{n}.A = Model.listElementVariables{n}.A;
%         ModelF.listElementVariables{n}.nDof = Model.listElementVariables{n}.nDof;
        ModelF.listElementVariables{n}.R = Model.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = Model.listElementVariables{n}.x;
%         ModelF.listElementVariables{n}.xI0 = Model.listElementVariables{n}.xI0;
        ModelF.listElementVariables{n}.velocity = Model.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = Model.listElementVariables{n}.acceleration;
        ModelF.listElementVariables{n}.relCoo = Model.listElementVariables{n}.relCoo;
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(Model.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = Model.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

% for n = ModelF.listNumberNodes
%     ModelF.listNodes{n}.InitializeD_Opti();
% end
% for n = ModelF.listNumberElementVariables
%     ModelF.listElementVariables{n}.InitializeD_Opti();
% end

tic
S = DirectTranscriptionOpti(ModelF);
S.parameters.relTolRes = tol;
S.parameters.rho = rho_num; % 0.3 best for now
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
% S.NodeToMinimize = [7 8 9 10];
S.JointToMinimize = [8];
S.parameters.scaling = scaling;
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])

DeplCoudeOpti = ModelF.listElementVariables{4}.relCoo;
DeplCoude0 = Model.listElementVariables{4}.relCoo;
u1_init = Model.listElementVariables{end-1}.value;
u2_init = Model.listElementVariables{end}.value(1,:);
u3_init = Model.listElementVariables{end}.value(2,:);

% Plots

uCartRigid.u1_init = u1_init;
uCartRigid.u2_init = u2_init;
uCartRigid.u3_init = u3_init;
uCartRigid.time = D.parameters.time;

% save('uCartRigid','uCartRigid')

timeSteps = S.timeValues;
timeLoc = S.timesteps;

uCartFlex.u1 = ModelF.listElementVariables{end-1}.value(timeLoc);
uCartFlex.u2 = ModelF.listElementVariables{end}.value(1,timeLoc);
uCartFlex.u3 = ModelF.listElementVariables{end}.value(2,timeLoc);
uCartFlex.time = timeSteps;

% save('uCartFlex','uCartFlex')

% figure
% hold on
% plot(ModelF.listNodes{2}.position(1,timeLoc),ModelF.listNodes{2}.position(2,timeLoc),ModelF.listNodes{11}.position(1,timeLoc),ModelF.listNodes{11}.position(2,timeLoc), 'Linewidth',2)
% set(gca, 'ColorOrderIndex',1)
% plot(trajcart,zeros(size(trajcart)),trajx,trajy, 'Linewidth',2, 'Color','r')
% grid on


figure
hold on
plot(timeSteps,ModelF.listNodes{2}.position(1,timeLoc),timeSteps,ModelF.listNodes{11}.position(1,timeLoc),timeSteps,ModelF.listNodes{11}.position(2,timeLoc),'Linewidth',2)
set(gca, 'ColorOrderIndex',1)
plot(timeSteps,trajcart,'--',timeSteps,trajx,'--',timeSteps,trajy,'--','Linewidth',2)
legend('cart', 'X', 'Y','cartd','Xd','Yd')
grid on

figure
hold on
plot(timeSteps,ModelF.listElementVariables{end-1}.value(timeLoc),timeSteps,ModelF.listElementVariables{end}.value(1,timeLoc),timeSteps,ModelF.listElementVariables{end}.value(2,timeLoc),'Linewidth',2)
set(gca, 'ColorOrderIndex',1)
plot(timeSteps,u1_init(timeLoc),'--',timeSteps,u2_init(timeLoc),'--',timeSteps,u3_init(timeLoc),'--','Linewidth',2)
xlabel('Time [s]','Fontsize',16)
ylabel('Commands','Fontsize',16)
legend('Joint 1 [N]','Joint 2 [Nm]','Joint 3 [Nm]','Joint 1 rigid [N]','Joint 2 rigid [Nm]','Joint 3 rigid [Nm]','Location','Best')
% title('Commands of a flexible cart system','Fontsize',18)
grid on

figure
plot(timeSteps,DeplCoude0(timeLoc),timeSteps,DeplCoudeOpti(timeLoc),'Linewidth',2)
xlabel('Time [s]','Fontsize',16)
ylabel('$\mathbf{\eta}$ \textbf{[rad]}','interpreter','latex','Fontsize',16)
legend('Rigid','Flexible')
grid on

% nod = 8;
% comp = 6;
% 
% vx_init = Model.listNodes{nod}.velocity_InitOpti(comp,timeLoc);
% vx = Model.listNodes{nod}.velocity(comp,timeLoc);
% 
% dotv_init = Model.listNodes{nod}.acceleration_InitOpti(comp,timeLoc);
% dotv = Model.listNodes{nod}.acceleration(comp,timeLoc);
% 
% figure
% plot(timeSteps,vx_init,timeSteps,vx)
% legend('Vx_{init}','Vx')
% grid on
% 
% figure
% plot(timeSteps,dotv_init,timeSteps,dotv)
% legend('dotv_{init}','dotv')
% grid on

anlFlex = poleAnalysisOpti(S,1,0);

beta = ModelF.listElementVariables{4}.relCoo(timeLoc);
betadot = ModelF.listElementVariables{4}.velocity(timeLoc);
% beta = Model.listElementVariables{4}.relCoo_InitOpti;
% betadot = Model.listElementVariables{4}.velocity_InitOpti;
figure
axis([min(beta) max(beta) min(betadot) max(betadot)])
grid on
% title('Phase plot','FontSize',18)
xlabel('$\eta$ \textbf{[rad]}','interpreter','latex','FontSize',18)
ylabel('$\dot{\eta}$ \textbf{[rad/s]}','interpreter','latex','FontSize',18)
hold on
for i = 1:length(beta)
    if i==1
        plot(beta(i),betadot(i),'-o','MarkerFaceColor','g','MarkerEdgeColor','g','MarkerSize',12)
    elseif i==length(beta)
        plot(beta(i),betadot(i),'-o','MarkerFaceColor','r','MarkerEdgeColor','r','MarkerSize',12)
    else
        plot(beta(i),betadot(i),'-o','MarkerFaceColor','y','MarkerEdgeColor','b')
    end
    drawnow
    pause(.1)
end
set(gca, 'ColorOrderIndex',1)
plot(beta,betadot,'-','Linewidth',2)
stab = plot(10*[-anlFlex.V(64,60) anlFlex.V(64,60)], 10*[-anlFlex.V(128,60) anlFlex.V(128,60)],'Linewidth',2); % 64 corresponds to the eta value and 128 corresponds to \dot{eta}
unstab = plot(10*[-anlFlex.V(64,61) anlFlex.V(64,61)], 10*[-anlFlex.V(128,61) anlFlex.V(128,61)],'Linewidth',2); % 60 is the column number that corresponds to the unstable eigen vector and 61 is the stable eigen vector 
legend([stab, unstab],'Unstable','Stable')


xcart = ModelF.listNodes{2}.position(1,timeLoc);
ycart = ModelF.listNodes{2}.position(2,timeLoc);
x = ModelF.listNodes{end}.position(1,timeLoc);
y = ModelF.listNodes{end}.position(2,timeLoc);

RelError = zeros(size(timeSteps));
for i = 1:length(RelError)
    RelError(i) = 100*(norm([trajx(i)-x(i) trajy(i)-y(i)]))/norm([trajx(i) trajy(i)]);
end
% figure
% plot(timeSteps,RelError)
% title('Relative error of the direct dynamic trajectory','Fontsize',13)
% xlabel('Time [s]')
% ylabel('Relative Error (%)')
% grid on

figure
hold on
plot(real(anlFlex.poles(anlFlex.indexUnstable)),imag(anlFlex.poles(anlFlex.indexUnstable)),'d','MarkerFaceColor','r','MarkerEdgeColor','r')
plot(real(anlFlex.poles(anlFlex.indexStable)),imag(anlFlex.poles(anlFlex.indexStable)),'o','MarkerFaceColor','b','MarkerEdgeColor','b')
xlabel('Real part','FontSize',16)
ylabel('Imaginary part','FontSize',16)
title('Poles of the system','FontSize',18)
legend('Unstable','Stable', 'Location', 'Best')
grid on
axis([-350 350 -350 350])
viscircles([0 0],50*2*pi,'Color','k');

% save('CartFlexD','D')
% save('CartFlexS','S')

clear elements

%% Direct dynamic simulation
if true
    in = 1;
    initialCondition = [];
    for n = ModelF.listNumberNodes
        x = ModelF.listNodes{n}.position(:,in);
        R = dimR(ModelF.listNodes{n}.R(:,in));
        H = [R x; 0 0 0 1];
        h = logSE3(H);
        initialCondition = [initialCondition; [ModelF.listNodes{n}.listNumberDof' h ModelF.listNodes{n}.velocity(:,in) ModelF.listNodes{n}.acceleration(:,in)]];
    end
    for n = ModelF.listNumberElementVariables
        if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
            initialCondition = [initialCondition; [ModelF.listElementVariables{n}.listNumberDof' ModelF.listElementVariables{n}.relCoo(:,in) ModelF.listElementVariables{n}.velocity(:,in) ModelF.listElementVariables{n}.acceleration(:,in)]];
        elseif strcmp(ModelF.listElementVariables{n}.DofType,'LagrangeMultiplier')
            initialCondition = [initialCondition; [ModelF.listElementVariables{n}.listNumberDof' zeros(ModelF.listElementVariables{n}.nL,1) ModelF.listElementVariables{n}.value(:,in) zeros(size(ModelF.listElementVariables{n}.value(:,in)))]];
        end
    end
    
    elements{1}.type = 'RigidBodyElement';
    elements{1}.nodes = [2];
    elements{1}.mass = 3;
    % elements{1}.J = eye(3);

    elements{2}.type = 'RigidBodyElement';
    elements{2}.nodes = [4 3 5];
    elements{2}.mass = 6.875;
    elements{2}.J = 0.57*eye(3);
    % elements{2}.g = [0 9.81 0];

    elements{3}.type = 'RigidBodyElement';
    elements{3}.nodes = [7 6 8];
    elements{3}.mass = 6.875/2;
    elements{3}.J = 0.0723*eye(3);
    % elements{3}.g = [0 9.81 0];

    elements{4}.type = 'RigidBodyElement';
    elements{4}.nodes = [10 9 11];
    elements{4}.mass = 6.875/2;
    elements{4}.J = 0.0723*eye(3);
    % elements{4}.g = [0 9.81 0];

    elements{5}.type = 'KinematicConstraint';
    elements{5}.nodes = [1 2];
    elements{5}.A = [1 0 0 0 0 0]';

    elements{6}.type = 'KinematicConstraint';
    elements{6}.nodes = [2 3];
    elements{6}.A = [0 0 0 0 0 1]';

    elements{7}.type = 'KinematicConstraint';
    elements{7}.nodes = [5 6];
    elements{7}.A = [0 0 0 0 0 1]';

    elements{8}.type = 'KinematicConstraint';
    elements{8}.nodes = [8 9];
    elements{8}.d = 0.25; % 40 (31 commence a instable)
    elements{8}.k = 50; % 50
    elements{8}.A = [0 0 0 0 0 1]';

    elements{9}.type = 'ForceInKinematicConstraint';
    elements{9}.elements = [5];
    elements{9}.f = uCartFlex.u1;

    elements{10}.type = 'ForceInKinematicConstraint';
    elements{10}.elements = [6];
    elements{10}.f = uCartFlex.u2;

    elements{11}.type = 'ForceInKinematicConstraint';
    elements{11}.elements = [7];
    elements{11}.f = uCartFlex.u3;
    
    BC = [1];

    ModelFD = FEModel();
    ModelFD.CreateFEModel(nodes,elements);
    ModelFD.defineBC(BC);

    elements{9}.f = uCartRigid.u1_init;
    elements{10}.f = uCartRigid.u2_init;
    elements{11}.f = uCartRigid.u3_init;

    ModelRD = FEModel();
    ModelRD.CreateFEModel(nodes,elements);
    ModelRD.defineBC(BC);

    DF = DynamicIntegration(ModelFD);
    DF.parameters.finaltime = finaltime;
    DF.parameters.timestepsize = timestepsize;
    DF.parameters.rho = rho_num;
    DF.parameters.scaling = scaling;
    DF.parameters.relTolRes = tol;
    DF.runIntegration(initialCondition);
%     DF.runIntegration();
    
    DR = DynamicIntegration(ModelRD);
    DR.parameters.finaltime = finaltime;
    DR.parameters.timestepsize = timestepsize;
    DR.parameters.rho = rho_num;
    DR.parameters.scaling = scaling;
    DR.parameters.relTolRes = tol;
    DR.runIntegration();
    
    x = ModelFD.listNodes{end}.position(1,:);
    y = ModelFD.listNodes{end}.position(2,:);

    xR = ModelRD.listNodes{end}.position(1,:);
    yR = ModelRD.listNodes{end}.position(2,:);
    nPlot = 1:3:length(trajx);
    figure
    hold on
    plot(x(nPlot),y(nPlot), ':o','Linewidth',2, 'Color','r')
    plot(xR(nPlot),yR(nPlot),'--', 'Linewidth',2, 'Color','b')
    plot(trajx(nPlot),trajy(nPlot), 'Linewidth',2, 'Color','k')
    xlabel('X [m]','Fontsize',16)
    ylabel('Y [m]','Fontsize',16)
    axis([-1.1 1.1 -1.6 -0.3 -1 1])
    axis equal
    legend('With u','With u_{rigid}','Prescribed', 'Location', 'Best')
    grid on
    
    RelError = zeros(size(timeSteps));
    RelErrorR = zeros(size(timeSteps));
    for i = 1:length(RelError)
        RelError(i) = 100*(norm([trajx(i)-x(i) trajy(i)-y(i)])/norm([trajx(i) trajy(i)]));

        RelErrorR(i) = 100*(norm([trajx(i)-xR(i) trajy(i)-yR(i)])/norm([trajx(i) trajy(i)]));
    end
    figure
    plot(timeSteps,RelError,timeSteps,RelErrorR)
    title('Relative error of the direct dynamic trajectory','Fontsize',13)
    xlabel('time')
    ylabel('Relative Error (%)')
    legend('Flex','Rigid')
    grid on

    RMSRelError = sqrt(mean(RelError.^2))
    MaxRelError = max(RelError)
    RMSRelErrorR = sqrt(mean(RelErrorR.^2))
    MaxRelErrorR = max(RelErrorR)
    
    radius = [];
    radiusR = [];
    radiusDesired = 1*ones(size(timeVector));
    for i = 1:length(timeVector)
        radius(i) = norm([x(i) y(i)+1.5]);
        radiusR(i) = norm([xR(i) yR(i)+1.5]); 
    end
    figure
    hold on
    plot(timeVector, [radius; radiusR; radiusDesired],'Linewidth',2)
    xlabel('Time [s]','Fontsize',16)
    ylabel('Radius [m]','Fontsize',16)
    legend('With u','With u_{rigid}','Prescribed', 'Location', 'Best')
%     title('Radius of the circle described by the end-effector')
    grid on
end
    
