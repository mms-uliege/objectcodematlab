%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear

finaltime = 2.5;
timestepsize = 0.01;


nodes = [1 0 0 -1;
         2 0 0 -1;
         3 0 0 0;
         4 0 0 0;
         5 0 sqrt(2)/2 sqrt(2)/2;
         6 0 sqrt(2)/2 sqrt(2)/2;
         7 0 sqrt(2) 0];
     
nElem = 2;
Start = 2;
End = 3;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 5;
End = 6;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 8;
End = 9;
nodes = createInterNodes(nodes,nElem,Start,End);

elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [3 2 4];
elements{1}.mass = 6.875;
elements{2}.J = 0.57*eye(3);
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [6 5 7];
elements{2}.mass = 6.875;
elements{2}.J = 0.57*eye(3);

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [9 8 10];
elements{3}.mass = 6.875;
elements{3}.J = 0.57*eye(3);

elements{5}.type = 'KinematicConstraint';
elements{5}.nodes = [1 2];
elements{5}.A = [0 0 0 0 0 1]';

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [4 5];
elements{6}.A = [0 0 0 1 0 0]';

elements{7}.type = 'KinematicConstraint';
elements{7}.nodes = [7 8];
elements{7}.A = [0 0 0 1 0 0]';

% elements{8}.type = 'KinematicConstraint';
% elements{8}.nodes = [6 7];
% elements{8}.A = [0 0 0 0 0 1]';

% elements{9}.type = 'RotSpringDamperElement';
% elements{9}.damping = 200; % 1
% elements{9}.stiffness = 1000; % Minimum Phase 7.5
% % elements{9}.stiffness = 3; % Non-Minimum Phase
% elements{9}.nodes = [6 7];
% elements{9}.A = [0 0 1];

load('uArmRigi')
% Input torques
elements{8}.type = 'ForceInKinematicConstraint';
elements{8}.elements = [5];
elements{8}.f = u.u1_init;

elements{9}.type = 'ForceInKinematicConstraint';
elements{9}.elements = [6];
elements{9}.f = u.u2_init;

elements{10}.type = 'ForceInKinematicConstraint';
elements{10}.elements = [7];
elements{10}.f = u.u3_init;

TrajParam.points = [0 sqrt(2) 0;...
                    0 sqrt(2) 0.2;...
                    sqrt(2)/4 3*sqrt(2)/4 0.5;...
                    sqrt(2)/2 sqrt(2)/2 0.2;...
                    sqrt(2)/2 sqrt(2)/2 -.1];
                    
TrajParam.timeVector = 0:timestepsize:finaltime;
TrajParam.intervals = [0.3 0.7 1.3 1.8 2.2];

[trajx trajy trajz] = PointTraj(TrajParam);

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.0;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

% Plots

timeSteps = D.parameters.time;

endNode = 10;
figure
hold on
plot3(Model.listNodes{endNode}.position(1,:),Model.listNodes{endNode}.position(2,:),Model.listNodes{endNode}.position(3,:), 'Linewidth',3)
plot3(trajx,trajy,trajz, 'Linewidth',1, 'Color','r')
grid on

figure
hold on
plot(timeSteps,Model.listNodes{endNode}.position(1,:),timeSteps,Model.listNodes{endNode}.position(2,:),timeSteps,Model.listNodes{endNode}.position(3,:))
plot(timeSteps,trajx,'--',timeSteps,trajy,'--',timeSteps,trajz,'--')
legend('X', 'Y', 'Z','Xd','Yd','Zd')
grid on

figure
hold on
title('Velocity of end effector of a flexible Robot','Fontsize',18)
plot(timeSteps,Model.listNodes{endNode}.velocity(1,:),...
    timeSteps,Model.listNodes{endNode}.velocity(2,:),...
    timeSteps,Model.listNodes{endNode}.velocity(3,:),...
    timeSteps,Model.listNodes{endNode}.velocity(4,:),...
    timeSteps,Model.listNodes{endNode}.velocity(5,:),...
    timeSteps,Model.listNodes{endNode}.velocity(6,:))
xlabel('Time (s)','Fontsize',16)
ylabel('Velocity','Fontsize',16)
legend('1','2','3','4','5','6')
grid on

figure
hold on
title('Acceleration of end effector of a flexible Robot','Fontsize',18)
plot(timeSteps,Model.listNodes{endNode}.acceleration(1,:),...
    timeSteps,Model.listNodes{endNode}.acceleration(2,:),...
    timeSteps,Model.listNodes{endNode}.acceleration(3,:),...
    timeSteps,Model.listNodes{endNode}.acceleration(4,:),...
    timeSteps,Model.listNodes{endNode}.acceleration(5,:),...
    timeSteps,Model.listNodes{endNode}.acceleration(6,:))
xlabel('Time (s)','Fontsize',16)
ylabel('Acceleration','Fontsize',16)
legend('1','2','3','4','5','6')
grid on
