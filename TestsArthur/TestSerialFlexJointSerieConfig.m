%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear
close all

finaltime = 1.5;
timestepsize = 0.01;
t_i = 0.2;
t_f = 1.2;

a1 = 0.02;
b1 = 0.02;
l1 = 1;
a2 = 0.005;
b2 = 0.005;
l2 = l1;

m_end = 0.1; % end-effector mass

%% Creating nodes
angle = 45*pi/180;
nodes = [1 0 0 0;
         2 0 0 0;
         3 0 0 0;
         4 l1*cos(angle) 0 l1*sin(angle);
         5 l1*cos(angle) 0 l1*sin(angle);
         6 l1*cos(angle) 0 l1*sin(angle);
         7 l1*cos(angle)+l2*cos(angle) 0 0];
     
nElem1 = 2;
Start1 = 3;
End1 = 4;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

nElem2 = 4;
Start2 = End1+nElem1+1;
End2 = Start2+1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);
 
%% Rigid Model

rho = 2700;
m1 = a1*b1*l1*rho;
IxxR1 = m1*(a1^2+b1^2)/12;
IyyR1 = m1*(a1^2+l1^2)/12;
IzzR1 = m1*(b1^2+l1^2)/12;

m2 = a2*b2*l2*rho;
IxxR2 = m2*(a2^2+b2^2)/12;
IyyR2 = m2*(a2^2+l2^2)/12;
IzzR2 = m2*(b2^2+l2^2)/12;

count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elements{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR1 IyyR1 IzzR1]);
% elements{count}.g = [0 0 -9.81];
count = count +1;

elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
elements{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR2 IyyR2 IzzR2]);
% elements{count}.g = [0 0 -9.81];
count = count +1;

% count = 0;
% 
% E = 50000e9; nu = 0.3; G = E/(2*(1+nu)); rho = 2700;
% A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;
% 
% KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
% MCS = diag(rho*[A A A Ixx Iyy Izz]);
% 
% type = 'FlexibleBeamElement';
% % count = size(elementsFlex,2);
% for i = 1:nElem1
%     elements{count+i}.type = type;
%     elements{count+i}.nodes = [nodes(Start1+i-1,1) nodes(Start1+i,1)];
%     elements{count+i}.KCS = KCS;
%     elements{count+i}.MCS = MCS;
% end
% count = size(elements,2);
% for i = 1:nElem2
%     elements{count+i}.type = type;
%     elements{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
%     elements{count+i}.KCS = KCS;
%     elements{count+i}.MCS = MCS;
% end

% count = size(elements,2)+1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [Start2-1];
% elements{count}.mass = 0.2; % 0.07
% % elements{count}.g = [0 0 -9.81];

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = m_end; % 0.07
% elements{count}.g = [0 0 -9.81];

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-2 Start2-1];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'RotSpringDamperElement';
elements{count}.stiffness = 100;
elements{count}.damping = 100;
elements{count}.nodes = [Start2-1 Start2];
elements{count}.A = [0 1 0];
count = count+1;

% Trajectory

y_end = l2;
z_end = nodes(end,4);
r = y_end/2;

timeVector = 0:timestepsize:finaltime;
trajx = nodes(end,2)*ones(size(timeVector));
trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajy;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elements{count}.elements = [count-4 count-3 count-2];
elements{count}.active = 1;

% Boundary Condition
BC = [1];

% Solving
Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.0;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

uRigi.u1 = Model.listElementVariables{end}.value(1,:);
uRigi.u2 = Model.listElementVariables{end}.value(2,:);
uRigi.u3 = Model.listElementVariables{end}.value(3,:);
uRigi.time = D.parameters.time;

save('uRigiSerialFlexJointSerieConfig3D','uRigi')

%% Flexible Model
count = 0;

E = 90e9; nu = 0.3; G = E/(2*(1+nu));
A1 = a1*b1; Ixx1 = a1*b1*(a1^2+b1^2)/12; Iyy1 = b1*a1^3/12;Izz1 = a1*b1^3/12;

KCS1 = diag([E*A1 G*A1 G*A1 G*Ixx1 E*Iyy1 E*Izz1]);
MCS1 = diag(rho*[A1 A1 A1 Ixx1 Iyy1 Izz1]);

A2 = a2*b2; Ixx2 = a2*b2*(a2^2+b2^2)/12; Iyy2 = b2*a2^3/12;Izz2 = a2*b2^3/12;

KCS2 = diag([E*A2 G*A2 G*A2 G*Ixx2 E*Iyy2 E*Izz2]);
MCS2 = diag(rho*[A2 A2 A2 Ixx2 Iyy2 Izz2]);

type = 'FlexibleBeamElement';
% % count = size(elementsFlex,2); % first link (flexible)
% for i = 1:nElem1
%     elementsFlex{count+i}.type = type;
%     elementsFlex{count+i}.nodes = [nodes(Start1+i-1,1) nodes(Start1+i,1)];
%     elementsFlex{count+i}.KCS = KCS1;
%     elementsFlex{count+i}.MCS = MCS1;
% end
count = 1;
elementsFlex{count}.type = 'RigidBodyElement';   % first link (rigid)
elementsFlex{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elementsFlex{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
elementsFlex{count}.J = diag([IxxR1 IyyR1 IzzR1]);
% elementsFlex{count}.g = [0 0 -9.81];

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'RigidBodyElement';  % second link (rigid)
elementsFlex{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
elementsFlex{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
elementsFlex{count}.J = diag([IxxR2 IyyR2 IzzR2]);
% elementsFlex{count}.g = [0 0 -9.81];

% count = size(elementsFlex,2); % Second link (flexible)
% for i = 1:nElem2
%     elementsFlex{count+i}.type = type;
%     elementsFlex{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
%     elementsFlex{count+i}.KCS = KCS2;
%     elementsFlex{count+i}.MCS = MCS2;
% end

% count = size(elementsFlex,2)+1;  % Small mass at end of first link
% elementsFlex{count}.type = 'RigidBodyElement';
% elementsFlex{count}.nodes = [Start2-1];
% elementsFlex{count}.mass = 0.2; % 0.07
% % elementsFlex{count}.g = [0 0 -9.81];

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'RigidBodyElement';
elementsFlex{count}.nodes = [nodes(end,1)];
elementsFlex{count}.mass = m_end; % 0.07
% elementsFlex{count}.g = [0 0 -9.81];

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [Start2-1 Start2];
elementsFlex{count}.A = [0 0 0 0 1 0]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [1 2];
elementsFlex{count}.A = [0 0 0 0 0 1]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [2 3];
elementsFlex{count}.A = [0 0 0 0 1 0]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [Start2-2 Start2-1];
elementsFlex{count}.A = [0 0 0 0 1 0]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'RotSpringDamperElement';
elementsFlex{count}.stiffness = 10;
elementsFlex{count}.damping = 0.1;
elementsFlex{count}.nodes = [Start2-1 Start2];
elementsFlex{count}.A = [0 1 0];
count = count+1;

% Trajectory

npts = 151;

timeVector = 0:finaltime/(npts-1):finaltime;
trajx = nodes(end,2)*ones(size(timeVector));
trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

elementsFlex{count}.type = 'TrajectoryConstraint';
elementsFlex{count}.nodes = [nodes(end,1)];
elementsFlex{count}.T = [trajx;...
                         trajy;...
                         trajz];
elementsFlex{count}.Axe = [1 0 0;...
                           0 1 0;...
                           0 0 1];
elementsFlex{count}.elements = [count-4 count-3 count-2];
elementsFlex{count}.active = 1;

% Solving

ModelF = FEModel();
ModelF.CreateFEModel(nodes,elementsFlex);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from rigid solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = Model.listNodes{n}.R;
    ModelF.listNodes{n}.position = Model.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = Model.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = Model.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
%         ModelF.listElementVariables{n}.A = Model.listElementVariables{n}.A;
%         ModelF.listElementVariables{n}.nDof = Model.listElementVariables{n}.nDof;
        ModelF.listElementVariables{n}.R = Model.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = Model.listElementVariables{n}.x;
%         ModelF.listElementVariables{n}.xI0 = Model.listElementVariables{n}.xI0;
        ModelF.listElementVariables{n}.velocity = Model.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = Model.listElementVariables{n}.acceleration;
        ModelF.listElementVariables{n}.relCoo = Model.listElementVariables{n}.relCoo;
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(Model.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = Model.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

tic
S = DirectTranscriptionOpti(ModelF);
% S.parameters.relTolRes = 1e-12;
S.parameters.rho = 0.0;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
% S.NodeToMinimize = [Start2+[1:nElem2-1]];
% S.JointToMinimize = []; 
S.parameters.scaling = 1e6;
S.linConst = false;
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])

%% Plots

timeSteps = S.timeValues;
timeLoc = S.timesteps;
% timeSteps = D.parameters.time;
% timeLoc = 1:length(D.parameters.time);

uBeam.u1 = ModelF.listElementVariables{end}.value(1,timeLoc);
uBeam.u2 = ModelF.listElementVariables{end}.value(2,timeLoc);
uBeam.u3 = ModelF.listElementVariables{end}.value(3,timeLoc);
uBeam.time = S.timeValues;

save('uSerialFlexJointSerieConfig3D','uBeam')

endNode = nodes(end,1);
figure
hold on
plot3(ModelF.listNodes{endNode}.position(1,timeLoc),ModelF.listNodes{endNode}.position(2,timeLoc),ModelF.listNodes{endNode}.position(3,timeLoc), 'Linewidth',3)
plot3(trajx,trajy,trajz, 'Linewidth',1, 'Color','r')
grid on

figure
hold on
plot(timeSteps,ModelF.listNodes{endNode}.position(1,timeLoc),timeSteps,ModelF.listNodes{endNode}.position(2,timeLoc),timeSteps,ModelF.listNodes{endNode}.position(3,timeLoc))
plot(timeSteps,trajx,'--',timeSteps,trajy,'--',timeSteps,trajz,'--')
legend('X', 'Y', 'Z','Xd','Yd','Zd')
grid on

figure
hold on
plot(timeSteps,uBeam.u1,timeSteps,uBeam.u2,timeSteps,uBeam.u3,'Linewidth',2)
plot(uRigi.time,uRigi.u1,':',uRigi.time,uRigi.u2,':',uRigi.time,uRigi.u3,':','Linewidth',2)
% plot(timeSteps,u1_init(timeLoc),':',timeSteps,u2_init(timeLoc),':',timeSteps,u3_init(timeLoc),':','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Commands','Fontsize',16)
legend('u1','u2','u3','u1_{rigid}','u2_{rigid}','u3_{rigid}','Location', 'Best')
title('Commands of an experimental Robot','Fontsize',18)
grid on

joint1_init = Model.listElementVariables{1};
joint2_init = Model.listElementVariables{2};
joint3_init = Model.listElementVariables{3};

joint1 = ModelF.listElementVariables{1};
joint2 = ModelF.listElementVariables{2};
joint3 = ModelF.listElementVariables{3};

figure
hold on
plot(timeSteps,joint1.relCoo(timeLoc),timeSteps,joint2.relCoo(timeLoc),timeSteps,joint3.relCoo(timeLoc),'Linewidth',2)
plot(D.parameters.time,joint1_init.relCoo,'--',D.parameters.time,joint2_init.relCoo,'--',D.parameters.time,joint3_init.relCoo,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints angle (rad)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% title('Joints angle of a flexible Robot','Fontsize',18)
grid on


figure
hold on
plot(timeSteps,joint1.velocity(timeLoc),timeSteps,joint2.velocity(timeLoc),timeSteps,joint3.velocity(timeLoc),'Linewidth',2)
plot(D.parameters.time,joint1_init.velocity,'--',D.parameters.time,joint2_init.velocity,'--',D.parameters.time,joint3_init.velocity,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints velocity (rad/s)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
title('Joints velocity','Fontsize',18)
grid on

figure
hold on
plot(timeSteps,joint1.acceleration(timeLoc),timeSteps,joint2.acceleration(timeLoc),timeSteps,joint3.acceleration(timeLoc),'Linewidth',2)
plot(D.parameters.time,joint1_init.acceleration,'--',D.parameters.time,joint2_init.acceleration,'--',D.parameters.time,joint3_init.acceleration,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints accelerations (rad/s^2)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
title('Joints accelerations','Fontsize',18)
grid on



