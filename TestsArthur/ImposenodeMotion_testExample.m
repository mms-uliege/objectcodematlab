%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
close all

finaltime = 5;
h = 0.01;
initialtime = 0;
t_i = 0.2;
t_f = 2;
rho_num = 0.2;

nodes = [1 0 0 0;...
         2 0 0 1];

m1 = 0;
m2 = 1;
k = 81;
c = 1;
count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [1];
elements{count}.mass = m1;

count = 2;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [2];
elements{count}.mass = m2;

count = 3;
elements{count}.type = 'LinSpringDamperElement';
elements{count}.nodes = [1 2];
elements{count}.stiffness = k;
elements{count}.damping = c;
elements{count}.A = [0 0 1];

% trajectory
timeVector = initialtime:h:finaltime;
r = 0.3;
trajz = halfCircleTraj(nodes(1,4),nodes(1,4),r,timeVector,'cos',t_i,t_f);
count = 4;
elements{count}.type = 'ImposeNodeMotion';
elements{count}.nodes = [1];
elements{count}.T = trajz;
elements{count}.Axe = [0 0 1];

% Boundary Condition
BC = [];

%% Solving model
Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = h;
D.parameters.rho = rho_num;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

% plots
figure
hold on
plot(timeVector,trajz,'o')
plot(timeVector,Model.listNodes{1}.position(3,:),'Linewidth',2)
plot(timeVector,Model.listNodes{2}.position(3,:),'Linewidth',2)
xlabel('Time [s]')
ylabel('Z position [m]')
title('Z position of node 1 and 2')
legend('desired','node 1','node 2')
grid on

% FFT analysis
peaks = find(Model.listNodes{2}.position(3,:)>=0.99*max(Model.listNodes{2}.position(3,:)),2);
disp(['Resonance pulse is: ',num2str(sqrt(k/m2)), ' theoretically'])
