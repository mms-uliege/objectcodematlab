%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test to see if the analytical gradients are the same as those computed
% using the finite difference
clear all

finaltime = 1.5;
timestepsize = 0.1;
t_i = 0.2;
t_f = 1.3;

a1 = 0.05;
b1 = a1;
l1 = 1;
l2 = l1;
e1 = 0.01;
rapport = a1/e1;
a2 = 0.05;
b2 = a2;
e2 = a2/rapport;
a2In = a2-2*e2;
b2In = b2-2*e2;
a1In = a1-2*e1; % inner length
b1In = b1-2*e1;

nPass = 100;

% grav = [0 0 -9.81];
grav = [0 0 0];

m_end = 0.1; % end-effector mass

%% Creating nodes
angle = 45*pi/180;
nodes = [1 0 0 0;
         2 0 0 0;
         3 0 0 0;
         4 l1*cos(angle) 0 l1*sin(angle);
         5 l1*cos(angle) 0 l1*sin(angle);
         6 l1*cos(angle)+l2*cos(angle) 0 0];
     
nElem1 = 2;
Start1 = 3;
End1 = 4;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

nElem2 = 2;
Start2 = End1+nElem1;
End2 = Start2+1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);
 
%% Rigid Model

rho = 2700;
m1 = (a1*b1-a1In*b1In)*l1*rho;
% IxxR1 = m1*(a1^2+b1^2)/12;
IxxR1 = m1*(a1^2+b1^2-a1In^2-b1In^2)/12;
IyyR1 = m1*(a1^2+l1^2)/12;
IzzR1 = m1*(b1^2+l1^2)/12;

m2 = (a2*b2-a2In*b2In)*l2*rho;
IxxR2 = m2*(a2^2+b2^2-a2In^2-b2In^2)/12;
IyyR2 = m2*(a2^2+l2^2)/12;
IzzR2 = m2*(b2^2+l2^2)/12;

count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elements{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR1 IyyR1 IzzR1]);
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
elements{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR2 IyyR2 IzzR2]);
elements{count}.g = grav;

E = 70e9; nu = 0.3; G = E/(2*(1+nu));

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = m_end; % 0.07
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.A = [0 0 0 0 1 0]';
count = count+1;

% Trajectory

y_end = l2;
z_end = nodes(end,4);
r = y_end/2;

timeVector = 0:timestepsize:finaltime;
trajx = nodes(end,2)*ones(size(timeVector));
trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);


elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajy;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elements{count}.elements = [count-3 count-2 count-1];
elements{count}.active = 1;

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.01;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

%% Flexible Model

% A2 = a2*b2; Ixx2 = a2*b2*(a2^2+b2^2)/12; Iyy2 = b2*a2^3/12;Izz2 = a2*b2^3/12;
A2 = a2*b2-a2In*b2In; Ixx2 = (a2*b2*(a2^2+b2^2)-a2In*b2In*(a2In^2+b2In^2))/12; Iyy2 = (b2*a2^3-b2In*a2In^3)/12;Izz2 = (a2*b2^3-a2In*b2In^3)/12;

KCS2 = diag([E*A2 G*A2 G*A2 G*Ixx2 E*Iyy2 E*Izz2]);
MCS2 = diag(rho*[A2 A2 A2 Ixx2 Iyy2 Izz2]);

type = 'FlexibleBeamElement';
count = 1;
elementsFlex{count}.type = 'RigidBodyElement';   % first link (rigid)
elementsFlex{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elementsFlex{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
elementsFlex{count}.J = diag([IxxR1 IyyR1 IzzR1]);
elementsFlex{count}.g = grav;

count = size(elementsFlex,2); % Second link (flexible)
for i = 1:nElem2
    elementsFlex{count+i}.type = type;
    elementsFlex{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
    elementsFlex{count+i}.KCS = KCS2;
    elementsFlex{count+i}.MCS = MCS2;
    elementsFlex{count+i}.g = grav;
end
% count = size(elementsFlex,2)+1;
% elementsFlex{count}.type = 'RigidBodyElement';
% elementsFlex{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
% elementsFlex{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
% elementsFlex{count}.J = diag([IxxR2 IyyR2 IzzR2]);
% elementsFlex{count}.g = grav;

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'RigidBodyElement';
elementsFlex{count}.nodes = [nodes(end,1)];
elementsFlex{count}.mass = m_end; % 0.07
elementsFlex{count}.g = grav;

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [1 2];
elementsFlex{count}.A = [0 0 0 0 0 1]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [2 3];
elementsFlex{count}.A = [0 0 0 0 1 0]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [Start2-1 Start2];
elementsFlex{count}.A = [0 0 0 0 1 0]';
count = count+1;

% Trajectory

npts = finaltime/timestepsize + 1;

timeVector = 0:finaltime/(npts-1):finaltime;
trajx = nodes(end,2)*ones(size(timeVector));
trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

elementsFlex{count}.type = 'TrajectoryConstraint';
elementsFlex{count}.nodes = [nodes(end,1)];
elementsFlex{count}.T = [trajx;...
                         trajy;...
                         trajz];
elementsFlex{count}.Axe = [1 0 0;...
                           0 1 0;...
                           0 0 1];
elementsFlex{count}.elements = [count-3 count-2 count-1];
elementsFlex{count}.active = 1;

% Solving

ModelF = FEModel();
ModelF.CreateFEModel(nodes,elementsFlex);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from rigid solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = 0.01;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.parameters.scaling = 1e6;
S.linConst = false;
S.nPass = nPass;

S.parameters.InitialComputation();

for i = S.model.listNumberDof
    S.model.listDof{i}.InitializeD(D.parameters.nstep);
end
for i = S.model.listNumberElementVariables
    S.model.listElementVariables{i}.InitializeD(D.parameters.nstep);
end
for i = S.model.listNumberElements
    S.model.listElements{i}.InitializeD(D.parameters.nstep);
end

% Selection of the discrete time steps according to the number of npts
S.timeValues = zeros(1,S.npts);
S.timesteps = zeros(1,S.npts);
for k = 1:S.npts
    S.timeValues(k) = (k-1)*S.parameters.finaltime/(S.npts-1);
    S.timesteps(k) = find(D.parameters.time>=S.timeValues(k),1);
    if k~= 1 && abs(S.timeValues(k)-D.parameters.time(S.timesteps(k)-1))< 1e-6
        S.timesteps(k) = S.timesteps(k)-1;
    end
end

S.LocMotionDof = S.model.listFreeDof(1:S.nCoord);

% Saves the constant (over time and iteration) mass matrix (not like K,C
% that are can be constant over iter but not along time).
M = S.model.getTangentMatrix_Opti('Mass',S.timesteps(1),S.parameters.scaling);
S.M_Opti = M(S.LocMotionDof,S.LocMotionDof);

% Copying every trajectory information into a vector that has
% the same size in the first guess and in the optimization
% (inserting 0s between trajectory values)
for nE = 1:length(S.model.listElements)
    if isa(S.model.listElements{nE},'TrajectoryConstraint')
        T = zeros(size(S.model.listElements{nE}.T,1),length(D.parameters.time));
        T(:,S.timesteps) = S.model.listElements{nE}.T(:,1:length(S.timesteps));
%                     T(:,S.timesteps) = S.model.listElements{nE}.T;
        S.model.listElements{nE}.T = T;
    end
end

% Making a list of flexible elements that will be needed in the
% Sective function.
S.listFlexBeamElem = [];
for nE = 1:length(S.model.listElements)
    if isa(S.model.listElements{nE},'FlexibleBeamElement')
        S.listFlexBeamElem = [S.listFlexBeamElem nE];
    end
end            

x_0 = zeros(S.nVar*S.npts,1);
nfixDof = length(S.model.listFixedDof);
for k = 1:S.npts
    for var = S.model.listNodes
        if isempty(find(S.model.listFixedNodes==var{1}.numberNode))
            locV = S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
            locVdot = 2*S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
            x_0(locV) = var{1}.velocity_InitOpti(:,S.timesteps(k));
            x_0(locVdot) = var{1}.acceleration_InitOpti(:,S.timesteps(k));
        end
    end
    for var = S.model.listElementVariables
        if strcmp(var{1}.DofType,'MotionDof')
            locV = S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
            locVdot = 2*S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
            x_0(locV) = var{1}.velocity_InitOpti(:,S.timesteps(k));
            x_0(locVdot) = var{1}.acceleration_InitOpti(:,S.timesteps(k));
        else
            locValue = 3*S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
            x_0(locValue) = var{1}.value_InitOpti(:,S.timesteps(k));
        end
    end

end

sizeMotionConst = S.nCoord + S.nLagrangeM + S.nControls;
sizeIntregrationConst = 3*S.nCoord;

delta = 1e-6; % Perturbation size
x_i = x_0;
Diff_gradceq = zeros(S.nVar*S.npts, S.npts*(sizeMotionConst + sizeIntregrationConst) - sizeIntregrationConst);
[c, ceq, gradc, gradceq] = directTranscriptionOptiNLConstraints(x_i, S);
for i = 1:length(x_i)
    x_pert = zeros(size(x_i));
    x_pert(i) = delta;
%     [c_pertm, ceq_pertm] = NLConstraints(x_i-x_pert, S);
%     [c_pert, ceq_pert] = NLConstraints(x_i+x_pert, S);
%     line=(ceq_pertm-ceq_pert)'/(2*delta);
    [c_pert, ceq_pert] = NLConstraints(x_i+x_pert, S);
    line=(ceq-ceq_pert)'/delta;
    Diff_gradceq(i,:) = -line;
    if i==floor(length(x_i)/2)
        disp('halfway there...')
    end
    if i==floor(3*length(x_i)/4)
        disp('almost there...')
    end
    if i==length(x_i)
        disp('Done !')
    end
    spy(Diff_gradceq,10)
    drawnow
end
% figure
% spy(Diff_gradceq,10)
% figure
% spy(gradceq,10)

Diff = Diff_gradceq'-gradceq';
Tb = 3;
sE = (sizeMotionConst + sizeIntregrationConst);
% bloc0 = Diff_gradceq(1:2*S.nVar,1:sE);
% blocM = Diff_gradceq(Tb*S.nVar+1:(Tb+2)*S.nVar,Tb*(sE)+1:(Tb+1)*(sE));
bloc00 = gradceq(1:2*S.nVar,1:sE);
blocM0 = gradceq(Tb*S.nVar+1:(Tb+2)*S.nVar,Tb*(sE)+1:(Tb+1)*(sE));

bloc0 = Diff(1:2*S.nVar,1:sE);
blocM = Diff(Tb*S.nVar+1:(Tb+2)*S.nVar,Tb*(sE)+1:(Tb+1)*(sE));

figure
spy(bloc0',10)
figure
spy(bloc00',10)
figure
spy(blocM',10)
figure
spy(blocM0',10)

tol = 1e-8;
figure
hold on
spy(abs(Diff)>tol,10)

% for id =1:length(line)
%     if line(id)<=1e-6
%         line(id) = 0;
%     end
% end



