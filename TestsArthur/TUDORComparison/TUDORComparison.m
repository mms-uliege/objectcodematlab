%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compare different TudorModels
clear all
% close all

% perfect model
perf = load('C:\Users\Arthur\Documents\MATLAB\OoCode\TUDORFlexLinkRigidJoints\S');
perf.commands = perf.S.model.listElementVariables{end}.value;
perf.velocity(1,:) = perf.S.model.listElementVariables{1}.velocity;
perf.velocity(2,:) = perf.S.model.listElementVariables{2}.velocity;
perf.velocity(3,:) = perf.S.model.listElementVariables{3}.velocity;
% flexible joint model
flex = load('C:\Users\Arthur\Documents\MATLAB\OoCode\TUDORFlexLinkFlexJoints\S');
flex.commands = flex.S.model.listElementVariables{end}.value;
flex.velocity(1,:) = flex.S.model.listElementVariables{1}.velocity;
flex.velocity(2,:) = flex.S.model.listElementVariables{3}.velocity;
flex.velocity(3,:) = flex.S.model.listElementVariables{5}.velocity;
% viscous friction only model
visc = load('C:\Users\Arthur\Documents\MATLAB\OoCode\TUDORFlexLinkViscJoints\S');
visc.commands = visc.S.model.listElementVariables{end}.value;
visc.velocity(1,:) = visc.S.model.listElementVariables{1}.velocity;
visc.velocity(2,:) = visc.S.model.listElementVariables{2}.velocity; 
visc.velocity(3,:) = visc.S.model.listElementVariables{3}.velocity;
% viscous friction only model (in joints directly)
viscB = load('C:\Users\Arthur\Documents\MATLAB\OoCode\TUDORFlexLinkViscJoints\SBis');
viscB.commands = viscB.S.model.listElementVariables{end}.value;
viscB.velocity(1,:) = viscB.S.model.listElementVariables{1}.velocity;
viscB.velocity(2,:) = viscB.S.model.listElementVariables{2}.velocity;
viscB.velocity(3,:) = viscB.S.model.listElementVariables{3}.velocity;

% viscous friction only model (in joints directly and joint traj)
viscBJ = load('C:\Users\Arthur\Documents\MATLAB\OoCode\TUDORFlexLinkViscJoints\SBisJ');
viscBJ.commands = [viscBJ.S.model.listElementVariables{end-2}.value; viscBJ.S.model.listElementVariables{end-1}.value;viscBJ.S.model.listElementVariables{end}.value];
viscBJ.velocity(1,:) = viscBJ.S.model.listElementVariables{1}.velocity;
viscBJ.velocity(2,:) = viscBJ.S.model.listElementVariables{2}.velocity;
viscBJ.velocity(3,:) = viscBJ.S.model.listElementVariables{3}.velocity;

% plot comparison
figure
% title('Comparison of torque inputs')
hold on
plot(perf.S.timeValues,perf.commands(1,:),'bo',perf.S.timeValues,perf.commands(2,:),'ko',perf.S.timeValues,perf.commands(3,:),'ro')
plot(flex.S.timeValues,flex.commands(1,:),'b--',flex.S.timeValues,flex.commands(2,:),'k--',flex.S.timeValues,flex.commands(3,:),'r--')
% plot(visc.S.timeValues,visc.commands(1,:),'b.-',visc.S.timeValues,visc.commands(2,:),'k.-',visc.S.timeValues,visc.commands(3,:),'r.-')
plot(viscB.S.timeValues,viscB.commands(1,:),'b-',viscB.S.timeValues,viscB.commands(2,:),'k-',viscB.S.timeValues,viscB.commands(3,:),'r-')
plot(viscBJ.S.timeValues,viscBJ.commands(1,:),'b:',viscBJ.S.timeValues,viscBJ.commands(2,:),'k:',viscBJ.S.timeValues,viscBJ.commands(3,:),'r:')
legend('perf_{u1}','perf_{u2}','perf_{u3}','flex_{u1}','flex_{u2}','flex_{u3}','visc_{u1}','visc_{u2}','visc_{u3}','viscB_{u1}','viscB_{u2}','viscB_{u3}')
% legend('perf_{u1}','perf_{u2}','perf_{u3}','flex_{u1}','flex_{u2}','flex_{u3}','visc_{u1}','visc_{u2}','visc_{u3}', 'Location','SouthEastOutside')
xlabel('Time (s)')
ylabel('Torque (Nm)')
grid on

% figure
% title('Comparison of torque inputs')
% hold on
% plot(perf.S.timeValues,perf.commands(1,:),'ko',perf.S.timeValues,perf.commands(2,:),'ro',perf.S.timeValues,perf.commands(3,:),'bo')
% plot(viscB.S.timeValues,viscB.commands(1,:),'k*',viscB.S.timeValues,viscB.commands(2,:),'r*',viscB.S.timeValues,viscB.commands(3,:),'b*')
% legend('perf_{u1}','perf_{u2}','perf_{u3}','viscB_{u1}','viscB_{u2}','viscB_{u3}')
% xlabel('Time (s)')
% ylabel('Torque (Nm)')

figure
% title('Comparison of velocity inputs')
hold on
plot(perf.S.timeValues,perf.velocity(1,:),'bo',perf.S.timeValues,perf.velocity(2,:),'ko',perf.S.timeValues,perf.velocity(3,:),'ro')
plot(flex.S.timeValues,flex.velocity(1,:),'b--',flex.S.timeValues,flex.velocity(2,:),'k--',flex.S.timeValues,flex.velocity(3,:),'r--')
% plot(visc.S.timeValues,visc.velocity(1,:),'b.-',visc.S.timeValues,visc.velocity(2,:),'k.-',visc.S.timeValues,visc.velocity(3,:),'r.-')
plot(viscB.S.timeValues,viscB.velocity(1,:),'b-',viscB.S.timeValues,viscB.velocity(2,:),'k-',viscB.S.timeValues,viscB.velocity(3,:),'r-')
plot(viscBJ.S.timeValues,viscBJ.velocity(1,:),'b:',viscBJ.S.timeValues,viscBJ.velocity(2,:),'k:',viscBJ.S.timeValues,viscBJ.velocity(3,:),'r:')
legend('perf_{u1}','perf_{u2}','perf_{u3}','flex_{u1}','flex_{u2}','flex_{u3}','visc_{u1}','visc_{u2}','visc_{u3}','viscB_{u1}','viscB_{u2}','viscB_{u3}')
% legend('perf_{u1}','perf_{u2}','perf_{u3}','flex_{u1}','flex_{u2}','flex_{u3}','visc_{u1}','visc_{u2}','visc_{u3}', 'Location','SouthEastOutside')
xlabel('Time (s)')
ylabel('Joint Velocity (rad/s)')
grid on

% % Difference between perf and visc
% figure
% diff_u1 = perf.commands(1,:) - visc.commands(1,:);
% diff_u2 = perf.commands(2,:) - visc.commands(2,:);
% diff_u3 = perf.commands(3,:) - visc.commands(3,:);
% plot(1:201,diff_u1,1:201,diff_u2,1:201,diff_u3)
% 
% % Difference between visc and viscB
% figure
% diffB_u1 = visc.commands(1,:) - viscB.commands(1,:);
% diffB_u2 = visc.commands(2,:) - viscB.commands(2,:);
% diffB_u3 = visc.commands(3,:) - viscB.commands(3,:);
% plot(1:201,diffB_u1,1:201,diffB_u2,1:201,diffB_u3)
