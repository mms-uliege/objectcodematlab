%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nodes = [1 0;
         2 1.5];
    
e{1}.type = 'LumpedMass1DElement';
e{1}.mass = 5;
e{1}.nodes = 2;

e{2}.type = 'Spring1DElement';
e{2}.stiffness = 100;
e{2}.natural_length = 1;
e{2}.nodes = [1 2];

e{3}.type = 'Damper1DElement';
e{3}.damping = 5;
e{3}.nodes = [1 2];

% e{4}.type = 'ExternalForce';
% e{4}.nodes = 2;
% e{4}.amplitude = 100;
% e{4}.frequency = 5;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,e);
Model.defineBC(BC);

S = DynamicIntegration(Model);
S.parameters.finaltime = 5;
S.parameters.timestepsize = 0.05;
S.runIntegration();

plot(S.model.listNodes{2}.position)