%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test of rigid lambda kinematic manipulator in direct dynamic
clear

finaltime = 2;
timestepsize = 0.01;

%% Creation of the model

% Nodes
nodes = [1 0 0.5 0;
         2 0 0.5 0;
         3 0 0.5 0;
         4 sqrt(3)/2 0 0;
         5 0 -0.5 0;
         6 0 -0.5 0;
         7 sqrt(3)/2 0 0;
         8 3*sqrt(3)/4 0.25 0;
         9 3*sqrt(3)/4 0.25 0;
         10 sqrt(3) 0.5 0];
     
nElem = 2;
Start = 3;
End = 4;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 7;
End = 8;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 9;
End = 10;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 12;
End = 13;
nodes = createInterNodes(nodes,nElem,Start,End);
     
% Elements
elements = [];
elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2];
elements{1}.mass = 0.5;

elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [6];
elements{2}.mass = 0.5;

J = 0.57*eye(3);
mass = 0.5;

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [4 3 5];
elements{3}.mass = mass;
elements{3}.J = J;

elements{4}.type = 'RigidBodyElement';
elements{4}.nodes = [8 7 9];
elements{4}.mass = mass;
elements{4}.J = J;

elements{5}.type = 'RigidBodyElement';
elements{5}.nodes = [10 9 11];
elements{5}.mass = mass/2;
elements{5}.J = J;

elements{6}.type = 'RigidBodyElement';
elements{6}.nodes = [13 12 14];
elements{6}.mass = mass/2;
elements{6}.J = J;

elements{7}.type = 'KinematicConstraint';
elements{7}.nodes = [1 2];
elements{7}.A = [1 0 0 0 0 0]';

elements{8}.type = 'KinematicConstraint';
elements{8}.nodes = [2 3];
elements{8}.A = [0 0 0 0 0 1]';

elements{9}.type = 'KinematicConstraint';
elements{9}.nodes = [1 6];
elements{9}.A = [1 0 0 0 0 0]';

elements{10}.type = 'KinematicConstraint';
elements{10}.nodes = [6 7];
elements{10}.A = [0 0 0 0 0 1]';

elements{11}.type = 'KinematicConstraint';
elements{11}.nodes = [5 9];
elements{11}.A = [0 0 0 0 0 1]';

elements{12}.type = 'KinematicConstraint';
elements{12}.nodes = [11 12];
elements{12}.A = [0 0 0 0 0 1]';

elements{13}.type = 'RotSpringDamperElement';
elements{13}.damping = 0.1; %
elements{13}.stiffness = 2; %
elements{13}.nodes = [11 12];
elements{13}.A = [0 0 1];

% Trajectory

TrajParam.points = [sqrt(3) 0.5 0;...
                    3 0 0];
                    
TrajParam.timeVector = 0:timestepsize:finaltime;
TrajParam.intervals = [0.3 0.7];

[trajx trajy trajz] = PointTraj(TrajParam);

t = TrajParam.timeVector;

load('uLambdaRigid')
u1_init = interp1(uLambdaRigid.time,uLambdaRigid.u1_init,t,'linear');
u2_init = interp1(uLambdaRigid.time,uLambdaRigid.u2_init,t,'linear');
% u1 = u1_init;
% u2 = u2_init;

load('uLambdaFlex')
u1 = interp1(uLambdaFlex.time,uLambdaFlex.u1,t,'linear');
u2 = interp1(uLambdaFlex.time,uLambdaFlex.u2,t,'linear');
% u1 = smooth(u1);
% u2 = smooth(u2);

elements{14}.type = 'ForceInKinematicConstraint';
elements{14}.elements = [7];
elements{14}.f = u1;

elements{15}.type = 'ForceInKinematicConstraint';
elements{15}.elements = [9];
elements{15}.f = u2;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

%% Dynamic integration for initial guess

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.0;
% D.parameters.relTolRes = 1e-12;
% D.parameters.scaling = 1e6;
D.runIntegration();


%% Post process

EndNode = 14;

timeSteps = D.parameters.time;

xEff = Model.listNodes{EndNode}.position(1,:);
yEff = Model.listNodes{EndNode}.position(2,:);

figure
title('Commands of flexible lambda manipulator')
hold on
plot(timeSteps,u1,timeSteps,u2,'Linewidth',3)
plot(D.parameters.time,u1_init,':',D.parameters.time,u2_init,':','Linewidth',3)
legend('u1','u2','u1_{rigid}','u2_{rigid}','Location','Best')
xlabel('time')
ylabel('command')
grid on

figure
title('X and Y of lambda manipulator effector')
hold on
plot(timeSteps,xEff,timeSteps,yEff)
plot(timeSteps,trajx,':',timeSteps,trajy,':')
legend('x','y','x_d','y_d')
xlabel('time')
ylabel('position')
grid on

figure
title('Trajectory of lambda manipulator effector')
hold on
plot(xEff,yEff,'Linewidth',3)
plot(trajx,trajy, 'Linewidth',1, 'Color','r')
xlabel('x')
ylabel('y')
grid on


