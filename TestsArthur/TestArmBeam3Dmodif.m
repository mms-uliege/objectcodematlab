%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear

finaltime = 1.5;
timestepsize = 0.01;
t_i = 0.2;
t_f = 1.2;

nodes = [1 0 0 -1;
         2 0 0 -1;
         3 0 0 0;
         4 0 0 0;
         5 0 sqrt(2)/2 sqrt(2)/2;
         6 0 sqrt(2)/2 sqrt(2)/2;
         7 0 sqrt(2) 0];
     
nElem = 2;
Start = 2;
End = 3;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 5;
End = 6;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 6;
Start = 8;
End = 9;
nodes = createInterNodes(nodes,nElem,Start,End);

d = 0.05;
m1 = 2;
l1 = 1;
I1 = (m1/4)*(d^2/4 + l1^2/3);
J1 = 0.5*m1*(d/2)^2;

m2 = m1;
l2 = l1;
I2 = (m2/4)*(d^2/4 + l2^2/3);
J2 = 0.5*m2*(d/2)^2;

m3 = 0.5;
l3 = 0.25;
I3 = (m3/4)*(d^2/4 + l3^2/3);
J3 = 0.5*m3*(d/2)^2;

elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [3 2 4];
elements{1}.mass = 2;
elements{1}.J = diag([J1 I1 I1]);
% elements{1}.g = [0 0 -9.81];
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [6 5 7];
elements{2}.mass = m2;
elements{2}.J = diag([J2 I2 I2]);
% elements{2}.g = [0 0 -9.81];

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [9 8 10];
elements{3}.mass = m3;
elements{3}.J = diag([J3 I3 I3]);
% elements{3}.g = [0 0 -9.81];

circular = true;
if ~circular
% square
    a = 0.005;
    E = 210000e9; nu = 0.3; G = E/(2*(1+nu)); rho = 7900;
    A = a^2; I = a^4/12; J = 2*I;
else
% circular
    d = 0.005;
    E = 70000e9; nu = 0.3; G = E/(2*(1+nu)); rho = 2700;
    A = pi*d^2/4; I = pi*d^4/64; J = 2*I;
end

KCS = diag([E*A G*A G*A G*J E*I E*I]);
MCS = diag(rho*[A A A J I I]);

type = 'FlexibleBeamElement';
Start = 10;
End = nodes(end,1);
nElem = End-Start;
count = size(elements,2);
for i = 1:nElem
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start+i-1,1) nodes(Start+i,1)];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
end

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 0 1]';
count = count+1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [4 5];
elements{count}.A = [0 0 0 1 0 0]';
count = count+1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [7 8];
elements{count}.A = [0 0 0 1 0 0]';
count = count+1;
% Trajectory

x_end = 1;
z_end = nodes(end,4);
r = 0.5;

timeVector = 0:timestepsize:finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'sin',t_i,t_f);
trajy = nodes(end,3)*ones(size(timeVector));
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);


elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                 trajy;...
                 trajz];
elements{count}.Axe = [1 0 0;...
                   0 1 0;...
                   0 0 1];
elements{count}.elements = [count-3 count-2 count-1];
elements{count}.active = 1;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

compute = 1;
if compute
    D = DynamicIntegration(Model);
    D.parameters.finaltime = finaltime;
    D.parameters.timestepsize = timestepsize;
    D.parameters.rho = 0.0;
    % D.parameters.relTolRes = 1e-10;
    D.parameters.scaling = 1e6;
    D.runIntegration();
else
    load('ArmBeamD')
    Model = D.model;
end

uRigiBeam.u1 = Model.listElementVariables{end}.value(1,:);
uRigiBeam.u2 = Model.listElementVariables{end}.value(2,:);
uRigiBeam.u3 = Model.listElementVariables{end}.value(3,:);
uRigiBeam.time = D.parameters.time;

save('uArmRigiBeam','uRigiBeam')

count = length(Model.listElements);
Model.listElements{count}.active = 1;

for n = Model.listNumberNodes
    Model.listNodes{n}.InitializeD_Opti();
end
for n = Model.listNumberElementVariables
    Model.listElementVariables{n}.InitializeD_Opti();
end

if ~circular
% square
    E = 210e9; G = E/(2*(1+nu));
else
% circular
    E = 20e9; nu = 0.3; G = E/(2*(1+nu));
end
KCS = diag([E*A G*A G*A G*J E*I E*I]);
for elem = Model.listElements
   if isa(elem{1},'FlexibleBeamElement')
       elem{1}.KCS = KCS;
   end
end

npts = 76;
% TrajParam.timeVector = 0:finaltime/(npts-1):finaltime;
% % [trajx trajy trajz] = PointTraj(TrajParam);
% [trajx trajy trajz] = PointTrajCurve(TrajParam);

timeVector = 0:finaltime/(npts-1):finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'sin',t_i,t_f);
trajy = nodes(end,3)*ones(size(timeVector));
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

Model.listElements{count}.T = [trajx;...
                            trajy;...
                            trajz];
tic
S = DirectTranscriptionOpti(Model);
% S.parameters.relTolRes = 1e-12;
S.parameters.rho = 0.0;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.NodeToMinimize = [11 12 13];
% S.JointToMinimize = [8];
S.parameters.scaling = 1e0;
S.linConst = false;
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])

% Plots

timeSteps = S.timeValues;
timeLoc = S.timesteps;

uBeam.u1 = Model.listElementVariables{end}.value(1,timeLoc);
uBeam.u2 = Model.listElementVariables{end}.value(2,timeLoc);
uBeam.u3 = Model.listElementVariables{end}.value(3,timeLoc);
uBeam.time = S.timeValues;

save('uArmBeam','uBeam')

endNode = nodes(end,1);
figure
hold on
plot3(Model.listNodes{endNode}.position(1,timeLoc),Model.listNodes{endNode}.position(2,timeLoc),Model.listNodes{endNode}.position(3,timeLoc), 'Linewidth',3)
plot3(trajx,trajy,trajz, 'Linewidth',1, 'Color','r')
grid on

figure
hold on
plot(timeSteps,Model.listNodes{endNode}.position(1,timeLoc),timeSteps,Model.listNodes{endNode}.position(2,timeLoc),timeSteps,Model.listNodes{endNode}.position(3,timeLoc))
plot(timeSteps,trajx,'--',timeSteps,trajy,'--',timeSteps,trajz,'--')
legend('X', 'Y', 'Z','Xd','Yd','Zd')
grid on

figure
hold on
plot(timeSteps,Model.listElementVariables{end}.value(1,timeLoc),timeSteps,Model.listElementVariables{end}.value(2,timeLoc),timeSteps,Model.listElementVariables{end}.value(3,timeLoc),'Linewidth',2)
plot(D.parameters.time,Model.listElementVariables{end}.value_InitOpti(1,:),'--',D.parameters.time,Model.listElementVariables{end}.value_InitOpti(2,:),'--',D.parameters.time,Model.listElementVariables{end}.value_InitOpti(3,:),'--','Linewidth',2)
% plot(timeSteps,u1_init(timeLoc),':',timeSteps,u2_init(timeLoc),':',timeSteps,u3_init(timeLoc),':','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Commands (Nm)','Fontsize',16)
legend('u1','u2','u3','u1_{rigid}','u2_{rigid}','u3_{rigid}','Location', 'Best')
% title('Commands of a flexible Robot','Fontsize',18)
grid on

joint1_init = Model.listElementVariables{1}.relCoo_InitOpti;
joint2_init = Model.listElementVariables{2}.relCoo_InitOpti;
joint3_init = Model.listElementVariables{3}.relCoo_InitOpti;

joint1 = Model.listElementVariables{1}.relCoo(timeLoc);
joint2 = Model.listElementVariables{2}.relCoo(timeLoc);
joint3 = Model.listElementVariables{3}.relCoo(timeLoc);

figure
hold on
plot(timeSteps,joint1,timeSteps,joint2,timeSteps,joint3,'Linewidth',2)
plot(D.parameters.time,joint1_init,':',D.parameters.time,joint2_init,':',D.parameters.time,joint3_init,':','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints angle (rad)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid')
title('Joints angle of a flexible Robot','Fontsize',18)
grid on

% save('ArmBeamD','D')
% save('ArmBeamS','S')
