%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pinned pinned beam frequency analysis testing

clear all

finaltime = 1;
h = 0.01;
rho_num = 0.9;
scaling = 1e6;
tol = 1e-6;
time = 0:h:finaltime;

nodes = [1 0 0 0;   
         2 0 0 0;
         3 1 0 0;
         4 1 0 0];
     
nElem = 4;
Start = 2;
End = 3;
nodes = createInterNodes(nodes,nElem,Start,End);


% nodes = [1 0 0 0;
%          2 1 0 0];
%      
% nElem = 4;
% Start = 1;
% End = 2;
% nodes = createInterNodes(nodes,nElem,Start,End);
    
a1 = 0.01;
b1 = a1;
l1 = 1;
e1 = 0.002;
a1In = a1-2*e1; % inner length
b1In = b1-2*e1;
grav = [0 0 -20.81];

rho = 2700;
E = 70e9;
nu = 0.3;
G = E/(2*(1+nu));
A1 = a1*b1-a1In*b1In;
Ixx1 = (a1*b1*(a1^2+b1^2)-a1In*b1In*(a1In^2+b1In^2))/12;
Iyy1 = (b1*a1^3-b1In*a1In^3)/12;
Izz1 = (a1*b1^3-a1In*b1In^3)/12;
KCS1 = diag([E*A1 G*A1 G*A1 G*Ixx1 E*Iyy1 E*Izz1]);
MCS1 = diag(rho*[A1 A1 A1 Ixx1 Iyy1 Izz1]);
alpha = 0*0.0001;
beta = 0*0.01;

count = 0;
type = 'FlexibleBeamElement';
for i = 1:nElem
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start+i-1,1) nodes(Start+i,1)];
    elements{count+i}.KCS = KCS1;
    elements{count+i}.MCS = MCS1;
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;
    elements{count+i}.g = grav;
end

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 1 0]';
% elements{count}.A = [0 0 0 0 1 0;
%                      0 1 0 0 0 0;
%                      0 0 0 1 0 0;
%                      0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [nodes(end-1,1) nodes(end,1)];
% elements{count}.A = [0 0 0 0 1 0]';
elements{count}.A = [0 0 0 0 1 0;
                     1 0 0 0 0 0;
                     0 1 0 0 0 0;
                     0 0 0 1 0 0;
                     0 0 0 0 0 1]';

count = size(elements,2)+1;
s = round(finaltime*0.05/h);
e = round(s+20);
A = 5;
f = zeros(size(time));
f(s:e) = A*ones(1,e-s+1);
elements{count}.type = 'punctualForce';
elements{count}.nodes = [nodes(4,1) nodes(3,1)];
elements{count}.f = f;
elements{count}.DOF = [3];

% Boundary Condition
BC = [1 nodes(end,1)];

% Solving
Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = h;
D.parameters.rho = rho_num;
D.parameters.relTolRes = tol;
D.parameters.scaling = scaling;
D.runIntegration();

Model.ModalAnalysis(10,1);
Model.eigenFrequencies

figure
plot(time,Model.listNodes{3}.position)
legend('x','y','z')
grid on
xlabel('Time [s]')
ylabel('Position [m]')

figure
plot(time,Model.listNodes{end}.position)
legend('x','y','z')
grid on
xlabel('Time [s]')
ylabel('Position [m]')

