%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test parallel manipulator
clear all
close all

finaltime = 1.5;
timestepsize = 0.01;
t_i = 0.2;
t_f = 1.2;

%% Creating nodes
sideTriUp = 1; % side length of the upper triangle
sideTriLow = 0.2; % side length of lower triangle
nodes = [1 sideTriUp/(2*cos(pi/6)) 0 0; % upper triangle
         2 -sideTriUp*tan(pi/6)/2 -sideTriUp/2 0;
         3 -sideTriUp*tan(pi/6)/2 sideTriUp/2 0;
         4 sideTriUp/(2*cos(pi/6)) 0 0; % first arm
         5 sideTriLow/(2*cos(pi/6)) 0 -1;
         6 -sideTriUp*tan(pi/6)/2 -sideTriUp/2 0; % second arm
         7 -sideTriLow*tan(pi/6)/2 -sideTriLow/2 -1;
         8 -sideTriUp*tan(pi/6)/2 sideTriUp/2 0; %third arm
         9 -sideTriLow*tan(pi/6)/2 sideTriLow/2 -1;
         10 sideTriLow/(2*cos(pi/6)) 0 -1; % lower triangle with center
         11 -sideTriLow*tan(pi/6)/2 -sideTriLow/2 -1;
         12 -sideTriLow*tan(pi/6)/2 sideTriLow/2 -1;
         13 0 0 -1];

nElem1 = 2; % even number !
nDiv1 = nElem1 + 1;
Start1 = 4;
End1 = Start1 + 1;
nodes = createInterNodes(nodes,nDiv1,Start1,End1);
nodes = doubleNode(nodes,Start1+1);

nElem2 = nElem1; % even number !
nDiv2 = nElem2 + 1;
Start2 = End1+nDiv1+1;
End2 = Start2 + 1;
nodes = createInterNodes(nodes,nDiv2,Start2,End2);
nodes = doubleNode(nodes,Start2+1);

nElem3 = nElem1; % even number !
nDiv3 = nElem3 + 1;
Start3 = End2+nDiv2+1;
End3 = Start3 + 1;
nodes = createInterNodes(nodes,nDiv3,Start3,End3);
nodes = doubleNode(nodes,Start3+1);

prism1 = (nodes(Start1,2:4)-nodes(Start1+2,2:4))/norm(nodes(Start1,2:4)-nodes(Start1+2,2:4));
prism2 = (nodes(Start2,2:4)-nodes(Start2+2,2:4))/norm(nodes(Start2,2:4)-nodes(Start2+2,2:4));
prism3 = (nodes(Start3,2:4)-nodes(Start3+2,2:4))/norm(nodes(Start3,2:4)-nodes(Start3+2,2:4));
 
%% Flexible Model
m_end = 0.5; % end efector load [kg]
b = sideTriLow/2;
h = sideTriLow*sin(pi/3);
IxxR = 0.1;
IyyR = 0.1;
IzzR = 0.05;

rho = 2700;

a1R = 0.02; % first arm param
b1R = 0.02;
l1R = norm(nodes(Start1,2:4)-nodes(Start1+1,2:4));
a1 = 0.005;
b1 = 0.005;
l1 = norm(nodes(Start1+2,2:4)-nodes(Start1+2+nElem1,2:4));
m1 = a1R*b1R*l1R*rho;
IxxR1 = m1*(a1R^2+b1R^2)/12;
IyyR1 = m1*(a1R^2+l1R^2)/12;
IzzR1 = m1*(b1R^2+l1R^2)/12;

a2R = a1R; % second arm param
b2R = b1R;
l2R = norm(nodes(Start2,2:4)-nodes(Start2+1,2:4));
a2 = a1;
b2 = b1;
l2 = norm(nodes(Start2+2,2:4)-nodes(Start2+2+nElem2,2:4));
m2 = a2R*b2R*l2R*rho;
IxxR2 = m2*(a2R^2+b2R^2)/12;
IyyR2 = m2*(a2R^2+l2R^2)/12;
IzzR2 = m2*(b2R^2+l2R^2)/12;

a3R = a1R; % third arm param
b3R = b1R;
l3R = norm(nodes(Start3,2:4)-nodes(Start3+1,2:4));
a3 = a1;
b3 = b1;
l3 = norm(nodes(Start3+2,2:4)-nodes(Start3+2+nElem3,2:4));
m3 = a3R*b3R*l3R*rho;
IxxR3 = m3*(a3R^2+b3R^2)/12;
IyyR3 = m3*(a3R^2+l3R^2)/12;
IzzR3 = m3*(b3R^2+l3R^2)/12;


E = 90e9; nu = 0.3; G = E/(2*(1+nu));
A1 = a1*b1; Ixx1 = a1*b1*(a1^2+b1^2)/12; Iyy1 = b1*a1^3/12;Izz1 = a1*b1^3/12;

KCS1 = diag([E*A1 G*A1 G*A1 G*Ixx1 E*Iyy1 E*Izz1]);
MCS1 = diag(rho*[A1 A1 A1 Ixx1 Iyy1 Izz1]);

A2 = a2*b2; Ixx2 = a2*b2*(a2^2+b2^2)/12; Iyy2 = b2*a2^3/12;Izz2 = a2*b2^3/12;

KCS2 = diag([E*A2 G*A2 G*A2 G*Ixx2 E*Iyy2 E*Izz2]);
MCS2 = diag(rho*[A2 A2 A2 Ixx2 Iyy2 Izz2]);

A3 = a3*b3; Ixx3 = a3*b3*(a3^3+b3^2)/12; Iyy3 = b3*a3^3/12;Izz3 = a3*b3^3/12;

KCS3 = diag([E*A3 G*A3 G*A3 G*Ixx3 E*Iyy3 E*Izz3]);
MCS3 = diag(rho*[A3 A3 A3 Ixx3 Iyy3 Izz3]);

type = 'FlexibleBeamElement';

count = 1;
elements{count}.type = 'RigidBodyElement'; % end effector
elements{count}.nodes = [nodes(end,1) nodes(end-3:end-1,1)'];
elements{count}.mass = m_end;
elements{count}.J = diag([IxxR IyyR IzzR]);
% elements{count}.g = [0 0 -9.81];

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement'; % first Rigid
elements{count}.nodes = [Start1+1 Start1];
elements{count}.mass = m1;
elements{count}.J = diag([IxxR1 IyyR1 IzzR1]);
% elements{count}.g = [0 0 -9.81];

ccount = size(elements,2); % first flexible arm
for i = 1:nElem1
    elements{count+i}.type = type;
    elements{count+i}.nodes = [Start1+i+1 Start1+i+2];
    elements{count+i}.KCS = KCS1;
    elements{count+i}.MCS = MCS1;
end

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement'; % second Rigid
elements{count}.nodes = [Start2+1 Start2];
elements{count}.mass = m2;
elements{count}.J = diag([IxxR2 IyyR2 IzzR2]);
% elements{count}.g = [0 0 -9.81];

count = size(elements,2); % second flexible arm
for i = 1:nElem2
    elements{count+i}.type = type;
    elements{count+i}.nodes = [Start2+i+1 Start2+i+2];
    elements{count+i}.KCS = KCS2;
    elements{count+i}.MCS = MCS2;
end

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement'; % third Rigid
elements{count}.nodes = [Start3+1 Start3];
elements{count}.mass = m3;
elements{count}.J = diag([IxxR3 IyyR3 IzzR3]);
% elements{count}.g = [0 0 -9.81];

count = size(elements,2); % third flexible arm
for i = 1:nElem3
    elements{count+i}.type = type;
    elements{count+i}.nodes = [Start3+i+1 Start3+i+2];
    elements{count+i}.KCS = KCS3;
    elements{count+i}.MCS = MCS3;
end

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % first upper spherical joint
elements{count}.nodes = [1 Start1];
elements{count}.A = [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % second upper spherical joint
elements{count}.nodes = [2 Start2];
elements{count}.A = [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % third upper spherical joint
elements{count}.nodes = [3 Start3];
elements{count}.A = [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % first lower spherical joint
elements{count}.nodes = [Start1+2+nElem1 nodes(end-3,1)];
elements{count}.A = [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';  % second lower spherical joint
elements{count}.nodes = [Start2+2+nElem2 nodes(end-2,1)];
elements{count}.A = [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % third lower spherical joint
elements{count}.nodes = [Start3+2+nElem3 nodes(end-1,1)];
elements{count}.A = [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start1+1 Start1+2];
elements{count}.A = [prism1 0 0 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2+1 Start2+2];
elements{count}.A = [prism2 0 0 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start3+1 Start3+2];
elements{count}.A = [prism3 0 0 0]';

% Trajectory

x_end = nodes(end,2);
y_end = sideTriLow;
r = y_end/2;

timeVector = 0:timestepsize:finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'cos',t_i,t_f);
trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
trajz = nodes(end,4)*ones(size(timeVector));

% Boundary Condition
BC = [1 2 3];

% Solving
Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.0;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

%% Plots

% timeSteps = S.timeValues;
% timeLoc = S.timesteps;
timeSteps = D.parameters.time;
timeLoc = 1:length(D.parameters.time);

u1 = ModelF.listElementVariables{end}.value(1,timeLoc);
u2 = ModelF.listElementVariables{end}.value(2,timeLoc);
u3 = ModelF.listElementVariables{end}.value(3,timeLoc);

endNode = nodes(end,1);
figure
hold on
plot3(ModelF.listNodes{endNode}.position(1,timeLoc),ModelF.listNodes{endNode}.position(2,timeLoc),ModelF.listNodes{endNode}.position(3,timeLoc), 'Linewidth',3)
plot3(trajx,trajy,trajz, 'Linewidth',1, 'Color','r')
grid on

figure
hold on
plot(timeSteps,Model.listNodes{endNode}.position(1,timeLoc),timeSteps,Model.listNodes{endNode}.position(2,timeLoc),timeSteps,Model.listNodes{endNode}.position(3,timeLoc))
plot(timeSteps,trajx,'--',timeSteps,trajy,'--',timeSteps,trajz,'--')
legend('X', 'Y', 'Z','Xd','Yd','Zd')
grid on

figure
hold on
plot(timeSteps,u1,timeSteps,u2,timeSteps,u3,'Linewidth',2)
% plot(uRigi.time,uRigi.u1,':',uRigi.time,uRigi.u2,':',uRigi.time,uRigi.u3,':','Linewidth',2)
plot(timeRigi,u1_init,':',timeRigi,u2_init,':',timeRigi,u3_init,':','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Commands (N)','Fontsize',16)
legend('u1','u2','u3','u1_{rigid}','u2_{rigid}','u3_{rigid}','Location', 'Best')
% legend('u1','u2','u3','Location', 'Best')
title('Commands of a parallel Robot','Fontsize',18)
grid on

joint1_init = Model.listElementVariables{7};
joint2_init = Model.listElementVariables{8};
joint3_init = Model.listElementVariables{9};

joint1 = ModelF.listElementVariables{7};
joint2 = ModelF.listElementVariables{8};
joint3 = ModelF.listElementVariables{9};

figure
hold on
plot(timeSteps,joint1.relCoo(timeLoc),timeSteps,joint2.relCoo(timeLoc),timeSteps,joint3.relCoo(timeLoc),'Linewidth',2)
plot(timeRigi,joint1_init.relCoo,'--',timeRigi,joint2_init.relCoo,'--',timeRigi,joint3_init.relCoo,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints position (m)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% legend('Joint 1','Joint 2','Joint 3','Location', 'Best')
grid on


