%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear all
% close all

finaltime = 1.5;
timestepsize = 0.01;
t_i = 0.2;
t_f = 1.3;

a1 = 0.05;
b1 = a1;
l1 = 1;
l2 = l1;
e1 = 0.01;
rapport = a1/e1;
a2 = 0.0075;
b2 = a2;
e2 = a2/rapport;
a2In = a2-2*e2;
b2In = b2-2*e2;
a1In = a1-2*e1; % inner length
b1In = b1-2*e1;

m_end = 0.1; % end-effector mass

% perturbation of the system (as a grav force on the end-effector)
f = 1; % frequency of the sine perturbation
ampl = 0.05; % amplitude of the sine perturbation
% grav = [0 0 -9.81];
grav = [0 0 0];
pert = [0 0 0];
t = 0:timestepsize:finaltime;
% pert = [zeros(1,length(t)); ampl*sin(2*pi*f.*t); zeros(1,length(t))]';
% pert = [zeros(1,length(t)); zeros(1,length(t));
% awgn(ampl*sin(2*pi*f.*t),25)]'; % with white gaussian noise
% grav = [zeros(1,length(t)); zeros(1,length(t)); [0 0 0 -9.81*ones(1,length(t)-3)]]';

%% Creating nodes
angle = 45*pi/180;
nodes = [1 0 0 0;
         2 0 0 0;
         3 0 0 0;
         4 l1*cos(angle) 0 l1*sin(angle);
         5 l1*cos(angle) 0 l1*sin(angle);
         6 l1*cos(angle)+l2*cos(angle) 0 0];
     
nElem1 = 2;
Start1 = 3;
End1 = 4;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

nElem2 = 4;
Start2 = End1+nElem1;
End2 = Start2+1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);

% nodes = doubleNode(nodes,size(nodes,1));
 
%% Flexible Model

rho = 2700;
m1 = (a1*b1-a1In*b1In)*l1*rho;
IxxR1 = m1*(a1^2+b1^2)/12;
IyyR1 = m1*(a1^2+l1^2)/12;
IzzR1 = m1*(b1^2+l1^2)/12;

m2 = (a2*b2-a2In*b2In)*l2*rho;
IxxR2 = m2*(a2^2+b2^2)/12;
IyyR2 = m2*(a2^2+l2^2)/12;
IzzR2 = m2*(b2^2+l2^2)/12;

count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elements{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR1 IyyR1 IzzR1]);
elements{count}.g = grav;

E = 70e9; nu = 0.3; G = E/(2*(1+nu));
% A2 = a2*b2; Ixx2 = a2*b2*(a2^2+b2^2)/12; Iyy2 = b2*a2^3/12;Izz2 = a2*b2^3/12;
A2 = a2*b2-a2In*b2In; Ixx2 = (a2*b2*(a2^2+b2^2)-a2In*b2In*(a2In^2+b2In^2))/12; Iyy2 = (b2*a2^3-b2In*a2In^3)/12;Izz2 = (a2*b2^3-a2In*b2In^3)/12;

KCS2 = diag([E*A2 G*A2 G*A2 G*Ixx2 E*Iyy2 E*Izz2]);
MCS2 = diag(rho*[A2 A2 A2 Ixx2 Iyy2 Izz2]);

type = 'FlexibleBeamElement';
count = size(elements,2);
for i = 1:nElem2
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
    elements{count+i}.KCS = KCS2;
    elements{count+i}.MCS = MCS2;
    elements{count+i}.g = grav;
end

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = m_end; % 0.03*0.019*0.04*2700
elements{count}.g = grav+pert;

% count = size(elements,2)+1;
% elements{count}.type = 'KinematicConstraint';
% elements{count}.nodes = [nodes(end,1) nodes(end,1)-1];

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.A = [0 0 0 0 1 0]';
count = count+1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.A = [0 0 0 0 1 0]';
count = count+1;

% Trajectory

y_end = l2;
z_end = nodes(end,4);
r = y_end/2;

timeVector = 0:timestepsize:finaltime;
trajx = nodes(end,2)*ones(size(timeVector));
trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

load('uRigiExperiment3D')
u1_init = interp1(uRigi.time,uRigi.u1,timeVector,'linear');
u2_init = interp1(uRigi.time,uRigi.u2,timeVector,'linear');
u3_init = interp1(uRigi.time,uRigi.u3,timeVector,'linear');

load('uExperiment3D')
u1 = interp1(uBeam.time,uBeam.u1,timeVector,'linear');
u2 = interp1(uBeam.time,uBeam.u2,timeVector,'linear');
u3 = interp1(uBeam.time,uBeam.u3,timeVector,'linear');

% u1(1:5) = u1(12)*ones(1,5);
% u2(1:5) = u2(12)*ones(1,5);
% u3(1:5) = u3(12)*ones(1,5);

elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = [count-3];
elements{count}.f = u1;
count = count+1;

elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = [count-3];
elements{count}.f = u2;
count = count+1;

elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = [count-3];
elements{count}.f = u3;

% Boundary Condition
BC = [1];

% Model Def
Model = FEModel();% Flex model
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

ModelR = FEModel();% Rigid model
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

% Solving Flexible
D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.01;
% D.parameters.relTolRes = 1e-10; 
D.parameters.scaling = 1e6;
D.runIntegration();

% Solving Rigid
ModelR.listElements{count-2}.f = u1_init;
ModelR.listElements{count-1}.f = u2_init;
ModelR.listElements{count}.f = u3_init;

DR = DynamicIntegration(ModelR);
DR.parameters.finaltime = finaltime;
DR.parameters.timestepsize = timestepsize;
DR.parameters.rho = 0.01;
% DR.parameters.relTolRes = 1e-10;
DR.parameters.scaling = 1e6;
DR.runIntegration();

%% Plots

x = Model.listNodes{end}.position(1,:);
y = Model.listNodes{end}.position(2,:);
z = Model.listNodes{end}.position(3,:);

xR = ModelR.listNodes{end}.position(1,:);
yR = ModelR.listNodes{end}.position(2,:);
zR = ModelR.listNodes{end}.position(3,:);

figure
hold on
plot3(x,y,z, '-o','Linewidth',2, 'Color','r')
plot3(xR,yR,zR,'--', 'Linewidth',2, 'Color','b')
plot3(trajx,trajy,trajz, 'Linewidth',2, 'Color','k')
legend('With u','With u_{rigid}','Prescribed')
xlabel('X [m]','Fontsize',16)
ylabel('Y [m]','Fontsize',16)
zlabel('Z [m]','Fontsize',16)
% title('Trajectory of flexible arm','Fontsize',18)
grid on

% figure
% hold on
% plot(timeVector,x,timeVector,y,timeVector,z)
% plot(timeVector,trajx,'--',timeVector,trajy,'--',timeVector,trajz,'--')
% legend('X', 'Y', 'Z','Xd','Yd','Zd')
% grid on

joint1_init = ModelR.listElementVariables{1};
joint2_init = ModelR.listElementVariables{2};
joint3_init = ModelR.listElementVariables{3};

joint1 = Model.listElementVariables{1};
joint2 = Model.listElementVariables{2};
joint3 = Model.listElementVariables{3};

figure
hold on
plot(timeVector,joint1.relCoo,timeVector,joint2.relCoo,timeVector,joint3.relCoo,'Linewidth',2)
plot(timeVector,joint1_init.relCoo,'--',timeVector,joint2_init.relCoo,'--',timeVector,joint3_init.relCoo,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints angle (rad)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% title('Joints angle of a flexible Robot','Fontsize',18)
grid on

figure
hold on
plot(timeVector,u1,timeVector,u2,timeVector,u3,'Linewidth',2)
plot(timeVector,u1_init(:),'--',timeVector,u2_init(:),'--',timeVector,u3_init(:),'--','Linewidth',2)
xlabel('Time [s]','Fontsize',16)
ylabel('Commands [Nm]','Fontsize',16)
legend('u_1','u_2','u_3','u_{1,rigid}','u_{2,rigid}','u_{3,rigid}','Location','Best')
% title('Commands of a flexible arm system','Fontsize',18)
grid on

RelError = zeros(size(timeVector));
RelErrorR = zeros(size(timeVector));
for i = 1:length(RelError)
    RelError(i) = 100*(norm([trajx(i)-x(i) trajy(i)-y(i) trajz(i)-z(i)])/norm([trajx(i) trajy(i) trajz(i)]));
    RelErrorR(i) = 100*(norm([trajx(i)-xR(i) trajy(i)-yR(i) trajz(i)-zR(i)])/norm([trajx(i) trajy(i) trajz(i)]));
end
figure
plot(timeVector,RelError,timeVector,RelErrorR)
title('Relative error of the direct dynamic trajectory','Fontsize',13)
xlabel('Time')
ylabel('Relative Error (%)')
legend('Flex','Rigid')
grid on

RMSRelError = sqrt(mean(RelError.^2))
MaxRelError = max(RelError)
RMSRelErrorR = sqrt(mean(RelErrorR.^2))
MaxRelErrorR = max(RelErrorR)

figure
hold on
plot(timeVector,joint1.velocity,timeVector,joint2.velocity,timeVector,joint3.velocity,'Linewidth',2)
plot(timeVector,joint1_init.velocity,'--',timeVector,joint2_init.velocity,'--',timeVector,joint3_init.velocity,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints velocity (rad/s)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid')
title('Joints velocity','Fontsize',18)
grid on

figure
hold on
plot(timeVector,joint1.acceleration,timeVector,joint2.acceleration,timeVector,joint3.acceleration,'Linewidth',2)
plot(timeVector,joint1_init.acceleration,'--',timeVector,joint2_init.acceleration,'--',timeVector,joint3_init.acceleration,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints accelerations (rad/s^2)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid')
title('Joints accelerations','Fontsize',18)
grid on

