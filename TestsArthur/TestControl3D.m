%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control

finaltime = 2;
timestepsize = 0.01;

nodes = [1 0 0 0;
         2 0 0 0;
         3 0.5 0.75 0;
         4 0.5 0.75 0;
         5 1 0 0];
     
elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2 3];
elements{1}.mass = 1;
elements{1}.J = ones(3);

elements{10}.type = 'RigidBodyElement';
elements{10}.nodes = [4 5];
elements{10}.mass = 1;
elements{10}.J = ones(3);

% elements{2}.type = 'ExternalForce';
% elements{2}.nodes = 3;
% elements{2}.DOF = 2;
% elements{2}.amplitude = -10;
% elements{2}.frequency = 0;

% elements{20}.type = 'ExternalForce';
% elements{20}.nodes = 5;
% elements{20}.DOF = 2;
% elements{20}.amplitude = -10;
% elements{20}.frequency = 0;

elements{3}.type = 'KinematicConstraint';
elements{3}.nodes = [1 2];
elements{3}.A = [0 0 0 1 0 0;...
                 0 0 0 0 1 0;...
                 0 0 0 0 0 1]';
% elements{3}.k = 1e2;

elements{4}.type = 'KinematicConstraint';
elements{4}.nodes = [3 4];
elements{4}.A = [0 0 0 1 0 0;...
                 0 0 0 0 1 0;...
                 0 0 0 0 0 1]';
% elements{4}.k = 1e2;


% Trajectory

x_end = 1.5;
x_i = nodes(end,2);

y_end = 0.5;
y_i = nodes(end,3);

z_end = -0.7;
z_i = nodes(end,4);

A = [finaltime^5 finaltime^4 finaltime^3 0 0 1
     5*finaltime^4 4*finaltime^3 3*finaltime^2 0 0 0
     20*finaltime^3 12*finaltime^2 6*finaltime 0 0 0
     0 0 0 0 0 1
     0 0 0 0 1 0
     0 0 0 1 0 0];

cpx = A\[x_end 0 0 x_i 0 0]';
cpy = A\[y_end 0 0 y_i 0 0]';
cpz = A\[z_end 0 0 z_i 0 0]';

temps = 0:timestepsize:finaltime;
trajx = cpx(1)*temps.^5 + cpx(2)*temps.^4 + cpx(3)*temps.^3 + cpx(4)*temps.^2 + cpx(5)*temps + cpx(6);
trajy = cpy(1)*temps.^5 + cpy(2)*temps.^4 + cpy(3)*temps.^3 + cpy(4)*temps.^2 + cpy(5)*temps + cpy(6);
trajz = cpz(1)*temps.^5 + cpz(2)*temps.^4 + cpz(3)*temps.^3 + cpz(4)*temps.^2 + cpz(5)*temps + cpz(6);


elements{5}.type = 'TrajectoryConstraint';
elements{5}.nodes = [5];
elements{5}.T = [trajx;...
                 trajy;...
                 trajz];
elements{5}.A = [1 0 0;...
                 0 1 0;...
                 0 0 1];
% elements{5}.finaltime = finaltime;


BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

S = DynamicIntegration(Model);
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = timestepsize;
S.parameters.rho = 0.9;
S.runIntegration();

figure
hold on
plot3(Model.listNodes{5}.position(1,:),Model.listNodes{5}.position(2,:),Model.listNodes{5}.position(3,:), 'Linewidth',2)
plot3(Model.listNodes{5}.position(1,:),Model.listNodes{5}.position(2,:),-3*ones(1,finaltime/timestepsize + 1), 'Linewidth',2,'Color',[.8 .8 .8])
axis(3*[-1 1 -1 1 -1 1])
grid on


figure
plot(S.parameters.time,Model.listNodes{5}.position(1,:),S.parameters.time,Model.listNodes{5}.position(2,:),S.parameters.time,Model.listNodes{5}.position(3,:))
legend('X', 'Y', 'Z')

figure
plot(S.parameters.time,Model.listElementVariables{4}.value(1,:),...
    S.parameters.time,Model.listElementVariables{4}.value(2,:),...
    S.parameters.time,Model.listElementVariables{4}.value(3,:),...
    S.parameters.time,Model.listElementVariables{4}.value(4,:),...
    S.parameters.time,Model.listElementVariables{4}.value(5,:),...
    S.parameters.time,Model.listElementVariables{4}.value(6,:))
legend('1','2','3','4','5','6')

figure
plot(S.parameters.time,Model.listElementVariables{5}.value(1,:),...
    S.parameters.time,Model.listElementVariables{5}.value(2,:),...
    S.parameters.time,Model.listElementVariables{5}.value(3,:),...
    S.parameters.time,Model.listElementVariables{5}.value(4,:),...
    S.parameters.time,Model.listElementVariables{5}.value(5,:),...
    S.parameters.time,Model.listElementVariables{5}.value(6,:))
legend('1','2','3','4','5','6')

figure
hold on
plot(S.parameters.time,Model.listElementVariables{6}.value(1,:))
plot(S.parameters.time,Model.listElementVariables{6}.value(2,:))
plot(S.parameters.time,Model.listElementVariables{6}.value(3,:))




