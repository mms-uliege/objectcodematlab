%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control

clear
clc

finaltime = 2;
timestepsize = 0.01;
     
nodes = [1 -1 0 0;
 2 -1 0 0];
     
elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2];
elements{1}.mass = 3;
% elements{1}.J = eye(3); 

% 
elements{4}.type = 'KinematicConstraint';
elements{4}.nodes = [1 2];
elements{4}.A = [1 0 0 0 0 0]';

% elements{10}.type = 'ExternalForce';
% elements{10}.nodes = 4;
% elements{10}.DOF = 1;
% elements{10}.amplitude = -10;
% elements{10}.frequency = 0;

% Trajectory

x_end = 1;
x_i = nodes(end,2);
% x_end = nodes(end,2);
% x_i = nodes(end,2);
r = 1;

timeVector = 0:timestepsize:finaltime;
trajx = halfCircleTraj(x_i,x_end,r,timeVector,'lin',0.2,1.7);

elements{6}.type = 'TrajectoryConstraint';
elements{6}.nodes = [2];
elements{6}.T = [trajx];
elements{6}.Axe = [1 0 0];
elements{6}.elements = [4];
elements{6}.active = 0;

% elements{6}.type = 'TrajectoryConstraint';
% elements{6}.nodes = [2];
% elements{6}.T = [trajx];
% elements{6}.Axe = [1 0 0];
% elements{6}.elements = [4];
% elements{6}.active = 1;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.0;
% D.parameters.relTolRes = 1e-12;
% D.parameters.scaling = 1e6;
D.runIntegration();


npts = 81;
timeVector = 0:finaltime/(npts-1):finaltime;
trajx = halfCircleTraj(x_i,x_end,r,timeVector,'lin',0.2,1.7);
Model.listElements{6}.active = 1;
Model.listElements{6}.T = [trajx];

for n = Model.listNumberNodes
    Model.listNodes{n}.InitializeD_Opti();
end
for n = Model.listNumberElementVariables
    Model.listElementVariables{n}.InitializeD_Opti();
end

S = DirectTranscriptionOpti(Model);
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = timestepsize;
S.npts = npts;
S.parameters.rho = 0.0;
S.parameters.scaling = 1e6;
xSol = S.runOpti(D);

timeSteps = S.timeValues;
timeLoc = S.timesteps;

figure
hold on
plot(timeSteps,trajx,'--')
plot(timeSteps,Model.listNodes{2}.position(1,timeLoc),timeSteps,Model.listNodes{2}.position(2,timeLoc),'Linewidth',2)
legend('Xd','X', 'Y')

vx_init = Model.listNodes{2}.velocity_InitOpti(1,timeLoc);
vx = Model.listNodes{2}.velocity(1,timeLoc);

dotv_init = Model.listNodes{2}.acceleration_InitOpti(1,timeLoc);
dotv = Model.listNodes{2}.acceleration(1,timeLoc);

figure
plot(timeSteps,vx_init,timeSteps,vx)
legend('Vx_{init}','Vx')
grid on

figure
plot(timeSteps,dotv_init,timeSteps,dotv)
legend('dotv_{init}','dotv')
grid on

u_init = Model.listElementVariables{end}.value_InitOpti(:,timeLoc);

figure
plot(timeSteps,u_init,timeSteps,Model.listElementVariables{end}.value(:,timeLoc))
legend('U_{init}','U')
grid on


