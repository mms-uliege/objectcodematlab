/*
 * This S-function was generated with an automatic
 * code-generator written with Maple 7.
 *
 * (c) by Reinhard Gahleitner
 * Johannes Kepler University Linz, Austria
 * Department of Automatic Control and Control Systems Technology
 * gahleitner@mechatronik.uni-linz.ac.at
 */

#define S_FUNCTION_NAME sflatness_qM_p
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <math.h>

/* defines for simulation sizes */
#define NUMSFCNPARAMS    1
#define NUMCONTSTATES    0
#define NUMDISCSTATES    0
#define NUMINPUTPORTS    1
#define INPUTPORT0WIDTH  12
#define FEEDTHROUGHPORT0 1
#define NUMOUTPUTPORTS   1
#define OUTPUTPORT0WIDTH 3

static void mdlInitializeSizes(SimStruct *S)
{
  ssSetNumSFcnParams(S,NUMSFCNPARAMS);
  if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
    return; /* Parameter mismatch will be reported by Simulink */
  }
  ssSetNumContStates(S,NUMCONTSTATES);
  ssSetNumDiscStates(S,NUMDISCSTATES);

  if (!ssSetNumInputPorts(S,NUMINPUTPORTS)) return;
  ssSetInputPortWidth(S,0,INPUTPORT0WIDTH);
  ssSetInputPortDirectFeedThrough(S,0,FEEDTHROUGHPORT0);

  if (!ssSetNumOutputPorts(S,NUMOUTPUTPORTS)) return;
  ssSetOutputPortWidth(S,0,OUTPUTPORT0WIDTH);

  ssSetNumSampleTimes(S,1);
  ssSetNumRWork(S,0);
  ssSetNumIWork(S,0);
  ssSetNumPWork(S,0);
  ssSetNumModes(S,0);
  ssSetNumNonsampledZCs(S,0);
  ssSetOptions(S,0);
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
  ssSetSampleTime(S,0,INHERITED_SAMPLE_TIME);
  ssSetOffsetTime(S,0,0.0);
}

static void calcOutputs(real_T *y,real_T *u,real_T *p)
{
  real_T t1;
  real_T t102;
  real_T t106;
  real_T t107;
  real_T t108;
  real_T t109;
  real_T t11;
  real_T t110;
  real_T t114;
  real_T t117;
  real_T t12;
  real_T t122;
  real_T t126;
  real_T t127;
  real_T t129;
  real_T t13;
  real_T t130;
  real_T t134;
  real_T t135;
  real_T t14;
  real_T t141;
  real_T t15;
  real_T t157;
  real_T t158;
  real_T t162;
  real_T t164;
  real_T t166;
  real_T t167;
  real_T t17;
  real_T t170;
  real_T t171;
  real_T t176;
  real_T t179;
  real_T t18;
  real_T t182;
  real_T t185;
  real_T t188;
  real_T t19;
  real_T t195;
  real_T t196;
  real_T t197;
  real_T t198;
  real_T t199;
  real_T t20;
  real_T t201;
  real_T t202;
  real_T t203;
  real_T t204;
  real_T t207;
  real_T t208;
  real_T t209;
  real_T t21;
  real_T t210;
  real_T t214;
  real_T t216;
  real_T t217;
  real_T t218;
  real_T t22;
  real_T t222;
  real_T t224;
  real_T t225;
  real_T t232;
  real_T t237;
  real_T t238;
  real_T t239;
  real_T t24;
  real_T t240;
  real_T t242;
  real_T t244;
  real_T t245;
  real_T t248;
  real_T t25;
  real_T t254;
  real_T t256;
  real_T t258;
  real_T t261;
  real_T t263;
  real_T t264;
  real_T t265;
  real_T t27;
  real_T t270;
  real_T t273;
  real_T t28;
  real_T t287;
  real_T t296;
  real_T t3;
  real_T t30;
  real_T t31;
  real_T t32;
  real_T t321;
  real_T t328;
  real_T t33;
  real_T t332;
  real_T t345;
  real_T t35;
  real_T t352;
  real_T t353;
  real_T t354;
  real_T t355;
  real_T t357;
  real_T t36;
  real_T t360;
  real_T t361;
  real_T t365;
  real_T t366;
  real_T t37;
  real_T t372;
  real_T t374;
  real_T t375;
  real_T t376;
  real_T t379;
  real_T t38;
  real_T t380;
  real_T t389;
  real_T t393;
  real_T t395;
  real_T t396;
  real_T t397;
  real_T t399;
  real_T t40;
  real_T t400;
  real_T t403;
  real_T t404;
  real_T t405;
  real_T t407;
  real_T t408;
  real_T t41;
  real_T t410;
  real_T t412;
  real_T t419;
  real_T t426;
  real_T t428;
  real_T t430;
  real_T t44;
  real_T t444;
  real_T t45;
  real_T t451;
  real_T t455;
  real_T t461;
  real_T t47;
  real_T t472;
  real_T t473;
  real_T t476;
  real_T t479;
  real_T t48;
  real_T t481;
  real_T t482;
  real_T t487;
  real_T t489;
  real_T t49;
  real_T t499;
  real_T t50;
  real_T t500;
  real_T t502;
  real_T t503;
  real_T t505;
  real_T t506;
  real_T t507;
  real_T t51;
  real_T t510;
  real_T t514;
  real_T t516;
  real_T t519;
  real_T t52;
  real_T t520;
  real_T t521;
  real_T t528;
  real_T t53;
  real_T t534;
  real_T t539;
  real_T t54;
  real_T t544;
  real_T t548;
  real_T t549;
  real_T t55;
  real_T t550;
  real_T t551;
  real_T t552;
  real_T t553;
  real_T t554;
  real_T t555;
  real_T t556;
  real_T t557;
  real_T t56;
  real_T t563;
  real_T t564;
  real_T t565;
  real_T t566;
  real_T t567;
  real_T t568;
  real_T t571;
  real_T t572;
  real_T t573;
  real_T t574;
  real_T t578;
  real_T t579;
  real_T t580;
  real_T t583;
  real_T t585;
  real_T t587;
  real_T t59;
  real_T t592;
  real_T t6;
  real_T t60;
  real_T t61;
  real_T t615;
  real_T t62;
  real_T t625;
  real_T t626;
  real_T t63;
  real_T t631;
  real_T t634;
  real_T t638;
  real_T t64;
  real_T t640;
  real_T t642;
  real_T t65;
  real_T t652;
  real_T t654;
  real_T t655;
  real_T t656;
  real_T t657;
  real_T t658;
  real_T t659;
  real_T t660;
  real_T t661;
  real_T t663;
  real_T t669;
  real_T t673;
  real_T t676;
  real_T t68;
  real_T t685;
  real_T t686;
  real_T t689;
  real_T t69;
  real_T t690;
  real_T t693;
  real_T t70;
  real_T t701;
  real_T t705;
  real_T t706;
  real_T t707;
  real_T t708;
  real_T t709;
  real_T t710;
  real_T t711;
  real_T t712;
  real_T t713;
  real_T t714;
  real_T t715;
  real_T t716;
  real_T t718;
  real_T t722;
  real_T t723;
  real_T t724;
  real_T t725;
  real_T t726;
  real_T t727;
  real_T t728;
  real_T t729;
  real_T t730;
  real_T t732;
  real_T t736;
  real_T t737;
  real_T t74;
  real_T t740;
  real_T t742;
  real_T t744;
  real_T t747;
  real_T t748;
  real_T t75;
  real_T t751;
  real_T t754;
  real_T t760;
  real_T t761;
  real_T t762;
  real_T t764;
  real_T t765;
  real_T t766;
  real_T t768;
  real_T t774;
  real_T t775;
  real_T t785;
  real_T t786;
  real_T t789;
  real_T t79;
  real_T t791;
  real_T t793;
  real_T t795;
  real_T t796;
  real_T t797;
  real_T t799;
  real_T t801;
  real_T t803;
  real_T t805;
  real_T t807;
  real_T t810;
  real_T t82;
  real_T t824;
  real_T t83;
  real_T t830;
  real_T t837;
  real_T t847;
  real_T t849;
  real_T t850;
  real_T t87;
  real_T t88;
  real_T t9;
  real_T t92;
  real_T t93;
  {
    t1 = p[17];
    t3 = u[3];
    t6 = u[6];
    t9 = u[9];
    t11 = p[28];
    t12 = u[1];
    t13 = cos(t12);
    t14 = t13*t13;
    t15 = t11*t14;
    t17 = p[36];
    t18 = u[2];
    t19 = t18+t12;
    t20 = cos(t19);
    t21 = t20*t20;
    t22 = t17*t21;
    t24 = p[32];
    t25 = t24*t14;
    t27 = p[40];
    t28 = t27*t21;
    t30 = p[39];
    t31 = sin(t19);
    t32 = t31*t31;
    t33 = t30*t32;
    t35 = p[27];
    t36 = sin(t12);
    t37 = t36*t36;
    t38 = t35*t37;
    t40 = p[35];
    t41 = t40*t32;
    t44 = p[31];
    t45 = t44*t37;
    t47 = u[4];
    t48 = u[5];
    t49 = t47+t48;
    t50 = t21*t49;
    t51 = p[38];
    t52 = t50*t51;
    t53 = t3*t48;
    t54 = p[23];
    t55 = t54*t54;
    t56 = t53*t55;
    t59 = t32*t49;
    t60 = p[34];
    t61 = t59*t60;
    t62 = t3*t47;
    t63 = p[22];
    t64 = t63*t63;
    t65 = t62*t64;
    t68 = t20*t31;
    t69 = t68*t60;
    t70 = t6*t48;
    t74 = u[8];
    t75 = t3*t74;
    t79 = t53*t64;
    t82 = t68*t51;
    t83 = t6*t47;
    t87 = u[7];
    t88 = t3*t87;
    t92 = t59*t51;
    t93 = t62*t55;
    t102 = -2.0*t82*t70*t55-2.0*t82*t75*t55-2.0*t82*t83*t55-2.0*t82*t88*t55-2.0
*t69*t70*t64-2.0*t69*t75*t64+t45*t9-2.0*t52*t56+2.0*t61*t65+2.0*t61*t79+2.0*t92
*t93;
    t106 = t13*t36;
    t107 = p[21];
    t108 = t107*t107;
    t109 = t106*t108;
    t110 = t60*t6;
    t114 = t3*t60;
    t117 = p[30];
    t122 = t117*t3;
    t126 = sin(t18);
    t127 = t20*t126;
    t129 = t127*t48*t31;
    t130 = t107*t51;
    t134 = t107*t60;
    t135 = t127*t134;
    t141 = t127*t130;
    t157 = t14*t108;
    t158 = t9*t60;
    t162 = t51*t9;
    t164 = p[26];
    t166 = p[20];
    t167 = t166*t166;
    t170 = t47*t47;
    t171 = t170*t3;
    t176 = t11*t37;
    t179 = t24*t37;
    t182 = t35*t14;
    t185 = t44*t14;
    t188 = t14*t164*t9*t167+t21*t51*t9*t55+t157*t117*t9+t157*t158+t157*t162+2.0
*t176*t171+2.0*t179*t171+2.0*t182*t171+2.0*t185*t171-2.0*t38*t171-2.0*t45*t171;
    t195 = cos(t18);
    t196 = t195*t13;
    t197 = t196*t31;
    t198 = t108*t51;
    t199 = t198*t88;
    t201 = t195*t36;
    t202 = t201*t170;
    t203 = t31*t108;
    t204 = t51*t3;
    t207 = t20*t36;
    t208 = t207*t107;
    t209 = t47*t63;
    t210 = t110*t209;
    t214 = t114*t87*t63;
    t216 = t51*t6;
    t217 = t47*t54;
    t218 = t216*t217;
    t222 = t204*t87*t54;
    t224 = t13*t31;
    t225 = t224*t107;
    t232 = t21*t126*t107;
    t237 = t20*t13;
    t238 = t237*t126;
    t239 = t108*t60;
    t240 = t239*t83;
    t242 = t239*t88;
    t244 = t207*t170;
    t245 = t126*t108;
    t248 = t198*t83;
    t254 = -t244*t245*t114-t244*t245*t204-t197*t240+t238*t199+t232*t210+t232*
t214+t232*t218+t232*t222+t238*t240+t238*t242+t238*t248;
    t256 = t237*t170;
    t258 = t130*t3*t54;
    t261 = t134*t3*t63;
    t263 = t31*t49;
    t264 = t263*t13;
    t265 = t6*t54;
    t270 = t36*t170*t31;
    t273 = t48*t63;
    t287 = t48*t54;
    t296 = t6*t63;
    t321 = t106*t164;
    t328 = -2.0*t20*t51*t6*t55*t31*t49-2.0*t20*t60*t6*t64*t31*t49-2.0*t225*t204
*t74*t54-t109*t204*t87-3.0*t109*t216*t47+t202*t203*t114-2.0*t264*t134*t296-4.0*
t321*t83*t167-2.0*t321*t88*t167-t197*t242-t197*t248;
    t332 = t237*t107;
    t345 = t50*t60;
    t352 = t21*t195;
    t353 = t49*t107;
    t354 = t352*t353;
    t355 = t114*t209;
    t357 = t204*t217;
    t360 = t31*t107;
    t361 = t20*t195*t360;
    t365 = t195*t107;
    t366 = t59*t365;
    t372 = t263*t36*t107;
    t374 = t40*t21;
    t375 = t49*t3;
    t376 = t375*t48;
    t379 = t30*t21;
    t380 = t375*t47;
    t389 = -t361*t214-t361*t218-2.0*t22*t376-2.0*t22*t380-t361*t222+t366*t355+
t366*t357+t372*t357+2.0*t374*t376+2.0*t379*t376+2.0*t379*t380;
    t393 = t32*t17;
    t395 = t17*t20;
    t396 = t31*t6;
    t397 = t396*t48;
    t399 = t31*t3;
    t400 = t399*t74;
    t403 = t24*t13;
    t404 = t36*t3;
    t405 = t404*t87;
    t407 = t27*t20;
    t408 = t396*t47;
    t410 = t399*t87;
    t412 = t27*t32;
    t419 = t40*t20;
    t426 = t44*t13;
    t428 = t30*t20;
    t430 = -t41*t376+t412*t376-t41*t380-t407*t397+t419*t397-t407*t400+t419*t400
+t426*t405+t419*t408+t428*t408+t419*t410;
    t444 = t11*t13;
    t451 = t396*t49;
    t455 = t6*t36*t47;
    t461 = t35*t13;
    t472 = t37*t170;
    t473 = t198*t3;
    t476 = t164*t3*t167;
    t479 = t239*t3;
    t481 = t108*t117;
    t482 = t481*t3;
    t487 = -2.0*t395*t451-4.0*t403*t455+2.0*t461*t405-2.0*t407*t451+2.0*t428*
t451-4.0*t444*t455+4.0*t461*t455+t472*t473+2.0*t472*t476+t472*t479+2.0*t472*
t482;
    t489 = t14*t170;
    t499 = t263*t13*t126;
    t500 = t198*t62;
    t502 = t195*t48;
    t503 = t237*t502;
    t505 = t126*t48;
    t506 = t505*t224;
    t507 = t239*t62;
    t510 = t196*t20*t49;
    t514 = 2.0*t374*t380-t489*t473-2.0*t489*t476-t489*t479-2.0*t489*t482-t499*
t500+t503*t500+t506*t500-t510*t500+t506*t507-t510*t507;
    t516 = t237*t353;
    t519 = t36*t47;
    t520 = t519*t360;
    t521 = t114*t273;
    t528 = t204*t287;
    t534 = t352*t48*t107;
    t539 = t372*t355-2.0*t516*t355+t534*t355-2.0*t516*t357+t534*t357-t499*t507+
t503*t507-2.0*t516*t521-2.0*t516*t528+2.0*t520*t521+2.0*t520*t528;
    y[0] = 1/t1*(t539+t38*t9+t328+t487-3.0*t109*t110*t47-t109*t114*t87-2.0*t109
*t122*t87+t202*t203*t204-2.0*t264*t130*t265-2.0*t225*t110*t273-2.0*t225*t216*
t287+2.0*t332*t158*t63+2.0*t332*t162*t54-2.0*t69*t83*t64-2.0*t69*t88*t64+t102+
t254+p[42]*t6+p[25]*t9+t514-4.0*t109*t117*t6*t47+t129*t130*t62*t54+t129*t134*
t62*t63+t21*t60*t9*t64-2.0*t225*t114*t74*t63+2.0*t270*t261-2.0*t225*t218-3.0*
t208*t210-t208*t214-3.0*t208*t218-t208*t222-2.0*t225*t210-2.0*t15*t171-2.0*t25*
t171-t197*t199+2.0*t92*t56+t1*t3+t15*t9+t22*t9+t28*t9+t33*t9+t41*t9+2.0*t428*
t410+2.0*t428*t397+2.0*t428*t400-2.0*t33*t376-2.0*t444*t405-2.0*t395*t408-2.0*
t395*t410+2.0*t419*t451+4.0*t426*t455+2.0*t393*t376-2.0*t403*t405-2.0*t407*t408
-2.0*t407*t410+2.0*t412*t380-2.0*t28*t380-2.0*t28*t376+2.0*t393*t380-2.0*t395*
t397-2.0*t395*t400-t354*t355-t354*t357-t361*t210-2.0*t345*t65-2.0*t345*t79-2.0*
t52*t93-2.0*t225*t222+2.0*t270*t258-t256*t258-t256*t261+t25*t9-2.0*t33*t380-2.0
*t214*t225-2.0*t135*t62*t63*t31*t49-2.0*t141*t62*t54*t31*t49+t188+2.0*t430+t389
);
    t544 = u[10];
    t548 = t60*t544;
    t549 = t548*t64;
    t550 = u[11];
    t551 = t60*t550;
    t552 = t551*t64;
    t553 = t51*t544;
    t554 = t553*t55;
    t555 = t51*t550;
    t556 = t555*t55;
    t557 = p[18];
    t563 = p[37];
    t564 = t563*t544;
    t565 = t563*t550;
    t566 = p[41];
    t567 = t566*t544;
    t568 = t566*t550;
    t571 = -t164*t544*t167-t557*t47-t481*t544-p[29]*t544-p[33]*t544-p[43]*t87-
t549-t552-t554-t556-t564-t565-t567-t568;
    t572 = t126*t107;
    t573 = t572*t51;
    t574 = t47*t74;
    t578 = t502*t107;
    t579 = p[0];
    t580 = t60*t579;
    t583 = t572*t60;
    t585 = t579*t20*t49;
    t587 = t505*t107;
    t592 = t579*t31*t49;
    t615 = t579*t107*t51;
    t625 = 2.0*t69*t3*t64*t6;
    t626 = -2.0*t321*t3*t167*t6-t578*t51*t579*t31+3.0*t587*t51*t74*t54+3.0*t587
*t60*t74*t63-2.0*t109*t122*t6+t127*t48*t615+t263*t195*t615+t587*t580*t20-t578*
t580*t31+t365*t60*t592+2.0*t573*t574*t54+2.0*t583*t574*t63-t573*t585-t583*t585-
t625;
    t631 = 2.0*t82*t3*t55*t6;
    t634 = t587*t60*t87*t63;
    t638 = t587*t51*t87*t54;
    t640 = t48*t48;
    t642 = t195*t640*t107;
    t652 = t60*t63*t592;
    t654 = t51*t54*t592;
    t655 = t3*t3;
    t656 = t60*t655;
    t657 = t656*t64;
    t658 = t59*t657;
    t659 = t51*t655;
    t660 = t659*t55;
    t661 = t59*t660;
    t663 = t195*t640*t48;
    t669 = t519*t579;
    t673 = t404*t6;
    t676 = 2.0*t642*t51*t47*t54+2.0*t642*t60*t47*t63+t117*t107*t669+t663*t130*
t54+t663*t134*t63+t164*t166*t669+2.0*t461*t673-t631+4.0*t634+4.0*t638+t652+t654
+t658+t661;
    t685 = t126*t126;
    t686 = t685*t108;
    t689 = t195*t195;
    t690 = t689*t108;
    t693 = t47*t655;
    t701 = -t365*t551*t63-t15*t693+t176*t693+t179*t693+t182*t693+t185*t693-t38*
t693-2.0*t403*t673+2.0*t426*t673-2.0*t444*t673-t45*t693-t686*t548-t690*t548-
t686*t553-t690*t553;
    t705 = t49*t655;
    t706 = t41*t705;
    t707 = t33*t705;
    t708 = t393*t705;
    t709 = t412*t705;
    t710 = t374*t705;
    t711 = t379*t705;
    t712 = t22*t705;
    t713 = t28*t705;
    t714 = t59*t195;
    t715 = t655*t54;
    t716 = t130*t715;
    t718 = t114*t296;
    t722 = 2.0*t225*t718;
    t723 = t519*t31;
    t724 = t655*t63;
    t725 = t134*t724;
    t726 = t723*t725;
    t727 = t237*t49;
    t728 = t727*t725;
    t729 = 2.0*t232*t718-t25*t693+t714*t716-t706-t707+t708+t709+t710+t711-t712-
t713-t722+t726-t728;
    t730 = t352*t48;
    t732 = t204*t265;
    t736 = t3*t6;
    t737 = t239*t736;
    t740 = t245*t656;
    t742 = t207*t47;
    t744 = t352*t49;
    t747 = t237*t195;
    t748 = t48*t108;
    t751 = t198*t736;
    t754 = t245*t659;
    t760 = 2.0*t225*t732;
    t761 = t723*t716;
    t762 = t747*t748*t656+t747*t748*t659+2.0*t232*t732+2.0*t238*t737+2.0*t238*
t751-t264*t740-t264*t754+t730*t716-t744*t716+t730*t725-t744*t725-t742*t740-t742
*t754-t760+t761;
    t764 = t727*t716;
    t765 = t201*t47;
    t766 = t203*t659;
    t768 = t49*t108;
    t774 = t505*t13;
    t775 = t203*t656;
    t785 = t37*t47;
    t786 = t481*t655;
    t789 = t164*t655*t167;
    t791 = t14*t47;
    t793 = -t365*t555*t54-t747*t768*t656-t747*t768*t659-2.0*t197*t737-2.0*t197*
t751+t714*t725+t765*t766+t765*t775+t774*t766+t774*t775+t785*t786+t785*t789-t791
*t786-t764;
    t795 = t50*t657;
    t796 = t50*t660;
    t797 = t399*t6;
    t799 = 2.0*t419*t797;
    t801 = 2.0*t428*t797;
    t803 = 2.0*t395*t797;
    t805 = 2.0*t407*t797;
    t807 = t365*t548*t63;
    t810 = t365*t553*t54;
    t824 = -2.0*t135*t724*t263-2.0*t141*t715*t263+t129*t716+t129*t725-2.0*t361*
t718-2.0*t361*t732-t791*t789-t795-t796+t799+t801-t803-t805-2.0*t807-2.0*t810;
    y[1] = -(t571+t626+t676+t701+t729+t762+t793+t824)/t557;
    t830 = p[19];
    t837 = -2.0*t583*t209*t87-t830*t48-p[44]*t74-t549-t552-t554-t556-t564-t565-
t567-t568;
    t847 = -t578*t51*t170*t54-t578*t60*t170*t63-2.0*t573*t217*t87-t625-t631+
t634+t638+t652+t654+t658+t661;
    t849 = -t706-t707+t708+t709+t710+t711-t712-t713-t722+t726-t728;
    t850 = -t760+t761-t764-t795-t796+t799+t801-t803-t805-t807-t810;
    y[2] = -(t837+t847+t849+t850)/t830;
    return;
  }
}

static void mdlOutputs(SimStruct *S,int_T tid)
{
  real_T *y = ssGetOutputPortRealSignal(S,0);
  real_T *p = mxGetPr(ssGetSFcnParam(S,0));
  int_T i;
  InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);
  real_T u[INPUTPORT0WIDTH];

  for(i=0; i<INPUTPORT0WIDTH; i++) {
    u[i]=*uPtrs[i];
  }
  calcOutputs(y,u,p);
}

static void mdlTerminate(SimStruct *S)
{
}

#ifdef MATLAB_MEX_FILE   /* Compile as a MEX-file? */
  #include "simulink.c"  /* MEX-file interface mechanism */
#else
  #include "cg_sfun.h"   /* Code generation registration */
#endif
