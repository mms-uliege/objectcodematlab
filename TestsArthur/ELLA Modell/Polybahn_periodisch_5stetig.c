/*  Polynombahn mit Np-Koeffizienten, periodisch implementiert
      
    Autor: Peter Staufer
    Datum: 15.08.2010
 
    u=[t,delta_q,T_end,T_Periode]
 *
 *      t...aktuelle Systemzeit
 *      delta_q...Verfahrweg
 *      T_end...Endzeit der Trajektorie
 *      T_Periode...Periodendauer des Zyklus
 *
    y=[q,q_p,q_pp,q_ppp,q_pppp]

    mit p=[Ts]...Parametervektor
*/
 
#define S_FUNCTION_NAME  Polybahn_periodisch_5stetig
#define S_FUNCTION_LEVEL 2
#include <math.h>
#include "simstruc.h"

#define NP 9 //muss immer ungerade sein

void gaussj(real_T A[NP+1][NP+1],  real_T b[NP+1]);

double coeff_vec(int i,int j,double t);

/*====================*
 * S-function methods *
 *====================*/

/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *    The sizes information is used by Simulink to determine the S-function
 *    block's characteristics (number of inputs, outputs, states, etc.).
 */
static void mdlInitializeSizes(SimStruct *S)
{
    int i;
/* See sfuntmpl_doc.c for more details on the macros below */

    ssSetNumSFcnParams(S, 0);  /* Number of expected parameters */
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        /* Return if number of expected != number of actual parameters */
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 4)) return;
    for(i=0;i<4;i++){ 
		ssSetInputPortWidth(   S,  i, 1);
	    ssSetInputPortDirectFeedThrough(S, i, 1);
	}

    if (!ssSetNumOutputPorts(S, 5)) return;
    for(i=0;i<5;i++) 
		ssSetOutputPortWidth(   S,  i, 1);

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, 0);
}



/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *    This function is used to specify the sample time(s) for your
 *    S-function. You must register the same number of sample times as
 *    specified in ssSetNumSampleTimes.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, -1);
    ssSetOffsetTime(S, 0, 0.0);

}



#undef MDL_INITIALIZE_CONDITIONS   /* Change to #undef to remove function */
#if defined(MDL_INITIALIZE_CONDITIONS)
  /* Function: mdlInitializeConditions ========================================
   * Abstract:
   *    In this function, you should initialize the continuous and discrete
   *    states for your S-function block.  The initial states are placed
   *    in the state vector, ssGetContStates(S) or ssGetRealDiscStates(S).
   *    You can also perform any other initialization activities that your
   *    S-function may require. Note, this routine will be called at the
   *    start of simulation and if it is present in an enabled subsystem
   *    configured to reset states, it will be call when the enabled subsystem
   *    restarts execution to reset the states.
   */
  static void mdlInitializeConditions(SimStruct *S)
{
  }
#endif /* MDL_INITIALIZE_CONDITIONS */



#undef MDL_START  /* Change to #undef to remove function */
#if defined(MDL_START) 
  /* Function: mdlStart =======================================================
   * Abstract:
   *    This function is called once at start of model execution. If you
   *    have states that should be initialized once, this is the place
   *    to do it.
   */
  static void mdlStart(SimStruct *S)
  {
}
#endif /*  MDL_START */



/* Function: mdlOutputs =======================================================
 * Abstract:
 *    In this function, you compute the outputs of your S-function
 *    block. Generally outputs are placed in the output vector, ssGetY(S).
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    InputRealPtrsType uPtrs0       = ssGetInputPortRealSignalPtrs(S,0);
   	InputRealPtrsType uPtrs1 	   = ssGetInputPortRealSignalPtrs(S,1);
   	InputRealPtrsType uPtrs2 	   = ssGetInputPortRealSignalPtrs(S,2);
    InputRealPtrsType uPtrs3 	   = ssGetInputPortRealSignalPtrs(S,3);
    
    double TIME,delta_q,delta_T,T_Periode,t;
    double vorz,rueckphase;
    double q,q_p,q_pp,q_ppp,q_pppp;
    int i,n;
    
    double *y0         = ssGetOutputPortRealSignal(S,0);
	double *y1         = ssGetOutputPortRealSignal(S,1);
	double *y2         = ssGetOutputPortRealSignal(S,2);
    double *y3         = ssGetOutputPortRealSignal(S,3);
	double *y4         = ssGetOutputPortRealSignal(S,4);

    real_T koeff[]={-1471.0,10080.0,-30240.0,52080.0,-56700.0,40446.0,-18900.0,5580.0,-945.0,70.0}; // f�r NP=9;
   
    TIME=(double)      *uPtrs0[0];		// Lokale Subsystemzeit
    delta_q=(double)   *uPtrs1[0];		// Verfahrweg
    delta_T=(double)   *uPtrs2[0];		// Dauer einer Polynombahn
    T_Periode=(double) *uPtrs3[0];		// Gesamtdauer einer Periode    
    n=NP+1;
    
    for (i=0;i<n;i++){ 
       koeff[i]=koeff[i]/pow(delta_T,i);
    }
    
    //Vorzeichen von delta_phi ermitteln und mit Betrag weiterrechnen
    vorz= fabs(delta_q)/delta_q;
    delta_q=fabs(delta_q);
     
    t=TIME;
            while((t-T_Periode)>0)
              t=t-T_Periode;
 
            if (t>T_Periode/2){  //R�ckkehrphase
                t=t-T_Periode/2;
                rueckphase=1;
            }else{
                rueckphase=0;
            }
            
            if (t<=0.0){
                q=0;
                q_p=0;
                q_pp=0;
                q_ppp=0;
                q_pppp=0;
            }else if (t<delta_T) {
                q=0;
                q_p=0;
                q_pp=0;
                q_ppp=0;
                q_pppp=0;
                for(i = 0; i < n; i++) {
                    q=q+koeff[i]*pow(t+delta_T,i);
                }
                for(i = 1; i < n; i++) {
                    q_p=q_p+koeff[i]*coeff_vec(1,i,t+delta_T);
                }
                for(i = 2; i < n; i++) {
                    q_pp=q_pp+koeff[i]*coeff_vec(2,i,t+delta_T);
                }
                for(i = 3; i < n; i++) {
                    q_ppp=q_ppp+koeff[i]*coeff_vec(3,i,t+delta_T);
                }
                for(i = 4; i < n; i++) {
                    q_pppp=q_pppp+koeff[i]*coeff_vec(4,i,t+delta_T);
                }
            }else{
                q=1;
                q_p=0;
                q_pp=0;
                q_ppp=0;
                q_pppp=0;
            }
            

            q=delta_q*vorz*q;
            q_p=delta_q*vorz*q_p;
            q_pp=delta_q*vorz*q_pp;
            q_ppp=delta_q*vorz*q_ppp;
            q_pppp=delta_q*vorz*q_pppp;

            if (rueckphase==1){
                q=vorz*delta_q-q;
                q_p=-1*q_p;
                q_pp=-1*q_pp;
                q_ppp=-1*q_ppp;
                q_pppp=-1*q_pppp;
            }    
       
        y0[0] = q;
        y1[0] = q_p;
        y2[0] = q_pp;
        y3[0] = q_ppp;
        y4[0] = q_pppp;    
}



#undef MDL_UPDATE  /* Change to #undef to remove function */
#if defined(MDL_UPDATE)
  /* Function: mdlUpdate ======================================================
   * Abstract:
   *    This function is called once for every major integration time step.
   *    Discrete states are typically updated here, but this function is useful
   *    for performing any tasks that should only take place once per
   *    integration step.
   */
  static void mdlUpdate(SimStruct *S, int_T tid)
  {
  }
#endif /* MDL_UPDATE */



#undef MDL_DERIVATIVES  /* Change to #undef to remove function */
#if defined(MDL_DERIVATIVES)
  /* Function: mdlDerivatives =================================================
   * Abstract:
   *    In this function, you compute the S-function block's derivatives.
   *    The derivatives are placed in the derivative vector, ssGetdX(S).
   */
  static void mdlDerivatives(SimStruct *S)
  {
  }
#endif /* MDL_DERIVATIVES */



/* Function: mdlTerminate =====================================================
 * Abstract:
 *    In this function, you should perform any actions that are necessary
 *    at the termination of a simulation.  For example, if memory was
 *    allocated in mdlStart, this is the place to free it.
 */
static void mdlTerminate(SimStruct *S)
{
    
}


/*======================================================*
 * See sfuntmpl_doc.c for the optional S-function methods *
 *======================================================*/

/*=============================*
 * Required S-function trailer *
 *=============================*/

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif


double coeff_vec(int i,int j,double t)
{
    double y;
    int k;
    y=1;
    for (k = 0; k < i; k++){
        y=y*(j-k);    
    }
    y=y*pow(t,j-i);
    return y;
}
