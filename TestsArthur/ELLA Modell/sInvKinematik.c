/*
 * This S-function was generated with an automatic
 * code-generator written with Maple 7.
 *
 * (c) by Reinhard Gahleitner
 * Johannes Kepler University Linz, Austria
 * Department of Automatic Control and Control Systems Technology
 * gahleitner@mechatronik.uni-linz.ac.at
 */

#define S_FUNCTION_NAME sInvKinematik
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <math.h>

/* defines for simulation sizes */
#define NUMSFCNPARAMS    2
#define NUMCONTSTATES    1
#define NUMDISCSTATES    0
#define NUMINPUTPORTS    1
#define INPUTPORT0WIDTH  15
#define FEEDTHROUGHPORT0 1
#define NUMOUTPUTPORTS   1
#define OUTPUTPORT0WIDTH 15

static void mdlInitializeSizes(SimStruct *S)
{
  ssSetNumSFcnParams(S,NUMSFCNPARAMS);
  if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
    return; /* Parameter mismatch will be reported by Simulink */
  }
  ssSetNumContStates(S,NUMCONTSTATES);
  ssSetNumDiscStates(S,NUMDISCSTATES);

  if (!ssSetNumInputPorts(S,NUMINPUTPORTS)) return;
  ssSetInputPortWidth(S,0,INPUTPORT0WIDTH);
  ssSetInputPortDirectFeedThrough(S,0,FEEDTHROUGHPORT0);

  if (!ssSetNumOutputPorts(S,NUMOUTPUTPORTS)) return;
  ssSetOutputPortWidth(S,0,OUTPUTPORT0WIDTH);

  ssSetNumSampleTimes(S,1);
  ssSetNumRWork(S,0);
  ssSetNumIWork(S,0);
  ssSetNumPWork(S,0);
  ssSetNumModes(S,0);
  ssSetNumNonsampledZCs(S,0);
  ssSetOptions(S,0);
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
  ssSetSampleTime(S,0,CONTINUOUS_SAMPLE_TIME);
  ssSetOffsetTime(S,0,0.0);
}

#define MDL_INITIALIZE_CONDITIONS
static void mdlInitializeConditions(SimStruct *S)
{
  real_T *x = ssGetContStates(S);
  real_T *p = mxGetPr(ssGetSFcnParam(S,1));
  int_T i;

  for(i=0; i<NUMCONTSTATES; i++) {
    x[i]=p[i];
  }
}

static void calcOutputs(real_T *y,real_T *u,real_T *p)
{
  real_T t1;
  real_T t10;
  real_T t100;
  real_T t101;
  real_T t1033;
  real_T t104;
  real_T t106;
  real_T t108;
  real_T t11;
  real_T t111;
  real_T t112;
  real_T t115;
  real_T t117;
  real_T t119;
  real_T t12;
  real_T t120;
  real_T t121;
  real_T t123;
  real_T t125;
  real_T t126;
  real_T t127;
  real_T t128;
  real_T t13;
  real_T t132;
  real_T t133;
  real_T t134;
  real_T t139;
  real_T t140;
  real_T t142;
  real_T t145;
  real_T t149;
  real_T t15;
  real_T t150;
  real_T t151;
  real_T t153;
  real_T t154;
  real_T t155;
  real_T t156;
  real_T t157;
  real_T t158;
  real_T t16;
  real_T t166;
  real_T t17;
  real_T t170;
  real_T t174;
  real_T t175;
  real_T t176;
  real_T t177;
  real_T t178;
  real_T t18;
  real_T t180;
  real_T t183;
  real_T t185;
  real_T t187;
  real_T t19;
  real_T t191;
  real_T t192;
  real_T t195;
  real_T t196;
  real_T t198;
  real_T t2;
  real_T t20;
  real_T t201;
  real_T t202;
  real_T t203;
  real_T t208;
  real_T t213;
  real_T t214;
  real_T t215;
  real_T t220;
  real_T t221;
  real_T t224;
  real_T t226;
  real_T t227;
  real_T t228;
  real_T t229;
  real_T t232;
  real_T t233;
  real_T t236;
  real_T t238;
  real_T t24;
  real_T t242;
  real_T t243;
  real_T t245;
  real_T t246;
  real_T t248;
  real_T t25;
  real_T t250;
  real_T t253;
  real_T t258;
  real_T t259;
  real_T t262;
  real_T t265;
  real_T t267;
  real_T t269;
  real_T t272;
  real_T t273;
  real_T t274;
  real_T t278;
  real_T t28;
  real_T t280;
  real_T t281;
  real_T t282;
  real_T t286;
  real_T t287;
  real_T t288;
  real_T t29;
  real_T t295;
  real_T t296;
  real_T t298;
  real_T t3;
  real_T t30;
  real_T t302;
  real_T t303;
  real_T t310;
  real_T t319;
  real_T t32;
  real_T t326;
  real_T t33;
  real_T t330;
  real_T t332;
  real_T t334;
  real_T t336;
  real_T t337;
  real_T t338;
  real_T t34;
  real_T t340;
  real_T t341;
  real_T t342;
  real_T t346;
  real_T t351;
  real_T t357;
  real_T t36;
  real_T t361;
  real_T t364;
  real_T t367;
  real_T t369;
  real_T t37;
  real_T t370;
  real_T t371;
  real_T t374;
  real_T t375;
  real_T t379;
  real_T t39;
  real_T t390;
  real_T t393;
  real_T t396;
  real_T t398;
  real_T t407;
  real_T t411;
  real_T t417;
  real_T t42;
  real_T t426;
  real_T t427;
  real_T t43;
  real_T t434;
  real_T t435;
  real_T t438;
  real_T t445;
  real_T t446;
  real_T t449;
  real_T t45;
  real_T t450;
  real_T t453;
  real_T t456;
  real_T t457;
  real_T t46;
  real_T t460;
  real_T t464;
  real_T t465;
  real_T t466;
  real_T t470;
  real_T t473;
  real_T t475;
  real_T t479;
  real_T t48;
  real_T t480;
  real_T t481;
  real_T t484;
  real_T t49;
  real_T t490;
  real_T t494;
  real_T t497;
  real_T t5;
  real_T t501;
  real_T t513;
  real_T t517;
  real_T t52;
  real_T t520;
  real_T t524;
  real_T t526;
  real_T t53;
  real_T t532;
  real_T t549;
  real_T t55;
  real_T t550;
  real_T t553;
  real_T t556;
  real_T t559;
  real_T t562;
  real_T t564;
  real_T t574;
  real_T t575;
  real_T t59;
  real_T t6;
  real_T t60;
  real_T t604;
  real_T t607;
  real_T t63;
  real_T t64;
  real_T t641;
  real_T t643;
  real_T t647;
  real_T t649;
  real_T t65;
  real_T t650;
  real_T t653;
  real_T t66;
  real_T t664;
  real_T t668;
  real_T t67;
  real_T t671;
  real_T t677;
  real_T t689;
  real_T t697;
  real_T t7;
  real_T t701;
  real_T t705;
  real_T t71;
  real_T t72;
  real_T t731;
  real_T t75;
  real_T t76;
  real_T t77;
  real_T t8;
  real_T t80;
  real_T t82;
  real_T t83;
  real_T t843;
  real_T t86;
  real_T t866;
  real_T t870;
  real_T t882;
  real_T t89;
  real_T t9;
  real_T t90;
  real_T t93;
  real_T t935;
  real_T t94;
  real_T t943;
  real_T t954;
  real_T t97;
  real_T t98;
  {
    t1 = u[2];
    t2 = u[0];
    t3 = 1/t2;
    t5 = atan(t1*t3);
    y[0] = -t5;
    t6 = p[1];
    t7 = t2*t2;
    t8 = u[1];
    t9 = t8*t8;
    t10 = t1*t1;
    t11 = t7+t9+t10;
    t12 = sqrt(t11);
    t13 = 1/t12;
    t15 = p[0];
    t16 = t15*t15;
    t17 = t6*t6;
    t18 = t16+t17-t7-t9-t10;
    t19 = t18*t18;
    t20 = 1/t16;
    t24 = 4.0-t19*t20/t17;
    t25 = sqrt(t24);
    t28 = asin(t6*t13*t25/2.0);
    t29 = t8*t3;
    t30 = 1/t7;
    t32 = 1.0+t10*t30;
    t33 = sqrt(t32);
    t34 = 1/t33;
    t36 = atan(t29*t34);
    y[1] = t28+t36;
    t37 = 1/t15;
    t39 = 1/t6;
    t42 = acos(t18*t37*t39/2.0);
    y[2] = -0.3141592653589793E1+t42;
    t43 = u[5];
    t45 = t1*t30;
    t46 = u[3];
    t48 = t43*t3-t45*t46;
    t49 = 1/t32;
    y[3] = -t48*t49;
    t52 = 1/t12/t11;
    t53 = t6*t52;
    t55 = u[4];
    t59 = 2.0*t2*t46+2.0*t8*t55+2.0*t1*t43;
    t60 = t25*t59;
    t63 = t39*t13;
    t64 = 1/t25;
    t65 = t63*t64;
    t66 = t18*t20;
    t67 = -t59;
    t71 = -t53*t60/4.0-t65*t66*t67/2.0;
    t72 = 1/t11;
    t75 = 4.0-t17*t72*t24;
    t76 = sqrt(t75);
    t77 = 1/t76;
    t80 = t55*t3;
    t82 = t8*t30;
    t83 = t34*t46;
    t86 = 1/t33/t32;
    t89 = 1/t7/t2;
    t90 = t10*t89;
    t93 = 2.0*t45*t43-2.0*t90*t46;
    t94 = t86*t93;
    t97 = t80*t34-t82*t83-t29*t94/2.0;
    t98 = t9*t30;
    t100 = 1.0+t98*t49;
    t101 = 1/t100;
    y[4] = 2.0*t71*t77+t97*t101;
    t104 = t39*t64;
    y[5] = -t67*t37*t104;
    t106 = u[8];
    t108 = t43*t30;
    t111 = t1*t89;
    t112 = t46*t46;
    t115 = u[6];
    t117 = t106*t3-2.0*t108*t46+2.0*t111*t112-t45*t115;
    t119 = t32*t32;
    t120 = 1/t119;
    t121 = t48*t120;
    y[6] = -t117*t49+t121*t93;
    t123 = t11*t11;
    t125 = 1/t12/t123;
    t126 = t6*t125;
    t127 = t59*t59;
    t128 = t25*t127;
    t132 = t39*t52*t64;
    t133 = t59*t18;
    t134 = t20*t67;
    t139 = t55*t55;
    t140 = u[7];
    t142 = t43*t43;
    t145 = 2.0*t112+2.0*t2*t115+2.0*t139+2.0*t8*t140+2.0*t142+2.0*t1*t106;
    t149 = t17*t6;
    t150 = 1/t149;
    t151 = t150*t13;
    t153 = 1/t25/t24;
    t154 = t151*t153;
    t155 = t16*t16;
    t156 = 1/t155;
    t157 = t19*t156;
    t158 = t67*t67;
    t166 = -t145;
    t170 = 3.0/8.0*t126*t128+t132*t133*t134/2.0-t53*t25*t145/4.0-t154*t157*t158
/2.0-t63*t64*t158*t20/2.0-t65*t66*t166/2.0;
    t174 = 1/t76/t75;
    t175 = t71*t174;
    t176 = 1/t123;
    t177 = t17*t176;
    t178 = t24*t59;
    t180 = t72*t18;
    t183 = t177*t178+2.0*t180*t134;
    t185 = t140*t3;
    t187 = t55*t30;
    t191 = t8*t89;
    t192 = t34*t112;
    t195 = t86*t46;
    t196 = t195*t93;
    t198 = t34*t115;
    t201 = 1/t33/t119;
    t202 = t93*t93;
    t203 = t201*t202;
    t208 = t43*t46;
    t213 = t7*t7;
    t214 = 1/t213;
    t215 = t10*t214;
    t220 = 2.0*t142*t30-8.0*t111*t208+2.0*t45*t106+6.0*t215*t112-2.0*t90*t115;
    t221 = t86*t220;
    t224 = t185*t34-2.0*t187*t83-t80*t94+2.0*t191*t192+t82*t196-t82*t198+3.0/
4.0*t29*t203-t29*t221/2.0;
    t226 = t100*t100;
    t227 = 1/t226;
    t228 = t97*t227;
    t229 = t49*t55;
    t232 = t9*t89;
    t233 = t49*t46;
    t236 = t120*t93;
    t238 = 2.0*t82*t229-2.0*t232*t233-t98*t236;
    y[7] = 2.0*t170*t77-t175*t183+t224*t101-t228*t238;
    t242 = t16*t15;
    t243 = 1/t242;
    t245 = t150*t153;
    t246 = t245*t18;
    y[8] = -t166*t37*t104-t158*t243*t246;
    t248 = u[11];
    t250 = t106*t30;
    t253 = t43*t89;
    t258 = t1*t214;
    t259 = t112*t46;
    t262 = t46*t115;
    t265 = u[9];
    t267 = t248*t3-3.0*t250*t46+6.0*t253*t112-3.0*t108*t115-6.0*t258*t259+6.0*
t111*t262-t45*t265;
    t269 = t117*t120;
    t272 = t119*t32;
    t273 = 1/t272;
    t274 = t48*t273;
    y[9] = -t267*t49+2.0*t269*t93-2.0*t274*t202+t121*t220;
    t278 = t123*t11;
    t280 = 1/t12/t278;
    t281 = t6*t280;
    t282 = t127*t59;
    t286 = t39*t125;
    t287 = t286*t64;
    t288 = t127*t18;
    t295 = t150*t52;
    t296 = t295*t153;
    t298 = t156*t158;
    t302 = t145*t18;
    t303 = t302*t134;
    t310 = t20*t166;
    t319 = u[10];
    t326 = 6.0*t262+2.0*t2*t265+6.0*t55*t140+2.0*t8*t319+6.0*t43*t106+2.0*t1*
t248;
    t330 = t17*t17;
    t332 = 1/t330/t6;
    t334 = t24*t24;
    t336 = 1/t25/t334;
    t337 = t332*t13*t336;
    t338 = t19*t18;
    t340 = 1/t155/t16;
    t341 = t338*t340;
    t342 = t158*t67;
    t346 = t18*t156;
    t351 = t157*t67*t166;
    t357 = -t326;
    t361 = -15.0/16.0*t281*t25*t282-9.0/8.0*t287*t288*t134+9.0/8.0*t126*t60*
t145+3.0/4.0*t296*t59*t19*t298+3.0/4.0*t132*t303+3.0/4.0*t132*t59*t158*t20+3.0/
4.0*t132*t133*t310-t53*t25*t326/4.0-3.0/2.0*t337*t341*t342-3.0/2.0*t154*t346*
t342-3.0/2.0*t154*t351-3.0/2.0*t65*t134*t166-t65*t66*t357/2.0;
    t364 = t170*t174;
    t367 = t75*t75;
    t369 = 1/t76/t367;
    t370 = t71*t369;
    t371 = t183*t183;
    t374 = 1/t278;
    t375 = t17*t374;
    t379 = t176*t18;
    t390 = -2.0*t375*t24*t127-4.0*t379*t134*t59+t177*t24*t145+2.0*t72*t158*t20+
2.0*t180*t310;
    t393 = t201*t46*t202;
    t396 = t319*t3;
    t398 = t140*t30;
    t407 = t34*t265;
    t411 = t142*t89;
    t417 = t106*t46;
    t426 = 1/t213/t2;
    t427 = t10*t426;
    t434 = 6.0*t108*t106-12.0*t411*t46+36.0*t258*t43*t112-12.0*t111*t417-12.0*
t111*t43*t115+2.0*t45*t248-24.0*t427*t259+18.0*t215*t262-2.0*t90*t265;
    t435 = t86*t434;
    t438 = t55*t89;
    t445 = t8*t214;
    t446 = t34*t259;
    t449 = t86*t112;
    t450 = t449*t93;
    t453 = t83*t115;
    t456 = t86*t115;
    t457 = t456*t93;
    t460 = t195*t220;
    t464 = 1/t33/t272;
    t465 = t202*t93;
    t466 = t464*t465;
    t470 = t201*t93*t220;
    t473 = -9.0/4.0*t82*t393+t396*t34-3.0*t398*t83-3.0/2.0*t185*t94-3.0*t187*
t198-3.0/2.0*t80*t221-t82*t407-t29*t435/2.0+6.0*t438*t192+3.0*t187*t196+9.0/4.0
*t80*t203-6.0*t445*t446-3.0*t191*t450+6.0*t191*t453+3.0/2.0*t82*t457+3.0/2.0*
t82*t460-15.0/8.0*t29*t466+9.0/4.0*t29*t470;
    t475 = t224*t227;
    t479 = 1/t226/t100;
    t480 = t97*t479;
    t481 = t238*t238;
    t484 = t139*t30;
    t490 = t120*t55;
    t494 = t49*t140;
    t497 = t9*t214;
    t501 = t120*t46;
    t513 = 2.0*t484*t49-8.0*t191*t229*t46-4.0*t82*t490*t93+2.0*t82*t494+6.0*
t497*t49*t112+4.0*t232*t501*t93-2.0*t232*t49*t115+2.0*t98*t273*t202-t98*t120*
t220;
    y[10] = 2.0*t361*t77-2.0*t364*t183+3.0/2.0*t370*t371-t175*t390+t473*t101
-2.0*t475*t238+2.0*t480*t481-t228*t513;
    t517 = t166*t243;
    t520 = t153*t18*t67;
    t524 = 1/t155/t15;
    t526 = t332*t336;
    y[11] = -t357*t37*t104-3.0*t517*t150*t520-3.0*t342*t524*t526*t19-t342*t243*
t245;
    t532 = u[14];
    t549 = t1*t426;
    t550 = t112*t112;
    t553 = t112*t115;
    t556 = t115*t115;
    t559 = t46*t265;
    t562 = u[12];
    t564 = t532*t3-4.0*t248*t30*t46+12.0*t106*t89*t112-6.0*t250*t115-24.0*t43*
t214*t259+24.0*t253*t262-4.0*t108*t265+24.0*t549*t550-36.0*t258*t553+6.0*t111*
t556+8.0*t111*t559-t45*t562;
    t574 = t119*t119;
    t575 = 1/t574;
    y[12] = -t564*t49+3.0*t267*t120*t93-6.0*t117*t273*t202+3.0*t269*t220+6.0*
t48*t575*t465-6.0*t274*t93*t220+t121*t434;
    t604 = t20*t357;
    t607 = t158*t166;
    t641 = 3.0/2.0*t296*t145*t19*t298+3.0*t332*t52*t336*t59*t338*t340*t342+t132
*t326*t18*t134+3.0/2.0*t132*t302*t310+3.0*t132*t59*t67*t310+t132*t133*t604-9.0*
t337*t341*t607-9.0*t154*t346*t607-2.0*t154*t157*t67*t357+3.0*t296*t133*t156*
t342-9.0/4.0*t150*t125*t153*t127*t19*t298-9.0/4.0*t287*t288*t310+15.0/4.0*t39*
t280*t64*t282*t18*t134-9.0/4.0*t287*t127*t158*t20;
    t643 = 1/t330/t149;
    t647 = 1/t25/t334/t24;
    t649 = t19*t19;
    t650 = t155*t155;
    t653 = t158*t158;
    t664 = t140*t140;
    t668 = u[13];
    t671 = t106*t106;
    t677 = -6.0*t556-8.0*t559-2.0*t2*t562-6.0*t664-8.0*t55*t319-2.0*t8*t668-6.0
*t671-8.0*t43*t248-2.0*t1*t532;
    t689 = t166*t166;
    t697 = t123*t123;
    t701 = t127*t127;
    t705 = t145*t145;
    t731 = -15.0/2.0*t643*t13*t647*t649/t650*t653-2.0*t65*t134*t357-t65*t66*
t677/2.0+3.0/2.0*t132*t145*t158*t20-9.0*t337*t19*t340*t653-3.0/2.0*t154*t157*
t689+t53*t25*t677/4.0+105.0/32.0*t6/t12/t697*t25*t701+9.0/8.0*t126*t25*t705-9.0
/2.0*t286*t64*t59*t303+3.0*t295*t153*t59*t351-45.0/8.0*t281*t128*t145+3.0/2.0*
t126*t60*t326-3.0/2.0*t151*t153*t653*t156-3.0/2.0*t63*t64*t689*t20;
    t843 = 6.0*t671*t30+8.0*t108*t248-24.0*t411*t115+2.0*t45*t532-2.0*t90*t562
-48.0*t253*t417+72.0*t142*t214*t112+72.0*t258*t106*t112-16.0*t111*t248*t46-24.0
*t111*t106*t115-16.0*t111*t43*t265+120.0*t10/t213/t7*t550-144.0*t427*t553+18.0*
t215*t556+24.0*t215*t559-192.0*t549*t43*t259+144.0*t258*t208*t115;
    t866 = t202*t202;
    t870 = t220*t220;
    t882 = -4.0*t319*t30*t83-2.0*t396*t94-6.0*t398*t198-3.0*t185*t221-4.0*t187*
t407-2.0*t80*t435-t82*t34*t562-t29*t86*t843/2.0+12.0*t140*t89*t192+9.0/2.0*t185
*t203-24.0*t55*t214*t446-15.0/2.0*t80*t466+24.0*t8*t426*t34*t550+6.0*t191*t34*
t556+105.0/16.0*t29/t33/t574*t866+9.0/4.0*t29*t201*t870+t668*t3*t34-9.0*t187*
t393-9.0/2.0*t82*t201*t115*t202;
    t935 = t46*t93;
    t943 = 6.0*t398*t196+24.0*t438*t453+6.0*t187*t457+6.0*t187*t460+9.0*t80*
t470+8.0*t191*t407*t46+2.0*t82*t86*t265*t93+2.0*t82*t435*t46+3.0*t29*t201*t434*
t93-12.0*t438*t450+12.0*t445*t86*t259*t93-36.0*t445*t192*t115-6.0*t191*t449*
t220+3.0*t82*t456*t220-45.0/4.0*t29*t464*t202*t220+9.0*t191*t201*t112*t202+15.0
/2.0*t82*t464*t46*t465-9.0*t82*t201*t935*t220-12.0*t191*t86*t935*t115;
    t954 = t226*t226;
    t1033 = -24.0*t9*t426*t49*t259-18.0*t497*t120*t112*t93+18.0*t497*t233*t115+
6.0*t232*t120*t115*t93+6.0*t232*t501*t220-6.0*t98*t575*t465+6.0*t98*t273*t93*
t220+36.0*t445*t229*t112+24.0*t191*t120*t55*t46*t93+12.0*t82*t273*t55*t202-12.0
*t232*t273*t46*t202;
    y[13] = 2.0*(t641+t731)*t77-3.0*t361*t174*t183+9.0/2.0*t170*t369*t371-3.0*
t364*t390-15.0/4.0*t71/t76/t367/t75*t371*t183+9.0/2.0*t370*t183*t390-t175*(6.0*
t17/t697*t24*t282+12.0*t374*t18*t134*t127-6.0*t375*t178*t145-6.0*t176*t158*t20*
t59-6.0*t379*t310*t59-6.0*t379*t134*t145+t177*t24*t326+6.0*t72*t67*t310+2.0*
t180*t604)+(t882+t943)*t101-3.0*t473*t227*t238+6.0*t224*t479*t481-3.0*t475*t513
-6.0*t97/t954*t481*t238+6.0*t480*t238*t513-t228*(6.0*t187*t494-12.0*t139*t89*
t233-6.0*t484*t236+2.0*t82*t49*t319-2.0*t232*t49*t265-t98*t120*t434-12.0*t191*
t494*t46-12.0*t191*t229*t115-6.0*t82*t120*t140*t93-6.0*t82*t490*t220+t1033);
    y[14] = -t677*t37*t104-4.0*t357*t243*t150*t520-18.0*t166*t524*t332*t336*t19
*t158-6.0*t517*t245*t158-3.0*t689*t243*t246-15.0*t653/t155/t242*t643*t647*t338
-9.0*t653*t524*t526*t18;
    return;
  }
}

static void mdlOutputs(SimStruct *S,int_T tid)
{
  real_T *y = ssGetOutputPortRealSignal(S,0);
  real_T *x = ssGetContStates(S);
  real_T *p = mxGetPr(ssGetSFcnParam(S,0));
  int_T i;
  InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);
  real_T u[INPUTPORT0WIDTH];

  for(i=0; i<INPUTPORT0WIDTH; i++) {
    u[i]=*uPtrs[i];
  }
  calcOutputs(y,u,p);
}

#define MDL_DERIVATIVES
static void calcDerivatives(real_T *dx,real_T *x)
{
  {
    dx[0] = x[0];
    return;
  }
}

static void mdlDerivatives(SimStruct *S)
{
  real_T *dx = ssGetdX(S);
  real_T *x  = ssGetContStates(S);
  real_T *p  = mxGetPr(ssGetSFcnParam(S,0));
  InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);
  real_T u[INPUTPORT0WIDTH];
  int_T i;

  for(i=0; i<INPUTPORT0WIDTH; i++) {
    u[i]=*uPtrs[i];
  }
  calcDerivatives(dx,x);
}

static void mdlTerminate(SimStruct *S)
{
}

#ifdef MATLAB_MEX_FILE   /* Compile as a MEX-file? */
  #include "simulink.c"  /* MEX-file interface mechanism */
#else
  #include "cg_sfun.h"   /* Code generation registration */
#endif
