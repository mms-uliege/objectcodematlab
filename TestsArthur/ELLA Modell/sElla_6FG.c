/*
 * This S-function was generated with an automatic
 * code-generator written in Maple 6-10
 *
 * (c) by Reinhard Gahleitner, Hubert Gattringer, Renhui Li, Wolfgang H�barth
 * Institute of Automatic Control and Control System Technology, Institute for Robotics
 * Johannes Kepler University Linz, Austria
 * Version 2 November 2008
 */

#define S_FUNCTION_NAME sElla_6FG
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <math.h>

#define INPUTPORT0WIDTH 3

void gaussj(real_T a[6][6],  real_T b[6]);

static void mdlInitializeSizes(SimStruct *S)
{
  ssSetNumSFcnParams(S,2);
  if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
  {
    return; /* Parameter mismatch will be reported by Simulink */
  }
  ssSetNumContStates(S,12);
  ssSetNumDiscStates(S,0);

  if (!ssSetNumInputPorts(S,1)) return;
  ssSetInputPortWidth(S,0,3);
  ssSetInputPortDirectFeedThrough(S,0,0);

  if (!ssSetNumOutputPorts(S,1)) return;
  ssSetOutputPortWidth(S,0,27);

  ssSetNumSampleTimes(S,1);
  ssSetNumRWork(S,0);
  ssSetNumIWork(S,0);
  ssSetNumPWork(S,0);
  ssSetNumModes(S,0);
  ssSetNumNonsampledZCs(S,0);
  ssSetOptions(S,0);
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
  ssSetSampleTime(S,0,CONTINUOUS_SAMPLE_TIME);
  ssSetOffsetTime(S,0,0.0);
}

#define MDL_INITIALIZE_CONDITIONS
static void mdlInitializeConditions(SimStruct *S)
{
  real_T *x = ssGetContStates(S);
  int_T n = ssGetNumContStates(S);
  real_T *p = mxGetPr(ssGetSFcnParam(S,1));
  int_T i;

  for(i=0;i<n;i++)
  {
    x[i]=p[i];
  }
}

static void calcOutputs(real_T *y,real_T *x,real_T *p)
{
  real_T t1;
  real_T t10;
  real_T t101;
  real_T t102;
  real_T t11;
  real_T t110;
  real_T t111;
  real_T t117;
  real_T t118;
  real_T t119;
  real_T t120;
  real_T t122;
  real_T t124;
  real_T t125;
  real_T t126;
  real_T t128;
  real_T t13;
  real_T t130;
  real_T t131;
  real_T t135;
  real_T t137;
  real_T t138;
  real_T t14;
  real_T t140;
  real_T t144;
  real_T t147;
  real_T t150;
  real_T t153;
  real_T t155;
  real_T t163;
  real_T t170;
  real_T t173;
  real_T t177;
  real_T t180;
  real_T t181;
  real_T t185;
  real_T t189;
  real_T t193;
  real_T t196;
  real_T t197;
  real_T t2;
  real_T t20;
  real_T t200;
  real_T t201;
  real_T t204;
  real_T t205;
  real_T t206;
  real_T t207;
  real_T t209;
  real_T t211;
  real_T t213;
  real_T t214;
  real_T t217;
  real_T t22;
  real_T t221;
  real_T t225;
  real_T t23;
  real_T t234;
  real_T t235;
  real_T t236;
  real_T t237;
  real_T t239;
  real_T t24;
  real_T t240;
  real_T t241;
  real_T t245;
  real_T t25;
  real_T t257;
  real_T t258;
  real_T t259;
  real_T t26;
  real_T t263;
  real_T t267;
  real_T t27;
  real_T t271;
  real_T t278;
  real_T t28;
  real_T t29;
  real_T t292;
  real_T t294;
  real_T t295;
  real_T t296;
  real_T t297;
  real_T t3;
  real_T t30;
  real_T t302;
  real_T t307;
  real_T t308;
  real_T t31;
  real_T t313;
  real_T t32;
  real_T t33;
  real_T t333;
  real_T t342;
  real_T t345;
  real_T t349;
  real_T t35;
  real_T t353;
  real_T t354;
  real_T t356;
  real_T t358;
  real_T t36;
  real_T t365;
  real_T t368;
  real_T t37;
  real_T t371;
  real_T t372;
  real_T t373;
  real_T t376;
  real_T t378;
  real_T t38;
  real_T t380;
  real_T t382;
  real_T t385;
  real_T t389;
  real_T t391;
  real_T t395;
  real_T t4;
  real_T t410;
  real_T t413;
  real_T t415;
  real_T t416;
  real_T t419;
  real_T t421;
  real_T t423;
  real_T t425;
  real_T t427;
  real_T t428;
  real_T t43;
  real_T t431;
  real_T t434;
  real_T t436;
  real_T t437;
  real_T t438;
  real_T t44;
  real_T t440;
  real_T t442;
  real_T t445;
  real_T t46;
  real_T t460;
  real_T t463;
  real_T t47;
  real_T t48;
  real_T t49;
  real_T t51;
  real_T t56;
  real_T t6;
  real_T t60;
  real_T t64;
  real_T t66;
  real_T t70;
  real_T t8;
  real_T t95;
  real_T t97;
  {
    y[0] = x[0];
    y[1] = x[1];
    y[2] = x[2];
    y[3] = x[3];
    y[4] = x[4];
    y[5] = x[5];
    y[6] = x[6];
    y[7] = x[7];
    y[8] = x[8];
    y[9] = x[9];
    y[10] = x[10];
    y[11] = x[11];
    t1 = cos(y[3]);
    t2 = y[4]+y[5];
    t3 = cos(t2);
    t4 = cos(y[5]);
    t6 = p[21];
    t8 = p[24];
    t10 = sin(t2);
    t11 = sin(y[5]);
    t13 = t10*t11*t6;
    t14 = t3*t4*t6+t3*t8+t13;
    y[12] = t1*t14;
    y[13] = t10*t4*t6+t10*t8-t3*t11*t6;
    t20 = sin(y[3]);
    y[14] = -t20*t14;
    t22 = sin(y[4]);
    y[15] = t22*y[9];
    t23 = cos(y[4]);
    y[16] = t23*y[9];
    y[17] = y[10];
    y[18] = y[9]*t10;
    y[19] = y[9]*t3;
    y[20] = y[17]+y[11];
    t24 = p[22];
    t25 = t24*t24;
    t26 = p[34];
    t27 = t25*t26;
    t28 = p[23];
    t29 = t28*t28;
    t30 = p[38];
    t31 = t29*t30;
    t32 = p[37];
    t33 = p[41];
    t35 = t6*t6;
    t36 = t26*t35;
    t37 = t4*t4;
    t38 = t36*t37;
    t43 = p[30];
    t44 = t35*t43;
    t46 = p[20];
    t47 = t46*t46;
    t48 = p[26];
    t49 = t47*t48;
    t51 = t30*t35;
    t56 = t26*t26;
    t60 = t30*t30;
    t64 = p[33];
    t66 = p[29];
    t70 = 2.0*t38*t24*t30*t28-t27*t44-t27*t49-t27*t51-t31*t44-t31*t49-t31*t36+
t56*t37*t35*t25+t60*t37*t35*t29-t32*t64-t32*t66-t33*t64-t33*t66;
    t95 = -t27*t64-t27*t66-t25*t56*t35-t31*t64-t31*t66-t29*t60*t35-t32*t35*t43-
t32*t47*t48-t32*t26*t35-t32*t30*t35-t33*t35*t43-t33*t47*t48-t33*t26*t35-t33*t30
*t35;
    t97 = 1/(t70+t95);
    t101 = t11*t6;
    t102 = t101*y[17];
    t110 = y[11]*y[11];
    t111 = t110*t6;
    t117 = y[9]*y[9];
    t118 = t26*t117;
    t119 = t24*t6;
    t120 = 2.0*y[4];
    t122 = sin(t120+y[5]);
    t124 = t118*t119*t122;
    t125 = t30*t117;
    t126 = t28*t6;
    t128 = t125*t126*t122;
    t130 = p[0];
    t131 = t23*t130;
    t135 = t26*t3;
    t137 = t135*t130*t24;
    t138 = t30*t3;
    t140 = t138*t130*t28;
    t144 = sin(2.0*t2);
    t147 = t118*t25*t144/2.0;
    t150 = t125*t29*t144/2.0;
    t153 = 2.0*t26*y[11]*t24*t102+2.0*t30*y[11]*t28*t102+t26*t11*t111*t24+t30*
t11*t111*t28-t124-t128-t46*t48*t131-t6*t43*t131-t137-t140-t6*t26*t131-t147-t150
-t6*t30*t131;
    t155 = sin(t120);
    t163 = t35*t155;
    t170 = p[18];
    t173 = p[31];
    t177 = p[36];
    t180 = t117*t177*t144/2.0;
    t181 = p[27];
    t185 = p[32];
    t189 = p[28];
    t193 = p[35];
    t196 = t117*t193*t144/2.0;
    t197 = p[39];
    t200 = t117*t197*t144/2.0;
    t201 = p[40];
    t204 = t117*t201*t144/2.0;
    t205 = -t117*t47*t48*t155/2.0-t117*t35*t43*t155/2.0-t118*t163/2.0-t125*t163
/2.0-p[43]*y[17]-t170*y[4]+t170*y[1]+t117*t173*t155/2.0-t180+t117*t181*t155/2.0
-t117*t185*t155/2.0-t117*t189*t155/2.0+t196+t200-t204;
    t206 = t153+t205;
    t207 = (t27+t31+t32+t33)*t97*t206;
    t209 = t26*t4*t119;
    t211 = t30*t4*t126;
    t213 = (t209+t27+t211+t31+t32+t33)*t97;
    t214 = y[17]*y[17];
    t217 = t24*t11*t6;
    t221 = t28*t11*t6;
    t225 = p[19];
    t234 = -t26*t214*t217-t30*t214*t221-t137-t140-p[44]*y[11]-t204-t225*y[5]+
t225*y[2]-t147-t150-t124/2.0-t118*t217/2.0-t128/2.0-t125*t221/2.0+t196-t180+
t200;
    t235 = t213*t234;
    t236 = -t207+t235;
    t237 = p[46];
    t239 = t22*y[17];
    t240 = p[47];
    t241 = y[9]*t240;
    t245 = t35*t10*t11;
    t257 = t3*t3;
    t258 = t26*t257;
    t259 = t4*t6;
    t263 = t30*t257;
    t267 = t23*t23;
    t271 = 2.0*t135*t4*t245+2.0*t135*t24*t13+2.0*t138*t4*t245+2.0*t138*t28*t13+
t173+t181+t193+t197+2.0*t258*t259*t24+2.0*t263*t259*t28+t36+t51+t267*t189+t257*
t177+t257*t201;
    t278 = t37*t35;
    t292 = t267*t185-t173*t267-t181*t267-t193*t257-t197*t257+p[25]+2.0*t258*
t278+2.0*t263*t278+t263*t29+t267*t35*t43-t38-t51*t37+t267*t47*t48+t258*t25-t36*
t257-t51*t257;
    t294 = 1/(t271+t292);
    t295 = t23*t294;
    t296 = y[9]*t26;
    t297 = t296*t6;
    t302 = y[11]*t24;
    t307 = y[9]*t30;
    t308 = t307*t6;
    t313 = y[11]*t28;
    t333 = t35*y[17]*t155;
    t342 = 2.0*t297*y[17]*t24*t122+t297*t302*t122+t297*t302*t11+2.0*t308*y[17]*
t28*t122+t308*t313*t122+t308*t313*t11+t296*t25*y[17]*t144+t307*t29*y[17]*t144+y
[9]*t47*t48*y[17]*t155+y[9]*t35*t43*y[17]*t155+t296*t333+t296*t25*y[11]*t144+
t307*t333+t307*t29*y[11]*t144;
    t345 = p[17];
    t349 = y[17]*t155;
    t353 = y[9]*t193;
    t354 = y[17]*t144;
    t356 = y[11]*t144;
    t358 = y[9]*t197;
    t365 = y[9]*t177;
    t368 = y[9]*t201;
    t371 = -p[42]*y[9]-t345*y[3]+t345*y[0]-y[9]*t173*t349-y[9]*t181*t349-t353*
t354-t353*t356-t358*t354-t358*t356+y[9]*t185*t349+y[9]*t189*t349+t365*t354+t365
*t356+t368*t354+t368*t356;
    t372 = t342+t371;
    t373 = t372*t240;
    t376 = t6+p[45];
    t378 = t22*y[9];
    t380 = y[17]*t376-t378*t240;
    t382 = t23*y[9];
    t385 = -t382*t376+t378*t237;
    y[21] = -t236*t237-t239*t241+t295*t373-y[17]*t380+t382*t385+t22*t130;
    t389 = t23*y[17];
    t391 = t22*t294;
    t395 = -y[17]*t237+t382*t240;
    y[22] = t236*t376-t389*t241-t391*t373+y[17]*t395-t378*t385+t131;
    y[23] = t239*y[9]*t376-t295*t372*t376+t389*y[9]*t237+t391*t372*t237-t382*
t395+t378*t380;
    t410 = t213*t206;
    t413 = t44+t49+t64+t66+t36+2.0*t209+t27+t51+2.0*t211+t31+t32+t33;
    t415 = t97*t413*t234;
    t416 = t410-t415;
    t419 = t207-t235-t410+t415;
    t421 = -t101+p[49];
    t423 = -y[20];
    t425 = y[11]*t6;
    t427 = t294*t372;
    t428 = p[50];
    t431 = y[20]*t428;
    t434 = t4*y[11]*t6;
    t436 = t259+t8+p[48];
    t437 = y[20]*t436;
    t438 = y[9]*t10;
    t440 = -t434+t437-t438*t428;
    t442 = y[9]*t3;
    t445 = -t442*t436+t438*t421;
    y[24] = -t4*t110*t6-t11*t416*t6+t419*t421-t423*t4*t425+t427*t3*t428-y[18]*
t431+t423*t440+t442*t445+t10*t130;
    t460 = t11*y[11]*t6;
    t463 = -t460+t423*t421+t442*t428;
    y[25] = t11*t110*t6-t4*t416*t6-t419*t436-y[20]*t11*t425-t427*t10*t428-y[19]
*t431+y[20]*t463-t438*t445+t3*t130;
    y[26] = -t427*t3*t436+y[18]*t437+y[19]*t460+t427*t10*t421+y[19]*y[20]*t421-
y[18]*t434-t442*t463+t438*t440;
    return;
  }
}

static void mdlOutputs(SimStruct *S,int_T tid)
{
  real_T *y = ssGetOutputPortRealSignal(S,0);
  real_T *x = ssGetContStates(S);
  int_T n = ssGetNumContStates(S);
  real_T *p  = mxGetPr(ssGetSFcnParam(S,0));

  calcOutputs(y,x,p);
}

#define MDL_DERIVATIVES
static void calc_M_h(real_T *dx,real_T *x,real_T *u,real_T *p,real_T MM_sub[6][6],
real_T hh_sub[6])
{
  real_T t1;
  real_T t10;
  real_T t103;
  real_T t105;
  real_T t108;
  real_T t11;
  real_T t110;
  real_T t112;
  real_T t118;
  real_T t12;
  real_T t120;
  real_T t126;
  real_T t128;
  real_T t13;
  real_T t131;
  real_T t132;
  real_T t134;
  real_T t14;
  real_T t140;
  real_T t142;
  real_T t148;
  real_T t150;
  real_T t153;
  real_T t154;
  real_T t156;
  real_T t157;
  real_T t159;
  real_T t16;
  real_T t160;
  real_T t161;
  real_T t162;
  real_T t166;
  real_T t168;
  real_T t169;
  real_T t171;
  real_T t172;
  real_T t174;
  real_T t18;
  real_T t181;
  real_T t184;
  real_T t187;
  real_T t19;
  real_T t191;
  real_T t195;
  real_T t2;
  real_T t205;
  real_T t21;
  real_T t214;
  real_T t217;
  real_T t221;
  real_T t226;
  real_T t23;
  real_T t231;
  real_T t238;
  real_T t239;
  real_T t24;
  real_T t245;
  real_T t248;
  real_T t25;
  real_T t251;
  real_T t254;
  real_T t262;
  real_T t263;
  real_T t269;
  real_T t27;
  real_T t272;
  real_T t275;
  real_T t278;
  real_T t279;
  real_T t282;
  real_T t283;
  real_T t29;
  real_T t294;
  real_T t300;
  real_T t301;
  real_T t309;
  real_T t31;
  real_T t312;
  real_T t319;
  real_T t328;
  real_T t329;
  real_T t33;
  real_T t332;
  real_T t336;
  real_T t34;
  real_T t35;
  real_T t36;
  real_T t37;
  real_T t38;
  real_T t39;
  real_T t4;
  real_T t41;
  real_T t42;
  real_T t44;
  real_T t46;
  real_T t47;
  real_T t49;
  real_T t5;
  real_T t50;
  real_T t53;
  real_T t54;
  real_T t56;
  real_T t57;
  real_T t59;
  real_T t60;
  real_T t62;
  real_T t64;
  real_T t7;
  real_T t71;
  real_T t72;
  real_T t74;
  real_T t77;
  real_T t79;
  real_T t8;
  real_T t80;
  real_T t85;
  real_T t87;
  real_T t93;
  real_T t94;
  real_T t97;
  {
    t1 = p[1];
    t2 = t1*t1;
    MM_sub[0][0] = t2*p[4];
    MM_sub[1][0] = 0.0;
    MM_sub[2][0] = 0.0;
    MM_sub[3][0] = 0.0;
    MM_sub[4][0] = 0.0;
    MM_sub[5][0] = 0.0;
    MM_sub[0][1] = 0.0;
    t4 = p[2];
    t5 = t4*t4;
    MM_sub[1][1] = t5*p[5];
    MM_sub[2][1] = 0.0;
    MM_sub[3][1] = 0.0;
    MM_sub[4][1] = 0.0;
    MM_sub[5][1] = 0.0;
    MM_sub[0][2] = 0.0;
    MM_sub[1][2] = 0.0;
    t7 = p[3];
    t8 = t7*t7;
    MM_sub[2][2] = t8*p[6];
    MM_sub[3][2] = 0.0;
    MM_sub[4][2] = 0.0;
    MM_sub[5][2] = 0.0;
    MM_sub[0][3] = 0.0;
    MM_sub[1][3] = 0.0;
    MM_sub[2][3] = 0.0;
    t10 = x[4];
    t11 = cos(t10);
    t12 = t11*t11;
    t13 = p[20];
    t14 = t13*t13;
    t16 = p[26];
    t18 = p[21];
    t19 = t18*t18;
    t21 = p[30];
    t23 = sin(t10);
    t24 = t23*t23;
    t25 = p[31];
    t27 = p[32];
    t29 = p[27];
    t31 = p[28];
    t33 = x[5];
    t34 = t10+t33;
    t35 = cos(t34);
    t36 = cos(t33);
    t37 = t36*t18;
    t38 = p[22];
    t39 = t37+t38;
    t41 = sin(t34);
    t42 = sin(t33);
    t44 = t41*t42*t18;
    t46 = pow(-t35*t39-t44,2.0);
    t47 = p[34];
    t49 = p[23];
    t50 = t37+t49;
    t53 = pow(-t35*t50-t44,2.0);
    t54 = p[38];
    t56 = t41*t41;
    t57 = p[35];
    t59 = t35*t35;
    t60 = p[36];
    t62 = p[39];
    t64 = p[40];
    MM_sub[3][3] = t12*t14*t16+t12*t19*t21+t24*t25+t12*t27+t24*t29+t12*t31+t46*
t47+t53*t54+t56*t57+t59*t60+t56*t62+t59*t64+p[25];
    MM_sub[4][3] = 0.0;
    MM_sub[5][3] = 0.0;
    MM_sub[0][4] = 0.0;
    MM_sub[1][4] = 0.0;
    MM_sub[2][4] = 0.0;
    MM_sub[3][4] = 0.0;
    t71 = t42*t42;
    t72 = t71*t19;
    t74 = t39*t39;
    t77 = t50*t50;
    t79 = p[37];
    t80 = p[41];
    MM_sub[4][4] = t19*t21+t14*t16+p[33]+p[29]+t72*t47+t74*t47+t72*t54+t77*t54+
t79+t80;
    MM_sub[5][4] = t38*t47*t39+t49*t54*t50+t79+t80;
    MM_sub[0][5] = 0.0;
    MM_sub[1][5] = 0.0;
    MM_sub[2][5] = 0.0;
    MM_sub[3][5] = 0.0;
    MM_sub[4][5] = MM_sub[5][4];
    t85 = t38*t38;
    t87 = t49*t49;
    MM_sub[5][5] = t85*t47+t87*t54+t79+t80;
    t93 = p[7];
    t94 = x[6];
    t97 = tanh(t93*t94*t1);
    t103 = t2*t2;
    t105 = t94*t94;
    t108 = p[17];
    t110 = t108*x[3];
    t112 = t108*x[0];
    hh_sub[0] = t1*u[0]-t1*p[8]*t97-p[9]*t2*t94-p[10]*t103*t105*t94+t110-t112;
    t118 = x[7];
    t120 = tanh(t93*t4*t118);
    t126 = t5*t5;
    t128 = t118*t118;
    t131 = p[18];
    t132 = t131*t10;
    t134 = t131*x[1];
    hh_sub[1] = t4*u[1]-t4*p[11]*t120-p[12]*t5*t118-p[13]*t126*t128*t118+t132-
t134;
    t140 = x[8];
    t142 = tanh(t93*t7*t140);
    t148 = t8*t8;
    t150 = t140*t140;
    t153 = p[19];
    t154 = t153*t33;
    t156 = t153*x[2];
    hh_sub[2] = t7*u[2]-t7*p[14]*t142-p[15]*t8*t140-p[16]*t148*t150*t140+t154-
t156;
    t157 = x[9];
    t159 = x[10];
    t160 = 2.0*t10;
    t161 = sin(t160);
    t162 = t159*t161;
    t166 = t157*t57;
    t168 = sin(2.0*t34);
    t169 = t159*t168;
    t171 = x[11];
    t172 = t171*t168;
    t174 = t157*t62;
    t181 = t157*t60;
    t184 = t157*t64;
    t187 = t47*t157;
    t191 = t54*t157;
    t195 = -t157*t25*t162-t157*t29*t162-t166*t169-t166*t172-t174*t169-t174*t172
+t157*t27*t162+t157*t31*t162+t181*t169+t181*t172+t184*t169+t184*t172+t187*t85*
t159*t168+t191*t87*t159*t168;
    t205 = t19*t159*t161;
    t214 = t187*t18;
    t217 = sin(t160+t33);
    t221 = t171*t38;
    t226 = t191*t18;
    t231 = t171*t49;
    t238 = t157*t14*t16*t159*t161+t157*t19*t21*t159*t161+t187*t205+t187*t85*
t171*t168+t191*t205+t191*t87*t171*t168+2.0*t214*t159*t38*t217+t214*t221*t217+
t214*t221*t42+2.0*t226*t159*t49*t217+t226*t231*t217+t226*t231*t42-t110+t112-p
[42]*t157;
    hh_sub[3] = t195+t238;
    t239 = t157*t157;
    t245 = t239*t60*t168/2.0;
    t248 = t239*t62*t168/2.0;
    t251 = t239*t57*t168/2.0;
    t254 = t239*t64*t168/2.0;
    t262 = p[0];
    t263 = t11*t262;
    t269 = t47*t35*t262*t38;
    t272 = t54*t35*t262*t49;
    t275 = t47*t239;
    t278 = t275*t85*t168/2.0;
    t279 = t54*t239;
    t282 = t279*t87*t168/2.0;
    t283 = t239*t29*t161/2.0-t245+t248+t251-t254-t239*t31*t161/2.0-t239*t27*
t161/2.0-t13*t16*t263-t18*t21*t263-t269-t272-t18*t47*t263-t278-t282;
    t294 = t19*t161;
    t300 = t171*t171;
    t301 = t300*t18;
    t309 = t275*t38*t18*t217;
    t312 = t279*t49*t18*t217;
    t319 = t42*t18*t159;
    t328 = -t18*t54*t263-t239*t14*t16*t161/2.0-t239*t19*t21*t161/2.0-t275*t294/
2.0-t279*t294/2.0+t47*t42*t301*t38+t54*t42*t301*t49-t309-t312+t239*t25*t161/2.0
+2.0*t47*t171*t38*t319+2.0*t54*t171*t49*t319-t132+t134-p[43]*t159;
    hh_sub[4] = t283+t328;
    t329 = t159*t159;
    t332 = t38*t42*t18;
    t336 = t49*t42*t18;
    hh_sub[5] = -t47*t329*t332-t54*t329*t336-t269-t272-p[44]*t171-t254-t154+
t156-t278-t282-t309/2.0-t275*t332/2.0-t312/2.0-t279*t336/2.0+t251-t245+t248;
    return;
  }
}

static void mdlDerivatives(SimStruct *S)
{
  real_T *dx = ssGetdX(S);
  real_T *x  = ssGetContStates(S);
  real_T *p  = mxGetPr(ssGetSFcnParam(S,0));
  InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);
  real_T u[INPUTPORT0WIDTH];
  int_T i;
  real_T MM_sub[6][6];
  real_T hh_sub[6];

  for(i=0; i<INPUTPORT0WIDTH; i++) { 
  u[i]=*uPtrs[i]; 
  } 

  calc_M_h(dx,x,u,p,MM_sub,hh_sub);
  gaussj(MM_sub,hh_sub);

  for(i=0;i<6;i++)
  {
    dx[i]=x[i+6]; 
    dx[i+6]=hh_sub[i]; 
  }
}

static void mdlTerminate(SimStruct *S)
{
}

#ifdef MATLAB_MEX_FILE   /* Compile as a MEX-file? */
  #include "simulink.c"  /* MEX-file interface mechanism */
#else
  #include "cg_sfun.h"   /* Code generation registration */
#endif
 
 
#define SWAP(a,b) {temp=(a);(a)=(b);(b)=temp;} 
 
static void gaussj(double a[6][6], double b[6]) 
{ 
 int_T indxc[6]; 
 int_T indxr[6]; 
 int_T ipiv[6]; 
 int i,icol,irow,j,k,l,ll; 
 double big,dum,pivinv,temp; 
 
 for (j=0;j<6;j++) ipiv[j]=0; 
 for (i=0;i<6;i++) { 
	big=0.0; 
	for (j=0;j<6;j++) 
	   if (ipiv[j] != 1) 
	for (k=0;k<6;k++) { 
	   if (ipiv[k] == 0) { 
		  if (fabs(a[j][k]) >= big) { 
			 big=fabs(a[j][k]); 
			 irow=j; 
			 icol=k; 
		  } 
	    } else if (ipiv[k] > 1) 
			mexPrintf("S i n g u l � r e    M a t r i x "); 
	} 
	++(ipiv[icol]); 
	if (irow != icol) { 
	   for (l=0;l<6;l++) SWAP(a[irow][l],a[icol][l]) 
	   SWAP(b[irow],b[icol]) 
	} 
	indxr[i]=irow; 
	indxc[i]=icol; 
	if (a[icol][icol] == 0.0) mexPrintf(" S i n g u l � r e   M a t r i x"); 
	pivinv=1.0/a[icol][icol]; 
	a[icol][icol]=1.0; 
	for (l=0;l<6;l++) a[icol][l] *= pivinv; 
	b[icol] *= pivinv; 
	for (ll=0;ll<6;ll++) 
	if (ll != icol) { 
	   dum=a[ll][icol]; 
	   a[ll][icol]=0.0; 
	   for (l=0;l<6;l++) a[ll][l] -= a[icol][l]*dum; 
	   b[ll] -= b[icol]*dum; 
	} 
 } 
 for (l=6-1;l>=0;l--) { 
	if (indxr[l] != indxc[l]) 
	   for (k=0;k<6;k++) 
			SWAP(a[k][indxr[l]],a[k][indxc[l]]); 
 } 
}	 
 
#undef SWAP 
