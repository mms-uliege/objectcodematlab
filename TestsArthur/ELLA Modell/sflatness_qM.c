/*
 * This S-function was generated with an automatic
 * code-generator written with Maple 7.
 *
 * (c) by Reinhard Gahleitner
 * Johannes Kepler University Linz, Austria
 * Department of Automatic Control and Control Systems Technology
 * gahleitner@mechatronik.uni-linz.ac.at
 */

#define S_FUNCTION_NAME sflatness_qM
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <math.h>

/* defines for simulation sizes */
#define NUMSFCNPARAMS    1
#define NUMCONTSTATES    0
#define NUMDISCSTATES    0
#define NUMINPUTPORTS    1
#define INPUTPORT0WIDTH  9
#define FEEDTHROUGHPORT0 1
#define NUMOUTPUTPORTS   1
#define OUTPUTPORT0WIDTH 3

static void mdlInitializeSizes(SimStruct *S)
{
  ssSetNumSFcnParams(S,NUMSFCNPARAMS);
  if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
    return; /* Parameter mismatch will be reported by Simulink */
  }
  ssSetNumContStates(S,NUMCONTSTATES);
  ssSetNumDiscStates(S,NUMDISCSTATES);

  if (!ssSetNumInputPorts(S,NUMINPUTPORTS)) return;
  ssSetInputPortWidth(S,0,INPUTPORT0WIDTH);
  ssSetInputPortDirectFeedThrough(S,0,FEEDTHROUGHPORT0);

  if (!ssSetNumOutputPorts(S,NUMOUTPUTPORTS)) return;
  ssSetOutputPortWidth(S,0,OUTPUTPORT0WIDTH);

  ssSetNumSampleTimes(S,1);
  ssSetNumRWork(S,0);
  ssSetNumIWork(S,0);
  ssSetNumPWork(S,0);
  ssSetNumModes(S,0);
  ssSetNumNonsampledZCs(S,0);
  ssSetOptions(S,0);
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
  ssSetSampleTime(S,0,INHERITED_SAMPLE_TIME);
  ssSetOffsetTime(S,0,0.0);
}

static void calcOutputs(real_T *y,real_T *u,real_T *p)
{
  real_T t1;
  real_T t10;
  real_T t102;
  real_T t103;
  real_T t11;
  real_T t110;
  real_T t111;
  real_T t114;
  real_T t119;
  real_T t12;
  real_T t123;
  real_T t125;
  real_T t126;
  real_T t13;
  real_T t130;
  real_T t131;
  real_T t135;
  real_T t139;
  real_T t14;
  real_T t141;
  real_T t15;
  real_T t156;
  real_T t160;
  real_T t165;
  real_T t166;
  real_T t167;
  real_T t17;
  real_T t170;
  real_T t172;
  real_T t175;
  real_T t176;
  real_T t183;
  real_T t186;
  real_T t191;
  real_T t194;
  real_T t197;
  real_T t20;
  real_T t202;
  real_T t205;
  real_T t207;
  real_T t215;
  real_T t216;
  real_T t217;
  real_T t218;
  real_T t219;
  real_T t220;
  real_T t221;
  real_T t222;
  real_T t223;
  real_T t224;
  real_T t225;
  real_T t229;
  real_T t230;
  real_T t232;
  real_T t233;
  real_T t235;
  real_T t237;
  real_T t241;
  real_T t242;
  real_T t244;
  real_T t25;
  real_T t252;
  real_T t253;
  real_T t254;
  real_T t255;
  real_T t256;
  real_T t257;
  real_T t258;
  real_T t259;
  real_T t26;
  real_T t260;
  real_T t269;
  real_T t27;
  real_T t270;
  real_T t271;
  real_T t273;
  real_T t275;
  real_T t279;
  real_T t284;
  real_T t285;
  real_T t286;
  real_T t287;
  real_T t288;
  real_T t29;
  real_T t290;
  real_T t291;
  real_T t292;
  real_T t295;
  real_T t296;
  real_T t299;
  real_T t3;
  real_T t301;
  real_T t303;
  real_T t304;
  real_T t306;
  real_T t309;
  real_T t311;
  real_T t328;
  real_T t338;
  real_T t343;
  real_T t350;
  real_T t353;
  real_T t355;
  real_T t36;
  real_T t37;
  real_T t38;
  real_T t4;
  real_T t40;
  real_T t43;
  real_T t44;
  real_T t45;
  real_T t46;
  real_T t47;
  real_T t48;
  real_T t5;
  real_T t50;
  real_T t51;
  real_T t53;
  real_T t55;
  real_T t58;
  real_T t6;
  real_T t60;
  real_T t63;
  real_T t64;
  real_T t66;
  real_T t68;
  real_T t69;
  real_T t7;
  real_T t70;
  real_T t73;
  real_T t76;
  real_T t79;
  real_T t82;
  real_T t83;
  real_T t86;
  real_T t89;
  real_T t9;
  real_T t92;
  real_T t93;
  real_T t94;
  real_T t98;
  {
    t1 = p[17];
    t3 = u[1];
    t4 = cos(t3);
    t5 = u[2];
    t6 = t5+t3;
    t7 = sin(t6);
    t9 = p[21];
    t10 = t4*t7*t9;
    t11 = p[34];
    t12 = u[3];
    t13 = t11*t12;
    t14 = u[4];
    t15 = p[22];
    t17 = t13*t14*t15;
    t20 = u[5];
    t25 = p[38];
    t26 = t25*t12;
    t27 = p[23];
    t29 = t26*t14*t27;
    t36 = cos(t6);
    t37 = t36*t36;
    t38 = sin(t5);
    t40 = t37*t38*t9;
    t43 = t36*t4;
    t44 = t43*t38;
    t45 = t9*t9;
    t46 = t45*t11;
    t47 = t12*t14;
    t48 = t46*t47;
    t50 = t45*t25;
    t51 = t50*t47;
    t53 = cos(t5);
    t55 = t53*t4*t7;
    t58 = sin(t3);
    t60 = t36*t58*t9;
    t63 = p[39];
    t64 = t7*t7;
    t66 = u[6];
    t68 = -2.0*t10*t13*t20*t15-2.0*t10*t26*t20*t27+t63*t64*t66-2.0*t10*t17-2.0*
t10*t29+t40*t17-t60*t17+t40*t29-t60*t29+t44*t48+t44*t51-t55*t48-t55*t51;
    t69 = p[31];
    t70 = t58*t58;
    t73 = p[35];
    t76 = p[27];
    t79 = p[40];
    t82 = p[28];
    t83 = t4*t4;
    t86 = p[36];
    t89 = p[32];
    t92 = t36*t7;
    t93 = t92*t11;
    t94 = t15*t15;
    t98 = t12*t20;
    t102 = t92*t25;
    t103 = t27*t27;
    t110 = t4*t58;
    t111 = t110*t45;
    t114 = p[30];
    t119 = -2.0*t111*t114*t12*t14-2.0*t102*t47*t103-2.0*t102*t98*t103-t111*t13*
t14+t79*t37*t66+t86*t37*t66-2.0*t93*t47*t94+t73*t64*t66+t69*t70*t66+t76*t70*t66
+t82*t83*t66+t89*t83*t66-2.0*t93*t98*t94;
    t123 = p[26];
    t125 = p[20];
    t126 = t125*t125;
    t130 = t43*t9;
    t131 = t11*t66;
    t135 = t25*t66;
    t139 = t36*t53;
    t141 = t139*t7*t9;
    t156 = t83*t45;
    t160 = t37*t25*t66*t103+t37*t11*t66*t94-2.0*t110*t123*t47*t126-t111*t26*t14
+t156*t114*t66+2.0*t130*t131*t15+2.0*t130*t135*t27+t1*u[0]+p[42]*t12+t156*t131-
t141*t17-t141*t29+p[25]*t66;
    t165 = t86*t36;
    t166 = t7*t12;
    t167 = t166*t20;
    t170 = t89*t4;
    t172 = t58*t12*t14;
    t175 = t79*t36;
    t176 = t166*t14;
    t183 = t69*t4;
    t186 = t63*t36;
    t191 = t82*t4;
    t194 = t76*t4;
    t197 = t73*t36;
    t202 = t83*t123*t66*t126+t156*t135-2.0*t165*t167-2.0*t165*t176-2.0*t175*
t167+2.0*t186*t167+2.0*t197*t167-2.0*t170*t172+2.0*t183*t172-2.0*t191*t172+2.0*
t194*t172-2.0*t175*t176+2.0*t186*t176+2.0*t197*t176;
    y[0] = 1/t1*(t68+t119+t160+t202);
    t205 = t139*t7;
    t207 = t12*t12;
    t215 = u[7];
    t216 = t25*t215;
    t217 = t216*t103;
    t218 = t11*t215;
    t219 = t218*t94;
    t220 = u[8];
    t221 = t11*t220;
    t222 = t221*t94;
    t223 = t25*t220;
    t224 = t223*t103;
    t225 = t45*t114;
    t229 = t11*t207;
    t230 = t229*t15;
    t232 = t25*t207;
    t233 = t232*t27;
    t235 = t46*t207;
    t237 = t50*t207;
    t241 = -t205*t9*t11*t207*t15-t205*t9*t25*t207*t27-t123*t215*t126-t225*t215+
t40*t230+t40*t233+t44*t235-t55*t235+t44*t237-t55*t237-t217-t219-t222-t224;
    t242 = t38*t9;
    t244 = t14*t20;
    t252 = t10*t230;
    t253 = t10*t233;
    t254 = p[37];
    t255 = t254*t215;
    t256 = t254*t220;
    t257 = p[41];
    t258 = t257*t215;
    t259 = t257*t220;
    t260 = p[18];
    t269 = p[0];
    t270 = t269*t36;
    t271 = t25*t27*t270;
    t273 = t4*t269;
    t275 = 2.0*t242*t11*t244*t15+2.0*t242*t25*t244*t27-t114*t9*t273-p[43]*t14-p
[29]*t215-p[33]*t215-t260*t3-t252-t253-t255-t256-t258-t259-t271;
    t279 = t58*t207;
    t284 = t7*t207;
    t285 = t197*t284;
    t286 = t186*t284;
    t287 = t165*t284;
    t288 = t175*t284;
    t290 = t11*t15*t270;
    t291 = t38*t38;
    t292 = t291*t45;
    t295 = t53*t53;
    t296 = t295*t45;
    t299 = -t123*t125*t273-t170*t279+t183*t279-t191*t279+t194*t279-t292*t216-
t296*t216-t292*t218-t296*t218+t285+t286-t287-t288-t290;
    t301 = t92*t229*t94;
    t303 = t92*t232*t103;
    t304 = t53*t9;
    t306 = t304*t218*t15;
    t309 = t304*t216*t27;
    t311 = t11*t269;
    t328 = t20*t20;
    t338 = t242*t11*t328*t15-t110*t123*t207*t126-t139*t269*t9*t25-t242*t25*t269
*t7+t242*t25*t328*t27-t110*t225*t207-t304*t221*t15-t304*t223*t27-t242*t311*t7-
t304*t311*t36-t301-t303-2.0*t306-2.0*t309;
    y[1] = -(t241+t275+t299+t338)/t260;
    t343 = t14*t14;
    t350 = -t242*t11*t343*t15-t242*t25*t343*t27-t252-t253+t285+t286-t287-t288-
t301-t303-t306-t309;
    t353 = p[19];
    t355 = -p[44]*t20-t353*t5-t217-t219-t222-t224-t255-t256-t258-t259-t271-t290
;
    y[2] = -(t350+t355)/t353;
    return;
  }
}

static void mdlOutputs(SimStruct *S,int_T tid)
{
  real_T *y = ssGetOutputPortRealSignal(S,0);
  real_T *p = mxGetPr(ssGetSFcnParam(S,0));
  int_T i;
  InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);
  real_T u[INPUTPORT0WIDTH];

  for(i=0; i<INPUTPORT0WIDTH; i++) {
    u[i]=*uPtrs[i];
  }
  calcOutputs(y,u,p);
}

static void mdlTerminate(SimStruct *S)
{
}

#ifdef MATLAB_MEX_FILE   /* Compile as a MEX-file? */
  #include "simulink.c"  /* MEX-file interface mechanism */
#else
  #include "cg_sfun.h"   /* Code generation registration */
#endif
