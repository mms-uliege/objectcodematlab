%% Parameter Ella
%============================================================

% clear all;
% close all;
% clc;

Ts=2e-4;     %Abtastzeit Modell
g=9.81;
% g=0;

s=tf('s');
%Tiefpassfilter 150Hz
Gs1=1/(s/(2*pi*150)+1);
Gz1=c2d(Gs1,400e-6,'zoh');

%Tiefpassfilter 100Hz
Gs2=1/(s/(2*pi*100)+1);
Gz2=c2d(Gs2,400e-6,'zoh');

%Tiefpassfilter 30Hz
Gs3=1/(s/(2*pi*30)+1);
Gz3=c2d(Gs3,400e-6,'zoh');

%Tiefpassfilter 15Hz
Gs4=1/(s/(2*pi*15)+1);
Gz4=c2d(Gs4,400e-6,'zoh');

%Diff mit Tiefpassfilter
Gas=s/(s/50+1);
Gaz=c2d(Gas,400e-6,'zoh');

%Diff mit Tiefpassfilter
Gbs=s/(s/100+1);
Gbz=c2d(Gbs,400e-6,'zoh');

%Diff mit Tiefpassfilter
Gdiff=s/(s/150+1);
Gdiffz=c2d(Gdiff,400e-6,'zoh');

flag=1; %0..keine Reibung;; 1..mit Reibung

%% -------------Oberarm------------------------------------

%L�ngenabmessungen
L_B2A=0.06; % Ende 1. starres St�ck
L_B2B=L_B2A+0.7005; % bis zum Ende elastisches St�ck
L_B2C=L_B2B+0.199; % bis Schwerpunkt 2. starres St�ck
L_B2=L_B2C; % gesamte L�nge des Balkens

%% -------------Unterarm-----------------------------------

%L�ngenabmessungen
L_B3A=0.194;
L_B3B=L_B3A+0.62;
L_B3C=L_B3B+0.06;
L_B3 =L_B3C+0.06;

%% ------------- Gesamt -----------------------------------

L_ges=L_B2+L_B3;
params_kin=[L_B2, L_B3];
l2=L_B2;
l3=L_B3;

%% ------------- Params Roboter ---------------------------

epsilon=500; %Steigung tanh der Reibkennlinie

%Motor 1
i_G1=100;
B_M1=(0.33+0.011+1.69)*10^-4; %Tr�gheit: Motor+Bremse+Getriebe
d_11=flag*0.2;
d_12=flag*0.00195;
d_13=flag*(-2.0028e-9);

%Motor 2
i_G2=100;
C_M2=(0.33+0.011+1.69)*10^-4; %Tr�gheit: Motor+Bremse+Getriebe
d_21=flag*0.14;
d_22=flag*0.0022;
d_23=flag*(-3.1806e-9);

%Motor 3
m_M3=3; %Gewicht: Motor+Bremse+Getriebe;
i_G3=160;
C_M3=(0.33+0.011+0.193)*10^-4; %Tr�gheit: Motor+Bremse+Getriebe;
d_31=flag*0.07;
d_32=flag*(4.7699e-4);
d_33=flag*(-5.9251e-10);

%Basis
B_S1=0.3000;

%Balkenmasse 2
sA2=0.475;
m_S2=2.25;
A_S2=5.1800e-4;
B_S2=0.1625;
C_S2=0.1625;

%Endmasse 2
m_E2=3.3;

V_E2=0.15^2*pi/4*0.2; %fiktives Volumen
rho_E2=(m_E2+m_M3)/V_E2; %fiktive Dichte

A_E2=1*(1/4*(m_E2+m_M3)*(0.15/2)^2+1/12*(m_E2+m_M3)*(0.2)^2);
B_E2=1*(1/4*(m_E2+m_M3)*(0.15/2)^2+1/12*(m_E2+m_M3)*(0.2)^2);
C_E2=1/2*(m_E2+m_M3)*(0.15/2)^2;

%Balkenmasse 3
sA3=0.505;
m_S3=1.4;
A_S3=2.18e-4;
B_S3=0.08374; 
C_S3=0.08374;   

%Endmasse 3
sE3=l3-0.06;
m_E3=2.1;

A_E3=2.066e-3;
B_E3=3.3e-3;
C_E3=4.378e-3;

%Steifigkeiten
k1=12000; % 12000   120000
k2=13000; % 13000   120000
k3=7500; % 7500     29000

%D�mpfungen Armkoordinate
d_A1=0.25;
d_A2=0.25;
d_A3=0.1;

%Abst�nde der IMUs
rx_IMU1=-0.01;
ry_IMU1=-0.095;
rz_IMU1=-0.04;

rx_IMU2=-0.075;
ry_IMU2=0.035;
rz_IMU2=0;

%% RedModell 6FG
%=========================================================================

params_6FG=...
       [g,i_G1,i_G2,i_G3,...
        B_M1,C_M2,C_M3,...
        epsilon,...
        d_11,d_12,d_13,...
        d_21,d_22,d_23,...
        d_31,d_32,d_33,...
        k1,k2,k3,...
        sA2,l2,sA3,sE3,l3,...
        B_S1,...
        m_S2,A_S2,B_S2,C_S2,...
        m_E2+m_M3,A_E2,B_E2,C_E2,...
        m_S3,A_S3,B_S3,C_S3,...
        m_E3,A_E3,B_E3,C_E3,...
        d_A1,d_A2,d_A3,...
        rx_IMU1,ry_IMU1,rz_IMU1,...
        rx_IMU2,ry_IMU2,rz_IMU2];
    
%% -------------Startposition -----------------------------

xE=1.4;
yE=-0.5;
zE=-0.75;

% xE=1.5882;
% yE=-0.5;
% zE=0;

rE=sqrt(xE^2+yE^2+zE^2);
q1 = -atan(zE / xE);
alpha=acos((l2^2+l3^2-rE^2)/(2*l2*l3));
q3=-(pi-alpha);
gamma=asin(l3/rE*sin(alpha));
beta=atan(yE/xE*cos(q1));
q2=gamma+beta;

rx=cos(q1)*cos(q2+q3)*(cos(q3)*l2+l3)+cos(q1)*sin(q2+q3)*sin(q3)*l2;
ry=sin(q2+q3)*(cos(q3)*l2+l3)-cos(q2+q3)*sin(q3)*l2;
rz=-sin(q1)*cos(q2+q3)*(cos(q3)*l2+l3)-sin(q1)*sin(q2+q3)*sin(q3)*l2;

%Bahnparameter 3D
xb=0;
yb=1.5;
zb=1.5;

% xb=-0.1882;
% yb=1.5;
% zb=0.75;

%Anfangswerte
ic_6FG=[q1,q2,q3,q1,q2,q3,0,0,0,0,0,0];
