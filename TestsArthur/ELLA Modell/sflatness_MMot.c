/*
 * This S-function was generated with an automatic
 * code-generator written with Maple 7.
 *
 * (c) by Reinhard Gahleitner
 * Johannes Kepler University Linz, Austria
 * Department of Automatic Control and Control Systems Technology
 * gahleitner@mechatronik.uni-linz.ac.at
 */

#define S_FUNCTION_NAME sflatness_MMot
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <math.h>

/* defines for simulation sizes */
#define NUMSFCNPARAMS    1
#define NUMCONTSTATES    0
#define NUMDISCSTATES    0
#define NUMINPUTPORTS    1
#define INPUTPORT0WIDTH  21
#define FEEDTHROUGHPORT0 1
#define NUMOUTPUTPORTS   1
#define OUTPUTPORT0WIDTH 3

static void mdlInitializeSizes(SimStruct *S)
{
  ssSetNumSFcnParams(S,NUMSFCNPARAMS);
  if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
    return; /* Parameter mismatch will be reported by Simulink */
  }
  ssSetNumContStates(S,NUMCONTSTATES);
  ssSetNumDiscStates(S,NUMDISCSTATES);

  if (!ssSetNumInputPorts(S,NUMINPUTPORTS)) return;
  ssSetInputPortWidth(S,0,INPUTPORT0WIDTH);
  ssSetInputPortDirectFeedThrough(S,0,FEEDTHROUGHPORT0);

  if (!ssSetNumOutputPorts(S,NUMOUTPUTPORTS)) return;
  ssSetOutputPortWidth(S,0,OUTPUTPORT0WIDTH);

  ssSetNumSampleTimes(S,1);
  ssSetNumRWork(S,0);
  ssSetNumIWork(S,0);
  ssSetNumPWork(S,0);
  ssSetNumModes(S,0);
  ssSetNumNonsampledZCs(S,0);
  ssSetOptions(S,0);
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
  ssSetSampleTime(S,0,INHERITED_SAMPLE_TIME);
  ssSetOffsetTime(S,0,0.0);
}

static void calcOutputs(real_T *y,real_T *u,real_T *p)
{
  real_T t1;
  real_T t100;
  real_T t1003;
  real_T t1006;
  real_T t101;
  real_T t1010;
  real_T t1014;
  real_T t1018;
  real_T t103;
  real_T t1032;
  real_T t104;
  real_T t1041;
  real_T t105;
  real_T t106;
  real_T t1061;
  real_T t107;
  real_T t1070;
  real_T t1072;
  real_T t1081;
  real_T t1089;
  real_T t109;
  real_T t1093;
  real_T t1097;
  real_T t11;
  real_T t110;
  real_T t1103;
  real_T t1106;
  real_T t111;
  real_T t1111;
  real_T t1114;
  real_T t112;
  real_T t1127;
  real_T t113;
  real_T t1135;
  real_T t116;
  real_T t1161;
  real_T t1168;
  real_T t117;
  real_T t1175;
  real_T t1176;
  real_T t1177;
  real_T t118;
  real_T t1180;
  real_T t1185;
  real_T t119;
  real_T t1190;
  real_T t1193;
  real_T t1196;
  real_T t120;
  real_T t1206;
  real_T t1210;
  real_T t1217;
  real_T t1221;
  real_T t123;
  real_T t1232;
  real_T t1239;
  real_T t124;
  real_T t1246;
  real_T t125;
  real_T t1254;
  real_T t126;
  real_T t1262;
  real_T t1267;
  real_T t1270;
  real_T t1271;
  real_T t1276;
  real_T t128;
  real_T t129;
  real_T t130;
  real_T t1308;
  real_T t1312;
  real_T t133;
  real_T t134;
  real_T t1340;
  real_T t1347;
  real_T t135;
  real_T t136;
  real_T t1375;
  real_T t139;
  real_T t1395;
  real_T t1396;
  real_T t14;
  real_T t140;
  real_T t1403;
  real_T t1411;
  real_T t142;
  real_T t1424;
  real_T t1427;
  real_T t143;
  real_T t1435;
  real_T t1438;
  real_T t1439;
  real_T t144;
  real_T t1441;
  real_T t1442;
  real_T t1445;
  real_T t1446;
  real_T t1447;
  real_T t1459;
  real_T t146;
  real_T t1463;
  real_T t147;
  real_T t1470;
  real_T t1472;
  real_T t1473;
  real_T t1474;
  real_T t1476;
  real_T t1477;
  real_T t1479;
  real_T t1480;
  real_T t1481;
  real_T t1490;
  real_T t1493;
  real_T t1494;
  real_T t1496;
  real_T t15;
  real_T t150;
  real_T t1500;
  real_T t1501;
  real_T t1504;
  real_T t1505;
  real_T t1507;
  real_T t1509;
  real_T t1511;
  real_T t1514;
  real_T t1515;
  real_T t1518;
  real_T t1520;
  real_T t1522;
  real_T t1523;
  real_T t1528;
  real_T t153;
  real_T t1537;
  real_T t154;
  real_T t1540;
  real_T t1545;
  real_T t1547;
  real_T t1548;
  real_T t1552;
  real_T t1556;
  real_T t1560;
  real_T t1563;
  real_T t1567;
  real_T t157;
  real_T t1571;
  real_T t1574;
  real_T t158;
  real_T t1581;
  real_T t1583;
  real_T t1588;
  real_T t1589;
  real_T t1591;
  real_T t1593;
  real_T t1596;
  real_T t16;
  real_T t1602;
  real_T t1603;
  real_T t1604;
  real_T t1605;
  real_T t1606;
  real_T t1607;
  real_T t161;
  real_T t1610;
  real_T t1612;
  real_T t1613;
  real_T t1614;
  real_T t1615;
  real_T t1616;
  real_T t1617;
  real_T t162;
  real_T t1625;
  real_T t1627;
  real_T t1640;
  real_T t1643;
  real_T t1644;
  real_T t1649;
  real_T t165;
  real_T t1650;
  real_T t166;
  real_T t1666;
  real_T t1680;
  real_T t1681;
  real_T t1686;
  real_T t169;
  real_T t1699;
  real_T t17;
  real_T t170;
  real_T t1703;
  real_T t171;
  real_T t1713;
  real_T t172;
  real_T t1722;
  real_T t1726;
  real_T t1729;
  real_T t173;
  real_T t1730;
  real_T t1732;
  real_T t1734;
  real_T t1737;
  real_T t1740;
  real_T t1742;
  real_T t1744;
  real_T t1745;
  real_T t1749;
  real_T t1750;
  real_T t1751;
  real_T t1752;
  real_T t1753;
  real_T t1754;
  real_T t1756;
  real_T t1758;
  real_T t176;
  real_T t1760;
  real_T t1762;
  real_T t1763;
  real_T t1764;
  real_T t1765;
  real_T t1766;
  real_T t1767;
  real_T t1768;
  real_T t1769;
  real_T t1770;
  real_T t1771;
  real_T t1773;
  real_T t1775;
  real_T t1777;
  real_T t1779;
  real_T t1781;
  real_T t1783;
  real_T t1784;
  real_T t1787;
  real_T t1788;
  real_T t1791;
  real_T t1792;
  real_T t1793;
  real_T t1796;
  real_T t1798;
  real_T t18;
  real_T t1802;
  real_T t1805;
  real_T t1807;
  real_T t1810;
  real_T t1811;
  real_T t1812;
  real_T t1813;
  real_T t1814;
  real_T t1815;
  real_T t1817;
  real_T t1818;
  real_T t182;
  real_T t1820;
  real_T t1821;
  real_T t1823;
  real_T t1837;
  real_T t185;
  real_T t1857;
  real_T t186;
  real_T t1864;
  real_T t1868;
  real_T t1869;
  real_T t1871;
  real_T t1873;
  real_T t1876;
  real_T t1887;
  real_T t189;
  real_T t1898;
  real_T t19;
  real_T t190;
  real_T t1900;
  real_T t191;
  real_T t1934;
  real_T t194;
  real_T t1941;
  real_T t1943;
  real_T t1946;
  real_T t1947;
  real_T t1952;
  real_T t1967;
  real_T t1970;
  real_T t198;
  real_T t1983;
  real_T t199;
  real_T t1995;
  real_T t1997;
  real_T t1999;
  real_T t20;
  real_T t200;
  real_T t2001;
  real_T t2004;
  real_T t2007;
  real_T t2011;
  real_T t2012;
  real_T t2016;
  real_T t2019;
  real_T t202;
  real_T t2020;
  real_T t2024;
  real_T t2029;
  real_T t203;
  real_T t2030;
  real_T t2033;
  real_T t2036;
  real_T t2037;
  real_T t2039;
  real_T t204;
  real_T t2043;
  real_T t2044;
  real_T t2047;
  real_T t2048;
  real_T t205;
  real_T t2050;
  real_T t2052;
  real_T t2054;
  real_T t2056;
  real_T t2058;
  real_T t2059;
  real_T t2061;
  real_T t2063;
  real_T t2064;
  real_T t2065;
  real_T t2066;
  real_T t2067;
  real_T t2068;
  real_T t2069;
  real_T t2071;
  real_T t2072;
  real_T t2073;
  real_T t2074;
  real_T t2075;
  real_T t2077;
  real_T t2089;
  real_T t209;
  real_T t21;
  real_T t2103;
  real_T t2106;
  real_T t2109;
  real_T t2111;
  real_T t2114;
  real_T t2120;
  real_T t2127;
  real_T t2128;
  real_T t2129;
  real_T t213;
  real_T t2130;
  real_T t2131;
  real_T t2132;
  real_T t2133;
  real_T t2134;
  real_T t2135;
  real_T t2136;
  real_T t2137;
  real_T t2138;
  real_T t2139;
  real_T t214;
  real_T t2140;
  real_T t2141;
  real_T t2143;
  real_T t2145;
  real_T t2146;
  real_T t2147;
  real_T t2149;
  real_T t215;
  real_T t2151;
  real_T t2152;
  real_T t2159;
  real_T t2161;
  real_T t2165;
  real_T t2166;
  real_T t2183;
  real_T t2186;
  real_T t22;
  real_T t2202;
  real_T t2209;
  real_T t2212;
  real_T t2213;
  real_T t2215;
  real_T t2216;
  real_T t222;
  real_T t2221;
  real_T t2223;
  real_T t2224;
  real_T t2226;
  real_T t2227;
  real_T t2236;
  real_T t224;
  real_T t2243;
  real_T t225;
  real_T t2250;
  real_T t2255;
  real_T t2256;
  real_T t2258;
  real_T t2265;
  real_T t227;
  real_T t228;
  real_T t2280;
  real_T t23;
  real_T t231;
  real_T t24;
  real_T t240;
  real_T t242;
  real_T t245;
  real_T t248;
  real_T t25;
  real_T t251;
  real_T t252;
  real_T t254;
  real_T t258;
  real_T t26;
  real_T t260;
  real_T t261;
  real_T t262;
  real_T t266;
  real_T t267;
  real_T t27;
  real_T t272;
  real_T t273;
  real_T t274;
  real_T t276;
  real_T t278;
  real_T t28;
  real_T t281;
  real_T t283;
  real_T t285;
  real_T t29;
  real_T t292;
  real_T t293;
  real_T t294;
  real_T t30;
  real_T t301;
  real_T t31;
  real_T t311;
  real_T t312;
  real_T t316;
  real_T t321;
  real_T t322;
  real_T t325;
  real_T t339;
  real_T t34;
  real_T t345;
  real_T t346;
  real_T t348;
  real_T t35;
  real_T t352;
  real_T t353;
  real_T t354;
  real_T t36;
  real_T t364;
  real_T t366;
  real_T t367;
  real_T t368;
  real_T t37;
  real_T t370;
  real_T t374;
  real_T t377;
  real_T t379;
  real_T t38;
  real_T t382;
  real_T t383;
  real_T t384;
  real_T t388;
  real_T t391;
  real_T t392;
  real_T t393;
  real_T t398;
  real_T t399;
  real_T t4;
  real_T t400;
  real_T t402;
  real_T t403;
  real_T t404;
  real_T t407;
  real_T t41;
  real_T t414;
  real_T t415;
  real_T t42;
  real_T t423;
  real_T t424;
  real_T t425;
  real_T t428;
  real_T t429;
  real_T t43;
  real_T t433;
  real_T t437;
  real_T t438;
  real_T t44;
  real_T t441;
  real_T t442;
  real_T t446;
  real_T t447;
  real_T t45;
  real_T t450;
  real_T t453;
  real_T t46;
  real_T t460;
  real_T t461;
  real_T t467;
  real_T t470;
  real_T t471;
  real_T t48;
  real_T t480;
  real_T t484;
  real_T t49;
  real_T t490;
  real_T t494;
  real_T t497;
  real_T t498;
  real_T t499;
  real_T t5;
  real_T t50;
  real_T t500;
  real_T t508;
  real_T t509;
  real_T t51;
  real_T t516;
  real_T t517;
  real_T t518;
  real_T t52;
  real_T t521;
  real_T t522;
  real_T t526;
  real_T t528;
  real_T t529;
  real_T t53;
  real_T t532;
  real_T t533;
  real_T t538;
  real_T t543;
  real_T t544;
  real_T t545;
  real_T t548;
  real_T t549;
  real_T t55;
  real_T t550;
  real_T t551;
  real_T t556;
  real_T t558;
  real_T t561;
  real_T t562;
  real_T t564;
  real_T t568;
  real_T t57;
  real_T t572;
  real_T t573;
  real_T t574;
  real_T t578;
  real_T t579;
  real_T t58;
  real_T t580;
  real_T t584;
  real_T t59;
  real_T t595;
  real_T t601;
  real_T t602;
  real_T t604;
  real_T t605;
  real_T t606;
  real_T t614;
  real_T t616;
  real_T t617;
  real_T t62;
  real_T t620;
  real_T t621;
  real_T t622;
  real_T t63;
  real_T t630;
  real_T t633;
  real_T t638;
  real_T t647;
  real_T t650;
  real_T t656;
  real_T t657;
  real_T t658;
  real_T t66;
  real_T t662;
  real_T t67;
  real_T t68;
  real_T t682;
  real_T t683;
  real_T t684;
  real_T t69;
  real_T t694;
  real_T t7;
  real_T t70;
  real_T t71;
  real_T t710;
  real_T t711;
  real_T t713;
  real_T t72;
  real_T t726;
  real_T t727;
  real_T t728;
  real_T t73;
  real_T t731;
  real_T t732;
  real_T t735;
  real_T t736;
  real_T t739;
  real_T t740;
  real_T t743;
  real_T t746;
  real_T t747;
  real_T t748;
  real_T t752;
  real_T t753;
  real_T t757;
  real_T t758;
  real_T t76;
  real_T t762;
  real_T t763;
  real_T t767;
  real_T t77;
  real_T t774;
  real_T t775;
  real_T t778;
  real_T t78;
  real_T t781;
  real_T t784;
  real_T t787;
  real_T t789;
  real_T t791;
  real_T t795;
  real_T t797;
  real_T t799;
  real_T t8;
  real_T t80;
  real_T t801;
  real_T t804;
  real_T t805;
  real_T t808;
  real_T t809;
  real_T t81;
  real_T t810;
  real_T t813;
  real_T t816;
  real_T t819;
  real_T t820;
  real_T t823;
  real_T t855;
  real_T t86;
  real_T t860;
  real_T t863;
  real_T t864;
  real_T t87;
  real_T t88;
  real_T t884;
  real_T t885;
  real_T t889;
  real_T t891;
  real_T t902;
  real_T t905;
  real_T t906;
  real_T t910;
  real_T t915;
  real_T t919;
  real_T t92;
  real_T t921;
  real_T t925;
  real_T t93;
  real_T t930;
  real_T t94;
  real_T t941;
  real_T t949;
  real_T t950;
  real_T t953;
  real_T t954;
  real_T t957;
  real_T t958;
  real_T t96;
  real_T t964;
  real_T t965;
  real_T t97;
  real_T t971;
  real_T t974;
  real_T t976;
  real_T t979;
  real_T t98;
  real_T t982;
  real_T t983;
  real_T t984;
  real_T t987;
  real_T t99;
  real_T t990;
  real_T t991;
  real_T t998;
  real_T t999;
  {
    t1 = p[1];
    t4 = t1*t1;
    t5 = t4*t4;
    t7 = u[18];
    t8 = t7*t7;
    t11 = p[17];
    t14 = u[2];
    t15 = u[1];
    t16 = t14+t15;
    t17 = cos(t16);
    t18 = t17*t17;
    t19 = cos(t14);
    t20 = t18*t19;
    t21 = u[5];
    t22 = p[21];
    t23 = t21*t22;
    t24 = t20*t23;
    t25 = p[38];
    t26 = u[6];
    t27 = t25*t26;
    t28 = u[4];
    t29 = p[23];
    t30 = t28*t29;
    t31 = t27*t30;
    t34 = u[3];
    t35 = t25*t34;
    t36 = u[7];
    t37 = t36*t29;
    t38 = t35*t37;
    t41 = cos(t15);
    t42 = sin(t16);
    t43 = t41*t42;
    t44 = u[8];
    t45 = t36+t44;
    t46 = sin(t14);
    t48 = t43*t45*t46;
    t49 = t22*t22;
    t50 = p[34];
    t51 = t49*t50;
    t52 = t34*t28;
    t53 = t51*t52;
    t55 = t28+t21;
    t57 = t43*t55*t46;
    t58 = t26*t28;
    t59 = t51*t58;
    t62 = t34*t36;
    t63 = t51*t62;
    t66 = sin(t15);
    t67 = t28*t28;
    t68 = t66*t67;
    t69 = t42*t55;
    t70 = t68*t69;
    t71 = t46*t49;
    t72 = t50*t34;
    t73 = t71*t72;
    t76 = t41*t17;
    t77 = t19*t44;
    t78 = t76*t77;
    t80 = t19*t21;
    t81 = t76*t80;
    t86 = t17*t19;
    t87 = t68*t86;
    t88 = t21*t49;
    t92 = t21*t21;
    t93 = t46*t92;
    t94 = t76*t93;
    t96 = t19*t42;
    t97 = t22*t17;
    t98 = t96*t97;
    t99 = u[9];
    t100 = t25*t99;
    t101 = t100*t30;
    t103 = t42*t42;
    t104 = t19*t103;
    t105 = t45*t22;
    t106 = t104*t105;
    t107 = t35*t30;
    t109 = -2.0*t87*t88*t72-t98*t101+t106*t107+2.0*t24*t31+2.0*t24*t38-t48*t53+
t78*t53-t94*t53-2.0*t57*t59-2.0*t57*t63+2.0*t81*t59+2.0*t81*t63+2.0*t70*t73;
    t110 = t55*t22;
    t111 = t76*t110;
    t112 = t21*t29;
    t113 = t27*t112;
    t116 = t66*t28;
    t117 = t42*t22;
    t118 = t116*t117;
    t119 = t44*t29;
    t120 = t35*t119;
    t123 = t20*t105;
    t124 = p[22];
    t125 = t28*t124;
    t126 = t72*t125;
    t128 = t66*t36;
    t129 = t128*t117;
    t130 = t35*t112;
    t133 = t41*t67;
    t134 = t133*t117;
    t135 = t21*t124;
    t136 = t72*t135;
    t139 = t19*t41;
    t140 = t55*t55;
    t142 = t139*t42*t140;
    t143 = t49*t25;
    t144 = t143*t52;
    t146 = t46*t21;
    t147 = t146*t43;
    t150 = t143*t58;
    t153 = t140*t22;
    t154 = t43*t153;
    t157 = t17*t46;
    t158 = t116*t157;
    t161 = t50*t26;
    t162 = t161*t125;
    t165 = t36*t124;
    t166 = t72*t165;
    t169 = t17*t55;
    t170 = t68*t169;
    t171 = t22*t50;
    t172 = t34*t124;
    t173 = t171*t172;
    t176 = 2.0*t154*t107-4.0*t111*t113-4.0*t111*t162-4.0*t111*t166+4.0*t118*
t120-t123*t126+2.0*t129*t130+2.0*t134*t136+t142*t144+2.0*t147*t150+2.0*t147*t63
-3.0*t158*t63+4.0*t170*t173;
    t182 = t161*t135;
    t185 = t44*t124;
    t186 = t72*t185;
    t189 = t76*t46;
    t190 = t26*t36;
    t191 = t51*t190;
    t194 = t68*t17;
    t198 = u[10];
    t199 = t34*t198;
    t200 = t51*t199;
    t202 = t17*t140;
    t203 = t202*t50;
    t204 = t124*t124;
    t205 = t42*t204;
    t209 = t34*t21;
    t213 = t202*t25;
    t214 = t29*t29;
    t215 = t214*t42;
    t222 = t139*t42;
    t224 = t99*t28;
    t225 = t143*t224;
    t227 = -2.0*t194*t71*t161+8.0*t203*t209*t205+8.0*t203*t52*t205+8.0*t213*
t209*t215+8.0*t213*t52*t215+4.0*t118*t182+4.0*t118*t186+2.0*t154*t126+2.0*t129*
t136+2.0*t189*t191+t189*t200-t222*t200-t222*t225;
    t228 = t143*t190;
    t231 = t76*t105;
    t240 = t18*t46;
    t242 = t240*t92*t22;
    t245 = t41*t28*t97;
    t248 = t133*t69;
    t251 = t66*t42;
    t252 = t251*t105;
    t254 = t251*t110;
    t258 = t72*t198*t124;
    t260 = t146*t76;
    t261 = t55*t49;
    t262 = t72*t28;
    t266 = t157*t171;
    t267 = t124*t42;
    t272 = t46*t44;
    t273 = t17*t42;
    t274 = t272*t273;
    t276 = t171*t52*t124;
    t278 = -2.0*t266*t52*t267*t45+2.0*t260*t261*t262-t242*t107-4.0*t111*t182
-4.0*t111*t186+t252*t126+2.0*t154*t136-2.0*t231*t136+6.0*t254*t162-3.0*t245*
t166+2.0*t248*t173-2.0*t222*t228-t98*t258+t274*t276;
    t281 = t22*t25;
    t283 = t281*t52*t29;
    t285 = t267*t55;
    t292 = t157*t281;
    t293 = t29*t42;
    t294 = t293*t55;
    t301 = t116*t169;
    t311 = t43*t55*t19;
    t312 = t35*t28;
    t316 = t146*t273;
    t321 = t103*t55;
    t322 = t146*t321;
    t325 = t96*t153;
    t339 = -4.0*t86*t23*t50*t52*t285+4.0*t301*t171*t209*t124+2.0*t316*t171*t62*
t124+4.0*t325*t72*t125*t17+4.0*t325*t35*t30*t17+4.0*t301*t281*t209*t29-4.0*t266
*t58*t285-4.0*t266*t62*t285-4.0*t292*t58*t294-4.0*t292*t62*t294-2.0*t311*t88*
t312+t274*t283-2.0*t322*t276;
    t345 = t66*t17;
    t346 = t345*t22;
    t348 = t51*t224;
    t352 = t19*t66;
    t353 = t352*t67;
    t354 = t42*t49;
    t364 = t143*t199;
    t366 = t240*t22;
    t367 = t50*t99;
    t368 = t367*t125;
    t370 = t161*t165;
    t374 = t43*t22;
    t377 = -4.0*t86*t23*t25*t52*t294+2.0*t353*t354*t161-2.0*t194*t71*t27-2.0*
t374*t101+t189*t225+2.0*t189*t228+t189*t364-2.0*t222*t191-t222*t348-t346*t258+
t366*t258+t366*t368+2.0*t366*t370;
    t379 = t27*t37;
    t382 = t68*t42;
    t383 = t29*t26;
    t384 = t281*t383;
    t388 = t35*t198*t29;
    t391 = t76*t140;
    t392 = t26*t124;
    t393 = t171*t392;
    t398 = t67*t28;
    t399 = t139*t398;
    t400 = t354*t35;
    t402 = t18*t55;
    t403 = t402*t25;
    t404 = t62*t214;
    t407 = t273*t25;
    t414 = t321*t25;
    t415 = t58*t214;
    t423 = t103*t45;
    t424 = t423*t25;
    t425 = t52*t214;
    t428 = -4.0*t407*t190*t214-2.0*t407*t199*t214-2.0*t407*t224*t214-4.0*t374*
t379-2.0*t374*t388+4.0*t382*t384-2.0*t391*t384-2.0*t391*t393+t399*t400-4.0*t403
*t404+4.0*t414*t404+4.0*t414*t415+2.0*t424*t425;
    t429 = t99*t21;
    t433 = t26*t44;
    t437 = t26*t21;
    t438 = t437*t214;
    t441 = u[11];
    t442 = t34*t441;
    t446 = t34*t44;
    t447 = t446*t214;
    t450 = t209*t214;
    t453 = t273*t50;
    t460 = t321*t50;
    t461 = t58*t204;
    t467 = t62*t204;
    t470 = t423*t50;
    t471 = t52*t204;
    t480 = -4.0*t453*t190*t204-2.0*t453*t199*t204-2.0*t453*t224*t204-2.0*t453*
t429*t204-4.0*t453*t433*t204-2.0*t407*t429*t214-4.0*t407*t433*t214-2.0*t407*
t442*t214+4.0*t414*t438+4.0*t414*t447+2.0*t424*t450+4.0*t460*t461+4.0*t460*t467
+2.0*t470*t471;
    t484 = t437*t204;
    t490 = t446*t204;
    t494 = t66*t41*t49;
    t497 = t66*t66;
    t498 = t497*t28;
    t499 = t498*t49;
    t500 = t72*t36;
    t508 = t18*t45;
    t509 = t508*t25;
    t516 = t41*t41;
    t517 = t516*t28;
    t518 = t517*t49;
    t521 = t41*t398;
    t522 = t521*t49;
    t526 = p[40];
    t528 = t526*t17*t140;
    t529 = t209*t42;
    t532 = -t494*t35*t198-t494*t72*t198-2.0*t453*t442*t204-4.0*t494*t27*t36+4.0
*t522*t72*t66-4.0*t403*t438-4.0*t403*t447-2.0*t509*t450+4.0*t460*t484+4.0*t460*
t490+3.0*t499*t500-3.0*t518*t500+8.0*t528*t529;
    t533 = t50*t17;
    t538 = t17*t25;
    t543 = p[35];
    t544 = t543*t17;
    t545 = t544*t140;
    t548 = p[39];
    t549 = t548*t17;
    t550 = t549*t140;
    t551 = t52*t42;
    t556 = p[36];
    t558 = t556*t17*t140;
    t561 = p[30];
    t562 = t561*t41;
    t564 = t49*t66;
    t568 = t562*t34;
    t572 = t561*t497;
    t573 = t572*t28;
    t574 = t34*t49;
    t578 = t76*t22;
    t579 = u[12];
    t580 = t50*t579;
    t584 = t25*t579;
    t595 = -2.0*t533*t26*t205*t45-4.0*t533*t99*t205*t55-2.0*t538*t26*t215*t45
-6.0*t562*t26*t564*t36+2.0*t578*t580*t124-4.0*t494*t161*t36-2.0*t568*t564*t198+
2.0*t578*t584*t29+6.0*t573*t574*t36-8.0*t545*t529-8.0*t550*t529-8.0*t550*t551+
8.0*t558*t551;
    t601 = p[26];
    t602 = t601*t41;
    t604 = p[20];
    t605 = t604*t604;
    t606 = t605*t66;
    t614 = t41*t49;
    t616 = t99*t66;
    t617 = t616*t28;
    t620 = t561*t516;
    t621 = t620*t34;
    t622 = t49*t28;
    t630 = t508*t50;
    t633 = t402*t50;
    t638 = t209*t204;
    t647 = -4.0*t538*t99*t215*t55-6.0*t562*t99*t564*t28-6.0*t602*t99*t606*t28+
8.0*t568*t49*t398*t66-6.0*t621*t622*t36-5.0*t614*t50*t617-2.0*t509*t425-4.0*
t633*t461-4.0*t633*t467-2.0*t630*t471-4.0*t633*t484-4.0*t633*t490-2.0*t630*t638
;
    t650 = t35*t36;
    t656 = t601*t516;
    t657 = t656*t34;
    t658 = t605*t28;
    t662 = t602*t34;
    t682 = t601*t497;
    t683 = t682*t28;
    t684 = t34*t605;
    t694 = -4.0*t403*t415-3.0*t518*t650+4.0*t522*t35*t66-6.0*t657*t658*t36+8.0*
t662*t605*t398*t66+8.0*t558*t529+8.0*t528*t551+2.0*t470*t638+3.0*t499*t650-6.0*
t602*t26*t606*t36-2.0*t662*t606*t198+6.0*t683*t684*t36-8.0*t545*t551+2.0*t316*
t281*t58*t29;
    t710 = t19*t92;
    t711 = t710*t273;
    t713 = t146*t402;
    t726 = t526*t18;
    t727 = t55*t26;
    t728 = t727*t28;
    t731 = t55*t34;
    t732 = t731*t36;
    t735 = t45*t34;
    t736 = t735*t21;
    t739 = 2.0*t316*t171*t58*t124+2.0*t316*t281*t62*t29-2.0*t292*t52*t293*t45+
2.0*t260*t261*t312-2.0*t311*t88*t262+t711*t276+2.0*t713*t276-2.0*t322*t283+t711
*t283+2.0*t713*t283-4.0*t726*t728-4.0*t726*t732-2.0*t726*t736;
    t740 = t727*t21;
    t743 = t731*t44;
    t746 = t516*t34;
    t747 = p[32];
    t748 = t747*t28;
    t752 = p[27];
    t753 = t34*t752;
    t757 = p[28];
    t758 = t757*t28;
    t762 = p[31];
    t763 = t34*t762;
    t767 = t103*t140;
    t774 = t497*t67;
    t775 = t51*t26;
    t778 = t143*t26;
    t781 = t516*t579;
    t784 = t579*t18;
    t787 = 2.0*t767*t161*t204+2.0*t767*t27*t214+6.0*t517*t753*t36+6.0*t517*t763
*t36-6.0*t746*t748*t36-6.0*t746*t758*t36+t784*t526+t784*t556-4.0*t726*t740-4.0*
t726*t743+t781*t747+t781*t757+4.0*t774*t775+4.0*t774*t778;
    t789 = t497*t579;
    t791 = t579*t103;
    t795 = t521*t17;
    t797 = t354*t72;
    t799 = t71*t35;
    t801 = t521*t42;
    t804 = t34*t29;
    t805 = t281*t804;
    t808 = t116*t17;
    t809 = t99*t124;
    t810 = t171*t809;
    t813 = t43*t45;
    t816 = t43*t55;
    t819 = t99*t29;
    t820 = t281*t819;
    t823 = 2.0*t801*t173-2.0*t813*t393+t399*t797+t791*t543+t791*t548-t795*t73+
t789*t752+t789*t762-t795*t799+2.0*t801*t805-5.0*t808*t810-5.0*t808*t820-4.0*
t816*t810;
    t855 = -2.0*t374*t72*t441*t124-2.0*t374*t100*t112-2.0*t374*t367*t135-4.0*
t374*t161*t185+2.0*t353*t354*t27+t366*t101-t222*t364-2.0*t374*t258-4.0*t346*
t370+2.0*t366*t379+t366*t388-2.0*t374*t368-4.0*t374*t370+4.0*t382*t393;
    t860 = t133*t17;
    t863 = t66*t398;
    t864 = t863*t17;
    t884 = t22*t46;
    t885 = t767*t884;
    t889 = t76*t140*t46;
    t891 = -2.0*t374*t35*t441*t29-4.0*t374*t27*t119+2.0*t885*t126+t864*t173+
t189*t348-4.0*t346*t379-t346*t388-2.0*t813*t384-4.0*t860*t384-4.0*t860*t393-
t889*t53+t864*t805-4.0*t816*t820;
    t902 = t146*t68;
    t905 = t139*t169;
    t906 = t143*t62;
    t910 = t710*t43;
    t915 = t345*t153;
    t919 = 2.0*t87*t261*t35+t252*t107-4.0*t111*t31+t915*t126+2.0*t134*t130-t889
*t144+t910*t144+2.0*t59*t147-2.0*t57*t150-3.0*t245*t38-t98*t388-2.0*t902*t400
-2.0*t905*t906;
    t921 = t104*t110;
    t925 = t124*t140;
    t930 = t29*t140;
    t941 = t20*t44*t22;
    t949 = t548*t18;
    t950 = t26*t140;
    t953 = t26*t18;
    t954 = t526*t140;
    t957 = -2.0*t240*t171*t52*t925-2.0*t240*t281*t52*t930-2.0*t87*t88*t35+t941*
t107-t242*t126+t941*t126-t94*t144+2.0*t24*t162+2.0*t24*t166+2.0*t921*t166+2.0*
t81*t906+2.0*t949*t950-2.0*t953*t954;
    t958 = t556*t140;
    t964 = t497*t26;
    t965 = t752*t67;
    t971 = t762*t67;
    t974 = t516*t49;
    t976 = t18*t50;
    t979 = t543*t103;
    t982 = t526*t42;
    t983 = t17*t26;
    t984 = t983*t44;
    t987 = t526*t103;
    t990 = t17*t34;
    t991 = t990*t441;
    t998 = t41*t99;
    t999 = t747*t66;
    t1003 = t976*t579*t204+6.0*t774*t26*t747+6.0*t774*t26*t757-6.0*t998*t999*
t28+t974*t584+2.0*t987*t736-4.0*t979*t740+4.0*t987*t740+4.0*t987*t743-2.0*t953*
t958-6.0*t964*t965-6.0*t964*t971-4.0*t982*t984-2.0*t982*t991;
    t1006 = t41*t26;
    t1010 = t41*t34;
    t1014 = t752*t41;
    t1018 = t66*t26;
    t1032 = t20*t110;
    t1041 = -6.0*t1006*t999*t36-2.0*t1010*t999*t198+6.0*t1018*t1014*t36+6.0*
t616*t1014*t28-5.0*t614*t25*t617-2.0*t1032*t162-2.0*t1032*t31-2.0*t231*t107+6.0
*t118*t38-3.0*t158*t906+2.0*t921*t31+2.0*t921*t38+t910*t53;
    t1061 = t352*t28*t42;
    t1070 = t272*t43;
    t1072 = 2.0*t87*t261*t72-2.0*t1032*t166+3.0*t1061*t63+3.0*t1061*t906+t1070*
t144-4.0*t111*t38+2.0*t154*t130-t48*t144+2.0*t147*t906+2.0*t81*t150+2.0*t254*
t166-2.0*t98*t370-2.0*t98*t379-2.0*t57*t906;
    t1081 = t18*t25;
    t1089 = t516*t26;
    t1093 = t516*t67;
    t1097 = t757*t67;
    t1103 = t26*t103;
    t1106 = t1081*t579*t214-6.0*t1089*t747*t67+6.0*t1093*t26*t752+6.0*t1093*t26
*t762+t620*t579*t49+t656*t579*t605-6.0*t1089*t1097+2.0*t1103*t954-4.0*t111*t120
+6.0*t118*t166+t142*t53+4.0*t170*t805+t974*t580;
    t1111 = t548*t103;
    t1114 = t543*t18;
    t1127 = t139*t17*t45;
    t1135 = t106*t126+2.0*t885*t107+t915*t107+t11*t26+2.0*t1103*t958-2.0*t1111*
t950+2.0*t1114*t950-t1127*t53-2.0*t231*t126-2.0*t905*t150+t579*p[25]-2.0*t905*
t59-2.0*t979*t950+p[42]*t99;
    t1161 = -2.0*t1032*t38-t123*t107+t1070*t53-t1127*t144+4.0*t118*t113-2.0*
t231*t130+t78*t144+2.0*t248*t805+6.0*t254*t31+2.0*t254*t38-2.0*t905*t63+2.0*t70
*t799-2.0*t902*t797;
    t1168 = t26*t605*t67;
    t1175 = t548*t42;
    t1176 = t17*t99;
    t1177 = t1176*t28;
    t1180 = t983*t36;
    t1185 = t990*t198;
    t1190 = t735*t28;
    t1193 = t1176*t21;
    t1196 = -4.0*t1093*t778-2.0*t1111*t1190-4.0*t1111*t728-4.0*t1111*t732-6.0*
t656*t1168+2.0*t1175*t1177+4.0*t1175*t1180+2.0*t1175*t1185+2.0*t1175*t1193+2.0*
t921*t162-t98*t368-2.0*t979*t736-4.0*t979*t743;
    t1206 = t757*t66;
    t1210 = t762*t41;
    t1217 = t66*t34;
    t1221 = t543*t42;
    t1232 = -2.0*t1010*t1206*t198+6.0*t1018*t1210*t36+2.0*t1217*t1210*t198+6.0*
t616*t1210*t28-4.0*t1111*t740-4.0*t1111*t743+4.0*t1175*t984+2.0*t1175*t991+2.0*
t1221*t1177+4.0*t1221*t1180+2.0*t1221*t1185-4.0*t979*t728-4.0*t979*t732;
    t1239 = t556*t18;
    t1246 = t34*t747;
    t1254 = t34*t757;
    t1262 = t556*t42;
    t1267 = t99*t42*t55;
    t1270 = t26*t42;
    t1271 = t1270*t45;
    t1276 = -8.0*t1217*t752*t398*t41-8.0*t1217*t762*t398*t41+8.0*t863*t1246*t41
+8.0*t863*t1254*t41-2.0*t983*t1262*t45-2.0*t979*t1190+2.0*t1221*t1193+4.0*t1221
*t984-2.0*t1239*t736-4.0*t1239*t740-4.0*t1239*t743+4.0*t544*t1267+4.0*t549*
t1267+2.0*t544*t1271;
    t1308 = -6.0*t1006*t1206*t36+2.0*t1217*t1014*t198-6.0*t998*t1206*t28-4.0*
t982*t1180-2.0*t982*t1185-2.0*t1239*t1190-2.0*t726*t1190+2.0*t987*t1190-2.0*
t982*t1193-4.0*t1239*t728+2.0*t549*t1271+4.0*t987*t728+4.0*t987*t732;
    t1312 = t67*t26*t49;
    t1340 = t556*t103;
    t1347 = -2.0*t1081*t26*t214*t140-2.0*t976*t26*t204*t140-4.0*t1176*t1262*t55
-4.0*t1176*t982*t55-2.0*t983*t982*t45-2.0*t1111*t736-2.0*t1262*t1177-2.0*t982*
t1177-4.0*t1262*t1180-2.0*t1262*t1185-4.0*t1239*t732+6.0*t572*t1312+4.0*t1340*
t728+4.0*t1340*t732;
    t1375 = 2.0*t1114*t1190+4.0*t1114*t728+4.0*t1114*t732+2.0*t1114*t736+6.0*
t682*t1168+2.0*t1340*t1190-2.0*t1262*t1193+2.0*t1221*t991-4.0*t1262*t984-2.0*
t1262*t991+2.0*t1340*t736+4.0*t1340*t740+4.0*t1340*t743;
    t1395 = t497*t34;
    t1396 = t752*t28;
    t1403 = t762*t28;
    t1411 = 6.0*t498*t1246*t36+6.0*t498*t1254*t36-6.0*t1395*t1396*t36-6.0*t1395
*t1403*t36-4.0*t1093*t775+4.0*t1114*t740+4.0*t1114*t743+2.0*t949*t1190-6.0*t620
*t1312+4.0*t949*t728+4.0*t949*t732+2.0*t949*t736+4.0*t949*t740+4.0*t949*t743;
    t1424 = p[7];
    t1427 = tanh(t1424*t1*t7);
    y[0] = 1/t1*(p[10]*t5*t8*t7+t4/t11*(t1072+t377+t176+t1003+t1308+t1106+t1276
+t595+t1161+t855+t227+t532+t1411+t739+t1232+t647+t278+t1041+t891+t1135+t919+
t823+t1375+t339+t109+t480+t1347+t694+t1196+t428+t957+t787)*p[4]+p[9]*t4*t7+p[8]
*t1427*t1-t11*u[0]+t11*u[15]);
    t1435 = p[2];
    t1438 = t1435*t1435;
    t1439 = t1438*t1438;
    t1441 = u[19];
    t1442 = t1441*t1441;
    t1445 = t86*t23;
    t1446 = t34*t34;
    t1447 = t25*t1446;
    t1459 = t1270*t55;
    t1463 = t50*t1446;
    t1470 = t72*t392;
    t1472 = 4.0*t111*t1470;
    t1473 = t1446*t124;
    t1474 = t171*t1473;
    t1476 = 2.0*t301*t1474;
    t1477 = t35*t383;
    t1479 = 4.0*t118*t1477;
    t1480 = t34*t26;
    t1481 = t51*t1480;
    t1490 = t35*t819;
    t1493 = t1446*t29;
    t1494 = t281*t1493;
    t1496 = t72*t809;
    t1500 = 4.0*t316*t171*t172*t26+4.0*t316*t281*t804*t26-4.0*t1445*t1447*t294
-4.0*t1445*t1463*t285-8.0*t266*t172*t1459-8.0*t292*t804*t1459+4.0*t1061*t1481+
4.0*t24*t1470+t274*t1474+4.0*t24*t1477-4.0*t905*t1481-2.0*t98*t1490+t274*t1494
-2.0*t98*t1496-t1472+t1476+t1479;
    t1501 = t88*t1463;
    t1504 = t710*t41;
    t1505 = t354*t1463;
    t1507 = t139*t67;
    t1509 = t354*t1447;
    t1511 = t140*t49;
    t1514 = t34*t99;
    t1515 = t143*t1514;
    t1518 = t272*t41;
    t1520 = t352*t36;
    t1522 = t139*t17;
    t1523 = t45*t49;
    t1528 = t51*t1514;
    t1537 = t71*t1447;
    t1540 = p[0];
    t1545 = -2.0*t146*t42*t55*t1540*t281+t222*t1511*t1447-t1522*t1523*t1447+
t222*t1511*t1463-t1522*t1523*t1463-2.0*t311*t1501+t1504*t1505+t1504*t1509+t1507
*t1505+t1518*t1505+t1520*t1505+t1518*t1509+t1520*t1509-2.0*t222*t1515+2.0*t189*
t1528-2.0*t222*t1528-t813*t1537;
    t1547 = t80*t22;
    t1548 = t50*t1540;
    t1552 = t146*t22;
    t1556 = t240*t92;
    t1560 = t20*t44;
    t1563 = t50*t28;
    t1567 = t25*t28;
    t1571 = t104*t45;
    t1574 = t25*t1540;
    t1581 = t20*t45;
    t1583 = t767*t46;
    t1588 = t128*t17;
    t1589 = t71*t1463;
    t1591 = 6.0*t1547*t1567*t119-2.0*t1547*t1548*t169+6.0*t1547*t1563*t185-2.0*
t1547*t1574*t169-2.0*t1552*t1548*t69-t1556*t1474+t1560*t1474+t1571*t1474+2.0*
t1583*t1474+2.0*t366*t1490-t1556*t1494+t1560*t1494+t1571*t1494-t1581*t1494+2.0*
t1583*t1494+2.0*t366*t1496-t1588*t1589;
    t1593 = t44*t49;
    t1596 = t92*t49;
    t1602 = t133*t42;
    t1603 = t1602*t1474;
    t1604 = u[14];
    t1605 = t50*t1604;
    t1606 = t1605*t204;
    t1607 = u[13];
    t1610 = p[18];
    t1612 = p[37];
    t1613 = t1612*t1607;
    t1614 = t1612*t1604;
    t1615 = p[41];
    t1616 = t1615*t1607;
    t1617 = t1615*t1604;
    t1625 = t36*t1446*t49;
    t1627 = t1522*t1593*t1463-t189*t1596*t1463-t601*t1607*t605+2.0*t189*t1515-
t1588*t1537-t813*t1589-t1607*p[29]-t1607*p[33]-t1610*t36+t572*t1625-p[43]*t198+
t1603-t1606-t1613-t1614-t1616-t1617;
    t1640 = t1446*t605*t36;
    t1643 = t92*t92;
    t1644 = t46*t1643;
    t1649 = t19*t22;
    t1650 = t25*t1604;
    t1666 = t67*t1540;
    t1680 = -4.0*t1395*t1403*t26+4.0*t498*t1246*t26-4.0*t1395*t1396*t26-t656*
t1640-t620*t1625-t1644*t171*t124-t1644*t281*t29-t1649*t1650*t29-2.0*t1010*t1206
*t99+4.0*t498*t1254*t26+4.0*t41*t1446*t1097*t66-4.0*t746*t758*t26+t602*t1666*
t604+t562*t1666*t22+4.0*t517*t763*t26-4.0*t746*t748*t26+4.0*t517*t753*t26;
    t1681 = t44*t44;
    t1686 = t26*t26;
    t1699 = t66*t1446;
    t1703 = t1446*t747;
    t1713 = t36*t1540;
    t1722 = t1446*t17;
    t1726 = 4.0*t1722*t140*t548*t42;
    t1729 = 2.0*t990*t1262*t99;
    t1730 = t34*t103;
    t1732 = t55*t556*t26;
    t1734 = 4.0*t1730*t1732;
    t1737 = 4.0*t1722*t958*t42;
    t1740 = 2.0*t990*t982*t99;
    t1742 = t55*t526*t26;
    t1744 = 4.0*t1730*t1742;
    t1745 = 3.0*t884*t25*t1681*t29-2.0*t602*t1686*t605*t66-2.0*t562*t1686*t49*
t66+3.0*t884*t50*t1681*t124-4.0*t1699*t971*t41+4.0*t68*t1703*t41-4.0*t1699*t965
*t41-t1649*t1605*t124+t601*t66*t1713*t604+t561*t66*t1713*t22+2.0*t1217*t1210*
t99-t1726-t1729+t1734+t1737-t1740+t1744;
    t1749 = 4.0*t1722*t954*t42;
    t1750 = t1463*t204;
    t1751 = t508*t1750;
    t1752 = t1447*t214;
    t1753 = t508*t1752;
    t1754 = t50*t1607;
    t1756 = t1649*t1754*t124;
    t1758 = t25*t1607;
    t1760 = t1649*t1758*t29;
    t1762 = t42*t45;
    t1763 = t1548*t124;
    t1764 = t1762*t1763;
    t1765 = t1574*t29;
    t1766 = t1762*t1765;
    t1767 = t423*t1750;
    t1768 = t423*t1752;
    t1769 = t202*t1763;
    t1770 = t202*t1765;
    t1771 = t34*t18;
    t1773 = t55*t543*t26;
    t1775 = 4.0*t1771*t1773;
    t1777 = t55*t548*t26;
    t1779 = 4.0*t1771*t1777;
    t1781 = 4.0*t1771*t1732;
    t1783 = 4.0*t1771*t1742;
    t1784 = t25*t1686;
    t1787 = 2.0*t273*t1784*t214;
    t1788 = t50*t1686;
    t1791 = 2.0*t273*t1788*t204;
    t1792 = t1749-t1751-t1753-2.0*t1756-2.0*t1760+t1764+t1766+t1767+t1768+t1769
+t1770+t1775+t1779-t1781-t1783-t1787-t1791;
    t1793 = t34*t42;
    t1796 = 2.0*t1793*t544*t99;
    t1798 = 4.0*t1730*t1773;
    t1802 = 4.0*t1722*t140*t543*t42;
    t1805 = 2.0*t1793*t549*t99;
    t1807 = 4.0*t1730*t1777;
    t1810 = t1754*t204;
    t1811 = t1758*t214;
    t1812 = t1650*t214;
    t1813 = t884*t50;
    t1814 = t1540*t17;
    t1815 = t1814*t45;
    t1817 = t1540*t42;
    t1818 = t1817*t140;
    t1820 = t272*t22;
    t1821 = t1548*t17;
    t1823 = t884*t25;
    t1837 = 4.0*t602*t1446*t605*t67*t66-t561*t1607*t49+4.0*t683*t684*t26-2.0*
t662*t606*t99-t1813*t1815+t1813*t1818-t1823*t1815+t1823*t1818+t1820*t1821+t1796
-t1798-t1802+t1805-t1807-t1810-t1811-t1812;
    t1857 = t46*t92*t21*t22;
    t1864 = t28*t441;
    t1868 = t77*t22;
    t1869 = t1574*t42;
    t1871 = t93*t22;
    t1873 = t143*t1686;
    t1876 = t51*t1686;
    t1887 = t710*t22;
    t1898 = t1540*t22*t25;
    t1900 = -2.0*t568*t564*t99+4.0*t573*t574*t26+4.0*t562*t1446*t49*t67*t66+4.0
*t1813*t135*t441-2.0*t1857*t1563*t124-2.0*t1857*t1567*t29+2.0*t1823*t1864*t29-
t1868*t1869+t1871*t1869-2.0*t222*t1873+2.0*t189*t1876-2.0*t222*t1876-4.0*t657*
t658*t26-4.0*t621*t622*t26+6.0*t1887*t50*t124*t44+6.0*t1887*t25*t29*t44+t710*
t17*t1898;
    t1934 = t143*t1480;
    t1941 = t86*t140*t1898-2.0*t1010*t999*t99+2.0*t1217*t1014*t99+t682*t1640+
t711*t1474+2.0*t713*t1474-4.0*t1032*t1477+t711*t1494+2.0*t713*t1494-4.0*t1032*
t1470+4.0*t325*t1463*t124*t17+4.0*t325*t1447*t29*t17-2.0*t266*t1473*t1762-2.0*
t292*t1493*t1762-4.0*t158*t1934-4.0*t57*t1934+4.0*t81*t1934;
    t1943 = t116*t69;
    t1946 = t116*t86;
    t1947 = t88*t1447;
    t1952 = t261*t1447;
    t1967 = t146*t116;
    t1970 = t261*t1463;
    t1983 = 4.0*t1061*t1934+4.0*t147*t1934-4.0*t158*t1481-4.0*t57*t1481+4.0*t81
*t1481-2.0*t1946*t1501-2.0*t1967*t1505-2.0*t1967*t1509+2.0*t1943*t1537+2.0*
t1943*t1589-4.0*t905*t1934-2.0*t1946*t1947+2.0*t1946*t1952+2.0*t1946*t1970-2.0*
t311*t1947+2.0*t260*t1952+2.0*t260*t1970;
    t1995 = 4.0*t111*t1477;
    t1997 = 2.0*t301*t1494;
    t1999 = 4.0*t118*t1470;
    t2001 = t1649*t50;
    t2004 = t1784*t29;
    t2007 = t1788*t124;
    t2011 = t50*t36*t124;
    t2012 = t1820*t2011;
    t2016 = t1552*t50*t198*t124;
    t2019 = t25*t36*t29;
    t2020 = t1820*t2019;
    t2024 = t1552*t25*t198*t29;
    t2029 = 4.0*t203*t1446*t204*t42;
    t2030 = t2001*t1814*t140+4.0*t147*t1481+4.0*t921*t1470-2.0*t322*t1474+4.0*
t921*t1477-2.0*t322*t1494+t1887*t1821+2.0*t366*t2004+2.0*t366*t2007-t1995+t1997
+t1999+6.0*t2012+6.0*t2016+6.0*t2020+6.0*t2024+t2029;
    t2033 = t34*t214;
    t2036 = 2.0*t407*t2033*t99;
    t2037 = t2033*t26;
    t2039 = 4.0*t414*t2037;
    t2043 = 4.0*t213*t1446*t214*t42;
    t2044 = t34*t204;
    t2047 = 2.0*t453*t2044*t99;
    t2048 = t2044*t26;
    t2050 = 4.0*t460*t2048;
    t2052 = 2.0*t374*t2004;
    t2054 = 2.0*t374*t2007;
    t2056 = 4.0*t633*t2048;
    t2058 = 4.0*t403*t2037;
    t2059 = t1887*t2011;
    t2061 = t1887*t2019;
    t2063 = t1602*t1494;
    t2064 = t128*t42;
    t2065 = t2064*t1474;
    t2066 = t76*t45;
    t2067 = t2066*t1474;
    t2068 = t43*t140;
    t2069 = t2068*t1474;
    t2071 = 2.0*t374*t1490;
    t2072 = t2064*t1494;
    t2073 = -t2036+t2039+t2043-t2047+t2050-t2052-t2054-t2056-t2058+6.0*t2059+
6.0*t2061+t2063+t2065-t2067+t2069-t2071+t2072;
    t2074 = t2066*t1494;
    t2075 = t2068*t1494;
    t2077 = 2.0*t374*t1496;
    t2089 = t96*t17;
    t2103 = t41*t1686;
    t2106 = t516*t36;
    t2109 = -t2074+t2075-t2077+t1522*t1593*t1447-t189*t1596*t1447-t1581*t1474
-2.0*t366*t1463*t925-2.0*t366*t1447*t930-2.0*t2089*t281*t1686*t29-2.0*t2089*
t171*t1686*t124-t391*t1589-t860*t1537-t391*t1537+t1507*t1509-t860*t1589-2.0*
t2103*t1206+t2106*t1446*t752;
    t2111 = t497*t36;
    t2114 = t497*t1446;
    t2120 = t516*t1446;
    t2127 = t1446*t18;
    t2128 = t45*t543;
    t2129 = t2127*t2128;
    t2130 = t45*t548;
    t2131 = t2127*t2130;
    t2132 = t556*t45;
    t2133 = t2127*t2132;
    t2134 = t526*t45;
    t2135 = t2127*t2134;
    t2136 = t1446*t103;
    t2137 = t2136*t2128;
    t2138 = t2136*t2130;
    t2139 = t2136*t2132;
    t2140 = t2136*t2134;
    t2141 = t1686*t42;
    t2143 = 2.0*t2141*t544;
    t2145 = 2.0*t2141*t549;
    t2146 = t2106*t1446*t762+t2111*t1446*t757-t2114*t752*t36-t2114*t762*t36-
t2120*t747*t36-t2120*t757*t36+t2111*t1703+t2129+t2131-t2133-t2135-t2137-t2138+
t2139+t2140+t2143+t2145;
    t2147 = t1686*t17;
    t2149 = 2.0*t2147*t1262;
    t2151 = 2.0*t2147*t982;
    t2152 = t66*t1686;
    t2159 = t46*t46;
    t2161 = t1607*t49;
    t2165 = t19*t19;
    t2166 = t2165*t49;
    t2183 = t1548*t42;
    t2186 = -t2149-t2151+2.0*t2152*t1210-2.0*t2103*t999+2.0*t2152*t1014-t50*
t2159*t2161-t25*t2159*t2161-t2166*t1754-t2166*t1758+2.0*t189*t1873+t2001*t1817*
t45+2.0*t1813*t1864*t124+4.0*t1823*t112*t441+t272*t17*t1898+t96*t45*t1898-t1868
*t2183+t1871*t2183;
    t2202 = tanh(t1424*t1435*t1441);
    y[1] = 1/t1435*(p[13]*t1439*t1442*t1441-t1438*(t1500+t1545+t1591+t1627+
t1680+t1745+t1792+t1837+t1900+t1941+t1983+t2030+t2073+t2109+t2146+t2186)/t1610*
p[5]+p[12]*t1438*t1441+p[11]*t2202*t1435-t1610*t15+t1610*u[16]);
    t2209 = p[3];
    t2212 = t2209*t2209;
    t2213 = t2212*t2212;
    t2215 = u[20];
    t2216 = t2215*t2215;
    t2221 = p[19];
    t2223 = -t2221*t44-p[44]*t441-t1472+t1476+t1479+t1603-t1606-t1613-t1614-
t1616-t1617;
    t2224 = -t1726-t1729+t1734+t1737-t1740+t1744+t1749-t1751-t1753-t1756-t1760+
t1764;
    t2226 = t1766+t1767+t1768+t1769+t1770+t1775+t1779-t1781-t1783-t1787-t1791;
    t2227 = t36*t36;
    t2236 = -2.0*t884*t50*t2227*t124-2.0*t884*t25*t2227*t29+t1796-t1798-t1802+
t1805-t1807-t1810-t1811-t1812-t1995+t1997;
    t2243 = t50*t67*t124;
    t2250 = t25*t67*t29;
    t2255 = -2.0*t1813*t125*t198-2.0*t1823*t30*t198-t1868*t2243-t1868*t2250+
t1871*t2243+t1871*t2250+t1999+t2012+2.0*t2016+t2020+2.0*t2024;
    t2256 = t2029-t2036+t2039+t2043-t2047+t2050-t2052-t2054-t2056-t2058+t2059+
t2061;
    t2258 = t2063+t2065-t2067+t2069-t2071+t2072-t2074+t2075-t2077+t2129+t2131;
    t2265 = -4.0*t1547*t1563*t165-4.0*t1547*t1567*t37-t2133-t2135-t2137-t2138+
t2139+t2140+t2143+t2145-t2149-t2151;
    t2280 = tanh(t1424*t2209*t2215);
    y[2] = 1/t2209*(p[16]*t2213*t2216*t2215-t2212*(t2223+t2224+t2226+t2236+
t2255+t2256+t2258+t2265)/t2221*p[6]+p[15]*t2212*t2215+p[14]*t2280*t2209-t2221*
t14+t2221*u[17]);
    return;
  }
}

static void mdlOutputs(SimStruct *S,int_T tid)
{
  real_T *y = ssGetOutputPortRealSignal(S,0);
  real_T *p = mxGetPr(ssGetSFcnParam(S,0));
  int_T i;
  InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);
  real_T u[INPUTPORT0WIDTH];

  for(i=0; i<INPUTPORT0WIDTH; i++) {
    u[i]=*uPtrs[i];
  }
  calcOutputs(y,u,p);
}

static void mdlTerminate(SimStruct *S)
{
}

#ifdef MATLAB_MEX_FILE   /* Compile as a MEX-file? */
  #include "simulink.c"  /* MEX-file interface mechanism */
#else
  #include "cg_sfun.h"   /* Code generation registration */
#endif
