% Compare 2 simulink models with different init files (init vs initBahn)

% %% To generate the computed values from simullink model
% JointTorque = [TrajTorqueMotor.Data(:,1)';TrajTorqueMotor.Data(:,2)';TrajTorqueMotor.Data(:,3)'];
% JointPose = [TrajJointMotor.Data(:,1)';TrajJointMotor.Data(:,2)';TrajJointMotor.Data(:,3)'];
% Time = TrajJointMotor.Time';
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\Time1sCoulCutFastBisSimulink','Time')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorque1sCoulCutFastBisSimulink','JointTorque')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPose1sCoulCutFastBisSimulink','JointPose')
% JointPose = [TrajJoint.Data(:,1)';TrajJoint.Data(:,2)';TrajJoint.Data(:,3)'];
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPoseDesired1sCoulCutFastBisSimulink','JointPose')

load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\Time1sCoulCutFastBisSimulink')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorque1sCoulCutFastBisSimulink')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPose1sCoulCutFastBisSimulink')
TimeSimulink1 = Time;
JointTorqueSimulink1 = JointTorque;
JointPoseSimulink1 = JointPose;


% JointTorque = [TrajTorqueMotor.Data(:,1)';TrajTorqueMotor.Data(:,2)';TrajTorqueMotor.Data(:,3)'];
% JointPose = [TrajJointMotor.Data(:,1)';TrajJointMotor.Data(:,2)';TrajJointMotor.Data(:,3)'];
% Time = TrajJointMotor.Time';
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\Time1sCoulCutFastBisSimulink','Time')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorque1sCoulCutFastBisSimulink','JointTorque')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPose1sCoulCutFastBisSimulink','JointPose')

figure
hold on
plot(Time,JointTorqueSimulink1)
plot(Time,JointTorqueSimulink2)
title('Torque')


figure
hold on
plot(Time,JointPoseSimulink1)
plot(Time,JointPoseSimulink2)
title('Positions')
