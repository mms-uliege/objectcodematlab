%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear all
clc

finaltime = 10;
timestepsize = 0.01;

nodes = [1 0 0 0;
         2 0 0 0;
         3 0 0 1;
         4 0 0 1;
         5 0 .5 1.866;
         6 0 .5 1.866;
         7 0 .75 1.433;
         8 0 .75 1.433;
         9 0 1 1];
     
elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2 3];
elements{1}.mass = 1;
elements{1}.J = eye(3);
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [4 5];
elements{2}.mass = 1;
elements{2}.J = eye(3);

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [6 7];
elements{3}.mass = .5;
elements{3}.J = eye(3);

elements{4}.type = 'RigidBodyElement';
elements{4}.nodes = [8 9];
elements{4}.mass = .5;
elements{4}.J = eye(3);

% elements{10}.type = 'ExternalForce';
% elements{10}.nodes = 5;
% elements{10}.DOF = 3;
% elements{10}.amplitude = -10;
% elements{10}.frequency = 0;

elements{5}.type = 'KinematicConstraint';
elements{5}.nodes = [1 2];
elements{5}.A = [0 0 0 0 0 1]';

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [3 4];
elements{6}.A = [0 0 0 1 0 0]';

elements{7}.type = 'KinematicConstraint';
elements{7}.nodes = [5 6];
elements{7}.A = [0 0 0 1 0 0]';

elements{8}.type = 'KinematicConstraint';
elements{8}.nodes = [7 8];
elements{8}.A = [0 0 0 1 0 0]';

% Trajectory

x_end = 0;
x_i = nodes(end,2);

y_end = 1.8;
y_i = nodes(end,3);

z_end = 1;
z_i = nodes(end,4);

A = [finaltime^5 finaltime^4 finaltime^3 0 0 1
     5*finaltime^4 4*finaltime^3 3*finaltime^2 0 0 0
     20*finaltime^3 12*finaltime^2 6*finaltime 0 0 0
     0 0 0 0 0 1
     0 0 0 0 1 0
     0 0 0 1 0 0];

cpx = A\[x_end 0 0 x_i 0 0]';
cpy = A\[y_end 0 0 y_i 0 0]';
% cpz = A\[z_end 0 0 z_i 0 0]';

Az = [finaltime^6 finaltime^5 finaltime^4 finaltime^3 0 0 1
     6*finaltime^5 5*finaltime^4 4*finaltime^3 3*finaltime^2 0 0 0
     30*finaltime^4 20*finaltime^3 12*finaltime^2 6*finaltime 0 0 0
     finaltime^6/2^6 finaltime^5/2^5 finaltime^4/2^4 finaltime^3/2^3 0 0 1
     0 0 0 0 0 0 1
     0 0 0 0 0 1 0
     0 0 0 0 1 0 0];

cpz = Az\[z_end 0 0 z_i+(y_end-y_i)/2 z_i 0 0]';

temps = 0:timestepsize:finaltime;
trajx = cpx(1)*temps.^5 + cpx(2)*temps.^4 + cpx(3)*temps.^3 + cpx(4)*temps.^2 + cpx(5)*temps + cpx(6);
trajy = cpy(1)*temps.^5 + cpy(2)*temps.^4 + cpy(3)*temps.^3 + cpy(4)*temps.^2 + cpy(5)*temps + cpy(6);
% trajz = cpz(1)*temps.^5 + cpz(2)*temps.^4 + cpz(3)*temps.^3 + cpz(4)*temps.^2 + cpz(5)*temps + cpz(6);
trajz = cpz(1)*temps.^6 + cpz(2)*temps.^5 + cpz(3)*temps.^4 + cpz(4)*temps.^3 + cpz(5)*temps.^2 + cpz(6)*temps + cpz(7);


elements{9}.type = 'TrajectoryConstraint';
elements{9}.nodes = [9];
elements{9}.T = [trajx;...
                 trajy;...
                 trajz];
elements{9}.Axe = [1 0 0;...
                 0 1 0;...
                 0 0 1];
elements{9}.elements = [5 6 7];


elements{10}.type = 'RotSpringDamperElement';
elements{10}.damping = .0;
elements{10}.stiffness = .1;
% elements{10}.natural_angle = pi/3;
elements{10}.nodes = [7 8];
elements{10}.A = [1 0 0];


BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

S = DynamicIntegration(Model);
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = timestepsize;
S.parameters.rho = 0.9;
S.runIntegration();

% Plots

figure
hold on
plot3(Model.listNodes{9}.position(1,:),Model.listNodes{9}.position(2,:),Model.listNodes{9}.position(3,:), 'Linewidth',2)
plot3(trajx,trajy,trajz, 'Linewidth',2, 'Color','r')
plot3(Model.listNodes{9}.position(1,:),Model.listNodes{9}.position(2,:),-3*ones(1,finaltime/timestepsize + 1), 'Linewidth',2,'Color',[.8 .8 .8])
axis(3*[-1 1 -1 1 -1 1])
grid on


figure
hold on
plot(S.parameters.time,Model.listNodes{9}.position(1,:),S.parameters.time,Model.listNodes{9}.position(2,:),S.parameters.time,Model.listNodes{9}.position(3,:))
plot(S.parameters.time,trajx,'--',S.parameters.time,trajy,'--',S.parameters.time,trajz,'--')
legend('X', 'Y', 'Z','Xd','Yd','Zd')
grid on


figure
plot(S.parameters.time,Model.listElementVariables{end}.value(1,:),S.parameters.time,Model.listElementVariables{end}.value(2,:),S.parameters.time,Model.listElementVariables{end}.value(3,:))
legend('u1','u2','u3')
grid on


