%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control

finaltime = 3.5;
timestepsize = 0.01;

nodes = [1 -1 0 0;
         2 -1 0 0;
         3 -1 0 0;
         4 -1.6614 -0.75 0;
         5 -1.6614 -0.75 0;
         6 -1 -1.5 0];

elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2];
elements{1}.mass = 3;
% elements{1}.J = eye(3);
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [3 4];
elements{2}.mass = 6.875;
elements{2}.J = 0.57*eye(3);

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [5 6];
elements{3}.mass = 6.875;
elements{3}.J = 0.57*eye(3);

elements{4}.type = 'KinematicConstraint';
elements{4}.nodes = [1 2];
elements{4}.A = [1 0 0 0 0 0]';

elements{5}.type = 'KinematicConstraint';
elements{5}.nodes = [2 3];
elements{5}.A = [0 0 0 0 0 1]';

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [4 5];
elements{6}.A = [0 0 0 0 0 1]';

load u
elements{11}.type = 'ForceInKinematicConstraint';
elements{11}.elements = [4];
elements{11}.f = u1;

elements{12}.type = 'ForceInKinematicConstraint';
elements{12}.elements = [5];
elements{12}.f = u2;

elements{13}.type = 'ForceInKinematicConstraint';
elements{13}.elements = [6];
elements{13}.f = u3;


% desired traj
cart_end = 1;
cart_i = -1;

x_end = 1;
x_i = -1;

y_end = -1.5;
y_i = -1.5;

trajcart = halfCircleTraj(cart_i,cart_end, finaltime,timestepsize,'lin',1,2.5);
trajx = halfCircleTraj(x_i,x_end, finaltime,timestepsize,'sin',1,2.5);
trajy = halfCircleTraj(y_i,y_end, finaltime,timestepsize,'cos',1,2.5);


BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

S = DynamicIntegration(Model);
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = timestepsize;
S.parameters.rho = 0.;
S.runIntegration();

% Plots

figure
hold on
plot(Model.listNodes{2}.position(1,:),Model.listNodes{2}.position(2,:),Model.listNodes{6}.position(1,:),Model.listNodes{6}.position(2,:), 'Linewidth',3)
plot(trajcart,zeros(size(trajcart)),trajx,trajy, 'Linewidth',1, 'Color','r')
grid on


figure
hold on
plot(S.parameters.time,Model.listNodes{2}.position(1,:),S.parameters.time,Model.listNodes{6}.position(1,:),S.parameters.time,Model.listNodes{6}.position(2,:))
plot(S.parameters.time,trajcart,'--',S.parameters.time,trajx,'--',S.parameters.time,trajy,'--')
legend('cart', 'X', 'Y','cartd','Xd','Yd')
grid on




