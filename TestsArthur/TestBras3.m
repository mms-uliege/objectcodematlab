%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control

finaltime = 2.5;
timestepsize = 0.01;

nodes = [1 0 0 0;
         2 0 0 0;
         3 -0.75 -0.6614 0;
         4 -0.75 -0.6614 0;
         5 -1.5 0 0];
     
m = 6.875;
% a = sqrt(0.57*6/m);
a = 0.05;
E = 100e9; nu = 0.33; G = E/(2*(1+nu));
A = a^2; I = a^4/12; J = 2*I;
rho = 8400;

KCS = diag([E*A G*A G*A G*J E*I E*I]);
MCS =  diag(rho*[A A A J I I]);
     
elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2 3];
elements{1}.mass = 6.875;
elements{1}.J = 0.57*eye(3);
     
% elements{2}.type = 'RigidBodyElement';
% elements{2}.nodes = [4 5];
% elements{2}.mass = 6.875/2;
% elements{2}.J = 0.57*eye(3);

elements{2}.type = 'FlexibleBeamElement';
elements{2}.nodes = [4 5];
elements{2}.KCS = KCS;
elements{2}.MCS = MCS;

elements{4}.type = 'KinematicConstraint';
elements{4}.nodes = [1 2];
elements{4}.A = [0 0 0 0 0 1]';

elements{5}.type = 'KinematicConstraint';
elements{5}.nodes = [3 4];
elements{5}.A = [0 0 0 0 0 1]';


% Trajectory

x_end = 1.5;
x_i = -1.5;

y_end = 0;
y_i = 0;

r = 1.5;

trajx = halfCircleTraj(x_i,x_end,r,finaltime,timestepsize,'sin',0.2,2);
trajy = halfCircleTraj(y_i,y_end,r,finaltime,timestepsize,'cos',0.2,2);

elements{8}.type = 'TrajectoryConstraint';
elements{8}.nodes = [5];
elements{8}.T = [trajx;...
                 trajy];
elements{8}.Axe = [1 0 0;...
                   0 1 0];
elements{8}.elements = [4 5];
elements{8}.active = 1;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.15;
% D.parameters.relTolRes = 1e-12;
D.runIntegration();

