%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear

finaltime = 2;
timestepsize = 0.01;
t_i = 0.2;
t_f = 1.8;

nodes = [1 0 0 0;
         2 0 0 0;
         3 0 0 0;
         4 0 sqrt(2)/8 sqrt(2)/8;
         5 0 sqrt(2)/8 sqrt(2)/8;
         6 0 sqrt(2)/4 0];
     
nElem = 2;
Start = 3;
End = 4;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 6;
End = 7;
nodes = createInterNodes(nodes,nElem,Start,End);

elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [4 3 5];
elements{1}.mass = 0.25;
elements{1}.J = 0.002*eye(3);

elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [7 6 8];
elements{2}.mass = 0.25;
elements{2}.J = 0.002*eye(3);

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [2];
elements{3}.mass = 0.4;

elements{4}.type = 'RigidBodyElement';
elements{4}.nodes = [5];
elements{4}.mass = 0.2;

elements{5}.type = 'RigidBodyElement';
elements{5}.nodes = [nodes(end,1)];
elements{5}.mass = 0.2;

% elements{2}.type = 'RigidBodyElement';
% elements{2}.nodes = [2];
% elements{2}.mass = 0.4;
% 
% elements{3}.type = 'RigidBodyElement';
% elements{3}.nodes = [5];
% elements{3}.mass = 0.2;
% 
% elements{4}.type = 'RigidBodyElement';
% elements{4}.nodes = [nodes(end,1)];
% elements{4}.mass = 0.2;

circular = true;
if ~circular
% square
    a = 0.005;
    E = 210000e9; nu = 0.3; G = E/(2*(1+nu)); rho = 7900;
    A = a^2; I = a^4/12; J = 2*I;
else
% circular
    d = 0.005;
    E = 70000e9; nu = 0.3; G = E/(2*(1+nu)); rho = 2700;
    A = pi*d^2/4; I = pi*d^4/64; J = 2*I;
end

KCS = diag([E*A G*A G*A G*J E*I E*I]);
MCS = diag(rho*[A A A J I I]);

% type = 'FlexibleBeamElement';
% Start = 6;
% End = nodes(end,1);
% nElem = End-Start;
% count = size(elements,2);
% for i = 1:nElem
%     elements{count+i}.type = type;
%     elements{count+i}.nodes = [nodes(Start+i-1,1) nodes(Start+i,1)];
%     elements{count+i}.KCS = KCS;
%     elements{count+i}.MCS = MCS;
% end

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 0 1]';
count = count+1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.A = [0 0 0 1 0 0]';
count = count+1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [5 6];
elements{count}.A = [0 0 0 1 0 0]';
count = count+1;
% Trajectory
%                 
% TrajParam.points = [0 sqrt(2)/4 0;...
%                     0 sqrt(2)/4 0.07;...
%                     1/4 1/4 0.2;...
%                     sqrt(2)/4 0 0.07;...
%                     sqrt(2)/4 0 0];
% TrajParam.tau = 0.05;                    
% TrajParam.timeVector = 0:timestepsize:finaltime;
% % TrajParam.intervals = [0.2 0.5 1.2 1.8 2.3];
% TrajParam.intervals = [0.2 0.4 1 1.6 1.8];
% 
% [trajx trajy trajz] = PointTraj(TrajParam);
% % % [trajx trajy trajz] = PointTrajCurve(TrajParam);

x_end = 0.3;
z_end = nodes(end,4);
r = 0.15;

timeVector = 0:timestepsize:finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'sin',t_i,t_f);
trajy = nodes(end,3)*ones(size(timeVector));
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);


elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                 trajy;...
                 trajz];
elements{count}.Axe = [1 0 0;...
                   0 1 0;...
                   0 0 1];
elements{count}.elements = [count-3 count-2 count-1];
elements{count}.active = 1;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

compute = 1;
if compute
    D = DynamicIntegration(Model);
    D.parameters.finaltime = finaltime;
    D.parameters.timestepsize = timestepsize;
    D.parameters.rho = 0.2;
    % D.parameters.relTolRes = 1e-10;
    D.parameters.scaling = 1e6;
    D.runIntegration();
else
    load('ArmBeamD')
    Model = D.model;
end

uRigiBeam.u1 = Model.listElementVariables{end}.value(1,:);
uRigiBeam.u2 = Model.listElementVariables{end}.value(2,:);
uRigiBeam.u3 = Model.listElementVariables{end}.value(3,:);
uRigiBeam.time = D.parameters.time;

% save('uArmRigiBeam','uRigiBeam')

% count = length(Model.listElements);
% Model.listElements{count}.active = 1;

for n = Model.listNumberNodes
    Model.listNodes{n}.InitializeD_Opti();
end
for n = Model.listNumberElementVariables
    Model.listElementVariables{n}.InitializeD_Opti();
end

if ~circular
% square
    E = 210e9; G = E/(2*(1+nu));
else
% circular
    E = 20e9; nu = 0.3; G = E/(2*(1+nu));
end
KCS = diag([E*A G*A G*A G*J E*I E*I]);
for elem = Model.listElements
   if isa(elem{1},'FlexibleBeamElement')
       elem{1}.KCS = KCS;
   end
end

npts = 100;
% TrajParam.timeVector = 0:finaltime/(npts-1):finaltime;
% [trajx trajy trajz] = PointTraj(TrajParam);
% [trajx trajy trajz] = PointTrajCurve(TrajParam);

timeVector = 0:finaltime/(npts-1):finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'sin',t_i,t_f);
trajy = nodes(end,3)*ones(size(timeVector));
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

Model.listElements{count}.T = [trajx;...
                            trajy;...
                            trajz];
tic
S = DirectTranscriptionOpti(Model);
% S.parameters.relTolRes = 1e-12;
S.parameters.rho = 0.2;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.NodeToMinimize = [7];
% S.JointToMinimize = [];
S.parameters.scaling = 1e6;
S.linConst = false;
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])

% Plots

timeSteps = S.timeValues;
timeLoc = S.timesteps;

uBeam.u1 = Model.listElementVariables{end}.value(1,timeLoc);
uBeam.u2 = Model.listElementVariables{end}.value(2,timeLoc);
uBeam.u3 = Model.listElementVariables{end}.value(3,timeLoc);
uBeam.time = S.timeValues;

% save('uArmBeam','uBeam')

endNode = nodes(end,1);
figure
hold on
plot3(Model.listNodes{endNode}.position(1,timeLoc),Model.listNodes{endNode}.position(2,timeLoc),Model.listNodes{endNode}.position(3,timeLoc), 'Linewidth',3)
plot3(trajx,trajy,trajz, 'Linewidth',1, 'Color','r')
grid on

figure
hold on
plot(timeSteps,Model.listNodes{endNode}.position(1,timeLoc),timeSteps,Model.listNodes{endNode}.position(2,timeLoc),timeSteps,Model.listNodes{endNode}.position(3,timeLoc))
plot(timeSteps,trajx,'--',timeSteps,trajy,'--',timeSteps,trajz,'--')
legend('X', 'Y', 'Z','Xd','Yd','Zd')
grid on

figure
hold on
plot(timeSteps,Model.listElementVariables{end}.value(1,timeLoc),timeSteps,Model.listElementVariables{end}.value(2,timeLoc),timeSteps,Model.listElementVariables{end}.value(3,timeLoc),'Linewidth',2)
plot(D.parameters.time,Model.listElementVariables{end}.value_InitOpti(1,:),':',D.parameters.time,Model.listElementVariables{end}.value_InitOpti(2,:),':',D.parameters.time,Model.listElementVariables{end}.value_InitOpti(3,:),':','Linewidth',2)
% plot(timeSteps,u1_init(timeLoc),':',timeSteps,u2_init(timeLoc),':',timeSteps,u3_init(timeLoc),':','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Commands','Fontsize',16)
legend('u1','u2','u3','u1_{rigid}','u2_{rigid}','u3_{rigid}','Location', 'Best')
title('Commands of a flexible Robot','Fontsize',18)
grid on

joint1_init = Model.listElementVariables{1}.relCoo_InitOpti;
joint2_init = Model.listElementVariables{2}.relCoo_InitOpti;
joint3_init = Model.listElementVariables{3}.relCoo_InitOpti;

joint1 = Model.listElementVariables{1}.relCoo(timeLoc);
joint2 = Model.listElementVariables{2}.relCoo(timeLoc);
joint3 = Model.listElementVariables{3}.relCoo(timeLoc);

figure
hold on
plot(timeSteps,joint1,timeSteps,joint2,timeSteps,joint3,'Linewidth',2)
plot(D.parameters.time,joint1_init,':',D.parameters.time,joint2_init,':',D.parameters.time,joint3_init,':','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints angle (rad)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid')
title('Joints angle of a flexible Robot','Fontsize',18)
grid on

joint1 = Model.listElementVariables{1};
joint2 = Model.listElementVariables{2};
joint3 = Model.listElementVariables{3};

figure
hold on
plot(timeSteps,joint1.velocity(timeLoc),timeSteps,joint2.velocity(timeLoc),timeSteps,joint3.velocity(timeLoc),'Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints velocity (rad/s)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3')
title('Joints velocity','Fontsize',18)
grid on

figure
hold on
plot(timeSteps,joint1.acceleration(timeLoc),timeSteps,joint2.acceleration(timeLoc),timeSteps,joint3.acceleration(timeLoc),'Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints accelerations (rad/s^2)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3')
title('Joints accelerations','Fontsize',18)
grid on




% 
% u2 = Model.listElementVariables{end}.value(2,timeLoc);
% v2 = Model.listElementVariables{2}.velocity(timeLoc);
% pos2 = Model.listElementVariables{2}.relCoo(timeLoc);
% 
% % file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestServo\TestServo\posIn.txt','w');
% file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestControl1\TestControl1\posIn.txt','w');
% for i = 1:length(pos2)
%     fprintf(file,'%10.10f \n',pos2(i));
% end
% fclose(file);
% 
% % file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestServo\TestServo\velocityIn.txt','w');
% file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestControl1\TestControl1\velocityIn.txt','w');
% for i = 1:length(v2)
%     fprintf(file,'%10.10f \n',v2(i));
% end
% fclose(file);
% 
% % file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestServo\TestServo\torqueIn.txt','w');
% file = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\ControlCode\TestControl1\TestControl1\torqueIn.txt','w');
% for i = 1:length(u2)
%     fprintf(file,'%10.10f \n',u2(i));
% end
% fclose(file);
