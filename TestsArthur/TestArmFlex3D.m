%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear

finaltime = 2.5;
timestepsize = 0.01;

nodes = [1 0 0 -1;
         2 0 0 -1;
         3 0 0 0;
         4 0 0 0;
         5 0 sqrt(2)/2 sqrt(2)/2;
         6 0 sqrt(2)/2 sqrt(2)/2;
         7 0 sqrt(2)*(3/4) sqrt(2)/4;
         8 0 sqrt(2)*(3/4) sqrt(2)/4;
         9 0 sqrt(2) 0];
     
nElem = 2;
Start = 2;
End = 3;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 5;
End = 6;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 8;
End = 9;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 11;
End = 12;
nodes = createInterNodes(nodes,nElem,Start,End);

elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [3 2 4];
elements{1}.mass = 6.875;
elements{2}.J = 0.57*eye(3);
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [6 5 7];
elements{2}.mass = 6.875;
elements{2}.J = 0.57*eye(3);

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [9 8 10];
elements{3}.mass = 6.875/2;
elements{3}.J = 0.0723*eye(3);

elements{4}.type = 'RigidBodyElement';
elements{4}.nodes = [12 11 13];
elements{4}.mass = 6.875/2;
elements{4}.J = 0.0723*eye(3);

elements{5}.type = 'KinematicConstraint';
elements{5}.nodes = [1 2];
elements{5}.A = [0 0 0 0 0 1]';

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [4 5];
elements{6}.A = [0 0 0 1 0 0]';

elements{7}.type = 'KinematicConstraint';
elements{7}.nodes = [7 8];
elements{7}.A = [0 0 0 1 0 0]';

elements{8}.type = 'KinematicConstraint';
elements{8}.nodes = [10 11];
elements{8}.A = [0 0 0 1 0 0]';

elements{9}.type = 'RotSpringDamperElement';
elements{9}.damping = 200;
elements{9}.stiffness = 100; 
elements{9}.nodes = [10 11];
elements{9}.A = [1 0 0];

% Trajectory

TrajParam.points = [0 sqrt(2) 0;...
                    0 sqrt(2) 0.2;...
                    sqrt(2)/4 3*sqrt(2)/4 0.5;...
                    sqrt(2)/2 sqrt(2)/2 0.2;...
                    sqrt(2)/2 sqrt(2)/2 -.1];
                    
TrajParam.timeVector = 0:timestepsize:finaltime;
TrajParam.intervals = [0.3 0.7 1.3 1.8 2.2];

[trajx trajy trajz] = PointTraj(TrajParam);

elements{11}.type = 'TrajectoryConstraint';
elements{11}.nodes = [13];
elements{11}.T = [trajx;...
                 trajy;...
                 trajz];
elements{11}.Axe = [1 0 0;...
                   0 1 0;...
                   0 0 1];
elements{11}.elements = [5 6 7];
elements{11}.active = 1;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.2;
% D.parameters.relTolRes = 1e-12;
D.runIntegration();

DeplCoude0 = Model.listElementVariables{4}.relCoo;
u1_init = Model.listElementVariables{end}.value(1,:);
u2_init = Model.listElementVariables{end}.value(2,:);
u3_init = Model.listElementVariables{end}.value(3,:);

Model.listElements{11}.active = 1;

Model.listElements{9}.stiffness = 50; % to make it unstable
Model.listElements{9}.damping = 0.25;

for n = Model.listNumberNodes
    Model.listNodes{n}.InitializeD_Opti();
end
for n = Model.listNumberElementVariables
    Model.listElementVariables{n}.InitializeD_Opti();
end

npts = 60;
TrajParam.timeVector = 0:finaltime/(npts-1):finaltime;
[trajx trajy trajz] = PointTraj(TrajParam);
Model.listElements{11}.T = [trajx;...
                            trajy;...
                            trajz];
tic
S = DirectTranscriptionOpti(Model);
% S.parameters.relTolRes = 1e-12;
S.parameters.rho = 0.2;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
% S.NodeToMinimize = [6 7];
S.JointToMinimize = [8];
S.parameters.scaling = 1e6;
S.linConst = false;
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])

DeplCoudeOpti = Model.listElementVariables{4}.relCoo;

% Plots

timeSteps = S.timeValues;
timeLoc = S.timesteps;

figure
hold on
plot3(Model.listNodes{13}.position(1,timeLoc),Model.listNodes{13}.position(2,timeLoc),Model.listNodes{13}.position(3,timeLoc), 'Linewidth',3)
plot3(trajx,trajy,trajz, 'Linewidth',1, 'Color','r')
grid on

figure
hold on
plot(timeSteps,Model.listNodes{13}.position(1,timeLoc),timeSteps,Model.listNodes{13}.position(2,timeLoc),timeSteps,Model.listNodes{13}.position(3,timeLoc))
plot(timeSteps,trajx,'--',timeSteps,trajy,'--',timeSteps,trajz,'--')
legend('X', 'Y', 'Z','Xd','Yd','Zd')
grid on

figure
hold on
plot(timeSteps,Model.listElementVariables{end}.value(1,timeLoc),timeSteps,Model.listElementVariables{end}.value(2,timeLoc),timeSteps,Model.listElementVariables{end}.value(3,timeLoc),'Linewidth',2)
plot(timeSteps,u1_init(timeLoc),':',timeSteps,u2_init(timeLoc),':',timeSteps,u3_init(timeLoc),':','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Commands','Fontsize',16)
legend('u1','u2','u3','u1_{rigid}','u2_{rigid}','u3_{rigid}')
title('Commands of a flexible Robot','Fontsize',18)
grid on

figure
plot(timeSteps,DeplCoude0(timeLoc),timeSteps,DeplCoudeOpti(timeLoc))
legend('Beta init','Beta Opti')
grid on

uFlex.u1 = Model.listElementVariables{end}.value(1,timeLoc);
uFlex.u2 = Model.listElementVariables{end}.value(2,timeLoc);
uFlex.u3 = Model.listElementVariables{end}.value(3,timeLoc);
uFlex.time = S.timeValues;

save('uArmFlex','uFlex')

beta = Model.listElementVariables{4}.relCoo(timeLoc);
betadot = Model.listElementVariables{4}.velocity(timeLoc);
% beta = Model.listElementVariables{4}.relCoo_InitOpti;
% betadot = Model.listElementVariables{4}.velocity_InitOpti;
figure
axis([min(beta) max(beta) min(betadot) max(betadot)])
grid on
title('Phase plot','FontSize',18)
xlabel('\beta [rad]','FontSize',16)
ylabel('d\beta/dt [rad/s]','FontSize',16)
hold on
for i = 1:length(beta)
    if i==1
        plot(beta(i),betadot(i),'-o','MarkerFaceColor','g','MarkerSize',12)
    elseif i==length(beta)
        plot(beta(i),betadot(i),'-o','MarkerFaceColor','r','MarkerSize',12)
    else
        plot(beta(i),betadot(i),'-o','MarkerFaceColor','y')
    end
    drawnow
    pause(.1)
end
plot(beta,betadot,'-')

% save('ArmFlexD','D')
% save('ArmFlexS','S')