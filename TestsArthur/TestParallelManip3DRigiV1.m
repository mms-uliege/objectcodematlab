%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test parallel manipulator
clear all
close all

finaltime = 1.5;
timestepsize = 0.01;
t_i = 0.2;
t_f = 1.2;

a1 = 0.02;
b1 = 0.02;
l1 = 1;
a2 = 0.005;
b2 = 0.005;
l2 = l1;

%% Creating nodes
sideTriUp = 1; % side length of the upper triangle
sideTriLow = 0.2; % side length of lower triangle
angle = 45*pi/180;
nodes = [1 sideTriUp/(2*cos(pi/6)) 0 0; % upper triangle
         2 -sideTriUp*tan(pi/6)/2 -sideTriUp/2 0;
         3 -sideTriUp*tan(pi/6)/2 sideTriUp/2 0;
         4 sideTriUp/(2*cos(pi/6)) 0 0;
         5 -sideTriUp*tan(pi/6)/2 -sideTriUp/2 0;
         6 -sideTriUp*tan(pi/6)/2 sideTriUp/2 0;
         7 sideTriLow/(2*cos(pi/6)) 0 -1; % lower triangle
         8 -sideTriLow*tan(pi/6)/2 -sideTriLow/2 -1;
         9 -sideTriLow*tan(pi/6)/2 sideTriLow/2 -1;
         10 sideTriLow/(2*cos(pi/6)) 0 -1;
         11 -sideTriLow*tan(pi/6)/2 -sideTriLow/2 -1;
         12 -sideTriLow*tan(pi/6)/2 sideTriLow/2 -1;
         13 0 0 -1];

 
%% Rigid Model
m_end = 0.5; % end efector load [kg]
b = sideTriLow/2;
h = sideTriLow*sin(pi/3);
IxxR = 0.1;
IyyR = 0.1;
IzzR = 0.05;

count = 1;

elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [1 2 3];
elements{count}.mass = 0; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([0 0 0]);

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1) nodes(end-3:end-1,1)'];
elements{count}.mass = m_end; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR IyyR IzzR]);
% elements{count}.g = [0 0 -9.81];

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 4];
elements{count}.A = [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 5];
elements{count}.A = [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [3 6];
elements{count}.A = [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [7 10];
elements{count}.A = [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [8 11];
elements{count}.A = [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [9 12];
elements{count}.A = [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1]';

prism1 = (nodes(4,2:4)-nodes(7,2:4))/norm(nodes(4,2:4)-nodes(7,2:4));
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [4 7];
elements{count}.A = [prism1 0 0 0]';

prism2 = (nodes(5,2:4)-nodes(8,2:4))/norm(nodes(5,2:4)-nodes(8,2:4));
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [5 8];
elements{count}.A = [prism2 0 0 0]';

prism3 = (nodes(6,2:4)-nodes(9,2:4))/norm(nodes(6,2:4)-nodes(9,2:4));
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [6 9];
elements{count}.A = [prism3 0 0 0]';

% Trajectory

x_end = nodes(end,2);
y_end = sideTriLow;
r = y_end/2;

timeVector = 0:timestepsize:finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'cos',t_i,t_f);
trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
trajz = nodes(end,4)*ones(size(timeVector));

count = size(elements,2)+1;
elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajy;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elements{count}.elements = [count-3 count-2 count-1];
elements{count}.active = 1;

% Boundary Condition
BC = [1 2 3];

% Solving
Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.0;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

u1 = Model.listElementVariables{end}.value(1,:);
u2 = Model.listElementVariables{end}.value(2,:);
u3 = Model.listElementVariables{end}.value(3,:);

%% Plots

timeSteps = D.parameters.time;
timeLoc = 1:length(D.parameters.time);

endNode = nodes(end,1);
figure
hold on
plot3(Model.listNodes{endNode}.position(1,timeLoc),Model.listNodes{endNode}.position(2,timeLoc),Model.listNodes{endNode}.position(3,timeLoc), 'Linewidth',3)
plot3(trajx,trajy,trajz, 'Linewidth',1, 'Color','r')
grid on

figure
hold on
plot(timeSteps,Model.listNodes{endNode}.position(1,timeLoc),timeSteps,Model.listNodes{endNode}.position(2,timeLoc),timeSteps,Model.listNodes{endNode}.position(3,timeLoc))
plot(timeSteps,trajx,'--',timeSteps,trajy,'--',timeSteps,trajz,'--')
legend('X', 'Y', 'Z','Xd','Yd','Zd')
grid on

figure
hold on
plot(timeSteps,u1,timeSteps,u2,timeSteps,u3,'Linewidth',2)
% plot(uRigi.time,uRigi.u1,':',uRigi.time,uRigi.u2,':',uRigi.time,uRigi.u3,':','Linewidth',2)
% plot(timeSteps,u1_init(timeLoc),':',timeSteps,u2_init(timeLoc),':',timeSteps,u3_init(timeLoc),':','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Commands (N)','Fontsize',16)
% legend('u1','u2','u3','u1_{rigid}','u2_{rigid}','u3_{rigid}','Location', 'Best')
legend('u1','u2','u3','Location', 'Best')
title('Commands of a parallel Robot','Fontsize',18)
grid on

joint1 = Model.listElementVariables{7};
joint2 = Model.listElementVariables{8};
joint3 = Model.listElementVariables{9};

figure
hold on
plot(timeSteps,joint1.relCoo(timeLoc),timeSteps,joint2.relCoo(timeLoc),timeSteps,joint3.relCoo(timeLoc),'Linewidth',2)
% plot(D.parameters.time,joint1_init.relCoo,'--',D.parameters.time,joint2_init.relCoo,'--',D.parameters.time,joint3_init.relCoo,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints position (m)','Fontsize',16)
% legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
legend('Joint 1','Joint 2','Joint 3','Location', 'Best')
grid on


