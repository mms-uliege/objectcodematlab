%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Test of flexible cart with beam, in direct dynamic

clear

finaltime = 2;
timestepsize = 0.01;
t_i = 0.2;
t_f = 1.7;


%% Creation of the model

% Nodes
nodes = [1 -1 0 0;
         2 -1 0 0;
         3 -1 0 0;
         4 -1.6614 -0.75 0;
         5 -1.6614 -0.75 0;
         6 -1 -1.5 0];
     
nElem1 = 4;
Start1 = 3;
End1 = 4;
nodes = createInterNodes(nodes,nElem1,Start1,End1);
     
nElem2 = 4;
Start2 = End1+nElem1;
End2 = Start2+1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);

%% Flexible Model
a = 0.005;
b = a;
l = 1;
rho = 8400;
E = 120e9; nu = 0.33; G = E/(2*(1+nu));
A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;
KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

count = 1;
elementsFlex = [];
elementsFlex{count}.type = 'RigidBodyElement';
elementsFlex{count}.nodes = [2];
elementsFlex{count}.mass = 3;
% elementsFlex{count}.J = eye(3);

type = 'FlexibleBeamElement';
% count = size(elementsFlex,2);
for i = 1:nElem1
    elementsFlex{count+i}.type = type;
    elementsFlex{count+i}.nodes = [nodes(Start1+i-1,1) nodes(Start1+i,1)];
    elementsFlex{count+i}.KCS = KCS;
    elementsFlex{count+i}.MCS = MCS;
end

count = size(elementsFlex,2);
for i = 1:nElem2
    elementsFlex{count+i}.type = type;
    elementsFlex{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
    elementsFlex{count+i}.KCS = KCS;
    elementsFlex{count+i}.MCS = MCS;
end

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [1 2];
elementsFlex{count}.A = [1 0 0 0 0 0]';
count = count+1;

elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [2 3];
elementsFlex{count}.A = [0 0 0 0 0 1]';
count = count+1;

elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [Start2-1 Start2];
elementsFlex{count}.A = [0 0 0 0 0 1]';
count = count+1;

% Trajectory
cart_end = 1;
cart_i = -1;

x_end = 1;
x_i = -1;

y_end = -1.5;
y_i = -1.5;

r = 1;

timeVector = 0:timestepsize:finaltime;
trajcart = halfCircleTraj(cart_i,cart_end,r,timeVector,'lin',t_i,t_f);
trajx = halfCircleTraj(x_i,x_end,r,timeVector,'sin',t_i,t_f);
trajy = halfCircleTraj(y_i,y_end,r,timeVector,'cos',t_i,t_f);

t = timeVector;

load('uCartRigid')
u1_init = interp1(uCartRigid.time,uCartRigid.u1_init,t,'pchip');
u2_init = interp1(uCartRigid.time,uCartRigid.u2_init,t,'pchip');
u3_init = interp1(uCartRigid.time,uCartRigid.u3_init,t,'pchip');
% u1 = u1_init;
% u2 = u2_init;
% u3 = u3_init;

load('uCartBeam')
u1 = interp1(uCartBeam.time,uCartBeam.u1,t,'linear');
u2 = interp1(uCartBeam.time,uCartBeam.u2,t,'linear');
u3 = interp1(uCartBeam.time,uCartBeam.u3,t,'linear');
% u1 = smooth(u1);
% u2 = smooth(u2);
% u3 = smooth(u3);

elementsFlex{count}.type = 'ForceInKinematicConstraint';
elementsFlex{count}.elements = [count-3];
elementsFlex{count}.f = u1;
count = count+1;

elementsFlex{count}.type = 'ForceInKinematicConstraint';
elementsFlex{count}.elements = [count-3];
elementsFlex{count}.f = u2;
count = count+1;

elementsFlex{count}.type = 'ForceInKinematicConstraint';
elementsFlex{count}.elements = [count-3];
elementsFlex{count}.f = u3;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elementsFlex);
Model.defineBC(BC);

%% Dynamic integration

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.9;
% D.parameters.relTolRes = 1e-12;
% D.parameters.scaling = 1e6;
D.runIntegration();

%% Plots
timeSteps = D.parameters.time;

figure
hold on
plot(Model.listNodes{2}.position(1,:),Model.listNodes{2}.position(2,:),Model.listNodes{end}.position(1,:),Model.listNodes{end}.position(2,:), 'Linewidth',3)
plot(trajcart,zeros(size(trajcart)),trajx,trajy, 'Linewidth',1, 'Color','r')
grid on


figure
hold on
plot(timeSteps,Model.listNodes{2}.position(1,:),timeSteps,Model.listNodes{end}.position(1,:),timeSteps,Model.listNodes{end}.position(2,:))
plot(timeSteps,trajcart,'--',timeSteps,trajx,'--',timeSteps,trajy,'--')
legend('cart', 'X', 'Y','cartd','Xd','Yd')
grid on

figure
hold on
plot(timeSteps,u1,timeSteps,u2,timeSteps,u3,'Linewidth',3)
plot(timeSteps,u1_init(:),':',timeSteps,u2_init(:),':',timeSteps,u3_init(:),':','Linewidth',3)
xlabel('Time (s)','Fontsize',16)
ylabel('Commands','Fontsize',16)
legend('u1','u2','u3','u1_{rigid}','u2_{rigid}','u3_{rigid}','Location','Best')
title('Commands of a flexible cart system','Fontsize',18)
grid on

% Error Plot
xcart = Model.listNodes{2}.position(1,:);
x = Model.listNodes{end}.position(1,:);
y = Model.listNodes{end}.position(2,:);

RelError = zeros(size(timeSteps));
for i = 1:length(RelError)
    RelError(i) = 100*(norm([trajx(i)-x(i) trajy(i)-y(i)]))/norm([trajx(i) trajy(i)]);
end
figure
plot(timeSteps,RelError)
title('Relative error of the direct dynamic trajectory','Fontsize',13)
xlabel('time')
ylabel('Relative Error (%)')
grid on

MeanRelError = mean(RelError)
MaxRelError = max(RelError)


