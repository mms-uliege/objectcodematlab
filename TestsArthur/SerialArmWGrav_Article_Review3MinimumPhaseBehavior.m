%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test inverse dynamics of a serial manipulator (2 links, 3 dof).
% considering gravity, with a initial guess that is the deformed state.
% Initial guess computed staticaly and nor rigid model to start with
% With possibility to add some PD feedback on joint position level

% Model used for Multibody system Dynamics journal with some tests to
% improve direct dynamics result

clear all
% close all

listPolesAnalysis = [];
% list = [0.1 0.3 0.5 0.7 0.9 1 1.2 1.5 1.7 1.9 2 2.1 2.5 3 5];
% list = [0.4 0.5 0.6 0.7 0.8 0.9 1];
list = [0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1];
% list = [1];

for li = list

clear elements elementsF ModelR ModelF D S
    
    
computeOpti = true;
% computeDirectDyn = true;
computeDirectDyn = false;

finaltime = 1.5;
timestepsize = 0.1;
t_i = 0.2;
t_f = finaltime-0.2;
timeShift = 0;
    
% kp1 = 10;
% kd1 = 1;
% kp2 = 10;
% kd2 = 1;
% kp3 = 10;
% kd3 = 1;
kp1 = 0;
kd1 = 0;
kp2 = 0;
kd2 = 0;
kp3 = 0;
kd3 = 0;

hypForPreComp = 'notConstant';
hypForComp = 'notConstant';
% hypForComp = 'constant';

a1 = 0.05;
% a1 = 0.025;
b1 = a1;
l1 = 1;
l2 = l1; 
e1 = 0.01;
rapport = a1/e1;
a2 = 0.0075;
% a2 = 0.02;
b2 = a2;
e2 = a2/rapport;
a2In = a2-2*e2;
b2In = b2-2*e2;
a1In = a1-2*e1; % inner length
b1In = b1-2*e1;

alpha = 0*0.0001; % Damping proportionnal to mass
beta = 0*0.01; % Damping proportionnal to stiffness
rho_num = 0.0;
scaling = 1e6;
tol = 1e-6;
% listM = [1 0 0];
% listKt = [1 0 0];
% listCt = [1 0 0];
% listPhiq = [1 0 0];
% listM = [1 1 1];
% listKt = [1 1 1];
% listCt = [1 1 1];
% listPhiq = [1 1 1];

nPass = 0;

grav = [0 0 -9.81];
% grav = [0 0 0];
% t = 0:timestepsize:finaltime;
% grav = [zeros(1,length(t)); zeros(1,length(t)); [0 0 0 -9.81*ones(1,length(t)-3)]]';

m_end = 0.1; % end-effector mass

%% Creating nodes
angle = 45*pi/180;
nodes = [1 0 0 0;
         2 0 0 0;
         3 0 0 0;
         4 l1*cos(angle) 0 l1*sin(angle);
         5 l1*cos(angle) 0 l1*sin(angle);
         6 l1*cos(angle)+l2*cos(angle) 0 0];
     
nElem1 = 4;
Start1 = 3;
End1 = 4;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

nElem2 = 4;
Start2 = End1+nElem1;
End2 = Start2+1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);
 
%% Rigid Model

rho = 2700;
m1 = (a1*b1-a1In*b1In)*l1*rho;
% IxxR1 = m1*(a1^2+b1^2)/12;
IxxR1 = m1*(a1^2+b1^2-a1In^2-b1In^2)/12;
IyyR1 = m1*(a1^2+l1^2)/12;
IzzR1 = m1*(b1^2+l1^2)/12;

m2 = (a2*b2-a2In*b2In)*l2*rho;
IxxR2 = m2*(a2^2+b2^2-a2In^2-b2In^2)/12;
IyyR2 = m2*(a2^2+l2^2)/12;
IzzR2 = m2*(b2^2+l2^2)/12;

E = 70e9; nu = 0.3; G = E/(2*(1+nu));
A1 = a1*b1-a1In*b1In; Ixx1 = (a1*b1*(a1^2+b1^2)-a1In*b1In*(a1In^2+b1In^2))/12; Iyy1 = (b1*a1^3-b1In*a1In^3)/12;Izz1 = (a1*b1^3-a1In*b1In^3)/12;
KCS1 = (30-29*li)*diag([E*A1 G*A1 G*A1 G*Ixx1 E*Iyy1 E*Izz1]);
MCS1 = diag(rho*[A1 A1 A1 Ixx1 Iyy1 Izz1]);

A2 = a2*b2-a2In*b2In; Ixx2 = (a2*b2*(a2^2+b2^2)-a2In*b2In*(a2In^2+b2In^2))/12; Iyy2 = (b2*a2^3-b2In*a2In^3)/12;Izz2 = (a2*b2^3-a2In*b2In^3)/12;
KCS2 = (1+li*29)*diag([E*A2 G*A2 G*A2 G*Ixx2 E*Iyy2 E*Izz2]);
MCS2 = diag(rho*[A2 A2 A2 Ixx2 Iyy2 Izz2]);

% count = 0;
% type = 'FlexibleBeamElement';
% for i = 1:nElem1
%     elements{count+i}.type = type;
%     elements{count+i}.nodes = [nodes(Start1+i-1,1) nodes(Start1+i,1)];
%     elements{count+i}.KCS = KCS1;
%     elements{count+i}.MCS = MCS1;
%     elements{count+i}.yAxis = [0 0 1];
%     elements{count+i}.alpha = alpha;
%     elements{count+i}.beta = beta;
%     elements{count+i}.g = grav;
% end
count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elements{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR1 IyyR1 IzzR1]);
elements{count}.g = grav;

% type = 'FlexibleBeamElement';
% count = size(elements,2);
% for i = 1:nElem2
%     elements{count+i}.type = type;
%     elements{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
%     elements{count+i}.KCS = KCS2;
%     elements{count+i}.MCS = MCS2;
%     elements{count+i}.yAxis = [0 0 1];
%     elements{count+i}.alpha = alpha;
%     elements{count+i}.beta = beta;
%     elements{count+i}.g = grav;
% end
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
elements{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR2 IyyR2 IzzR2]);
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = m_end;
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.A = [0 0 0 0 1 0]';
count = count+1;

% Trajectory

y_end = l2;
z_end = nodes(end,4);
r = y_end/2;

timeVector = 0:timestepsize:finaltime;
trajx = nodes(end,2)*ones(size(timeVector));
trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);
% trajy = nodes(end,3)*ones(size(timeVector));
% trajz = nodes(end,4)*ones(size(timeVector));


elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajy;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elements{count}.elements = [count-3 count-2 count-1];
elements{count}.active = 1;

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
% D = StaticIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
D.parameters.relTolRes = tol;
D.parameters.scaling = scaling;
D.runIntegration();

%% Flexible Model
if computeOpti == true
% count = 1;
% elementsF{count}.type = 'RigidBodyElement';
% elementsF{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
% elementsF{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
% elementsF{count}.J = diag([IxxR1 IyyR1 IzzR1]);
% elementsF{count}.g = grav;
% 
% type = 'FlexibleBeamElement';
% count = size(elementsF,2);
% for i = 1:nElem2
%     elementsF{count+i}.type = type;
%     elementsF{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
%     elementsF{count+i}.KCS = KCS2;
%     elementsF{count+i}.MCS = MCS2;
%     elementsF{count+i}.yAxis = [0 0 1];
% %     elementsF{count+i}.listM = listM;
% %     elementsF{count+i}.listCt = listCt;
% %     elementsF{count+i}.listKt = listKt;
% %     elementsF{count+i}.listPhiq = listPhiq;
%     elementsF{count+i}.alpha = alpha;
%     elementsF{count+i}.beta = beta;
%     elementsF{count+i}.g = grav;
% end
count = 0;
type = 'FlexibleBeamElement';
for i = 1:nElem1
    elementsF{count+i}.type = type;
    elementsF{count+i}.nodes = [nodes(Start1+i-1,1) nodes(Start1+i,1)];
    elementsF{count+i}.KCS = KCS1;
    elementsF{count+i}.MCS = MCS1;
    elementsF{count+i}.yAxis = [0 0 1];
%     elementsF{count+i}.listM = listM;
%     elementsF{count+i}.listCt = listCt;
%     elementsF{count+i}.listKt = listKt;
%     elementsF{count+i}.listPhiq = listPhiq;
    elementsF{count+i}.alpha = alpha;
    elementsF{count+i}.beta = beta;
    elementsF{count+i}.g = grav;
end

% count = size(elementsF,2)+1;
% elementsF{count}.type = 'RigidBodyElement';
% elementsF{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
% elementsF{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
% elementsF{count}.J = diag([IxxR2 IyyR2 IzzR2]);
% elementsF{count}.g = grav;
type = 'FlexibleBeamElement';
count = size(elementsF,2);
for i = 1:nElem2
    elementsF{count+i}.type = type;
    elementsF{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
    elementsF{count+i}.KCS = KCS2;
    elementsF{count+i}.MCS = MCS2;
    elementsF{count+i}.yAxis = [0 0 1];
%     elementsF{count+i}.listM = listM;
%     elementsF{count+i}.listCt = listCt;
%     elementsF{count+i}.listKt = listKt;
%     elementsF{count+i}.listPhiq = listPhiq;
    elementsF{count+i}.alpha = alpha;
    elementsF{count+i}.beta = beta;
    elementsF{count+i}.g = grav;
end

count = size(elementsF,2)+1;
elementsF{count}.type = 'RigidBodyElement';
elementsF{count}.nodes = [nodes(end,1)];
elementsF{count}.mass = m_end;
elementsF{count}.g = grav;

count = size(elementsF,2)+1;
elementsF{count}.type = 'KinematicConstraint';
elementsF{count}.nodes = [1 2];
elementsF{count}.A = [0 0 0 0 0 1]';

count = size(elementsF,2)+1;
elementsF{count}.type = 'KinematicConstraint';
elementsF{count}.nodes = [2 3];
elementsF{count}.A = [0 0 0 0 1 0]';

count = size(elementsF,2)+1;
elementsF{count}.type = 'KinematicConstraint';
elementsF{count}.nodes = [Start2-1 Start2];
elementsF{count}.A = [0 0 0 0 1 0]';
count = count+1;

% Trajectory

elementsF{count}.type = 'TrajectoryConstraint';
elementsF{count}.nodes = [nodes(end,1)];
elementsF{count}.T = [trajx;...
                     trajy;...
                     trajz];
elementsF{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elementsF{count}.elements = [count-3 count-2 count-1];
elementsF{count}.active = 1;

% Solving
npts = finaltime/timestepsize + 1;
ModelF = FEModel();
ModelF.CreateFEModel(nodes,elementsF);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from rigid solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
        ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

% Start second full optimization with newly computed initial guess
tic
S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = rho_num;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.parameters.scaling = scaling;
S.linConst = false;
S.ConstIter = hypForComp;
S.nPass = nPass;
S.parameters.relTolRes = tol;
% analys = S.runPoleAnalysisOpti(D)
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])

%% Plots
anlFlex1 = poleAnalysisOpti(S,1,0);
listPolesAnalysis = [listPolesAnalysis poleAnalysisOpti(S,1,0)];
disp(['usLowFreq with scaling at ', num2str(li), ', is ', num2str(listPolesAnalysis(end).usLowFreq),' Hz.'])

% for i = 1:150
% anl = poleAnalysisOpti(S,i,0);
% usLowF(i) = anl.usLowFreq;
% end
% figure
% plot(usLowF)

end

end

clear D nodes elements elementsF S ModelR ModelF


nodes = [1 0 0 0;
         2 0 0 0;
         3 0 0 0;
         4 l1*cos(angle) 0 l1*sin(angle);
         5 l1*cos(angle) 0 l1*sin(angle);
         6 l1*cos(angle)+l2*cos(angle) 0 0];
     
nElem1 = 4;
Start1 = 3;
End1 = 4;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

nElem2 = 2;
Start2 = End1+nElem1;
End2 = Start2+1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);
 
%% Rigid Model

rho = 2700;
m1 = (a1*b1-a1In*b1In)*l1*rho;
% IxxR1 = m1*(a1^2+b1^2)/12;
IxxR1 = m1*(a1^2+b1^2-a1In^2-b1In^2)/12;
IyyR1 = m1*(a1^2+l1^2)/12;
IzzR1 = m1*(b1^2+l1^2)/12;

m2 = (a2*b2-a2In*b2In)*l2*rho;
IxxR2 = m2*(a2^2+b2^2-a2In^2-b2In^2)/12;
IyyR2 = m2*(a2^2+l2^2)/12;
IzzR2 = m2*(b2^2+l2^2)/12;

E = 70e9; nu = 0.3; G = E/(2*(1+nu));
A1 = a1*b1-a1In*b1In; Ixx1 = (a1*b1*(a1^2+b1^2)-a1In*b1In*(a1In^2+b1In^2))/12; Iyy1 = (b1*a1^3-b1In*a1In^3)/12;Izz1 = (a1*b1^3-a1In*b1In^3)/12;
KCS1 = diag([E*A1 G*A1 G*A1 G*Ixx1 E*Iyy1 E*Izz1]);
MCS1 = diag(rho*[A1 A1 A1 Ixx1 Iyy1 Izz1]);

A2 = a2*b2-a2In*b2In; Ixx2 = (a2*b2*(a2^2+b2^2)-a2In*b2In*(a2In^2+b2In^2))/12; Iyy2 = (b2*a2^3-b2In*a2In^3)/12;Izz2 = (a2*b2^3-a2In*b2In^3)/12;
KCS2 = diag([E*A2 G*A2 G*A2 G*Ixx2 E*Iyy2 E*Izz2]);
MCS2 = diag(rho*[A2 A2 A2 Ixx2 Iyy2 Izz2]);

count = 0;
type = 'FlexibleBeamElement';
for i = 1:nElem1
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start1+i-1,1) nodes(Start1+i,1)];
    elements{count+i}.KCS = KCS1;
    elements{count+i}.MCS = MCS1;
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;
    elements{count+i}.g = grav;
end
% count = 1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
% elements{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
% elements{count}.J = diag([IxxR1 IyyR1 IzzR1]);
% elements{count}.g = grav;

% type = 'FlexibleBeamElement';
% count = size(elements,2);
% for i = 1:nElem2
%     elements{count+i}.type = type;
%     elements{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
%     elements{count+i}.KCS = KCS2;
%     elements{count+i}.MCS = MCS2;
%     elements{count+i}.yAxis = [0 0 1];
%     elements{count+i}.alpha = alpha;
%     elements{count+i}.beta = beta;
%     elements{count+i}.g = grav;
% end
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
elements{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR2 IyyR2 IzzR2]);
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = m_end;
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.A = [0 0 0 0 1 0]';
count = count+1;

% Trajectory

y_end = l2;
z_end = nodes(end,4);
r = y_end/2;

timeVector = 0:timestepsize:finaltime;
trajx = nodes(end,2)*ones(size(timeVector));
trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);
% trajy = nodes(end,3)*ones(size(timeVector));
% trajz = nodes(end,4)*ones(size(timeVector));


elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajy;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elements{count}.elements = [count-3 count-2 count-1];
elements{count}.active = 1;

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
% D = StaticIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
D.parameters.relTolRes = tol;
D.parameters.scaling = scaling;
D.runIntegration();

t = find(D.parameters.time==timeShift,1);

%% Flexible Model
if computeOpti == true
% count = 1;
% elementsF{count}.type = 'RigidBodyElement';
% elementsF{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
% elementsF{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
% elementsF{count}.J = diag([IxxR1 IyyR1 IzzR1]);
% elementsF{count}.g = grav;
% 
% type = 'FlexibleBeamElement';
% count = size(elementsF,2);
% for i = 1:nElem2
%     elementsF{count+i}.type = type;
%     elementsF{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
%     elementsF{count+i}.KCS = KCS2;
%     elementsF{count+i}.MCS = MCS2;
%     elementsF{count+i}.yAxis = [0 0 1];
%     elementsF{count+i}.alpha = alpha;
%     elementsF{count+i}.beta = beta;
%     elementsF{count+i}.g = grav;
% end
count = 0;
type = 'FlexibleBeamElement';
for i = 1:nElem1
    elementsF{count+i}.type = type;
    elementsF{count+i}.nodes = [nodes(Start1+i-1,1) nodes(Start1+i,1)];
    elementsF{count+i}.KCS = KCS1;
    elementsF{count+i}.MCS = MCS1;
    elementsF{count+i}.yAxis = [0 0 1];
    elementsF{count+i}.alpha = alpha;
    elementsF{count+i}.beta = beta;
    elementsF{count+i}.g = grav;
end

count = size(elementsF,2)+1;
elementsF{count}.type = 'RigidBodyElement';
elementsF{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
elementsF{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
elementsF{count}.J = diag([IxxR2 IyyR2 IzzR2]);
elementsF{count}.g = grav;

count = size(elementsF,2)+1;
elementsF{count}.type = 'RigidBodyElement';
elementsF{count}.nodes = [nodes(end,1)];
elementsF{count}.mass = m_end;
elementsF{count}.g = grav;

count = size(elementsF,2)+1;
elementsF{count}.type = 'KinematicConstraint';
elementsF{count}.nodes = [1 2];
elementsF{count}.A = [0 0 0 0 0 1]';

count = size(elementsF,2)+1;
elementsF{count}.type = 'KinematicConstraint';
elementsF{count}.nodes = [2 3];
elementsF{count}.A = [0 0 0 0 1 0]';

count = size(elementsF,2)+1;
elementsF{count}.type = 'KinematicConstraint';
elementsF{count}.nodes = [Start2-1 Start2];
elementsF{count}.A = [0 0 0 0 1 0]';
count = count+1;

% Trajectory

elementsF{count}.type = 'TrajectoryConstraint';
elementsF{count}.nodes = [nodes(end,1)];
elementsF{count}.T = [trajx;...
                     trajy;...
                     trajz];
elementsF{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elementsF{count}.elements = [count-3 count-2 count-1];
elementsF{count}.active = 1;

% Solving
npts = finaltime/timestepsize + 1;
ModelF = FEModel();
ModelF.CreateFEModel(nodes,elementsF);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from rigid solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
        ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

% Start second full optimization with newly computed initial guess
tic
S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = rho_num;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.parameters.scaling = scaling;
S.linConst = false;
S.ConstIter = hypForComp;
S.nPass = nPass;
S.parameters.relTolRes = tol;
% analys = S.runPoleAnalysisOpti(D)
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])

%% Plots

anlFlex2 = poleAnalysisOpti(S,1,0);
listPolesAnalysis = [listPolesAnalysis poleAnalysisOpti(S,1,0)];




figure
hold on
% Flexible 1st link
% plot(real(anlFlex1.poles(anlFlex1.indexUnstable)),imag(anlFlex1.poles(anlFlex1.indexUnstable)),'d','MarkerFaceColor','r','MarkerEdgeColor','r')
% plot(real(anlFlex1.poles(anlFlex1.indexStable)),imag(anlFlex1.poles(anlFlex1.indexStable)),'o','MarkerFaceColor','b','MarkerEdgeColor','b')
% Flexible 2nd link
plot(real(anlFlex2.poles(anlFlex2.indexUnstable)),imag(anlFlex2.poles(anlFlex2.indexUnstable)),'d','MarkerFaceColor','r','MarkerEdgeColor','r')
plot(real(anlFlex2.poles(anlFlex2.indexStable)),imag(anlFlex2.poles(anlFlex2.indexStable)),'o','MarkerFaceColor','b','MarkerEdgeColor','b')
xlabel('Real part','FontSize',16)
ylabel('Imaginary part','FontSize',16)
title('Poles of the system','FontSize',18)
legend('Unstable','Stable', 'Location', 'Best')
grid on
axis([-700 700 -700 700])
% axis([-1000 1000 -1000 1000])
viscircles([0 0],100*2*pi,'Color','k');

figure
hold on
for c = 1:length(listPolesAnalysis)
    plot(real(listPolesAnalysis(c).poles(listPolesAnalysis(c).indexUnstable)),imag(listPolesAnalysis(c).poles(listPolesAnalysis(c).indexUnstable)),'d','MarkerFaceColor','r','MarkerEdgeColor','r')
    plot(real(listPolesAnalysis(c).poles(listPolesAnalysis(c).indexStable)),imag(listPolesAnalysis(c).poles(listPolesAnalysis(c).indexStable)),'o','MarkerFaceColor','c','MarkerEdgeColor','c')
end
xlabel('Real part')
ylabel('Imaginary part')
title('Poles of the system')
grid on
% axis([-400 400 -11 11])
axis([-5*1e6 5*1e6 -5*1e6 5*1e6])

figure
for c = 1:length(listPolesAnalysis)
    hold off
    plot(real(listPolesAnalysis(c).poles(listPolesAnalysis(c).indexUnstable)),imag(listPolesAnalysis(c).poles(listPolesAnalysis(c).indexUnstable)),'d','MarkerFaceColor','r','MarkerEdgeColor','r')
    hold on
    plot(real(listPolesAnalysis(c).poles(listPolesAnalysis(c).indexStable)),imag(listPolesAnalysis(c).poles(listPolesAnalysis(c).indexStable)),'o','MarkerFaceColor','c','MarkerEdgeColor','c')
    xlabel('Real part')
    ylabel('Imaginary part')
    title('Poles of the system')
    grid on
%     axis([-400 400 -11 11])
    axis([-5*1e5 5*1e5 -5*1e5 5*1e5])
%     drawnow
    pause(0.5)
end

figure
hold on
for c = 1:length(list)
    plot(list(c), listPolesAnalysis(c).usLowFreq,'ko')
end
title('First unstable eigenfrequency')
ylabel('Frequency [Hz]')
xlabel('Parameter scaling value')
grid on

figure
hold on
for c = 1:length(list)
    plot(list(c), listPolesAnalysis(c).usLowFreq,'ko')
end
plot(list(c)+1, listPolesAnalysis(c+1).usLowFreq,'ko')
title('First unstable eigenfrequency')
ylabel('Frequency [Hz]')
xlabel('Parameter scaling value')
grid on


end