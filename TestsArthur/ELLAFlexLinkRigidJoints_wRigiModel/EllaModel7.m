%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test to try to get a control of 3 joints of the Ella robot of JKU Linz. WITH
% Visc JOINTS (ALL) and a dynamic computation at the beginning
% With line trajectory as for the actual Ella robot USING Cartesian TRAJECTORY.

% parameters comming from https://robfile.mechatronik.uni-linz.ac.at/doku.php?id=start:robots:ella:mechsetup
% close all
clear elements

finaltime = 2.05; % 2 seconds
timestepsize = 0.01;

npts = round(finaltime/timestepsize + 1); % number of points for optimization
hypForPreComp = 'notConstant';
hypForComp = 'constant';

% kp1 = 100;
% kd1 = 5;
% kp2 = 100;
% kd2 = 5;
% kp3 = 100;
% kd3 = 5;
kp1 = 0;
kd1 = 0;
kp2 = 0;
kd2 = 0;
kp3 = 0;
kd3 = 0;

t_i = 0.8; % time at end of pre-actuation
t_f = finaltime - 0.2; % time at begining of post-actuation
% t_i = 0; % time at end of pre-actuation
% t_f = finaltime; % time at begining of post-actuation

grav = [0 0 -9.81]; % gravity in m/s�
% grav = [0 0 0]; % gravity in m/s�

endPoseInit = [1.4 0.75 -0.5]; % changed in the coordinate we have in our model
angleLink0 = 0.4918; % rad, angle at the base (0.4918)
angleLink1 = -0.1840; % angle of the first joint in rad (0.1840)
angleLink2 = 0.9926; % angle of the second joint in rad (-0.9926)
% angleLink0 = 0; % rad, angle at the base (0) test when robot is facing front
% angleLink1 = -0.1840; % angle of the first joint in rad (0.1840)
% angleLink2 = 0.9926; % angle of the second joint in rad (-0.9926)
% angleLink1 = 45; % angle of the first link relative to the horizon in deg
% angleLink2 = -angleLink1; % angle of the second link relativeley to horizon at tip of link 1f link 1

gear1 = 100;
gear2 = 100;
gear3 = 160;

% Passive third joint damping and stiffness
damp1 = 1.95*1e-3*gear1^2;
damp2 = 2.2*1e-3*gear2^2;
damp3 = 4.76*1e-4*gear3^2;
% stif1 = 1.2*1e5;
% stif2 = 1.2*1e5;
% stif3 = 2.9*1e4;
coul1 = 0.2*gear1;  
coul2 = 0.14*gear2;
coul3 = 0.07*gear3;
% damp1 = 0;
% damp2 = 0;
% damp3 = 0;
% coul1 = 0;
% coul2 = 0;
% coul3 = 0;
stif1 = 0;
stif2 = 0;
stif3 = 0;

alpha = 0.0001;  % Damping coeff proportional to mass
beta = 0.02; % Damping coeff proportional to stiffness

rho_num = 0.01;
listM = [1 0 0];
listKt = [1 0 0];
listCt = [1 0 0];
listPhiq = [1 0 0];
% listM = [1 1 1];
% listKt = [1 1 1];
% listCt = [1 1 1];
% listPhiq = [1 1 1];

link1.E = 210e9; % link modulus in N/m�
link1.l = 0.9595; % link length in m
% link1.w = 0.015; % link width in m
% link1.h = 0.004; % link height in m
link1.rho = 7800; % mass density in kg/m�
link1.A = 3.01e-4; % cross section area of the link in m�
link1.m = link1.l * link1.A * link1.rho; % mass of link in kg
% link1.J = diag([link1.m*(link1.w^2 + link1.h^2)/12 link1.m*(link1.l^2 + link1.h^2)/12 link1.m*(link1.l^2 + link1.w^2)/12]); % inertia in kg*m�
link1.J = diag([5.1800e-4 0.1625 0.1625]); % inertia in kg*m�
link1.nu = 0.27; % poisson coef
link1.G = link1.E/(2*(1+link1.nu)); % compression modulus
% link1.Ixx = (link1.w*link1.h)*(link1.w^2 + link1.h^2)/12; % area moment inertia in case of beam in m^4
% link1.Iyy = (link1.w*link1.h^3)/12; % area moment inertia in case of beam in m^4
% link1.Izz = (link1.w^3*link1.h)/12; % area moment inertia in case of beam in m^4
link1.Ixx = 3.85e-8; % area moment inertia in case of beam in m^4
link1.Iyy = 1.75e-8; % area moment inertia in case of beam in m^4
link1.Izz = 2.1e-8; % area moment inertia in case of beam in m^4
link1.KCS = diag([link1.E*link1.A link1.G*link1.A link1.G*link1.A link1.G*link1.Ixx link1.E*link1.Iyy link1.E*link1.Izz]);
link1.MCS = diag(link1.rho*[link1.A link1.A link1.A link1.Ixx link1.Iyy link1.Izz]);
link1.nodes = [0 0 0;...
               link1.l*cos(angleLink1)*cos(angleLink0) link1.l*cos(angleLink1)*sin(angleLink0) link1.l*sin(-angleLink1)]; % nodes of the body, starting with bottom to top (center of mass in the middle)
link1.nElem = 2; % Number of elements composing the link
% adding necessary nodes
link1.nodes = CreateInterCoord(link1.nodes,link1.nElem,1,2);

link2.E = 210e9; % link modulus in N/m�
link2.l = 0.9340; % link length in m
% link2.w = 0.015; % link width in m
% link2.h = 0.004; % link height in m
link2.rho = 7800; % mass density in kg/m�
link2.A = 2.09e-4; % cross section area of the link in m�
link2.m = link2.l * link2.A * link2.rho; % mass of link in kg
% link2.m = 1.4; % mass of link in kg TESTING
% link2.J = diag([link2.m*(link2.w^2 + link2.h^2)/12 link2.m*(link2.l^2 + link2.h^2)/12 link2.m*(link2.l^2 + link2.w^2)/12]); % inertia in kg*m�
link2.J = diag([2.18e-4 0.08374 0.08374]); % inertia in kg*m�
link2.nu = 0.27; % poisson coef
link2.G = link2.E/(2*(1+link2.nu)); % compression modulus
% link2.Ixx = (link2.w*link2.h)*(link2.w^2 + link2.h^2)/12; % area moment inertia in case of beam in m^4
% link2.Iyy = (link2.w*link2.h^3)/12; % area moment inertia in case of beam in m^4
% link2.Izz = (link2.w^3*link2.h)/12; % area moment inertia in case of beam in m^4
link2.Ixx = 1.76e-8; % area moment inertia in case of beam in m^4
link2.Iyy = 0.75e-8; % area moment inertia in case of beam in m^4
link2.Izz = 1.01e-8; % area moment inertia in case of beam in m^4
link2.KCS = diag([link2.E*link2.A link2.G*link2.A link2.G*link2.A link2.G*link2.Ixx link2.E*link2.Iyy link2.E*link2.Izz]);
link2.MCS = diag(link2.rho*[link2.A link2.A link2.A link2.Ixx link2.Iyy link2.Izz]);
link2.nodes = [link1.nodes(end,1) link1.nodes(end,2) link1.nodes(end,3);...
               (link2.l*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*cos(angleLink0) (link2.l*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*sin(angleLink0) link1.nodes(end,3)-link2.l*sin(angleLink2+angleLink1)]; % nodes of the body, starting with bottom to top (center of mass in the middle)
link2.nElem = 2; % Number of elements composing the link
% adding necessary nodes
link2.nodes = CreateInterCoord(link2.nodes,link2.nElem,1,2);

% put all nodes' coord into one matrix only     
nodes = [link1.nodes(1,:);...
         link1.nodes(1,:);...
         link1.nodes;...
         link2.nodes]; % List of nodes to build the model

% number all nodes properly so they can be used by FEMmodel
nodes = [1:size(nodes,1);nodes']';
     
% creating first flexible link with link.nElem number of elements (rotating aroud z)
count = 1;
Start1 = 3; % first node of the 1st link
% creating first rigid body (rotating aroud z)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+link1.nElem/2 Start1+[0:link1.nElem/2-1] Start1+link1.nElem/2+1+[0:link1.nElem/2-1]];
elements{count}.mass = link1.m;
% elements{count}.J = link1.J+diag([link1.m*0.475^2 0 0]);
elements{count}.J = link1.J;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating first flexible link with link.nElem number of elements (rotating aroud y)
count = size(elements,2)+1;
Start2 = Start1+link1.nElem+1; % first node of the 2nd link
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2+link2.nElem/2 Start2+[0:link2.nElem/2-1] Start2+link2.nElem/2+1+[0:link2.nElem/2-1]];
elements{count}.mass = link2.m;
% elements{count}.J = link2.J+diag([link2.m*0.505^2 0 0]);
elements{count}.J = link2.J;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body joint(rotating aroud Z) Base
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [2];
% elements{count}.mass = 0.3;
elements{count}.J = diag([0 0 0.3+((0.33+0.011+1.69)*10^-4)*gear1^2]);
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body joint(rotating aroud y) inertia of motor 2 (shoulder)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [3];
% elements{count}.mass = 0.3;
elements{count}.J = diag([0 ((0.33+0.011+1.69)*10^-4)*gear2^2 0]);
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body joint(rotating aroud y) Elbow
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2-1];
elements{count}.mass = 3.3+3;
elements{count}.J = diag([0.0299 0.0177+((0.33+0.011+0.193)*10^-4)*gear3^2 0.0299]);
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body end-effector
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = 2.1;
elements{count}.J = diag([0.0021 0.0044 0.0033]);
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% Creating the kinematic constraints
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.k = stif1;
elements{count}.d = damp1;
elements{count}.coulomb = coul1;
elements{count}.A = [0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.k = stif2;
elements{count}.d = damp2;
elements{count}.coulomb = coul2;
% elements{count}.A = [0 0 0 0 1 0]';
elements{count}.A = [0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]'/norm([0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]);
% elements{count}.A = [0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.k = stif3;
elements{count}.d = damp3;
elements{count}.coulomb = coul3;
% elements{count}.A = [0 0 0 0 1 0]';
elements{count}.A = [0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]'/norm([0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]);
% elements{count}.A = [0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]';

% Trajectory

r = 0.30;
y_end = -0.75;
z_end = 1;
% x_end = nodes(end,2);
x_end = 1.4;

timeVector = 0:timestepsize:finaltime;
% trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'lin',t_i,t_f);
% trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'lin',t_i,t_f);
% trajx = nodes(end,2)*ones(size(timeVector));
trajy = lineTraj(nodes(end,3),y_end,timeVector,t_i,t_f);
trajz = lineTraj(nodes(end,4),z_end,timeVector,t_i,t_f);
% trajx = x_end*ones(size(timeVector));
trajx = lineTraj(nodes(end,2),x_end,timeVector,t_i,t_f);
% load('C:\Users\Arthur\Documents\MATLAB\OoCode\ELLAFlexLinkViscJoints\TrajSimulink')
% trajy = interp1(TrajParam.time,-TrajParam.trajy,timeVector,'linear');
% trajz = interp1(TrajParam.time,TrajParam.trajz,timeVector,'linear');
% trajx = interp1(TrajParam.time,TrajParam.trajx,timeVector,'linear');

count = size(elements,2)+1;
elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajy;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elements{count}.elements = [count-3 count-2 count-1];
elements{count}.active = 1;

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
% D = StaticIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();
save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkRigidJoints_wRigiModel\D7','D')

%% Flexible Model
clear elements

count = 0;
Start1 = 3; % first node of the 1st link
for i = 1:link1.nElem
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start1-1+i Start1+i];
    elements{count+i}.KCS = link1.KCS;
    elements{count+i}.MCS = link1.MCS;
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.listPhiq = listPhiq;
    elements{count+i}.g = grav;
end
% count = 1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [Start1+link1.nElem/2 Start1+[0:link1.nElem/2-1] Start1+link1.nElem/2+1+[0:link1.nElem/2-1]];
% elements{count}.mass = link1.m;
% % elements{count}.J = link1.J+diag([link1.m*0.475^2 0 0]);
% elements{count}.J = link1.J;
% elements{count}.g = grav;
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;

% creating first flexible link with link.nElem number of elements (rotating aroud z)
count = size(elements,2);
Start2 = Start1+link1.nElem+1; % first node of the 2nd link
for i = 1:link2.nElem
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start2-1+i Start2+i];
    elements{count+i}.KCS = link2.KCS;
    elements{count+i}.MCS = link2.MCS;
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;    
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.listPhiq = listPhiq;
    elements{count+i}.g = grav;
end

% creating rigid body joint(rotating aroud Z) Base
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [2];
% elements{count}.mass = 0.3;
elements{count}.J = diag([0 0 0.3+((0.33+0.011+1.69)*10^-4)*gear1^2]);
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body joint(rotating aroud y) inertia of motor 2 (shoulder)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [3];
% elements{count}.mass = 0.3;
elements{count}.J = diag([0 ((0.33+0.011+1.69)*10^-4)*gear2^2 0]);
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body joint(rotating aroud y) Elbow
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2-1];
elements{count}.mass = 3.3+3;
elements{count}.J = diag([0.0299 0.0177+((0.33+0.011+0.193)*10^-4)*gear3^2 0.0299]);
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body end-effector
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = 2.1;
elements{count}.J = diag([0.0021 0.0044 0.0033]);
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% Creating the kinematic constraints
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.k = stif1;
elements{count}.d = damp1;
elements{count}.coulomb = coul1;
elements{count}.A = [0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.k = stif2;
elements{count}.d = damp2;
elements{count}.coulomb = coul2;
% elements{count}.A = [0 0 0 0 1 0]';
elements{count}.A = [0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.k = stif3;
elements{count}.d = damp3;
elements{count}.coulomb = coul3;
% elements{count}.A = [0 0 0 0 1 0]';
elements{count}.A = [0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]';

% Trajectory
r = 0.30;
y_end = -0.75;
z_end = 1;

timeVector = 0:timestepsize:finaltime;
% trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'lin',t_i,t_f);
% trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'lin',t_i,t_f);
% trajx = nodes(end,2)*ones(size(timeVector));
% load('C:\Users\Arthur\Documents\MATLAB\OoCode\ELLAFlexLinkViscJoints\TrajJointSimulink')
% joint1 = interp1(TrajJointParam.time,TrajJointParam.joint1,timeVector,'linear');
% joint2 = interp1(TrajJointParam.time,TrajJointParam.joint2,timeVector,'linear');
% joint3 = interp1(TrajJointParam.time,TrajJointParam.joint3,timeVector,'linear');

count = size(elements,2)+1;
elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajy;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elements{count}.elements = [count-3 count-2 count-1];
elements{count}.active = 1;

% Solving
ModelF = FEModel();
ModelF.CreateFEModel(nodes,elements);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from static solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
%     dh = zeros(6,npts);
%     for i = 1:npts
%         dh(:,i) = logSE3([dimR(ModelF.listNodes{n}.R(:,i)) ModelF.listNodes{n}.position(:,i); 0 0 0 1]);
%     end
%     ModelF.listNodes{n}.velocity = deriveVal(dh,timestepsize);
%     ModelF.listNodes{n}.acceleration = deriveVal(dh,timestepsize);
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
        ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
%         ModelF.listElementVariables{n}.velocity = deriveVal(ModelF.listElementVariables{n}.relCoo,timestepsize);
        ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
%         ModelF.listElementVariables{n}.acceleration = deriveVal(ModelF.listElementVariables{n}.velocity,timestepsize);
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

% % First part optimization to compute the equilibrium solution for the first
% % preactuation phase. Then copy this preactuation phase, to have full initial guess equilibrium to compute full
% % optimization.
% tic
% % EndPreActStep = t_i/(finaltime/(npts-1))+1;
% % EndPreActStep = round((t_i/(finaltime/(npts-1))+1)*0.47); 
% EndPreTime = t_i*0.5;%0.75
% EndPreActStep = round(EndPreTime/(finaltime/(npts-1))+1);
% S = DirectTranscriptionOpti(ModelF);
% S.parameters.rho = rho_num;
% S.npts = EndPreActStep+1;
% S.parameters.finaltime = EndPreTime;
% S.parameters.timestepsize = finaltime/(npts-1);
% S.parameters.scaling = 1e6;
% S.linConst = false;
% S.ConstIter = hypForPreComp;
% xSol = S.runOpti(D);
% 
% % Copy the state at the end of preactuation phase to start as new initial
% % guess.
% % Redefine the trajectory that was cut to only the preactuation phase
% ModelF.listElements{end}.T = [trajx;...
%                               trajy;...
%                               trajz];
% % To have a smooth position, vel and acc, during the first time steps,
% % search for the moment the position of the system is equal to the equilibrium point of the flex system.
% % We then set the system at this point as initial guess.
% tolEqui = 1e-3; % tolerence at which we consider that the flexible system is at equilibrium with respect to the static equilibrium in the preactuation phase
% for n = ModelF.listNumberNodes
%     for c = (EndPreActStep+1):size(ModelF.listNodes{n}.position,2)
%         if norm(ModelF.listNodes{n}.position(:,c)-ModelF.listNodes{n}.position(:,EndPreActStep))<tolEqui
%             Step2 = c; % Copy the values until the time step where we are at the same position... not sure it actually works... supress this 
%             break
%         end
%     end
%     for step = EndPreActStep+1:Step2
%         ModelF.listNodes{n}.R_InitOpti(:,step) = ModelF.listNodes{n}.R(:,EndPreActStep);
%         ModelF.listNodes{n}.position_InitOpti(:,step) = ModelF.listNodes{n}.position(:,EndPreActStep);
%         ModelF.listNodes{n}.velocity_InitOpti(:,step) = ModelF.listNodes{n}.velocity(:,EndPreActStep);
%         ModelF.listNodes{n}.acceleration_InitOpti(:,step) = ModelF.listNodes{n}.acceleration(:,EndPreActStep);
%     end
% %     ModelF.listNodes{n}.InitializeD_Opti();
% end
% for n = ModelF.listNumberElementVariables
%     if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
%         for c = (EndPreActStep+1):size(ModelF.listElementVariables{n}.relCoo,2)
%             if norm(ModelF.listElementVariables{n}.relCoo(:,c)-ModelF.listElementVariables{n}.relCoo(:,EndPreActStep))<tolEqui
%                 Step2 = c;
%                 break
%             end
%         end
%         for step =  EndPreActStep+1:Step2
%             ModelF.listElementVariables{n}.R_InitOpti(:,step) = ModelF.listElementVariables{n}.R(:,EndPreActStep);
%             ModelF.listElementVariables{n}.x_InitOpti(:,step) = ModelF.listElementVariables{n}.x(:,EndPreActStep);
%             ModelF.listElementVariables{n}.velocity_InitOpti(:,step) = ModelF.listElementVariables{n}.velocity(:,EndPreActStep);
%             ModelF.listElementVariables{n}.acceleration_InitOpti(:,step) = ModelF.listElementVariables{n}.acceleration(:,EndPreActStep);
%             ModelF.listElementVariables{n}.relCoo_InitOpti(:,step) = ModelF.listElementVariables{n}.relCoo(:,EndPreActStep);
%         end
%     else
%         for c = (EndPreActStep+1):size(ModelF.listElementVariables{n}.value,2)
%             if norm(ModelF.listElementVariables{n}.value(:,c)-ModelF.listElementVariables{n}.value(:,EndPreActStep))<tolEqui
%                 Step2 = c;
%                 break
%             end
%         end
%         for step =  EndPreActStep+1:Step2
%             ModelF.listElementVariables{n}.value_InitOpti(:,step) = ModelF.listElementVariables{n}.value(:,EndPreActStep); 
%         end
%     end
% %     ModelF.listElementVariables{n}.InitializeD_Opti();
% end

% Start second full optimization with newly computed initial guess
tic
S = DirectTranscriptionOpti(ModelF);
% S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = rho_num;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.parameters.scaling = 1e6;
% S.UbCommand = 50;
% S.LbCommand = -S.UbCommand;
S.ConstIter = hypForComp;
% analys = S.runPoleAnalysisOpti(D)
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkRigidJoints_wRigiModel\SBis7','S')
save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkRigidJoints_wRigiModel\SBis71sCoulCut','S')

% %% Direct dynamics
% timeSteps = S.timeValues;
% timeLoc = S.timesteps;
% 
% uRigi.u1 = D.model.listElementVariables{end}.value(1,:);
% uRigi.u2 = D.model.listElementVariables{end}.value(2,:);
% uRigi.u3 = D.model.listElementVariables{end}.value(3,:);
% uRigi.jointPos1 = D.model.listElementVariables{1}.relCoo;
% uRigi.jointPos2 = D.model.listElementVariables{2}.relCoo;
% uRigi.jointPos3 = D.model.listElementVariables{3}.relCoo;
% uRigi.jointVel1 = D.model.listElementVariables{1}.velocity;
% uRigi.jointVel2 = D.model.listElementVariables{2}.velocity;
% uRigi.jointVel3 = D.model.listElementVariables{3}.velocity;
% time = D.parameters.time;
% uRigi.time = time;
% 
% uBeam.u1 = S.model.listElementVariables{end}.value(1,timeLoc);
% uBeam.u2 = S.model.listElementVariables{end}.value(2,timeLoc);
% uBeam.u3 = S.model.listElementVariables{end}.value(3,timeLoc);
% uBeam.jointPos1 = S.model.listElementVariables{1}.relCoo;
% uBeam.jointPos2 = S.model.listElementVariables{2}.relCoo;
% uBeam.jointPos3 = S.model.listElementVariables{3}.relCoo;
% uBeam.jointVel1 = S.model.listElementVariables{1}.velocity;
% uBeam.jointVel2 = S.model.listElementVariables{2}.velocity;
% uBeam.jointVel3 = S.model.listElementVariables{3}.velocity;
% uBeam.time = timeSteps;
% 
% clear elements
%      
% % creating first flexible link with link.nElem number of elements (rotating aroud z)
% count = 0;
% for i = 1:link1.nElem
%     elements{count+i}.type = 'FlexibleBeamElement';
%     elements{count+i}.nodes = [Start1-1+i Start1+i];
%     elements{count+i}.KCS = link1.KCS;
%     elements{count+i}.MCS = link1.MCS;
%     elements{count+i}.yAxis = [0 0 1];
%     elements{count+i}.alpha = alpha;
%     elements{count+i}.beta = beta;
%     elements{count+i}.g = grav;
% end
% 
% % creating first flexible link with link.nElem number of elements (rotating aroud z)
% count = size(elements,2);
% for i = 1:link2.nElem
%     elements{count+i}.type = 'FlexibleBeamElement';
%     elements{count+i}.nodes = [Start2-1+i Start2+i];
%     elements{count+i}.KCS = link2.KCS;
%     elements{count+i}.MCS = link2.MCS;
%     elements{count+i}.yAxis = [0 0 1];
%     elements{count+i}.alpha = alpha;
%     elements{count+i}.beta = beta;
%     elements{count+i}.g = grav;
% end
% 
% % creating rigid body joint(rotating aroud Z) Base
% count = size(elements,2)+1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [2];
% % elements{count}.mass = 0.3;
% elements{count}.J = diag([0 0 0.3+((0.33+0.011+1.69)*10^-4)*gear1^2]);
% elements{count}.g = grav;
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;
% 
% % creating rigid body joint(rotating aroud y) inertia of motor 2 (shoulder)
% count = size(elements,2)+1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [3];
% % elements{count}.mass = 0.3;
% elements{count}.J = diag([0 ((0.33+0.011+1.69)*10^-4)*gear2^2 0]);
% elements{count}.g = grav;
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;
% 
% % creating rigid body joint(rotating aroud y) Elbow
% count = size(elements,2)+1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [Start2-1];
% elements{count}.mass = 3.3+3;
% elements{count}.J = diag([0.0299 0.0177+((0.33+0.011+0.193)*10^-4)*gear3^2 0.0299]);
% elements{count}.g = grav;
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;
% 
% % creating rigid body end-effector
% count = size(elements,2)+1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [nodes(end,1)];
% elements{count}.mass = 2.1;
% elements{count}.J = diag([0.0021 0.0044 0.0033]);
% elements{count}.g = grav;% 
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;
% 
% % Creating the kinematic constraints
% count = size(elements,2)+1;
% elements{count}.type = 'KinematicConstraint';
% elements{count}.nodes = [1 2];
% elements{count}.k = stif1;
% elements{count}.d = damp1;
% elements{count}.coulomb = coul1;
% elements{count}.A = [0 0 0 0 0 1]';
% 
% count = size(elements,2)+1;
% elements{count}.type = 'KinematicConstraint';
% elements{count}.nodes = [2 3];
% elements{count}.k = stif2;
% elements{count}.d = damp2;
% elements{count}.coulomb = coul2;
% elements{count}.A = [0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]';
% 
% count = size(elements,2)+1;
% elements{count}.type = 'KinematicConstraint';
% elements{count}.nodes = [Start2-1 Start2];
% elements{count}.k = stif3;
% elements{count}.d = damp3;
% elements{count}.coulomb = coul3;
% elements{count}.A = [0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]';
% 
% % applying computed torques
% u1_init = interp1(uRigi.time,uRigi.u1,timeVector,'linear');
% u2_init = interp1(uRigi.time,uRigi.u2,timeVector,'linear');
% u3_init = interp1(uRigi.time,uRigi.u3,timeVector,'linear');
% 
% u1 = interp1(uBeam.time,uBeam.u1,timeVector,'linear');
% u2 = interp1(uBeam.time,uBeam.u2,timeVector,'linear');
% u3 = interp1(uBeam.time,uBeam.u3,timeVector,'linear');
% 
% % count = size(elements,2)+1;
% % elements{count}.type = 'ForceInKinematicConstraint';
% % elements{count}.elements = [count-3];
% % elements{count}.f = u1;
% % 
% % count = size(elements,2)+1;
% % elements{count}.type = 'ForceInKinematicConstraint';
% % elements{count}.elements = [count-3];
% % elements{count}.f = u2;
% % 
% % count = size(elements,2)+1;
% % elements{count}.type = 'ForceInKinematicConstraint';
% % elements{count}.elements = [count-3];
% % elements{count}.f = u3;
% 
% count = size(elements,2)+1;
% elements{count}.type = 'ForceInKinematicConstraintPD'; % try with PD control
% elements{count}.elements = [count-3];
% elements{count}.ref = uBeam.jointPos1;
% elements{count}.dref = uBeam.jointVel1;
% elements{count}.kp = kp1;
% elements{count}.kd = kd1;
% elements{count}.f = u1;
% 
% count = size(elements,2)+1;
% elements{count}.type = 'ForceInKinematicConstraintPD'; % try with PD control
% elements{count}.elements = [count-3];
% elements{count}.ref = uBeam.jointPos2;
% elements{count}.dref = uBeam.jointVel2;
% elements{count}.kp = kp2;
% elements{count}.kd = kd2;
% elements{count}.f = u2;
% 
% count = size(elements,2)+1;
% elements{count}.type = 'ForceInKinematicConstraintPD'; % try with PD control
% elements{count}.elements = [count-3];
% elements{count}.ref = uBeam.jointPos3;
% elements{count}.dref = uBeam.jointVel3;
% elements{count}.kp = kp3;
% elements{count}.kd = kd3;
% elements{count}.f = u3;
% 
% % Boundary Condition
% BC = [1];
% 
% % Model Def
% ModelFD = FEModel();% Flex model
% ModelFD.CreateFEModel(nodes,elements);
% ModelFD.defineBC(BC);
% 
% ModelRD = FEModel();% Rigid model
% ModelRD.CreateFEModel(nodes,elements);
% ModelRD.defineBC(BC);
% 
% % Solving Flexible
% DF = DynamicIntegration(ModelFD);
% DF.parameters.finaltime = finaltime;
% DF.parameters.timestepsize = timestepsize;
% DF.parameters.rho = rho_num;
% DF.parameters.scaling = 1e6;
% DF.runIntegration();
% 
% % Solving Rigid
% ModelRD.listElements{count-2}.f = u1_init;
% ModelRD.listElements{count-2}.ref = uRigi.jointPos1;
% ModelRD.listElements{count-2}.dref = uRigi.jointVel1;
% ModelRD.listElements{count-1}.f = u2_init;
% ModelRD.listElements{count-1}.ref = uRigi.jointPos2;
% ModelRD.listElements{count-1}.dref = uRigi.jointVel2;
% ModelRD.listElements{count}.f = u3_init;
% ModelRD.listElements{count}.ref = uRigi.jointPos3;
% ModelRD.listElements{count}.dref = uRigi.jointVel3;
% 
% DR = DynamicIntegration(ModelRD);
% DR.parameters.finaltime = finaltime;
% DR.parameters.timestepsize = timestepsize;
% DR.parameters.rho = rho_num;
% DR.parameters.scaling = 1e6;
% DR.runIntegration();
% 
% %% Saving all datas
% 
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkRigidJoints_wRigiModel\DFBis7','DF')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkRigidJoints_wRigiModel\DRBis7','DR')
% 
% %% Plots
% % Plot efforts in the 3 joints
% 
% % figure
% % plot(time,uRigi.u1,time,uRigi.u2,time,uRigi.u3)
% % legend('u1','u2','u3')
% % xlabel('Time (s)')
% % ylabel('Effort (Nm)')
% 
% figure
% hold on
% plot(time,uRigi.u1,'--',time,uRigi.u2,'--',time,uRigi.u3,'--')
% plot(time,uBeam.u1,time,uBeam.u2,time,uBeam.u3)
% legend('u1_{rigi}','u2_{rigi}','u3_{rigi}','u1','u2','u3')
% xlabel('Time (s)')
% ylabel('Effort (Nm)')
% 
% figure
% hold on
% plot(time,uRigi.u1/gear1,'--',time,uRigi.u2/gear2,'--',time,uRigi.u3/gear3,'--')
% plot(time,uBeam.u1/gear1,time,uBeam.u2/gear2,time,uBeam.u3/gear3)
% legend('u1_{rigi}','u2_{rigi}','u3_{rigi}','u1','u2','u3')
% xlabel('Time (s)')
% ylabel('Motor effort (Nm)')
% 
% % Plot the joints angles
% joint1 = D.model.listElementVariables{1};
% joint2 = D.model.listElementVariables{2};
% joint3 = D.model.listElementVariables{3};
% % save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkViscJoints\Joint1R','joint1')
% % save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkViscJoints\Joint2R','joint2')
% % save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkViscJoints\Joint3R','joint3')
% 
% figure
% plot(time,joint1.relCoo,time,joint2.relCoo,time,joint3.relCoo)
% legend('joint1','joint2','joint3')
% xlabel('Time (s)')
% ylabel('Relative motion of joint (rad)')
% 
% endEffector = S.model.listNodes{end};
% endEffectorD = D.model.listNodes{end};
% figure
% hold on
% plot(time,endEffectorD.position(1,:),time,endEffectorD.position(2,:),time,endEffectorD.position(3,:))
% plot(time,endEffector.position(1,:),time,endEffector.position(2,:),time,endEffector.position(3,:))
% legend('x','y','z','xR','yR','zR')
% xlabel('Time (s)')
% ylabel('End-effector motion before/after optimization (m)')
% 
% disp(['max value of the u2 torque is ', num2str(max(abs(u2))),' Nm'])