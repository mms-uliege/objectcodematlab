%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Test of lambda kinemtaic manipulator with beam, for direct dynamic
clear

finaltime = 1;
timestepsize = 0.01;

%% Creation of the model

% Nodes
nodes = [1 0 0.5 0;
         2 0 0.5 0;
         3 0 0.5 0;
         4 sqrt(3)/2 0 0;
         5 0 -0.5 0;
         6 0 -0.5 0;
         7 sqrt(3)/2 0 0;
         8 sqrt(3) 0.5 0];
     
nElem = 2;
Start = 3;
End = 4;
nodes = createInterNodes(nodes,nElem,Start,End);
     
nElem = 2;
Start = 7;
End = 8;
nodes = createInterNodes(nodes,nElem,Start,End);

nElem = 2;
Start = 9;
End = 10;
nodes = createInterNodes(nodes,nElem,Start,End);

% Elements
elements = [];
elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2];
elements{1}.mass = .5;

elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [6];
elements{2}.mass = .5;

a = 0.005;
b = 0.08;
E = 120e9; nu = 0.33; G = E/(2*(1+nu)); rho = 7600;
A = a*b; I1 = (a*b^3)/12; I2  = (b*a^3)/12;J = (a^2 + b^2)*a*b/12;
KCS = diag([E*A G*A G*A G*J E*I1 E*I2]);
MCS = diag(rho*[A A A J I1 I2]);

type = 'FlexibleBeamElement';
Start = 3;
End = 5;
nElem = End-Start;
count = size(elements,2);
for i = 1:nElem
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start+i-1,1) nodes(Start+i,1)];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
end

type = 'FlexibleBeamElement';
Start = 7;
End = nodes(end,1);
nElem = End-Start;
count = size(elements,2);
for i = 1:nElem
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start+i-1,1) nodes(Start+i,1)];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
end

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % Element 9
elements{count}.nodes = [1 2];
elements{count}.A = [1 0 0 0 0 0]';
count = count+1;

elements{count}.type = 'KinematicConstraint'; % Element 10
elements{count}.nodes = [2 3];
elements{count}.A = [0 0 0 0 0 1]';
count = count+1;

elements{count}.type = 'KinematicConstraint'; % Element 11
elements{count}.nodes = [1 6];
elements{count}.A = [1 0 0 0 0 0]';
count = count+1;

elements{count}.type = 'KinematicConstraint'; % Element 12
elements{count}.nodes = [6 7];
elements{count}.A = [0 0 0 0 0 1]';
count = count+1;

elements{count}.type = 'KinematicConstraint'; % Element 13
elements{count}.nodes = [5 9];
elements{count}.A = [0 0 0 0 0 1]';
count = count+1;

% Trajectory

TrajParam.points = [sqrt(3) 0.5 0;...
                    3 0 0];
                    
TrajParam.timeVector = 0:timestepsize:finaltime;
TrajParam.intervals = [0.3 0.7];

[trajx trajy trajz] = PointTraj(TrajParam);

t = TrajParam.timeVector;

load('uLambdaRigid')
u1_init = interp1(uLambdaRigid.time,uLambdaRigid.u1_init,t,'linear');
u2_init = interp1(uLambdaRigid.time,uLambdaRigid.u2_init,t,'linear');
% u1 = u1_init;
% u2 = u2_init;

load('uLambdaBeam')
u1 = interp1(uLambdaBeam.time,uLambdaBeam.u1,t,'linear');
u2 = interp1(uLambdaBeam.time,uLambdaBeam.u2,t,'linear');
% u1 = smooth(u1);
% u2 = smooth(u2);

elements{14}.type = 'ForceInKinematicConstraint';
elements{14}.elements = [9];
elements{14}.f = u1;

elements{15}.type = 'ForceInKinematicConstraint';
elements{15}.elements = [11];
elements{15}.f = u2;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

%% Dynamic integration for initial guess

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.2;
% D.parameters.relTolRes = 1e-12;
D.parameters.scaling = 1e6;
D.runIntegration();


EndNode = 11;

time = D.parameters.time;

xEff = Model.listNodes{EndNode}.position(1,:);
yEff = Model.listNodes{EndNode}.position(2,:);

figure
hold on
title('Commands of rigid lambda manipulator')
plot(time,u1,time,u2)
plot(time,u1_init,':',time,u2_init,':')
legend('u1','u2','u1_{rigi}','u2_{rigi}')
xlabel('time')
ylabel('command')
grid on

figure
title('X and Y of lambda manipulator effector')
hold on
plot(time,xEff,time,yEff)
plot(time,trajx,':',time,trajy,':')
legend('x','y','x_d','y_d')
xlabel('time')
ylabel('position')
grid on

figure
title('Trajectory of lambda manipulator effector')
hold on
plot(xEff,yEff,'Linewidth',3)
plot(trajx,trajy, 'Linewidth',1, 'Color','r')
xlabel('x')
ylabel('y')
grid on