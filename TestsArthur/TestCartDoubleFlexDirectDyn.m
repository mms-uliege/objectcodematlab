%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear

finaltime = 2;
timestepsize = 0.01;

nodes = [1 -1 0 0;
         2 -1 0 0;
         3 -1 0 0;
         4 -1.6614 -0.75 0;
         5 -1.6614 -0.75 0;
         6 -1 -1.5 0];

nodes = createInterNodes(nodes,2,3,4);
nodes = createInterNodes(nodes,6,6,7);
nodes = doubleNode(nodes,[8 10]);

elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2];
elements{1}.mass = 3;
% elements{1}.J = eye(3);
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [4 3 5];
elements{2}.mass = 6.875;
elements{2}.J = 0.57*eye(3);

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [7 6 8];
elements{3}.mass = 6.875/3;
elements{3}.J = 0.0217*eye(3);

elements{4}.type = 'RigidBodyElement';
elements{4}.nodes = [10 9 11];
elements{4}.mass = 6.875/3;
elements{4}.J = 0.0217*eye(3);

elements{5}.type = 'RigidBodyElement';
elements{5}.nodes = [13 12 14];
elements{5}.mass = 6.875/3;
elements{5}.J = 0.0217*eye(3);

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [1 2];
elements{6}.A = [1 0 0 0 0 0]';

elements{7}.type = 'KinematicConstraint';
elements{7}.nodes = [2 3];
elements{7}.A = [0 0 0 0 0 1]';

elements{8}.type = 'KinematicConstraint';
elements{8}.nodes = [5 6];
elements{8}.A = [0 0 0 0 0 1]';

elements{9}.type = 'KinematicConstraint';
elements{9}.nodes = [8 9];
elements{9}.A = [0 0 0 0 0 1]';

elements{10}.type = 'KinematicConstraint';
elements{10}.nodes = [11 12];
elements{10}.A = [0 0 0 0 0 1]';

elements{11}.type = 'RotSpringDamperElement';
elements{11}.damping = 0.75; % 200 
elements{11}.stiffness = 125; % 100
elements{11}.nodes = [8 9];
elements{11}.A = [0 0 1];

elements{12}.type = 'RotSpringDamperElement';
elements{12}.damping = 0.75; % 200
elements{12}.stiffness = 125; % 100
elements{12}.nodes = [11 12];
elements{12}.A = [0 0 1];

% Trajectory

cart_end = 1;
cart_i = -1;

x_end = 1;
x_i = -1;

y_end = -1.5;
y_i = -1.5;

r = 1;

timeVector = 0:timestepsize:finaltime;
trajcart = halfCircleTraj(cart_i,cart_end,r,timeVector,'lin',0.2,1.7);
trajx = halfCircleTraj(x_i,x_end,r,timeVector,'sin',0.2,1.7);
trajy = halfCircleTraj(y_i,y_end,r,timeVector,'cos',0.2,1.7);

t = 0:timestepsize:finaltime;

load('uCartDoubleRigid')
u1_init = interp1(uCartDoubleRigid.time,uCartDoubleRigid.u1_init,t,'linear');
u2_init = interp1(uCartDoubleRigid.time,uCartDoubleRigid.u2_init,t,'linear');
u3_init = interp1(uCartDoubleRigid.time,uCartDoubleRigid.u3_init,t,'linear');
% u1 = u1_init;
% u2 = u2_init;
% u3 = u3_init;

load('uCartDoubleFlex')
u1 = interp1(uCartDoubleFlex.time,uCartDoubleFlex.u1,t,'linear');
u2 = interp1(uCartDoubleFlex.time,uCartDoubleFlex.u2,t,'linear');
u3 = interp1(uCartDoubleFlex.time,uCartDoubleFlex.u3,t,'linear');

elements{13}.type = 'ForceInKinematicConstraint';
elements{13}.elements = [6];
elements{13}.f = u1;

elements{14}.type = 'ForceInKinematicConstraint';
elements{14}.elements = [7];
elements{14}.f = u2;

elements{15}.type = 'ForceInKinematicConstraint';
elements{15}.elements = [8];
elements{15}.f = u3;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

Modelr = FEModel();
Modelr.CreateFEModel(nodes,elements);
Modelr.defineBC(BC);


D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.0;
% D.parameters.relTolRes = 1e-12;
D.runIntegration();

xcart = Model.listNodes{2}.position(1,:);
ycart = Model.listNodes{2}.position(2,:);
x = Model.listNodes{end}.position(1,:);
y = Model.listNodes{end}.position(2,:);

Modelr.listElements{13}.f = u1_init;
Modelr.listElements{14}.f = u2_init;
Modelr.listElements{15}.f = u3_init;

Dr = DynamicIntegration(Modelr);
Dr.parameters.finaltime = finaltime;
Dr.parameters.timestepsize = timestepsize;
Dr.parameters.rho = 0.0;
% D.parameters.relTolRes = 1e-12;
Dr.runIntegration();

xcartr = Modelr.listNodes{2}.position(1,:);
ycartr = Modelr.listNodes{2}.position(2,:);
xr = Modelr.listNodes{end}.position(1,:);
yr = Modelr.listNodes{end}.position(2,:);

% Plots

timeSteps = D.parameters.time;

% figure
% hold on
% plot(xcart,ycart,x,y, 'Linewidth',3, 'Color','k')
% plot(xcartr,ycartr,':',xr,yr,':', 'Linewidth',3, 'Color','b')
% plot(trajcart,zeros(size(trajcart)),trajx,trajy, 'Linewidth',1, 'Color','r')
% legend('Flexible control','','Rigid control','Prescribed')
% title('Trajectory of the cart')
% grid on

figure
hold on
plot(x,y, '-o','Linewidth',2, 'Color','r')
plot(xr,yr,'--', 'Linewidth',2, 'Color','b')
plot(trajx,trajy, 'Linewidth',2, 'Color','k')
legend('With u','With u_{rigid}','Prescribed')
xlabel('X [m]','Fontsize',16)
ylabel('Y [m]','Fontsize',16)
% title('Trajectory of the cart','Fontsize',18)
grid on


figure
hold on
plot(timeSteps,xcart,timeSteps,x,timeSteps,y)
plot(timeSteps,trajcart,'--',timeSteps,trajx,'--',timeSteps,trajy,'--')
legend('cart', 'X', 'Y','cartd','Xd','Yd')
grid on

figure
hold on
plot(timeSteps,u1,timeSteps,u2,timeSteps,u3,'Linewidth',2)
plot(timeSteps,u1_init(:),'--',timeSteps,u2_init(:),'--',timeSteps,u3_init(:),'--','Linewidth',2)
xlabel('Time [s]','Fontsize',16)
ylabel('Commands','Fontsize',16)
legend('u_1 [N]','u_2 [Nm]','u_3 [Nm]','u_{1,rigid} [N]','u_{2,rigid} [Nm]','u_{3,rigid} [Nm]','Location','Best')
% title('Commands of an underactuated cart system','Fontsize',18)
grid on

RelError = zeros(size(timeSteps));
RelErrorR = zeros(size(timeSteps));
for i = 1:length(RelError)
    RelError(i) = 100*(norm([trajx(i)-x(i) trajy(i)-y(i)])/norm([trajx(i) trajy(i)]));
    
    RelErrorR(i) = 100*(norm([trajx(i)-xr(i) trajy(i)-yr(i)])/norm([trajx(i) trajy(i)]));
end
figure
plot(timeSteps,RelError,timeSteps,RelErrorR)
title('Relative error of the direct dynamic trajectory','Fontsize',13)
xlabel('time')
ylabel('Relative Error (%)')
legend('Flexible','Rigid')
grid on

RMSRelError = sqrt(mean(RelError.^2))
MaxRelError = max(RelError)
RMSRelErrorR = sqrt(mean(RelErrorR.^2))
MaxRelErrorR = max(RelErrorR)


