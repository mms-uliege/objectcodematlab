%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Complete 7dof model of Sawyer robot with elastic joints.
% Geometry and inertia infos coming from CAD solidworks model.

% --!-- Y axis is vertical like CAD model and X axis is toward the front of
% the robot (In Gantry position) --!--

clear all
close all

finaltime = 2; % sec
timestepsize = 0.01; % sec

t_i = 0.2; % time at end of pre-actuation
t_f = finaltime - 0.2; % time at begining of post-actuation

%% parameters
stiff1 = 0;
damp1 = 0; % Nms/rad
coul1 = 5; % Nm
stiff1Bis = 500;
damp1Bis = 10; % Nms/rad
coul1Bis = 0; % Nm

stiff2 = 0;
damp2 = 0; % Nms/rad
coul2 = 2; % Nm
stiff2Bis = 500;
damp2Bis = 5; % Nms/rad
coul2Bis = 0; % Nm

stiff3 = 0;
damp3 = 0; % Nms/rad
coul3 = 2; % Nm
stiff3Bis = 500;
damp3Bis = 5; % Nms/rad
coul3Bis = 0; % Nm

stiff4 = 0;
damp4 = 0; % Nms/rad
coul4 = 0.5; % Nm
stiff4Bis = 500;
damp4Bis = 1; % Nms/rad
coul4Bis = 0; % Nm

stiff5 = 0;
damp5 = 0; % Nms/rad
coul5 = 1; % Nm
stiff5Bis = 500;
damp5Bis = 2; % Nms/rad
coul5Bis = 0; % Nm

stiff6 = 0;
damp6 = 0; % Nms/rad
coul6 = 0.5; % Nm
stiff6Bis = 500;
damp6Bis = 1; % Nms/rad
coul6Bis = 0; % Nm

stiff7 = 0;
damp7 = 0; % Nms/rad
coul7 = 0.5; % Nm
stiff7Bis = 5000;
damp7Bis = 1; % Nms/rad
coul7Bis = 0; % Nm

grav = [0 -9.81 0]; % gravity in the vertical Y axis
%% joint 1
joint1.axis = [0 0 0 0 1 0]'; % 1st joint along vertical Y axis part 1
joint1.stiff = stiff1;
joint1.damp = damp1; % Nms/rad
joint1.coul = coul1; % Nm

joint1Bis.axis = 0*[0 0 0 0 1 0]'; % 1st joint along vertical Y axis part 2
joint1Bis.stiff = stiff1Bis;
joint1Bis.damp = damp1Bis; % Nms/rad
joint1Bis.coul = coul1Bis; % Nm
%% joint 2
joint2.axis = [0 0 0 0 0 1]'; % 2nd joint along horizontal Z axis part 1
joint2.stiff = stiff2;
joint2.damp = damp2; % Nms/rad
joint2.coul = coul2; % Nm

joint2Bis.axis = 0*[0 0 0 0 0 1]'; % 2nd joint along horizontal Z axis part 2
joint2Bis.stiff = stiff2Bis;
joint2Bis.damp = damp2Bis; % Nms/rad
joint2Bis.coul = coul2Bis; % Nm
%% joint 3
joint3.axis = 0*[0 0 0 1 0 0]'; % 3rd joint along horizontal X axis part 1
joint3.stiff = stiff3;
joint3.damp = damp3; % Nms/rad
joint3.coul = coul3; % Nm

joint3Bis.axis = 0*[0 0 0 1 0 0]'; % 3rd joint along horizontal X axis part 2
joint3Bis.stiff = stiff3Bis;
joint3Bis.damp = damp3Bis; % Nms/rad
joint3Bis.coul = coul3Bis; % Nm
%% joint 4
joint4.axis = [0 0 0 0 0 1]'; % 4th joint along horizontal Z axis part 1
joint4.stiff = stiff4;
joint4.damp = damp4; % Nms/rad
joint4.coul = coul4; % Nm

joint4Bis.axis = 0*[0 0 0 0 0 1]'; % 4th joint along horizontal Z axis part 2
joint4Bis.stiff = stiff4Bis;
joint4Bis.damp = damp4Bis; % Nms/rad
joint4Bis.coul = coul4Bis; % Nm
%% joint 5
joint5.axis = [0 0 0 1 0 0]'; % 5th joint along horizontal X axis part 1
joint5.nodes = [1 2];
joint5.stiff = stiff5;
joint5.damp = damp5; % Nms/rad
joint5.coul = coul5; % Nm

joint5Bis.axis = 0*[0 0 0 1 0 0]'; % 5th joint along horizontal X axis part 2
joint5Bis.stiff = stiff5Bis;
joint5Bis.damp = damp5Bis; % Nms/rad
joint5Bis.coul = coul5Bis; % Nm
%% joint 6
joint6.axis = [0 0 0 0 0 1]'; % 6th joint along horizontal Z axis part 1
joint6.stiff = stiff6;
joint6.damp = damp6; % Nms/rad
joint6.coul = coul6; % Nm

joint6Bis.axis = 0*[0 0 0 0 0 1]'; % 6th joint along horizontal Z axis part 2
joint6Bis.stiff = stiff6Bis;
joint6Bis.damp = damp6Bis; % Nms/rad
joint6Bis.coul = coul6Bis; % Nm
%% joint 7
joint7.axis = [0 0 0 0 1 0]'; % 7th joint along horizontal Y axis part 1
joint7.stiff = stiff7;
joint7.damp = damp7; % Nms/rad
joint7.coul = coul7; % Nm

joint7Bis.axis = 0*[0 0 0 0 1 0]'; % 7th joint along horizontal Y axis part 2
joint7Bis.stiff = stiff7Bis;
joint7Bis.damp = damp7Bis; % Nms/rad
joint7Bis.coul = coul7Bis; % Nm

%% link 1
link1.m = 6.1704; % kg
link1.J = [0.1194 0.0008 -0.0012;...
           0.0008 0.0205 -0.0025;...
           -0.0012 -0.0025 0.1228]; % kg/m^2
link1.nodes = [0 0.092 0;...
               0.0239 0.2984 0.0030;...               
               0.0810 0.3170 -0.0595]; % m
link1.g = grav;

%% link 2
link2.m = 3.0599; % kg
link2.J = [0.0101 0 -0.0028;...
           0 0.0155 0;...
           -0.0028 0 0.0114]; % kg/m^2
link2.nodes = [0.0810 0.3170 -0.0595;...
               0.1058 0.3170 -0.1562;...               
               0.2203 0.3170 -0.1925]; % m
link2.g = grav;

%% link 3
link3.m = 2.1221; % kg
link3.J = [0.0025 0 0.0006;...
           0 0.0175 0;...
           0.0006 0 0.0178]; % kg/m^2
link3.nodes = [0.2203 0.3170 -0.1925;...
               0.3614 0.3170 -0.1925;...
               0.4810 0.3170 -0.1500]; % m
link3.g = grav;

%% link 4
link4.m = 1.8770; % kg
link4.J = [0.0047 0 0.0015;...
           0 0.0078 0;...
           0.0015 0 0.0053]; % kg/m^2
link4.nodes = [0.4810 0.3170 -0.1500;...
               0.5076 0.3169 -0.0544;...
               0.6075 0.3170 -0.0240]; % m
link4.g = grav;

%% link 5
link5.m = 1.6913; % kg
link5.J = [0.0016 0 -0.0004;...
           0 0.0139 0;...
           -0.0004 0 0.0140]; % kg/m^2
link5.nodes = [0.6075 0.3170 -0.0240;...
               0.7489 0.3173 -0.0250;...               
               0.8810 0.3170 -0.0645]; % m
link5.g = grav;

%% link 6
link6.m = 1.0727; % kg
link6.J = [0.0025 0 0;...
           0 0.0020 0.0003;...
           0 0.0003 0.0016]; % kg/m^2
link6.nodes = [0.8810 0.3170 -0.0645;...
               0.8797 0.3048 -0.1390;...
               0.8810 0.2410 -0.1603]; % m
link6.g = grav;

%% link 7
link7.m = 0.3678;
link7.J = [0.0002 0 0;...
           0 0.0004 0;...
           0 0 0.0004]; % kg/m^2
link7.nodes = [0.8810 0.2410 -0.1603;...
               0.8916 0.2161 -0.1603;...
               0.8810 0.1833 -0.1603]; % m
link7.g = grav;

%% Building the Model

nodes = [link1.nodes(1,:);...
         link1.nodes(1,:);...
         link1.nodes;...
         link2.nodes(1,:);...
         link2.nodes;...
         link3.nodes(1,:);...
         link3.nodes;...
         link4.nodes(1,:);...
         link4.nodes;...
         link5.nodes(1,:);...
         link5.nodes;...
         link6.nodes(1,:);...
         link6.nodes;...
         link7.nodes(1,:);...
         link7.nodes];

% number all nodes properly so they can be used by FEMmodel
nodes = [1:size(nodes,1);nodes']';

count = 1;
% creating 1st rigid body (rotating aroud y)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [4 3 5];
elements{count}.mass = link1.m;
elements{count}.J = link1.J;
elements{count}.g = link1.g;
count = count +1;

% creating 2nd rigid body (rotating aroud z)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [8 7 9];
elements{count}.mass = link2.m;
elements{count}.J = link2.J;
elements{count}.g = link2.g;
count = count +1;

% creating 3rd rigid body (rotating aroud x)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [12 11 13];
elements{count}.mass = link3.m;
elements{count}.J = link3.J;
elements{count}.g = link3.g;
count = count +1;

% creating 4th rigid body (rotating aroud z)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [16 15 17];
elements{count}.mass = link4.m;
elements{count}.J = link4.J;
elements{count}.g = link4.g;
count = count +1;

% creating 5th rigid body (rotating aroud x)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [20 19 21];
elements{count}.mass = link5.m;
elements{count}.J = link5.J;
elements{count}.g = link5.g;
count = count +1;

% creating 6th rigid body (rotating aroud z)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [24 23 25];
elements{count}.mass = link6.m;
elements{count}.J = link6.J;
elements{count}.g = link6.g;
count = count +1;

% creating 7th rigid body (rotating aroud y)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [28 27 29];
elements{count}.mass = link7.m;
elements{count}.J = link7.J;
elements{count}.g = link7.g;
count = count +1;

% creating 1st joint (rotating aroud y) part 1
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = joint1.axis;
elements{count}.k = joint1.stiff;
elements{count}.d = joint1.damp;
elements{count}.coulomb = joint1.coul;
count = count +1;

% creating 1st joint (rotating aroud y) part 2
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.A = joint1Bis.axis;
elements{count}.k = joint1Bis.stiff;
elements{count}.d = joint1Bis.damp;
elements{count}.coulomb = joint1Bis.coul;
count = count +1;

% creating 2nd joint (rotating aroud z) part 1
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [5 6];
elements{count}.A = joint2.axis;
elements{count}.k = joint2.stiff;
elements{count}.d = joint2.damp;
elements{count}.coulomb = joint2.coul;
count = count +1;

% creating 2nd joint (rotating aroud z) part 2
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [6 7];
elements{count}.A = joint2Bis.axis;
elements{count}.k = joint2Bis.stiff;
elements{count}.d = joint2Bis.damp;
elements{count}.coulomb = joint2Bis.coul;
count = count +1;

% creating 3rd joint (rotating aroud x) part 1
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [9 10];
elements{count}.A = joint3.axis;
elements{count}.k = joint3.stiff;
elements{count}.d = joint3.damp;
elements{count}.coulomb = joint3.coul;
count = count +1;

% creating 3rd joint (rotating aroud x) part 2
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [10 11];
elements{count}.A = joint3Bis.axis;
elements{count}.k = joint3Bis.stiff;
elements{count}.d = joint3Bis.damp;
elements{count}.coulomb = joint3Bis.coul;
count = count +1;

% creating 4th joint (rotating aroud z) part 1
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [13 14];
elements{count}.A = joint4.axis;
elements{count}.k = joint4.stiff;
elements{count}.d = joint4.damp;
elements{count}.coulomb = joint4.coul;
count = count +1;

% creating 4th joint (rotating aroud z) part 2
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [14 15];
elements{count}.A = joint4Bis.axis;
elements{count}.k = joint4Bis.stiff;
elements{count}.d = joint4Bis.damp;
elements{count}.coulomb = joint4Bis.coul;
count = count +1;

% creating 5th joint (rotating aroud x) part 1
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [17 18];
elements{count}.A = joint5.axis;
elements{count}.k = joint5.stiff;
elements{count}.d = joint5.damp;
elements{count}.coulomb = joint5.coul;
count = count +1;

% creating 5th joint (rotating aroud x) part 2
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [18 19];
elements{count}.A = joint5Bis.axis;
elements{count}.k = joint5Bis.stiff;
elements{count}.d = joint5Bis.damp;
elements{count}.coulomb = joint5Bis.coul;
count = count +1;

% creating 6th joint (rotating aroud z) part 1
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [21 22];
elements{count}.A = joint6.axis;
elements{count}.k = joint6.stiff;
elements{count}.d = joint6.damp;
elements{count}.coulomb = joint6.coul;
count = count +1;

% creating 6th joint (rotating aroud z) part 2
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [22 23];
elements{count}.A = joint6Bis.axis;
elements{count}.k = joint6Bis.stiff;
elements{count}.d = joint6Bis.damp;
elements{count}.coulomb = joint6Bis.coul;
count = count +1;

% creating 7th joint (rotating aroud y) part 1
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [25 26];
elements{count}.A = joint7.axis;
elements{count}.k = joint7.stiff;
elements{count}.d = joint7.damp;
elements{count}.coulomb = joint7.coul;
count = count +1;

% creating 7th joint (rotating aroud y) part 2
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [26 27];
elements{count}.A = joint7Bis.axis;
elements{count}.k = joint7Bis.stiff;
elements{count}.d = joint7Bis.damp;
elements{count}.coulomb = joint7Bis.coul;
count = count +1;

% Trajectory

r = 0.2;
z_end = nodes(end,4);
x_end = nodes(end,2)-2*r;
start_ang1 = 0;
start_ang2 = 0;
start_ang3 = 0;
end_ang1 = 0;
end_ang2 = 0;
end_ang3 = 0;

timeVector = 0:timestepsize:finaltime;
trajy = nodes(end,3)*ones(size(timeVector));
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'sin',t_i,t_f);
traja1 = lineTraj(start_ang1,end_ang1,timeVector,t_i,t_f);
traja2 = lineTraj(start_ang2,end_ang2,timeVector,t_i,t_f);
traja3 = lineTraj(start_ang3,end_ang3,timeVector,t_i,t_f);


% elements{count}.type = 'TrajectoryConstraint4';
% elements{count}.nodes = [nodes(end,1)];
% elements{count}.T = [trajx;...
%                      trajy;...
%                      trajz;...
%                      traja1;...
%                      traja2;...
%                      traja3];
% elements{count}.Axe = [1 0 0 0 0 0;...
%                        0 1 0 0 0 0;...
%                        0 0 1 0 0 0;...
%                        0 0 0 1 0 0;...
%                        0 0 0 0 1 0;...
%                        0 0 0 0 0 1];
% % elements{count}.elements = [count-14 count-12 count-10 count-8 count-6 count-4 count-2];
% elements{count}.elements = [count-14 count-12 count-8 count-6 count-4 count-2];
% % elements{count}.elements = [count-14 count-12 count-8];
% elements{count}.active = 1;

elements{count}.type = 'ImposeNodeMotion';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajy;...
                     trajz];...
%                      traja1;...
%                      traja2;...
%                      traja3];
elements{count}.Axe = [1 0 0 0 0 0;...
                       0 1 0 0 0 0;...
                       0 0 1 0 0 0];...
%                        0 0 0 1 0 0;...
%                        0 0 0 0 1 0;...
%                        0 0 0 0 0 1];

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.0;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();





