%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code to analyse the bag file that recorded joint position and torque to
% determine the stiffness of the joints of the robot.
clear all
% close all

% Load the bag file with joint positions and torques
bag = rosbag('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\recordedBag_stiffnessMeasurements.bag');
% bag = rosbag('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\recordedBag_vibrationMeasurements.bag');
jointStates = bag.readMessages;

ti = bag.StartTime;
tf = bag.EndTime;

duration = bag.EndTime-bag.StartTime;
nMess = bag.NumMessages;

time = zeros(1,nMess);
JointPose = zeros(7,nMess);
JointVelocity = zeros(7,nMess);
JointTorque = zeros(7,nMess);
diffTorque = zeros(7,nMess);
diffPose = zeros(7,nMess);
stiffness = zeros(7,nMess);

for i = 1:nMess
    time(i) = bag.MessageList.Time(i)-ti;
    if ~isempty(jointStates{i}.Position(2:end-1))
        JointPose(:,i) = jointStates{i}.Position(2:end-1);
        JointPose(4,i) = -JointPose(4,i);
        JointVelocity(:,i) = jointStates{i}.Velocity(2:end-1);
        JointVelocity(4,i) = -JointVelocity(4,i);
        JointTorque(:,i) = jointStates{i}.Effort(2:end-1);
     
        diffTorque(:,i) = abs((JointTorque(:,i)-JointTorque(:,1)));
        diffPose(:,i) = abs((JointPose(:,i)-JointPose(:,1)));
        stiffness(:,i) = diffTorque(:,i)./diffPose(:,i);
    end
end

stiff1 = stiffness(1,:);
stiff2 = stiffness(2,:);
stiff3 = stiffness(3,:);
stiff4 = stiffness(4,:);
stiff5 = stiffness(5,:);
stiff6 = stiffness(6,:);
stiff7 = stiffness(7,:);

stiff1 = stiff1(isfinite(stiff1));
stiff1 = stiff1(stiff1~=0);
stiff2 = stiff2(isfinite(stiff2));
stiff2 = stiff2(stiff2~=0);
stiff3 = stiff3(isfinite(stiff3));
stiff3 = stiff3(stiff3~=0);
stiff4 = stiff4(isfinite(stiff4));
stiff4 = stiff4(stiff4~=0);
stiff5 = stiff5(isfinite(stiff5));
stiff5 = stiff5(stiff5~=0);
stiff6 = stiff6(isfinite(stiff6));
stiff6 = stiff6(stiff6~=0);
stiff7 = stiff7(isfinite(stiff7));
stiff7 = stiff7(stiff7~=0);

% stiff1 = stiff1(stiff1<0.5e4);
% stiff2 = stiff2(stiff2<1e4);
% stiff4 = stiff4(stiff4<0.5e4);

k1 = mean(stiff1);
k2 = mean(stiff2);
k3 = mean(stiff3);
k4 = mean(stiff4);
k5 = mean(stiff5);
k6 = mean(stiff6);
k7 = mean(stiff7);

% fit1 = fit(diffPose(1,:)',diffTorque(1,:)','poly1');
% fit2 = fit(diffPose(2,:)',diffTorque(2,:)','poly1');
% fit4 = fit(diffPose(4,:)',diffTorque(4,:)','poly1');
ft = fittype('b*x');
fit1 = fit(diffPose(1,:)',diffTorque(1,:)',ft,'StartPoint',-700);
fit2 = fit(diffPose(2,:)',diffTorque(2,:)',ft,'StartPoint',-900);
fit3 = fit(diffPose(3,:)',diffTorque(3,:)',ft,'StartPoint',-700);
fit4 = fit(diffPose(4,:)',diffTorque(4,:)',ft,'StartPoint',400);
fit5 = fit(diffPose(5,:)',diffTorque(5,:)',ft,'StartPoint',-700);
fit6 = fit(diffPose(6,:)',diffTorque(6,:)',ft,'StartPoint',-700);
fit7 = fit(diffPose(7,:)',diffTorque(7,:)',ft,'StartPoint',-700);
% fit1 = fit(diffPose(1,:)',diffTorque(1,:)',ft);
% fit2 = fit(diffPose(2,:)',diffTorque(2,:)',ft);
% fit4 = fit(diffPose(4,:)',diffTorque(4,:)',ft);

% Joint 1
figure
hold on
% plot(JointTorque(1,:),JointPose(1,:),'*')
plot(diffPose(1,:),diffTorque(1,:),'*')
plot(fit1)
xlabel('Joint torque 1')
ylabel('Joint position 1')
% title(['Stiffness plot of joint 1, measured is ',num2str(k1)])
title(['Stiffness plot of joint 1, measured is ',num2str(fit1.b)])
grid on
% figure
% hist(stiff1,200)
% histfit(stiff1)

% Joint 2
figure
hold on
% plot(JointTorque(2,:),JointPose(2,:),'*')
plot(diffPose(2,:),diffTorque(2,:),'*')
plot(fit2)
xlabel('Joint torque 2')
ylabel('Joint position 2')
% title(['Stiffness plot of joint 2, measured is ',num2str(k2)])
title(['Stiffness plot of joint 2, measured is ',num2str(fit2.b)])
grid on
% figure
% hist(stiff2,200)
% histfit(stiff2)

% % Joint 3
% figure
% plot(JointTorque(3,:),JointPose(3,:),'*')
% xlabel('Joint torque 3')
% ylabel('Joint position 3')
% title(['Stiffness plot of joint 3, measured is ',num2str(k3)])
% grid on
figure
hold on
plot(diffPose(3,:),diffTorque(3,:),'*')
plot(fit3)
xlabel('Joint torque 3')
ylabel('Joint position 3')
title(['Stiffness plot of joint 3, measured is ',num2str(fit3.b)])
grid on

% Joint 4
figure
hold on
% plot(JointTorque(4,:),JointPose(4,:),'*')
plot(diffPose(4,:),diffTorque(4,:),'*')
plot(fit4)
xlabel('Joint torque 4')
ylabel('Joint position 4')
% title(['Stiffness plot of joint 4, measured is ',num2str(k4)])
title(['Stiffness plot of joint 4, measured is ',num2str(fit4.b)])
grid on
% figure
% hist(stiff4,200)
% histfit(stiff4)

% % Joint 5
% figure
% plot(JointTorque(5,:),JointPose(5,:),'*')
% xlabel('Joint torque 5')
% ylabel('Joint position 5')
% title(['Stiffness plot of joint 5, measured is ',num2str(k5)])
% grid on
figure
hold on
plot(diffPose(5,:),diffTorque(5,:),'*')
plot(fit5)
xlabel('Joint torque 5')
ylabel('Joint position 5')
title(['Stiffness plot of joint 5, measured is ',num2str(fit5.b)])
grid on

% % Joint 6
% figure
% plot(JointTorque(6,:),JointPose(6,:),'*')
% xlabel('Joint torque 6')
% ylabel('Joint position 6')
% title(['Stiffness plot of joint 6, measured is ',num2str(k6)])
% grid on
figure
hold on
plot(diffPose(6,:),diffTorque(6,:),'*')
plot(fit6)
xlabel('Joint torque 6')
ylabel('Joint position 6')
title(['Stiffness plot of joint 6, measured is ',num2str(fit6.b)])
grid on

% % Joint 7
% figure
% plot(JointTorque(7,:),JointPose(7,:),'*')
% xlabel('Joint torque 7')
% ylabel('Joint position 7')
% title(['Stiffness plot of joint 7, measured is ',num2str(k7)])
% grid on
figure
hold on
plot(diffPose(7,:),diffTorque(7,:),'*')
plot(fit7)
xlabel('Joint torque 7')
ylabel('Joint position 7')
title(['Stiffness plot of joint 7, measured is ',num2str(fit7.b)])
grid on

%% Plot 3 joints of interest position
figure
plot(time,[JointPose(1,:);JointPose(2,:);JointPose(4,:)])
xlabel('Time (s)')
ylabel('Joint position (rad)')
title('plot of joint 1 2 4 positions')
legend('1','2','3')
grid on

