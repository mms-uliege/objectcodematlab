%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Model of the Sawyer robot using the Peter Corke Robotics tool box
% Read the saved datas in the .txt files and see how it is behaving.
% or from the bag files (when it has been recorded)
clear all

type_of_traj = 'Vibration'; % either the 'Circle' trajectory, or 'Vibration' trajectory (divided in 2sections)
dur = '21';
amplitude = 0.05; % amplitude of the vibration
target_freq = 1; % [Hz] frequency at which arm should vibrate
amp = ['_',num2str(100*amplitude)]; % Save amplitude info as cm
targ_freq = ['_',num2str(10*target_freq)]; % save frequency info as tenth of a Hz
if ~strcmp(type_of_traj,'Vibration')
    amp = '';
    targ_freq = '';
end

% Param theta (angle between X, around Z)
% Param d (distance between X, along Z)
% Param a (distance between Z, along X)
% Param alpha (angle between Z, around X)

L(1) = Link([0     0.317     0.081     -pi/2]);
L(2) = Link([pi/2  0.1925    0          pi/2]);
L(3) = Link([0     0.400     0          pi/2]);
L(4) = Link([pi    0.168     0          pi/2]);
L(5) = Link([0     0.400     0          pi/2]);
L(6) = Link([pi    0.1363    0          pi/2]);
L(7) = Link([0     0.13375   0             0]);
L(2).offset = pi/2;
L(4).offset = pi;
L(6).offset = pi;

Sawyer = SerialLink(L,'name','Sawyer');

q1 = 0;
q2 = 0;
q3 = 0;
q4 = 0;
q5 = 0;
q6 = 0;
q7 = 0;

initAngle = [q1 q2 q3 q4 q5 q6 q7]; % see initial pose 
initAngle(4) = -initAngle(4);
% Sawyer.plot(initAngle)  % see initial pose 

% Load saved datas from the .txt files
% Flexible datas
poseData = ['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\Sawyer3DofPose',type_of_traj,'Flex_',dur,targ_freq,amp,'.txt'];
data = dlmread(poseData,' ',1,0);
timeFlex = data(:,1);
joint1Flex = data(:,2);
joint2Flex = data(:,3);
joint3Flex = data(:,4);
% Rigid datas
poseData = ['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\Sawyer3DofPose',type_of_traj,'Rigid_',dur,targ_freq,amp,'.txt'];
data = dlmread(poseData,' ',1,0);
timeRigid = data(:,1);
joint1Rigid = data(:,2);
joint2Rigid = data(:,3);
joint3Rigid = data(:,4);

nData = length(timeFlex);

allJointPoseFlex = [joint1Flex -joint2Flex zeros(nData,1) joint3Flex zeros(nData,1) pi/2*ones(nData,1) -pi/2*ones(nData,1)];
allJointPoseRigid = [joint1Rigid -joint2Rigid zeros(nData,1) joint3Rigid zeros(nData,1) pi/2*ones(nData,1) -pi/2*ones(nData,1)];

% Flexible endpoint forward kinematics
endFramesFlex = Sawyer.fkine(allJointPoseFlex);
endPoseFlex = transl(endFramesFlex);
% Rigid endpoint forward kinematics
endFramesRigid = Sawyer.fkine(allJointPoseRigid);
endPoseRigid = transl(endFramesRigid);

% Flexible and rigid joint angles
figure
hold on
plot(timeFlex,allJointPoseFlex)
set(gca,'ColorOrderIndex',1)
plot(timeRigid,allJointPoseRigid,'--')
xlabel('Time (s)')
ylabel('Joint positions (rad)')
title('Simulated joint positions')
legend('1','2','3','4','5','6','7','1r','2r','3r','4r','5r','6r','7r')
grid on

% Flexible and rigid end effector position
figure
hold on
plot(timeFlex,endPoseFlex)
set(gca,'ColorOrderIndex',1)
plot(timeRigid,endPoseRigid,'--')
xlabel('Time (s)')
ylabel('End positions (rad)')
title('Simulated End positions')
legend('x','y','z','xr','yr','zr')
grid on

% Flexible and rigid end effector trajectory
figure
hold on
plot3(endPoseFlex(:,1),endPoseFlex(:,2),endPoseFlex(:,3))
plot3(endPoseRigid(:,1),endPoseRigid(:,2),endPoseRigid(:,3))
xlabel('X')
ylabel('Y')
zlabel('Z')
title('Simulated End trajectory')
legend('Flex','Rigid')
axis('equal')
grid on

% Sawyer.plot(allJointPoseFlex)
% Sawyer.plot(allJointPoseRigid)





