%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script to compare measured datas from Rigid and flexible simulations

clear bagFlex
clear bagRigid
% close all
type_of_traj = 'Vibration'; % either the 'Circle' trajectory, or 'Vibration' trajectory (divided in 2sections)
dur = '20';
target_freq = 0.3; % [Hz] frequency at which arm should vibrate
amplitude = 0.12; % amplitude of the vibration
amp = ['_',num2str(100*amplitude)]; % Save amplitude info as cm
targ_freq = ['_',num2str(10*target_freq)]; % save frequency info as tenth of a Hz
if ~strcmp(type_of_traj,'Vibration')
    amp = '';
    targ_freq = '';
end

% Load the bag file with joint positions and torques Flexible assumption
bagFlex = rosbag(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\recordedBag_',type_of_traj,'TrajComparisonFlex_',dur,targ_freq,amp,'.bag']);
bagFlex = bagFlex.select('Topic','/robot/joint_states');
jointStatesFlex = bagFlex.readMessages;
nMessFlex = bagFlex.NumMessages;
timeFlex = zeros(1,nMessFlex);
JointPoseFlex = zeros(7,nMessFlex);
JointVelocityFlex = zeros(7,nMessFlex);
JointTorqueFlex = zeros(7,nMessFlex);
tiF = bagFlex.StartTime;
tfF = bagFlex.EndTime;
excludeFlex = zeros(1,nMessFlex);

for i = 1:nMessFlex
    timeFlex(i) = bagFlex.MessageList.Time(i)-tiF;
    if ~isempty(jointStatesFlex{i}.Position(2:end-1))
        JointPoseFlex(:,i) = jointStatesFlex{i}.Position(2:end-1);
        JointPoseFlex(4,i) = -JointPoseFlex(4,i);
        JointVelocityFlex(:,i) = jointStatesFlex{i}.Velocity(2:end-1);
        JointVelocityFlex(4,i) = -JointVelocityFlex(4,i);
        JointTorqueFlex(:,i) = jointStatesFlex{i}.Effort(2:end-1);
        excludeFlex(i) = 1;
    end
end
excludeFlex = logical(excludeFlex);

% Load the bag file with joint positions and torques Rigid assumption
bagRigid = rosbag(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\recordedBag_',type_of_traj,'TrajComparisonRigid_',dur,targ_freq,amp,'.bag']);
bagRigid = bagRigid.select('Topic','/robot/joint_states');
jointStatesRigid = bagRigid.readMessages;
nMessRigid = bagRigid.NumMessages;
timeRigid = zeros(1,nMessRigid);
JointPoseRigid = zeros(7,nMessRigid);
JointVelocityRigid = zeros(7,nMessRigid);
JointTorqueRigid = zeros(7,nMessRigid);
tiR = bagRigid.StartTime;
tfR = bagRigid.EndTime;
excludeRigid = zeros(1,nMessRigid);

for i = 1:nMessRigid
    timeRigid(i) = bagRigid.MessageList.Time(i)-tiR;
    if ~isempty(jointStatesRigid{i}.Position(2:end-1))
        JointPoseRigid(:,i) = jointStatesRigid{i}.Position(2:end-1);
        JointPoseRigid(4,i) = -JointPoseRigid(4,i);
        JointVelocityRigid(:,i) = jointStatesRigid{i}.Velocity(2:end-1);
        JointVelocityRigid(4,i) = -JointVelocityRigid(4,i);
        JointTorqueRigid(:,i) = jointStatesRigid{i}.Effort(2:end-1);
        excludeRigid(i) = 1;
    end
end
excludeRigid = logical(excludeRigid);

% Load the simulated result
joint_of_interest = [1 2 4];

poseVal = 0.025;
range = 0.5; % range around which the value is considered the same 1+-range)

% rigid model
RigidPoseFile = fopen(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\Sawyer3DofPose',type_of_traj,'Rigid_',dur,targ_freq,amp,'.txt'],'r');
RigidPoseData = textscan(RigidPoseFile,'%n %n %n %n','HeaderLines',1,'Delimiter',' ');
fclose(RigidPoseFile);
RigidVelocityFile = fopen(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\Sawyer3DofVelocity',type_of_traj,'Rigid_',dur,targ_freq,amp,'.txt'],'r');
RigidVelocityData = textscan(RigidVelocityFile,'%n %n %n %n','HeaderLines',1,'Delimiter',' ');
fclose(RigidVelocityFile);
RigidAccelerationFile = fopen(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\Sawyer3DofAcceleration',type_of_traj,'Rigid_',dur,targ_freq,amp,'.txt'],'r');
RigidAccelerationData = textscan(RigidAccelerationFile,'%n %n %n %n','HeaderLines',1,'Delimiter',' ');
fclose(RigidAccelerationFile);
RigidTorqueFile = fopen(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\Sawyer3DofTorque',type_of_traj,'Rigid_',dur,targ_freq,amp,'.txt'],'r');
RigidTorqueData = textscan(RigidTorqueFile,'%n %n %n %n','HeaderLines',1,'Delimiter',' ');
fclose(RigidTorqueFile);
SimRigidPose = [RigidPoseData{2} -RigidPoseData{3} RigidPoseData{4}];
SimRigidVelocity = [RigidVelocityData{2} -RigidVelocityData{3} RigidVelocityData{4}];
SimRigidAcceleration = [RigidAccelerationData{2} RigidAccelerationData{3} RigidAccelerationData{4}];
SimRigidTorque = [RigidTorqueData{2} -RigidTorqueData{3} -RigidTorqueData{4}];

rigidMeasIndex = find(and(JointPoseRigid(joint_of_interest(1),:)>(poseVal*(1-range)),JointPoseRigid(joint_of_interest(1),:)<=(poseVal*(1+range))),1);
rigidSimIndex = find(and(SimRigidPose(:,1)>(poseVal*(1-range)),SimRigidPose(:,1)<=(poseVal*(1+range))),1);
if isempty(rigidMeasIndex) || isempty(rigidSimIndex)
    timeShiftRigid = 0;
else
    timeShiftRigid = timeRigid(rigidMeasIndex)-RigidPoseData{1}(rigidSimIndex);
end
SimRigidTime = RigidPoseData{1}; %+timeShiftRigid;

% flexible model
FlexPoseFile = fopen(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\Sawyer3DofPose',type_of_traj,'Flex_',dur,targ_freq,amp,'.txt'],'r');
FlexPoseData = textscan(FlexPoseFile,'%n %n %n %n','HeaderLines',1,'Delimiter',' ');
fclose(FlexPoseFile);
FlexVelocityFile = fopen(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\Sawyer3DofVelocity',type_of_traj,'Flex_',dur,targ_freq,amp,'.txt'],'r');
FlexVelocityData = textscan(FlexVelocityFile,'%n %n %n %n','HeaderLines',1,'Delimiter',' ');
fclose(FlexVelocityFile);
FlexAccelerationFile = fopen(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\Sawyer3DofAcceleration',type_of_traj,'Flex_',dur,targ_freq,amp,'.txt'],'r');
FlexAccelerationData = textscan(FlexAccelerationFile,'%n %n %n %n','HeaderLines',1,'Delimiter',' ');
fclose(FlexAccelerationFile);
FlexTorqueFile = fopen(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\Sawyer3DofTorque',type_of_traj,'Flex_',dur,targ_freq,amp,'.txt'],'r');
FlexTorqueData = textscan(FlexTorqueFile,'%n %n %n %n','HeaderLines',1,'Delimiter',' ');
fclose(FlexTorqueFile);
SimFlexPose = [FlexPoseData{2} -FlexPoseData{3} FlexPoseData{4}];
SimFlexVelocity = [FlexVelocityData{2} -FlexVelocityData{3} FlexVelocityData{4}];
SimFlexAcceleration = [FlexAccelerationData{2} FlexAccelerationData{3} FlexAccelerationData{4}];
SimFlexTorque = [FlexTorqueData{2} -FlexTorqueData{3} -FlexTorqueData{4}];

flexMeasIndex = find(and(JointPoseFlex(joint_of_interest(1),:)>(poseVal*(1-range)),JointPoseFlex(joint_of_interest(1),:)<=(poseVal*(1+range))),1);
flexSimIndex = find(and(SimFlexPose(:,1)>(poseVal*(1-range)),SimFlexPose(:,1)<=(poseVal*(1+range))),1);
if isempty(flexMeasIndex) || isempty(flexSimIndex)
    timeShiftFlex = 0;
else
    timeShiftFlex = timeFlex(flexMeasIndex)-FlexPoseData{1}(flexSimIndex);
end
SimFlexTime = FlexPoseData{1}; %+timeShiftFlex;

% Plot results

axesLim = [0 3]; % Axis range to zoom of zone of interest
selec = 1; % select 1 value every 15 to have less markers
plotMeas_rigid = 1;
plotMeas_flex = 1;
plotSim_rigid = 0;
plotSim_flex = 0;
putTitle = 0;
lineType = {'-','--',':','-.'};

leg = {};

figure
hold on
i = 0;
if plotMeas_flex
i = i+1;
plot(timeFlex(excludeFlex)-timeShiftFlex,JointPoseFlex(joint_of_interest,excludeFlex),lineType{i},'Linewidth',2)
set(gca,'ColorOrderIndex',1)
leg = [leg, '1', '2', '3'];
end
if plotMeas_rigid
i = i+1;
plot(timeRigid(excludeRigid)-timeShiftRigid,JointPoseRigid(joint_of_interest,excludeRigid),lineType{i},'Linewidth',2)
set(gca,'ColorOrderIndex',1)
leg = [leg, '1R', '2R', '3R'];
end
if plotSim_flex
i = i+1;
plot(SimFlexTime(1:selec:end),SimFlexPose(1:selec:end,:),lineType{i},'Linewidth',2,'Markersize',4)
set(gca,'ColorOrderIndex',1)
leg = [leg, 'Sim1', 'Sim2', 'Sim3'];
end
if plotSim_rigid
i = i+1;
plot(SimRigidTime(1:selec:end),SimRigidPose(1:selec:end,:),lineType{i},'Linewidth',2,'Markersize',4)
set(gca,'ColorOrderIndex',1)
leg = [leg, 'Sim1R', 'Sim2R', 'Sim3R'];
end
xlabel('Time [s]','Fontsize',14)
ylabel('Joint position [rad]','Fontsize',14)
if putTitle
    if strcmp(type_of_traj,'Vibration')
        title(['Joint position comparison for time ',dur,', freq ',num2str(target_freq),' Hz',' and ampl ',num2str(amplitude*100),' cm'])
    else
        title(['Joint position comparison for time ',dur])
    end
end
% legend('1','2','3','4','5','6','7','1R','2R','3R','4R','5R','6R','7R')
% legend('1','2','3','1R','2R','3R','sim1R','sim2R','sim3R','sim1F','sim2F','sim3F')
legend(leg,'Location','Best')
xlim(axesLim)
grid on

i = 0;
figure
hold on
if plotMeas_flex
i = i+1;
plot(timeFlex(excludeFlex)-timeShiftFlex,JointTorqueFlex(joint_of_interest,excludeFlex),lineType{i},'Linewidth',2)
set(gca,'ColorOrderIndex',1)
end
if plotMeas_rigid
i = i+1;
plot(timeRigid(excludeRigid)-timeShiftRigid,JointTorqueRigid(joint_of_interest,excludeRigid),lineType{i},'Linewidth',2)
set(gca,'ColorOrderIndex',1)
end
if plotSim_flex
i = i+1;
plot(SimFlexTime(1:selec:end),SimFlexTorque(1:selec:end,:),lineType{i},'Linewidth',2,'Markersize',4)
set(gca,'ColorOrderIndex',1)
end
if plotSim_rigid
i = i+1;
plot(SimRigidTime(1:selec:end),SimRigidTorque(1:selec:end,:),lineType{i},'Linewidth',2,'Markersize',4)
set(gca,'ColorOrderIndex',1)
end
xlabel('Time [s]','Fontsize',14)
ylabel('Joint Torque [Nm]','Fontsize',14)
if putTitle
    if strcmp(type_of_traj,'Vibration')
        title(['Joint torque comparison for time ',dur,', freq ',num2str(target_freq),' Hz',' and ampl ',num2str(amplitude*100),' cm'])
    else
        title(['Joint torque comparison for time ',dur])
    end
end
% legend('1','2','3','4','5','6','7','1R','2R','3R','4R','5R','6R','7R')
% legend('1','2','3','1R','2R','3R','sim1R','sim2R','sim3R','sim1F','sim2F','sim3F')
legend(leg,'Location','Best')
xlim(axesLim)
grid on

i = 0;
figure
hold on
if plotMeas_flex
i = i+1;
plot(timeFlex(excludeFlex)-timeShiftFlex,JointVelocityFlex(joint_of_interest,excludeFlex),lineType{i},'Linewidth',2)
set(gca,'ColorOrderIndex',1)
end
if plotMeas_rigid
i = i+1;
plot(timeRigid(excludeRigid)-timeShiftRigid,JointVelocityRigid(joint_of_interest,excludeRigid),lineType{i},'Linewidth',2)
set(gca,'ColorOrderIndex',1)
end
if plotSim_flex
i = i+1;
plot(SimFlexTime(1:selec:end),SimFlexVelocity(1:selec:end,:),lineType{i},'Linewidth',2,'Markersize',4)
set(gca,'ColorOrderIndex',1)
end
if plotSim_rigid
i = i+1;
plot(SimRigidTime(1:selec:end),SimRigidVelocity(1:selec:end,:),lineType{i},'Linewidth',2,'Markersize',4)
set(gca,'ColorOrderIndex',1)
end
xlabel('Time [s]','Fontsize',14)
ylabel('Joint Velocity [rad/s]','Fontsize',14)
if putTitle
    if strcmp(type_of_traj,'Vibration')
        title(['Joint Velocity comparison for time ',dur,', freq ',num2str(target_freq),' Hz',' and ampl ',num2str(amplitude*100),' cm'])
    else
        title(['Joint Velocity comparison for time ',dur])
    end
end
% legend('1','2','3','1R','2R','3R','sim1R','sim2R','sim3R','sim1F','sim2F','sim3F')
% legend('sim1R','sim2R','sim3R','sim1F','sim2F','sim3F')
legend(leg,'Location','Best')
xlim(axesLim)
grid on

if plotSim_rigid || plotSim_flex
    i = 0;
    figure
    hold on
    if plotSim_flex
    i = i+1;
    set(gca,'ColorOrderIndex',1)
    plot(SimFlexTime(1:selec:end),SimFlexAcceleration(1:selec:end,:),lineType{i},'Linewidth',2)
    end
    if plotSim_rigid
    i = i+1;
    set(gca,'ColorOrderIndex',1)
    plot(SimRigidTime(1:selec:end),SimRigidAcceleration(1:selec:end,:),lineType{i},'Linewidth',2)
    end
    xlabel('Time [s]','Fontsize',14)
    ylabel('Joint acceleration [rad/s^2]','Fontsize',14)
    title('Joint acceleration comparison')
    legend('sim1F','sim2F','sim3F','sim1R','sim2R','sim3R','Location','Best')
    grid on
end

% %% Make a video of the vibrations (on torque)
% flexTimeMeas = timeFlex(excludeFlex)-timeShiftFlex;
% rigidTimeMeas = timeRigid(excludeRigid)-timeShiftRigid;
% torque1 = JointTorqueFlex(joint_of_interest(1),excludeFlex);
% torque2 = JointTorqueFlex(joint_of_interest(2),excludeFlex);
% torque3 = JointTorqueFlex(joint_of_interest(3),excludeFlex);
% torque1R = JointTorqueRigid(joint_of_interest(1),excludeRigid);
% torque2R = JointTorqueRigid(joint_of_interest(2),excludeRigid);
% torque3R = JointTorqueRigid(joint_of_interest(3),excludeRigid);
% y_lim_max = max([max(torque1),max(torque2),max(torque3),max(torque1R),max(torque2R),max(torque3R)]);
% y_lim_min = min([min(torque1),min(torque2),min(torque3),min(torque1R),min(torque2R),min(torque3R)]);
% x_lim_max = max(max(flexTimeMeas),max(rigidTimeMeas));
% x_lim_min = min(min(flexTimeMeas),min(rigidTimeMeas));
% 
% f = figure;
% u1 = animatedline('Color',[0    0.4470    0.7410]);
% u2 = animatedline('Color',[0.8500    0.3250    0.0980]);
% u3 = animatedline('Color',[0.9290    0.6940    0.1250]);
% u1R = animatedline('Color',[0    0.4470    0.7410],'LineStyle','--');
% u2R = animatedline('Color',[0.8500    0.3250    0.0980],'LineStyle','--');
% u3R = animatedline('Color',[0.9290    0.6940    0.1250],'LineStyle','--');
% legend('1','2','3','1R','2R','3R')
% hold on
% % plot sim
% 
% v = VideoWriter(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\Video\torqueSim_',type_of_traj,'_',dur,targ_freq,amp,'.avi']);
% v.FrameRate = 25; 
% open(v);
% startIndex = find(flexTimeMeas==0,1);
% nTime = min(length(flexTimeMeas),length(rigidTimeMeas));
% for i = 1:nTime
%     addpoints(u1,flexTimeMeas(i),torque1(i));
%     addpoints(u2,flexTimeMeas(i),torque2(i));
%     addpoints(u3,flexTimeMeas(i),torque3(i));
%     addpoints(u1R,rigidTimeMeas(i),torque1R(i));
%     addpoints(u2R,rigidTimeMeas(i),torque2R(i));
%     addpoints(u3R,rigidTimeMeas(i),torque3R(i));
% %     plot(x_sim_1,y_sim_1,'black',x_sim_2,y_sim_2,'black')
%     xlim([x_lim_min, x_lim_max]);
%     ylim([y_lim_min, y_lim_max]);
%     M(i) = getframe(f);
% %     pause(0.1/norm_velocity(k));
% end
% writeVideo(v,M)
% close(v)


