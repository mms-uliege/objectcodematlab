%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simplified 3dof model of Sawyer robot with elastic joints.
% Geometry and inertia infos coming from CAD solidworks model.

% --!-- Y axis is vertical like CAD model and X axis is toward the front of
% the robot (In Gantry position) --!--

clear all
% close all
% listM = [1 1 1];
% listKt = [1 1 1];
% listCt = [1 1 1];
% listPhiq = [1 1 1];
type_of_traj = 'Circle'; % either the 'Circle' trajectory, or 'Vibration' trajectory (divided in 2sections)
target_freq = 0.3; % [Hz] frequency at which arm should vibrate
amplitude = 0.12; % amplitude of the vibration 0.05 initialy
amp = ['_',num2str(100*amplitude)]; % Save amplitude info as cm
targ_freq = ['_',num2str(10*target_freq)]; % save frequency info as tenth of a Hz
if ~strcmp(type_of_traj,'Vibration')
    amp = '';
    targ_freq = '';
end
dur = '20';
preAct = 0.3; % duration of pre actuation phase
postAct = 0.2; % duration of pre actuation phase
finaltime = str2double(dur)/10 + preAct + postAct; % sec
timestepsize = 0.01; % sec
initial_time = 0.1; % Time to cut out at the beginning of the simulation

t_i = preAct; % time at end of pre-actuation
t_f = finaltime - postAct; % time at begining of post-actuation

rho_num = 0;

load(['C:\ObjectCodeMatlab\TestsArthur\SawyerLumpedMass\D3dof_',type_of_traj,'_',dur,targ_freq,amp])
load(['C:\ObjectCodeMatlab\TestsArthur\SawyerLumpedMass\S3dof_',type_of_traj,'_',dur,targ_freq,amp])
% Forces
uRigi.u1 = D.model.listElementVariables{end}.value(1,:);
uRigi.u2 = D.model.listElementVariables{end}.value(2,:);
uRigi.u3 = D.model.listElementVariables{end}.value(3,:);
uFlex.u1 = S.model.listElementVariables{end}.value(1,:);
uFlex.u2 = S.model.listElementVariables{end}.value(2,:);
uFlex.u3 = S.model.listElementVariables{end}.value(3,:);
% joint traj
uRigi.j1 = D.model.listElementVariables{1}.relCoo;
uRigi.j2 = D.model.listElementVariables{3}.relCoo;
uRigi.j3 = D.model.listElementVariables{5}.relCoo;
uFlex.j1 = S.model.listElementVariables{1}.relCoo;
uFlex.j2 = S.model.listElementVariables{3}.relCoo;
uFlex.j3 = S.model.listElementVariables{5}.relCoo;

%% parameters
stiff1 = 0;
damp1 = 0; % Nms/rad
coul1 = 0; % Nm ----- from urdf = 5
stiff1Bis = 750; % Comming from mean values of measured stiffnesses (+-750) previous was 739
damp1Bis = 10; % Nms/rad ----- from urdf = 10
coul1Bis = 0; % Nm

stiff2 = 0;
damp2 = 0; % Nms/rad
coul2 = 0; % Nm ----- from urdf = 2
stiff2Bis = 1000; % Comming from mean values of measured stiffnesses (+-1000) previous was 963
damp2Bis = 5; % Nms/rad ----- from urdf = 5
coul2Bis = 0; % Nm

stiff3 = 0;
damp3 = 0; % Nms/rad
coul3 = 0; % Nm ----- from urdf = 0.5
stiff3Bis = 450; % Comming from mean values of measured stiffnesses (+-450) previous was 364
damp3Bis = 1; % Nms/rad ----- from urdf = 1
coul3Bis = 0; % Nm

grav = [0 -9.81 0]; % gravity in the vertical Y axis
%% joint 1
joint1.axis = [0 0 0 0 1 0]'; % 1st joint along vertical Y axis part 1
joint1.stiff = stiff1;
joint1.damp = damp1; % Nms/rad
joint1.coul = coul1; % Nm

joint1Bis.axis = [0 0 0 0 1 0]'; % 1st joint along vertical Y axis part 2
joint1Bis.stiff = stiff1Bis;
joint1Bis.damp = damp1Bis; % Nms/rad
joint1Bis.coul = coul1Bis; % Nm
%% joint 2
joint2.axis = [0 0 0 0 0 1]'; % 2nd joint along horizontal Z axis part 1
joint2.stiff = stiff2;
joint2.damp = damp2; % Nms/rad
joint2.coul = coul2; % Nm

joint2Bis.axis = [0 0 0 0 0 1]'; % 2nd joint along horizontal Z axis part 2
joint2Bis.stiff = stiff2Bis;
joint2Bis.damp = damp2Bis; % Nms/rad
joint2Bis.coul = coul2Bis; % Nm
%% joint 3
joint3.axis = [0 0 0 0 0 1]'; % 3rd joint along horizontal X axis part 1
joint3.stiff = stiff3;
joint3.damp = damp3; % Nms/rad
joint3.coul = coul3; % Nm

joint3Bis.axis = [0 0 0 0 0 1]'; % 3rd joint along horizontal X axis part 2
joint3Bis.stiff = stiff3Bis;
joint3Bis.damp = damp3Bis; % Nms/rad
joint3Bis.coul = coul3Bis; % Nm

%% link 1
link1.m = 6.1704; % kg
link1.J = [0.1194 0.0008 -0.0012;...
           0.0008 0.0205 -0.0025;...
           -0.0012 -0.0025 0.1228]; % kg/m^2
link1.nodes = [0 0.092 0;...
               0.0239 0.2984 0.0030;...               
               0.0810 0.3170 -0.0595]; % m
link1.g = grav;

%% link 2
link2.m = 5.182; % kg
link2.J = [0.0142 0 -0.0135;...
           0 0.1164 0;...
           -0.0135 0 0.1111]; % kg/m^2
link2.nodes = [0.0810 0.3170 -0.0595;...
               0.2105 0.3170 -0.1707;...               
               0.4810 0.3170 -0.1500]; % m
link2.g = grav;

%% link 3
link3.m = 5.0088; % kg
link3.J = [0.0244 -0.0096 -0.0204;...
           -0.0096 0.1577 0.0045;...
           -0.0204 0.0045 0.1463]; % kg/m^2
link3.nodes = [0.4810 0.3170 -0.1500;...
               0.6970 0.3070 -0.0704;...
               0.8810 0.1833 -0.1603]; % m
link3.g = grav;

%% Flexible simulation

nodes = [link1.nodes(1,:);...
         link1.nodes(1,:);...
         link1.nodes;...
         link2.nodes(1,:);...
         link2.nodes;...
         link3.nodes(1,:);...
         link3.nodes];

% number all nodes properly so they can be used by FEMmodel
nodes = [1:size(nodes,1);nodes']';

count = 1;
% creating 1st rigid body (rotating aroud y)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [4 3 5];
elements{count}.mass = link1.m;
elements{count}.J = link1.J;
elements{count}.g = link1.g;
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;
count = count +1;

% creating 2nd rigid body (rotating aroud z)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [8 7 9];
elements{count}.mass = link2.m;
elements{count}.J = link2.J;
elements{count}.g = link2.g;
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;
count = count +1;

% creating 3rd rigid body (rotating aroud z)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [12 11 13];
elements{count}.mass = link3.m;
elements{count}.J = link3.J;
elements{count}.g = link3.g;
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;
count = count +1;

% creating 1st joint (rotating aroud y) part 1
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = joint1.axis;
elements{count}.k = joint1.stiff;
elements{count}.d = joint1.damp;
elements{count}.coulomb = joint1.coul;
count = count +1;

% creating 1st joint (rotating aroud y) part 2
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.A = joint1Bis.axis;
elements{count}.k = joint1Bis.stiff;
elements{count}.d = joint1Bis.damp;
elements{count}.coulomb = joint1Bis.coul;
count = count +1;

% creating 2nd joint (rotating aroud z) part 1
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [5 6];
elements{count}.A = joint2.axis;
elements{count}.k = joint2.stiff;
elements{count}.d = joint2.damp;
elements{count}.coulomb = joint2.coul;
count = count +1;

% creating 2nd joint (rotating aroud z) part 2
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [6 7];
elements{count}.A = joint2Bis.axis;
elements{count}.k = joint2Bis.stiff;
elements{count}.d = joint2Bis.damp;
elements{count}.coulomb = joint2Bis.coul;
count = count +1;

% creating 3rd joint (rotating aroud z) part 1
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [9 10];
elements{count}.A = joint3.axis;
elements{count}.k = joint3.stiff;
elements{count}.d = joint3.damp;
elements{count}.coulomb = joint3.coul;
count = count +1;

% creating 3rd joint (rotating aroud z) part 2
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [10 11];
elements{count}.A = joint3Bis.axis;
elements{count}.k = joint3Bis.stiff;
elements{count}.d = joint3Bis.damp;
elements{count}.coulomb = joint3Bis.coul;
count = count +1;


% Trajectory (effort on joint or trajectory on joint)

r = 0.2;
z_end = nodes(end,4);
x_end = nodes(end,2)-2*r;

timeVector = 0:timestepsize:finaltime;
if strcmp(type_of_traj,'Circle')
    % Half circle trajectory of the arm
    trajy = nodes(end,3)*ones(size(timeVector));
    trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);
    trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'sin',t_i,t_f);
elseif strcmp(type_of_traj,'Vibration')
    % Sine/cosine vibration of the arm
    % trajectory divided into sections, first section would be to move the arm
    % to a starting position, second section would be to follow a given
    % trajectory
    blending_time1 = 0.45;
    blending_time2 = 0.45;
    waiting_time_1 = 0.1;
    waiting_time_2 = 0.1;
    approach_time = round(finaltime/4,1);
    dist_x = amplitude*3/5; % tested for 0.05 initially
    section1_ti = initial_time + waiting_time_1; % end of 1st waiting step (before going towards body)
    section1_tf = section1_ti + approach_time; % end of 1st phase (then comes some waiting time 1)
%     section2_ti = section1_tf + waiting_time_1; % start of 2nd phase
    section2_ti = section1_ti + approach_time/2; % start of 2nd phase
    section2_tf = finaltime - waiting_time_2; % end of 2nd phase, then comes post actuation
    section1_i_index = 1; % global starting time index
    section1_f_index = round(section1_tf/timestepsize); % global ending time index of the 1st phase
    section2_i_index = round(section2_ti/timestepsize); % global starting time index of the 2nd phase
    section2_f_index = round(finaltime/timestepsize + 1); % global ending time index of whole traj
    % section 1 (some pre actuation phase and a post actuation/waiting
    % time)
    trajy = smoothSineTraj(nodes(end,3),target_freq,amplitude,timeVector,section2_ti,section2_ti+blending_time1,section2_tf-blending_time2,section2_tf);
%     trajz = nodes(end,4)*ones(size(timeVector));
    trajz = smoothSineTraj(nodes(end,4),target_freq,amplitude,timeVector,section2_ti,section2_ti+blending_time1,section2_tf-blending_time2,section2_tf);
    trajx = lineTraj(nodes(end,2),nodes(end,2)-dist_x,timeVector,section1_ti,section1_tf);
    % section 2 (sine oscillation with smooth blending, without pre
    % actuation but with post actuation)
    %     trajz(section2_i_index:section2_f_index) = smoothSineTraj(trajz(section1_f_index),target_freq,amplitude,timeVector(section2_i_index:section2_f_index),section2_ti,section2_ti+blending_time,section2_tf-blending_time,section2_tf);
%     plot(timeVector,[trajx;trajy;trajz])
%     plot(timeVector,[deriveVal(trajx,timestepsize);deriveVal(trajy,timestepsize);deriveVal(trajz,timestepsize)])
%     plot(timeVector,[deriveVal(deriveVal(trajx,timestepsize),timestepsize);deriveVal(trajy,timestepsize);deriveVal(trajz,timestepsize)])
end

% count = size(elements,2)+1;
% elements{count}.type = 'ForceInKinematicConstraint';
% elements{count}.elements = [count-6];
% elements{count}.f = uRigi.u1;
% % elements{count}.ref = uRigi.j1;
% % elements{count}.kp = 10;
% % elements{count}.kd = 1;
% count = count+1;
% 
% elements{count}.type = 'ForceInKinematicConstraint';
% elements{count}.elements = [count-5];
% elements{count}.f = uRigi.u2;
% % elements{count}.ref = uRigi.j2;
% % elements{count}.kp = 10;
% % elements{count}.kd = 1;
% count = count+1;
% 
% elements{count}.type = 'ForceInKinematicConstraint';
% elements{count}.elements = [count-4];
% elements{count}.f = uRigi.u3;
% % elements{count}.ref = uRigi.j3;
% % elements{count}.kp = 10;
% % elements{count}.kd = 1;

count = size(elements,2)+1;
elements{count}.type = 'TrajectoryConstraintJoint';
elements{count}.elements = [count-6];
elements{count}.T = uRigi.j1;
count = count+1;

elements{count}.type = 'TrajectoryConstraintJoint';
elements{count}.elements = [count-5];
elements{count}.T = uRigi.j2;
count = count+1;

elements{count}.type = 'TrajectoryConstraintJoint';
elements{count}.elements = [count-4];
elements{count}.T = uRigi.j3;


t = find(S.timeValues==0,1);
in = t; % time step choosen for initial condition
initialCondition = [];
for n = S.model.listNumberNodes
    x = S.model.listNodes{n}.position(:,in);
    R = dimR(S.model.listNodes{n}.R(:,in));
    H = [R x; 0 0 0 1];
    h = logSE3(H);
    initialCondition = [initialCondition; [S.model.listNodes{n}.listNumberDof' h S.model.listNodes{n}.velocity(:,in) S.model.listNodes{n}.acceleration(:,in)]];
end
for n = S.model.listNumberElementVariables
    if strcmp(S.model.listElementVariables{n}.DofType,'MotionDof')
        initialCondition = [initialCondition; [S.model.listElementVariables{n}.listNumberDof' S.model.listElementVariables{n}.relCoo(:,in) S.model.listElementVariables{n}.velocity(:,in) S.model.listElementVariables{n}.acceleration(:,in)]];
    elseif strcmp(S.model.listElementVariables{n}.DofType,'LagrangeMultiplier')
        initialCondition = [initialCondition; [S.model.listElementVariables{n}.listNumberDof' zeros(S.model.listElementVariables{n}.nL,1) S.model.listElementVariables{n}.value(:,in) zeros(size(S.model.listElementVariables{n}.value(:,in)))]];
    end
end


% Boundary Condition
BC = [1];

% Solving
ModelRD = FEModel();
ModelRD.CreateFEModel(nodes,elements);
ModelRD.defineBC(BC);

DDR = DynamicIntegration(ModelRD);
DDR.parameters.finaltime = finaltime;
DDR.parameters.timestepsize = timestepsize;
DDR.parameters.rho = rho_num;
% DDR.parameters.relTolRes = 1e-10;
DDR.parameters.scaling = 1e6;
% DDR.runIntegration();
DDR.runIntegration(initialCondition);
save(['C:\ObjectCodeMatlab\TestsArthur\SawyerLumpedMass\DDR3dof_',type_of_traj,'_',dur,targ_freq,amp],'DDR')


% elements{10}.f = uFlex.u1;
% % elements{10}.ref = uFlex.j1;
% elements{11}.f = uFlex.u2;
% % elements{11}.ref = uFlex.j2;
% elements{12}.f = uFlex.u3;
% % elements{12}.ref = uFlex.j3;

elements{10}.T = uFlex.j1;
elements{11}.T = uFlex.j2;
elements{12}.T = uFlex.j3;

% Solving
ModelFD = FEModel();
ModelFD.CreateFEModel(nodes,elements);
ModelFD.defineBC(BC);

DDF = DynamicIntegration(ModelFD);
DDF.parameters.finaltime = finaltime;
DDF.parameters.timestepsize = timestepsize;
DDF.parameters.rho = rho_num;
% DD.parameters.relTolRes = 1e-10;
DDF.parameters.scaling = 1e6;
% DDF.runIntegration();
DDF.runIntegration(initialCondition);
save(['C:\ObjectCodeMatlab\TestsArthur\SawyerLumpedMass\DDF3dof_',type_of_traj,'_',dur,targ_freq,amp],'DDF')


%% Plots
time = 0:timestepsize:finaltime;
% Rigid model
endNodeRigid = ModelRD.listNodes{end};
joint1Rigid = ModelRD.listElementVariables{1};
joint2Rigid = ModelRD.listElementVariables{2};
joint3Rigid = ModelRD.listElementVariables{3};
joint4Rigid = ModelRD.listElementVariables{4};
joint5Rigid = ModelRD.listElementVariables{5};
joint6Rigid = ModelRD.listElementVariables{6};
% Flexible model
endNodeFlex = ModelFD.listNodes{end};
joint1Flex = ModelFD.listElementVariables{1};
joint2Flex = ModelFD.listElementVariables{2};
joint3Flex = ModelFD.listElementVariables{3};
joint4Flex = ModelFD.listElementVariables{4};
joint5Flex = ModelFD.listElementVariables{5};
joint6Flex = ModelFD.listElementVariables{6};


% Plot motor joint angles
figure
hold on
plot(time,[joint1Flex.relCoo;joint3Flex.relCoo;joint5Flex.relCoo])
set(gca,'ColorOrderIndex',1)
plot(time,[joint1Rigid.relCoo;joint3Rigid.relCoo;joint5Rigid.relCoo],'--')
xlabel('Time (s)')
ylabel('Motor joint position (rad)')
title('Comparison of motor joint positions')
legend('1','3','5','1R','3R','5R')
grid on

% Plot motor joint velocities
figure
hold on
plot(time,[joint1Flex.velocity;joint3Flex.velocity;joint5Flex.velocity])
set(gca,'ColorOrderIndex',1)
plot(time,[joint1Rigid.velocity;joint3Rigid.velocity;joint5Rigid.velocity],'--')
xlabel('Time (s)')
ylabel('Motor joint velocities (rad/s)')
title('Comparison of motor joint velocities')
legend('1','3','5','1R','3R','5R')
grid on

% Plot motor joint accelerations
figure
hold on
plot(time,[joint1Flex.acceleration;joint3Flex.acceleration;joint5Flex.acceleration])
set(gca,'ColorOrderIndex',1)
plot(time,[joint1Rigid.acceleration;joint3Rigid.acceleration;joint5Rigid.acceleration],'--')
xlabel('Time (s)')
ylabel('Motor joint accelerations (rad/s^2)')
title('Comparison of motor joint accelerations')
legend('1','3','5','1R','3R','5R')
grid on

% Plot flex joint angles
figure
hold on
plot(time,[joint2Flex.relCoo;joint4Flex.relCoo;joint6Flex.relCoo])
set(gca,'ColorOrderIndex',1)
plot(time,[joint2Rigid.relCoo;joint4Rigid.relCoo;joint6Rigid.relCoo],'--')
xlabel('Time (s)')
ylabel('Flex joint position (rad)')
title('Comparison of flex joint positions')
legend('2','4','6','2R','4R','6R')
grid on

leg = {};
figure
hold on
plot3(trajx,trajy,trajz)
leg = [leg,'Desired'];
plot3(endNodeRigid.position(1,:),endNodeRigid.position(2,:),endNodeRigid.position(3,:))
leg = [leg,'Rigid'];
plot3(endNodeFlex.position(1,:),endNodeFlex.position(2,:),endNodeFlex.position(3,:))
leg = [leg,'Flex'];
xlabel('X (m)')
ylabel('Y (m)')
zlabel('Z (m)')
legend(leg)
title('Desired trajectory of the robot')
grid on

% Calculating error
RelError = zeros(size(timeVector));
RelErrorR = zeros(size(timeVector));
for i = 1:length(RelError)
    RelError(i) = 100*(norm([trajx(i)-endNodeFlex.position(1,i) trajy(i)-endNodeFlex.position(2,i) trajz(i)-endNodeFlex.position(3,i)])/norm([trajx(i) trajy(i) trajz(i)]));
    RelErrorR(i) = 100*(norm([trajx(i)-endNodeRigid.position(1,i) trajy(i)-endNodeRigid.position(2,i) trajz(i)-endNodeRigid.position(3,i)])/norm([trajx(i) trajy(i) trajz(i)]));
end
%     figure
%     plot(timeVector,RelError,timeVector,RelErrorR)
%     title('Relative error of the direct dynamic trajectory','Fontsize',13)
%     xlabel('Time')
%     ylabel('Relative Error (%)')
%     legend('Flex','Rigid')
%     grid on

RMSRelError = sqrt(mean(RelError.^2))
MaxRelError = max(RelError)
RMSRelErrorR = sqrt(mean(RelErrorR.^2))
MaxRelErrorR = max(RelErrorR)
