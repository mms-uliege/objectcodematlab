%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compare LAMH Codamotion measurement with desired trajectory

% close all
clear ref
clear flexCoda
clear rigidCoda

type_of_traj = 'Vibration'; % either the 'Circle' trajectory, or 'Vibration' trajectory (divided in 2sections)
target_freq = 0.3; % [Hz] frequency at which arm should vibrate
amplitude = 0.12; % amplitude of the vibration 0.05 initialy
amp = ['_',num2str(100*amplitude)]; % Save amplitude info as cm
targ_freq = ['_',num2str(10*target_freq)]; % save frequency info as tenth of a Hz
if ~strcmp(type_of_traj,'Vibration')
    amp = '';
    targ_freq = '';
end
dur = '20';

poseVal = -0.4;
range = 0.005; % range around which the value is considered the same 1+-range)
axis2look = 3;

%% IMPORT MEASUREMENTS
% Import Reference Measurements
Ref_CodaFile = fopen(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\Measurements_LAMH\reference.txt'],'r');
Ref_CodaData = textscan(Ref_CodaFile,'%n %n %n %n %n %n %n %n %n %n %n %n %n %n %n %n','HeaderLines',6,'Delimiter',' ');
fclose(Ref_CodaFile);
ref.time = Ref_CodaData{1};
ref.point1 = [Ref_CodaData{2} Ref_CodaData{4} -Ref_CodaData{3}];
ref.point2 = [Ref_CodaData{5} Ref_CodaData{7} -Ref_CodaData{6}];
ref.point3 = [Ref_CodaData{8} Ref_CodaData{10} -Ref_CodaData{9}];
ref.point16 = [Ref_CodaData{14} Ref_CodaData{16} -Ref_CodaData{15}];
ref.p1 = [mean(ref.point1(:,1)) mean(ref.point1(:,2)) mean(ref.point1(:,3))]/1000;
ref.p2 = [mean(ref.point2(:,1)) mean(ref.point2(:,2)) mean(ref.point2(:,3))]/1000;
ref.p3 = [mean(ref.point3(:,1)) mean(ref.point3(:,2)) mean(ref.point3(:,3))]/1000;
ref.p16 = [mean(ref.point16(:,1)) mean(ref.point16(:,2)) mean(ref.point16(:,3))]/1000;
% ref.p1 = ref.point1(1,1:3)/1000;
% ref.p2 = ref.point2(1,1:3)/1000;
% ref.p3 = ref.point3(1,1:3)/1000;

% Import Flexible Measurements
FlexEnd_CodaFile = fopen(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\Measurements_LAMH\',type_of_traj,'Flex_',dur,targ_freq,amp,'_2','.txt'],'r');
FlexEnd_CodaData = textscan(FlexEnd_CodaFile,'%n %n %n %n %n %n %n %n %n %n %n %n %n %n %n %n','HeaderLines',6,'Delimiter',' ');
fclose(FlexEnd_CodaFile);
flexCoda.time = FlexEnd_CodaData{1};
flexCoda.timeStep = flexCoda.time(2)-flexCoda.time(1);
flexCoda.endPoint = [FlexEnd_CodaData{14} FlexEnd_CodaData{16} -FlexEnd_CodaData{15}]/1000;

% Import Rigid Measurements
RigidEnd_CodaFile = fopen(['C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\Measurements_LAMH\',type_of_traj,'Rigid_',dur,targ_freq,amp,'_2','.txt'],'r');
RigidEnd_CodaData = textscan(RigidEnd_CodaFile,'%n %n %n %n %n %n %n %n %n %n %n %n %n %n %n %n','HeaderLines',6,'Delimiter',' ');
fclose(RigidEnd_CodaFile);
rigidCoda.time = RigidEnd_CodaData{1};
rigidCoda.timeStep = rigidCoda.time(2)-rigidCoda.time(1);
rigidCoda.endPoint = [RigidEnd_CodaData{14} RigidEnd_CodaData{16} -RigidEnd_CodaData{15}]/1000;

% % Import desired trajectory
% load(['C:\ObjectCodeMatlab\TestsArthur\SawyerLumpedMass\S3dof_',type_of_traj,'_',dur,targ_freq,amp,'.mat'])
% time = S.timeValues;
% trajx = S.model.listElements{end}.T(1,:);
% trajy = S.model.listElements{end}.T(2,:);
% trajz = S.model.listElements{end}.T(3,:);
% Import "desired trajectory of Coda Marker point
% load(['C:\ObjectCodeMatlab\TestsArthur\SawyerLumpedMass\D3dof_',type_of_traj,'_',dur,targ_freq,amp,'_wCodaNode','.mat'])
% time = D.parameters.time;
% trajDur = D.parameters.timestepsize*D.parameters.nstep;
% trajx = D.model.listNodes{end-1}.position(1,:);
% trajy = D.model.listNodes{end-1}.position(2,:);
% trajz = D.model.listNodes{end-1}.position(3,:);
load(['C:\ObjectCodeMatlab\TestsArthur\SawyerLumpedMass\S3dof_',type_of_traj,'_',dur,targ_freq,amp,'_wCodaNode','.mat'])
time = S.timeValues;
trajDur = S.parameters.timestepsize*S.parameters.nstep;
trajx = S.model.listNodes{end-1}.position(1,:);
trajy = S.model.listNodes{end-1}.position(2,:);
trajz = S.model.listNodes{end-1}.position(3,:);
% load(['C:\ObjectCodeMatlab\TestsArthur\SawyerLumpedMass\S3dof_',type_of_traj,'_',dur,targ_freq,amp,'_wCodaNode','.mat'])
% time = S.timeValues;
% trajDur = S.parameters.timestepsize*S.parameters.nstep;
% trajx = S.model.listNodes{end}.position(1,:);
% trajy = S.model.listNodes{end}.position(2,:);
% trajz = S.model.listNodes{end}.position(3,:);

%% Operations to convert measurment
% Geometric parameters to convert values
height_screw_head = 0.018; % height of the top of the screw head measured from base surface of the robot.
height_markers = 0.004; % distance between gluing surface of the markes and surface of emitting led.
p16 = [0.9481+height_markers 0.2131 -0.1603]; % point on the robot near the end-effector on which the "end" marker was sticked.
p15 = [0.8810 0.1833 -0.1603]; % end-effector of the robot used in the model

p10 = [ 0.0866  height_screw_head+height_markers  0.0359];
p20 = [-0.0359  height_screw_head+height_markers  0.0866];
p30 = [ 0.0359  height_screw_head+height_markers -0.0866];

% A = [p10 1 0 0 0 0 0;...
%       0 -p10(1) 0 0 p10(2:3) 1 0 0;...
%       0 0 -p10(1) 0 0 -p10(2) 0 p10(3) 1;...
%      p20 1 0 0 0 0 0;...
%       0 -p20(1) 0 0 p20(2:3) 1 0 0;...
%       0 0 -p20(1) 0 0 -p20(2) 0 p20(3) 1;...
%      p30 1 0 0 0 0 0;...
%       0 -p30(1) 0 0 p30(2:3) 1 0 0;...
%       0 0 -p30(1) 0 0 -p30(2) 0 p30(3) 1];
% b = [ref.p1';ref.p2';ref.p3'];
% x = A\b;
% x = linsolve(A,b);
% T_base2coda = [x(1:4)'; x(2) x(5:7)'; x(3) x(6) x(8:9)'; 0 0 0 1];

% Compute the vector pointing towards the center of the robot based on the
% Coda meaurements
center_p20_p30 = (p20+p30)/2; 
est_Rt = eye(3); % Estimated transformation pure geometry
est_xt = (ref.p2+ref.p3)/2;
est_xt(2) = est_xt(2)-center_p20_p30(2); % Center of the base of the robot estimated with coda measurements
est_T = [est_Rt est_xt';0 0 0 1]; % Transformation from the inertial frame of the coda, to the center of the robot 

% Test to see what is the angle (around Sawyer's Y axis)
test_ang = -20:0.01:20;
err_p1 = zeros(1,length(test_ang));
err_p2 = zeros(1,length(test_ang));
err_p3 = zeros(1,length(test_ang));
err_p16 = zeros(1,length(test_ang));
for a = 1:length(test_ang)
%     test_p1 = inv(troty(test_ang(a))*est_T)*[ref.p1 1]';
%     test_p2 = inv(troty(test_ang(a))*est_T)*[ref.p2 1]';
%     test_p3 = inv(troty(test_ang(a))*est_T)*[ref.p3 1]';
%     test_p16 = inv(troty(test_ang(a))*est_T)*[ref.p16 1]';
%     err_p1(a) = norm(abs(p10-test_p1(1:3)'))/norm(p10);
%     err_p2(a) = norm(abs(p20-test_p2(1:3)'))/norm(p20);
%     err_p3(a) = norm(abs(p30-test_p3(1:3)'))/norm(p30);
%     err_p16(a) = norm(abs(p16-test_p16(1:3)'))/norm(p16);
    
    test_p1 = est_T*troty(test_ang(a))*[p10 1]';
    test_p2 = est_T*troty(test_ang(a))*[p20 1]';
    test_p3 = est_T*troty(test_ang(a))*[p30 1]';
    test_p16 = est_T*troty(test_ang(a))*[p16 1]';
    err_p1(a) = norm(abs(ref.p1-test_p1(1:3)'))/norm(ref.p1);
    err_p2(a) = norm(abs(ref.p2-test_p2(1:3)'))/norm(ref.p2);
    err_p3(a) = norm(abs(ref.p3-test_p3(1:3)'))/norm(ref.p3);
    err_p16(a) = norm(abs(ref.p16-test_p16(1:3)'))/norm(ref.p16);
end
nAng_p1 = find(err_p1==min(err_p1));
nAng_p2 = find(err_p2==min(err_p2));
nAng_p3 = find(err_p3==min(err_p3));
nAng_p16 = find(err_p16==min(err_p16));
% plot error
figure
plot(test_ang,[err_p1;err_p2;err_p3;err_p16],'Linewidth',2)
xlabel('change in angle [deg]')
ylabel('Relative error on point')
title('Relative error on transformation matrix with respect to frame angle')
legend(['p1 (',num2str(test_ang(nAng_p1)),'�)'],['p2 (',num2str(test_ang(nAng_p2)),'�)'],['p3 (',num2str(test_ang(nAng_p3)),'�)'],['p16 (',num2str(test_ang(nAng_p16)),'�)'])
grid on

mean_ang = mean([test_ang(nAng_p1) test_ang(nAng_p2) test_ang(nAng_p3)]);
est_T = est_T*troty(test_ang(nAng_p16));
% est_T = est_T*troty(test_ang(nAng_p2));
% est_T = est_T*troty(mean_ang);


% Convert measured trajectories (convert sim to measure)
traj = [trajx;trajy;trajz];
traj_trans = zeros(size(traj));
for i = 1:size(traj,2)
    trans = est_T*[traj(:,i);1];
    traj_trans(:,i) = trans(1:3);
end
% Convert measured trajectories (convert measure to sim)
traj_transFlex = zeros(size(flexCoda.endPoint));
traj_transRigid = zeros(size(rigidCoda.endPoint));
for i = 1:size(traj_transFlex,1)
    trans_flex = est_T'*[flexCoda.endPoint(i,:) 1]';
    traj_transFlex(i,:) = trans_flex(1:3);
end
for i = 1:size(traj_transRigid,1)
    trans_rigid = est_T'*[rigidCoda.endPoint(i,:) 1]';
    traj_transRigid(i,:) = trans_rigid(1:3);
end


%% Find time shift
flexMeasIndex = find(and(flexCoda.endPoint(:,axis2look)>(poseVal*(1-sign(poseVal)*range)),flexCoda.endPoint(:,axis2look)<=(poseVal*(1+sign(poseVal)*range))),1);
rigidMeasIndex = find(and(rigidCoda.endPoint(:,axis2look)>(poseVal*(1-sign(poseVal)*range)),rigidCoda.endPoint(:,axis2look)<=(poseVal*(1+sign(poseVal)*range))),1);
flexSimIndex = find(and(traj_trans(axis2look,:)>(poseVal*(1-sign(poseVal)*range)),traj_trans(axis2look,:)<=(poseVal*(1+sign(poseVal)*range))),1);
if isempty(flexMeasIndex) || isempty(flexSimIndex) || isempty(rigidMeasIndex)
    timeShiftFlex = 0;
    timeShiftRigid = 0;
else
    timeShiftFlex = flexCoda.time(flexMeasIndex)-time(flexSimIndex);
    timeShiftRigid = flexCoda.time(rigidMeasIndex)-time(flexSimIndex);
end
flexCoda.time = flexCoda.time-timeShiftFlex;
rigidCoda.time = rigidCoda.time-timeShiftRigid;

% start_flexMeasIndex = find(flexCoda.time==0,1);
start_flexMeasIndex = find(and(flexCoda.time>(0-flexCoda.timeStep/2),flexCoda.time<(0+flexCoda.timeStep/2)),1);
start_rigidMeasIndex = find(and(rigidCoda.time>(0-rigidCoda.timeStep/2),rigidCoda.time<(0+rigidCoda.timeStep/2)),1);


eff_trajFlex = flexCoda.endPoint(start_flexMeasIndex:start_flexMeasIndex+round(trajDur/flexCoda.timeStep),:);
eff_trajRigid = rigidCoda.endPoint(start_flexMeasIndex:start_rigidMeasIndex+round(trajDur/rigidCoda.timeStep),:);

%% compute error on trajectory

% eff_trajFlex = zeros(3,length(time));
% eff_trajRigid = zeros(3,length(time));
err_flex = zeros(1,length(time));
err_rigid = zeros(1,length(time));
err_flex_YZ = zeros(1,length(time));
err_rigid_YZ = zeros(1,length(time));
for t = 1:length(err_flex)
    flex_index = find(and(flexCoda.time>(time(t)-flexCoda.timeStep/2),flexCoda.time<(time(t)+flexCoda.timeStep/2)));
    rigid_index = find(and(rigidCoda.time>(time(t)-rigidCoda.timeStep/2),rigidCoda.time<(time(t)+rigidCoda.timeStep/2)));

%     flex_point = flexCoda.endPoint(flex_index,:);
%     rigid_point = rigidCoda.endPoint(rigid_index,:);
    err_flex(t) = 100*(norm(traj_trans(:,t)'-flexCoda.endPoint(flex_index,:))/norm(traj_trans(:,t)));
    err_rigid(t) = 100*(norm(traj_trans(:,t)'-rigidCoda.endPoint(rigid_index,:))/norm(traj_trans(:,t)));
    err_flex_YZ(t) = 100*(norm(traj_trans(2:3,t)'-flexCoda.endPoint(flex_index,2:3))/norm(traj_trans(2:3,t)));
    err_rigid_YZ(t) = 100*(norm(traj_trans(2:3,t)'-rigidCoda.endPoint(rigid_index,2:3))/norm(traj_trans(2:3,t)));
end
figure
plot(time,[err_flex;err_rigid])
legend('Flex','Rigid')
title('Relative Error on trajectory')
RMSRelError = sqrt(mean(err_flex.^2))
MaxRelError = max(err_flex)
RMSRelErrorR = sqrt(mean(err_rigid.^2))
MaxRelErrorR = max(err_rigid)

figure
plot(time,[err_flex_YZ;err_rigid_YZ])
legend('Flex','Rigid')
title('Relative Error on trajectory in YZ plane')
RMSRelError_YZ = sqrt(mean(err_flex_YZ.^2))
MaxRelError_YZ = max(err_flex_YZ)
RMSRelErrorR_YZ = sqrt(mean(err_rigid_YZ.^2))
MaxRelErrorR_YZ = max(err_rigid_YZ)

%% plot measured coda trajectories
putTitle = 0;
lineType = {'-','--',':','-.'};
show_flexCoda = 1;
show_rigidCoda = 1;
show_desired = 1;
leg = {};

i = 0;
figure
hold on
if show_flexCoda
    i = i+1;
    set(gca,'ColorOrderIndex',1)
    plot(flexCoda.time,flexCoda.endPoint,lineType{i},'Linewidth',2)
    leg = [leg, 'x_{F,coda}', 'y_{F,coda}', 'z_{F,coda}'];
end
if show_rigidCoda
    i = i+1;
    set(gca,'ColorOrderIndex',1)
    plot(rigidCoda.time,rigidCoda.endPoint,lineType{i},'Linewidth',2)
    leg = [leg, 'x_{R,coda}', 'y_{R,coda}', 'z_{R,coda}'];
end
if show_desired
    i = i+1;
    set(gca,'ColorOrderIndex',1)
%     plot(time,[trajx;trajy;trajz],lineType{i},'Linewidth',2)
    plot(time,traj_trans,lineType{i},'Linewidth',2)
    leg = [leg, 'x_{d}', 'y_{d}', 'z_{d}'];
end
xlabel('Time [s]')
ylabel('End-effector position [m]')
if putTitle
    title('Comparison between end-effector trajectories')
end
grid on
legend(leg)


leg = {};
figure
hold on
if show_flexCoda
%     plot3(flexCoda.endPoint(:,1),flexCoda.endPoint(:,2),flexCoda.endPoint(:,3),'Linewidth',2)
    plot3(eff_trajFlex(:,1),eff_trajFlex(:,2),eff_trajFlex(:,3),'Linewidth',2)
    leg = [leg, 'FEM'];
end
if show_rigidCoda
%     plot3(rigidCoda.endPoint(:,1),rigidCoda.endPoint(:,2),rigidCoda.endPoint(:,3),'Linewidth',2)
    plot3(eff_trajRigid(:,1),eff_trajRigid(:,2),eff_trajRigid(:,3),'Linewidth',2)
    leg = [leg, 'Rigid'];
end
if show_desired
%     plot3(trajx,trajy,trajz,'Linewidth',2)
    plot3(traj_trans(1,:),traj_trans(2,:),traj_trans(3,:),'Linewidth',2)
    leg = [leg, 'Desired'];
end
xlabel('X [m]','Fontsize',14)
ylabel('Z [m]','Fontsize',14)
zlabel('Y [m]','Fontsize',14)
if putTitle
    title('3D Comparison between end-effector trajectories')
end
grid on
legend(leg,'Location','Best')

% leg = {};
% figure
% hold on
% if show_flexCoda
%     plot3(traj_transFlex(:,1),traj_transFlex(:,2),traj_transFlex(:,3))
%     leg = [leg, 'Flex'];
% end
% if show_rigidCoda
%     plot3(traj_transRigid(:,1),traj_transRigid(:,2),traj_transRigid(:,3))
%     leg = [leg, 'Rigid'];
% end
% if show_desired
%     plot3(trajx,trajy,trajz,'Linewidth',2)
%     leg = [leg, 'Desired'];
% end
% xlabel('X')
% ylabel('Z')
% zlabel('Y')
% if putTitle
%     title('3D Comparison between end-effector trajectories')
% end
% grid on
% legend(leg)

