%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simplified 3dof model of Sawyer robot with elastic joints.
% Geometry and inertia infos coming from CAD solidworks model.

% --!-- Y axis is vertical like CAD model and X axis is toward the front of
% the robot (In Gantry position) --!--

clear all
% close all

% hypForComp = 'constant';
hypForComp = 'notConstant';
% listM = [1 0 0];
% listKt = [1 0 0];
% listCt = [1 0 0];
% listPhiq = [1 0 0];
listM = [1 1 1];
listKt = [1 1 1];
listCt = [1 1 1];
listPhiq = [1 1 1];
dur = '50';
finaltime = str2double(dur)/10; % sec
timestepsize = 0.01; % sec

time = 0:timestepsize:finaltime;

rho_num = 0.0;

%% parameters
stiff1 = 0;
damp1 = 0; % Nms/rad
coul1 = 0; % Nm ----- from urdf = 5
stiff1Bis = 750; % Comming from mean values of measured stiffnesses
damp1Bis = 10; % Nms/rad ----- from urdf = 10
coul1Bis = 0; % Nm

stiff2 = 0;
damp2 = 0; % Nms/rad
coul2 = 0; % Nm ----- from urdf = 2
stiff2Bis = 1000; % Comming from mean values of measured stiffnesses
damp2Bis = 5; % Nms/rad ----- from urdf = 5
coul2Bis = 0; % Nm

stiff3 = 0;
damp3 = 0; % Nms/rad
coul3 = 0; % Nm ----- from urdf = 0.5
stiff3Bis = 450; % Comming from mean values of measured stiffnesses
damp3Bis = 1; % Nms/rad ----- from urdf = 1
coul3Bis = 0; % Nm

grav = [0 -9.81 0]; % gravity in the vertical Y axis
%% joint 1
joint1.axis = [0 0 0 0 1 0]'; % 1st joint along vertical Y axis part 1
joint1.stiff = stiff1;
joint1.damp = damp1; % Nms/rad
joint1.coul = coul1; % Nm

joint1Bis.axis = [0 0 0 0 1 0]'; % 1st joint along vertical Y axis part 2
joint1Bis.stiff = stiff1Bis;
joint1Bis.damp = damp1Bis; % Nms/rad
joint1Bis.coul = coul1Bis; % Nm
%% joint 2
joint2.axis = [0 0 0 0 0 1]'; % 2nd joint along horizontal Z axis part 1
joint2.stiff = stiff2;
joint2.damp = damp2; % Nms/rad
joint2.coul = coul2; % Nm

joint2Bis.axis = [0 0 0 0 0 1]'; % 2nd joint along horizontal Z axis part 2
joint2Bis.stiff = stiff2Bis;
joint2Bis.damp = damp2Bis; % Nms/rad
joint2Bis.coul = coul2Bis; % Nm
%% joint 3
joint3.axis = [0 0 0 0 0 1]'; % 3rd joint along horizontal X axis part 1
joint3.stiff = stiff3;
joint3.damp = damp3; % Nms/rad
joint3.coul = coul3; % Nm

joint3Bis.axis = [0 0 0 0 0 1]'; % 3rd joint along horizontal X axis part 2
joint3Bis.stiff = stiff3Bis;
joint3Bis.damp = damp3Bis; % Nms/rad
joint3Bis.coul = coul3Bis; % Nm

%% link 1
link1.m = 6.1704; % kg
link1.J = [0.1194 0.0008 -0.0012;...
           0.0008 0.0205 -0.0025;...
           -0.0012 -0.0025 0.1228]; % kg/m^2
link1.nodes = [0 0.092 0;...
               0.0239 0.2984 0.0030;...               
               0.0810 0.3170 -0.0595]; % m
link1.g = grav;

%% link 2
link2.m = 5.182; % kg
link2.J = [0.0142 0 -0.0135;...
           0 0.1164 0;...
           -0.0135 0 0.1111]; % kg/m^2
link2.nodes = [0.0810 0.3170 -0.0595;...
               0.2105 0.3170 -0.1707;...               
               0.4810 0.3170 -0.1500]; % m
link2.g = grav;

%% link 3
link3.m = 5.0088; % kg
link3.J = [0.0244 -0.0096 -0.0204;...
           -0.0096 0.1577 0.0045;...
           -0.0204 0.0045 0.1463]; % kg/m^2
link3.nodes = [0.4810 0.3170 -0.1500;...
               0.6970 0.3070 -0.0704;...
               0.8810 0.1833 -0.1603]; % m
link3.g = grav;

%% Building the Model

% nodes = [link1.nodes(1,:);...
%          link1.nodes(1,:);...
%          link1.nodes;...
%          link2.nodes(1,:);...
%          link2.nodes;...
%          link3.nodes(1,:);...
%          link3.nodes];

nodes = [link1.nodes(1,:);...
         link1.nodes;...
         link2.nodes;...
         link3.nodes];

% number all nodes properly so they can be used by FEMmodel
nodes = [1:size(nodes,1);nodes']';

count = 1;
% creating 1st rigid body (rotating aroud y)
elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [4 3 5];
elements{count}.nodes = [3 2 4];
elements{count}.mass = link1.m;
elements{count}.J = link1.J;
elements{count}.g = link1.g;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;
count = count +1;

% creating 2nd rigid body (rotating aroud z)
elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [8 7 9];
elements{count}.nodes = [6 5 7];
elements{count}.mass = link2.m;
elements{count}.J = link2.J;
elements{count}.g = link2.g;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;
count = count +1;

% creating 3rd rigid body (rotating aroud z)
elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [12 11 13];
elements{count}.nodes = [9 8 10];
elements{count}.mass = link3.m;
elements{count}.J = link3.J;
elements{count}.g = link3.g;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;
count = count +1;

% % creating 1st joint (rotating aroud y) part 1
% elements{count}.type = 'KinematicConstraint';
% elements{count}.nodes = [1 2];
% % elements{count}.A = 0*joint1.axis;
% elements{count}.A = [];
% elements{count}.k = joint1.stiff;
% elements{count}.d = joint1.damp;
% elements{count}.coulomb = joint1.coul;
% count = count +1;

% creating 1st joint (rotating aroud y) part 2
elements{count}.type = 'KinematicConstraint';
% elements{count}.nodes = [2 3];
elements{count}.nodes = [1 2];
elements{count}.A = joint1Bis.axis;
elements{count}.k = joint1Bis.stiff;
elements{count}.d = joint1Bis.damp;
elements{count}.coulomb = joint1Bis.coul;
count = count +1;

% % creating 2nd joint (rotating aroud z) part 1
% elements{count}.type = 'KinematicConstraint';
% elements{count}.nodes = [5 6];
% % elements{count}.A = 0*joint2.axis;
% elements{count}.A = [];
% elements{count}.k = joint2.stiff;
% elements{count}.d = joint2.damp;
% elements{count}.coulomb = joint2.coul;
% count = count +1;

% creating 2nd joint (rotating aroud z) part 2
elements{count}.type = 'KinematicConstraint';
% elements{count}.nodes = [6 7];
elements{count}.nodes = [4 5];
elements{count}.A = joint2Bis.axis;
elements{count}.k = joint2Bis.stiff;
elements{count}.d = joint2Bis.damp;
elements{count}.coulomb = joint2Bis.coul;
count = count +1;

% % creating 3rd joint (rotating aroud z) part 1
% elements{count}.type = 'KinematicConstraint';
% elements{count}.nodes = [9 10];
% % elements{count}.A = 0*joint3.axis;
% elements{count}.A = [];
% elements{count}.k = joint3.stiff;
% elements{count}.d = joint3.damp;
% elements{count}.coulomb = joint3.coul;
% count = count +1;

% creating 3rd joint (rotating aroud z) part 2
elements{count}.type = 'KinematicConstraint';
% elements{count}.nodes = [10 11];
elements{count}.nodes = [7 8];
elements{count}.A = joint3Bis.axis;
elements{count}.k = joint3Bis.stiff;
elements{count}.d = joint3Bis.damp;
elements{count}.coulomb = joint3Bis.coul;
count = count +1;

% s = 800;
% e = 810;
% A = 5;
% f = zeros(size(time));
% f(s:e) = A*ones(1,e-s+1);
% elements{count}.type = 'punctualForce';
% elements{count}.nodes = [nodes(end,1)];
% elements{count}.f = f;
% elements{count}.DOF = [2];

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();
% save(['C:\ObjectCodeMatlab\TestsArthur\SawyerLumpedMass\D3dof_',dur],'D')

%% Plots

% Rigid model
endNodeRigid = ModelR.listNodes{end};
joint2Rigid = ModelR.listElementVariables{1};
joint4Rigid = ModelR.listElementVariables{2};
joint6Rigid = ModelR.listElementVariables{3};
% joint4Rigid = ModelR.listElementVariables{4};
% joint5Rigid = ModelR.listElementVariables{5};
% joint6Rigid = ModelR.listElementVariables{6};
% torque1Rigid = ModelR.listElementVariables{end}.value(1,:);
% torque2Rigid = ModelR.listElementVariables{end}.value(2,:);
% torque3Rigid = ModelR.listElementVariables{end}.value(3,:);

% 
% 
% % Plot motor joint angles
% figure
% hold on
% plot(time,[joint1Rigid.relCoo;joint3Rigid.relCoo;joint5Rigid.relCoo],'--')
% xlabel('Time (s)')
% ylabel('Motor joint position (rad)')
% title('Comparison of motor joint positions')
% legend('1','3','5')
% grid on
% 
% % Plot motor joint velocities
% figure
% hold on
% % plot(time,[joint1Flex.velocity;joint3Flex.velocity;joint5Flex.velocity])
% set(gca,'ColorOrderIndex',1)
% plot(time,[joint1Rigid.velocity;joint3Rigid.velocity;joint5Rigid.velocity],'--')
% xlabel('Time (s)')
% ylabel('Motor joint velocities (rad/s)')
% title('Comparison of motor joint velocities')
% % legend('1','3','5','1R','3R','5R')
% legend('1','3','5')
% grid on
% 
% % Plot motor joint accelerations
% figure
% hold on
% % plot(time,[joint1Flex.acceleration;joint3Flex.acceleration;joint5Flex.acceleration])
% set(gca,'ColorOrderIndex',1)
% plot(time,[joint1Rigid.acceleration;joint3Rigid.acceleration;joint5Rigid.acceleration],'--')
% xlabel('Time (s)')
% ylabel('Motor joint accelerations (rad/s^2)')
% title('Comparison of motor joint accelerations')
% % legend('1','3','5','1R','3R','5R')
% legend('1','3','5')
% grid on

% Plot flex joint angles
figure
hold on
% plot(time,f)
set(gca,'ColorOrderIndex',1)
plot(time,[joint2Rigid.relCoo;joint4Rigid.relCoo;joint6Rigid.relCoo])
xlabel('Time (s)')
ylabel('Flex joint position (rad)')
title('Comparison of flex joint positions')
% legend('2','4','6','2R','4R','6R')
% legend('f','1','3','5')
legend('1','3','5')
grid on

% Plot flex joint velocities
figure
hold on
% plot(time,f)
set(gca,'ColorOrderIndex',1)
plot(time,[joint2Rigid.velocity;joint4Rigid.velocity;joint6Rigid.velocity])
xlabel('Time (s)')
ylabel('Flex joint velocity (rad)')
title('Comparison of flex joint velocities')
% legend('2','4','6','2R','4R','6R')
% legend('f','1','3','5')
legend('1','3','5')
grid on

% Plot flex joint accelerations
figure
hold on
% plot(time,f)
set(gca,'ColorOrderIndex',1)
plot(time,[joint2Rigid.acceleration;joint4Rigid.acceleration;joint6Rigid.acceleration])
xlabel('Time (s)')
ylabel('Flex joint acceleration (rad)')
title('Comparison of flex joint accelerations')
% legend('2','4','6','2R','4R','6R')
% legend('f','1','3','5')
legend('1','3','5')
grid on

% % Plot joint torques
% figure
% hold on
% % plot(time,f)
% % set(gca,'ColorOrderIndex',1)
% plot(time,[torque1Rigid;torque2Rigid;torque3Rigid])
% xlabel('Time (s)')
% ylabel('Joint torques (Nm)')
% title('Comparison of joint torques')
% % legend('1','2','3','1R','2R','3R')
% % legend('f','1','3','5')
% legend('1','3','5')
% grid on
