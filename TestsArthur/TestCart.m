%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control

finaltime = 3.5;
timestepsize = 0.01;

nodes = [1 -1 0 0;
         2 -1 0 0;
         3 -1 0 0;
         4 -1.6614 -0.75 0;
         5 -1.6614 -0.75 0;
         6 -1 -1.5 0];

elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2];
elements{1}.mass = 3;
% elements{1}.J = eye(3);
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [3 4];
elements{2}.mass = 6.875;
elements{2}.J = 0.57*eye(3);

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [5 6];
elements{3}.mass = 6.875;
elements{3}.J = 0.57*eye(3);

% elements{20}.type = 'ExternalForce';
% elements{20}.nodes = [3 4 5 6];
% elements{20}.DOF = 2;
% elements{20}.amplitude = -10;
% elements{20}.frequency = 0;

elements{4}.type = 'KinematicConstraint';
elements{4}.nodes = [1 2];
elements{4}.A = [1 0 0 0 0 0]';

elements{5}.type = 'KinematicConstraint';
elements{5}.nodes = [2 3];
elements{5}.A = [0 0 0 0 0 1]';

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [4 5];
elements{6}.A = [0 0 0 0 0 1]';

% Trajectory

cart_end = 1;
cart_i = -1;

x_end = 1;
x_i = -1;

% y_end = 1;
% y_i = -1;
y_end = -1.5;
y_i = -1.5;

r = 1;

trajcart = halfCircleTraj(cart_i,cart_end,r,finaltime,timestepsize,'lin',1,2.5);
trajx = halfCircleTraj(x_i,x_end,r,finaltime,timestepsize,'sin',1,2.5);
trajy = halfCircleTraj(y_i,y_end,r,finaltime,timestepsize,'cos',1,2.5);

elements{8}.type = 'TrajectoryConstraint';
elements{8}.nodes = [2];
elements{8}.T = [trajcart];
elements{8}.Axe = [1 0 0];
elements{8}.elements = [4];

elements{9}.type = 'TrajectoryConstraint';
elements{9}.nodes = [6];
elements{9}.T = [trajx;...
                 trajy];
elements{9}.Axe = [1 0 0;...
                   0 1 0];
elements{9}.elements = [5 6];

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

S = DynamicIntegration(Model);
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = timestepsize;
S.parameters.rho = 0.2;
S.parameters.scaling = 1e9;
S.runIntegration();

% Plots

figure
hold on
plot(Model.listNodes{2}.position(1,:),Model.listNodes{2}.position(2,:),Model.listNodes{6}.position(1,:),Model.listNodes{6}.position(2,:), 'Linewidth',3)
plot(trajcart,zeros(size(trajcart)),trajx,trajy, 'Linewidth',1, 'Color','r')
grid on


figure
hold on
plot(S.parameters.time,Model.listNodes{2}.position(1,:),S.parameters.time,Model.listNodes{6}.position(1,:),S.parameters.time,Model.listNodes{6}.position(2,:))
plot(S.parameters.time,trajcart,'--',S.parameters.time,trajx,'--',S.parameters.time,trajy,'--')
legend('cart', 'X', 'Y','cartd','Xd','Yd')
grid on


figure
plot(S.parameters.time,Model.listElementVariables{end-1}.value,S.parameters.time,Model.listElementVariables{end}.value(1,:),S.parameters.time,Model.listElementVariables{end}.value(2,:))
legend('u1','u2','u3')
grid on

% Save of nodal values, controls and multipliers over time

save('DynInt_RigidCart','S')


