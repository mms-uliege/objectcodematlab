%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test gravity
clear

finaltime = 10;
timestepsize = 0.01;

nodes = [1 0 0 0;
         2 0 0 0;
         3 1 0 0;
         4 1 0 0;
         5 2 0 0];

count = 1;     
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [3 2];
elements{count}.mass = 1;
elements{count}.g = [9.81 -0.2 0];
count = count+1;

elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [5 4];
elements{count}.mass = 1;
elements{count}.g = [9.81 -0.2 0];
count = count+1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 0 1]';
count = count+1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [3 4];
elements{count}.A = [0 0 0 0 0 1]';
count = count+1;

elements{count}.type = 'RotSpringDamperElement';
elements{count}.damping = 2; % 200 (31 commence a instable)
elements{count}.stiffness = 10; % 100
elements{count}.natural_angle = pi/2;
elements{count}.nodes = [3 4];
elements{count}.A = [0 0 1];

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.2;
% D.parameters.relTolRes = 1e-12;
D.runIntegration();


DeplCoudeOpti = Model.listElementVariables{2}.relCoo;

% Plots

time = D.parameters.time;

uCartFlex.u1 = Model.listElementVariables{end-1}.value(:);
uCartFlex.u2 = Model.listElementVariables{end}.value(1,:);
uCartFlex.u3 = Model.listElementVariables{end}.value(2,:);
uCartFlex.time = time;

figure
hold on
plot(Model.listNodes{2}.position(1,:),Model.listNodes{2}.position(2,:),Model.listNodes{end}.position(1,:),Model.listNodes{end}.position(2,:), 'Linewidth',3)
grid on

figure
hold on
plot(time,Model.listNodes{2}.position(1,:),time,Model.listNodes{end}.position(1,:),time,Model.listNodes{end}.position(2,:))
legend('cart', 'X', 'Y')
grid on

xcart = Model.listNodes{2}.position(1,:);
ycart = Model.listNodes{2}.position(2,:);
x = Model.listNodes{end}.position(1,:);
y = Model.listNodes{end}.position(2,:);


% save('CartFlexD','D')
% save('CartFlexS','S')
