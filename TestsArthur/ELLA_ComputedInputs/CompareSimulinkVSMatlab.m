%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Comparison between the joint Torques/Position and cartesian position of
% the Ella Robot using the Simulink model and the Finite Element Code.

% load the finite element code solution (first generate the input with the
% "GenerateInput" script

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\EndPose')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointPose')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointVel')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointTorque')

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\EndPose3sCoul')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointPose3sCoul')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointVel3sCoul')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointTorque3sCoul')

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\EndPoseR1sCoul')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointPoseR1sCoul')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointVelR1sCoul')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointTorqueR1sCoul')

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\EndPose1sCoulCutFast')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointPose1sCoulCutFast')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointVel1sCoulCutFast')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointTorque1sCoulCutFast')
% % Time
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\Time1sCoulCutFast')

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPose1sCoulCutFastBis')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPose1sCoulCutFastBis')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVel1sCoulCutFastBis')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorque1sCoulCutFastBis')
% % Time
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\Time1sCoulCutFastBis')

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPose1sCoulCutActual')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPose1sCoulCutActual')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVel1sCoulCutActual')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorque1sCoulCutActual')
% % Time
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\Time1sCoulCutActual')

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPose1sCoulCutActual105')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPose1sCoulCutActual105')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVel1sCoulCutActual105')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorque1sCoulCutActual105')
% % Time
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\Time1sCoulCutActual105')

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPose1sCoulCutActualMend')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPose1sCoulCutActualMend')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVel1sCoulCutActualMend')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorque1sCoulCutActualMend')
% % Time
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\Time1sCoulCutActualMend')

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPose1sCoulCutActualMend105')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPose1sCoulCutActualMend105')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVel1sCoulCutActualMend105')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorque1sCoulCutActualMend105')
% % Time
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\Time1sCoulCutActualMend105')

% Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\EndPose1sCoulCutActualLumped')
% Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\JointPose1sCoulCutActualLumped')
% Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\JointVel1sCoulCutActualLumped')
% Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\JointTorque1sCoulCutActualLumped')
% Time
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\Time1sCoulCutActualLumped')

duration = '115';
% Cartesian position
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\EndPoseCutActual_',duration])
% Joint positions
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointPoseCutActual_',duration])
% Joint Velocities
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointVelCutActual_',duration])
% Joint Torques
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointTorqueCutActual_',duration])
% Time
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\TimeCutActual_',duration])

F.EndPose = EndPose;
F.JointPose = JointPose;
F.JointVel = JointVel;
F.JointTorque = JointTorque;
F.Time = Time;

% close all
lineWidth = 1.5;
% If the simulink variables were stored in workspace, plot them
shiftTime = 0.2;
if exist('TrajJoint')
    loadedTime = TrajJoint.Time;
    loaded1 = TrajJoint.Data(:,1);
    loaded2 = TrajJoint.Data(:,2);
    loaded3 = TrajJoint.Data(:,3);
    
    figure
    hold on
    plot(loadedTime+shiftTime,loaded1,loadedTime+shiftTime,loaded2,loadedTime+shiftTime,loaded3,'Linewidth',lineWidth)
%     plot(loadedTime,loaded1,loadedTime,loaded2,loadedTime,loaded3)
    set(gca,'ColorOrderIndex',1)
    plot(F.Time,F.JointPose(1,:),':',F.Time,F.JointPose(2,:),':',F.Time,F.JointPose(3,:),':','Linewidth',lineWidth)
    legend('Simulink 1','Simulink 2','Simulink 3','FE 1','FE 2','FE 3')
    xlabel('Time [s]')
    ylabel('Loaded Joint positions [rad]')
    axis([shiftTime shiftTime+str2double(duration)/100 -inf inf])
    title('Comparison of joint positions (Flexible and simulink)')
    grid on
end
if exist('TrajCart')
    loadedTime = TrajCart.Time;
    loadedX = TrajCart.Data(:,1);
    loadedY = TrajCart.Data(:,2);
    loadedZ = TrajCart.Data(:,3);
    
    figure
    hold on
    plot(loadedTime+shiftTime,loadedX,loadedTime+shiftTime,loadedY,loadedTime+shiftTime,loadedZ,'Linewidth',lineWidth)
    set(gca,'ColorOrderIndex',1)
    plot(F.Time,F.EndPose(1,:),':',F.Time,-F.EndPose(2,:),':',F.Time,F.EndPose(3,:),':','Linewidth',lineWidth)
    legend('loaded x','loaded y','loaded z','simulated x','simulated y','simulated z')
    xlabel('Time [s]')
    ylabel('Loaded cartesian end positions [m]')
    axis([shiftTime shiftTime+str2double(duration)/100 -inf inf])
    title('Comparison of cartesian position (Flexible and simulink)')
    grid on
end

if exist('TrajJointMotor')
    loadedTime = TrajJointMotor.Time;
    loaded1 = TrajJointMotor.Data(:,1);
    loaded2 = TrajJointMotor.Data(:,2);
    loaded3 = TrajJointMotor.Data(:,3);
    hl = loadedTime(2)-loadedTime(1);
    hs = Time(2)-Time(1);
    
    figure
    hold on
    plot(loadedTime+shiftTime,loaded1,loadedTime+shiftTime,loaded2,loadedTime+shiftTime,loaded3,'Linewidth',lineWidth)
    set(gca,'ColorOrderIndex',1)
    plot(F.Time,F.JointPose(1,:),':',F.Time,F.JointPose(2,:),':',F.Time,F.JointPose(3,:),':','Linewidth',lineWidth)
    
%     plot(Time(1,501:end),JointPose(1,501:end)-loaded1',':',Time(1,501:end),JointPose(2,501:end)-loaded2',':',Time(1,501:end),JointPose(3,501:end)-loaded3',':','Linewidth',lineWidth)
    legend('Simulink 1','Simulink 2','Simulink 3','FE 1','FE 2','FE 3')
    xlabel('Time [s]')
    ylabel('Joint positions [rad]')
    title('Comparison of joint motor positions (Flexible and simulink)')
    axis([shiftTime shiftTime+str2double(duration)/100 -inf inf])
    grid on
    
    figure
    hold on
    plot(loadedTime+shiftTime,deriveVal(loaded1',hl),loadedTime+shiftTime,deriveVal(loaded2',hl),loadedTime+shiftTime,deriveVal(loaded3',hl),'Linewidth',lineWidth)
    set(gca,'ColorOrderIndex',1)
    %     plot(loadedTime,loaded1,loadedTime,loaded2,loadedTime,loaded3)
    plot(F.Time,deriveVal(F.JointPose(1,:),hs),':',F.Time,deriveVal(F.JointPose(2,:),hs),':',F.Time,deriveVal(F.JointPose(3,:),hs),':','Linewidth',lineWidth)
    legend('Simulink 1','Simulink 2','Simulink 3','FE 1','FE 2','FE 3')
    xlabel('Time [s]')
    ylabel('Joint velocities [rad/s]')
    title('Comparison of 1st derivative of joint positions (Flexible and simulink)')
    axis([shiftTime shiftTime+str2double(duration)/100 -inf inf])
    grid on
    
    figure
    hold on
    plot(loadedTime+shiftTime,deriveVal(deriveVal(loaded1',hl),hl),loadedTime+shiftTime,deriveVal(deriveVal(loaded2',hl),hl),loadedTime+shiftTime,deriveVal(deriveVal(loaded3',hl),hl),'Linewidth',lineWidth)
    set(gca,'ColorOrderIndex',1)
    %     plot(loadedTime,loaded1,loadedTime,loaded2,loadedTime,loaded3)
    plot(F.Time,deriveVal(deriveVal(F.JointPose(1,:),hs),hs),':',F.Time,deriveVal(deriveVal(F.JointPose(2,:),hs),hs),':',F.Time,deriveVal(deriveVal(F.JointPose(3,:),hs),hs),':','Linewidth',lineWidth)
    legend('Simulink 1','Simulink 2','Simulink 3','FE 1','FE 2','FE 3')
    xlabel('Time [s]')
    ylabel('Joint accelerations [rad/s^2]')
    title('Comparison of 2nd derivative of joint positions (Flexible and simulink)')
    axis([shiftTime shiftTime+str2double(duration)/100 -inf inf])
    grid on
end

if exist('TrajTorqueMotor')
    loadedTime = TrajTorqueMotor.Time;
    loaded1 = TrajTorqueMotor.Data(:,1);
    loaded2 = TrajTorqueMotor.Data(:,2);
    loaded3 = TrajTorqueMotor.Data(:,3);
    
    figure
    hold on
    plot(loadedTime+shiftTime,loaded1,loadedTime+shiftTime,loaded2,loadedTime+shiftTime,loaded3,'Linewidth',lineWidth)
    set(gca,'ColorOrderIndex',1)
    plot(F.Time,F.JointTorque(1,:),':',F.Time,F.JointTorque(2,:),':',F.Time,F.JointTorque(3,:),':','Linewidth',lineWidth)
    legend('Simulink 1','Simulink 2','Simulink 3','FE 1','FE 2','FE 3')
    xlabel('Time [s]')
    ylabel('Joint torques [Nm]')
    title('Comparison of joint motor torques (Flexible and simulink)')
    axis([shiftTime shiftTime+str2double(duration)/100 -inf inf])
    grid on
end

clear EndPose
clear JointTorque
clear JointPose
clear JointVel

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\EndPoseR1sCoulCutFast')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointPoseR1sCoulCutFast')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointVelR1sCoulCutFast')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointTorqueR1sCoulCutFast')
% % Time
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\TimeR1sCoulCutFast')

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPoseR1sCoulCutFastBis')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPoseR1sCoulCutFastBis')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVelR1sCoulCutFastBis')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorqueR1sCoulCutFastBis')
% % Time
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\TimeR1sCoulCutFastBis')

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPoseR1sCoulCutActual')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPoseR1sCoulCutActual')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVelR1sCoulCutActual')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorqueR1sCoulCutActual')
% % Time
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\TimeR1sCoulCutActual')

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPoseR1sCoulCutActual105')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPoseR1sCoulCutActual105')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVelR1sCoulCutActual105')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorqueR1sCoulCutActual105')
% % Time
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\TimeR1sCoulCutActual105')

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPoseR1sCoulCutActualMend')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPoseR1sCoulCutActualMend')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVelR1sCoulCutActualMend')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorqueR1sCoulCutActualMend')
% % Time
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\TimeR1sCoulCutActualMend')

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPoseR1sCoulCutActualMend105')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPoseR1sCoulCutActualMend105')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVelR1sCoulCutActualMend105')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorqueR1sCoulCutActualMend105')
% % Time
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\TimeR1sCoulCutActualMend105')

% % Cartesian position
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\EndPoseR1sCoulCutActualLumped')
% % Joint positions
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\JointPoseR1sCoulCutActualLumped')
% % Joint Velocities
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\JointVelR1sCoulCutActualLumped')
% % Joint Torques
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\JointTorqueR1sCoulCutActualLumped')
% % Time
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\TimeR1sCoulCutActualLumped')

% Cartesian position
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\EndPoseRCutActual_',duration])
% Joint positions
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointPoseRCutActual_',duration])
% Joint Velocities
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointVelRCutActual_',duration])
% Joint Torques
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointTorqueRCutActual_',duration])
% Time
load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\TimeRCutActual_',duration])

R.EndPose = EndPose;
R.JointPose = JointPose;
R.JointVel = JointVel;
R.JointTorque = JointTorque;
R.Time = Time;

if exist('TrajJoint')
    loadedTime = TrajJoint.Time;
    loaded1 = TrajJoint.Data(:,1);
    loaded2 = TrajJoint.Data(:,2);
    loaded3 = TrajJoint.Data(:,3);
    hl = loadedTime(2)-loadedTime(1);
    hs = Time(2)-Time(1);
    
    figure
    hold on
    plot(loadedTime+shiftTime,loaded1,loadedTime+shiftTime,loaded2,loadedTime+shiftTime,loaded3,'Linewidth',lineWidth)
%     plot(loadedTime,loaded1,loadedTime,loaded2,loadedTime,loaded3)
    set(gca,'ColorOrderIndex',1)
    plot(R.Time,R.JointPose(1,:),':',R.Time,R.JointPose(2,:),':',R.Time,R.JointPose(3,:),':','Linewidth',lineWidth)
    legend('Simulink 1','Simulink 2','Simulink 3','FE 1','FE 2','FE 3')
    xlabel('Time [s]')
    ylabel('Joint positions [rad]')
    title('Comparison of joint positions (rigid and simulink)')
    axis([shiftTime shiftTime+str2double(duration)/100 -inf inf])
    grid on
    
    figure
    hold on
    plot(loadedTime+shiftTime,deriveVal(loaded1',hl),loadedTime+shiftTime,deriveVal(loaded2',hl),loadedTime+shiftTime,deriveVal(loaded3',hl),'Linewidth',lineWidth)
%     plot(loadedTime,loaded1,loadedTime,loaded2,loadedTime,loaded3)
    set(gca,'ColorOrderIndex',1)
    plot(R.Time,deriveVal(R.JointPose(1,:),hs),':',R.Time,deriveVal(R.JointPose(2,:),hs),':',R.Time,deriveVal(R.JointPose(3,:),hs),':','Linewidth',lineWidth)
    legend('Simulink 1','Simulink 2','Simulink 3','FE 1','FE 2','FE 3')
    xlabel('Time [s]')
    ylabel('Joint velocities [rad/s]')
    title('Comparison of 1st derivative of joint positions (rigid and simulink)')
    axis([shiftTime shiftTime+str2double(duration)/100 -inf inf])
    grid on
    
    figure
    hold on
    plot(loadedTime+shiftTime,deriveVal(deriveVal(loaded1',hl),hl),loadedTime+shiftTime,deriveVal(deriveVal(loaded2',hl),hl),loadedTime+shiftTime,deriveVal(deriveVal(loaded3',hl),hl),'Linewidth',lineWidth)
%     plot(loadedTime,loaded1,loadedTime,loaded2,loadedTime,loaded3)
    set(gca,'ColorOrderIndex',1)
    plot(R.Time,deriveVal(deriveVal(R.JointPose(1,:),hs),hs),':',R.Time,deriveVal(deriveVal(R.JointPose(2,:),hs),hs),':',R.Time,deriveVal(deriveVal(R.JointPose(3,:),hs),hs),':','Linewidth',lineWidth)
    legend('Simulink 1','Simulink 2','Simulink 3','FE 1','FE 2','FE 3')
    xlabel('Time [s]')
    ylabel('Joint accelerations [rad/s^2]')
    title('Comparison of 2nd derivative of joint positions (rigid and simulink)')
    axis([shiftTime shiftTime+str2double(duration)/100 -inf inf])
    grid on
end

% if exist('TrajCart')
%     loadedTime = TrajCart.Time;
%     loadedX = TrajCart.Data(:,1);
%     loadedY = TrajCart.Data(:,2);
%     loadedZ = TrajCart.Data(:,3);
%     
%     figure
%     hold on
%     plot(loadedTime+shiftTime,loadedX,loadedTime+shiftTime,loadedY,loadedTime+shiftTime,loadedZ)
%     plot(Time,EndPose(1,:),':',Time,-EndPose(2,:),':',Time,EndPose(3,:),':')
%     legend('loaded x','loaded y','loaded z','simulated x','simulated y','simulated z')
%     xlabel('Time')
%     ylabel('Loaded cartesian end positions')
%     title('Comparison of cartesian position (rigid and simulink)')
% end

if exist('TrajJointMotor')
    loadedTime = TrajJointMotor.Time;
    loaded1 = TrajJointMotor.Data(:,1);
    loaded2 = TrajJointMotor.Data(:,2);
    loaded3 = TrajJointMotor.Data(:,3);
    
    figure
    hold on
    plot(loadedTime+shiftTime,loaded1,loadedTime+shiftTime,loaded2,loadedTime+shiftTime,loaded3,'Linewidth',lineWidth)
    set(gca,'ColorOrderIndex',1)
    plot(R.Time,R.JointPose(1,:),':',R.Time,R.JointPose(2,:),':',R.Time,R.JointPose(3,:),':','Linewidth',lineWidth)
    legend('Simulink 1','Simulink 2','Simulink 3','FE 1','FE 2','FE 3')
    xlabel('Time [s]')
    ylabel('Joint positions [rad]')
    title('Comparison of joint motor positions (rigid and simulink)')
    axis([shiftTime shiftTime+str2double(duration)/100 -inf inf])
    grid on
end

if exist('TrajTorqueMotor')
    loadedTime = TrajTorqueMotor.Time;
    loaded1 = TrajTorqueMotor.Data(:,1);
    loaded2 = TrajTorqueMotor.Data(:,2);
    loaded3 = TrajTorqueMotor.Data(:,3);
    
    figure
    hold on
    plot(loadedTime+shiftTime,loaded1,loadedTime+shiftTime,loaded2,loadedTime+shiftTime,loaded3,'Linewidth',lineWidth)
    set(gca,'ColorOrderIndex',1)
    plot(R.Time,R.JointTorque(1,:),':',R.Time,R.JointTorque(2,:),':',R.Time,R.JointTorque(3,:),':','Linewidth',lineWidth)
%     set(gca,'ColorOrderIndex',1)
%     plot(Time,deriveVal(JointPose(1,:),hs),'--',Time,deriveVal(JointPose(2,:),hs),'--',Time,deriveVal(JointPose(3,:),hs),'--')
%     set(gca,'ColorOrderIndex',1)
%     plot(Time,deriveVal(deriveVal(JointPose(1,:),hs),hs),':',Time,deriveVal(deriveVal(JointPose(2,:),hs),hs),':',Time,deriveVal(deriveVal(JointPose(3,:),hs),hs),':')
    legend('Simulink 1','Simulink 2','Simulink 3','FE 1','FE 2','FE 3')
    xlabel('Time [s]')
    ylabel('Joint torques [Nm]')
    title('Comparison of joint motor torques (rigid and simulink)')
    axis([shiftTime shiftTime+str2double(duration)/100 -inf inf])
    grid on
end

% figure
% hold on
% plot3(trajx,trajy,trajz)
% ModelF.Visu(80);ModelF.Visu(120);ModelF.Visu(140);ModelF.Visu(160);ModelF.Visu(195)
% xlabel('X [m]')
% ylabel('Y [m]')
% zlabel('Z [m]')
% axis([0 2 -inf inf -inf inf])
% grid on

if exist('TrajJointMotor')
    
    loadedTimeMotor = TrajJointMotor.Time;
    loaded1Motor = TrajJointMotor.Data(:,1);
    loaded2Motor = TrajJointMotor.Data(:,2);
    loaded3Motor = TrajJointMotor.Data(:,3);
    
    figure
    hold on
    plot(R.Time,[R.JointPose(1,:);R.JointPose(2,:);R.JointPose(3,:)],'Linewidth',lineWidth)
    set(gca,'ColorOrderIndex',1)
    plot(loadedTimeMotor+shiftTime,[loaded1Motor loaded2Motor loaded3Motor],':','Linewidth',lineWidth)
    set(gca,'ColorOrderIndex',1)
    plot(F.Time,[F.JointPose(1,:);F.JointPose(2,:);F.JointPose(3,:)],'-.','Linewidth',lineWidth)
    legend('Rigid 1','Rigid 2','Rigid 3','Lumped 1','Lumped 2','Lumped 3','FE 1','FE 2','FE 3')
    xlabel('Time [s]')
    ylabel('Joint positions [rad]')
%     title('Comparison of joint input positions')
    axis([shiftTime shiftTime+str2double(duration)/100 -inf inf])
    grid on
end

if exist('TrajJointMotor')
    
    loadedTimeMotor = TrajJointMotor.Time;
    loaded1Motor = TrajJointMotor.Data(:,1);
    loaded2Motor = TrajJointMotor.Data(:,2);
    loaded3Motor = TrajJointMotor.Data(:,3);
    
    figure
    hold on
    set(gca,'ColorOrderIndex',1)
    plot(loadedTimeMotor+shiftTime,[loaded1Motor-R.JointPose(1,500:3375)' loaded2Motor-R.JointPose(2,500:3375)' loaded3Motor-R.JointPose(3,500:3375)'],'Linewidth',lineWidth)
    set(gca,'ColorOrderIndex',1)
    plot(F.Time,[F.JointPose(1,:)-R.JointPose(1,:);F.JointPose(2,:)-R.JointPose(2,:);F.JointPose(3,:)-R.JointPose(3,:)],'-.','Linewidth',lineWidth)
    legend('Lumped 1','Lumped 2','Lumped 3','FE 1','FE 2','FE 3')
    xlabel('Time [s]')
    ylabel('Joint positions difference [rad]')
    title('Difference between rigid and flexible models of joint input positions')
    axis([shiftTime shiftTime+str2double(duration)/100 -inf inf])
    grid on
end

