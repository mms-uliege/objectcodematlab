/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer Industrie-Elektronik Ges.m.b.H.
 ********************************************************************
 * Program: ellaCtrl
 * File: ellaCtrl.c
 * Author: student
 * Created: Thu Dec 07 15:38:54 2017
 ********************************************************************
 * Implementation of program ellaCtrl
 ********************************************************************
 * Generated with B&R Automation Studio Target for Simulink V2.2
 * (ERT based)
 ********************************************************************/

#include "ellaCtrl.h"
#include "ellaCtrl_private.h"

/*  Defines */

/*  Data Types */

/**************************** GLOBAL DATA *************************************/
/*  Definitions */

/* Block signals (auto storage) */
BlockIO_ellaCtrl ellaCtrl_B;

/* Block states (auto storage) */
D_Work_ellaCtrl ellaCtrl_DWork;

/*  Declarations  */

/***************************** FILE SCOPE DATA ********************************/

/*************************** FUNCTIONS ****************************************/

/*********************************************************************
 * Lookup Binary Search Utility BINARYSEARCH_real_T
 */
void BINARYSEARCH_real_T( uint32_T *piLeft, uint32_T *piRght, real_T u, const
  real_T *pData, uint32_T iHi)
{
  /* Find the location of current input value in the data table. */
  *piLeft = 0;
  *piRght = iHi;
  if (u <= pData[0] ) {
    /* Less than or equal to the smallest point in the table. */
    *piRght = 0;
  } else if (u >= pData[iHi] ) {
    /* Greater than or equal to the largest point in the table. */
    *piLeft = iHi;
  } else {
    uint32_T i;

    /* Do a binary search. */
    while (( *piRght - *piLeft ) > 1 ) {
      /* Get the average of the left and right indices using to Floor rounding. */
      i = (*piLeft + *piRght) >> 1;

      /* Move either the right index or the left index so that */
      /*  LeftDataPoint <= CurrentValue < RightDataPoint */
      if (u < pData[i] ) {
        *piRght = i;
      } else {
        *piLeft = i;
      }
    }
  }
}

/* end macro BINARYSEARCH_real_T
 *********************************************************************/

/*********************************************************************
 * Lookup Utility LookUp_real_T_real_T
 */
void LookUp_real_T_real_T( real_T *pY, const real_T *pYData, real_T u, const
  real_T *pUData, uint32_T iHi)
{
  uint32_T iLeft;
  uint32_T iRght;
  BINARYSEARCH_real_T( &(iLeft), &(iRght), u, pUData, iHi);

  {
    real_T lambda;
    if (pUData[iRght] > pUData[iLeft] ) {
      real_T num;
      real_T den;
      den = pUData[iRght];
      den = den - pUData[iLeft];
      num = u;
      num = num - pUData[iLeft];
      lambda = num / den;
    } else {
      lambda = 0;
    }

    {
      real_T yLeftCast;
      real_T yRghtCast;
      yLeftCast = pYData[iLeft];
      yRghtCast = pYData[iRght];
      if (lambda != 0) {
        yLeftCast += lambda * ( yRghtCast - yLeftCast );
      }

      (*pY) = yLeftCast;
    }
  }
}

/* end function LookUp_real_T_real_T
 *********************************************************************/

/* Model step function */
void ellaCtrl_step(void)
{
  /* local block i/o variables */
  real_T rtb_DataTypeConversion;
  real_T rtb_Gain;
  real_T rtb_DiscreteTimeIntegrator;
  real_T rtb_LookupTableq3;
  real_T rtb_LookupTableq2;
  real_T rtb_LookupTableq1;

  {
    int32_T rtb_DataTypeConversion_i;

    /* DataTypeConversion: '<Root>/Data Type  Conversion' incorporates:
     *  Constant: '<Root>/enableControl'
     */
    rtb_DataTypeConversion = (real_T)enableControl;

    /* Outputs for enable SubSystem: '<Root>/Enabled Subsystem' incorporates:
     *  EnablePort: '<S1>/Enable'
     */
    if (rtb_DataTypeConversion > 0.0) {
      if (ellaCtrl_DWork.EnabledSubsystem_MODE == SUBSYS_DISABLED) {
        /* InitializeConditions for DiscreteIntegrator: '<S1>/Discrete-Time Integrator' */
        ellaCtrl_DWork.DiscreteTimeIntegrator_DSTATE = 0.0;
        ellaCtrl_DWork.EnabledSubsystem_MODE = SUBSYS_ENABLED;
      }
    } else {
      if (ellaCtrl_DWork.EnabledSubsystem_MODE == SUBSYS_ENABLED) {
        ellaCtrl_DWork.EnabledSubsystem_MODE = SUBSYS_DISABLED;
      }
    }

    if (ellaCtrl_DWork.EnabledSubsystem_MODE == SUBSYS_ENABLED) {
      /* DataTypeConversion: '<S1>/Data Type  Conversion' incorporates:
       *  Constant: '<S1>/enableControl'
       */
      rtb_DataTypeConversion_i = (int32_T)enableFF;

      /* DiscreteIntegrator: '<S1>/Discrete-Time Integrator' */
      rtb_DiscreteTimeIntegrator = ellaCtrl_DWork.DiscreteTimeIntegrator_DSTATE;

      /* Lookup Block: '<S1>/Lookup Table q4'
       * About '<S1>/Lookup Table q4 :'
       * Lookup Block: '<S1>/Lookup Table q4'
       * Input0  Data Type:  Floating Point real_T
       * Output0 Data Type:  Floating Point real_T
       * Lookup Method: Linear_Endpoint
       *
       * XData parameter uses the same data type and scaling as Input0
       * YData parameter uses the same data type and scaling as Output0


       */
      LookUp_real_T_real_T( &(rtb_LookupTableq3),
                           ellaCtrl_ConstP.LookupTableq4_YDat,
                           rtb_DiscreteTimeIntegrator, ellaCtrl_ConstP.pooled1,
                           7500U);

      /* Lookup Block: '<S1>/Lookup Table q5'
       * About '<S1>/Lookup Table q5 :'
       * Lookup Block: '<S1>/Lookup Table q5'
       * Input0  Data Type:  Floating Point real_T
       * Output0 Data Type:  Floating Point real_T
       * Lookup Method: Linear_Endpoint
       *
       * XData parameter uses the same data type and scaling as Input0
       * YData parameter uses the same data type and scaling as Output0


       */
      LookUp_real_T_real_T( &(rtb_LookupTableq2),
                           ellaCtrl_ConstP.LookupTableq5_YDat,
                           rtb_DiscreteTimeIntegrator, ellaCtrl_ConstP.pooled1,
                           7500U);

      /* Lookup Block: '<S1>/Lookup Table q6'
       * About '<S1>/Lookup Table q6 :'
       * Lookup Block: '<S1>/Lookup Table q6'
       * Input0  Data Type:  Floating Point real_T
       * Output0 Data Type:  Floating Point real_T
       * Lookup Method: Linear_Endpoint
       *
       * XData parameter uses the same data type and scaling as Input0
       * YData parameter uses the same data type and scaling as Output0


       */
      LookUp_real_T_real_T( &(rtb_LookupTableq1),
                           ellaCtrl_ConstP.LookupTableq6_YDat,
                           rtb_DiscreteTimeIntegrator, ellaCtrl_ConstP.pooled1,
                           7500U);

      /* Product: '<S1>/Product' */
      ellaCtrl_B.Product[0] = (real_T)rtb_DataTypeConversion_i *
        rtb_LookupTableq3;
      ellaCtrl_B.Product[1] = (real_T)rtb_DataTypeConversion_i *
        rtb_LookupTableq2;
      ellaCtrl_B.Product[2] = (real_T)rtb_DataTypeConversion_i *
        rtb_LookupTableq1;
      torque_simulink[0] = ellaCtrl_B.Product[0];
      torque_simulink[1] = ellaCtrl_B.Product[1];
      torque_simulink[2] = ellaCtrl_B.Product[2];

      /* Lookup Block: '<S1>/Lookup Table q1'
       * About '<S1>/Lookup Table q1 :'
       * Lookup Block: '<S1>/Lookup Table q1'
       * Input0  Data Type:  Floating Point real_T
       * Output0 Data Type:  Floating Point real_T
       * Lookup Method: Linear_Endpoint
       *
       * XData parameter uses the same data type and scaling as Input0
       * YData parameter uses the same data type and scaling as Output0


       */
      LookUp_real_T_real_T( &(rtb_LookupTableq1),
                           ellaCtrl_ConstP.LookupTableq1_YDat,
                           rtb_DiscreteTimeIntegrator, ellaCtrl_ConstP.pooled1,
                           7500U);

      /* Lookup Block: '<S1>/Lookup Table q2'
       * About '<S1>/Lookup Table q2 :'
       * Lookup Block: '<S1>/Lookup Table q2'
       * Input0  Data Type:  Floating Point real_T
       * Output0 Data Type:  Floating Point real_T
       * Lookup Method: Linear_Endpoint
       *
       * XData parameter uses the same data type and scaling as Input0
       * YData parameter uses the same data type and scaling as Output0


       */
      LookUp_real_T_real_T( &(rtb_LookupTableq2),
                           ellaCtrl_ConstP.LookupTableq2_YDat,
                           rtb_DiscreteTimeIntegrator, ellaCtrl_ConstP.pooled1,
                           7500U);

      /* Lookup Block: '<S1>/Lookup Table q3'
       * About '<S1>/Lookup Table q3 :'
       * Lookup Block: '<S1>/Lookup Table q3'
       * Input0  Data Type:  Floating Point real_T
       * Output0 Data Type:  Floating Point real_T
       * Lookup Method: Linear_Endpoint
       *
       * XData parameter uses the same data type and scaling as Input0
       * YData parameter uses the same data type and scaling as Output0


       */
      LookUp_real_T_real_T( &(rtb_LookupTableq3),
                           ellaCtrl_ConstP.LookupTableq3_YDat,
                           rtb_DiscreteTimeIntegrator, ellaCtrl_ConstP.pooled1,
                           7500U);

      /* Gain: '<S1>/rad to units' */
      ellaCtrl_B.radtounits[0] = 5.7295779513082322E+004 * rtb_LookupTableq1;
      ellaCtrl_B.radtounits[1] = 5.7295779513082322E+004 * rtb_LookupTableq2;
      ellaCtrl_B.radtounits[2] = 5.7295779513082322E+004 * rtb_LookupTableq3;
      q_desired_simulink[0] = ellaCtrl_B.radtounits[0];
      q_desired_simulink[1] = ellaCtrl_B.radtounits[1];
      q_desired_simulink[2] = ellaCtrl_B.radtounits[2];
    }

    /* end of Outputs for SubSystem: '<Root>/Enabled Subsystem' */
    ellaCtrl_B.Input1 = GlobalOverride;

    /* Gain: '<Root>/Gain' incorporates:
     *  DataTypeConversion: '<Root>/Data Type  Conversion1'
     */
    rtb_Gain = 0.0001 * (real_T)ellaCtrl_B.Input1;
    ellaCtrl_B.Input[0] = measuredData.qM[0];
    ellaCtrl_B.Input[1] = measuredData.qM[1];
    ellaCtrl_B.Input[2] = measuredData.qM[2];

    /* Update for enable SubSystem: '<Root>/Enabled Subsystem' incorporates:
     *  Update for EnablePort: '<S1>/Enable'
     */
    if (ellaCtrl_DWork.EnabledSubsystem_MODE == SUBSYS_ENABLED) {
      /* Update for DiscreteIntegrator: '<S1>/Discrete-Time Integrator' */
      ellaCtrl_DWork.DiscreteTimeIntegrator_DSTATE = 0.0004 * rtb_Gain +
        ellaCtrl_DWork.DiscreteTimeIntegrator_DSTATE;
    }

    /* end of Update for SubSystem: '<Root>/Enabled Subsystem' */
  }
}

/* Model initialize function */
void ellaCtrl_initialize(void)
{
  /* Start for enable SubSystem: '<Root>/Enabled Subsystem' */

  /* InitializeConditions for DiscreteIntegrator: '<S1>/Discrete-Time Integrator' */
  ellaCtrl_DWork.DiscreteTimeIntegrator_DSTATE = 0.0;

  /* end of Start for SubSystem: '<Root>/Enabled Subsystem' */
  q0_desired_simulink[0] = ellaCtrl_ConstB.radtounits[0];
  q0_desired_simulink[1] = ellaCtrl_ConstB.radtounits[1];
  q0_desired_simulink[2] = ellaCtrl_ConstB.radtounits[2];
}

/* Model terminate function */
void ellaCtrl_terminate(void)
{
  /* (no terminate code required) */
}

/*======================== TOOL VERSION INFORMATION ==========================*
 * MATLAB 7.7 (R2008b)04-Aug-2008                                             *
 * Simulink 7.2 (R2008b)04-Aug-2008                                           *
 * Real-Time Workshop 7.2 (R2008b)04-Aug-2008                                 *
 * Real-Time Workshop Embedded Coder 5.2 (R2008b)04-Aug-2008                  *
 * Stateflow 7.2 (R2008b)04-Aug-2008                                          *
 * Stateflow Coder 7.2 (R2008b)04-Aug-2008                                    *
 * Simulink Fixed Point 6.0 (R2008b)04-Aug-2008                               *
 *============================================================================*/

/*======================= LICENSE IN USE INFORMATION =========================*
 * control_toolbox                                                            *
 * matlab                                                                     *
 * real-time_workshop                                                         *
 * rtw_embedded_coder                                                         *
 * signal_toolbox                                                             *
 * simulink                                                                   *
 *============================================================================*/
