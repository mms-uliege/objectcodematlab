/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer Industrie-Elektronik Ges.m.b.H.
 ********************************************************************
 * Program: ellaCtrl
 * File: main.c
 * Author: student
 * Created: Thu Dec 07 15:38:54 2017
 ********************************************************************
 * Implementation of program ellaCtrl
 ********************************************************************
 * Generated with B&R Automation Studio Target for Simulink V2.2
 * (ERT based)
 ********************************************************************/

#define _ASMATH_
#define ASSTRING_H_
#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

#include "ellaCtrl.h"
#include "rtwtypes.h"

/*  Defines */

/*  Data Types */

/**************************** GLOBAL DATA *************************************/
/*  Definitions */

/*  Declarations  */

/***************************** FILE SCOPE DATA ********************************/

/*************************** FUNCTIONS ****************************************/
void _INIT mainINIT(void)
{
  UDINT st_ident;
  RTInfo_typ rt_info;
  rt_info.enable = 1;
  RTInfo(&rt_info);
  if (rt_info.cycle_time != 400) {
    /* cycle time does not match Simulink fixed-step size */
    ST_ident("control", 0, &st_ident);
    ST_tmp_suspend(st_ident);
    ERR_warning(33310, 0);
  }

  /* initialize model */
  ellaCtrl_initialize();
}

void _CYCLIC mainCYCLIC(void)
{
  /* call model step function */
  ellaCtrl_step();

  /* Get model outputs here */
}

void _EXIT mainEXIT(void)
{
  /* terminate model */
  ellaCtrl_terminate();
}

/*****************************************************************************
   Task sample time: 3.9999999999999996E-004s
 *****************************************************************************/

/*****************************************************************************
   B&R Automation Studio Target for Simulink Version: V2.2.128 (04-Mar-2010)
 *****************************************************************************/

/*======================== TOOL VERSION INFORMATION ==========================*
 * MATLAB 7.7 (R2008b)04-Aug-2008                                             *
 * Simulink 7.2 (R2008b)04-Aug-2008                                           *
 * Real-Time Workshop 7.2 (R2008b)04-Aug-2008                                 *
 * Real-Time Workshop Embedded Coder 5.2 (R2008b)04-Aug-2008                  *
 * Stateflow 7.2 (R2008b)04-Aug-2008                                          *
 * Stateflow Coder 7.2 (R2008b)04-Aug-2008                                    *
 * Simulink Fixed Point 6.0 (R2008b)04-Aug-2008                               *
 *============================================================================*/

/*======================= LICENSE IN USE INFORMATION =========================*
 * control_toolbox                                                            *
 * matlab                                                                     *
 * real-time_workshop                                                         *
 * rtw_embedded_coder                                                         *
 * signal_toolbox                                                             *
 * simulink                                                                   *
 *============================================================================*/
