/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer Industrie-Elektronik Ges.m.b.H.
 ********************************************************************
 * Program: ellaCtrl
 * File: ellaCtrl.h
 * Author: student
 * Created: Thu Dec 07 15:38:54 2017
 ********************************************************************
 * Header for program ellaCtrl
 ********************************************************************
 * Generated with B&R Automation Studio Target for Simulink V2.2
 * (ERT based)
 ********************************************************************/

#ifndef RTW_HEADER_ellaCtrl_h_
#define RTW_HEADER_ellaCtrl_h_
#ifndef ellaCtrl_COMMON_INCLUDES_
# define ellaCtrl_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* ellaCtrl_COMMON_INCLUDES_ */

#include "ellaCtrl_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((void*) 0)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((void) 0)
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((void*) 0)
#endif

#ifndef MIN
#define MIN(a,b)                       ((a) < (b) ? (a) : (b))
#endif

#ifndef MAX
#define MAX(a,b)                       ((a) > (b) ? (a) : (b))
#endif

/* Block signals (auto storage) */
typedef struct {
  real_T Input[3];                     /* '<Root>/Input' */
  real_T Product[3];                   /* '<S1>/Product' */
  real_T radtounits[3];                /* '<S1>/rad to units' */
  int16_T Input1;                      /* '<Root>/Input1' */
} BlockIO_ellaCtrl;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T DiscreteTimeIntegrator_DSTATE;/* '<S1>/Discrete-Time Integrator' */
  int_T EnabledSubsystem_MODE;         /* '<Root>/Enabled Subsystem' */
} D_Work_ellaCtrl;

/* Invariant block signals (auto storage) */
typedef struct {
  const real_T radtounits[3];          /* '<Root>/rad to units' */
} ConstBlockIO_ellaCtrl;

/* Constant parameters (auto storage) */
typedef struct {
  /* Expression: Time
   * Referenced by blocks:
   * '<S1>/Lookup Table q1'
   * '<S1>/Lookup Table q2'
   * '<S1>/Lookup Table q3'
   * '<S1>/Lookup Table q4'
   * '<S1>/Lookup Table q5'
   * '<S1>/Lookup Table q6'
   */
  real_T pooled1[7501];

  /* Expression: JointTorque(:,1)
   * '<S1>/Lookup Table q4'
   */
  real_T LookupTableq4_YDat[7501];

  /* Expression: JointTorque(:,2)
   * '<S1>/Lookup Table q5'
   */
  real_T LookupTableq5_YDat[7501];

  /* Expression: JointTorque(:,3)
   * '<S1>/Lookup Table q6'
   */
  real_T LookupTableq6_YDat[7501];

  /* Expression: JointPose(:,1)
   * '<S1>/Lookup Table q1'
   */
  real_T LookupTableq1_YDat[7501];

  /* Expression: JointPose(:,2)
   * '<S1>/Lookup Table q2'
   */
  real_T LookupTableq2_YDat[7501];

  /* Expression: JointPose(:,3)
   * '<S1>/Lookup Table q3'
   */
  real_T LookupTableq3_YDat[7501];
} ConstParam_ellaCtrl;

/* Block signals (auto storage) */
extern BlockIO_ellaCtrl ellaCtrl_B;

/* Block states (auto storage) */
extern D_Work_ellaCtrl ellaCtrl_DWork;
extern ConstBlockIO_ellaCtrl ellaCtrl_ConstB;/* constant block i/o */

/* Constant parameters (auto storage) */
extern const ConstParam_ellaCtrl ellaCtrl_ConstP;

/* Model entry point functions */
extern void ellaCtrl_initialize(void);
extern void ellaCtrl_step(void);
extern void ellaCtrl_terminate(void);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : ellaCtrl
 * '<S1>'   : ellaCtrl/Enabled Subsystem
 */
#endif                                 /* RTW_HEADER_ellaCtrl_h_ */
