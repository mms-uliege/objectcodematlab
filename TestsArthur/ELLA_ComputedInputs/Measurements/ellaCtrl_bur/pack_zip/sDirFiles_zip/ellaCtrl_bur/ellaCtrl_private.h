/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer Industrie-Elektronik Ges.m.b.H.
 ********************************************************************
 * Program: ellaCtrl
 * File: ellaCtrl_private.h
 * Author: student
 * Created: Thu Dec 07 15:38:54 2017
 ********************************************************************
 * Header for program ellaCtrl
 ********************************************************************
 * Generated with B&R Automation Studio Target for Simulink V2.2
 * (ERT based)
 ********************************************************************/

#ifndef RTW_HEADER_ellaCtrl_private_h_
#define RTW_HEADER_ellaCtrl_private_h_
#include "rtwtypes.h"
#define _ASMATH_
#define ASSTRING_H_
#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

#ifndef __RTWTYPES_H__
#error This file requires rtwtypes.h to be included
#else
#ifdef TMWTYPES_PREVIOUSLY_INCLUDED
#error This file requires rtwtypes.h to be included before tmwtypes.h
#else

/* Check for inclusion of an incorrect version of rtwtypes.h */
#ifndef RTWTYPES_ID_C08S16I32L32N32F1
#error This code was generated with a different "rtwtypes.h" than the file included
#endif                                 /* RTWTYPES_ID_C08S16I32L32N32F1 */
#endif                                 /* TMWTYPES_PREVIOUSLY_INCLUDED */
#endif                                 /* __RTWTYPES_H__ */

#ifndef MIN
#define MIN(a,b)                       ((a) < (b) ? (a) : (b))
#endif

#ifndef MAX
#define MAX(a,b)                       ((a) > (b) ? (a) : (b))
#endif

/* Imported (extern) block parameters */
extern boolean_T enableControl;        /* Variable: enableControl
                                        * '<Root>/enableControl'
                                        */
extern boolean_T enableFF;             /* Variable: enableFF
                                        * '<S1>/enableControl'
                                        */
void BINARYSEARCH_real_T( uint32_T *piLeft, uint32_T *piRght, real_T u, const
  real_T *pData, uint32_T iHi);
void LookUp_real_T_real_T( real_T *pY, const real_T *pYData, real_T u, const
  real_T *pUData, uint32_T iHi);

#endif                                 /* RTW_HEADER_ellaCtrl_private_h_ */
