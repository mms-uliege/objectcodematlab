/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer Industrie-Elektronik Ges.m.b.H.
 ********************************************************************
 * Program: ellaCtrl
 * File: ellaCtrl_types.h
 * Author: student
 * Created: Thu Dec 07 15:38:54 2017
 ********************************************************************
 * Header for program ellaCtrl
 ********************************************************************
 * Generated with B&R Automation Studio Target for Simulink V2.2
 * (ERT based)
 ********************************************************************/

#ifndef RTW_HEADER_ellaCtrl_types_h_
#define RTW_HEADER_ellaCtrl_types_h_
#include "rtwtypes.h"
#ifndef MIN
#define MIN(a,b)                       ((a) < (b) ? (a) : (b))
#endif

#ifndef MAX
#define MAX(a,b)                       ((a) > (b) ? (a) : (b))
#endif
#endif                                 /* RTW_HEADER_ellaCtrl_types_h_ */
