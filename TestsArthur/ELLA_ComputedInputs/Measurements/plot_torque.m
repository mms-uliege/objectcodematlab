%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% load('3sTrajRigid\JointTorqueR3s.mat')
% load('3sTrajRigid\TimeR3s.mat')
% load('rigidTraj_100percent.mat')

% rigid
% load('3sTrajRigidCoulCut\JointTorqueR3sCoulCut.mat')
% load('3sTrajRigidCoulCut\TimeR3sCoulCut.mat')
% load('3sTrajRigidCoulCut_100percent.mat')

load('3sTrajCoulCutBis\JointTorque3sCoulCutBis.mat')
load('3sTrajCoulCutBis\Time3sCoulCutBis.mat')
load('3sTrajCoulCutBis_100percent.mat')

close all
clc

figure
subplot(3,1,1)
hold all
plot(Time,JointTorque(1,:)')
plot(TARGET_DATA____cyclic_data_3_0_(1,:),TARGET_DATA____cyclic_data_3_0_(2,:));
ylabel('Torque 1 in Nm')
xlim([0,Time(end)])
grid on

subplot(3,1,2)
hold all
plot(Time,JointTorque(2,:)')
plot(TARGET_DATA____cyclic_data_3_1_(1,:),TARGET_DATA____cyclic_data_3_1_(2,:));
ylabel('Torque 2 in Nm')
xlim([0,Time(end)])
grid on

subplot(3,1,3)
hold all
plot(Time,JointTorque(3,:)')
plot(TARGET_DATA____cyclic_data_3_2_(1,:),TARGET_DATA____cyclic_data_3_2_(2,:));
xlabel('t in s')
ylabel('Torque 3 in Nm')
xlim([0,Time(end)])
grid on

% legend('M_{Mot1,sim}','M_{Mot2,sim}','M_{Mot3,sim}', 'M_{Mot1}','M_{Mot2}','M_{Mot3}')



figure;
plot(Time,JointPose)
legend('1','2','3')
ylabel('position in rad')
xlabel('t in s')
grid on

figure;
plot(Time,[zeros(1,size(JointPose,2));diff(JointPose)]/Ta)
legend('1','2','3')
ylabel('velocity in rad/s')
xlabel('t in s')
grid on

figure;
plot(Time,[zeros(2,size(JointPose,2));diff(diff(JointPose))]/Ta^2)
legend('1','2','3')
ylabel('acceleration in rad/s^2')
xlabel('t in s')
grid on


%%
figure
subplot(3,1,1)
hold all
load('3sTrajRigidCoulCut_100percent.mat')
plot(TARGET_DATA____cyclic_data_3_0_(1,:),TARGET_DATA____cyclic_data_3_0_(2,:));
load('3sTrajCoulCutBis_100percent.mat')
plot(TARGET_DATA____cyclic_data_3_0_(1,:),TARGET_DATA____cyclic_data_3_0_(2,:));
load('3sTrajCoulCutBisFF_100percent.mat')
plot(TARGET_DATA____cyclic_data_3_0_(1,:),TARGET_DATA____cyclic_data_3_0_(2,:));
ylabel('Torque 1 in Nm')
xlim([0,Time(end)])
grid on

subplot(3,1,2)
hold all
load('3sTrajRigidCoulCut_100percent.mat')
plot(TARGET_DATA____cyclic_data_3_1_(1,:),TARGET_DATA____cyclic_data_3_1_(2,:));
load('3sTrajCoulCutBis_100percent.mat')
plot(TARGET_DATA____cyclic_data_3_1_(1,:),TARGET_DATA____cyclic_data_3_1_(2,:));
load('3sTrajCoulCutBisFF_100percent.mat')
plot(TARGET_DATA____cyclic_data_3_1_(1,:),TARGET_DATA____cyclic_data_3_1_(2,:));
ylabel('Torque 2 in Nm')
xlim([0,Time(end)])
grid on

subplot(3,1,3)
hold all
load('3sTrajRigidCoulCut_100percent.mat')
plot(TARGET_DATA____cyclic_data_3_2_(1,:),TARGET_DATA____cyclic_data_3_2_(2,:));
load('3sTrajCoulCutBis_100percent.mat')
plot(TARGET_DATA____cyclic_data_3_2_(1,:),TARGET_DATA____cyclic_data_3_2_(2,:));
load('3sTrajCoulCutBisFF_100percent.mat')
plot(TARGET_DATA____cyclic_data_3_2_(1,:),TARGET_DATA____cyclic_data_3_2_(2,:));
xlabel('t in s')
ylabel('Torque 3 in Nm')
xlim([0,Time(end)])
grid on



