%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

% close all
% clc



load('1sTrajRigidCoulCut_100percent.mat')
tvec = TARGET_DATA____measuredData_MMot_0_(1,:);

rigidTraj.MMot(:,1) = TARGET_DATA____measuredData_MMot_0_(2,:)';
rigidTraj.MMot(:,2) = TARGET_DATA____measuredData_MMot_1_(2,:)';
rigidTraj.MMot(:,3) = TARGET_DATA____measuredData_MMot_2_(2,:)';
rigidTraj.acc(:,1) = TARGET_DATA____measuredData_aIMU2_0_(2,:)';
rigidTraj.acc(:,2) = TARGET_DATA____measuredData_aIMU2_1_(2,:)';
rigidTraj.acc(:,3) = TARGET_DATA____measuredData_aIMU2_2_(2,:)';

% load('1sTrajRigidCoulCutFF_100percent.mat')
% rigidTrajFF.MMot(:,1) = TARGET_DATA____measuredData_MMot_0_(2,:)';
% rigidTrajFF.MMot(:,2) = TARGET_DATA____measuredData_MMot_1_(2,:)';
% rigidTrajFF.MMot(:,3) = TARGET_DATA____measuredData_MMot_2_(2,:)';
% rigidTrajFF.acc(:,1) = TARGET_DATA____measuredData_aIMU2_0_(2,:)';
% rigidTrajFF.acc(:,2) = TARGET_DATA____measuredData_aIMU2_1_(2,:)';
% rigidTrajFF.acc(:,3) = TARGET_DATA____measuredData_aIMU2_2_(2,:)';

load('1sTrajCoulCut_100percent.mat')
flexTraj.MMot(:,1) = TARGET_DATA____measuredData_MMot_0_(2,:)';
flexTraj.MMot(:,2) = TARGET_DATA____measuredData_MMot_1_(2,:)';
flexTraj.MMot(:,3) = TARGET_DATA____measuredData_MMot_2_(2,:)';
flexTraj.acc(:,1) = TARGET_DATA____measuredData_aIMU2_0_(2,:)';
flexTraj.acc(:,2) = TARGET_DATA____measuredData_aIMU2_1_(2,:)';
flexTraj.acc(:,3) = TARGET_DATA____measuredData_aIMU2_2_(2,:)';

load('1sTrajCoulCutFF_100percent.mat')
flexTrajFF.MMot(:,1) = TARGET_DATA____measuredData_MMot_0_(2,:)';
flexTrajFF.MMot(:,2) = TARGET_DATA____measuredData_MMot_1_(2,:)';
flexTrajFF.MMot(:,3) = TARGET_DATA____measuredData_MMot_2_(2,:)';
flexTrajFF.acc(:,1) = TARGET_DATA____measuredData_aIMU2_0_(2,:)';
flexTrajFF.acc(:,2) = TARGET_DATA____measuredData_aIMU2_1_(2,:)';
flexTrajFF.acc(:,3) = TARGET_DATA____measuredData_aIMU2_2_(2,:)';

load('1sTrajCoulCut\JointTorque1sCoulCut.mat')
load('1sTrajCoulCut\Time1sCoulCut.mat')
FlexTorques = JointTorque;
FlexTime = Time;

load('1sTrajRigidCoulCut\JointTorqueR1sCoulCut.mat')
load('1sTrajRigidCoulCut\TimeR1sCoulCut.mat')
RigidTorques = JointTorque;
RigidTime = Time;

co = [0 0 1;
      0 0.5 0;
      1 0 0;
      0 0.75 0.75;
      0.75 0 0.75;
      0.75 0.75 0;
      0.25 0.25 0.25];
set(groot,'defaultAxesColorOrder',co)

t = find(and(tvec>=1.99,tvec<=2.01),1);

% figure
% subplot(3,1,1)
% hold all
% plot(tvec, rigidTraj.MMot(:,1))
% plot(tvec, flexTraj.MMot(:,1))
% plot(tvec, flexTrajFF.MMot(:,1))
% ylabel('Motor Torque 1 in Nm')
% subplot(3,1,2)
% hold all
% plot(tvec, rigidTraj.MMot(:,2))
% plot(tvec, flexTraj.MMot(:,2))
% plot(tvec, flexTrajFF.MMot(:,2))
% ylabel('Motor Torque 2 in Nm')
% subplot(3,1,3)
% hold all
% plot(tvec, rigidTraj.MMot(:,3))
% plot(tvec, flexTraj.MMot(:,3))
% plot(tvec, flexTrajFF.MMot(:,3))
% ylabel('Motor Torque 3 in Nm')
% xlabel('t in s')
% xlim([0,tvec(end)])
% grid on
% legend('rigid','flexible','flexible+FF','Location','Best')
% 
% 
% figure
% subplot(3,1,1)
% hold all
% plot(tvec, rigidTraj.acc(:,1))
% plot(tvec, flexTraj.acc(:,1))
% plot(tvec, flexTrajFF.acc(:,1))
% ylabel('Acceleration X in rad/s^2')
% subplot(3,1,2)
% hold all
% plot(tvec, rigidTraj.acc(:,2))
% plot(tvec, flexTraj.acc(:,2))
% plot(tvec, flexTrajFF.acc(:,2))
% ylabel('Acceleration Y in rad/s^2')
% subplot(3,1,3)
% hold all
% plot(tvec, rigidTraj.acc(:,3))
% plot(tvec, flexTraj.acc(:,3))
% plot(tvec, flexTrajFF.acc(:,3))
% ylabel('Acceleration Z in rad/s^2')
% xlabel('t in s')
% xlim([0,tvec(end)])
% grid on
% legend('rigid','flexible','flexible+FF','Location','Best')

shiftTime = 0.2;

figure
hold all
plot(tvec, rigidTraj.MMot(:,1))
plot(tvec, flexTraj.MMot(:,1))
plot(tvec, flexTrajFF.MMot(:,1))
plot(FlexTime,FlexTorques(1,:))
plot(RigidTime,RigidTorques(1,:))
if exist('TrajTorqueMotor')
    plot(TrajTorqueMotor.Time+shiftTime,TrajTorqueMotor.Data(:,1))
end
ylabel('Motor Torque 1 in Nm')
xlabel('t in s')
xlim([0,tvec(t)])
grid on
legend('rigid','flexible','flexible+FF','Computed flex','Computed rigid','Location','Best')

figure
hold all
plot(tvec, rigidTraj.MMot(:,2))
plot(tvec, flexTraj.MMot(:,2))
plot(tvec, flexTrajFF.MMot(:,2))
plot(FlexTime,FlexTorques(2,:))
plot(RigidTime,RigidTorques(2,:))
if exist('TrajTorqueMotor')
    plot(TrajTorqueMotor.Time+shiftTime,TrajTorqueMotor.Data(:,2))
end
ylabel('Motor Torque 2 in Nm')
xlabel('t in s')
xlim([0,tvec(t)])
grid on
legend('rigid','flexible','flexible+FF','Computed flex','Computed rigid','Location','Best')

figure
hold all
plot(tvec, rigidTraj.MMot(:,3))
plot(tvec, flexTraj.MMot(:,3))
plot(tvec, flexTrajFF.MMot(:,3))
plot(FlexTime,FlexTorques(3,:))
plot(RigidTime,RigidTorques(3,:))
if exist('TrajTorqueMotor')
    plot(TrajTorqueMotor.Time+shiftTime,TrajTorqueMotor.Data(:,3))
end
ylabel('Motor Torque 3 in Nm')
xlabel('t in s')
xlim([0,tvec(t)])
grid on
legend('rigid','flexible','flexible+FF','Computed flex','Computed rigid','Location','Best')

%% Accelerations
figure
hold all
plot(tvec, rigidTraj.acc(:,1))
plot(tvec, flexTraj.acc(:,1))
plot(tvec, flexTrajFF.acc(:,1))
ylabel('Acceleration X in rad/s^2')
xlabel('t in s')
xlim([0,tvec(t)])
grid on
legend('rigid','flexible','flexible+FF','Location','Best')

figure
hold all
plot(tvec, rigidTraj.acc(:,2))
plot(tvec, flexTraj.acc(:,2))
plot(tvec, flexTrajFF.acc(:,2))
ylabel('Acceleration Y in rad/s^2')
xlabel('t in s')
xlim([0,tvec(t)])
grid on
legend('rigid','flexible','flexible+FF','Location','Best')

figure
hold all
plot(tvec, rigidTraj.acc(:,3))
plot(tvec, flexTraj.acc(:,3))
plot(tvec, flexTrajFF.acc(:,3))
ylabel('Acceleration Z in rad/s^2')
xlabel('t in s')
xlim([0,tvec(t)])
grid on
legend('rigid','flexible','flexible+FF','Location','Best')