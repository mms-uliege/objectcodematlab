%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all

% rigid
load('3sTrajRigidCoulCut\JointPoseR3sCoulCut.mat')
load('3sTrajRigidCoulCut\TimeR3sCoulCut.mat')
load('3sTrajRigidCoulCut\JointTorqueR3sCoulCut.mat')

% load('3sTrajCoulCut\JointPose3sCoulCut.mat')
% load('3sTrajCoulCut\Time3sCoulCut.mat')
% load('3sTrajCoulCut\JointTorque3sCoulCut.mat')


Ta = Time(2)-Time(1)

JointPose = JointPose';
JointTorque = JointTorque';


figure(1);
plot(Time,JointPose)
legend('1','2','3')
ylabel('position in rad')
xlabel('t in s')
grid on

figure(2);
plot(Time,[zeros(1,size(JointPose,2));diff(JointPose)]/Ta)
legend('1','2','3')
ylabel('velocity in rad/s')
xlabel('t in s')
grid on

figure(3);
plot(Time,[zeros(2,size(JointPose,2));diff(diff(JointPose))]/Ta^2)
legend('1','2','3')
ylabel('acceleration in rad/s^2')
xlabel('t in s')
grid on

figure(4);
plot(Time,JointTorque)
legend('1','2','3')
ylabel('torque in Nm')
xlabel('t in s')
grid on



load('3sTrajCoulCutBis\JointPose3sCoulCutBis.mat')
load('3sTrajCoulCutBis\Time3sCoulCutBis.mat')
load('3sTrajCoulCutBis\JointTorque3sCoulCutBis.mat')
Ta = Time(2)-Time(1);
JointPose = JointPose';
JointTorque = JointTorque';

figure(1);
hold all
plot(Time,JointPose);
figure(2);
hold all
plot(Time,[zeros(1,size(JointPose,2));diff(JointPose)]/Ta);
figure(3);
hold all
plot(Time,[zeros(2,size(JointPose,2));diff(diff(JointPose))]/Ta^2);
figure(4);
hold all
plot(Time,JointTorque)


