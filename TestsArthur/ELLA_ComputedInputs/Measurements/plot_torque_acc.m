%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

close all


figure
subplot(3,1,1)
hold all
load('3sTrajRigidCoulCut_100percent_acc.mat')
plot(TARGET_DATA____measuredData_MMot_0_(1,:),TARGET_DATA____measuredData_MMot_0_(2,:));
load('3sTrajRigidCoulCutFF_100percent_acc.mat')
plot(TARGET_DATA____measuredData_MMot_0_(1,:),TARGET_DATA____measuredData_MMot_0_(2,:));
load('3sTrajCoulCutBis_100percent_acc.mat')
plot(TARGET_DATA____measuredData_MMot_0_(1,:),TARGET_DATA____measuredData_MMot_0_(2,:));
load('3sTrajCoulCutBisFF_100percent_acc.mat')
plot(TARGET_DATA____measuredData_MMot_0_(1,:),TARGET_DATA____measuredData_MMot_0_(2,:));
ylabel('Torque 1 in Nm')
xlim([0,Time(end)])
grid on

subplot(3,1,2)
hold all
load('3sTrajRigidCoulCut_100percent_acc.mat')
plot(TARGET_DATA____measuredData_MMot_1_(1,:),TARGET_DATA____measuredData_MMot_1_(2,:));
load('3sTrajRigidCoulCutFF_100percent_acc.mat')
plot(TARGET_DATA____measuredData_MMot_1_(1,:),TARGET_DATA____measuredData_MMot_1_(2,:));
load('3sTrajCoulCutBis_100percent_acc.mat')
plot(TARGET_DATA____measuredData_MMot_1_(1,:),TARGET_DATA____measuredData_MMot_1_(2,:));
load('3sTrajCoulCutBisFF_100percent_acc.mat')
plot(TARGET_DATA____measuredData_MMot_1_(1,:),TARGET_DATA____measuredData_MMot_1_(2,:));
ylabel('Torque 2 in Nm')
xlim([0,Time(end)])
grid on

subplot(3,1,3)
hold all
load('3sTrajRigidCoulCut_100percent_acc.mat')
plot(TARGET_DATA____measuredData_MMot_2_(1,:),TARGET_DATA____measuredData_MMot_2_(2,:));
load('3sTrajRigidCoulCutFF_100percent_acc.mat')
plot(TARGET_DATA____measuredData_MMot_2_(1,:),TARGET_DATA____measuredData_MMot_2_(2,:));
load('3sTrajCoulCutBis_100percent_acc.mat')
plot(TARGET_DATA____measuredData_MMot_2_(1,:),TARGET_DATA____measuredData_MMot_2_(2,:));
load('3sTrajCoulCutBisFF_100percent_acc.mat')
plot(TARGET_DATA____measuredData_MMot_2_(1,:),TARGET_DATA____measuredData_MMot_2_(2,:));
xlabel('t in s')
ylabel('Torque 3 in Nm')
xlim([0,Time(end)])
grid on
legend('rigid','rigid+FF','flexible','flexible+FF','Location','Best')

%%
figure
subplot(3,1,1)
hold all
load('3sTrajRigidCoulCut_100percent_acc.mat')
plot(TARGET_DATA____measuredData_aIMU2_0_(1,:),TARGET_DATA____measuredData_aIMU2_0_(2,:));
load('3sTrajRigidCoulCutFF_100percent_acc.mat')
plot(TARGET_DATA____measuredData_aIMU2_0_(1,:),TARGET_DATA____measuredData_aIMU2_0_(2,:));
load('3sTrajCoulCutBis_100percent_acc.mat')
plot(TARGET_DATA____measuredData_aIMU2_0_(1,:),TARGET_DATA____measuredData_aIMU2_0_(2,:));
load('3sTrajCoulCutBisFF_100percent_acc.mat')
plot(TARGET_DATA____measuredData_aIMU2_0_(1,:),TARGET_DATA____measuredData_aIMU2_0_(2,:));
ylabel('acceleration in rad/s^2')
xlim([0,Time(end)])
grid on

subplot(3,1,2)
hold all
load('3sTrajRigidCoulCut_100percent_acc.mat')
plot(TARGET_DATA____measuredData_aIMU2_1_(1,:),TARGET_DATA____measuredData_aIMU2_1_(2,:));
load('3sTrajRigidCoulCutFF_100percent_acc.mat')
plot(TARGET_DATA____measuredData_aIMU2_1_(1,:),TARGET_DATA____measuredData_aIMU2_1_(2,:));
load('3sTrajCoulCutBis_100percent_acc.mat')
plot(TARGET_DATA____measuredData_aIMU2_1_(1,:),TARGET_DATA____measuredData_aIMU2_1_(2,:));
load('3sTrajCoulCutBisFF_100percent_acc.mat')
plot(TARGET_DATA____measuredData_aIMU2_1_(1,:),TARGET_DATA____measuredData_aIMU2_1_(2,:));
ylabel('acceleration in rad/s^2')
xlim([0,Time(end)])
grid on

subplot(3,1,3)
hold all
load('3sTrajRigidCoulCut_100percent_acc.mat')
plot(TARGET_DATA____measuredData_aIMU2_2_(1,:),TARGET_DATA____measuredData_aIMU2_2_(2,:));
load('3sTrajRigidCoulCutFF_100percent_acc.mat')
plot(TARGET_DATA____measuredData_aIMU2_2_(1,:),TARGET_DATA____measuredData_aIMU2_2_(2,:));
load('3sTrajCoulCutBis_100percent_acc.mat')
plot(TARGET_DATA____measuredData_aIMU2_2_(1,:),TARGET_DATA____measuredData_aIMU2_2_(2,:));
load('3sTrajCoulCutBisFF_100percent_acc.mat')
plot(TARGET_DATA____measuredData_aIMU2_2_(1,:),TARGET_DATA____measuredData_aIMU2_2_(2,:));
xlabel('t in s')
ylabel('acceleration in rad/s^2')
xlim([0,Time(end)])
grid on
legend('rigid','rigid+FF','flexible','flexible+FF','Location','Best')
