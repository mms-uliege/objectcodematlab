%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc;
clear all;
close all;

s=tf('s');
Ta=0.0004;
%Ta=0.01;


%% Parameter Ella
%============================================================



%Tiefpassfilter f�r Gyro-signal qA1
Gs1=1/(s/(2*pi*10)+1);
Gz1=c2d(Gs1,Ta,'zoh');

%Tiefpassfilter f�r Beschl.-signal
Gs2=1/(s/(2*pi*15)+1);
Gz2=c2d(Gs2,Ta,'zoh');

%Tiefpassfilter f�r Motorsignale
Gs3=1/(s/(2*pi*50)+1);
Gz3=c2d(Gs3,Ta,'zoh');

%Filter f�r Reibungsidentifikation
wss=20;
GR=1/(s^2/wss^2+2*s/wss+1);
GRz=c2d(GR,Ta,'zoh');

%Filter f�r sanftes einschalten
wss=6;
Gsanfts=1/(s^2/wss^2+2*s/wss+1);
Gsanftz=c2d(Gsanfts,Ta,'zoh');
Sys_sanft=ss(Gsanftz);
asanft=Sys_sanft.a;
bsanft=Sys_sanft.b;
csanft=Sys_sanft.c;

%Filter f�r sanftes einschalten
wss=1.5;
Gsanft2s=1/(s^2/wss^2+2*s/wss+1);
Gsanft2z=c2d(Gsanft2s,Ta,'zoh');
Sys_sanft2=ss(Gsanft2z);
asanft2=Sys_sanft2.a;
bsanft2=Sys_sanft2.b;
csanft2=Sys_sanft2.c;

%Filter f�r sanftes einschalten
wss=5;
Gsanft3s=1/(s^2/wss^2+2*s/wss+1);
Gsanft3z=c2d(Gsanft3s,Ta,'zoh');
Sys_sanft3=ss(Gsanft3z);
asanft3=Sys_sanft3.a;
bsanft3=Sys_sanft3.b;
csanft3=Sys_sanft3.c;

%sanftes Ableiten f�r Beschleunigungssignal
Gas=s/(s/50+1);
Gaz=c2d(Gas,Ta,'zoh');

%sanftes Ableiten f�r qA_ppp
Gbs=s/(s/20+1);
Gbz=c2d(Gbs,Ta,'zoh');

g=9.81;
rho=7800; %Stahl
E_mod=21E10; %Stahl
nue=0.27; %Querdehnungszahl
G_mod=E_mod/(2*(1+nue));%Stahl

%Tiefpassfilter f�r zus�tzliches Reglermoment (Einschalten)
GsRegler=1/(s/(2*pi*0.5)+1);
GzRegler=c2d(GsRegler,Ta,'zoh');

%% -------------Oberarm------------------------------------

H_2=0.030;
B_2=0.030;
d_2=0.003;
h_2=H_2-2*d_2;
b_2=B_2-2*d_2;
Hh_2=(H_2+h_2)/2;
Bb_2=(B_2+b_2)/2;

L_B2A=0.06; % Ende 1. starres St�ck
L_B2B=L_B2A+0.7005; % bis zum Ende elastisches St�ck
L_B2C=L_B2B+0.199; % bis Schwerpunkt 2. starres St�ck
L_B2=L_B2C; % gesamte L�nge des Balkens

A_el2=3.01*10e-5; %lt Hersteller f�r 30x30x3

%Iy_B2=1/12*(H_2*B_2^3-h_2*b_2^3);
%Iz_B2=1/12*(B_2*H_2^3-b_2*h_2^3);
Iy_B2=0.35*10e-8; %lt Hersteller f�r 30x30x3
Iz_B2=0.35*10e-8; %lt Hersteller f�r 30x30x3
Ix_B2=Iy_B2+Iz_B2;
ID_B2=Hh_2^3*d_2; %lt H�tte S.E72

%% -------------Unterarm-----------------------------------

H_3=0.025;
B_3=0.025;
d_3=0.0025;
h_3=H_3-2*d_3;
b_3=B_3-2*d_3;
Hh_3=(H_3+h_3)/2;
Bb_3=(B_3+b_3)/2;

L_B3A=0.194;
L_B3B=L_B3A+0.62;
L_B3C=L_B3B+0.06;
L_B3 =L_B3C+0.06;

%A_el3=H_3*B_3-h_3*b_3;
A_el3=2.09*10e-5; %lt Hersteller f�r 25x25x2.5

%mS3=rho*A_el3*(L_B3B-L_B3A)

 
%Iy_B3=1/12*(H_3*B_3^3-h_3*b_3^3)
%Iz_B3=1/12*(B_3*H_3^3-b_3*h_3^3)
Iy_B3=0.169*10e-8; %lt Hersteller f�r 25x25x2.5
Iz_B3=0.169*10e-8; %lt Hersteller f�r 25x25x2.5
Ix_B3=Iy_B3+Iz_B3; %polare Fl�chentr�gheitsmoment
ID_B3=d_3*Hh_3^3; %Torsions-Fl�chentr�gheitsmoment


%% ------------- Gesamt -----------------------------------

L_ges=L_B2+L_B3;
params_kin=[L_B2, L_B3];
l2=L_B2;
l3=L_B3;

%% ------------- Params Roboter ---------------------------

epsilon=500; %Steigung tanh der Reibkennlinie

%Motor 1
i_G1=100;
B_M1=(0.33+0.011+1.69)*10^-4; %Tr�gheit: Motor+Bremse+Getriebe
d_11=0.2;
d_12=0.00195;
d_13=(-2.0028e-9);

%Motor 2
i_G2=100;
C_M2=(0.33+0.011+1.69)*10^-4; %Tr�gheit: Motor+Bremse+Getriebe
d_21=0.14;
d_22=0.0022;
d_23=(-3.1806e-9);

%Motor 3
m_M3=3; %Gewicht: Motor+Bremse+Getriebe;
i_G3=160;
C_M3=(0.33+0.011+0.193)*10^-4; %Tr�gheit: Motor+Bremse+Getriebe;
d_31=0.07;
d_32=(4.7699e-4);
d_33=(-5.9251e-10);

%Basis
B_S1=0.3000;

%Balkenmasse 2
sA2=0.475;
m_S2=2.25;
A_S2=5.1800e-4;
B_S2=0.1625;
C_S2=0.1625;

%Endmasse 2

%Drehteil klein: 0.69kg
%Drehteil gro�: 0.66kg
%Anbindung 1: 0.510kg
%Anbindung 2: 0.566kg
%Scheibe Stator: 0.258kg
%Scheibe Rotor: 0.528kg

%m_E2=0.69+0.66+0.51+0.566+0.258+0.528;
m_E2=3.3; %weil Schrauben noch nicht enthalten sind!

V_E2=0.15^2*pi/4*0.2; %fiktives Volumen
rho_E2=(m_E2+m_M3)/V_E2; %fiktive Dichte

A_E2=1*(1/4*(m_E2+m_M3)*(0.15/2)^2+1/12*(m_E2+m_M3)*(0.2)^2);
B_E2=1*(1/4*(m_E2+m_M3)*(0.15/2)^2+1/12*(m_E2+m_M3)*(0.2)^2);
C_E2=1/2*(m_E2+m_M3)*(0.15/2)^2;

%Balkenmasse 3
sA3=0.505;
m_S3=1.4;
A_S3=2.18e-4;
B_S3=0.08374; 
C_S3=0.08374;   

%Endmasse 3
sE3=l3-0.06;
%m_E3=(3.07+0.1); % gro�e Masse plus Masse der Kugel vom Lasertracker
%m_E3=0.566; % kleine Masse ohne Zusatz
%m_E3=1.254; % kleine Masse mit einer Zusatzplatte (+0.688kg)
m_E3=2.1; % kleine Masse mit zwei Zusatzplatte (+2*0.688kg)

A_E3=2.066e-3;
B_E3=3.3e-3;
C_E3=4.378e-3;

%Steifigkeiten f�r gro�e Masse mE3=3kg
% k1=8500;
% k2=12500;
% k3=6500;

%Steifigkeiten f�r mE3=2.1kg und einfaches Vo-Modell
k1=10000;
k2=13500;
k3=7500;

%D�mpfungen Armkoordinate
d_A1=0.25;
d_A2=0.25;
d_A3=0.1;

%Abst�nde der IMUs
rx_IMU1=-0.01;
ry_IMU1=-0.095;
rz_IMU1=-0.04;

rx_IMU2=-0.075;
ry_IMU2=0.035;
rz_IMU2=0;

%Achtung in rE l3 auf sE3 ge�ndert wegen Lasermessung!

%% RedModell-Parameter ohne Torsion
%=========================================================================

%Parameter f�r Beschleunigungsregelung
m2=m_E2+m_M3+m_S2/2;
%m3=m_E3+m_S3/2+0.35;
m3=m_E3+m_S3/2;

%Parameter f�r qA Berechnung
params_qA=[l2,sE3,m2,m3];

%Prarameter f�r Bahnkorrektur mit MLEM
params_qR_LEM=[g,l2,sE3,m2,1.05*m3,k1,k2,k3,B_S1];

params_Vo=[g,i_G1,i_G2,i_G3,...
        B_M1,C_M2,C_M3,...
        epsilon,...
        d_11,d_12,0*d_13,...
        d_21,d_22,0*d_23,...
        d_31,d_32,0*d_33,...
        sA2,l2,sA3,sE3,l3,...
        B_S1,...
        m_S2,A_S2,B_S2,C_S2,...
        m_E2+m_M3,A_E2,B_E2,C_E2,...
        m_S3,A_S3,B_S3,C_S3,...
        m_E3,A_E3,B_E3,C_E3,...
        d_A1,d_A2,d_A3,...
        rx_IMU1,ry_IMU1,rz_IMU1,...
        rx_IMU2,ry_IMU2,rz_IMU2];

params_6FG=...
       [g,i_G1,i_G2,i_G3,...
        B_M1,C_M2,C_M3,...
        epsilon,...
        d_11,d_12,d_13,...
        d_21,d_22,d_23,...
        d_31,d_32,d_33,...
        k1,k2,k3,...
        sA2,l2,sA3,sE3,l3,...
        B_S1,...
        m_S2,A_S2,B_S2,C_S2,...
        m_E2+m_M3,A_E2,B_E2,C_E2,...
        m_S3,A_S3,B_S3,C_S3,...
        m_E3,A_E3,B_E3,C_E3,...
        d_A1,d_A2,d_A3,...
        rx_IMU1,ry_IMU1,rz_IMU1,...
        rx_IMU2,ry_IMU2,rz_IMU2];

params_cT=[g,i_G1,i_G2,i_G3,...
        B_M1,C_M2,C_M3,...
        epsilon,...
        d_11,d_12,d_13,...
        d_21,d_22,d_23,...
        d_31,d_32,d_33,...
        sA2,l2,sA3,sE3,l3,...
        B_S1,...
        m_S2,0*A_S2,0*B_S2,0*C_S2,...
        m_E2+m_M3,0*A_E2,0*B_E2,0*C_E2,...
        m_S3,0*A_S3,0*B_S3,0*C_S3,...
        m_E3,0*A_E3,0*B_E3,0*C_E3,...
        d_A1,d_A2,d_A3];   

params_Vo_qpppp = [g,i_G1,i_G2,i_G3,B_M1,C_M2,C_M3,epsilon,d_11,d_12,d_13,d_21,d_22,d_23,d_31,d_32,d_33,k1,k2,k3,sA2,l2,sA3,sE3,l3,B_S1,m_S2,A_S2,B_S2,C_S2,m_E2,A_E2,B_E2,C_E2,m_S3,A_S3,B_S3,C_S3,m_E3,A_E3,B_E3,C_E3,d_A1,d_A2,d_A3,rx_IMU1,ry_IMU1,rz_IMU1,rx_IMU2,ry_IMU2,rz_IMU2];

k1_8FG=2500; %Torsion
k2_8FG=6000; %Biegung z1
k3_8FG=12000;  %Biegung y1
k4_8FG=3000;  %Biegung z2
k5_8FG=6500;  %Biegung y2 

params_8FG_red=...
       [g,i_G1,i_G2,i_G3,...
        B_M1,C_M2,C_M3,...
        epsilon,...
        d_11,d_12,d_13,...
        d_21,d_22,d_23,...
        d_31,d_32,d_33,...
        k1_8FG,k2_8FG,k3_8FG,k4_8FG,k5_8FG,...
        sA2,l2,sA3,sE3,sE3,...
        B_S1,...
        m_S2,...
        m_E2+m_M3,A_E2,...
        m_S3,...
        m_E3,...
        0,0,0,0,0,...
        rx_IMU1,ry_IMU1,rz_IMU1,...
        rx_IMU2,ry_IMU2,rz_IMU2];     
    
%Getriebesteifigkeiten
kGet1=120000;
kGet2=120000;
kGet3=29000;    
    
%D�mpfungsbeiwerte
dR1=0.01;
dR2=0.1;
dR3=0.01;
dR4=0.1;
dR5=0.1;
dR6=0.01;
dR7=0.1;
dR8=0.01;
dR9=0.1;

dA1=0.01;
dA2=0.01;
dA3=0.01;

%abge�nderte Masse mE2 weil die Starren Anbindungen zur Endmasse 2 gez�hlt
%werden
m_E2Ritz=m_E2+rho*A_el2*0.12+rho*A_el3*0.12;

params_Ritz=...
           [g,rho,E_mod,G_mod,...
            i_G1,i_G2,i_G3,...
            B_M1,C_M2,C_M3,B_S1,...
            epsilon,...
            d_11,d_12,0*d_13,...
            d_21,d_22,0*d_23,...
            d_31,d_32,0*d_33,...
            kGet1,kGet2,kGet3,...
            L_B2,L_B2A,L_B2B,L_B2C,...
            L_B3,L_B3A,L_B3B,L_B3C,0,...
            A_el2,ID_B2,0.5*Ix_B2,0.5*Iy_B2,0.6*Iz_B2,...
            A_el3,ID_B3,0.5*Ix_B3,0.5*Iy_B3,0.65*Iz_B3,...
            m_E2Ritz+m_M3,0*A_E2,0*B_E2,0*C_E2,...
            m_E3,0*A_E3,0*B_E3,0*C_E3,...
            0,0,0,0,0,0,0,0,0,0,0,0,...
            0*rx_IMU1,0*ry_IMU1,0*rz_IMU1,...
            0*rx_IMU2,0*ry_IMU2,0*rz_IMU2,...
            0,0,0,0,0,0];




