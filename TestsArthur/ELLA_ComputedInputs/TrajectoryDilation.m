%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save trajectories from the simulink models

duration = '115';

JointTorque1M = TrajTorqueMotor.Data(:,1);
JointTorque2M = TrajTorqueMotor.Data(:,2);
JointTorque3M = TrajTorqueMotor.Data(:,3);
JointTorqueTimeM = TrajTorqueMotor.Time;

JointTraj1M = TrajJointMotor.Data(:,1);
JointTraj2M = TrajJointMotor.Data(:,2);
JointTraj3M = TrajJointMotor.Data(:,3);
JointTrajTimeM = TrajJointMotor.Time;

JointTraj1 = TrajJoint.Data(:,1);
JointTraj2 = TrajJoint.Data(:,2);
JointTraj3 = TrajJoint.Data(:,3);
JointTrajTime = TrajJoint.Time;

trajxSimulink = CartesianTrajFromSimulink.Data(:,1);
trajySimulink = CartesianTrajFromSimulink.Data(:,2);
trajzSimulink = CartesianTrajFromSimulink.Data(:,3);
timeSimulink = CartesianTrajFromSimulink.Time;

% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTraj1_',duration],'JointTraj1')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTraj2_',duration],'JointTraj2')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTraj3_',duration],'JointTraj3')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTrajTime_',duration],'JointTrajTime')
% 
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\trajxSimulink_',duration],'trajxSimulink')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\trajySimulink_',duration],'trajySimulink')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\trajzSimulink_',duration],'trajzSimulink')
% save(['C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\timeSimulink_',duration],'timeSimulink')

save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\JointTraj1_',duration],'JointTraj1')
save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\JointTraj2_',duration],'JointTraj2')
save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\JointTraj3_',duration],'JointTraj3')
save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\JointTrajTime_',duration],'JointTrajTime')

save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\trajxSimulink_',duration],'trajxSimulink')
save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\trajySimulink_',duration],'trajySimulink')
save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\trajzSimulink_',duration],'trajzSimulink')
save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\timeSimulink_',duration],'timeSimulink')

save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\JointTraj1M_',duration],'JointTraj1M')
save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\JointTraj2M_',duration],'JointTraj2M')
save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\JointTraj3M_',duration],'JointTraj3M')
save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\JointTrajTimeM_',duration],'JointTrajTimeM')

save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\JointTorque1M_',duration],'JointTorque1M')
save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\JointTorque2M_',duration],'JointTorque2M')
save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\JointTorque3M_',duration],'JointTorque3M')
save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\Trajectories\JointTorqueTimeM_',duration],'JointTorqueTimeM')

figure
plot(JointTrajTime,[JointTraj1,JointTraj2,JointTraj3])
title(['Joint trajectory for ',duration])
xlabel('Time')
ylabel('Angle')
grid on

figure
plot(timeSimulink,[trajxSimulink,trajySimulink,trajzSimulink])
title(['Cartesian trajectory for ',duration])
xlabel('Time')
ylabel('Angle')
grid on

