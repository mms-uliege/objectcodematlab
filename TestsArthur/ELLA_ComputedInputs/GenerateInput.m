%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script to generate the input datas to be sent into the Simulink model in
% order to run simulation with optimized input.
% Load the desired S, optimized model and go look for the torque and joints
% inputs.
% Based on those, extrapolate missing data to have the correct time sampling.

clear simJointPose
clear simJointVel
clear simJointAcc
clear simJointTorque
clear JointTorque
clear JointPose
clear JointVel
clear JointAcc
clear simEndPose
clear endPose
clear EndPose

virtualExtraTime = 10; % [s] extra time to ad before
initialTime = 0.6; % [s] assumed initial time of the trajectory
finalTime = 2.15; % [s] time of the trajectory
timeStepSize = 0.0004; % [s] wanted sampling time for the control

gear1 = 100;
gear2 = 100;
gear3 = 160;

% Load model

%% model with rigid initial guess and perf joints (EllaModel6) and joint
% trajectory
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkRigidJoints_wRigiModel\D6')

%% model with rigid initial guess and perf joints (EllaModel7) and cartesian
% trajectory
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkRigidJoints_wRigiModel\D7')

%% model with rigid initial guess and perf joints (EllaModel7) and cartesian
% trajectory
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkRigidJoints_wRigiModel\SBis7')

%% model with rigid initial guess and perf joints (EllaModel9) and joint trajectory
% for one joint only
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\D9')

%% model with rigid initial guess and flexible joints (EllaModel10) and joint trajectory
% for one joint only
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\D10')

%% model with flexible (static) initial guess and perf joints (EllaModel4)
% and cartesian trajectory
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkViscJoints\S')

%% model with flexible (static) initial guess and perf joints (EllaModel5)
% and joint trajectory
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkViscJoints\SBis2')

%% model with flexible (static) initial guess and flexible joints (EllaModel8)
% and joint trajectory
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFlexJoints\S8')

%% model with flexible (static) initial guess and flexible joints (EllaModel3)
% and cartesian trajectory
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFlexJoints\S3')

%% model with rigid initial guess and visc joints (EllaModel7) and cartesian
% trajectory
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkRigidJoints_wRigiModel\SBis7')

%% model with rigid initial guess and visc joints (EllaModel7) and cartesian
% trajectory (3 sec simulation).
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkRigidJoints_wRigiModel\SBis71s')

%% model with rigid initial guess and visc joints (EllaModel7) and cartesian
% trajectory (3 sec simulation with increased coulomb friction).
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkRigidJoints_wRigiModel\SBis73sCoulCutBis')

%% model with rigid initial guess and visc joints (EllaModel7) and cartesian
% trajectory (1 sec simulation with increased coulomb friction).
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkRigidJoints_wRigiModel\SBis71sCoulCut')

%% model with rigid initial guess and friction joints (EllaModel11) and cartesian
% trajectory
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\D11')

%% model with rigid initial guess and friction joints (EllaModel11) and cartesian
% trajectory
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\S111sCoulCut')

%% model with rigid initial guess and friction joints (EllaModel11) and cartesian
% trajectory for actual trajectory tested on the Ella robot 1.4s
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\S111sCoulCutActual')

%% model with rigid initial guess and friction joints (EllaModel11) and cartesian
% trajectory for actual trajectory tested on the Ella robot 1.05s
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\S111sCoulCutActual105')

%% model with rigid initial guess and friction joints (EllaModel11) and cartesian
% trajectory
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\D12')

%% model with rigid initial guess and friction joints (EllaModel11) and cartesian
% trajectory for actual trajectory tested on the Ella robot 1.4s
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\S121sCoulCutActual')

%% model with rigid initial guess and friction joints (EllaModel11) and cartesian
% trajectory for actual trajectory tested on the Ella robot
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\S121sCoulCutActual105')

%% model with rigid initial guess and friction joints (EllaModel14) and cartesian
% trajectory for actual trajectory tested on the Ella robot (flexible
% joint, as for lumped mass model)
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\S14')
% load('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\D14')

%% model with rigid initial guess and friction joints (EllaModel16) and cartesian
% Latest model as of May 28th 2018 validated and testing for other
% trajectories
duration = '115';
% hyp = 'D'; % see if you want rigid (D) or Flexible (S)
hyp = 'S'; % see if you want rigid (D) or Flexible (S)
wantSave = 0; % want to save result (1) or just generate input (0)?
if exist('duration') && exist('hyp')
    load(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\',hyp,'16sCutActual_',duration])
end

%%
if hyp =='D'
    simTime = D.parameters.time;
    S = D;
elseif hyp =='S'
    simTime = S.timeValues;
end
% simTime = D.parameters.time;
% S = D;
% simTime = S.timeValues;

jointLoc = [1 2 3];
% jointLoc = [1 3 5];

simEndPose = S.model.listNodes{end}.position;
j = 1;

% for i = [1 2 3]
for i = jointLoc
    simJointPose(j,:) = S.model.listElementVariables{i}.relCoo;
%     simJointPose(j,:) = S.model.listElementVariables{i}.relCoo(1)*ones(length(S.model.listElementVariables{i}.relCoo),1); % To have constant fixed initial position
    simJointVel(j,:) = S.model.listElementVariables{i}.velocity;
    simJointAcc(j,:) = S.model.listElementVariables{i}.acceleration;
    j = j+1;
end
% simJointPose(1,:) = S.model.listElementVariables{1}.relCoo;
% simJointPose(2,:) = S.model.listElementVariables{3}.relCoo;
% simJointPose(3,:) = S.model.listElementVariables{5}.relCoo;
% simJointVel(1,:) = S.model.listElementVariables{1}.velocity;
% simJointVel(2,:) = S.model.listElementVariables{3}.velocity;
% simJointVel(3,:) = S.model.listElementVariables{5}.velocity;

i = 1;
for nElemVar = S.model.listElementVariables
    if strcmp(nElemVar{1}.DofType,'Command')
        simJointTorque(i:i+nElemVar{1}.nL-1,:) = nElemVar{1}.value;
        i = i+nElemVar{1}.nL;
    end
end

% Extrapolate inputs based on loaded data
Time = initialTime:timeStepSize:finalTime;
for i = 1:size(simEndPose,1)
%     EndPose(i,:) = interp1(simTime,simEndPose(i,:),Time,'pchip');
    EndPose(i,:) = interp1(simTime,simEndPose(i,:),Time,'spline');
%     EndPose(i,:) = interp1(simTime,simEndPose(i,:),Time,'linear');
end
for i = 1:size(simJointPose,1)
%     JointPose(i,:) = interp1(simTime,simJointPose(i,:),Time,'pchip');
%     JointVel(i,:) = interp1(simTime,simJointVel(i,:),Time,'pchip');
%     JointTorque(i,:) = interp1(simTime,simJointTorque(i,:),Time,'pchip');
    JointPose(i,:) = interp1(simTime,simJointPose(i,:),Time,'spline');
    JointVel(i,:) = interp1(simTime,simJointVel(i,:),Time,'spline');
    JointAcc(i,:) = interp1(simTime,simJointAcc(i,:),Time,'spline');
    JointTorque(i,:) = interp1(simTime,simJointTorque(i,:),Time,'spline');
%     JointPose(i,:) = interp1(simTime,simJointPose(i,:),Time,'linear');
%     JointVel(i,:) = interp1(simTime,simJointVel(i,:),Time,'linear');
%     JointTorque(i,:) = interp1(simTime,simJointTorque(i,:),Time,'linear');
end
J_motor1 = (0.33+0.011+1.69)*10^-4;
J_motor2 = (0.33+0.011+1.69)*10^-4;
J_motor3 = (0.33+0.011+0.193)*10^-4;
% Adapt torque values according to gear ratios
if size(JointTorque,1) == 3;
%     JointTorque(1,:) = JointTorque(1,:)/gear1;
%     JointTorque(2,:) = -JointTorque(2,:)/gear2;
%     JointTorque(3,:) = -JointTorque(3,:)/gear3;
    
    JointTorque(1,:) = JointTorque(1,:)/gear1 + JointAcc(1,:)*J_motor1*gear1;
    JointTorque(2,:) = -JointTorque(2,:)/gear2 - JointAcc(2,:)*J_motor2*gear2;
    JointTorque(3,:) = -JointTorque(3,:)/gear3 - JointAcc(3,:)*J_motor3*gear3;
    
    % Adapting relative joint position to absolute joint position
    initJ0 = 0.4918; % rad, angle at the base (0.4918)
%     initJ0 = 0; % rad, angle at the base (0.4918)
    initJ1 = 0.1840; % angle of the first joint in rad (0.1840)
    initJ2 = -0.9926; % angle of the second joint in rad (-0.9926)
    JointPose(1,:) = JointPose(1,:)+initJ0;
    JointPose(2,:) = -JointPose(2,:)+initJ1;
    JointPose(3,:) = -JointPose(3,:)+initJ2;
end
Time = Time - initialTime;
% Create structure to send data
% save('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\TimeR1sCoulCutFast','Time')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\Time1sCoulCutActual','Time')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\TimeR1sCoulCutActualMend','Time')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\TimeR1sCoulCutActualMend105','Time')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\TimeR1sCoulCutActualLumped','Time')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\TimeR1sCoulCutActualLumped105','Time')
if hyp == 'D' && exist('duration') && wantSave
    save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\TimeRCutActual_',duration],'Time')
elseif hyp == 'S' && exist('duration') && wantSave
    save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\TimeCutActual_',duration],'Time')
end

% torque = timeseries(JointTorque',Time);
ExtraTime = 0:timeStepSize:virtualExtraTime;
torque = timeseries([ones(length(ExtraTime),1)*JointTorque(:,1)';JointTorque'],[ExtraTime Time+virtualExtraTime]);

figure
plot(Time,JointTorque(1,:),Time,JointTorque(2,:),Time,JointTorque(3,:))
legend('1','2','3')
title('Joint Torque')
grid on
% save('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointTorqueR1sCoulCutFast','JointTorque')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorque1sCoulCutActual','JointTorque')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorqueR1sCoulCutActualMend','JointTorque')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorqueR1sCoulCutActualMend105','JointTorque')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\JointTorqueR1sCoulCutActualLumped','JointTorque')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\JointTorqueR1sCoulCutActualLumped105','JointTorque')
if hyp == 'D' && exist('duration') && wantSave
    save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointTorqueRCutActual_',duration],'JointTorque')
elseif hyp == 'S' && exist('duration') && wantSave
    save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointTorqueCutActual_',duration],'JointTorque')
end


% jointPose = timeseries(JointPose',Time);
ExtraTime = 0:timeStepSize:virtualExtraTime;
jointPose = timeseries([ones(length(ExtraTime),1)*JointPose(:,1)';JointPose'],[ExtraTime Time+virtualExtraTime]);

figure
plot(Time,JointPose(1,:),Time,JointPose(2,:),Time,JointPose(3,:))
legend('1','2','3')
title('Joint Pose')
grid on
% save('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointPoseR1sCoulCutFast','JointPose')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPose1sCoulCutActual','JointPose')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPoseR1sCoulCutActualMend','JointPose')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPoseR1sCoulCutActualMend105','JointPose')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\JointPoseR1sCoulCutActualLumped','JointPose')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\JointPoseR1sCoulCutActualLumped105','JointPose')
if hyp == 'D' && exist('duration') && wantSave
    save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointPoseRCutActual_',duration],'JointPose')
elseif hyp == 'S' && exist('duration') && wantSave
    save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointPoseCutActual_',duration],'JointPose')
end


endPose = timeseries(EndPose',Time);
figure
plot(Time,EndPose(1,:),Time,EndPose(2,:),Time,EndPose(3,:))
legend('x','y','z')
grid on
% save('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\EndPoseR1sCoulCutFast','EndPose')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPose1sCoulCutActual','EndPose')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPoseR1sCoulCutActualMend','EndPose')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPoseR1sCoulCutActualMend105','EndPose')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\EndPoseR1sCoulCutActualLumped','EndPose')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\EndPoseR1sCoulCutActualLumped105','EndPose')
if hyp == 'D' && exist('duration') && wantSave
    save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\EndPoseRCutActual_',duration],'EndPose')
elseif hyp == 'S' && exist('duration') && wantSave
    save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\EndPoseCutActual_',duration],'EndPose')
end


jointVel = timeseries(JointVel',Time);
% figure
% plot(Time,JointVel(1,:),Time,JointVel(2,:),Time,JointVel(3,:))
% legend('1','2','3')
% title('Joint Velocity')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLARigiLinkRigidJointsTest\JointVelR1sCoulCutFast','JointVel')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVel1sCoulCutActual','JointVel')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVelR1sCoulCutActualMend','JointVel')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVelR1sCoulCutActualMend105','JointVel')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\JointVelR1sCoulCutActualLumped','JointVel')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLARigidLinkFlexJoints_wRgiModel\JointVelR1sCoulCutActualLumped105','JointVel')
if hyp == 'D' && exist('duration') && wantSave
    save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointVelRCutActual_',duration],'JointVel')
elseif hyp == 'S' && exist('duration') && wantSave
    save(['C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\JointVelCutActual_',duration],'JointVel')
end


% plot derivatives of the JointPositions
h = Time(2)-Time(1);
figure
plot(Time,deriveVal(JointPose,h))
legend('1','2','3')
grid on

figure
plot(Time,deriveVal(deriveVal(JointPose,h),h))
legend('1','2','3')
grid on

% figure
% plot(Time,deriveVal(deriveVal(deriveVal(JointPose,h),h),h))
% 
% figure
% plot(Time,deriveVal(deriveVal(deriveVal(deriveVal(JointPose,h),h),h),h))
