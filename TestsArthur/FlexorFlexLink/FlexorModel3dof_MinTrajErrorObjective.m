%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Flexible model of Flexor robot from TUHH (2 linear joint that are perpendicular and 1 rotary joint).
% 3DOF Version Links modeled as flexible beam elements: 
% Z-axis perpendicular to motion plane X-axis along second linear joint and
% Y- axis along first linear joint

clear all
saveResults = 0;
% Time parameters
dur = '9'; % duration in tenth of seconds
preAct = 0.1; % duration of pre actuation phase
postAct = 0.1; % duration of post actuation phase
% initial_time = 0.1; % Time to cut out at the beginning of the simulation
timestepsize = 0.01;
finaltime = str2double(dur)/10 + preAct + postAct; % sec
t_i = preAct; % time at end of pre-actuation
t_f = finaltime - postAct; % time at begining of post-actuation
npts = round(finaltime/timestepsize + 1); % number of points for optimization

% Gravity parameter
grav = [0 0 0];

% Numerical parameters
rho_num = 0;
hypForComp = 'MinTrajError';
% hypForComp = 'notConstant';
listM = [1 0 0];
listKt = [1 0 0];
listCt = [1 0 0];
listPhiq = [1 0 0];
% listM = [1 1 1];
% listKt = [1 1 1];
% listCt = [1 1 1];
% listPhiq = [1 1 1];
type_of_traj = 'Line'; % line trajectory
step_size = num2str(1000*timestepsize);
listMR = [1 1 1];
listKtR = [1 1 1];
listCtR = [1 1 1];
listPhiqR = [1 1 1];

% General beam damping
alpha = 0.0001;  % Damping coeff proportional to mass of the beams
beta = 0.02; % Damping coeff proportional to stiffness of the beams

% It is assumed that 0 of y axis (joint 1), on axis X,
% the unknonwn are the positions q1 and q2 of joint 1 and 2 
q1 = 0; % [m] position of the linear joint 1
q2 = 0+0.61; % [m] position of the linear joint 2 + offset
lab = sqrt(q1^2+q2^2);
l1 = 0.4; % [m] length of link 1
l21 = 2/3; % [m] length of first part of link2
l22 = 1/3; % [m] length of second part link2
l3 = 0.4; % [m] length of link 3
ang_b0 = asin(-q1/lab);
ang_b1 = acos((lab^2+l21^2-l1^2)/(2*lab*l21));
ang_a1 = acos((lab^2+l1^2-l21^2)/(2*lab*l1));

%% Definition of links and joints
% Link 1
% link1.angle = -acos((l1^2+q2^2-l21^2)/(2*l1*q2)); % [rad] angle between the link and the x axis (CCW)
link1.angle = -(ang_a1-ang_b0); % [rad] angle between the link and the x axis (CCW)
link1.l = l1; % [m] length of first link
link1.e = 0.002; % [m] thickness of beam
link1.h = 0.08; % [m] height of beam
link1.rho = 7900; % [kg/m�] density of beam
link1.E = 181.4e9; % [N/m�]
link1.nu = 0.3; % poisson coef
link1.G = link1.E/(2*(1+link1.nu)); % compression modulus
link1.A = link1.e*link1.h; % [m�] cross section area
link1.m = link1.rho * link1.A * link1.l; % [kg] Mass of first link
link1.J = (link1.m/12)*diag([link1.e^2+link1.h^2 link1.l^2+link1.h^2 link1.e^2+link1.l^2]); % [kg*m�] rigid body inertia
link1.Ixx = (link1.e^2 + link1.h^2)*link1.e*link1.h/12; % [m^4] area moment inertia in case of beam torsion
link1.Iyy = (link1.e*link1.h^3)/12; % [m^4] area moment inertia in case of beam (out of plane lexion)
link1.Izz = (link1.e^3*link1.h)/12; % [m^4] area moment inertia in case of beam (in-plane flexion)
link1.KCS = diag([link1.E*link1.A link1.G*link1.A link1.G*link1.A link1.G*link1.Ixx link1.E*link1.Iyy link1.E*link1.Izz]);
link1.MCS = diag(link1.rho*[link1.A link1.A link1.A link1.Ixx link1.Iyy link1.Izz]);
link1.nodes = [0 q1 0;...
               link1.l*cos(link1.angle) q1+link1.l*sin(link1.angle) 0];
link1.xAxis = (link1.nodes(end,:)-link1.nodes(1,:))/norm(link1.nodes(end,:)-link1.nodes(1,:));
link1.yAxis = [-link1.xAxis(2) link1.xAxis(1) 0];
link1.rotA = [link1.xAxis; link1.yAxis; cross(link1.xAxis,link1.yAxis)]; % Rotation of the inertia with respect to inertial axis 
link1.nElem = 2; % Number of elements composing the link
% adding necessary nodes
link1.nodes = CreateInterCoord(link1.nodes,link1.nElem,1,2);

% Link 21
% link21.angle = acos((l21^2+q2^2-l1^2)/(2*l21*q2)); % [rad] angle between the link and the x axis (CCW)
link21.angle = ang_b1+ang_b0; % [rad] angle between the link and the x axis (CCW)
link21.l = l21; % [m] length of link
link21.e = 0.002; % [m] thickness of beam
link21.h = 0.08; % [m] height of beam
link21.rho = 7900; % [kg/m�] density of beam
link21.E = 181.4e9; % [N/m�]
link21.nu = 0.3; % poisson coef
link21.G = link21.E/(2*(1+link21.nu)); % compression modulus
link21.A = link21.e*link21.h; % [m�] cross section area
link21.m = link21.rho * link21.A * link21.l; % [kg] Mass of second link
link21.J = (link21.m/12)*diag([link21.e^2+link21.h^2 link21.l^2+link21.h^2 link21.e^2+link21.l^2]); % [kg*m�] rigid body inertia
link21.Ixx = (link21.e^2 + link21.h^2)*link21.e*link21.h/12; % [m^4] area moment inertia in case of beam torsion
link21.Iyy = (link21.e*link21.h^3)/12; % [m^4] area moment inertia in case of beam (out of plane lexion)
link21.Izz = (link21.e^3*link21.h)/12; % [m^4] area moment inertia in case of beam (in-plane flexion)
link21.KCS = diag([link21.E*link21.A link21.G*link21.A link21.G*link21.A link21.G*link21.Ixx link21.E*link21.Iyy link21.E*link21.Izz]);
link21.MCS = diag(link21.rho*[link21.A link21.A link21.A link21.Ixx link21.Iyy link21.Izz]);
link21.nodes = [link1.nodes(end,:);...
               link1.nodes(end,1)+link21.l*cos(link21.angle) link1.nodes(end,2)+link21.l*sin(link21.angle) 0];
link21.xAxis = (link21.nodes(end,:)-link21.nodes(1,:))/norm(link21.nodes(end,:)-link21.nodes(1,:));
link21.yAxis = [-link21.xAxis(2) link21.xAxis(1) 0];
link21.rotA = [link21.xAxis; link21.yAxis; cross(link21.xAxis,link21.yAxis)]; % Rotation of the inertia with respect to inertial axis 
link21.nElem = 4; % Number of elements composing the link
% adding necessary nodes
link21.nodes = CreateInterCoord(link21.nodes,link21.nElem,1,2);

% Link 22
link22.angle = link21.angle; % [rad] angle between the link and the x axis (CCW)
link22.l = l22; % [m] length of link
link22.e = 0.006; % [m] thickness of beam
link22.h = 0.08; % [m] height of beam
link22.rho = 7900; % [kg/m�] density of beam
link22.E = 200e9; % [N/m�]
link22.nu = 0.3; % poisson coef
link22.G = link22.E/(2*(1+link22.nu)); % compression modulus
link22.A = link22.e*link22.h; % [m�] cross section area
link22.m = link22.rho * link22.A * link22.l; % [kg] Mass of second link
link22.J = (link22.m/12)*diag([link22.e^2+link22.h^2 link22.l^2+link22.h^2 link22.e^2+link22.l^2]); % [kg*m�] rigid body inertia
link22.Ixx = (link22.e^2 + link22.h^2)*link22.e*link22.h/12; % [m^4] area moment inertia in case of beam torsion
link22.Iyy = (link22.e*link22.h^3)/12; % [m^4] area moment inertia in case of beam (out of plane lexion)
link22.Izz = (link22.e^3*link22.h)/12; % [m^4] area moment inertia in case of beam (in-plane flexion)
link22.KCS = diag([link22.E*link22.A link22.G*link22.A link22.G*link22.A link22.G*link22.Ixx link22.E*link22.Iyy link22.E*link22.Izz]);
link22.MCS = diag(link22.rho*[link22.A link22.A link22.A link22.Ixx link22.Iyy link22.Izz]);
link22.nodes = [link21.nodes(end,:);...
               link21.nodes(end,1)+link22.l*cos(link22.angle) link21.nodes(end,2)+link22.l*sin(link22.angle) 0];
link22.xAxis = (link22.nodes(end,:)-link22.nodes(1,:))/norm(link22.nodes(end,:)-link22.nodes(1,:));
link22.yAxis = [-link22.xAxis(2) link22.xAxis(1) 0];
link22.rotA = [link22.xAxis; link22.yAxis; cross(link22.xAxis,link22.yAxis)]; % Rotation of the inertia with respect to inertial axis 
link22.nElem = 2; % Number of elements composing the link
% adding necessary nodes
link22.nodes = CreateInterCoord(link22.nodes,link22.nElem,1,2);

% Link 3
link3.angle = 0;
link3.l = l3; % [m] length of first link
link3.e = 0.001; % [m] thickness of beam
link3.h = 0.08; % [m] height of beam
link3.rho = 7900; % [kg/m�] density of beam
link3.E = 181.4e9; % [N/m�]
link3.nu = 0.3; % poisson coef
link3.G = link3.E/(2*(1+link3.nu)); % compression modulus
link3.A = link3.e*link3.h; % [m�] cross section area
link3.m = link3.rho * link3.A * link3.l; % [kg] Mass of first link
link3.J = (link3.m/12)*diag([link3.e^2+link3.h^2 link3.l^2+link3.h^2 link3.e^2+link3.l^2]); % [kg*m�] rgid body inertia
link3.Ixx = (link3.e^2 + link3.h^2)*link3.e*link3.h/12; % [m^4] area moment inertia in case of beam torsion
link3.Iyy = (link3.e*link3.h^3)/12; % [m^4] area moment inertia in case of beam (out of plane lexion)
link3.Izz = (link3.e^3*link3.h)/12; % [m^4] area moment inertia in case of beam (in-plane flexion)
link3.KCS = diag([link3.E*link3.A link3.G*link3.A link3.G*link3.A link3.G*link3.Ixx link3.E*link3.Iyy link3.E*link3.Izz]);
link3.MCS = diag(link3.rho*[link3.A link3.A link3.A link3.Ixx link3.Iyy link3.Izz]);
link3.nodes = [link22.nodes(end,:);...
               link22.nodes(end,1)+link3.l*cos(link3.angle) link22.nodes(end,2)+link3.l*sin(link3.angle) 0];
link3.xAxis = (link3.nodes(end,:)-link3.nodes(1,:))/norm(link3.nodes(end,:)-link3.nodes(1,:));
link3.yAxis = [-link3.xAxis(2) link3.xAxis(1) 0];
link3.rotA = [link3.xAxis; link3.yAxis; cross(link3.xAxis,link3.yAxis)]; % Rotation of the inertia with respect to inertial axis 
link3.nElem = 2; % Number of elements composing the link
% adding necessary nodes
link3.nodes = CreateInterCoord(link3.nodes,link3.nElem,1,2);

% Linear Joint 1
joint1.axis = [0 1 0 0 0 0]'; % Axis of joint
joint1.stiff = 0; % stiffness of joint
joint1.coul = 0; % Coulomb friction of joint
joint1.damp = 0; % Viscous friction of joint

% Linear Joint 2
joint2.axis = [1 0 0 0 0 0]'; % Axis of joint
joint2.stiff = 0; % stiffness of joint
joint2.coul = 0; % Coulomb friction of joint
joint2.damp = 0; % Viscous friction of joint

% Hinge Joint 3
joint3.axis = [0 0 0 0 0 1]'; % Axis of joint
joint3.stiff = 0; % stiffness of joint
joint3.coul = 0; % Coulomb friction of joint
joint3.damp = 0; % Viscous friction of joint

% PASSIVE hinge Joint 4 (at first linear joint)
joint4.axis = [0 0 0 0 0 1]'; % Axis of joint
joint4.stiff = 0; % stiffness of joint
joint4.coul = 0; % Coulomb friction of joint
joint4.damp = 0; % Viscous friction of joint

% PASSIVE hinge Joint 5 (between first and second link)
joint5.axis = [0 0 0 0 0 1]'; % Axis of joint
joint5.stiff = 0; % stiffness of joint
joint5.coul = 0; % Coulomb friction of joint
joint5.damp = 0; % Viscous friction of joint

% PASSIVE hinge Joint 6 (at second linear joint)
joint6.axis = [0 0 0 0 0 1]'; % Axis of joint
joint6.stiff = 0; % stiffness of joint
joint6.coul = 0; % Coulomb friction of joint
joint6.damp = 0; % Viscous friction of joint

% Mass on linear joint 1 (first arm start)
m1 = 0.811;
J1 = link1.rotA*diag([588e-6 748e-6 490e-6])*link1.rotA';

% Mass on passive hinge  (first arm end)
mj1 = 1.031;
Jj1 = link1.rotA*diag([1175e-6 3055e-6 2250e-6])*link1.rotA';
% Mass on passive hinge  (second arm start)
mj2 = 0.811;
Jj2 = link21.rotA*diag([588e-6 748e-6 490e-6])*link21.rotA';

% Mass on linear joint 2 (second arm middle)
m2 = 1.275;
J2 = link22.rotA*diag([915e-6 1828e-6 1405e-6])*link22.rotA';

% Mass on hinge joint 3 (second arm end)
m31 = 1.87;
J31 = link22.rotA*[1548e-6 0 -334e-6; 0 3366e-6 0; -334e-6 0 3294e-6]*link22.rotA';

% Mass on hinge joint 3 (third arm start)
m32 = 0.82;
J32 = link3.rotA*[988e-6 0 420e-6; 0 1430e-6 0; 420e-6 0 903e-6]*link3.rotA';

% Mass on end-effector (third arm end)
m_end = 0.416; % [kg] end-effector mass
J_end = link3.rotA*diag([313e-6 395e-6 231e-6])*link3.rotA';


%% Construction of the rigid robot model
% Nodes
nodes = [link1.nodes(1,:);...
         link1.nodes(1,:);...
         link1.nodes;...    
         link21.nodes;...
         link22.nodes(2:end,:);...
         link3.nodes;...
         link22.nodes(1,:)]; % List of nodes to build the model
     
% number all nodes properly so they can be used by FEMmodel
nodes = [1:size(nodes,1);nodes']';     
     
% Elements
count = 1;
Start1 = 3; % first node of the 1st link
% creating first link (rotating aroud z)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+link1.nElem/2 Start1+[0:link1.nElem/2-1] Start1+link1.nElem/2+1+[0:link1.nElem/2-1]];
elements{count}.mass = link1.m;
elements{count}.J = link1.rotA*link1.J*link1.rotA';
elements{count}.g = grav;
elements{count}.listM = listMR;
elements{count}.listCt = listCtR;
elements{count}.listKt = listKtR;
elements{count}.listPhiq = listPhiqR;

% Second link 21
count = size(elements,2)+1;
Start21 = Start1+link1.nElem+1; % first node of the 21 link
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start21+link21.nElem/2 Start21+[0:link21.nElem/2-1] Start21+link21.nElem/2+1+[0:link21.nElem/2-1]];
elements{count}.mass = link21.m;
elements{count}.J = link21.rotA*link21.J*link21.rotA';
elements{count}.g = grav;
elements{count}.listM = listMR;
elements{count}.listCt = listCtR;
elements{count}.listKt = listKtR;
elements{count}.listPhiq = listPhiqR;

% Second link 22
count = size(elements,2)+1;
Start22 = Start21+link21.nElem; % first node of the 22 link
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start22+link22.nElem/2 Start22+[0:link22.nElem/2-1] Start22+link22.nElem/2+1+[0:link22.nElem/2-1]];
elements{count}.mass = link22.m;
elements{count}.J = link22.rotA*link22.J*link22.rotA';
elements{count}.g = grav;
elements{count}.listM = listMR;
elements{count}.listCt = listCtR;
elements{count}.listKt = listKtR;
elements{count}.listPhiq = listPhiqR;

% Third link (on hinge joint only)
count = size(elements,2)+1;
Start3 = Start22+link22.nElem+1; % first node of the 3rd link
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start3+link3.nElem/2 Start3+[0:link3.nElem/2-1] Start3+link3.nElem/2+1+[0:link3.nElem/2-1]];
elements{count}.mass = link3.m;
elements{count}.J = link3.rotA*link3.J*link3.rotA';
elements{count}.g = grav;
elements{count}.listM = listMR;
elements{count}.listCt = listCtR;
elements{count}.listKt = listKtR;
elements{count}.listPhiq = listPhiqR;

% creating rigid body 1st linear joint (along y)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1-1];
elements{count}.mass = m1;
elements{count}.J = J1;
elements{count}.g = grav;
elements{count}.listM = listMR;
elements{count}.listCt = listCtR;
elements{count}.listKt = listKtR;
elements{count}.listPhiq = listPhiqR;

% creating rigid body passive hinge end of link 1
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start21-1];
elements{count}.mass = mj1;
elements{count}.J = Jj1;
elements{count}.g = grav;
elements{count}.listM = listMR;
elements{count}.listCt = listCtR;
elements{count}.listKt = listKtR;
elements{count}.listPhiq = listPhiqR;

% creating rigid body passive hinge start of link 2
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start21];
elements{count}.mass = mj2;
elements{count}.J = Jj2;
elements{count}.g = grav;
elements{count}.listM = listMR;
elements{count}.listCt = listCtR;
elements{count}.listKt = listKtR;
elements{count}.listPhiq = listPhiqR;

% creating rigid body 2nd linear joint (along x)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start22];
elements{count}.mass = m2;
elements{count}.J = J2;
elements{count}.g = grav;
elements{count}.listM = listMR;
elements{count}.listCt = listCtR;
elements{count}.listKt = listKtR;
elements{count}.listPhiq = listPhiqR;

% creating rigid body 3rd rotating joint (around z)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start3-1];
elements{count}.mass = m31;
elements{count}.J = J31;
elements{count}.g = grav;
elements{count}.listM = listMR;
elements{count}.listCt = listCtR;
elements{count}.listKt = listKtR;
elements{count}.listPhiq = listPhiqR;

% creating rigid body 3rd rotating joint (around z)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start3];
elements{count}.mass = m32;
elements{count}.J = J32;
elements{count}.g = grav;
elements{count}.listM = listMR;
elements{count}.listCt = listCtR;
elements{count}.listKt = listKtR;
elements{count}.listPhiq = listPhiqR;

% creating rigid body end-effector
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end-1,1)];
elements{count}.mass = m_end;
elements{count}.J = J_end;
elements{count}.g = grav;
elements{count}.listM = listMR;
elements{count}.listCt = listCtR;
elements{count}.listKt = listKtR;
elements{count}.listPhiq = listPhiqR;

% PASSIVE hinge joint 4 (on 1st linear joint)
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start1-1 Start1];
elements{count}.k = joint4.stiff;
elements{count}.d = joint4.damp;
elements{count}.coulomb = joint4.coul;
elements{count}.A = joint4.axis;

% PASSIVE hinge joint 5 (between link 1 and link 21)
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start21-1 Start21];
elements{count}.k = joint5.stiff;
elements{count}.d = joint5.damp;
elements{count}.coulomb = joint5.coul;
elements{count}.A = joint5.axis;

% PASSIVE hinge joint 6 (on second linear joint)
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start21+link21.nElem/2+1 nodes(end,1)];
elements{count}.k = joint6.stiff;
elements{count}.d = joint6.damp;
elements{count}.coulomb = joint6.coul;
elements{count}.A = joint6.axis;

% ACTUATED first linear joint
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.k = joint1.stiff;
elements{count}.d = joint1.damp;
elements{count}.coulomb = joint1.coul;
elements{count}.A = joint1.axis;

% ACTUATED second linear joint
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 nodes(end,1)];
elements{count}.k = joint2.stiff;
elements{count}.d = joint2.damp;
elements{count}.coulomb = joint2.coul;
elements{count}.A = joint2.axis;

% ACTUATED third hinge joint
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start3-1 Start3];
elements{count}.k = joint3.stiff;
elements{count}.d = joint3.damp;
elements{count}.coulomb = joint3.coul;
elements{count}.A = joint3.axis;

% Trajectory
x_end = nodes(end-1,2)+0.3;
y_end = nodes(end-1,3)-0.3;
ang_end = 0;

timeVector = 0:timestepsize:finaltime;
trajx = lineTraj(nodes(end-1,2),x_end,timeVector,t_i,t_f);
trajy = lineTraj(nodes(end-1,3),y_end,timeVector,t_i,t_f);
trajAng = lineTraj(0,ang_end,timeVector,t_i,t_f);

count = size(elements,2)+1;
elements{count}.type = 'TrajectoryConstraint4';
elements{count}.nodes = [nodes(end-1,1)];
elements{count}.T = [trajx;...
                     trajy;...
                     trajAng];
elements{count}.Axe = [1 0 0 0 0 0;...
                       0 1 0 0 0 0;...
                       0 0 0 0 0 1];
elements{count}.elements = [count-3 count-2 count-1];
% elements{count}.active = 1;
% plot(timeVector,[trajx;trajy;trajz])

% Boundary Condition
BC = [1];

%% Solving rigid model
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();
if saveResults
    save(['C:\ObjectCodeMatlab\TestsArthur\FlexorFlexLink\D_3dof_',dur,'_h',step_size,'.mat'],'D')
end
%% Construction of the flexible robot model
clear elements

% Elements
% creating first link (rotating aroud z)
% count = 0;
% Start1 = 3; % first node of the 1st link
% for i = 1:link1.nElem
%     elements{count+i}.type = 'FlexibleBeamElement';
%     elements{count+i}.nodes = [Start1-1+i Start1+i];
%     elements{count+i}.KCS = link1.KCS;
%     elements{count+i}.MCS = link1.MCS;
%     elements{count+i}.yAxis = link1.yAxis;
%     elements{count+i}.alpha = alpha;
%     elements{count+i}.beta = beta;
%     elements{count+i}.listM = listM;
%     elements{count+i}.listCt = listCt;
%     elements{count+i}.listKt = listKt;
%     elements{count+i}.listPhiq = listPhiq;
%     elements{count+i}.g = grav;
% end
count = 1;
Start1 = 3; % first node of the 1st link
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+link1.nElem/2 Start1+[0:link1.nElem/2-1] Start1+link1.nElem/2+1+[0:link1.nElem/2-1]];
elements{count}.mass = link1.m;
elements{count}.J = link1.rotA*link1.J*link1.rotA';
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% Second link 21
count = size(elements,2);
Start21 = Start1+link1.nElem+1; % first node of link 21
for i = 1:link21.nElem
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start21-1+i Start21+i];
    elements{count+i}.KCS = link21.KCS;
    elements{count+i}.MCS = link21.MCS;
    elements{count+i}.yAxis = link21.yAxis;
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.listPhiq = listPhiq;
    elements{count+i}.g = grav;
end

% Second link 22
count = size(elements,2);
Start22 = Start21+link21.nElem; % first node of link 22
for i = 1:link22.nElem
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start22-1+i Start22+i];
    elements{count+i}.KCS = link22.KCS;
    elements{count+i}.MCS = link22.MCS;
    elements{count+i}.yAxis = link22.yAxis;
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.listPhiq = listPhiq;
    elements{count+i}.g = grav;
end

% Third link (on hinge joint only)
count = size(elements,2);
Start3 = Start22+link22.nElem+1; % first node of the 3rd link
for i = 1:link3.nElem
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start3-1+i Start3+i];
    elements{count+i}.KCS = link3.KCS;
    elements{count+i}.MCS = link3.MCS;
    elements{count+i}.yAxis = link3.yAxis;
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.listPhiq = listPhiq;
    elements{count+i}.g = grav;
end

% creating rigid body 1st linear joint (along y)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1-1];
elements{count}.mass = m1;
elements{count}.J = J1;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body passive hinge end of link 1
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start21-1];
elements{count}.mass = mj1;
elements{count}.J = Jj1;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body passive hinge start of link 2
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start21];
elements{count}.mass = mj2;
elements{count}.J = Jj2;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body 2nd linear joint (along x)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start22];
elements{count}.mass = m2;
elements{count}.J = J2;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body 3rd rotating joint (around z)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start3-1];
elements{count}.mass = m31;
elements{count}.J = J31;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body 3rd rotating joint (around z)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start3];
elements{count}.mass = m32;
elements{count}.J = J32;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body end-effector
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end-1,1)];
elements{count}.mass = m_end;
elements{count}.J = J_end;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% PASSIVE hinge joint 4 (on 1st linear joint)
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start1-1 Start1];
elements{count}.k = joint4.stiff;
elements{count}.d = joint4.damp;
elements{count}.coulomb = joint4.coul;
elements{count}.A = joint4.axis;

% PASSIVE hinge joint 5 (between link 1 and link 21)
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start21-1 Start21];
elements{count}.k = joint5.stiff;
elements{count}.d = joint5.damp;
elements{count}.coulomb = joint5.coul;
elements{count}.A = joint5.axis;

% PASSIVE hinge joint 6 (on second linear joint)
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start21+link21.nElem/2+1 nodes(end,1)];
elements{count}.k = joint6.stiff;
elements{count}.d = joint6.damp;
elements{count}.coulomb = joint6.coul;
elements{count}.A = joint6.axis;

% ACTUATED first linear joint
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.k = joint1.stiff;
elements{count}.d = joint1.damp;
elements{count}.coulomb = joint1.coul;
elements{count}.A = joint1.axis;

% ACTUATED second linear joint
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 nodes(end,1)];
elements{count}.k = joint2.stiff;
elements{count}.d = joint2.damp;
elements{count}.coulomb = joint2.coul;
elements{count}.A = joint2.axis;

% ACTUATED third hinge joint
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start3-1 Start3];
elements{count}.k = joint3.stiff;
elements{count}.d = joint3.damp;
elements{count}.coulomb = joint3.coul;
elements{count}.A = joint3.axis;

% count = size(elements,2)+1;
% elements{count}.type = 'TrajectoryConstraint4';
% elements{count}.nodes = [nodes(end-1,1)];
% elements{count}.T = [trajx;...
%                      trajy;...
%                      trajAng];
% elements{count}.Axe = [1 0 0 0 0 0;...
%                        0 1 0 0 0 0;...
%                        0 0 0 0 0 1];
% elements{count}.elements = [count-3 count-2 count-1];
% elements{count}.active = 1;

% Solving
ModelF = FEModel();
ModelF.CreateFEModel(nodes,elements);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from static solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
        ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

% Start second full optimization with newly computed initial guess
tic
S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = rho_num;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.parameters.scaling = 1e6;
S.ConstIter = hypForComp;
S.NodeToMinimize = nodes(end-1,1);
S.Traj.A = [1 0 0 0 0 0;...
            0 1 0 0 0 0;...
            0 0 0 0 0 1];
S.Traj.T = [trajx;...
            trajy;...
            trajAng];
% analys = S.runPoleAnalysisOpti(D)
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])
if saveResults
    save(['C:\ObjectCodeMatlab\TestsArthur\FlexorFlexLink\S_3dof_',dur,'_h',step_size,'.mat'],'S')
end
%% Plots
is_S = 0;
ua_forceCte = 80;
ub_forceCte = 80;
uc_forceCte = 10;
if exist('S')
    time = S.timeValues;
    a = S.model.listElementVariables{4}.relCoo;
    b = S.model.listElementVariables{5}.relCoo;
    c = S.model.listElementVariables{6}.relCoo;
    da = S.model.listElementVariables{4}.velocity;
    db = S.model.listElementVariables{5}.velocity;
    dc = S.model.listElementVariables{6}.velocity;
    d2a = S.model.listElementVariables{4}.acceleration;
    d2b = S.model.listElementVariables{5}.acceleration;
    d2c = S.model.listElementVariables{6}.acceleration;
    x = S.model.listNodes{end-1}.position(1,:);
    y = S.model.listNodes{end-1}.position(2,:);
    z = S.model.listNodes{end-1}.position(3,:);
    ua = S.model.listElementVariables{end}.value(1,:);
    ub = S.model.listElementVariables{end}.value(2,:);
    uc = S.model.listElementVariables{end}.value(3,:);
    is_S = 1;
end
is_D = 0;
if exist('D')
    timeR = D.parameters.time;
    aR = D.model.listElementVariables{4}.relCoo;
    bR = D.model.listElementVariables{5}.relCoo;
    cR = D.model.listElementVariables{6}.relCoo;
    daR = D.model.listElementVariables{4}.velocity;
    dbR = D.model.listElementVariables{5}.velocity;
    dcR = D.model.listElementVariables{6}.velocity;
    d2aR = D.model.listElementVariables{4}.acceleration;
    d2bR = D.model.listElementVariables{5}.acceleration;
    d2cR = D.model.listElementVariables{6}.acceleration;
    xR = D.model.listNodes{end-1}.position(1,:);
    yR = D.model.listNodes{end-1}.position(2,:);
    zR = D.model.listNodes{end-1}.position(3,:);
    uaR = D.model.listElementVariables{end}.value(1,:);
    ubR = D.model.listElementVariables{end}.value(2,:);
    ucR = D.model.listElementVariables{end}.value(3,:);
    is_D = 1;
end

if is_S
% Joint positions
leg = {};
figure
hold on
plot(time,[a;b;c],'Linewidth',2)
leg = [leg, 'a','b','c'];
if is_D
    set(gca,'ColorOrderIndex',1)
    plot(timeR,[aR;bR;cR],'--','Linewidth',2)
    leg = [leg, 'aR','bR','cR'];
end
title('Position of linear joints a (along y), b (along x) and c (around z)')
xlabel('Time [s]')
ylabel('Position [m or rad]')
legend(leg)
grid on

% Joint velocity
leg = {};
figure
hold on
plot(time,[da;db;dc],'Linewidth',2)
leg = [leg, 'da','db','dc'];
if is_D
    set(gca,'ColorOrderIndex',1)
    plot(timeR,[daR;dbR;dcR],'--','Linewidth',2)
    leg = [leg, 'daR','dbR','dcR'];
end
title('Velocity of linear joints a (along y), b (along x) and c (around z)')
xlabel('Time [s]')
ylabel('Velcity [m/s or rad/s]')
legend(leg)
grid on

% Joint accelerations
leg = {};
figure
hold on
plot(time,[d2a;d2b;d2c],'Linewidth',2)
leg = [leg, 'd2a','d2b','d2c'];
if is_D
    set(gca,'ColorOrderIndex',1)
    plot(timeR,[d2aR;d2bR;d2cR],'--','Linewidth',2)
    leg = [leg, 'd2aR','d2bR','d2cR'];
end
title('Acceleration of linear joints a (along y), b (along x) and c (around z)')
xlabel('Time [s]')
ylabel('Acceleration [m/s^2 or rad/s^2]')
legend(leg)
grid on

% Joint forces
leg = {};
figure
hold on
plot(time,[ua/ua_forceCte;ub/ub_forceCte;uc/uc_forceCte],'Linewidth',2)
leg = [leg, 'ua','ub','uc'];
if is_D
    set(gca,'ColorOrderIndex',1)
    plot(timeR,[uaR/ua_forceCte;ubR/ub_forceCte;ucR/uc_forceCte],'--','Linewidth',2)
    leg = [leg, 'uaR','ubR','ucR'];
end
title('Forces/torques on joints a (along y), b (along x) and c (around z)')
xlabel('Time [s]')
ylabel('Force [N or Nm]')
legend(leg)
grid on

% End-effector position
leg = {};
figure
hold on
plot(time,[x;y;z],'Linewidth',2)
leg = [leg, 'x','y','z'];
if is_D
    set(gca,'ColorOrderIndex',1)
    plot(timeR,[xR;yR;zR],'--','Linewidth',2)
    leg = [leg, 'xR','yR','zR'];
end
title('End-effector position')
xlabel('Time [s]')
ylabel('Position [m]')
legend(leg)
grid on

end

% analysis of the deformation measure inside the flexible beams
% Beam of interest
% beam = [2 3 4 5 6 7];
beam = [];
for i = S.model.listNumberElements
    if isa(S.model.listElements{i},'FlexibleBeamElement')
        beam = [beam i];
    end
end
epsilon = zeros(6,length(S.timeValues),length(beam)); % initialize deformation vectors
depsilon = zeros(6,length(S.timeValues),length(beam)); % initialize deformation rate vectors
for i = 1:length(beam)
    B = S.model.listElements{beam(i)};
    for t = 1:length(S.timeValues)
        xA = B.listNodes{1}.position(:,t);
        RA = dimR(B.listNodes{1}.R(:,t));
        HAm1 = [RA' -RA'*xA; 0 0 0 1];
        
        xB = B.listNodes{2}.position(:,t);
        RB = dimR(B.listNodes{2}.R(:,t));
        HB = [RB xB; 0 0 0 1];
        
        hBeam = logSE3(HAm1*HB);
        epsilon(:,t,i) = (hBeam-B.h0)/B.L;
    end
end
h = S.timeValues(2)-S.timeValues(1);
for i = 1:length(beam)
    depsilon(:,:,i) = deriveVal(epsilon(:,:,i),h);
end
compo = 6;
leg = {};
figure
hold on
title(['Phase plot of strain (compo ',num2str(compo),') in beams of the system'])
for i = 1:length(beam)
    plot(epsilon(compo,:,i),depsilon(compo,:,i))
    leg = [leg,num2str(i)];
end
xlabel('$\epsilon$','interpreter','latex','FontSize',18)
ylabel('$\dot{\epsilon}$','interpreter','latex','FontSize',18)
legend(leg)
grid on

% Plot deformation over time
leg = {};
figure
hold on
for i = 1:length(beam)
    plot(S.timeValues,epsilon(compo,:,i))
    leg = [leg,num2str(i)];
end
xlabel('Time [s]','interpreter','latex','FontSize',18)
ylabel('$\epsilon$','interpreter','latex','FontSize',18)
legend(leg)
grid on

% Plot deformation rate over time
leg = {};
figure
hold on
for i = 1:length(beam)
    plot(S.timeValues,depsilon(compo,:,i))
    leg = [leg,num2str(i)];
end
xlabel('Time [s]','interpreter','latex','FontSize',18)
ylabel('$\dot{\epsilon}$','interpreter','latex','FontSize',18)
legend(leg)
grid on
