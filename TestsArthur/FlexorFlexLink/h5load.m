%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Data, unit, sets] = h5load(file, varargin)
% 
%   [DATA, UNIT, SETS] = H5LOAD(FILE)
%   [DATA, UNIT, SETS] = H5LOAD(FILE, PARAM, VALUE,...)
%   [DATA, UNIT, SETS] = H5LOAD
% 
% This functions imports the DataSets of the HDF5-File FILE preserving the
% group nesting ofh the file and stores the data into the Matlab Struct 
% DATA while its properties are stored columnwise in the Cell UNIT. SETS 
% is a Struct with general information about the file. 
%
% If the function is called without any input argumennts, a file browser is
% opened prompting the user to select interactively. Afterwards a list
% dialog is shown, so a MODE can be selected (DEFAULT: '-struct').
%
% Inputs:
%   FILE            String. Vollst�ndiger Dateipfad der Messung or in PWD.       
%
% The following optional parameters can be set by PARAM/VALUE (case-sensitive):
%   'mode'          String: {'-struct'}|'-nodata'|'-chonly'
%       '-struct'   All Datasets will be imported to Structure-Variable
%                   The original nesting and group structure is preserved.
%       '-nodata'   No DataSets are imported. Only the properties and the
%                   variable list are generated. DATA consists of [].
%       '-chonly'   Only the DataSets are imported which have been
%                   identified to be measurement channels. This is the case
%                   when the DataSet is a vector and its size equals the
%                   determined number of samples of the measurement.
%   'verbose'       Scalar value. If set, any major steps executed in 
%                   function are displayed in Command Window.
%                   DEFAULT: 0.
%   'start':        Index of Elements from which Dataset is loaded.
%                   This applies only for channels of measurements
%                   DEFAULT: [1 1]. See H5read for details
%   'count':        Number of Elements in Each dimension to be imported.
%                   This applies only for channels of measurements
%                   DEFAULT: [Inf Inf]. See H5read for details.
%   'datatype':     Desired Datatype in Matlab for the channels.
%                       
% Outputs:
%   DATA        Matlab Struct with the Datasets of the HDF5-File.
%               To access a variable in DATA use third column of UNIT.
%               If MODE = '-chonly' only the channels of the measurements
%               is imported. For '-nodata' all fields of DATA equal [].
%
%   UNIT        Cell-Variable with the Properties of the Datasets in FILE.
%               These are mostly extracted directly via h5info. The
%               dimensions of units equal [<#DataSets> 9]. The columns have
%               the following meaning:
%               1)  Corresponding Variable Name of the Dataset
%               2)  Path of the DataSet withing the HDF5-File
%               3)  Path within DATA to access the data of the DataSet
%               4)  Variable Class of the Dataset
%               5)  Bytes per each Element of the DataSet
%               6)  Dimensions resp. Size of the DataSet
%               7)  Datatype of DataSet
%               8)  FillValue of the DataSet    (See H5INFO)
%               9)  Filters of the DataSet      (See H5INFO)
%              10)  Boolean specifying whether DataSets is a Channel
%
%   SETS        Matlab Struct containing general information about the
%               HDF-File like name, date, # of Datatypes, # of Channels,
%               # of Samples and so on.
%
% Unterfunktionen: (ausserhalb des m-Files)
%       See also H5INFO, H5READ, CHECKVARNAME .

% Change Log:
%   23/04/13    First version
%   12/11/13    Added variablility - Nesting of HDF5-File is preserved

%% 1  Initialisation of Parameters
Data=[]; unit={}; sets=[]; unitcols = 10; % # of columns of UNIT
opt.sprttype={'.h5';'.H5'}; % Supported Formats
opt.sprtclass = {'double','single'};
choices={'-struct';'-chonly';'-nodata'}; % Available MODI to call H5LOAD
choices_list={'Structure-Variable ''-struct'' (only for smal measurements)';...
              'Channels only ''-chonly'' (Only measurements are imported)';...
              ' No Data ''-nodata'' (Only List of variables is generated)'};

% DEFAULT-Werte f�r OPTIONS             
opt.mode='-struct';  % Dateien werden Struktur-Variable geladen
opt.type='double';   % Gr��e bzw. Genauigkeit der Daten
opt.start=[1 1];     % Startelement von Kanal, ab dem Daten geladen werden
opt.count=[Inf Inf]; % Anzahl der Elemente pro Dimension   
opt.verbose=0;       % Defaultm��ig wird der Fortschritt nicht im Window angezeigt

%% 2  Double checks of input parameters
if nargin; ninputs = nargin;
   calledwithnoArgs = 0;     % Wenn Funktionsaufruf durch Programmzeile
else
   calledwithnoArgs = 1;     % Wenn ohne Eingabeparameter aufgerufen wurde.
   % DTYPS = Filefilter f�r UIGETFILE aus DTYPS
   filefilt=''; dtyps=cell(1+numel(opt.sprttype),2); 
   for ii=1:numel(opt.sprttype); 
       filefilt=[filefilt,'*',opt.sprttype{ii},';'];  %#ok
       dtyps{ii+1,1}=['*',opt.sprttype{ii}];
       dtyps{ii+1,2}=[dtyps{ii+1,1},' File'];
   end
   dtyps(1,:)={filefilt(1:end-1),'All Measurements'};
   % Browser f�r die Dateiauswahl
   [nam,pfad]=uigetfile(dtyps,'Please select a HDF5-Flie','Multiselect','off');
   if ~isequal(pfad,0) && ~isequal(nam,0); file=fullfile(pfad,nam); 
   else disp('No file selected'); return 
   end 
   % LISTE f�r den Anwender zur Auswahl des Modus f�r den Datenimport
   [answ,ok]=listdlg('ListString',choices_list,'SelectionMode','single',...
                     'ListSize',[300 300],'InitialValue',3,'Name',...
                     'Modus f�r Datenimport','PromptString',...
                     'In welchem Format sollen die Daten importiert werden???',...
                     'OKString','Ausw�hlen','CancelString','Abbrechen');
   if ~ok; % Wenn "Abbrechen" ausgew�hlt wird
       disp('No Mode has been selected'); return; 
   else % Wenn "Ausw�hlen gedr�ckt wurde
       opt.mode=choices{answ}; 
   end 
   ninputs = 1; % Number of inputs is set to two 
end
if ninputs < 1; error('Not enough input arguments'); end
if ~exist('file','var') || ~ischar(file) || isempty(file) || ~exist(file,'file')
    disp('File is not a filename or the HDF5-File does not exist'); return;
end
[sets.drctry,name,ext]=fileparts(file); % Double check type of file
if ~ismember(ext,opt.sprttype); error('No supported file format!'); end
if nargin>3 && ~iscellstr(varargin(1:2:end)); error('Parameter fields must all be strings'); end
if nargin>3 && mod(numel(varargin),2); error('Wrong number of input arguments'); end
% Convert Parameter/Value Combination of input arguments to Structure
S=struct; for ii=1:2:numel(varargin); S.(lower(varargin{ii}))=varargin{ii+1}; end
if isfield(S,'mode') && ismember(lower(S.mode),choices);
    opt.mode=lower(varargin{1}); S = rmfield(S, 'mode');
end
if isfield(S,'verbose') && isscalar(S.verbose) && (islogical(S.verbose) || isnumeric(S.verbose))
    opt.verbose = logical(S.verbose); S = rmfield(S, 'verbose');
end
% All the other parameter value combinations will be considered w/o checking
flds = fieldnames(S); for ii = 1:numel(flds); opt.(flds{ii})=S.(flds{ii}); end

%% 3   List of Variable Names and Properties are imported
try S=h5info(file);  % Info �ber Kan�le werden importiert
catch ME1 % Fehlermeldung wird in Command Window ausgegegeben
    disp(['HDF5-File "',name,'"  unabled to be loaded. Invalid format']); 
    msgString=getReport(ME1,'extended','hyperlinks','on');
    disp(msgString); return;
end
%%% Wenn nicht alle Geforderten Variablen vorhanden -- Format �berpr�fung
req_flds={'Filename','Name','Groups','Datasets','Datatypes','Links','Attributes'};
if isstruct(S) && ~isequal(isfield(S,req_flds),ones(size(req_flds)))
    disp(['HDF5-File "',name,'" has a wrong format']); return;
end
%%% Extract number of Datastets and their complete paths 
flds=fieldnames(S); for ii=1:numel(flds); sets.(flds{ii})=S.(flds{ii}); end
Groups = rmfield(S,'Filename'); % Top Level is First Group
groupsep = Groups(1).Name; % Seperator inbetween Groups of Structure
count = 0; % Counter to loop all nested Groups
while count<numel(Groups) % Loop all existing Groups
    count = count + 1; % Counter is incremented
    if isempty(Groups(count).Groups); continue; end        
    before = numel(Groups); % Store current number of groups
    Groups = vertcat(Groups,Groups(count).Groups(:)); %#ok Columns
    Groups(before).Groups = []; % Remove them from that.
    % Paste nesting of Parents only if this is not toplevel
    if before == 1; continue; end    
    for jj = (before+1):(numel(Groups))
        Groups(jj).Name = strcat(Groups([before jj]).Name);
    end
end
%%% Count the total number of datasets
C = struct2cell(Groups); flds = fieldnames(Groups);
idx = find(ismember(flds,'Datasets'));
sets.nchannels = sum(cellfun(@numel,C(idx,:),'UniformOutput',true));
if ~sets.nchannels; % Wenn keine Daten enthalten sind.
    disp(['HDF5-File "',name,'" contains no data']); return;
end
% Extract properties of datasets  --> Cell-Variable UNIT
unit=cell(sets.nchannels,unitcols); unit(:,1:3)={''}; kk = 0;
for ii = 1:size(C,2) % Loop all Groups
    for jj = 1:numel(C{idx,ii}) % Loop all Datasets of each groups
        if isempty(C{idx,ii}(jj).Name); continue; end
        kk = kk+1; % Counter for the number of Datasets
        unit{kk,1} = checkvarname(C{idx,ii}(jj).Name); % Variable Name of Dataset
        if strcmp(C{ismember(flds,'Name'),ii}(end),groupsep)
            unit{kk,2} = [C{ismember(flds,'Name'),ii},unit{kk,1}];
        else
            unit{kk,2} = [C{ismember(flds,'Name'),ii},groupsep,unit{kk,1}]; % Path
        end
        unit{kk,3} = strrep(unit{kk,2},groupsep,'.'); % Location in Structure
        unit(kk,4:9) = {C{idx,ii}(jj).Datatype.Class,...
                        C{idx,ii}(jj).Datatype.Size,... % Bytes of one element
                        C{idx,ii}(jj).Dataspace.Size,...
                        C{idx,ii}(jj).Dataspace.Type,...
                        C{idx,ii}(jj).FillValue,...
                        C{idx,ii}(jj).Filters};
    end
end
unit=unit(1:kk,:); sets.ndatasets=size(unit,1); % Only Keep w/ non-empty name
% Assuming that the measurement channels are largest datasets in HDF5
sets.nsamples=max(cellfun(@prod,unit(:,6),'UniformOutput',true));
% Determing which DataSet is a measurement (# Elements equal to nsamples)
unit(:,10) = num2cell(eq(sets.nsamples*ones(sets.ndatasets,1),...
                cellfun(@prod,unit(:,6),'UniformOutput',true)) ...
              & cellfun(@any,cellfun(@ismember,unit(:,6),...
                num2cell(ones(sets.ndatasets,1)),'UniformOutput',false),...
                                              'UniformOutput',true));
sets.nchannels = sum(cell2mat(unit(:,10))); % Number of Channels

%% 4   Sequentially inport of Datasets
for ii=1:sets.ndatasets   % Loop for all available datasets
    if strcmp(opt.mode,'-nodata') % Only List of varialbe names 'nodata'
        eval(['Data',unit{ii,3},'=[];']); continue; % Kein Import
    end
    if opt.verbose % Progress is displayed in the command window
        disp(['Dataset Nr. ',num2str(ii),' of ',num2str(sets.ndatasets),...
              ' - ',unit{ii,2},' is imported...']);
    end   
    % Importing the data (Start and Count define interval)
    if ~unit{ii,10} && ~ismember(opt.mode,{'-chonly','-1map'}) 
        temp = h5read(file,unit{ii,2}); %#ok Other data loaded w/o any restrictions 
    elseif unit{ii,10} % ChannelType
        try temp = h5read(file,unit{ii,2},opt.start,opt.count);  
            temp = feval(opt.type,reshape(temp,numel(temp),1)); %#ok
        catch ME1
            if opt.verbose
                disp(['Error in h5read.m for "',unit{ii,2},'":']);
                msgString=getReport(ME1,'extended','hyperlinks','off');
                disp(msgString);
            end
            continue;
        end
    end
    % Put the Dataset into the MATLAB Structure Variable DATA
    switch opt.mode
        case {'-struct','-chonly'}; eval(['Data',unit{ii,3},'=temp;']);
    end
end

%% 5 Funktionsdefinition in MATLAB Workspace
if calledwithnoArgs
    assignin('caller','Data',Data); assignin('caller','sets',sets);
end
if opt.verbose; disp(['Import of Data completed successfully',char(10)]); end

%% ------------------
%% 6  Unterfunktionen
%% -------------------
function [out,ok]=checkvarname(in,opt)

% [OUT,OK]=CHECKVARNAME(IN,OPT)
%
% Diese Funktion kontrolliert Signalnamen, ob sie f�r einen Variablennamen
% geeignet sind, um bei automatischen Auswertefunktionen Fehlermeldungen zu
% vermeiden.
%
% IN    = Eingabeargument. Muss entweder ein String (Signalname).
% OPT   = Wenn 'UNIQUE' und IN eine Cell, dann werden gleiche Felder mit
%         einem Namen belegt der einzigartig in der Cell ist.
% OUT   = korrigierter Ausgabestring.
% OK    = Bit. Falls etwas korrigiert wurde 0, ansonsten 1.
%
% Zun�chst wird der Anfangsbuchstabe �berpr�ft, der nur kleiner und gro�er
% Buchstabe sein darf. Anschlie�end die restlichen Elemente Strings, welche
% zus�tzlich noch _ und die Zahlen sein d�rfen. Falls das erste Element die 
% Bedingungen nicht erf�llt, dann wird ein X vorgeh�ngt.
%
% Beispiel - IN = '33_2%(\detsd\)djsh3' ergibt OUT = 'X33_2djsh3'
%
% Unterfunktionen: (ausserhalb des m-Files)
%       KEINE --> Autark nutzbar
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Erstellt:                                                             %
% % Christian Schr�ck                                                     %
% % 19.07.2010                                                            %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% F�r das Anfangselement des Strings sind nur Buchstaben erlaubt
first=[65:90 97:122];

% F�r die weiteren Elemente sind daneben noch Zahlen,Unterstrich und Punkt erlaubt
rest=[48:57 65:90 95 97:122]; 

if exist('in','var') && ~isempty(in) && (ischar(in) || iscellstr(in))
    % Umwandlung von CHAR in CellString
    if ischar(in); s=1; in={in}; else s=0; end
else
    if ~exist('in','var'); in=''; end
    out=in; ok=0; return
end
% Additional Characters which are valid for certain variables
if nargin>=3 && ischar(zus); rest=unique([rest zus+0]); end

% Die Funktion kann nur ausgef�hrt werden, wenn das Eingabeargument eine
% String-Variable ist und nicht leer ist.
for ii=1:length(in)
    if isempty(in{ii}); out{ii}=''; continue; end
    
    % Umlaute und Verbindungselemente werden umgewandelt.
    old={'�','�','�','�','�','�','�','\','/','-',char(32),';',',',char(10)};
    new={'oe','ae','ue','Oe','Ae','Ue','ss','_','_','_','_','_','_','_'};
    for jj=1:length(old)
        in{ii}=strrep(in{ii},old{jj},new{jj});
    end
        
    % Sicherstellung, dass keine zwei Backslash hintereinander.
    while ~isempty(findstr(in{ii},'__'))
       in{ii}=strrep(in{ii},'__','_'); 
    end
    
    % Pr�fen des ersten Elements des Eingabesstrings:
    % Falls dieser kein Buchstabe ist, wird ein gro�es X davor gesetzt
    check=find(first==in{ii}(1),1);
    if isempty(check); in{ii}=['X',in{ii}]; end
    out{ii}(1)=in{ii}(1);
    
    % Nun werden die weiteren Elemente des Strings �berpr�ft, falls dieser
    % mehr als ein Element hat. Die Elemente, die weder Buchstaben, noch
    % Zahlen, noch der Unterstrich ist, werden nicht �bernommen
    if length(in{ii})>=2
        k=2;
        for n=2:length(in{ii})
            check=find(rest==in{ii}(n),1);
            if ~isempty(check); out{ii}(k)=in{ii}(n); k=k+1; end        
        end
    end
    
    % Sicherstellung, dass keine zwei Backslash hintereinander.
    while ~isempty(findstr(out{ii},'__'))
       out{ii}=strrep(out{ii},'__','_'); 
    end    
    
    % Zum Schluss wird nach dem letzten Buchstaben oder Zahl abgeschnitten
    out{ii}=out{ii}(1:find(out{ii}~=char(95),1,'last')); ok=1;
end

% Individuelle Vergabe von Variablennamen
if iscellstr(out) && numel(out)>1 && exist('opt','var') && strcmp(opt,'unique')        
    idx = find(~cellfun(@isempty,out,'UniformOutput',true));
    temp = genvarname(out(idx));
    out(idx) = temp;
end

% Wenn Eingabe ein String war, wird dieser wieder als String ausgegeben
if s; out=out{1}; end