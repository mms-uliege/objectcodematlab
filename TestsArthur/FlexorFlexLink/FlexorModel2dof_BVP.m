%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Flexible model of the Flexor robot from TUHH (2 linear joint that are perpendicular).
% 2DOF Version Links modeled as flexible beam elements: 
% Z-axis perpendicular to motion plane X-axis along second linear joint and
% Y- axis along first linear joint
% Here the second flexible link is divided in two equal parts (of length l21 and l22) 
% seperated by a hinge joint

% Solved using the BVP solver

clear all

% Time parameters
dur = '9'; % duration in tenth of seconds
preAct = 0.1; % duration of pre actuation phase
postAct = 0.1; % duration of post actuation phase
% initial_time = 0.1; % Time to cut out at the beginning of the simulation
timestepsize = 0.01;
finaltime = str2double(dur)/10 + preAct + postAct; % sec
t_i = preAct; % time at end of pre-actuation
t_f = finaltime - postAct; % time at begining of post-actuation
npts = round(finaltime/timestepsize + 1); % number of points for optimization

% Gravity parameter
grav = [0 0 0];

% Numerical parameters
rho_num = 0;
% hypForComp = 'constant';
hypForComp = 'notConstant';
% listM = [1 0 0];
% listKt = [1 0 0];
% listCt = [1 0 0];
% listPhiq = [1 0 0];
listM = [1 1 1];
listKt = [1 1 1];
listCt = [1 1 1];
listPhiq = [1 1 1];
type_of_traj = 'Line'; % line trajectory
step_size = num2str(1000*timestepsize);

% General beam damping
alpha = 0.0001;  % Damping coeff proportional to mass of the beams
beta = 0.02; % Damping coeff proportional to stiffness of the beams
% alpha = 0.00;  % Damping coeff proportional to mass of the beams
% beta = 2.25e-5; % Damping coeff proportional to stiffness of the beams

% It is assumed that 0 of y axis (joint 1), on axis X,
% the unknonwn are the positions q1 and q2 of joint 1 and 2 
q1 = -0.102860310578598; % [m] position of the linear joint 1
q2 = -0.141662479035540+0.61; % [m] position of the linear joint 2 + offset
lab = sqrt(q1^2+q2^2);
l1 = 0.42; % [m] length of link 1
l21 = 0.6; % [m] length of first part of link2
l22 = l21; % [m] length of second part link2
% l3 = 0.3; % [m] length of link 3 NOT USED HERE
ang_b0 = asin(-q1/lab);
ang_b1 = acos((lab^2+l21^2-l1^2)/(2*lab*l21));
ang_a1 = acos((lab^2+l1^2-l21^2)/(2*lab*l1));


%% Definition of links and joints
% Link 1
% link1.angle = -acos((l1^2+q2^2-l21^2)/(2*l1*q2)); % [rad] angle between the link and the x axis (CCW)
link1.angle = -(ang_a1-ang_b0); % [rad] angle between the link and the x axis (CCW)
link1.l = l1; % [m] length of first link
link1.e = 0.002; % [m] thickness of beam
link1.h = 0.08; % [m] height of beam
link1.rho = 7900; % [kg/m�] density of beam
link1.E = 181.4e9; % [N/m�]
link1.nu = 0.3; % poisson coef
link1.G = link1.E/(2*(1+link1.nu)); % compression modulus
link1.A = link1.e*link1.h; % [m�] cross section area
link1.m = link1.rho * link1.A * link1.l; % [kg] Mass of first link
link1.J = (link1.m/12)*diag([link1.e^2+link1.h^2 link1.l^2+link1.h^2 link1.e^2+link1.l^2]); % [kg*m�] rigid body inertia
link1.Ixx = (link1.e^2 + link1.h^2)*link1.e*link1.h/12; % [m^4] area moment inertia in case of beam torsion
link1.Iyy = (link1.e*link1.h^3)/12; % [m^4] area moment inertia in case of beam (out of plane lexion)
link1.Izz = (link1.e^3*link1.h)/12; % [m^4] area moment inertia in case of beam (in-plane flexion)
link1.KCS = diag([link1.E*link1.A link1.G*link1.A link1.G*link1.A link1.G*link1.Ixx link1.E*link1.Iyy link1.E*link1.Izz]);
link1.MCS = diag(link1.rho*[link1.A link1.A link1.A link1.Ixx link1.Iyy link1.Izz]);
link1.nodes = [0 q1 0;...
               link1.l*cos(link1.angle) q1+link1.l*sin(link1.angle) 0];
link1.xAxis = (link1.nodes(end,:)-link1.nodes(1,:))/norm(link1.nodes(end,:)-link1.nodes(1,:));
link1.yAxis = [-link1.xAxis(2) link1.xAxis(1) 0];
link1.rotA = [link1.xAxis; link1.yAxis; cross(link1.xAxis,link1.yAxis)]; % Rotation of the inertia with respect to inertial axis 
link1.nElem = 2; % Number of elements composing the link
% adding necessary nodes
link1.nodes = CreateInterCoord(link1.nodes,link1.nElem,1,2);

% Link 21
% link21.angle = acos((l21^2+q2^2-l1^2)/(2*l21*q2)); % [rad] angle between the link and the x axis (CCW)
link21.angle = ang_b1+ang_b0; % [rad] angle between the link and the x axis (CCW)
link21.l = l21; % [m] length of link
link21.e = 0.002; % [m] thickness of beam
link21.h = 0.08; % [m] height of beam
link21.rho = 7900; % [kg/m�] density of beam
link21.E = 181.4e9; % [N/m�]
link21.nu = 0.3; % poisson coef
link21.G = link21.E/(2*(1+link21.nu)); % compression modulus
link21.A = link21.e*link21.h; % [m�] cross section area
link21.m = link21.rho * link21.A * link21.l; % [kg] Mass of second link
link21.J = (link21.m/12)*diag([link21.e^2+link21.h^2 link21.l^2+link21.h^2 link21.e^2+link21.l^2]); % [kg*m�] rigid body inertia
link21.Ixx = (link21.e^2 + link21.h^2)*link21.e*link21.h/12; % [m^4] area moment inertia in case of beam torsion
link21.Iyy = (link21.e*link21.h^3)/12; % [m^4] area moment inertia in case of beam (out of plane lexion)
link21.Izz = (link21.e^3*link21.h)/12; % [m^4] area moment inertia in case of beam (in-plane flexion)
link21.KCS = diag([link21.E*link21.A link21.G*link21.A link21.G*link21.A link21.G*link21.Ixx link21.E*link21.Iyy link21.E*link21.Izz]);
link21.MCS = diag(link21.rho*[link21.A link21.A link21.A link21.Ixx link21.Iyy link21.Izz]);
link21.nodes = [link1.nodes(end,:);...
               link1.nodes(end,1)+link21.l*cos(link21.angle) link1.nodes(end,2)+link21.l*sin(link21.angle) 0];
link21.xAxis = (link21.nodes(end,:)-link21.nodes(1,:))/norm(link21.nodes(end,:)-link21.nodes(1,:));
link21.yAxis = [-link21.xAxis(2) link21.xAxis(1) 0];
link21.rotA = [link21.xAxis; link21.yAxis; cross(link21.xAxis,link21.yAxis)]; % Rotation of the inertia with respect to inertial axis 
link21.nElem = 2; % Number of elements composing the link
% adding necessary nodes
link21.nodes = CreateInterCoord(link21.nodes,link21.nElem,1,2);

% Link 22
link22.angle = link21.angle; % [rad] angle between the link and the x axis (CCW)
link22.l = l22; % [m] length of link
link22.e = 0.003; % [m] thickness of beam
link22.h = 0.08; % [m] height of beam
link22.rho = 7900; % [kg/m�] density of beam
link22.E = 179e9; % [N/m�]
link22.nu = 0.3; % poisson coef
link22.G = link22.E/(2*(1+link22.nu)); % compression modulus
link22.A = link22.e*link22.h; % [m�] cross section area
link22.m = link22.rho * link22.A * link22.l; % [kg] Mass of second link
link22.J = (link22.m/12)*diag([link22.e^2+link22.h^2 link22.l^2+link22.h^2 link22.e^2+link22.l^2]); % [kg*m�] rigid body inertia
link22.Ixx = (link22.e^2 + link22.h^2)*link22.e*link22.h/12; % [m^4] area moment inertia in case of beam torsion
link22.Iyy = (link22.e*link22.h^3)/12; % [m^4] area moment inertia in case of beam (out of plane lexion)
link22.Izz = (link22.e^3*link22.h)/12; % [m^4] area moment inertia in case of beam (in-plane flexion)
link22.KCS = diag([link22.E*link22.A link22.G*link22.A link22.G*link22.A link22.G*link22.Ixx link22.E*link22.Iyy link22.E*link22.Izz]);
link22.MCS = diag(link22.rho*[link22.A link22.A link22.A link22.Ixx link22.Iyy link22.Izz]);
link22.nodes = [link21.nodes(end,:);...
               link21.nodes(end,1)+link22.l*cos(link22.angle) link21.nodes(end,2)+link22.l*sin(link22.angle) 0];
link22.xAxis = (link22.nodes(end,:)-link22.nodes(1,:))/norm(link22.nodes(end,:)-link22.nodes(1,:));
link22.yAxis = [-link22.xAxis(2) link22.xAxis(1) 0];
link22.rotA = [link22.xAxis; link22.yAxis; cross(link22.xAxis,link22.yAxis)]; % Rotation of the inertia with respect to inertial axis 
link22.nElem = 2; % Number of elements composing the link
% adding necessary nodes
link22.nodes = CreateInterCoord(link22.nodes,link22.nElem,1,2);

% Linear Joint 1
joint1.axis = [0 1 0 0 0 0]'; % Axis of joint
joint1.stiff = 0; % stiffness of joint
joint1.coul = 0; % Coulomb friction of joint
joint1.damp = 0; % Viscous friction of joint

% Linear Joint 2
joint2.axis = [1 0 0 0 0 0]'; % Axis of joint
joint2.stiff = 0; % stiffness of joint
joint2.coul = 0; % Coulomb friction of joint
joint2.damp = 0; % Viscous friction of joint

% PASSIVE hinge Joint 4 (at first linear joint)
joint4.axis = [0 0 0 0 0 1]'; % Axis of joint
joint4.stiff = 0; % stiffness of joint
joint4.coul = 0; % Coulomb friction of joint
joint4.damp = 0; % Viscous friction of joint

% PASSIVE hinge Joint 5 (between first and second link)
joint5.axis = [0 0 0 0 0 1]'; % Axis of joint
joint5.stiff = 0; % stiffness of joint
joint5.coul = 0; % Coulomb friction of joint
joint5.damp = 0; % Viscous friction of joint

% PASSIVE hinge Joint 6 (at second linear joint)
joint6.axis = [0 0 0 0 0 1]'; % Axis of joint
joint6.stiff = 0; % stiffness of joint
joint6.coul = 0; % Coulomb friction of joint
joint6.damp = 0; % Viscous friction of joint

% Mass on linear joint 1 (first arm start)
m1 = 0.811;
J1 = link1.rotA*diag([588e-6 748e-6 490e-6])*link1.rotA';

% Mass on passive hinge  (first arm end)
mj1 = 1.031;
Jj1 = link1.rotA*diag([1175e-6 3055e-6 2250e-6])*link1.rotA';
% Mass on passive hinge  (second arm start)
mj2 = 0.811;
Jj2 = link21.rotA*diag([588e-6 748e-6 490e-6])*link21.rotA';

% Mass on linear joint 2 (second arm middle)
m2 = 1.275;
J2 = link22.rotA*diag([915e-6 1828e-6 1405e-6])*link22.rotA';

% Mass on end-effector (second arm end)
m_end = 0.929; % [kg] end-effector mass
J_end = link22.rotA*diag([671e-6 871e-6 544e-6])*link22.rotA';

%% Construction of the rigid robot model
% Nodes
nodes = [link1.nodes(1,:);...
         link1.nodes(1,:);...
         link1.nodes;...    
         link21.nodes;...
         link22.nodes(2:end,:);...
         link22.nodes(1,:)]; % List of nodes to build the model
     
% number all nodes properly so they can be used by FEMmodel
nodes = [1:size(nodes,1);nodes']';     
     
% Elements
count = 1;
Start1 = 3; % first node of the 1st link
% creating first link (rotating aroud z)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+link1.nElem/2 Start1+[0:link1.nElem/2-1] Start1+link1.nElem/2+1+[0:link1.nElem/2-1]];
elements{count}.mass = link1.m;
elements{count}.J = link1.rotA*link1.J*link1.rotA';
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% Second link 21
count = size(elements,2)+1;
Start21 = Start1+link1.nElem+1; % first node of the 21 link
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start21+link21.nElem/2 Start21+[0:link21.nElem/2-1] Start21+link21.nElem/2+1+[0:link21.nElem/2-1]];
elements{count}.mass = link21.m;
elements{count}.J = link21.rotA*link21.J*link21.rotA';
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% Second link 22
count = size(elements,2)+1;
Start22 = Start21+link21.nElem; % first node of the 22 link
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start22+link22.nElem/2 Start22+[0:link22.nElem/2-1] Start22+link22.nElem/2+1+[0:link22.nElem/2-1]];
elements{count}.mass = link22.m;
elements{count}.J = link22.rotA*link22.J*link22.rotA';
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body 1st linear joint (along y)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1-1];
elements{count}.mass = m1;
elements{count}.J = J1;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body passive hinge end of link 1
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start21-1];
elements{count}.mass = mj1;
elements{count}.J = Jj1;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body passive hinge start of link 2
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start21];
elements{count}.mass = mj2;
elements{count}.J = Jj2;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body 2nd linear joint (along x)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start22];
elements{count}.mass = m2;
elements{count}.J = J2;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body end-effector
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end-1,1)];
elements{count}.mass = m_end;
elements{count}.J = J_end;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% PASSIVE hinge joint 4 (on 1st linear joint)
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start1-1 Start1];
elements{count}.k = joint4.stiff;
elements{count}.d = joint4.damp;
elements{count}.coulomb = joint4.coul;
elements{count}.A = joint4.axis;

% PASSIVE hinge joint 5 (between link 1 and link 21)
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start21-1 Start21];
elements{count}.k = joint5.stiff;
elements{count}.d = joint5.damp;
elements{count}.coulomb = joint5.coul;
elements{count}.A = joint5.axis;

% PASSIVE hinge joint 6 (on second linear joint)
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start21+link21.nElem/2+1 nodes(end,1)];
elements{count}.k = joint6.stiff;
elements{count}.d = joint6.damp;
elements{count}.coulomb = joint6.coul;
elements{count}.A = joint6.axis;

% ACTUATED first linear joint
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.k = joint1.stiff;
elements{count}.d = joint1.damp;
elements{count}.coulomb = joint1.coul;
elements{count}.A = joint1.axis;

% ACTUATED second linear joint
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 nodes(end,1)];
elements{count}.k = joint2.stiff;
elements{count}.d = joint2.damp;
elements{count}.coulomb = joint2.coul;
elements{count}.A = joint2.axis;

% Trajectory
x_end = 1.2;
y_end = 0.2;
% x_end = nodes(end-1,2)+0.3;
% y_end = nodes(end-1,3)+0.3;

timeVector = 0:timestepsize:finaltime;
trajx = lineTraj(nodes(end-1,2),x_end,timeVector,t_i,t_f);
trajy = lineTraj(nodes(end-1,3),y_end,timeVector,t_i,t_f);
% % Trajectory loaded from the DATAs comming from Hamburg
% load('C:\ObjectCodeMatlab\TestsArthur\FlexorFlexLink\timeData.mat')
% load('C:\ObjectCodeMatlab\TestsArthur\FlexorFlexLink\trajxData.mat')
% load('C:\ObjectCodeMatlab\TestsArthur\FlexorFlexLink\trajyData.mat')
% indexI = find(timeVector==t_i,1);
% indexF = find(timeVector==t_f,1);
% lengthTime = length(timeVector);
% trajx(1:indexI) = trajxData(1)*ones(1,indexI);
% trajy(1:indexI) = trajyData(1)*ones(1,indexI);
% trajx(indexF:lengthTime) = trajxData(end)*ones(1,lengthTime-indexF+1);
% trajy(indexF:lengthTime) = trajyData(end)*ones(1,lengthTime-indexF+1);
% trajx(indexI:indexF) = interp1(timeData,trajxData,0:timestepsize:t_f-t_i,'linear');
% trajy(indexI:indexF) = interp1(timeData,trajyData,0:timestepsize:t_f-t_i,'linear');

count = size(elements,2)+1;
% elements{count}.type = 'TrajectoryConstraint';
elements{count}.type = 'TrajectoryConstraint4';
elements{count}.nodes = [nodes(end-1,1)];
elements{count}.T = [trajx;...
                     trajy];
elements{count}.Axe = [1 0 0;...
                       0 1 0];
elements{count}.elements = [count-2 count-1];
elements{count}.active = 1;
% plot(timeVector,[trajx;trajy])

% Boundary Condition
BC = [1];

%% Solving rigid model
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();
save(['C:\ObjectCodeMatlab\TestsArthur\FlexorFlexLink\D_2dof_',dur,'_h',step_size,'_BVP.mat'],'D')

%% Construction of the flexible robot model
clear elements

% Elements
% creating first link (rotating aroud z)
% count = 0;
% Start1 = 3; % first node of the 1st link
% for i = 1:link1.nElem
%     elements{count+i}.type = 'FlexibleBeamElement';
%     elements{count+i}.nodes = [Start1-1+i Start1+i];
%     elements{count+i}.KCS = link1.KCS;
%     elements{count+i}.MCS = link1.MCS;
%     elements{count+i}.yAxis = link1.yAxis;
%     elements{count+i}.alpha = alpha;
%     elements{count+i}.beta = beta;
%     elements{count+i}.listM = listM;
%     elements{count+i}.listCt = listCt;
%     elements{count+i}.listKt = listKt;
%     elements{count+i}.listPhiq = listPhiq;
%     elements{count+i}.g = grav;
% end
count = 1;
Start1 = 3; % first node of the 1st link
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+link1.nElem/2 Start1+[0:link1.nElem/2-1] Start1+link1.nElem/2+1+[0:link1.nElem/2-1]];
elements{count}.mass = link1.m;
elements{count}.J = link1.rotA*link1.J*link1.rotA';
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% Second link 21
count = size(elements,2);
Start21 = Start1+link1.nElem+1; % first node of link 21
for i = 1:link21.nElem
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start21-1+i Start21+i];
    elements{count+i}.KCS = link21.KCS;
    elements{count+i}.MCS = link21.MCS;
    elements{count+i}.yAxis = link21.yAxis;
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.listPhiq = listPhiq;
    elements{count+i}.g = grav;
end

% Second link 22
count = size(elements,2);
Start22 = Start21+link21.nElem; % first node of link 22
for i = 1:link22.nElem
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start22-1+i Start22+i];
    elements{count+i}.KCS = link22.KCS;
    elements{count+i}.MCS = link22.MCS;
    elements{count+i}.yAxis = link22.yAxis;
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.listPhiq = listPhiq;
    elements{count+i}.g = grav;
end

% creating rigid body 1st linear joint (along y)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1-1];
elements{count}.mass = m1;
elements{count}.J = J1;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body passive hinge end of link 1
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start21-1];
elements{count}.mass = mj1;
elements{count}.J = Jj1;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body passive hinge start of link 2
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start21];
elements{count}.mass = mj2;
elements{count}.J = Jj2;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body 2nd linear joint (along x)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start22];
elements{count}.mass = m2;
elements{count}.J = J2;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body end-effector
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end-1,1)];
elements{count}.mass = m_end;
elements{count}.J = J_end;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% PASSIVE hinge joint 4 (on 1st linear joint)
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start1-1 Start1];
elements{count}.k = joint4.stiff;
elements{count}.d = joint4.damp;
elements{count}.coulomb = joint4.coul;
elements{count}.A = joint4.axis;

% PASSIVE hinge joint 5 (between link 1 and link 21)
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start21-1 Start21];
elements{count}.k = joint5.stiff;
elements{count}.d = joint5.damp;
elements{count}.coulomb = joint5.coul;
elements{count}.A = joint5.axis;

% PASSIVE hinge joint 6 (on second linear joint)
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start21+link21.nElem/2+1 nodes(end,1)];
elements{count}.k = joint6.stiff;
elements{count}.d = joint6.damp;
elements{count}.coulomb = joint6.coul;
elements{count}.A = joint6.axis;

% ACTUATED first linear joint
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.k = joint1.stiff;
elements{count}.d = joint1.damp;
elements{count}.coulomb = joint1.coul;
elements{count}.A = joint1.axis;

% ACTUATED second linear joint
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 nodes(end,1)];
elements{count}.k = joint2.stiff;
elements{count}.d = joint2.damp;
elements{count}.coulomb = joint2.coul;
elements{count}.A = joint2.axis;

count = size(elements,2)+1;
elements{count}.type = 'TrajectoryConstraint4';
elements{count}.nodes = [nodes(end-1,1)];
elements{count}.T = [trajx;...
                     trajy];
elements{count}.Axe = [1 0 0;...
                       0 1 0];
elements{count}.elements = [count-2 count-1];
elements{count}.active = 1;

% Solving
ModelF = FEModel();
ModelF.CreateFEModel(nodes,elements);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from static solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
        ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

% Start second full optimization with newly computed initial guess
tic
S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = rho_num;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.parameters.scaling = 1e6;
S.ConstIter = hypForComp;
% analys = S.runPoleAnalysisOpti(D)
xSol = S.runBVP(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])
save(['C:\ObjectCodeMatlab\TestsArthur\FlexorFlexLink\S_2dof_',dur,'_h',step_size,'_BVP.mat'],'S')
