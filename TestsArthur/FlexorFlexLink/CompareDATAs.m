%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script to read and plot the DATAs from the Flexor measured data and
% compare it with own simulated datas (if compare == true)
% clear all
compare = 1;
compareR = 1;
% Load the DATAs using their 'h5load' function
[DATA, UNIT, SETS] = h5load('C:\ObjectCodeMatlab\TestsArthur\FlexorFlexLink\FF_line_0.9s_traj_1edof_EETPexact_desOut_01-Mar-2018_11_10_23_IROSfinal_....h5');

% Explaination of data:
% - a, b: position of actuated linear axis 1 and 2.
% - ua, ub: force on actuated linear axis 1 and 2.
% - Da, Db: velocity of actuated linear axis 1 and 2.
% - D2a, D2b: acceleration of actuated linear axis 1 and 2.
% - alpha, beta: angle of link 1 (CW) and link 2 (CCW) with respect to the x axis.
% - Dalpha, Dbeta: angular velocity of link 1 (CW) and link 2 (CCW) with respect to the x axis.
% - EEx, EEy: Cartesian position of end-effector.
% - DEEx, DEEy: Cartesian velocity of end-effector.
% - time: time vector.

% Loading own simulation results
init_time = 0.8; % shift in time so that sim time matches DATA time
a_init = -0.102860310578598; % shift in a so that sim a matches DATA a
b_init = -0.141662479035540; % shift in b so that sim b matches DATA b
ua_forceCte = 80;
ub_forceCte = 80;
load('C:\ObjectCodeMatlab\TestsArthur\FlexorFlexLink\S_2dof_9.mat')
load('C:\ObjectCodeMatlab\TestsArthur\FlexorFlexLink\D_2dof_9.mat')
% Extract model information
time_sim = S.timeValues+init_time;
a_sim = S.model.listElementVariables{4}.relCoo;
b_sim = S.model.listElementVariables{5}.relCoo;
da_sim = S.model.listElementVariables{4}.velocity;
db_sim = S.model.listElementVariables{5}.velocity;
d2a_sim = S.model.listElementVariables{4}.acceleration;
d2b_sim = S.model.listElementVariables{5}.acceleration;
ua_sim = S.model.listElementVariables{end}.value(1,:);
ub_sim = S.model.listElementVariables{end}.value(2,:);
x_sim = S.model.listNodes{end-1}.position(1,:);
y_sim = S.model.listNodes{end-1}.position(2,:);
dx_sim = S.model.listNodes{end-1}.velocity(1,:);
dy_sim = S.model.listNodes{end-1}.velocity(2,:);

% Rigid simulation
a_simR = D.model.listElementVariables{4}.relCoo;
b_simR = D.model.listElementVariables{5}.relCoo;
da_simR = D.model.listElementVariables{4}.velocity;
db_simR = D.model.listElementVariables{5}.velocity;
d2a_simR = D.model.listElementVariables{4}.acceleration;
d2b_simR = D.model.listElementVariables{5}.acceleration;
ua_simR = D.model.listElementVariables{end}.value(1,:);
ub_simR = D.model.listElementVariables{end}.value(2,:);

% Plot linear axis position
leg = {};
figure
hold on
plot(DATA.desired.time,[DATA.desired.a DATA.desired.b],'Linewidth',2)
title('Position of linear axis 1 (a) and 2 (b)')
xlabel('Time [s]')
ylabel('Position [m]')
leg = [leg, 'a', 'b'];
if compare
    set(gca,'ColorOrderIndex',1)
    plot(time_sim,[a_sim+a_init; b_sim+b_init],'--','Linewidth',2)
    leg = [leg, 'a_{sim}','b_{sim}'];
end
if compareR
    set(gca,'ColorOrderIndex',1)
    plot(time_sim,[a_simR+a_init; b_simR+b_init],'--')
    leg = [leg, 'a_{simR}','b_{simR}'];
end
legend(leg)
grid on

% Plot linear axis velocity
leg = {};
figure
hold on
plot(DATA.desired.time,[DATA.desired.Da DATA.desired.Db],'Linewidth',2)
title('Velocity of linear axis 1 (a) and 2 (b)')
xlabel('Time [s]')
ylabel('Velocity [m/s]')
leg = [leg, 'a', 'b'];
if compare
    set(gca,'ColorOrderIndex',1)
    plot(time_sim,[da_sim; db_sim],'--','Linewidth',2)
    leg = [leg, 'a_{sim}','b_{sim}'];
end
if compare
    set(gca,'ColorOrderIndex',1)
    plot(time_sim,[da_simR; db_simR],'--')
    leg = [leg, 'a_{simR}','b_{simR}'];
end
legend(leg)
grid on

% % Plot linear axis acceleration
% leg = {};
% figure
% holde on
% plot(DATA.desired.time,[DATA.desired.D2a DATA.desired.D2b],'Linewidth',2)
% title('Acceleration of linear axis 1 (a) and 2 (b)')
% xlabel('Time [s]')
% ylabel('Acceleration [m/s^2]')
% leg = [leg, 'a', 'b'];
% if compare
%     set(gca,'ColorOrderIndex',1)
%     plot(time_sim,[d2a_sim; d2b_sim],'--','Linewidth',2)
%     leg = [leg, 'a_{sim}','b_{sim}'];
% end
% if compareR
%     set(gca,'ColorOrderIndex',1)
%     plot(time_sim,[d2a_simR; d2b_simR],'--')
%     leg = [leg, 'a_{simR}','b_{simR}'];
% end
% grid on

% Plot linear axis force
leg = {};
figure
hold on
plot(DATA.desired.time,[DATA.desired.ua DATA.desired.ub],'Linewidth',2)
title('Force of linear axis 1 (ua) and 2 (ub)')
xlabel('Time [s]')
ylabel('Force [N]')
leg = [leg, 'ua', 'ub'];
if compare
    set(gca,'ColorOrderIndex',1)
    plot(time_sim,[ua_sim/ua_forceCte; ub_sim/ub_forceCte],'--','Linewidth',2)
    leg = [leg, 'ua_{sim}','ub_{sim}'];
end
if compareR
    set(gca,'ColorOrderIndex',1)
    plot(time_sim,[ua_simR/ua_forceCte; ub_simR/ub_forceCte],'--')
    leg = [leg, 'ua_{simR}','ub_{simR}'];
end
legend(leg)
grid on

% Plot cartesian end-effector position
leg = {};
figure
hold on
plot(DATA.desired.time,[DATA.desired.EEx DATA.desired.EEy],'Linewidth',2)
title('Cartesian end-effector position')
xlabel('Time [s]')
ylabel('Position [m]')
leg = [leg, 'EEx', 'EEy'];
if compare
    set(gca,'ColorOrderIndex',1)
    plot(time_sim,[x_sim; y_sim],'--','Linewidth',2)
    leg = [leg, 'x_{sim}','y_{sim}'];
end
legend(leg)
grid on

% Plot cartesian end-effector velocity
leg = {};
figure
hold on
plot(DATA.desired.time,[DATA.desired.DEEx DATA.desired.DEEy],'Linewidth',2)
title('Cartesian end-effector velocity')
xlabel('Time [s]')
ylabel('Velocity [m/s]')
leg = [leg, 'EEx', 'EEy'];
if compare
    set(gca,'ColorOrderIndex',1)
    plot(time_sim,[dx_sim; dy_sim],'--','Linewidth',2)
    leg = [leg, 'x_{sim}','y_{sim}'];
end
legend(leg)
grid on

% % Save the DATA.desired.EEx and DATA.desired.EEy trajectory information so
% % that it can be used in FE code.
% startTime = 1;
% endTime = startTime+0.9;
% initIndex = find(timeData==startTime);
% finalIndex = find(timeData==endTime);
% timeData = DATA.desired.time(initIndex:finalIndex)-startTime;
% trajxData = DATA.desired.EEx(initIndex:finalIndex);
% trajyData = DATA.desired.EEy(initIndex:finalIndex);
% save('C:\ObjectCodeMatlab\TestsArthur\FlexorFlexLink\timeData.mat','timeData');
% save('C:\ObjectCodeMatlab\TestsArthur\FlexorFlexLink\trajxData.mat','trajxData');
% save('C:\ObjectCodeMatlab\TestsArthur\FlexorFlexLink\trajyData.mat','trajyData');


