%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear

finaltime = 2;
timestepsize = 0.01;
t_i = 0.2;
t_f = 1.8;

nodes = [1 0 0 0;
         2 0 0 0;
         3 -0.3*sqrt(3)/2 0 -0.3*0.5;
         4 -0.3*sqrt(3)/2 0 -0.3*0.5;
         5 -0.3*sqrt(3) 0 0];
     
nElem1 = 4;
Start1 = 2;
End1 = 3;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

nElem2 = 4;
Start2 = End1+nElem1;
End2 = Start2+1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);


a = 0.002;
b = 0.01;
l = 0.3;
rho = 2700;
m = a*b*l*rho;
Ixx = m*(a^2+b^2)/12;
Iyy = m*(a^2+l^2)/12;
Izz = m*(b^2+l^2)/12;
count = 1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [3 2 4];
% elements{count}.mass = m; % 0.01*0.002*0.4*2700 = 0.0216
% % elements{count}.J = diag([Ixx Iyy Izz]);
% elements{count}.J = 0.001*eye(3);
% elements{count}.g = [0 0 -9.81];
% count = count +1;
% 
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [6 5 7];
% elements{count}.mass = m; % 0.01*0.002*0.4*2700 = 0.0216
% % elements{count}.J = diag([Ixx Iyy Izz]);
% elements{count}.J = 0.001*eye(3);
% elements{count}.g = [0 0 -9.81];
% count = count +1;

circular = false;
if ~circular
% square
%     a = 0.002;
%     b = 0.01;
    E = 70e9; nu = 0.3; G = E/(2*(1+nu));
    A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;
else
% circular
    d = 0.005;
    E = 70e9; nu = 0.3; G = E/(2*(1+nu));
    A = pi*d^2/4; I = pi*d^4/64; J = 2*I;
end

KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

type = 'FlexibleBeamElement';
if exist('elements')== 1
    count = size(elements,2);
else
    count = 0;
end
for i = 1:nElem1
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start1+i-1,1) nodes(Start1+i,1)];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
end

count = size(elements,2);
for i = 1:nElem2
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
end

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2];
elements{count}.mass = 0.0316; % 0.03*0.019*0.04*2700
% elements{count}.g = [0 0 -9.81];
count = count +1;

elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = 0.0316; % 0.03*0.019*0.04*2700
% elements{count}.g = [0 0 -9.81];
count = count +1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 1 0]';
count = count+1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.A = [0 0 0 0 1 0]';
count = count+1;

% Trajectory

x_end = -nodes(end,2);
z_end = nodes(end,4);
r = abs(nodes(end,2)+nodes(end,2))/2;

timeVector = 0:timestepsize:finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'sin',t_i,t_f);
trajy = nodes(end,3)*ones(size(timeVector));
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

load('uRigiExperimentDoubleBasic')
u1_init = interp1(uRigiBeam.time,uRigiBeam.u1,timeVector,'linear');
u2_init = interp1(uRigiBeam.time,uRigiBeam.u2,timeVector,'linear');
% u1 = u1_init;
% u2 = u2_init;

load('uExperimentDoubleBasic')
u1 = interp1(uBeam.time,uBeam.u1,timeVector,'linear');
u2 = interp1(uBeam.time,uBeam.u2,timeVector,'linear');

elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = [count-2];
elements{count}.f = u1;
count = count+1;

elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = [count-2];
elements{count}.f = u2;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

Modelr = FEModel();
Modelr.CreateFEModel(nodes,elements);
Modelr.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.0;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

Modelr.listElements{count-1}.f = u1_init;
Modelr.listElements{count}.f = u2_init;

Dr = DynamicIntegration(Modelr);
Dr.parameters.finaltime = finaltime;
Dr.parameters.timestepsize = timestepsize;
Dr.parameters.rho = 0.0;
% Dr.parameters.relTolRes = 1e-10;
Dr.parameters.scaling = 1e6;
Dr.runIntegration();

x = Model.listNodes{end}.position(1,:);
z = Model.listNodes{end}.position(3,:);
xr = Modelr.listNodes{end}.position(1,:);
zr = Modelr.listNodes{end}.position(3,:);

% Plots

timeSteps = D.parameters.time;

figure
hold on
plot(x,z, '-o','Linewidth',2, 'Color','r')
plot(xr,zr,'--', 'Linewidth',2, 'Color','b')
plot(trajx,trajz, 'Linewidth',2, 'Color','k')
legend('With u','With u_{rigid}','Prescribed')
xlabel('X [m]','Fontsize',16)
ylabel('Z [m]','Fontsize',16)
grid on


figure
hold on
plot(timeSteps,x,timeSteps,z)
plot(timeSteps,trajx,'--',timeSteps,trajz,'--')
legend('X', 'Z','Xd','Zd')
grid on

figure
hold on
plot(timeSteps,u1,timeSteps,u2,'Linewidth',2)
plot(timeSteps,u1_init(:),'--',timeSteps,u2_init(:),'--','Linewidth',2)
xlabel('Time [s]','Fontsize',16)
ylabel('Commands','Fontsize',16)
legend('u_1 [N]','u_2 [Nm]','u_{1,rigid} [N]','u_{2,rigid} [Nm]','Location','Best')
grid on

RelError = zeros(size(timeSteps));
RelErrorR = zeros(size(timeSteps));
for i = 1:length(RelError)
    RelError(i) = 100*(norm([trajx(i)-x(i) trajz(i)-z(i)])/norm([trajx(i) trajz(i)]));
    
    RelErrorR(i) = 100*(norm([trajx(i)-xr(i) trajz(i)-zr(i)])/norm([trajx(i) trajz(i)]));
end
figure
plot(timeSteps,RelError,timeSteps,RelErrorR)
title('Relative error of the direct dynamic trajectory','Fontsize',13)
xlabel('time')
ylabel('Relative Error (%)')
legend('Flexible','Rigid')
grid on

RMSRelError = sqrt(mean(RelError.^2))
MaxRelError = max(RelError)
RMSRelErrorR = sqrt(mean(RelErrorR.^2))
MaxRelErrorR = max(RelErrorR)



