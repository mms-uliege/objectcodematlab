%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Double Pendulum example to Test TrajectoryConstraint3 Element with changed manner to calculate constraint
% if the optimization process works for a traditionnal dynamic integration scheme

clear Model
clear elements
clear nodes

finaltime = 1;
timestepsize = 0.01;
time = 0:timestepsize:finaltime;
npts = finaltime/timestepsize +1;
t_i = 0.1;
t_f = finaltime-t_i;

% hypForComp = 'notConstant';
hypForComp = 'constant';
listM = [1 0 0];
listKt = [1 0 0];
listCt = [1 0 0];
nPass = 0;

grav = [0 0 -9.81];

l1 = 1;
l2 = 1;
angle = 30*pi/180;
nodes = [1 0 0 0;
         2 0 0 0;
         3 l1*cos(angle) 0 l1*sin(angle);
         4 l1*cos(angle) 0 l1*sin(angle);
         5 l1*cos(angle)+l2*cos(angle) 0 0];
     
%  nodes = [1 0 0 0;...
%      2 0 0 0;...
%      3 1 0 0];

nElem1 = 1;
Start1 = 2;
nodes = createInterNodes(nodes,nElem1,Start1,Start1+1);

nElem2 = 1;
Start2 = Start1+nElem1+1;
nodes = createInterNodes(nodes,nElem2,Start2,Start2+1);

stiff = 0;
damp = 0;
coulomb = 0;

rho_num = 0.1;
scaling = 1e6;

kp = 200;
kd = 5;

E = 210e9; % link modulus in N/m�
l = 1; % link length in m
w = 0.015; % link width in m
h = 0.004; % link height in m
rho = 7800; % mass density in kg/m�
A = 2.09e-4; % cross section area of the link in m�
m = l * A * rho; % mass of link in kg
% J = diag([m*(w^2 + h^2)/12 m*(l^2 + h^2)/12 m*(l^2 + w^2)/12]); % inertia in kg*m�
J = diag([2.18e-4 0.08374 0.08374]); % inertia in kg*m�
nu = 0.27; % poisson coef
G = E/(2*(1+nu)); % compression modulus
% Ixx = (w*h)*(w^2 + h^2)/12; % area moment inertia in case of beam in m^4
% Iyy = (w*h^3)/12; % area moment inertia in case of beam in m^4
% Izz = (w^3*h)/12; % area moment inertia in case of beam in m^4
Ixx = 1.76e-8; % area moment inertia in case of beam in m^4
Iyy = 1.01e-8; % area moment inertia in case of beam in m^4
Izz = 0.75e-8; % area moment inertia in case of beam in m^4
KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

count = 0;
for i = 1:nElem1
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start1-1+i Start1+i];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.g = grav;
end

count = size(elements,2);
for i = 1:nElem2
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start2-1+i Start2+i];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.g = grav;
end

% count = 1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [Start1+nElem1/2 Start1+(0:nElem1/2-1) Start1+nElem1/2+(0:nElem1/2-1)+1];
% elements{count}.mass = m;
% elements{count}.J = J;
% elements{count}.g = grav;
% 
% count = size(elements,2)+1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [Start2+nElem2/2 Start2+(0:nElem2/2-1) Start2+nElem2/2+(0:nElem2/2-1)+1];
% elements{count}.mass = m;
% elements{count}.J = J;
% elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = 3;
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.k = stiff;
elements{count}.d = damp;
elements{count}.coulomb = coulomb;
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.k = stiff;
elements{count}.d = damp;
elements{count}.coulomb = coulomb;
elements{count}.A = [0 0 0 0 1 0]';

% count = size(elements,2)+1;
% elements{count}.type = 'ForceInKinematicConstraint';
% elements{count}.elements = count-2;
% elements{count}.elements = count-1;
% elements{count}.f = 0*ones(size(time));
% elements{count}.ref = pi/6*ones(size(time));
% elements{count}.dref = 0*ones(size(time));

x_end = 0;
z_end = -1.5;
trajx = lineTraj(nodes(end,2),x_end,time,t_i,t_f);
% trajy = lineTraj(nodes(end,3),y_end,time,t_i,t_f);
trajz = lineTraj(nodes(end,4),z_end,time,t_i,t_f);

count = size(elements,2)+1;
elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 0 1];
elements{count}.elements = [count-2 count-1];
elements{count}.active = 1;

% BC
BC = [1];

% Create Model

ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

% Solving
 
% D = DynamicIntegration(ModelR);
D = StaticIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
% D.parameters.rho = rho_num;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = scaling;
D.runIntegration();

elements{count}.type = 'TrajectoryConstraint'; % need to be changed, because previously it was impossible to compute the "deltaq", since no variable _initOpti yet
% elements{count}.Axe = [1 0 0 0 0 0;...
%                        0 0 1 0 0 0];

ModelF = FEModel();
ModelF.CreateFEModel(nodes,elements);
ModelF.defineBC(BC);

for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
%     ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
%     ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
    dh = zeros(6,npts);
    for i = 1:npts
        dh(:,i) = logSE3([dimR(ModelF.listNodes{n}.R(:,i)) ModelF.listNodes{n}.position(:,i); 0 0 0 1]);
    end
    ModelF.listNodes{n}.velocity = deriveVal(dh,timestepsize);
    ModelF.listNodes{n}.acceleration = deriveVal(ModelF.listNodes{n}.velocity,timestepsize);
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
%         ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
%         ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
        ModelF.listElementVariables{n}.velocity = deriveVal(ModelF.listElementVariables{n}.relCoo,timestepsize);
        ModelF.listElementVariables{n}.acceleration = deriveVal(ModelF.listElementVariables{n}.velocity,timestepsize);
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

S = DirectTranscriptionOpti(ModelF);
S.npts = finaltime/timestepsize+1;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = timestepsize;
S.parameters.rho = rho_num;
S.nPass = nPass;
S.ConstIter = hypForComp;
% S.parameters.relTolRes = 1e-10;
S.parameters.scaling = scaling;
xSol = S.runOpti(D);



