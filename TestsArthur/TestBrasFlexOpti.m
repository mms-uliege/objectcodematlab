%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control

finaltime = 4;
timestepsize = 0.01;

nodes = [1 0 0 0;
         2 0 0 0;
         3 -0.75 -0.6614 0;
         4 -0.75 -0.6614 0;
         5 -1.125 -0.3307 0;
         6 -1.125 -0.3307 0;
         7 -1.5 0 0];
     
elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2 3];
elements{1}.mass = 6.875;
elements{1}.J = 0.57*eye(3);
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [4 5];
elements{2}.mass = 6.875/2;
elements{2}.J = 0.0723*eye(3);

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [6 7];
elements{3}.mass = 6.875/2;
elements{3}.J = 0.0723*eye(3);

elements{4}.type = 'KinematicConstraint';
elements{4}.nodes = [1 2];
elements{4}.A = [0 0 0 0 0 1]';

elements{5}.type = 'KinematicConstraint';
elements{5}.nodes = [3 4];
elements{5}.A = [0 0 0 0 0 1]';

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [5 6];
elements{6}.A = [0 0 0 0 0 1]';

elements{7}.type = 'RotSpringDamperElement';
elements{7}.damping = 1; % 1
elements{7}.stiffness = 100; % Minimum Phase 7.5
% elements{7}.stiffness = 3; % Non-Minimum Phase
elements{7}.nodes = [5 6];
elements{7}.A = [0 0 1];

% Trajectory

x_end = 1.5;
x_i = -1.5;

y_end = 0;
y_i = 0;

r = 1.5;

trajx = halfCircleTraj(x_i,x_end,r,finaltime,timestepsize,'sin',0.5,3.5);
trajy = halfCircleTraj(y_i,y_end,r,finaltime,timestepsize,'cos',.5,3.5);

elements{8}.type = 'TrajectoryConstraint';
elements{8}.nodes = [7];
elements{8}.T = [trajx;...
                 trajy];
elements{8}.Axe = [1 0 0;...
                   0 1 0];
elements{8}.elements = [4 5];
elements{8}.active = 1;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.15;
% D.parameters.relTolRes = 1e-12;
D.runIntegration();

% DeplCoude0 = Model.listElementVariables{2}.relCoo;
% u1_init = Model.listElementVariables{end}.value;
% 
% Model.listElements{8}.active = 1;
% 
% Model.listElements{7}.stiffness = 10; % to make it unstable
% Model.listElements{7}.damping = 0.25;
% 
% S = DirectTranscriptionOpti(Model);
% S.parameters.finaltime = finaltime;
% S.parameters.timestepsize = timestepsize;
% S.parameters.relTolRes = 1e-12;
% S.parameters.rho = 0.1;
% S.npts = 21;
% S.NodeToMinimize = [];
% S.JointToMinimize = [6];
% S.parameters.scaling = 1e0;
% S.runOpti();
% 
% DeplCoudeOpti = Model.listElementVariables{2}.relCoo;
% 
% % Plots
% 
% timeSteps = 0:S.parameters.finaltime/(S.npts-1):S.parameters.finaltime;
% timeLoc = zeros(S.npts,1);
% for i = 1:length(timeSteps)
%     timeLoc(i) = find(S.parameters.time>=timeSteps(i),1);
% end
% 
% figure
% hold on
% plot(Model.listNodes{4}.position(1,timeLoc),Model.listNodes{4}.position(2,timeLoc), 'Linewidth',3)
% plot(trajx,trajy, 'Linewidth',1, 'Color','r')
% grid on
% 
% 
% figure
% hold on
% plot(timeSteps,Model.listNodes{4}.position(1,timeLoc),timeSteps,Model.listNodes{4}.position(2,timeLoc))
% plot(S.parameters.time,trajx,'--',S.parameters.time,trajy,'--')
% legend('X', 'Y','Xd','Yd')
% grid on
% 
% 
% figure
% hold on
% plot(timeSteps,Model.listElementVariables{end}.value(timeLoc))
% plot(timeSteps,u1_init(timeLoc),':')
% legend('u1','u1_{init}')
% grid on
% 
% figure
% plot(timeSteps,DeplCoude0(timeLoc),timeSteps,DeplCoudeOpti(timeLoc))
% legend('Beta init','Beta Opti')
% grid on
% 

