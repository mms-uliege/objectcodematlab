%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dynamic integration test of spring-damper element on a double pendulum where the first and
% second link are mounted with spring and damper elements. The second one
% has a natural angle of pi/6, which makes the pendulum ocsillate as it is
% at an angle of 0 rad initialy.

nodes = [1 0 0 0;
         2 0 0 0;
         3 1 0 0;
         4 1 0 0;
         5 2 0 0];
     
elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [3 2];
elements{1}.mass = 1;
elements{1}.J = ones(3);

elements{10}.type = 'RigidBodyElement';
elements{10}.nodes = [5 4];
elements{10}.mass = 1;
elements{10}.J = ones(3);

elements{2}.type = 'RotSpringDamperElement';
elements{2}.damping = 1;
elements{2}.stiffness = 50;
elements{2}.natural_angle = 0;
elements{2}.nodes = [1 2];
elements{2}.A = [0 0 1];

elements{20}.type = 'RotSpringDamperElement';
elements{20}.damping = 1;
elements{20}.stiffness = 50;
elements{20}.natural_angle = pi/6;
elements{20}.nodes = [3 4];
elements{20}.A = [0 0 1];

% elements{2}.type = 'ExternalForce';
% elements{2}.nodes = 3;
% elements{2}.DOF = 2;
% elements{2}.amplitude = -10;
% elements{2}.frequency = 0;

% elements{20}.type = 'ExternalForce';
% elements{20}.nodes = 5;
% elements{20}.DOF = 2;
% elements{20}.amplitude = -10;
% elements{20}.frequency = 0;

elements{3}.type = 'KinematicConstraint';
elements{3}.nodes = [1 2];
elements{3}.A = [0 0 0 0 0 1]';
% elements{3}.k = 1e2;

elements{4}.type = 'KinematicConstraint';
elements{4}.nodes = [3 4];
elements{4}.A = [0 0 0 0 0 1]';
% elements{4}.k = 1e2;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

S = DynamicIntegration(Model);
S.parameters.finaltime = 5;
S.parameters.timestepsize = 0.01;
S.parameters.rho = 0.9;
S.runIntegration();