%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build boundary values for BVP formulation
% input parameter: - S           = DirectTranscriptionOpti class object
%                  - step        = step at which boundary condition is imposed
%                  - stability   = stable or unstable eigenspace ('stable'
%                  or 'unstable')
% output variable: - gradceq_bv  = gradient of boundary constraint on 
%                  eigenspace at time step "step"
%                  - Vs          = stable eigenvectors
%                  - Vu          = unstable eigenvectors
%                  - gradceq_vel = gradient of kinematic constraint at
%                  velocity level at time step "step"

% Arthur Lismonde, 25 october 2018

function [gradceq_bv,Vs,Vu,gradceq_vel] = BoundaryValues(S,step,stability)

if nargin < 2
    step = 1; % initial time step by default
end
if nargin < 3
    stability = 'stable'; % 'stable' eigenspace by default
end
% step = 1;

% S.LocMotionDof = S.model.listFreeDof(1:S.nCoord);

tempTime.timestep = step; % need a structure with timestep content to run the Update
% Motion Constraint Gradient
scaling = 1;
for i = S.model.listNumberElements
    S.model.listElements{i}.UpDate(tempTime);
end

K = S.model.getTangentMatrix_Opti('Stiffness',step,scaling);
K = K(S.LocMotionDof,S.LocMotionDof);
M = S.model.getTangentMatrix_Opti('Mass',step,scaling);
M = M(S.LocMotionDof,S.LocMotionDof);
C = S.model.getTangentMatrix_Opti('Damping',step,scaling);
C = C(S.LocMotionDof,S.LocMotionDof);

B = S.model.getTangentMatrix_Opti('Phiq',step,scaling);
A = B(S.LocMotionDof,S.listControls);
dServodq = B(S.listControls,S.LocMotionDof);
B = B(S.listLagrangeM,S.LocMotionDof);

E = zeros(2*S.nCoord+S.nControls+S.nLagrangeM,2*S.nCoord+S.nControls+S.nLagrangeM);
As = zeros(2*S.nCoord+S.nControls+S.nLagrangeM,2*S.nCoord+S.nControls+S.nLagrangeM);

% Like in Article
E(1:S.nCoord,1:S.nCoord) = eye(S.nCoord);
E(S.nCoord+1:2*S.nCoord,S.nCoord+1:2*S.nCoord) = M;

As(1:S.nCoord,S.nCoord+1:2*S.nCoord) = eye(S.nCoord);
As(S.nCoord+1:2*S.nCoord,1:S.nCoord) = -K;
As(S.nCoord+1:2*S.nCoord,S.nCoord+1:2*S.nCoord) = -C;
As(S.nCoord+1:2*S.nCoord,2*S.nCoord+1:2*S.nCoord+S.nControls+S.nLagrangeM) = -[B' -A];
As(2*S.nCoord+1:2*S.nCoord+S.nControls+S.nLagrangeM,1:S.nCoord) = [B; dServodq];

[V,D] = eig(As,E);
poles = diag(D);

epsilon = 1e-6;
largeNumber = 1.25*1e10;
% largeNumber = inf;
% Check stable (negative real value) and unstable poles (positive real
% value
indexStable = find(abs(poles)<largeNumber & real(poles) <= epsilon);
indexUnstable = find(abs(poles)<largeNumber & real(poles) > epsilon);
ns = length(indexStable); % number of stable poles
nu = length(indexUnstable); % number of unstable poles

Dstar = [[B; dServodq] zeros(S.nControls+S.nLagrangeM,S.nCoord);
          zeros(S.nControls+S.nLagrangeM,S.nCoord) [B; dServodq]];
% Vs = V([1:S.nCoord S.nCoord+S.nControls+S.nLagrangeM+[1:S.nCoord]],indexStable);
% Vu = V([1:S.nCoord S.nCoord+S.nControls+S.nLagrangeM+[1:S.nCoord]],indexUnstable);
Vs = V([1:2*S.nCoord],indexStable);
Vu = V([1:2*S.nCoord],indexUnstable);
% nullStable = null([Dstar; Vs']);
% nullUnstable = null([Dstar; Vu']);
nullStable = null([Vs'; Dstar]);
nullUnstable = null([Vu'; Dstar]);
if or(size(nullStable)~=[2*S.nCoord nu],size(nullUnstable)~=[2*S.nCoord ns])
    disp('Dimension problem of stable or unstable eigenspace...')
    gradceq_bv = [];gradceq_vel = [];    
    return
end

% figure
% plot(real(poles(indexUnstable)),imag(poles(indexUnstable)),'d','MarkerFaceColor','r','MarkerEdgeColor','r')
% hold on
% plot(real(poles(indexStable)),imag(poles(indexStable)),'o','MarkerFaceColor','b','MarkerEdgeColor','b')
% xlabel('Real part')
% ylabel('Imaginary part')
% title('Poles of the system ("flipped" in 1st quad)')
% legend('Unstable','Stable','Location','Best')
% grid on
    
if strcmp(stability,'stable')
    gradceq_bv = zeros(nu, 2*S.nCoord);
    gradceq_bv(1:nu,1:2*S.nCoord) = real(nullStable)';
elseif strcmp(stability,'unstable')
    gradceq_bv = zeros(ns, 2*S.nCoord);
    gradceq_bv(1:ns,1:2*S.nCoord) = real(nullUnstable)';
end

% Gradient of kinematic constraints at velocity level (m+s) for time step
% 'step'
gradceq_vel = [B; dServodq];
end



