%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test inverse dynamics of a serial manipulator (2 links, 3 dof).
% With ONLY some PD feedback on joint position level

clear
close all

computeOpti = true;
computeDirectDyn = true;

finaltime = 1.5;
timestepsize = 0.01;
t_i = 0.2;
t_f = 1.3;
    
kp1 = 100;
kd1 = 5;
kp2 = 100;
kd2 = 5;
kp3 = 100;
kd3 = 5;

hypForPreComp = 'notConstant';
hypForComp = 'notConstant';

a1 = 0.05;
b1 = a1;
l1 = 1;
l2 = l1; 
e1 = 0.01;
rapport = a1/e1;
a2 = 0.0075;
b2 = a2;
e2 = a2/rapport;
a2In = a2-2*e2;
b2In = b2-2*e2;
a1In = a1-2*e1; % inner length
b1In = b1-2*e1;

nPass = 0;

grav = [0 0 -9.81];
% grav = [0 0 0];
% t = 0:timestepsize:finaltime;
% grav = [zeros(1,length(t)); zeros(1,length(t)); [0 0 0 -9.81*ones(1,length(t)-3)]]';

m_end = 0.1; % end-effector mass

%% Creating nodes
angle = 45*pi/180;
nodes = [1 0 0 0;
         2 0 0 0;
         3 0 0 0;
         4 l1*cos(angle) 0 l1*sin(angle);
         5 l1*cos(angle) 0 l1*sin(angle);
         6 l1*cos(angle)+l2*cos(angle) 0 0];
     
nElem1 = 2;
Start1 = 3;
End1 = 4;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

nElem2 = 4;
Start2 = End1+nElem1;
End2 = Start2+1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);
 
%% Rigid Model

rho = 2700;
m1 = (a1*b1-a1In*b1In)*l1*rho;
% IxxR1 = m1*(a1^2+b1^2)/12;
IxxR1 = m1*(a1^2+b1^2-a1In^2-b1In^2)/12;
IyyR1 = m1*(a1^2+l1^2)/12;
IzzR1 = m1*(b1^2+l1^2)/12;

m2 = (a2*b2-a2In*b2In)*l2*rho;
IxxR2 = m2*(a2^2+b2^2-a2In^2-b2In^2)/12;
IyyR2 = m2*(a2^2+l2^2)/12;
IzzR2 = m2*(b2^2+l2^2)/12;

count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elements{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR1 IyyR1 IzzR1]);
elements{count}.g = grav;
count = count +1;

% if computeOpti == true
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
% elements{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
% elements{count}.J = diag([IxxR2 IyyR2 IzzR2]);
% elements{count}.g = grav;
% end

E = 70e9; nu = 0.3; G = E/(2*(1+nu));
% if computeOpti ~= true
% A2 = a2*b2; Ixx2 = a2*b2*(a2^2+b2^2)/12; Iyy2 = b2*a2^3/12;Izz2 = a2*b2^3/12;
A2 = a2*b2-a2In*b2In; Ixx2 = (a2*b2*(a2^2+b2^2)-a2In*b2In*(a2In^2+b2In^2))/12; Iyy2 = (b2*a2^3-b2In*a2In^3)/12;Izz2 = (a2*b2^3-a2In*b2In^3)/12;

KCS2 = diag([E*A2 G*A2 G*A2 G*Ixx2 E*Iyy2 E*Izz2]);
MCS2 = diag(rho*[A2 A2 A2 Ixx2 Iyy2 Izz2]);

type = 'FlexibleBeamElement';
count = size(elements,2);
for i = 1:nElem2
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
    elements{count+i}.KCS = KCS2;
    elements{count+i}.MCS = MCS2;
    elements{count+i}.g = grav;
end
% end

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = m_end;
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.A = [0 0 0 0 1 0]';
count = count+1;

% Trajectory

y_end = l2;
z_end = nodes(end,4);
r = y_end/2;

timeVector = 0:timestepsize:finaltime;
trajx = nodes(end,2)*ones(size(timeVector));
trajy = halfCircleTraj(nodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);


elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajy;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elements{count}.elements = [count-3 count-2 count-1];
elements{count}.active = 1;

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

% D = DynamicIntegration(ModelR);
D = StaticIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
% D.parameters.rho = 0.01;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

uRigi.u1 = ModelR.listElementVariables{end}.value(1,:);
uRigi.u2 = ModelR.listElementVariables{end}.value(2,:);
uRigi.u3 = ModelR.listElementVariables{end}.value(3,:);
uRigi.jointPos1 = ModelR.listElementVariables{1}.relCoo;
uRigi.jointPos2 = ModelR.listElementVariables{2}.relCoo;
uRigi.jointPos3 = ModelR.listElementVariables{3}.relCoo;
uRigi.jointVel1 = ModelR.listElementVariables{1}.velocity;
uRigi.jointVel2 = ModelR.listElementVariables{2}.velocity;
uRigi.jointVel3 = ModelR.listElementVariables{3}.velocity;
uRigi.time = D.parameters.time;

%% Flexible Model
if computeOpti == true
% Solving
npts = finaltime/timestepsize + 1;
ModelF = FEModel();
ModelF.CreateFEModel(nodes,elements);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from rigid solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
%     ModelF.listNodes{n}.velocity = zeros(6,D.parameters.nstep+1);
%     ModelF.listNodes{n}.acceleration = zeros(6,D.parameters.nstep+1);
    dh = zeros(6,npts);
    for i = 1:npts
        dh(:,i) = logSE3([dimR(ModelF.listNodes{n}.R(:,i)) ModelF.listNodes{n}.position(:,i); 0 0 0 1]);
    end
    ModelF.listNodes{n}.velocity = deriveVal(dh,timestepsize);
    ModelF.listNodes{n}.acceleration = deriveVal(dh,timestepsize);
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        % added because in static first computation, there were no velocity
        % nor acceleration, with initialize them to zero first (? check if
        % this works)
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
%         ModelF.listElementVariables{n}.velocity = zeros(ModelR.listElementVariables{n}.nDof,D.parameters.nstep+1);
        ModelF.listElementVariables{n}.velocity = deriveVal(ModelF.listElementVariables{n}.relCoo,timestepsize);
%         ModelF.listElementVariables{n}.acceleration = zeros(ModelR.listElementVariables{n}.nDof,D.parameters.nstep+1);
        ModelF.listElementVariables{n}.acceleration = deriveVal(ModelF.listElementVariables{n}.velocity,timestepsize);
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end
% 
% % First part optimization to compute the equilibrium solution for the first
% % preactuation phase. Then copy this preactuation phase, to have full initial guess equilibrium to compute full
% % optimization.
% tic
% % EndPreActStep = t_i/(finaltime/(npts-1))+1;
% % EndPreActStep = round((t_i/(finaltime/(npts-1))+1)*0.47); 
% EndPreTime = t_i*0.47;%0.75
% EndPreActStep = round(EndPreTime/(finaltime/(npts-1))+1);
% S = DirectTranscriptionOpti(ModelF);
% S.parameters.rho = 0.01;
% S.npts = EndPreActStep+1;
% S.parameters.finaltime = EndPreTime;
% S.parameters.timestepsize = finaltime/(npts-1);
% S.parameters.scaling = 1e6;
% S.linConst = false;
% S.ConstIter = hypForPreComp;
% xSol = S.runOpti(D);
% 
% % Copy the state at the end of preactuation phase to start as new initial
% % guess.
% % Redefine the trajectory that was cut to only the preactuation phase
% ModelF.listElements{end}.T = [trajx;...
%                               trajy;...
%                               trajz];
% % To have a smooth position, vel and acc, during the first time steps,
% % search for the moment the position of the system is equal to the equilibrium point of the flex system.
% % We then set the system at this point as initial guess.
% tolEqui = 1e-3; % tolerence at which we consider that the flexible system is at equilibrium with respect to the static equilibrium in the preactuation phase
% for n = ModelF.listNumberNodes
%     for c = (EndPreActStep+1):size(ModelF.listNodes{n}.position,2)
%         if norm(ModelF.listNodes{n}.position(:,c)-ModelF.listNodes{n}.position(:,EndPreActStep))<tolEqui
%             Step2 = c; % Copy the values until the time step where we are at the same position... not sure it actually works... supress this 
%             break
%         end
%     end
%     for step = EndPreActStep+1:Step2
%         ModelF.listNodes{n}.R_InitOpti(:,step) = ModelF.listNodes{n}.R(:,EndPreActStep);
%         ModelF.listNodes{n}.position_InitOpti(:,step) = ModelF.listNodes{n}.position(:,EndPreActStep);
%         ModelF.listNodes{n}.velocity_InitOpti(:,step) = ModelF.listNodes{n}.velocity(:,EndPreActStep);
%         ModelF.listNodes{n}.acceleration_InitOpti(:,step) = ModelF.listNodes{n}.acceleration(:,EndPreActStep);
%     end
% %     ModelF.listNodes{n}.InitializeD_Opti();
% end
% for n = ModelF.listNumberElementVariables
%     if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
%         for c = (EndPreActStep+1):size(ModelF.listElementVariables{n}.relCoo,2)
%             if norm(ModelF.listElementVariables{n}.relCoo(:,c)-ModelF.listElementVariables{n}.relCoo(:,EndPreActStep))<tolEqui
%                 Step2 = c;
%                 break
%             end
%         end
%         for step =  EndPreActStep+1:Step2
%             ModelF.listElementVariables{n}.R_InitOpti(:,step) = ModelF.listElementVariables{n}.R(:,EndPreActStep);
%             ModelF.listElementVariables{n}.x_InitOpti(:,step) = ModelF.listElementVariables{n}.x(:,EndPreActStep);
%             ModelF.listElementVariables{n}.velocity_InitOpti(:,step) = ModelF.listElementVariables{n}.velocity(:,EndPreActStep);
%             ModelF.listElementVariables{n}.acceleration_InitOpti(:,step) = ModelF.listElementVariables{n}.acceleration(:,EndPreActStep);
%             ModelF.listElementVariables{n}.relCoo_InitOpti(:,step) = ModelF.listElementVariables{n}.relCoo(:,EndPreActStep);
%         end
%     else
%         for c = (EndPreActStep+1):size(ModelF.listElementVariables{n}.value,2)
%             if norm(ModelF.listElementVariables{n}.value(:,c)-ModelF.listElementVariables{n}.value(:,EndPreActStep))<tolEqui
%                 Step2 = c;
%                 break
%             end
%         end
%         for step =  EndPreActStep+1:Step2
%             ModelF.listElementVariables{n}.value_InitOpti(:,step) = ModelF.listElementVariables{n}.value(:,EndPreActStep); 
%         end
%     end
% %     ModelF.listElementVariables{n}.InitializeD_Opti();
% end

% Start second full optimization with newly computed initial guess
tic
S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = 0.01;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.parameters.scaling = 1e6;
S.linConst = false;
S.ConstIter = hypForComp;
S.nPass = nPass;
% analys = S.runPoleAnalysisOpti(D)
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])

%% Plots

timeSteps = S.timeValues;
timeLoc = S.timesteps;

uBeam.u1 = ModelF.listElementVariables{end}.value(1,timeLoc);
uBeam.u2 = ModelF.listElementVariables{end}.value(2,timeLoc);
uBeam.u3 = ModelF.listElementVariables{end}.value(3,timeLoc);
uBeam.jointPos1 = S.model.listElementVariables{1}.relCoo;
uBeam.jointPos2 = S.model.listElementVariables{2}.relCoo;
uBeam.jointPos3 = S.model.listElementVariables{3}.relCoo;
uBeam.jointVel1 = S.model.listElementVariables{1}.velocity;
uBeam.jointVel2 = S.model.listElementVariables{2}.velocity;
uBeam.jointVel3 = S.model.listElementVariables{3}.velocity;
uBeam.time = S.timeValues;

% save('uExperiment3D','uBeam')

endNode = nodes(end,1);
% figure
% hold on
% plot3(ModelF.listNodes{endNode}.position(1,timeLoc),ModelF.listNodes{endNode}.position(2,timeLoc),ModelF.listNodes{endNode}.position(3,timeLoc), 'Linewidth',3)
% plot3(trajx,trajy,trajz, 'Linewidth',1, 'Color','r')
% grid on

% figure
% hold on
% plot(timeSteps,ModelF.listNodes{endNode}.position(1,timeLoc),timeSteps,ModelF.listNodes{endNode}.position(2,timeLoc),timeSteps,ModelF.listNodes{endNode}.position(3,timeLoc))
% plot(timeSteps,trajx,'--',timeSteps,trajy,'--',timeSteps,trajz,'--')
% legend('X', 'Y', 'Z','Xd','Yd','Zd')
% grid on

% figure
% hold on
% plot(timeSteps,uBeam.u1,timeSteps,uBeam.u2,timeSteps,uBeam.u3,'Linewidth',2)
% plot(uRigi.time,uRigi.u1,':',uRigi.time,uRigi.u2,':',uRigi.time,uRigi.u3,':','Linewidth',2)
% % plot(timeSteps,u1_init(timeLoc),':',timeSteps,u2_init(timeLoc),':',timeSteps,u3_init(timeLoc),':','Linewidth',2)
% xlabel('Time (s)','Fontsize',16)
% ylabel('Commands','Fontsize',16)
% legend('u1','u2','u3','u1_{rigid}','u2_{rigid}','u3_{rigid}','Location', 'Best')
% title('Commands of an experimental Robot','Fontsize',18)
% grid on

joint1_init = ModelR.listElementVariables{1};
joint2_init = ModelR.listElementVariables{2};
joint3_init = ModelR.listElementVariables{3};

joint1 = ModelF.listElementVariables{1};
joint2 = ModelF.listElementVariables{2};
joint3 = ModelF.listElementVariables{3};

figure
hold on
plot(timeSteps,joint1.relCoo(timeLoc),timeSteps,joint2.relCoo(timeLoc),timeSteps,joint3.relCoo(timeLoc),'Linewidth',2)
plot(D.parameters.time,joint1_init.relCoo,'--',D.parameters.time,joint2_init.relCoo,'--',D.parameters.time,joint3_init.relCoo,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints angle (rad)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% title('Joints angle of a flexible Robot','Fontsize',18)
grid on
% 
% figure
% hold on
% plot(timeSteps,joint1.velocity(timeLoc),timeSteps,joint2.velocity(timeLoc),timeSteps,joint3.velocity(timeLoc),'Linewidth',2)
% plot(D.parameters.time,joint1_init.velocity,'--',D.parameters.time,joint2_init.velocity,'--',D.parameters.time,joint3_init.velocity,'--','Linewidth',2)
% xlabel('Time (s)','Fontsize',16)
% ylabel('Joints velocity (rad/s)','Fontsize',16)
% legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% title('Joints velocity','Fontsize',18)
% grid on

% figure
% hold on
% plot(timeSteps,joint1.acceleration(timeLoc),timeSteps,joint2.acceleration(timeLoc),timeSteps,joint3.acceleration(timeLoc),'Linewidth',2)
% plot(D.parameters.time,joint1_init.acceleration,'--',D.parameters.time,joint2_init.acceleration,'--',D.parameters.time,joint3_init.acceleration,'--','Linewidth',2)
% xlabel('Time (s)','Fontsize',16)
% ylabel('Joints accelerations (rad/s^2)','Fontsize',16)
% legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% title('Joints accelerations','Fontsize',18)
% grid on

anl = poleAnalysisOpti(S)

end

if computeDirectDyn == true

    clear elementsDirDyn
    newNodes = nodes;
%     for l = 1:size(newNodes,1)
%         newNodes(l,2:4) = ModelF.listNodes{l}.position(:,1)';
%         for c = 2:size(newNodes,2)
%             if newNodes(l,c)<= 1e-6
%                 newNodes(l,c)=0;
%             end
%         end
%     end
%     figure
%     hold on
%     plot3(newNodes(:,2),newNodes(:,3),newNodes(:,4),'*r')
%     plot3(nodes(:,2),nodes(:,3),nodes(:,4),'ob')
    
    count = 1;
    elementsDirDyn{count}.type = 'RigidBodyElement';
    elementsDirDyn{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
    elementsDirDyn{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
    elementsDirDyn{count}.J = diag([IxxR1 IyyR1 IzzR1]);
    elementsDirDyn{count}.g = grav;
    
%     count = size(elementsDirDyn,2)+1;
%     elementsDirDyn{count}.type = 'RigidBodyElement';
%     elementsDirDyn{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
%     elementsDirDyn{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
%     elementsDirDyn{count}.J = diag([IxxR2 IyyR2 IzzR2]);
%     elementsDirDyn{count}.g = grav;

    type = 'FlexibleBeamElement';
    count = size(elementsDirDyn,2);
    for i = 1:nElem2
        elementsDirDyn{count+i}.type = type;
        elementsDirDyn{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
        elementsDirDyn{count+i}.KCS = KCS2;
        elementsDirDyn{count+i}.MCS = MCS2;
        elementsDirDyn{count+i}.g = grav;
    end

    count = size(elementsDirDyn,2)+1;
    elementsDirDyn{count}.type = 'RigidBodyElement';
    elementsDirDyn{count}.nodes = [nodes(end,1)];
    elementsDirDyn{count}.mass = m_end;
    elementsDirDyn{count}.g = grav;

    count = size(elementsDirDyn,2)+1;
    elementsDirDyn{count}.type = 'KinematicConstraint';
    elementsDirDyn{count}.nodes = [1 2];
    elementsDirDyn{count}.A = [0 0 0 0 0 1]';

    count = size(elementsDirDyn,2)+1;
    elementsDirDyn{count}.type = 'KinematicConstraint';
    elementsDirDyn{count}.nodes = [2 3];
    elementsDirDyn{count}.A = [0 0 0 0 1 0]';
    count = count+1;

    elementsDirDyn{count}.type = 'KinematicConstraint';
    elementsDirDyn{count}.nodes = [Start2-1 Start2];
    elementsDirDyn{count}.A = [0 0 0 0 1 0]';
    count = count+1;

    % Trajectory

    y_end = l2;
    z_end = newNodes(end,4);
    r = y_end/2;

    timeVector = 0:timestepsize:finaltime;
    trajx = newNodes(end,2)*ones(size(timeVector));
    trajy = halfCircleTraj(newNodes(end,3),y_end,r,timeVector,'sin',t_i,t_f);
    trajz = halfCircleTraj(newNodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

%     load('uRigiExperiment3D')
    u1_init = interp1(uRigi.time,uRigi.u1,timeVector,'linear');
    u2_init = interp1(uRigi.time,uRigi.u2,timeVector,'linear');
    u3_init = interp1(uRigi.time,uRigi.u3,timeVector,'linear');

%     load('uExperiment3D')
    u1 = interp1(uBeam.time,uBeam.u1,timeVector,'linear');
    u2 = interp1(uBeam.time,uBeam.u2,timeVector,'linear');
    u3 = interp1(uBeam.time,uBeam.u3,timeVector,'linear');

    % u1(1:5) = u1(12)*ones(1,5);
    % u2(1:5) = u2(12)*ones(1,5);
    % u3(1:5) = u3(12)*ones(1,5);

%     elementsDirDyn{count}.type = 'ForceInKinematicConstraint';
%     elementsDirDyn{count}.elements = [count-3];
%     elementsDirDyn{count}.f = u1;
%     count = count+1;
% 
%     elementsDirDyn{count}.type = 'ForceInKinematicConstraint';
%     elementsDirDyn{count}.elements = [count-3];
%     elementsDirDyn{count}.f = u2;
%     count = count+1;
% 
%     elementsDirDyn{count}.type = 'ForceInKinematicConstraint';
%     elementsDirDyn{count}.elements = [count-3];
%     elementsDirDyn{count}.f = u3;
    
    count = size(elementsDirDyn,2)+1;
    elementsDirDyn{count}.type = 'ForceInKinematicConstraintPD'; % try with PD control
    elementsDirDyn{count}.elements = [count-3];
    elementsDirDyn{count}.ref = uBeam.jointPos1;
    elementsDirDyn{count}.dref = uBeam.jointVel1;
    elementsDirDyn{count}.kp = kp1;
    elementsDirDyn{count}.kd = kd1;
    elementsDirDyn{count}.f = u1;

    count = size(elementsDirDyn,2)+1;
    elementsDirDyn{count}.type = 'ForceInKinematicConstraintPD'; % try with PD control
    elementsDirDyn{count}.elements = [count-3];
    elementsDirDyn{count}.ref = uBeam.jointPos2;
    elementsDirDyn{count}.dref = uBeam.jointVel2;
    elementsDirDyn{count}.kp = kp2;
    elementsDirDyn{count}.kd = kd2;
    elementsDirDyn{count}.f = u2;

    count = size(elementsDirDyn,2)+1;
    elementsDirDyn{count}.type = 'ForceInKinematicConstraintPD'; % try with PD control
    elementsDirDyn{count}.elements = [count-3];
    elementsDirDyn{count}.ref = uBeam.jointPos3;
    elementsDirDyn{count}.dref = uBeam.jointVel3;
    elementsDirDyn{count}.kp = kp3;
    elementsDirDyn{count}.kd = kd3;
    elementsDirDyn{count}.f = u3;

    % Boundary Condition
    BC = [1];

    % Model Def
    ModelFD = FEModel();% Flex model
    ModelFD.CreateFEModel(newNodes,elementsDirDyn);
    ModelFD.defineBC(BC);

    ModelRD = FEModel();% Rigid model
    ModelRD.CreateFEModel(newNodes,elementsDirDyn);
    ModelRD.defineBC(BC);

    % Solving Flexible
    DF = DynamicIntegration(ModelFD);
    DF.parameters.finaltime = finaltime;
    DF.parameters.timestepsize = timestepsize;
    DF.parameters.rho = 0.01;
    DF.parameters.scaling = 1e6;
    DF.runIntegration();

    % Solving Rigid
    ModelRD.listElements{count-2}.f = u1_init;
    ModelRD.listElements{count-2}.ref = uRigi.jointPos3;
    ModelRD.listElements{count-2}.dref = uRigi.jointVel3;
    ModelRD.listElements{count-1}.f = u2_init;
    ModelRD.listElements{count-1}.ref = uRigi.jointPos3;
    ModelRD.listElements{count-1}.dref = uRigi.jointVel3;
    ModelRD.listElements{count}.f = u3_init;
    ModelRD.listElements{count}.ref = uRigi.jointPos3;
    ModelRD.listElements{count}.dref = uRigi.jointVel3;

    DR = DynamicIntegration(ModelRD);
    DR.parameters.finaltime = finaltime;
    DR.parameters.timestepsize = timestepsize;
    DR.parameters.rho = 0.01;
    DR.parameters.scaling = 1e6;
    DR.runIntegration();

    %% Plots

    x = ModelFD.listNodes{end}.position(1,:);
    y = ModelFD.listNodes{end}.position(2,:);
    z = ModelFD.listNodes{end}.position(3,:);

    xR = ModelRD.listNodes{end}.position(1,:);
    yR = ModelRD.listNodes{end}.position(2,:);
    zR = ModelRD.listNodes{end}.position(3,:);

    figure
    hold on
    plot3(x,y,z, '-o','Linewidth',2, 'Color','r')
    plot3(xR,yR,zR,'--', 'Linewidth',2, 'Color','b')
    plot3(trajx,trajy,trajz, 'Linewidth',2, 'Color','k')
    legend('With u','With u_{rigid}','Prescribed')
    xlabel('X [m]','Fontsize',16)
    ylabel('Y [m]','Fontsize',16)
    zlabel('Z [m]','Fontsize',16)
    % title('Trajectory of flexible arm','Fontsize',18)
    grid on

    figure
    hold on
    plot(timeVector,x,timeVector,y,timeVector,z)
    plot(timeVector,trajx,'--',timeVector,trajy,'--',timeVector,trajz,'--')
    legend('X', 'Y', 'Z','Xd','Yd','Zd')
    grid on

    joint1_init = ModelRD.listElementVariables{1};
    joint2_init = ModelRD.listElementVariables{2};
    joint3_init = ModelRD.listElementVariables{3};

    joint1 = ModelFD.listElementVariables{1};
    joint2 = ModelFD.listElementVariables{2};
    joint3 = ModelFD.listElementVariables{3};

    figure
    hold on
    plot(timeVector,joint1.relCoo,timeVector,joint2.relCoo,timeVector,joint3.relCoo,'Linewidth',2)
    plot(timeVector,joint1_init.relCoo,'--',timeVector,joint2_init.relCoo,'--',timeVector,joint3_init.relCoo,'--','Linewidth',2)
    xlabel('Time (s)','Fontsize',16)
    ylabel('Joints angle (rad)','Fontsize',16)
    legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
    % title('Joints angle of a flexible Robot','Fontsize',18)
    grid on

    figure
    hold on
    plot(timeVector,u1,timeVector,u2,timeVector,u3,'Linewidth',2)
    plot(timeVector,u1_init(:),'--',timeVector,u2_init(:),'--',timeVector,u3_init(:),'--','Linewidth',2)
    xlabel('Time [s]','Fontsize',16)
    ylabel('Commands [Nm]','Fontsize',16)
    legend('u_1','u_2','u_3','u_{1,rigid}','u_{2,rigid}','u_{3,rigid}','Location','Best')
    % title('Commands of a flexible arm system','Fontsize',18)
    grid on

    RelError = zeros(size(timeVector));
    RelErrorR = zeros(size(timeVector));
    for i = 1:length(RelError)
        RelError(i) = 100*(norm([trajx(i)-x(i) trajy(i)-y(i) trajz(i)-z(i)])/norm([trajx(i) trajy(i) trajz(i)]));
        RelErrorR(i) = 100*(norm([trajx(i)-xR(i) trajy(i)-yR(i) trajz(i)-zR(i)])/norm([trajx(i) trajy(i) trajz(i)]));
    end
%     figure
%     plot(timeVector,RelError,timeVector,RelErrorR)
%     title('Relative error of the direct dynamic trajectory','Fontsize',13)
%     xlabel('Time')
%     ylabel('Relative Error (%)')
%     legend('Flex','Rigid')
%     grid on

    RMSRelError = sqrt(mean(RelError.^2))
    MaxRelError = max(RelError)
    RMSRelErrorR = sqrt(mean(RelErrorR.^2))
    MaxRelErrorR = max(RelErrorR)

    figure
    hold on
    plot(timeVector,joint1.velocity,timeVector,joint2.velocity,timeVector,joint3.velocity,'Linewidth',2)
    plot(timeVector,joint1_init.velocity,'--',timeVector,joint2_init.velocity,'--',timeVector,joint3_init.velocity,'--','Linewidth',2)
    xlabel('Time (s)','Fontsize',16)
    ylabel('Joints velocity (rad/s)','Fontsize',16)
    legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid')
    title('Joints velocity','Fontsize',18)
    grid on

    figure
    hold on
    plot(timeVector,joint1.acceleration,timeVector,joint2.acceleration,timeVector,joint3.acceleration,'Linewidth',2)
    plot(timeVector,joint1_init.acceleration,'--',timeVector,joint2_init.acceleration,'--',timeVector,joint3_init.acceleration,'--','Linewidth',2)
    xlabel('Time (s)','Fontsize',16)
    ylabel('Joints accelerations (rad/s^2)','Fontsize',16)
    legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid')
    title('Joints accelerations','Fontsize',18)
    grid on
    
    
end

