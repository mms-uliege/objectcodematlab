%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% linearCrank mechanism

clear
close all

finaltime = 1;
timestepsize = 0.01;
npts = finaltime/timestepsize + 1;
nPass = 0;
t_i = 0.2;
t_f = 0.8;

a1 = 0.02;
b1 = a1;
l1 = 1;
l2 = l1;
e1 = 0.002;
rapport = a1/e1;
a2 = 0.02;
b2 = a2;
e2 = a2/rapport;
a2In = a2-2*e2;
b2In = b2-2*e2;
a1In = a1-2*e1; % inner length
b1In = b1-2*e1;
angle = 45*pi/180;
mass = 0.5;

grav = [0 0 -0];

nodes = [1 0 0 0;
         2 0 0 0;
         3 l1*cos(angle) 0 l1*sin(angle);
         4 l1*cos(angle) 0 l1*sin(angle);
         5 l1*cos(angle)+l2*cos(angle) 0 0];
     
nElem1 = 2;
Start1 = 2;
End1 = 3;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

nElem2 = 2;
Start2 = End1+nElem1;
End2 = Start2+1;
nodes = createInterNodes(nodes,nElem2,Start2,End2);

nodes = doubleNode(nodes,nodes(end,1));

%% Rigid Model

rho = 2700;
m1 = (a1*b1-a1In*b1In)*l1*rho;
% IxxR1 = m1*(a1^2+b1^2)/12;
IxxR1 = m1*(a1^2+b1^2-a1In^2-b1In^2)/12;
IyyR1 = m1*(a1^2+l1^2)/12;
IzzR1 = m1*(b1^2+l1^2)/12;

m2 = (a2*b2-a2In*b2In)*l2*rho;
IxxR2 = m2*(a2^2+b2^2-a2In^2-b2In^2)/12;
IyyR2 = m2*(a2^2+l2^2)/12;
IzzR2 = m2*(b2^2+l2^2)/12;

count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elements{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR1 IyyR1 IzzR1]);
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
elements{count}.mass = m2; % 0.01*0.002*0.4*2700 = 0.0216
elements{count}.J = diag([IxxR2 IyyR2 IzzR2]);
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = mass;
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 Start1];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start1+nElem1 Start2];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [nodes(end-1,1) nodes(end,1)];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [nodes(end,1) 1];
elements{count}.A = [1 0 0 0 0 0]';

% Trajectory
r=l1/2;
x_end = nodes(end,2);

timeVector = 0:timestepsize:finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'cos',t_i,t_f);
count = count+1;
elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx];
elements{count}.Axe = [1 0 0];
elements{count}.elements = [count-4];
elements{count}.active = 1;

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.1;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e3;
D.runIntegration();

% endPoint = ModelR.listNodes{end};
% figure
% plot(D.parameters.time,endPoint.position(1,:),D.parameters.time,endPoint.position(2,:),D.parameters.time,endPoint.position(3,:))
% legend('x','y','z')
% 
% command = ModelR.listElementVariables{end};
% figure
% plot(D.parameters.time,command.value(1,:))
% legend('torque')
% 
% commandJoint = ModelR.listElementVariables{1};
% figure
% plot(D.parameters.time,commandJoint.relCoo(1,:))
% legend('Command Angle')

%% Flexible model

count = 1;
elementsFlex{count}.type = 'RigidBodyElement';
elementsFlex{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elementsFlex{count}.mass = m1; % 0.01*0.002*0.4*2700 = 0.0216
elementsFlex{count}.J = diag([IxxR1 IyyR1 IzzR1]);
elementsFlex{count}.g = grav;

E = 70e9; nu = 0.3; G = E/(2*(1+nu));
% A2 = a2*b2; Ixx2 = a2*b2*(a2^2+b2^2)/12; Iyy2 = b2*a2^3/12;Izz2 = a2*b2^3/12;
A2 = a2*b2-a2In*b2In; Ixx2 = (a2*b2*(a2^2+b2^2)-a2In*b2In*(a2In^2+b2In^2))/12; Iyy2 = (b2*a2^3-b2In*a2In^3)/12;Izz2 = (a2*b2^3-a2In*b2In^3)/12;

KCS2 = diag([E*A2 G*A2 G*A2 G*Ixx2 E*Iyy2 E*Izz2]);
MCS2 = diag(rho*[A2 A2 A2 Ixx2 Iyy2 Izz2]);

type = 'FlexibleBeamElement';
count = size(elementsFlex,2);
for i = 1:nElem2
    elementsFlex{count+i}.type = type;
    elementsFlex{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
    elementsFlex{count+i}.KCS = KCS2;
    elementsFlex{count+i}.MCS = MCS2;
    elementsFlex{count+i}.g = grav;
end

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'RigidBodyElement';
elementsFlex{count}.nodes = [nodes(end,1)];
elementsFlex{count}.mass = mass;
elementsFlex{count}.g = grav;

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [1 Start1];
elementsFlex{count}.A = [0 0 0 0 1 0]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [Start1+nElem1 Start2];
elementsFlex{count}.A = [0 0 0 0 1 0]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [nodes(end-1,1) nodes(end,1)];
elementsFlex{count}.A = [0 0 0 0 1 0]';

count = size(elementsFlex,2)+1;
elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [nodes(end,1) 1];
elementsFlex{count}.A = [1 0 0 0 0 0]';

% Trajectory
r=l1/2;
x_end = nodes(end,2);

timeVector = 0:timestepsize:finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'cos',t_i,t_f);
count = count+1;
elementsFlex{count}.type = 'TrajectoryConstraint';
elementsFlex{count}.nodes = [nodes(end,1)];
elementsFlex{count}.T = [trajx];
elementsFlex{count}.Axe = [1 0 0];
elementsFlex{count}.elements = [count-4];
elementsFlex{count}.active = 1;

% Solving
ModelF = FEModel();
ModelF.CreateFEModel(nodes,elementsFlex);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from rigid solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

% First part optimization to compute the equilibrium solution for the first
% preactuation phase. Then copy this preactuation phase, to have full initial guess equilibrium to compute full
% optimization.
tic
% EndPreActStep = t_i/(finaltime/(npts-1))+1;
% EndPreActStep = round((t_i/(finaltime/(npts-1))+1)*0.47);
EndPreTime = t_i*0.5;
EndPreActStep = round(EndPreTime/(finaltime/(npts-1))+1);
S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = 0.1;
S.npts = EndPreActStep+1;
S.parameters.finaltime = EndPreTime;
S.parameters.timestepsize = finaltime/(npts-1);
S.parameters.scaling = 1e3;
S.linConst = false;
S.ConstIter = 'notConstant';
S.nPass = 4;
xSol = S.runOpti(D);

% Copy the state at the end of preactuation phase to start as new initial
% guess.
% Redefine the trajectory that was cut to only the preactuation phase
ModelF.listElements{end}.T = [trajx];
% To have a smooth position, vel and acc, during the first time steps,
% search for the moment the position of the system is equal to the equilibrium point of the flex system.
% We then set the system at this point as initial guess.
tolEqui = 1e-4; % tolerence at which we consider that the flexible system is at equilibrium with respect to the static equilibrium in the preactuation phase
for n = ModelF.listNumberNodes
    for c = (EndPreActStep+1):size(ModelF.listNodes{n}.position,2)
        if norm(ModelF.listNodes{n}.position(:,c)-ModelF.listNodes{n}.position(:,EndPreActStep))<tolEqui
            Step2 = c; % Copy the values until the time step where we are at the same position... not sure it actually works... supress this 
            break
        end
    end
    for step = EndPreActStep+1:Step2
        ModelF.listNodes{n}.R_InitOpti(:,step) = ModelF.listNodes{n}.R(:,EndPreActStep);
        ModelF.listNodes{n}.position_InitOpti(:,step) = ModelF.listNodes{n}.position(:,EndPreActStep);
        ModelF.listNodes{n}.velocity_InitOpti(:,step) = ModelF.listNodes{n}.velocity(:,EndPreActStep);
        ModelF.listNodes{n}.acceleration_InitOpti(:,step) = ModelF.listNodes{n}.acceleration(:,EndPreActStep);
    end
%     ModelF.listNodes{n}.InitializeD_Opti();
end
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        for c = (EndPreActStep+1):size(ModelF.listElementVariables{n}.relCoo,2)
            if norm(ModelF.listElementVariables{n}.relCoo(:,c)-ModelF.listElementVariables{n}.relCoo(:,EndPreActStep))<tolEqui
                Step2 = c;
                break
            end
        end
        for step =  EndPreActStep+1:Step2
            ModelF.listElementVariables{n}.R_InitOpti(:,step) = ModelF.listElementVariables{n}.R(:,EndPreActStep);
            ModelF.listElementVariables{n}.x_InitOpti(:,step) = ModelF.listElementVariables{n}.x(:,EndPreActStep);
            ModelF.listElementVariables{n}.velocity_InitOpti(:,step) = ModelF.listElementVariables{n}.velocity(:,EndPreActStep);
            ModelF.listElementVariables{n}.acceleration_InitOpti(:,step) = ModelF.listElementVariables{n}.acceleration(:,EndPreActStep);
            ModelF.listElementVariables{n}.relCoo_InitOpti(:,step) = ModelF.listElementVariables{n}.relCoo(:,EndPreActStep);
        end
    else
        for c = (EndPreActStep+1):size(ModelF.listElementVariables{n}.value,2)
            if norm(ModelF.listElementVariables{n}.value(:,c)-ModelF.listElementVariables{n}.value(:,EndPreActStep))<tolEqui
                Step2 = c;
                break
            end
        end
        for step =  EndPreActStep+1:Step2
            ModelF.listElementVariables{n}.value_InitOpti(:,step) = ModelF.listElementVariables{n}.value(:,EndPreActStep); 
        end
    end
%     ModelF.listElementVariables{n}.InitializeD_Opti();
end

% Start second full optimization with newly computed initial guess

S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = 0.1;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.parameters.scaling = 1e3;
S.linConst = false;
S.ConstIter = 'constant';
S.nPass = nPass;
% analys = S.runPoleAnalysisOpti(D)
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])

analys = poleAnalysisOpti(S)

%% Direct analysis

newNodes = nodes;

count = 1;
elementsDirDyn{count}.type = 'RigidBodyElement';
elementsDirDyn{count}.nodes = [Start1+nElem1/2 Start1+[0:nElem1/2-1] Start1+nElem1/2+1+[0:nElem1/2-1]];
elementsDirDyn{count}.mass = m1;
elementsDirDyn{count}.J = diag([IxxR1 IyyR1 IzzR1]);
elementsDirDyn{count}.g = grav;

E = 70e9; nu = 0.3; G = E/(2*(1+nu));
% A2 = a2*b2; Ixx2 = a2*b2*(a2^2+b2^2)/12; Iyy2 = b2*a2^3/12;Izz2 = a2*b2^3/12;
A2 = a2*b2-a2In*b2In; Ixx2 = (a2*b2*(a2^2+b2^2)-a2In*b2In*(a2In^2+b2In^2))/12; Iyy2 = (b2*a2^3-b2In*a2In^3)/12;Izz2 = (a2*b2^3-a2In*b2In^3)/12;

KCS2 = diag([E*A2 G*A2 G*A2 G*Ixx2 E*Iyy2 E*Izz2]);
MCS2 = diag(rho*[A2 A2 A2 Ixx2 Iyy2 Izz2]);

type = 'FlexibleBeamElement';
count = size(elementsDirDyn,2)+1;
% for i = 1:nElem2
%     elementsDirDyn{count+i}.type = type;
%     elementsDirDyn{count+i}.nodes = [nodes(Start2+i-1,1) nodes(Start2+i,1)];
%     elementsDirDyn{count+i}.KCS = KCS2;
%     elementsDirDyn{count+i}.MCS = MCS2;
%     elementsDirDyn{count+i}.g = grav;
% end
elementsDirDyn{count}.type = 'RigidBodyElement';
elementsDirDyn{count}.nodes = [Start2+nElem2/2 Start2+[0:nElem2/2-1] Start2+nElem2/2+1+[0:nElem2/2-1]];
elementsDirDyn{count}.mass = m2;
elementsDirDyn{count}.J = diag([IxxR2 IyyR2 IzzR2]);
elementsDirDyn{count}.g = grav;

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'RigidBodyElement';
elementsDirDyn{count}.nodes = [nodes(end,1)];
elementsDirDyn{count}.mass = mass;
elementsDirDyn{count}.g = grav;

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'KinematicConstraint';
elementsDirDyn{count}.nodes = [1 Start1];
elementsDirDyn{count}.A = [0 0 0 0 1 0]';

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'KinematicConstraint';
elementsDirDyn{count}.nodes = [Start1+nElem1 Start2];
elementsDirDyn{count}.A = [0 0 0 0 1 0]';

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'KinematicConstraint';
elementsDirDyn{count}.nodes = [nodes(end-1,1) nodes(end,1)];
elementsDirDyn{count}.A = [0 0 0 0 1 0]';

count = size(elementsDirDyn,2)+1;
elementsDirDyn{count}.type = 'KinematicConstraint';
elementsDirDyn{count}.nodes = [nodes(end,1) 1];
elementsDirDyn{count}.A = [1 0 0 0 0 0]';


% Trajectory
r=l1/2;
x_end = nodes(end,2);

timeVector = 0:timestepsize:finaltime;
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'cos',t_i,t_f);

u1_init = interp1(D.parameters.time,ModelR.listElementVariables{end}.value,timeVector,'linear');

u1 = interp1(S.timeValues,ModelF.listElementVariables{end}.value,timeVector,'linear');
count = count+1;
elementsDirDyn{count}.type = 'ForceInKinematicConstraint';
elementsDirDyn{count}.elements = [count-4];
elementsDirDyn{count}.f = u1;

% Model Def
ModelFD = FEModel();% Flex model
ModelFD.CreateFEModel(newNodes,elementsDirDyn);
ModelFD.defineBC(BC);

ModelRD = FEModel();% Rigid model
ModelRD.CreateFEModel(newNodes,elementsDirDyn);
ModelRD.defineBC(BC);

% Solving Flexible
D = DynamicIntegration(ModelFD);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.01;
D.parameters.scaling = 1e3;
D.runIntegration();

% Solving Rigid
ModelRD.listElements{count}.f = u1_init;

DR = DynamicIntegration(ModelRD);
DR.parameters.finaltime = finaltime;
DR.parameters.timestepsize = timestepsize;
DR.parameters.rho = 0.01;
DR.parameters.scaling = 1e3;
DR.runIntegration();

% plots
x = ModelFD.listNodes{end}.position(1,:);

xR = ModelRD.listNodes{end}.position(1,:);

figure
hold on
plot(x, '-o','Linewidth',2, 'Color','r')
plot(xR,'--', 'Linewidth',2, 'Color','b')
plot(trajx, 'Linewidth',2, 'Color','k')
legend('With u','With u_{rigid}','Prescribed')
zlabel('X [m]','Fontsize',16)
grid on

figure
hold on
plot(timeVector,x)
plot(timeVector,trajx,'--')
legend('X','Xd')
grid on

joint1_init = ModelRD.listElementVariables{1};

joint1 = ModelFD.listElementVariables{1};

figure
hold on
plot(timeVector,joint1.relCoo,'Linewidth',2)
plot(timeVector,joint1_init.relCoo,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints pos (m)','Fontsize',16)
legend('Joint 1','Joint 1 rigid','Location', 'Best')
grid on

figure
hold on
plot(timeVector,u1,'Linewidth',2)
plot(timeVector,u1_init(:),'--','Linewidth',2)
xlabel('Time [s]','Fontsize',16)
ylabel('Commands [N]','Fontsize',16)
legend('u_1','u_{1,rigid}','Location','Best')
grid on

RelError = zeros(size(timeVector));
RelErrorR = zeros(size(timeVector));
for i = 1:length(RelError)
    RelError(i) = 100*((norm([trajx(i)-x(i)]))/(norm([trajx(i)])));
    RelErrorR(i) = 100*((norm([trajx(i)-xR(i)]))/(norm([trajx(i)])));
end

RMSRelError = sqrt(mean(RelError.^2))
MaxRelError = max(RelError)
RMSRelErrorR = sqrt(mean(RelErrorR.^2))
MaxRelErrorR = max(RelErrorR)
