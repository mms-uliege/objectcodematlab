%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Visualize eigen vectors
clear all
% load('C:\ObjectCodeMatlab\TestsArthur\S_SerialArm_wGrav_Article.mat')
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAModelMay2018\S16sCutActual_115.mat')
load('C:\ObjectCodeMatlab\TestsArthur\FlexorFlexLink\S_2dof_9_h10.mat')

%% compute eigenmodes
% close all
step = 1;
% step = 211;

check_shape = 1;
check_rank = 1;

S.LocMotionDof = S.model.listFreeDof(1:S.nCoord);

tempTime.timestep = step; % need a structure with timestep content to run the Update
% Motion Constraint Gradient
% scaling = S.parameters.scaling;
scaling = 1;
for i = S.model.listNumberElements
    S.model.listElements{i}.UpDate(tempTime);
end
K = S.model.getTangentMatrix_Opti('Stiffness',step,scaling);
K = K(S.LocMotionDof,S.LocMotionDof);
M = S.model.getTangentMatrix_Opti('Mass',step,scaling);
M = M(S.LocMotionDof,S.LocMotionDof);
C = S.model.getTangentMatrix_Opti('Damping',step,scaling);
C = C(S.LocMotionDof,S.LocMotionDof);

B = S.model.getTangentMatrix_Opti('Phiq',step,scaling);
A = B(S.LocMotionDof,S.listControls);
dServodq = B(S.listControls,S.LocMotionDof);
B = B(S.listLagrangeM,S.LocMotionDof);

E = zeros(2*S.nCoord+S.nControls+S.nLagrangeM,2*S.nCoord+S.nControls+S.nLagrangeM);
As = zeros(2*S.nCoord+S.nControls+S.nLagrangeM,2*S.nCoord+S.nControls+S.nLagrangeM);

% Like in Article
E(1:S.nCoord,1:S.nCoord) = eye(S.nCoord);
E(S.nCoord+1:2*S.nCoord,S.nCoord+1:2*S.nCoord) = M;

As(1:S.nCoord,S.nCoord+1:2*S.nCoord) = eye(S.nCoord);
As(S.nCoord+1:2*S.nCoord,1:S.nCoord) = -K;
As(S.nCoord+1:2*S.nCoord,S.nCoord+1:2*S.nCoord) = -C;
As(S.nCoord+1:2*S.nCoord,2*S.nCoord+1:2*S.nCoord+S.nControls+S.nLagrangeM) = -[B' -A];
As(2*S.nCoord+1:2*S.nCoord+S.nControls+S.nLagrangeM,1:S.nCoord) = [B; dServodq];

% % As in older code
% E(1:S.nCoord,S.nCoord+S.nControls+S.nLagrangeM+1:2*S.nCoord+S.nControls+S.nLagrangeM) = M;
% E(S.nCoord+S.nControls+S.nLagrangeM+1:2*S.nCoord+S.nControls+S.nLagrangeM,1:S.nCoord) = eye(S.nCoord);
% 
% As(S.nCoord+S.nControls+S.nLagrangeM+1:2*S.nCoord+S.nControls+S.nLagrangeM,S.nCoord+S.nControls+S.nLagrangeM+1:2*S.nCoord+S.nControls+S.nLagrangeM) = eye(S.nCoord);
% As(1:S.nCoord,1:S.nCoord) = -K;
% As(1:S.nCoord,S.nCoord+S.nControls+S.nLagrangeM+1:2*S.nCoord+S.nControls+S.nLagrangeM) = -C;
% As(1:S.nCoord,S.nCoord+1:S.nCoord+S.nControls+S.nLagrangeM) = -[B' -A];
% As(S.nCoord+1:S.nCoord+S.nControls+S.nLagrangeM,1:S.nCoord) = [B; dServodq];

% % Case of "free" system (without servo constraints)
% E(1:S.nCoord,1:S.nCoord) = eye(S.nCoord);
% E(S.nCoord+1:2*S.nCoord,S.nCoord+1:2*S.nCoord) = M;
% 
% As(1:S.nCoord,S.nCoord+1:2*S.nCoord) = eye(S.nCoord);
% As(S.nCoord+1:2*S.nCoord,1:S.nCoord) = -K;
% As(S.nCoord+1:2*S.nCoord,S.nCoord+1:2*S.nCoord) = -C;
% As(S.nCoord+1:2*S.nCoord,2*S.nCoord+1:2*S.nCoord+S.nLagrangeM) = -B';
% As(2*S.nCoord+1:2*S.nCoord+S.nLagrangeM,1:S.nCoord) = B;

[V,D] = eig(As,E);
% [V,D] = eig(As,E,'qz');
% poles = sort(diag(D));
poles = diag(D);

epsilon = 1e-6;
largeNumber = 1.25*1e10;
% largeNumber = inf;
% Check stable (negative real value) and unstable poles (positive real
% value

indexStable = find(abs(poles)<largeNumber & real(poles) <= epsilon);
indexUnstable = find(abs(poles)<largeNumber & real(poles) > epsilon);
stable_poles = poles(indexStable); % stable poles
unstable_poles = poles(indexUnstable); % unstable poles
sorted_stable_poles = sort(stable_poles); % sorted
sorted_unstable_poles = sort(unstable_poles); % sorted
ns = length(indexStable); % number of stable poles
nu = length(indexUnstable); % number of unstable poles
for i = 1:ns
    pol = sorted_stable_poles(i);
    sorted_index_stable(i) = find(poles(indexStable)==pol);
end
for i = 1:nu
    pol = sorted_unstable_poles(i);
    sorted_index_unstable(i) = find(poles(indexUnstable)==pol);
end

gradceq_bv = zeros(2*S.nCoord, 4*S.nCoord);
gradceq_bv(1:S.nControls+S.nLagrangeM,1:S.nCoord) = [B; dServodq];
gradceq_bv(S.nControls+S.nLagrangeM+1:2*(S.nControls+S.nLagrangeM),S.nCoord+1:2*S.nCoord) = [B; dServodq];

Dstar = [[B; dServodq] zeros(S.nControls+S.nLagrangeM,S.nCoord);
          zeros(S.nControls+S.nLagrangeM,S.nCoord) [B; dServodq]];
Dstar_free = [B zeros(S.nLagrangeM,S.nCoord);
          zeros(S.nLagrangeM,S.nCoord) B];
% Vs = V([1:S.nCoord S.nCoord+S.nControls+S.nLagrangeM+[1:S.nCoord]],indexStable);
% Vu = V([1:S.nCoord S.nCoord+S.nControls+S.nLagrangeM+[1:S.nCoord]],indexUnstable);
Vs = V([1:2*S.nCoord],indexStable);
Vu = V([1:2*S.nCoord],indexUnstable);
% Vs = V(indexStable,[1:S.nCoord S.nCoord+S.nControls+S.nLagrangeM+[1:S.nCoord]]);
% Vu = V(indexUnstable,[1:S.nCoord S.nCoord+S.nControls+S.nLagrangeM+[1:S.nCoord]]);
% nullStable = null([Dstar; Vs(:,2:end)']);
% nullStable = null([Dstar; Vs']);
% nullUnstable = null([Dstar; Vu']);
nullStable = null([Vs'; Dstar]);
nullUnstable = null([Vu'; Dstar]);
% nullStable = null([Dstar; Vs]);
% nullUnstable = null([Dstar; Vu]);
simpleNullStable = null(Vs');
simpleNullUnstable = null(Vu');

if check_rank
% Check independency of vectors
size_stable = size([Dstar; Vs']);
rank_stable = rank([Dstar; Vs']);
disp(['Size of STABLE concatenated matrix is ',num2str(size_stable(1)), ' x ',num2str(size_stable(2)),', while rank is ', num2str(rank_stable),'.'])
size_unstable = size([Dstar; Vu']);
rank_unstable = rank([Dstar; Vu']);
disp(['Size of UNSTABLE concatenated matrix is ',num2str(size_unstable(1)), ' x ',num2str(size_unstable(2)),', while rank is ', num2str(rank_unstable),'.'])
% STABLE
check_rank_stable = zeros(1,ns+1); % Check rank of concatenated matrix with ONLY ONE eigen vector
check_rank_stable_tot = zeros(1,ns+1); % Check rank of concatenated matrix with INCREASING number of eigen vector
check_rank_stable(1) = rank(Dstar);
check_rank_stable_tot(1) = rank(Dstar);
for i = 1:ns
    check_rank_stable(i+1) = rank([Dstar; Vs(:,i)']);
    check_rank_stable_tot(i+1) = rank([Dstar; Vs(:,1:i)']);
end
figure
plot(0:ns,[check_rank_stable;check_rank_stable_tot],'-*')
title('Rank check of concatenated matrix STABLE')
xlabel('Considered eigenvector')
ylabel('Rank of concatenated matrix')
grid on
legend('One at a time','"up to"')
% UNSTABLE
check_rank_unstable = zeros(1,nu+1); % Check rank of concatenated matrix with ONLY ONE eigen vector
check_rank_unstable_tot = zeros(1,nu+1); % Check rank of concatenated matrix with INCREASING number of eigen vector
check_rank_unstable(1) = rank(Dstar);
check_rank_unstable_tot(1) = rank(Dstar);
for i = 1:nu
    check_rank_unstable(i+1) = rank([Dstar; Vu(:,i)']);
    check_rank_unstable_tot(i+1) = rank([Dstar; Vu(:,1:i)']);
end
figure
plot(0:nu,[check_rank_unstable;check_rank_unstable_tot],'-*')
title('Rank check of concatenated matrix UNSTABLE')
xlabel('Considered eigenvector')
ylabel('Rank of concatenated matrix')
grid on
legend('One at a time','"up to"')
for i = 1:nu
    for j = 1:nu
        vu_rank(i,j) = rank(Vu(:,[i j]),1e-11);
%         vu_rank(i,j) = rank(Vu(:,[i j]));
    end
end
for i = 1:ns
    for j = 1:ns
        vs_rank(i,j) = rank(Vs(:,[i j]));
    end
end
figure
surf(vu_rank)
figure
surf(vs_rank)

end
%% Check shape
if check_shape

Model = S.model; 
% step = 1;
scale = 1;

for i = Model.listNumberNodes
    Model.listNodes{i}.InitializeD_Opti();
end
for i = Model.listNumberElementVariables
    Model.listElementVariables{i}.InitializeD_Opti();
end
% figure
% Stable vectors
np = 10; % # of discretization of the beam
a = round(sqrt(ns));
b = a+1;
figure
count = 1;
% for nEigen = 1:ns
for nEigen = sorted_index_stable
    %%%%%%%%
    for i = 1:S.nCoord
%         real(Vs(i+57,nEigen))
%         Model.listDof{Model.listFreeDof(i)}.value = scale*real(Vs(i,nEigen)/norm(Vs(:,nEigen)));
%         Model.listDof{Model.listFreeDof(i)}.d_value = scale*real(Vs(i+S.nCoord,nEigen)/norm(Vs(:,nEigen)));
        Model.listDof{Model.listFreeDof(i)}.value = scale*real(Vs(i,nEigen));
        Model.listDof{Model.listFreeDof(i)}.d_value = scale*real(Vs(i+S.nCoord,nEigen));
    end
    for i = Model.listNumberNodes
        Model.listNodes{i}.UpdateD_Opti(step)
    end
    for i = Model.listNumberElementVariables
        if strcmp(Model.listElementVariables{i}.DofType,'MotionDof')
            Model.listElementVariables{i}.UpdateD_Opti(step)
        end
    end
    %%%%%%%%
%     % Just on position of nodes (not angles nor relative joint)
%     %%%%%%%%%%%%%%
%     for i = 1:length(S.model.listNodes)-1
%         Model.listDof{Model.listFreeDof((i-1)*6+1)}.value = scale*real(Vs((i-1)*6+1,nEigen));
%         Model.listDof{Model.listFreeDof((i-1)*6+1)}.d_value = scale*real(Vs((i-1)*6+1+S.nCoord,nEigen));
%         Model.listDof{Model.listFreeDof((i-1)*6+2)}.value = scale*real(Vs((i-1)*6+1,nEigen));
%         Model.listDof{Model.listFreeDof((i-1)*6+2)}.d_value = scale*real(Vs((i-1)*6+1+S.nCoord,nEigen));
%         Model.listDof{Model.listFreeDof((i-1)*6+3)}.value = scale*real(Vs((i-1)*6+1,nEigen));
%         Model.listDof{Model.listFreeDof((i-1)*6+3)}.d_value = scale*real(Vs((i-1)*6+1+S.nCoord,nEigen));
%     end
%     for i = Model.listNumberNodes
%         Model.listNodes{i}.UpdateD_Opti(step)
%     end
%     %%%%%%%%%%%%%
%     Model.Visu(step)
%     pause(0.1)
%     axis([0 1.5 -1 1 0 0.8])
%     view([0 0])
%     pause()
%     figure
    listPoint = []; % initialization of "points" (nodes + beam discretization)
    listDisp = []; % initialization of "displacement" compared to initial straight case
    for Nelem = Model.listNumberElements
        elem = Model.listElements{Nelem};
        if isa(elem,'FlexibleBeamElement')
            % Modified by eigenmode
            RA = dimR(elem.listNodes{1}.R(:,step));
            RB = dimR(elem.listNodes{2}.R(:,step));
            xA = elem.listNodes{1}.position(:,step);
            xB = elem.listNodes{2}.position(:,step);
            HAm1 = [RA' -RA'*xA; 0 0 0 1];
            HA  = [RA xA; 0 0 0 1]; HB = [RB xB; 0 0 0 1];
            hi = logSE3(HAm1*HB);
            % Not modified
            xA_init = elem.listNodes{1}.position_InitOpti(:,step);
            xB_init = elem.listNodes{2}.position_InitOpti(:,step);
            RA_init = dimR(elem.listNodes{1}.R_InitOpti(:,step));
            RB_init = dimR(elem.listNodes{2}.R_InitOpti(:,step));
            HAm1_init = [RA_init' -RA_init'*xA_init; 0 0 0 1];
            HA_init  = [RA_init xA_init; 0 0 0 1]; HB_init = [RB_init xB_init; 0 0 0 1];
            h_init = logSE3(HAm1_init*HB_init);
            listPoint = [listPoint elem.listNumberNodes(1)+[0:1/np:1]];
            listPose = zeros(length(np+1),3);
            listPose_init = zeros(length(np+1),3);
            listPose(1,:) = xA';
            listPose_init(1,:) = xA_init';
            for i = 1:np
                coord = i/(np);
                Hi = HA*expSE3(coord*hi);
                H_init = HA_init*expSE3(coord*h_init);
                listPose(i+1,:) = Hi(1:3,4);
                listPose_init(i+1,:) = H_init(1:3,4);
%                 sca = 0.05;
%                 hold on
%                 plot3([Hi(1,4) Hi(1,4)+Hi(1,1)*sca],[Hi(2,4) Hi(2,4)+Hi(2,1)*sca],[Hi(3,4) Hi(3,4)+Hi(3,1)*sca],'g','Linewidth',1) % X axis
%                 plot3([Hi(1,4) Hi(1,4)+Hi(1,2)*sca],[Hi(2,4) Hi(2,4)+Hi(2,2)*sca],[Hi(3,4) Hi(3,4)+Hi(3,2)*sca],'r','Linewidth',1) % Y axis
%                 plot3([Hi(1,4) Hi(1,4)+Hi(1,3)*sca],[Hi(2,4) Hi(2,4)+Hi(2,3)*sca],[Hi(3,4) Hi(3,4)+Hi(3,3)*sca],'b','Linewidth',2) % Z axis
%                 grid on
%                 axis([0.6 1.5 -0.1 0.5 -0.1 0.8])
            end
%             plot3(listPose_init(:,1),listPose_init(:,2),listPose_init(:,3),'k')
%             hold off
            listDisp = [listDisp; listPose-listPose_init]; 
        end
    end
    
    subplot(a,b,count)
    plot(listPoint,listDisp', 'Linewidth', 2)
    title(['     #',num2str(nEigen),' -- ',num2str(abs(stable_poles(nEigen))),' rad/s'])
%     title(['#',num2str(nEigen),' with pulse of ',num2str(abs(stable_poles(nEigen))),' rad/s'])
%     xlabel('Nodes (with intermidiate interpolation points)')
%     ylabel('Difference compared to straight initial state')
    grid on
    count = count+1;
end
legend('x','y','z')

% unstable vectors
% figure
au = round(sqrt(nu));
bu = au+1;
figure
count = 1;
for nEigen = sorted_index_unstable
% for nEigen = 1:nu
    for i = 1:S.nCoord
%         real(Vs(i+57,nEigen))
        Model.listDof{Model.listFreeDof(i)}.value = scale*real(Vu(i,nEigen)/norm(Vu(:,nEigen)));
        Model.listDof{Model.listFreeDof(i)}.d_value = scale*real(Vu(i+S.nCoord,nEigen)/norm(Vu(:,nEigen)));
    end
    for i = Model.listNumberNodes
        Model.listNodes{i}.UpdateD_Opti(step)
    end
    for i = Model.listNumberElementVariables
        if strcmp(Model.listElementVariables{i}.DofType,'MotionDof')
            Model.listElementVariables{i}.UpdateD_Opti(step)
        end
    end
%     Model.Visu(step)
%     pause(0.1)
%     pause()
    listPoint = []; % initialization of "points" (nodes + beam discretization)
    listDisp = []; % initialization of "displacement" compared to initial straight case
    for Nelem = Model.listNumberElements
        elem = Model.listElements{Nelem};
        if isa(elem,'FlexibleBeamElement')
            % Modified by eigenmode
            RA = dimR(elem.listNodes{1}.R(:,step));
            RB = dimR(elem.listNodes{2}.R(:,step));
            xA = elem.listNodes{1}.position(:,step);
            xB = elem.listNodes{2}.position(:,step);
            HAm1 = [RA' -RA'*xA; 0 0 0 1];
            HA  = [RA xA; 0 0 0 1]; HB = [RB xB; 0 0 0 1];
            hi = logSE3(HAm1*HB);
            % Not modified
            xA_init = elem.listNodes{1}.position_InitOpti(:,step);
            xB_init = elem.listNodes{2}.position_InitOpti(:,step);
            RA_init = dimR(elem.listNodes{1}.R_InitOpti(:,step));
            RB_init = dimR(elem.listNodes{2}.R_InitOpti(:,step));
            HAm1_init = [RA_init' -RA_init'*xA_init; 0 0 0 1];
            HA_init  = [RA_init xA_init; 0 0 0 1]; HB_init = [RB_init xB_init; 0 0 0 1];
            h_init = logSE3(HAm1_init*HB_init);
            listPoint = [listPoint elem.listNumberNodes(1)+[0:1/np:1]];
            listPose = zeros(length(np+1),3);
            listPose_init = zeros(length(np+1),3);
            listPose(1,:) = xA';
            listPose_init(1,:) = xA_init';
            for i = 1:np
                coord = i/(np);
                Hi = HA*expSE3(coord*hi);
                H_init = HA_init*expSE3(coord*h_init);
                listPose(i+1,:) = Hi(1:3,4);
                listPose_init(i+1,:) = H_init(1:3,4);
            end
            listDisp = [listDisp; listPose-listPose_init]; 
        end
    end
    
    subplot(au,bu,count)
    plot(listPoint,listDisp','Linewidth',2)
    title(['     #',num2str(nEigen),' -- ',num2str(abs(unstable_poles(nEigen))),' rad/s'])
%     title(['#',num2str(nEigen),' with pulse of ',num2str(abs(unstable_poles(nEigen))),' rad/s'])
%     xlabel('Nodes (with intermidiate interpolation points)')
%     ylabel('Difference compared to straight initial state')
    grid on
    count = count+1;
end
legend('x','y','z')
% new complex
abs_unstable = complex(abs(real(sorted_unstable_poles)),abs(imag(sorted_unstable_poles)));
abs_stable = complex(abs(real(sorted_stable_poles)),abs(imag(sorted_stable_poles)));
% abs sorted
abs_real_unstable = abs(real(sorted_unstable_poles));
abs_imag_unstable = abs(imag(sorted_unstable_poles));
abs_real_stable = abs(real(sorted_stable_poles));
abs_imag_stable = abs(imag(sorted_stable_poles));
% abs NON sorted
% abs_real_unstable = abs(real(unstable_poles));
% abs_imag_unstable = abs(imag(unstable_poles));
% abs_real_stable = abs(real(stable_poles));
% abs_imag_stable = abs(imag(stable_poles));
% Plotting eigenvalues
figure
% not LOG scale
plot(real(unstable_poles),imag(unstable_poles),'d','MarkerFaceColor','r','MarkerEdgeColor','r')
hold on
plot(real(stable_poles),imag(stable_poles),'o','MarkerFaceColor','b','MarkerEdgeColor','b')
% % positive values
% plot(real(abs_unstable),imag(abs_unstable),'d','MarkerFaceColor','r','MarkerEdgeColor','r')
% hold on
% plot(real(abs_stable),imag(abs_stable),'o','MarkerFaceColor','b','MarkerEdgeColor','b')
% % LOG scale
% loglog(abs_real_unstable,abs_imag_unstable,'d','MarkerFaceColor','r','MarkerEdgeColor','r')
% hold on
% loglog(abs_real_stable,abs_imag_stable,'o','MarkerFaceColor','b','MarkerEdgeColor','b')
% set(gca,'xscale','log')
% set(gca,'yscale','log')
xlabel('Real part')
ylabel('Imaginary part')
title('Poles of the system ("flipped" in 1st quad)')
legend('Unstable','Stable','Location','Best')
grid on
end