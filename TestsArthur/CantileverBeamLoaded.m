%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Direct dynamic of a deformed cantilever beam under load
clear
% close all


timestepsize = 0.01;
finaltime = 2;

a1 = 0.05;
b1 = a1;
l1 = 1;
e1 = 0.005;
a1In = a1-2*e1; % inner length
b1In = b1-2*e1;
% nodes definition

nodes = [1 0 0 0;
         2 0 l1 0];
     
nElem1 = 4;
Start1 = 1;
End1 = 2;
nodes = createInterNodes(nodes,nElem1,Start1,End1);

% grav = [0 0 -9.81];
grav = [0 0 0];

% Elements definition

m_end = 1; % end-effector mass
rho = 2700;
E = 70e9; nu = 0.3; G = E/(2*(1+nu));
A1 = a1*b1-a1In*b1In; Ixx1 = (a1*b1*(a1^2+b1^2)-a1In*b1In*(a1In^2+b1In^2))/12; Iyy1 = (b1*a1^3-b1In*a1In^3)/12;Izz1 = (a1*b1^3-a1In*b1In^3)/12;

KCS1 = diag([E*A1 G*A1 G*A1 G*Ixx1 E*Iyy1 E*Izz1]);
MCS1 = diag(rho*[A1 A1 A1 Ixx1 Iyy1 Izz1]);

type = 'FlexibleBeamElement';
count = 0;
for i = 1:nElem1
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start1+i-1,1) nodes(Start1+i,1)];
    elements{count+i}.KCS = KCS1;
    elements{count+i}.MCS = MCS1;
    elements{count+i}.g = grav;
end

count = size(elements,2)+1;
elements{count}.type = 'ExternalForce';
elements{count}.nodes = [nodes(end,1)];
elements{count}.amplitude = 100;
elements{count}.frequency = 0;
elements{count}.DOF = 4;

% count = size(elements,2)+1;
% elements{count}.type = 'RotSpringDamperElement';
% elements{count}.nodes = [1 2];
% elements{count}.stiffness = 1000;
% elements{count}.damping = 20;
% elements{count}.A = [1 0 0];

% count = size(elements,2)+1;
% elements{count}.type = 'KinematicConstraint';
% elements{count}.nodes = [1 2];
% elements{count}.A = [0 0 0 1 0 0]';

% count = size(elements,2)+1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [nodes(end,1)];
% elements{count}.mass = m_end;
% elements{count}.g = grav;

% Boundary Condition
BC = [1];

% Problem solving

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration2(ModelR);
% D = StaticIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.1;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
tic
D.runIntegration();
laps = toc;
disp(['Computation lasted ', num2str(laps/60),' min.'])

% Plots

% nodeInit = ModelR.listNodes{1};
nodeEnd = ModelR.listNodes{end};
time = D.parameters.time;

figure
plot(time, nodeEnd.position(3,:))
xlabel('Time (s)')
ylabel('Position (m)')
legend('z')

