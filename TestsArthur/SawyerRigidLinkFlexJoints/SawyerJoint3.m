%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test to try to get a control of 3 joints of the sawyer.
% "Simplified sawyer to a 3 dof robot.blocking joint 3, 5, 6, 7. joint 1,2
% and 4 are considered as flexible joints as well
% -!- axis are aaccording to Solidworks CAD, meaning y along vertical axis
% and x towards the front of the robot -!-
% close all
clear all

finaltime = 2; % 2 seconds
timestepsize = 0.01;

npts = finaltime/timestepsize + 1; % number of points for optimization
hypForPreComp = 'notConstant';
hypForComp = 'notConstant';

t_i = 0.2; % time at end of pre-actuation
t_f = finaltime - 0.2; % time at begining of post-actuation

% grav = [0 -9.81 0]; % gravity in m/s�
grav = [0 0 0]; % gravity in m/s�

link1.m = 5.824; % mass of link in kg
link1.J = [0.118 0 0;...
           0 0.018 -0.002;...
           0 -0.002 0.121]; % Intertia at center of gravity in kgm� taken in the base axis
link1.nodes = [0 0.081 0;...
               0.02 0.297 0.004;...
               0.081 0.317 -0.0505]; % nodes of the body, starting with bottom to top (center of mass in the middle)

link2.m = 5.528; % mass of link in kg
link2.J = [0.019 0 -0.018;...
           0 0.127 0;...
           -0.018 0 0.117]; % Intertia at center of gravity in kgm� taken in the base axis
link2.nodes = [0.081 0.317 -0.0505;...
               0.202 0.317 -0.163;...
               0.481 0.317 -0.1505]; % nodes of the body, starting with bottom to top (center of mass in the middle)

link3.m = 5.009; % mass of link in kg
link3.J = [0.024 -0.01 -0.02;...
           -0.01 0.158 0.005;...
           -0.02 0.005 0.146]; % Intertia at center of gravity in kgm� taken in the base axis
link3.nodes = [0.481 0.317 -0.1505;...
               0.697 0.307 -0.07;...
               0.881 0.18325 -0.1603]; % nodes of the body, starting with bottom to top (center of mass in the middle)

% Stifness and damping parameters of joints
damp = 0.26*1e0; %0.26*1e-5;
stif = 1000;

           
nodes = [1 link1.nodes(1,:);...
         2 link1.nodes(1,:);... % flexible joint node
         3 link1.nodes(1,:);...
         4 link1.nodes(2,:);...
         5 link1.nodes(3,:);...
         6 link2.nodes(1,:);... % flexible joint node
         7 link2.nodes(1,:);...
         8 link2.nodes(2,:);...
         9 link2.nodes(3,:);...
         10 link3.nodes(1,:);... % flexible joint node
         11 link3.nodes(1,:);...
         12 link3.nodes(2,:);...
         13 link3.nodes(3,:)]; % List of nodes to build the rigid model
     
     
count = 1;
% creating first rigid body (rotating aroud y)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [4 3 5];
elements{count}.mass = link1.m;
elements{count}.J = link1.J;
elements{count}.g = grav;

count = size(elements,2)+1;
% creating first flexible joint (rotating aroud y)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [2];
elements{count}.mass = 0.001;
elements{count}.J = 0.0001*eye(3);
elements{count}.g = grav;

count = size(elements,2)+1;
% creating second rigid body (rotating aroud z)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [8 7 9];
elements{count}.mass = link2.m;
elements{count}.J = link2.J;
elements{count}.g = grav;

count = size(elements,2)+1;
% creating second flexible joint (rotating aroud y)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [6];
elements{count}.mass = 0.001;
elements{count}.J = 0.0001*eye(3);
elements{count}.g = grav;

count = size(elements,2)+1;
% creating third rigid body (rotating aroud z)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [12 11 13];
elements{count}.mass = link3.m;
elements{count}.J = link3.J;
elements{count}.g = grav;

count = size(elements,2)+1;
% creating third flexible joint (rotating aroud y)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [10];
elements{count}.mass = 0.001;
elements{count}.J = 0.0001*eye(3);
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'RotSpringDamperElement';
elements{count}.nodes = [2 3];
elements{count}.stiffness = stif;
elements{count}.damping = damp;
elements{count}.A = [0 1 0];

count = size(elements,2)+1;
elements{count}.type = 'RotSpringDamperElement';
elements{count}.nodes = [6 7];
elements{count}.stiffness = stif;
elements{count}.damping = damp;
elements{count}.A = [0 0 1];

count = size(elements,2)+1;
elements{count}.type = 'RotSpringDamperElement';
elements{count}.nodes = [10 11];
elements{count}.stiffness = stif;
elements{count}.damping = damp;
elements{count}.A = [0 0 1];

% Creating the kinematic constraints
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % flexible joint
elements{count}.nodes = [2 3];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [5 6];
elements{count}.A = [0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % flexible joint
elements{count}.nodes = [6 7];
elements{count}.A = [0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [9 10];
elements{count}.A = [0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % flexible joint
elements{count}.nodes = [10 11];
elements{count}.A = [0 0 0 0 0 1]';
count = count+1;

% Trajectory

r = 0.2;
z_end = nodes(end,4);
x_end = nodes(end,2)-2*r;

timeVector = 0:timestepsize:finaltime;
trajy = nodes(end,3)*ones(size(timeVector));
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);
trajx = halfCircleTraj(nodes(end,2),x_end,r,timeVector,'sin',t_i,t_f);

elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajy;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elements{count}.elements = [count-6 count-4 count-2];
elements{count}.active = 1;

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

% D = DynamicIntegration(ModelR);
D = StaticIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
% D.parameters.rho = 0.01;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

% Solving flexible dynamicaly
ModelF = FEModel();
ModelF.CreateFEModel(nodes,elements);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from static solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
%     ModelF.listNodes{n}.velocity = zeros(6,D.parameters.nstep+1);
%     ModelF.listNodes{n}.acceleration = zeros(6,D.parameters.nstep+1);
    dh = zeros(6,npts);
    for i = 1:npts
        dh(:,i) = logSE3([dimR(ModelF.listNodes{n}.R(:,i)) ModelF.listNodes{n}.position(:,i); 0 0 0 1]);
    end
    ModelF.listNodes{n}.velocity = deriveVal(dh,timestepsize);
    ModelF.listNodes{n}.acceleration = deriveVal(dh,timestepsize);
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        % added because in static first computation, there were no velocity
        % nor acceleration, with initialize them to zero first (? check if
        % this works)
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
%         ModelF.listElementVariables{n}.velocity = zeros(ModelR.listElementVariables{n}.nDof,D.parameters.nstep+1);
        ModelF.listElementVariables{n}.velocity = deriveVal(ModelF.listElementVariables{n}.relCoo,timestepsize);
%         ModelF.listElementVariables{n}.acceleration = zeros(ModelR.listElementVariables{n}.nDof,D.parameters.nstep+1);
        ModelF.listElementVariables{n}.acceleration = deriveVal(ModelF.listElementVariables{n}.velocity,timestepsize);
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

% Start second full optimization with newly computed initial guess
tic
S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = 0.1;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.parameters.scaling = 1e6;
S.ConstIter = hypForComp;
% analys = S.runPoleAnalysisOpti(D)
S.JointToMinimize = [count-6 count-4 count-2];
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])


timeSteps = S.timeValues;
timeLoc = S.timesteps;

uRigi.u1 = D.model.listElementVariables{end}.value(1,:);
uRigi.u2 = D.model.listElementVariables{end}.value(2,:);
uRigi.u3 = D.model.listElementVariables{end}.value(3,:);
time = D.parameters.time;
uRigi.time = time;

uBeam.u1 = ModelF.listElementVariables{end}.value(1,timeLoc);
uBeam.u2 = ModelF.listElementVariables{end}.value(2,timeLoc);
uBeam.u3 = ModelF.listElementVariables{end}.value(3,timeLoc);
uBeam.time = timeSteps;

% Plot the joints angles
joint1 = S.model.listElementVariables{1};
joint2 = S.model.listElementVariables{3};
joint3 = S.model.listElementVariables{5};

joint1R = D.model.listElementVariables{1};
joint2R = D.model.listElementVariables{3};
joint3R = D.model.listElementVariables{5};


figure
hold on
plot(time,uRigi.u1,'--',time,uRigi.u2,'--',time,uRigi.u3,'--')
plot(time,uBeam.u1,time,uBeam.u2,time,uBeam.u3)
legend('u1_{rigi}','u2_{rigi}','u3_{rigi}','u1','u2','u3')
xlabel('Time (s)')
ylabel('Effort (Nm)')

figure
hold on
plot(time,joint1R.relCoo,'--',time,joint2R.relCoo,'--',time,joint3R.relCoo,'--')
plot(time,joint1.relCoo,time,joint2.relCoo,time,joint3.relCoo)
legend('j1_{rigi}','j2_{rigi}','j3_{rigi}','j1','j2','j3')
xlabel('Time (s)')
ylabel('Angle (rad)')

figure
% hold on
% plot(time,joint1R.velocity,'--',time,joint2R.velocity,'--',time,joint3R.velocity,'--')
plot(time,joint1.velocity,time,joint2.velocity,time,joint3.velocity)
% legend('j1_{rigi}','j2_{rigi}','j3_{rigi}','j1','j2','j3')
xlabel('Time (s)')
ylabel('Velocity (rad/s)')

poseData = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\TestPoseCircleFlexJoint2.txt','w');
fprintf(poseData,'%s %s %s %s\n','time','joint1','joint2','joint3');
fprintf(poseData,'%.6f %.6f %.6f %.6f\n',[time;joint1.relCoo;joint2.relCoo;joint3.relCoo]);
fclose(poseData);

velocityData = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\TestVelocityCircleFlexJoint2.txt','w');
fprintf(velocityData,'%s %s %s %s\n','time','joint1','joint2','joint3');
fprintf(velocityData,'%.6f %.6f %.6f %.6f\n',[time;joint1.velocity;joint2.velocity;joint3.velocity]);
fclose(velocityData);

torqueData = fopen('C:\Users\Arthur\Desktop\ULg\FRIA\SetupExperimental\Sawyer\MatlabExportData\TestTorqueCircleFlexJoint2.txt','w');
fprintf(torqueData,'%s %s %s %s\n','time','joint1','joint2','joint3');
fprintf(torqueData,'%.6f %.6f %.6f %.6f\n',[time;uBeam.u1;uBeam.u2;uBeam.u3]);
fclose(torqueData);


clear elements

count = 1;
% creating first rigid body (rotating aroud y)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [4 3 5];
elements{count}.mass = link1.m;
elements{count}.J = link1.J;
elements{count}.g = grav;

count = size(elements,2)+1;
% creating first flexible joint (rotating aroud y)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [2];
elements{count}.mass = 0.001;
elements{count}.J = 0.0001*eye(3);
elements{count}.g = grav;

count = size(elements,2)+1;
% creating second rigid body (rotating aroud z)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [8 7 9];
elements{count}.mass = link2.m;
elements{count}.J = link2.J;
elements{count}.g = grav;

count = size(elements,2)+1;
% creating second flexible joint (rotating aroud y)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [6];
elements{count}.mass = 0.001;
elements{count}.J = 0.0001*eye(3);
elements{count}.g = grav;

count = size(elements,2)+1;
% creating third rigid body (rotating aroud z)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [12 11 13];
elements{count}.mass = link3.m;
elements{count}.J = link3.J;
elements{count}.g = grav;

count = size(elements,2)+1;
% creating third flexible joint (rotating aroud y)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [10];
elements{count}.mass = 0.001;
elements{count}.J = 0.0001*eye(3);
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'RotSpringDamperElement';
elements{count}.nodes = [2 3];
elements{count}.stiffness = stif;
elements{count}.damping = damp;
elements{count}.A = [0 1 0];

count = size(elements,2)+1;
elements{count}.type = 'RotSpringDamperElement';
elements{count}.nodes = [6 7];
elements{count}.stiffness = stif;
elements{count}.damping = damp;
elements{count}.A = [0 0 1];

count = size(elements,2)+1;
elements{count}.type = 'RotSpringDamperElement';
elements{count}.nodes = [10 11];
elements{count}.stiffness = stif;
elements{count}.damping = damp;
elements{count}.A = [0 0 1];

% Creating the kinematic constraints
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % flexible joint
elements{count}.nodes = [2 3];
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [5 6];
elements{count}.A = [0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % flexible joint
elements{count}.nodes = [6 7];
elements{count}.A = [0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [9 10];
elements{count}.A = [0 0 0 0 0 1]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint'; % flexible joint
elements{count}.nodes = [10 11];
elements{count}.A = [0 0 0 0 0 1]';

% u1 = uBeam.u1;
% u2 = uBeam.u2;
% u3 = uBeam.u3;

% u1 = uRigi.u1;
% u2 = uRigi.u2;
% u3 = uRigi.u3;

count = size(elements,2)+1;
elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = [count-6];
elements{count}.f = uBeam.u1;

count = size(elements,2)+1;
elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = [count-5];
elements{count}.f = uBeam.u2;

count = size(elements,2)+1;
elements{count}.type = 'ForceInKinematicConstraint';
elements{count}.elements = [count-4];
elements{count}.f = uBeam.u3;


% Boundary Condition
BC = [1];

% Solving
ModelRD = FEModel();
ModelRD.CreateFEModel(nodes,elements);
ModelRD.defineBC(BC);

RD = DynamicIntegration(ModelRD);
% RD = StaticIntegration(ModelRD);
RD.parameters.finaltime = finaltime;
RD.parameters.timestepsize = timestepsize;
RD.parameters.rho = 0.1;
% RD.parameters.relTolRes = 1e-10;
RD.parameters.scaling = 1e6;
RD.runIntegration();

%% Save data
save('C:\Users\Arthur\Documents\MATLAB\OoCode\SawyerRigidLinkFlexJoints\S_bis','S')
save('C:\Users\Arthur\Documents\MATLAB\OoCode\SawyerRigidLinkFlexJoints\RD_bis','RD')

%% plots
figure
title('End-effector trajectory')
hold on
plot3(RD.model.listNodes{end}.position(1,:),RD.model.listNodes{end}.position(2,:),RD.model.listNodes{end}.position(3,:))
plot3(trajx,trajy,trajz,'r')
