%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test Control
clear all
close all

finaltime = 2;
timestepsize = 0.005;
t_i = 0.2;
t_f = 1.7;

nodes = [1 -1 0 0;
         2 -1 0 0;
         3 -1 0 0;
         4 -1.6614 -0.75 0;
         5 -1.6614 -0.75 0;
         6 -1.3307 -1.125 0;
         7 -1.3307 -1.125 0;
         8 -1 -1.5 0];

nodes = createInterNodes(nodes,2,3,4);
nodes = createInterNodes(nodes,2,6,7);
nodes = createInterNodes(nodes,2,9,10);


elements{1}.type = 'RigidBodyElement';
elements{1}.nodes = [2];
elements{1}.mass = 3;
% elements{1}.J = eye(3);
     
elements{2}.type = 'RigidBodyElement';
elements{2}.nodes = [4 3 5];
elements{2}.mass = 6.875;
elements{2}.J = 0.57*eye(3);

elements{3}.type = 'RigidBodyElement';
elements{3}.nodes = [7 6 8];
elements{3}.mass = 6.875/2;
elements{3}.J = 0.0723*eye(3);

elements{4}.type = 'RigidBodyElement';
elements{4}.nodes = [10 9 11];
elements{4}.mass = 6.875/2;
elements{4}.J = 0.0723*eye(3);


elements{5}.type = 'KinematicConstraint';
elements{5}.nodes = [1 2];
elements{5}.A = [1 0 0 0 0 0]';

elements{6}.type = 'KinematicConstraint';
elements{6}.nodes = [2 3];
elements{6}.A = [0 0 0 0 0 1]';

elements{7}.type = 'KinematicConstraint';
elements{7}.nodes = [5 6];
elements{7}.A = [0 0 0 0 0 1]';

elements{8}.type = 'KinematicConstraint';
elements{8}.nodes = [8 9];
elements{8}.A = [0 0 0 0 0 1]';

elements{9}.type = 'RotSpringDamperElement';
elements{9}.damping = 0.25;
elements{9}.stiffness = 50;
elements{9}.nodes = [8 9];
elements{9}.A = [0 0 1];

% Trajectory

cart_end = 1;
cart_i = -1;

x_end = 1;
x_i = -1;

y_end = -1.5;
y_i = -1.5;

r = 1;

timeVector = 0:timestepsize:finaltime;
trajcart = halfCircleTraj(cart_i,cart_end,r,timeVector,'lin',t_i,t_f);
trajx = halfCircleTraj(x_i,x_end,r,timeVector,'sin',t_i,t_f);
trajy = halfCircleTraj(y_i,y_end,r,timeVector,'cos',t_i,t_f);

t = 0:timestepsize:finaltime;

load('uCartRigid')
u1_init = interp1(uCartRigid.time,uCartRigid.u1_init,t,'linear');
u2_init = interp1(uCartRigid.time,uCartRigid.u2_init,t,'linear');
u3_init = interp1(uCartRigid.time,uCartRigid.u3_init,t,'linear');
% u1 = u1_init;
% u2 = u2_init;
% u3 = u3_init;

load('uCartFlex')
u1 = interp1(uCartFlex.time,uCartFlex.u1,t,'linear');
u2 = interp1(uCartFlex.time,uCartFlex.u2,t,'linear');
u3 = interp1(uCartFlex.time,uCartFlex.u3,t,'linear');

elements{10}.type = 'ForceInKinematicConstraint';
elements{10}.elements = [5];
elements{10}.f = u1;

elements{11}.type = 'ForceInKinematicConstraint';
elements{11}.elements = [6];
elements{11}.f = u2;

elements{12}.type = 'ForceInKinematicConstraint';
elements{12}.elements = [7];
elements{12}.f = u3;

BC = [1];

Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

Modelr = FEModel();
Modelr.CreateFEModel(nodes,elements);
Modelr.defineBC(BC);


D = DynamicIntegration(Model);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.3;
% D.parameters.relTolRes = 1e-12;
D.runIntegration();

xcart = Model.listNodes{2}.position(1,:);
ycart = Model.listNodes{2}.position(2,:);
x = Model.listNodes{end}.position(1,:);
y = Model.listNodes{end}.position(2,:);

Modelr.listElements{10}.f = u1_init;
Modelr.listElements{11}.f = u2_init;
Modelr.listElements{12}.f = u3_init;

Dr = DynamicIntegration(Modelr);
Dr.parameters.finaltime = finaltime;
Dr.parameters.timestepsize = timestepsize;
Dr.parameters.rho = 0.3;
% D.parameters.relTolRes = 1e-12;
Dr.runIntegration();

xcartr = Modelr.listNodes{2}.position(1,:);
ycartr = Modelr.listNodes{2}.position(2,:);
xr = Modelr.listNodes{end}.position(1,:);
yr = Modelr.listNodes{end}.position(2,:);

% Plots

timeSteps = D.parameters.time;

% figure
% hold on
% plot(xcart,ycart,x,y, 'Linewidth',3, 'Color','k')
% plot(xcartr,ycartr,':',xr,yr,':', 'Linewidth',3, 'Color','b')
% plot(trajcart,zeros(size(trajcart)),trajx,trajy, 'Linewidth',1, 'Color','r')
% legend('Flexible control','','Rigid control','Prescribed')
% title('Trajectory of the cart')
% grid on

figure
hold on
plot(x,y, ':o','Linewidth',2, 'Color','r')
plot(xr,yr,'--', 'Linewidth',2, 'Color','b')
plot(trajx,trajy, 'Linewidth',2, 'Color','k')
legend('With u','With u_{rigid}','Prescribed')
xlabel('X [m]','Fontsize',16)
ylabel('Y [m]','Fontsize',16)
axis([-1.1 1.1 -1.6 -0.3])
axis equal
% title('Trajectory of the cart','Fontsize',18)
grid on


figure
hold on
plot(timeSteps,xcart,timeSteps,x,timeSteps,y)
plot(timeSteps,trajcart,'--',timeSteps,trajx,'--',timeSteps,trajy,'--')
legend('cart', 'X', 'Y','cartd','Xd','Yd')
grid on

figure
hold on
plot(timeSteps,u1,timeSteps,u2,timeSteps,u3,'Linewidth',2)
set(gca, 'ColorOrderIndex',1)
plot(timeSteps,u1_init(:),'--',timeSteps,u2_init(:),'--',timeSteps,u3_init(:),'--','Linewidth',2)
xlabel('Time [s]','Fontsize',16)
ylabel('Commands','Fontsize',16)
legend('Joint 1 [N]','Joint 2 [Nm]','Joint 3 [Nm]','Joint 1 rigid} [N]','Joint 2 rigid} [Nm]','Joint 3 rigid} [Nm]','Location','Best')
% title('Commands of an underactuated cart system','Fontsize',18)
grid on

RelError = zeros(size(timeSteps));
RelErrorR = zeros(size(timeSteps));
for i = 1:length(RelError)
    RelError(i) = 100*(norm([trajx(i)-x(i) trajy(i)-y(i)])/norm([trajx(i) trajy(i)]));
    
    RelErrorR(i) = 100*(norm([trajx(i)-xr(i) trajy(i)-yr(i)])/norm([trajx(i) trajy(i)]));
end
figure
plot(timeSteps,RelError,timeSteps,RelErrorR)
title('Relative error of the direct dynamic trajectory','Fontsize',13)
xlabel('time')
ylabel('Relative Error (%)')
legend('Flex','Rigid')
grid on

RMSRelError = sqrt(mean(RelError.^2))
MaxRelError = max(RelError)
RMSRelErrorR = sqrt(mean(RelErrorR.^2))
MaxRelErrorR = max(RelErrorR)

joint1 = Model.listElementVariables{1};
joint2 = Model.listElementVariables{2};
joint3 = Model.listElementVariables{3};
joint1_init = Modelr.listElementVariables{1};
joint2_init = Modelr.listElementVariables{2};
joint3_init = Modelr.listElementVariables{3};

figure
hold on
plot(timeSteps,joint1.relCoo,timeSteps,joint2.relCoo,timeSteps,joint3.relCoo,'Linewidth',2)
set(gca, 'ColorOrderIndex',1)
plot(D.parameters.time,joint1_init.relCoo,'--',D.parameters.time,joint2_init.relCoo,'--',D.parameters.time,joint3_init.relCoo,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints angle (rad)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid','Location', 'Best')
% title('Joints angle of a flexible Robot','Fontsize',18)
grid on


figure
hold on
plot(timeSteps,joint1.velocity,timeSteps,joint2.velocity,timeSteps,joint3.velocity,'Linewidth',2)
set(gca, 'ColorOrderIndex',1)
plot(D.parameters.time,joint1_init.velocity,'--',D.parameters.time,joint2_init.velocity,'--',D.parameters.time,joint3_init.velocity,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints velocity (rad/s)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid')
title('Joints velocity','Fontsize',18)
grid on

figure
hold on
plot(timeSteps,joint1.acceleration,timeSteps,joint2.acceleration,timeSteps,joint3.acceleration,'Linewidth',2)
set(gca, 'ColorOrderIndex',1)
plot(D.parameters.time,joint1_init.acceleration,'--',D.parameters.time,joint2_init.acceleration,'--',D.parameters.time,joint3_init.acceleration,'--','Linewidth',2)
xlabel('Time (s)','Fontsize',16)
ylabel('Joints accelerations (rad/s^2)','Fontsize',16)
legend('Joint 1','Joint 2','Joint 3','Joint 1 rigid','Joint 2 rigid','Joint 3 rigid')
title('Joints accelerations','Fontsize',18)
grid on

