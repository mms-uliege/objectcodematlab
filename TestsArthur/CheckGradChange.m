%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GradConstVariationCheck
% Check of how much the constraint changes in the computation of the
% gradient with constant hyp or not.
clear all
close all
% clear Model
% clear elements
% clear nodes

finaltime = 1;
timestepsize = 0.1;
time = 0:timestepsize:finaltime;
npts = finaltime/timestepsize +1;
t_i = 0.1;
t_f = finaltime-t_i;

hypForComp = 'notConstant';
% hypForComp = 'constant';
nPass = 0;

grav = [0 0 -9.81];

l1 = 1;
l2 = 1;
angle = 30*pi/180;
nodes = [1 0 0 0;
         2 0 0 0;
         3 l1*cos(angle) 0 l1*sin(angle);
         4 l1*cos(angle) 0 l1*sin(angle);
         5 l1*cos(angle)+l2*cos(angle) 0 0];
     
%  nodes = [1 0 0 0;...
%      2 0 0 0;...
%      3 1 0 0];

nElem1 = 1;
Start1 = 2;
nodes = createInterNodes(nodes,nElem1,Start1,Start1+1);

nElem2 = 1;
Start2 = Start1+nElem1+1;
nodes = createInterNodes(nodes,nElem2,Start2,Start2+1);

stiff = 0;
damp = 0;
coulomb = 0;

kp = 200;
kd = 5;

E = 210e9; % link modulus in N/m�
l = 1; % link length in m
w = 0.015; % link width in m
h = 0.004; % link height in m
rho = 7800; % mass density in kg/m�
A = 2.09e-4; % cross section area of the link in m�
m = l * A * rho; % mass of link in kg
% J = diag([m*(w^2 + h^2)/12 m*(l^2 + h^2)/12 m*(l^2 + w^2)/12]); % inertia in kg*m�
J = diag([2.18e-4 0.08374 0.08374]); % inertia in kg*m�
nu = 0.27; % poisson coef
G = E/(2*(1+nu)); % compression modulus
% Ixx = (w*h)*(w^2 + h^2)/12; % area moment inertia in case of beam in m^4
% Iyy = (w*h^3)/12; % area moment inertia in case of beam in m^4
% Izz = (w^3*h)/12; % area moment inertia in case of beam in m^4
Ixx = 1.76e-8; % area moment inertia in case of beam in m^4
Iyy = 1.01e-8; % area moment inertia in case of beam in m^4
Izz = 0.75e-8; % area moment inertia in case of beam in m^4
KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

count = 0;
for i = 1:nElem1
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start1-1+i Start1+i];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
    elements{count+i}.g = grav;
end

count = size(elements,2);
for i = 1:nElem2
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start2-1+i Start2+i];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
    elements{count+i}.g = grav;
end

% count = 1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [Start1+nElem1/2 Start1+(0:nElem1/2-1) Start1+nElem1/2+(0:nElem1/2-1)+1];
% elements{count}.mass = m;
% elements{count}.J = J;
% elements{count}.g = grav;
% 
% count = size(elements,2)+1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [Start2+nElem2/2 Start2+(0:nElem2/2-1) Start2+nElem2/2+(0:nElem2/2-1)+1];
% elements{count}.mass = m;
% elements{count}.J = J;
% elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = 3;
elements{count}.g = grav;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.k = stiff;
elements{count}.d = damp;
elements{count}.coulomb = coulomb;
elements{count}.A = [0 0 0 0 1 0]';

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.k = stiff;
elements{count}.d = damp;
elements{count}.coulomb = coulomb;
elements{count}.A = [0 0 0 0 1 0]';

% count = size(elements,2)+1;
% elements{count}.type = 'ForceInKinematicConstraint';
% elements{count}.elements = count-2;
% elements{count}.elements = count-1;
% elements{count}.f = 0*ones(size(time));
% elements{count}.ref = pi/6*ones(size(time));
% elements{count}.dref = 0*ones(size(time));

x_end = 0;
z_end = -1.5;
trajx = lineTraj(nodes(end,2),x_end,time,t_i,t_f);
% trajy = lineTraj(nodes(end,3),y_end,time,t_i,t_f);
trajz = lineTraj(nodes(end,4),z_end,time,t_i,t_f);

count = size(elements,2)+1;
elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 0 1];
elements{count}.elements = [count-2 count-1];
elements{count}.active = 1;

% BC
BC = [1];

% Create Model

ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

% Solving
 
% D = DynamicIntegration(ModelR);
D = StaticIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
% D.parameters.rho = 0.01;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();

elements{count}.type = 'TrajectoryConstraint'; % need to be changed, because previously it was impossible to compute the "deltaq", since no variable _initOpti yet
% elements{count}.Axe = [1 0 0 0 0 0;...
%                        0 0 1 0 0 0];

ModelF = FEModel();
ModelF.CreateFEModel(nodes,elements);
ModelF.defineBC(BC);

for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
%     ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
%     ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
    dh = zeros(6,npts);
    for i = 1:npts
        dh(:,i) = logSE3([dimR(ModelF.listNodes{n}.R(:,i)) ModelF.listNodes{n}.position(:,i); 0 0 0 1]);
    end
    ModelF.listNodes{n}.velocity = deriveVal(dh,timestepsize);
    ModelF.listNodes{n}.acceleration = deriveVal(ModelF.listNodes{n}.velocity,timestepsize);
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
%         ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
%         ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
        ModelF.listElementVariables{n}.velocity = deriveVal(ModelF.listElementVariables{n}.relCoo,timestepsize);
        ModelF.listElementVariables{n}.acceleration = deriveVal(ModelF.listElementVariables{n}.velocity,timestepsize);
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

S = DirectTranscriptionOpti(ModelF);
S.npts = finaltime/timestepsize+1;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = timestepsize;
S.parameters.rho = 0.01;
S.ConstIter = hypForComp;
S.nPass = nPass;
% S.parameters.relTolRes = 1e-10;
S.parameters.scaling = 1e6;


S.parameters.InitialComputation();

for i = S.model.listNumberDof
    S.model.listDof{i}.InitializeD(D.parameters.nstep);
end
for i = S.model.listNumberElementVariables
    S.model.listElementVariables{i}.InitializeD(D.parameters.nstep);
end
for i = S.model.listNumberElements
    S.model.listElements{i}.InitializeD(D.parameters.nstep);
end

% Selection of the discrete time steps according to the number of npts
S.timeValues = zeros(1,S.npts);
S.timesteps = zeros(1,S.npts);
for k = 1:S.npts
    S.timeValues(k) = (k-1)*S.parameters.finaltime/(S.npts-1);
    S.timesteps(k) = find(D.parameters.time>=S.timeValues(k),1);
    if k~= 1 && abs(S.timeValues(k)-D.parameters.time(S.timesteps(k)-1))< 1e-6
        S.timesteps(k) = S.timesteps(k)-1;
    end
end

S.LocMotionDof = S.model.listFreeDof(1:S.nCoord);

% Saves the constant (over time and iteration) mass matrix (not like K,C
% that are can be constant over iter but not along time).
M = S.model.getTangentMatrix_Opti('Mass',S.timesteps(1),S.parameters.scaling);
S.M_Opti = M(S.LocMotionDof,S.LocMotionDof);

% Copying every trajectory information into a vector that has
% the same size in the first guess and in the optimization
% (inserting 0s between trajectory values)
for nE = 1:length(S.model.listElements)
    if isa(S.model.listElements{nE},'TrajectoryConstraint')
        T = zeros(size(S.model.listElements{nE}.T,1),length(D.parameters.time));
        T(:,S.timesteps) = S.model.listElements{nE}.T(:,1:length(S.timesteps));
%                     T(:,S.timesteps) = S.model.listElements{nE}.T;
        S.model.listElements{nE}.T = T;
    end
end

% Making a list of flexible elements that will be needed in the
% Sective function.
S.listFlexBeamElem = [];
for nE = 1:length(S.model.listElements)
    if isa(S.model.listElements{nE},'FlexibleBeamElement')
        S.listFlexBeamElem = [S.listFlexBeamElem nE];
    end
end            

x_0 = zeros(S.nVar*S.npts,1);
nfixDof = length(S.model.listFixedDof);
for k = 1:S.npts
    for var = S.model.listNodes
        if isempty(find(S.model.listFixedNodes==var{1}.numberNode))
            locV = S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
            locVdot = 2*S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
            locA = 3*S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
            x_0(locV) = var{1}.velocity_InitOpti(:,S.timesteps(k));
            x_0(locVdot) = var{1}.acceleration_InitOpti(:,S.timesteps(k));
            x_0(locA) = var{1}.acceleration_InitOpti(:,S.timesteps(k));
        end
    end
    for var = S.model.listElementVariables
        if strcmp(var{1}.DofType,'MotionDof')
            locV = S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
            locVdot = 2*S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
            locA = 3*S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
            x_0(locV) = var{1}.velocity_InitOpti(:,S.timesteps(k));
            x_0(locVdot) = var{1}.acceleration_InitOpti(:,S.timesteps(k));
            x_0(locA) = var{1}.acceleration_InitOpti(:,S.timesteps(k));
        else
            locValue = 3*S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
            x_0(locValue) = var{1}.value_InitOpti(:,S.timesteps(k));
        end
    end

end

% Load pre-simulated optimization variables at every iteration
load('C:\ObjectCodeMatlab\TestsArthur\IntermediateIterationVariables\IntermediateVar.mat')
load('C:\ObjectCodeMatlab\TestsArthur\IntermediateIterationVariables\IntermediateVarConstant.mat')
% load('C:\ObjectCodeMatlab\TestsArthur\IntermediateIterationVariables\IntermediateVarConstantnPass.mat')
nIter = size(value,2);
nIterConstant = size(valueConstant,2);
% nIterConstantnPass = size(valueConstantnPass,2);
tol = 1e-3;

gradceqIteration = [];
gradceqIterationConst = [];
for i = 1:min(nIter,nIterConstant)
%     diff = value(:,i)-valueConstant(:,i);
    [cConst, ceqConst, gradcConst, gradceqConst] = directTranscriptionOptiNLConstraintsConst(valueConstant(:,i), S);
    [c, ceq, gradc, gradceq] = directTranscriptionOptiNLConstraints(value(:,i), S);
    diff = (gradceq-gradceqConst);
%     spy(abs(diff)>tol,10)
%     drawnow
    disp(['Iteration ',num2str(i)])
%     pause()
    gradceqIteration(:,:,i) = gradceq;
    gradceqIterationConst(:,:,i) = gradceqConst;
end

% for i = 1:min(nIter,nIterConstantnPass)
% %     diff = value(:,i)-valueConstant(:,i);
% %     [cConstnPass, ceqConstnPass, gradcConstnPass, gradceqConstnPass] = directTranscriptionOptiNLConstraintsConst(value(:,i), S);
%     [cConstnPass, ceqConstnPass, gradcConstnPass, gradceqConstnPass] = directTranscriptionOptiNLConstraintsConst(valueConstantnPass(:,i), S);
%     [c, ceq, gradc, gradceq] = directTranscriptionOptiNLConstraints(value(:,i), S);
%     diffnPass = (gradceq-gradceqConstnPass);
% %     spy(abs(diffnPass)>tol,10)
% %     drawnow
%     disp(['Iteration ',num2str(i)])
% %     pause()
%     % check the change in the constraint when it is calculated at each
%     % iteration, see if the change is actually important.
%     gradceqIteration(:,:,i) = gradceq;
%     gradceqIterationConst(:,:,i) = gradceqConstnPass;
% end
% Check max difference between iteration (with only one hypothesis)
for i = 2:nIter 
%     diffIter = gradceqIteration(:,:,i)-gradceqIteration(:,:,i-1);
    firstLineCol = gradceqIteration(S.model.listFixedDof,S.model.listFixedDof,i);
    diffIter = gradceqIterationConst(:,:,i)-gradceqIterationConst(:,:,i-1);
    %  Difference in the stiffness matrix with NON constant assumption at
    %  first time step
    diffKmatrix = gradceqIteration(S.LocMotionDof,S.LocMotionDof,i) - gradceqIteration(S.LocMotionDof,S.LocMotionDof,i-1);
    %  Difference in the stiffness matrix with constant assumption at first
    %  time step
    diffKmatrixConst = gradceqIterationConst(S.LocMotionDof,S.LocMotionDof,i) - gradceqIterationConst(S.LocMotionDof,S.LocMotionDof,i-1);
    %  Difference in the stiffness matrix with NON constant assumption at
    %  first time step
    diffCmatrix = gradceqIteration(S.LocMotionDof,S.LocMotionDof,i) - gradceqIteration(S.LocMotionDof,S.LocMotionDof,i-1);
    %  Difference in the damping matrix with constant assumption at first
    %  time step
    diffCmatrixConst = gradceqIterationConst(S.LocMotionDof,S.LocMotionDof,i) - gradceqIterationConst(S.LocMotionDof,S.LocMotionDof,i-1);
%     spy(abs(diffIter)>tol,10)
%     drawnow
    diffMax = max(max(diffIter));
    diffMean = mean(max(diffIter));
    maxMaxFirst = max(max(abs(firstLineCol)));
    diffMaxKmatrix = max(max(diffKmatrix));
    diffMeanKmatrix = mean(max(diffKmatrix));
    diffMaxKmatrixConst = max(max(diffKmatrixConst));
    diffMeanKmatrixConst = mean(max(diffKmatrixConst));

%     disp(['Between iteration ',num2str(i),' and ',num2str(i-1),', the max diff is ',num2str(diffMax),' and the mean diff is ',num2str(diffMean)])
    disp(['Between iteration ',num2str(i),' and ',num2str(i-1),', the max diff in the K matrix is ',num2str(diffMaxKmatrix),' and the mean diff is ',num2str(diffMeanKmatrix)])
    disp(['Between iteration ',num2str(i),' and ',num2str(i-1),', the max diff in the K matrix (constant) is ',num2str(diffMaxKmatrixConst),' and the mean diff is ',num2str(diffMeanKmatrixConst)])
%     disp(['At iteration ',num2str(i),', the max value in fixed dof is ',num2str(maxMaxFirst)])
%     pause()
end
% xSol = S.runOpti(D);


