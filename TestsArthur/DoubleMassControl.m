%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Basic test of the control of a point mass on a spring

close all
clear

finaltime = 1;
timestepsize = 0.01;
npts = finaltime/timestepsize + 1;
nPass = 0;

computeDirectDyn = true;
testGrad = false;

flex_damp = 0;
flex_stiff = 2000;
rigi_damp = 0;
rigi_stiff = 2000;
mass = 1;

hypForPreComp = 'notConstant';
hypForComp = 'notConstant';

t_i = 0.2;
t_f = 0.8;
% Nodes definition
nodes = [1 0 0 0;...
         2 0 0 0;...
         3 0 0 0.5;
         4 0 0 1];
     
grav = [0 0 -0];

% Elements definition
count = 1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = mass;
elements{count}.g = grav;
count = count+1;

elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end-1,1)];
elements{count}.mass = mass/2;
elements{count}.g = grav;
count = count+1;

elements{count}.type = 'LinSpringDamperElement';
elements{count}.nodes = [2 nodes(end-1,1)];
elements{count}.stiffness = rigi_stiff*2;
elements{count}.damping = rigi_damp*2;
% elements{count}.natural_length = 1;
elements{count}.A = [0 0 1];
count = count +1;

elements{count}.type = 'LinSpringDamperElement';
elements{count}.nodes = [nodes(end-1,1) nodes(end,1)];
elements{count}.stiffness = rigi_stiff;
elements{count}.damping = rigi_damp;
% elements{count}.natural_length = 1;
elements{count}.A = [0 0 1];
count = count +1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [2 nodes(end,1)];
% elements{count}.mass = 0;
% elements{count}.g = grav;
% count = count+1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 1 0 0 0]';
% elements{count}.A = [0 0 0 0 0 0]';
count = count+1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 nodes(end-1,1)];
elements{count}.A = [0 0 1 0 0 0]';
% elements{count}.A = [0 0 0 0 0 0]';
count = count+1;

elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [nodes(end-1,1) nodes(end,1)];
elements{count}.A = [0 0 1 0 0 0]';
% elements{count}.A = [0 0 0 0 0 0]';
count = count+1;


% Trajectory
r=2;
z_end = nodes(end,4);

timeVector = 0:timestepsize:finaltime;
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajz];
elements{count}.Axe = [0 0 1];
elements{count}.elements = [count-3];
elements{count}.active = 1;

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.3;
D.parameters.scaling = 1e6;
D.runIntegration();

% lagrange = ModelR.listElementVariables{3}.value(2,:);
command = ModelR.listElementVariables{end}.value;
endPos = ModelR.listNodes{end}.position;
firstNodePos = ModelR.listNodes{end-2}.position;

figure
plot(D.parameters.time,endPos(3,:),D.parameters.time,firstNodePos(3,:))
legend('End node Position', 'Command node position')
xlabel('time (s)')
ylabel('position z (m)')

figure
plot(D.parameters.time,command)
xlabel('time (s)')
ylabel('Command (N)')

%% Flexible Model

count = 1;
elementsFlex{count}.type = 'RigidBodyElement';
elementsFlex{count}.nodes = [nodes(end,1)];
elementsFlex{count}.mass = mass;
elementsFlex{count}.g = grav;
count = count+1;

elementsFlex{count}.type = 'RigidBodyElement';
elementsFlex{count}.nodes = [nodes(end-1,1)];
elementsFlex{count}.mass = mass/2;
elementsFlex{count}.g = grav;
count = count+1;

elementsFlex{count}.type = 'LinSpringDamperElement';
elementsFlex{count}.nodes = [2 nodes(end-1,1)];
elementsFlex{count}.stiffness = flex_stiff*2;
elementsFlex{count}.damping = flex_damp*2;
% elements{count}.natural_length = 1;
elementsFlex{count}.A = [0 0 1];
count = count +1;
% elementsFlex{count}.type = 'RigidBodyElement';
% elementsFlex{count}.nodes = [2 nodes(end,1)];
% elementsFlex{count}.mass = 0;
% elementsFlex{count}.g = grav;
% count = count+1;

elementsFlex{count}.type = 'LinSpringDamperElement';
elementsFlex{count}.nodes = [nodes(end-1,1) nodes(end,1)];
elementsFlex{count}.stiffness = flex_stiff;
elementsFlex{count}.damping = flex_damp;
% elements{count}.natural_length = 1;
elementsFlex{count}.A = [0 0 1];
count = count +1;


elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [1 2];
elementsFlex{count}.A = [0 0 1 0 0 0]';
% elements{count}.A = [0 0 0 0 0 0]';
count = count+1;

elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [2 3];
elementsFlex{count}.A = [0 0 1 0 0 0]';
% elements{count}.A = [0 0 0 0 0 0]';
count = count+1;

elementsFlex{count}.type = 'KinematicConstraint';
elementsFlex{count}.nodes = [3 nodes(end,1)];
elementsFlex{count}.A = [0 0 1 0 0 0]';
% elementsFlex{count}.A = [0 0 0 0 0 0]';
count = count+1;

% Trajectory
r=2;
z_end = nodes(end,4);

timeVector = 0:timestepsize:finaltime;
trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

elementsFlex{count}.type = 'TrajectoryConstraint';
elementsFlex{count}.nodes = [nodes(end,1)];
elementsFlex{count}.T = [trajz];
elementsFlex{count}.Axe = [0 0 1];
elementsFlex{count}.elements = [count-3];
elementsFlex{count}.active = 1;

% Solving
ModelF = FEModel();
ModelF.CreateFEModel(nodes,elementsFlex);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from rigid solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

% First part optimization to compute the equilibrium solution for the first
% preactuation phase. Then copy this preactuation phase, to have full initial guess equilibrium to compute full
% optimization.
tic
% EndPreActStep = t_i/(finaltime/(npts-1))+1;
% EndPreActStep = round((t_i/(finaltime/(npts-1))+1)*0.47);
EndPreTime = t_i*0.47;
EndPreActStep = round(EndPreTime/(finaltime/(npts-1))+1);
S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = 0.3;
S.npts = EndPreActStep+1;
S.parameters.finaltime = EndPreTime;
S.parameters.timestepsize = finaltime/(npts-1);
S.parameters.scaling = 1e6;
S.linConst = false;
S.ConstIter = hypForPreComp;
S.nPass = 4;
xSol = S.runOpti(D);

% Copy the state at the end of preactuation phase to start as new initial
% guess.
% Redefine the trajectory that was cut to only the preactuation phase
ModelF.listElements{end}.T = [trajz];
% To have a smooth position, vel and acc, during the first time steps,
% search for the moment the position of the system is equal to the equilibrium point of the flex system.
% We then set the system at this point as initial guess.
tolEqui = 1e-4; % tolerence at which we consider that the flexible system is at equilibrium with respect to the static equilibrium in the preactuation phase
for n = ModelF.listNumberNodes
    for c = (EndPreActStep+1):size(ModelF.listNodes{n}.position,2)
        if norm(ModelF.listNodes{n}.position(:,c)-ModelF.listNodes{n}.position(:,EndPreActStep))<tolEqui
            Step2 = c; % Copy the values until the time step where we are at the same position... not sure it actually works... supress this 
            break
        end
    end
    for step = EndPreActStep+1:Step2
        ModelF.listNodes{n}.R_InitOpti(:,step) = ModelF.listNodes{n}.R(:,EndPreActStep);
        ModelF.listNodes{n}.position_InitOpti(:,step) = ModelF.listNodes{n}.position(:,EndPreActStep);
        ModelF.listNodes{n}.velocity_InitOpti(:,step) = ModelF.listNodes{n}.velocity(:,EndPreActStep);
        ModelF.listNodes{n}.acceleration_InitOpti(:,step) = ModelF.listNodes{n}.acceleration(:,EndPreActStep);
    end
%     ModelF.listNodes{n}.InitializeD_Opti();
end
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        for c = (EndPreActStep+1):size(ModelF.listElementVariables{n}.relCoo,2)
            if norm(ModelF.listElementVariables{n}.relCoo(:,c)-ModelF.listElementVariables{n}.relCoo(:,EndPreActStep))<tolEqui
                Step2 = c;
                break
            end
        end
        for step =  EndPreActStep+1:Step2
            ModelF.listElementVariables{n}.R_InitOpti(:,step) = ModelF.listElementVariables{n}.R(:,EndPreActStep);
            ModelF.listElementVariables{n}.x_InitOpti(:,step) = ModelF.listElementVariables{n}.x(:,EndPreActStep);
            ModelF.listElementVariables{n}.velocity_InitOpti(:,step) = ModelF.listElementVariables{n}.velocity(:,EndPreActStep);
            ModelF.listElementVariables{n}.acceleration_InitOpti(:,step) = ModelF.listElementVariables{n}.acceleration(:,EndPreActStep);
            ModelF.listElementVariables{n}.relCoo_InitOpti(:,step) = ModelF.listElementVariables{n}.relCoo(:,EndPreActStep);
        end
    else
        for c = (EndPreActStep+1):size(ModelF.listElementVariables{n}.value,2)
            if norm(ModelF.listElementVariables{n}.value(:,c)-ModelF.listElementVariables{n}.value(:,EndPreActStep))<tolEqui
                Step2 = c;
                break
            end
        end
        for step =  EndPreActStep+1:Step2
            ModelF.listElementVariables{n}.value_InitOpti(:,step) = ModelF.listElementVariables{n}.value(:,EndPreActStep); 
        end
    end
%     ModelF.listElementVariables{n}.InitializeD_Opti();
end

% Start second full optimization with newly computed initial guess

S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = 0.3;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.parameters.scaling = 1e6;
S.linConst = false;
S.ConstIter = hypForComp;
S.nPass = nPass;
% analys = S.runPoleAnalysisOpti(D)
if testGrad~=true
    xSol = S.runOpti(D);
end
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])
if testGrad~=true
    analys = poleAnalysisOpti(S)
end

commandF = ModelF.listElementVariables{end}.value;
endPosF = ModelF.listNodes{end}.position;
firstNodePosF = ModelF.listNodes{end-2}.position;
figure
plot(S.parameters.time,endPosF(3,:),S.parameters.time,firstNodePosF(3,:))
legend('End node Position', 'Command node position')
xlabel('time (s)')
ylabel('position z (m)')
%% DirectDynamic tests


if computeDirectDyn == true
    newNodes = nodes;
%     for l = 1:size(newNodes,1)
%         newNodes(l,2:4) = ModelF.listNodes{l}.position(:,1)';
%         for c = 2:size(newNodes,2)
%             if newNodes(l,c)<= 1e-6
%                 newNodes(l,c)=0;
%             end
%         end
%     end
%     figure
%     hold on
%     plot3(newNodes(:,2),newNodes(:,3),newNodes(:,4),'*r')
%     plot3(nodes(:,2),nodes(:,3),nodes(:,4),'ob')
    
    
    count = 1;
    elementsDirDyn{count}.type = 'RigidBodyElement';
    elementsDirDyn{count}.nodes = [nodes(end,1)];
    elementsDirDyn{count}.mass = mass;
    elementsDirDyn{count}.g = grav;
    count = count+1;

    elementsDirDyn{count}.type = 'LinSpringDamperElement';
    elementsDirDyn{count}.nodes = [2 nodes(end,1)];
    elementsDirDyn{count}.stiffness = flex_stiff;
    elementsDirDyn{count}.damping = flex_damp;
    % elementsDirDyn{count}.natural_length = 1;
    elementsDirDyn{count}.A = [0 0 1];
    count = count +1;
%     elementsDirDyn{count}.type = 'RigidBodyElement';
%     elementsDirDyn{count}.nodes = [2 nodes(end,1)];
%     elementsDirDyn{count}.mass = 0;
%     elementsDirDyn{count}.g = grav;
%     count = count+1;


    elementsDirDyn{count}.type = 'KinematicConstraint';
    elementsDirDyn{count}.nodes = [1 2];
    elementsDirDyn{count}.A = [0 0 1 0 0 0]';
    % elementsDirDyn{count}.A = [0 0 0 0 0 0]';
    count = count+1;

    elementsDirDyn{count}.type = 'KinematicConstraint';
    elementsDirDyn{count}.nodes = [2 3];
    elementsDirDyn{count}.A = [0 0 1 0 0 0]';
    % elementsDirDyn{count}.A = [0 0 0 0 0 0]';
    count = count+1;

    % Trajectory
    r=2;
    z_end = nodes(end,4);

    timeVector = 0:timestepsize:finaltime;
    trajz = halfCircleTraj(nodes(end,4),z_end,r,timeVector,'cos',t_i,t_f);

    u1_init = interp1(D.parameters.time,ModelR.listElementVariables{end}.value,timeVector,'linear');

    u1 = interp1(S.timeValues,ModelF.listElementVariables{end}.value,timeVector,'linear');

    elementsDirDyn{count}.type = 'ForceInKinematicConstraint';
    elementsDirDyn{count}.elements = [count-2];
    elementsDirDyn{count}.f = u1;

    % Boundary Condition
    BC = [1];

    % Model Def
    ModelFD = FEModel();% Flex model
    ModelFD.CreateFEModel(newNodes,elementsDirDyn);
    ModelFD.defineBC(BC);

    ModelRD = FEModel();% Rigid model
    ModelRD.CreateFEModel(newNodes,elementsDirDyn);
    ModelRD.defineBC(BC);

    % Solving Flexible
    D = DynamicIntegration(ModelFD);
    D.parameters.finaltime = finaltime;
    D.parameters.timestepsize = timestepsize;
    D.parameters.rho = 0.3;
    D.parameters.scaling = 1e6;
    D.runIntegration();

    % Solving Rigid
    ModelRD.listElements{count}.f = u1_init;

    DR = DynamicIntegration(ModelRD);
    DR.parameters.finaltime = finaltime;
    DR.parameters.timestepsize = timestepsize;
    DR.parameters.rho = 0.3;
    DR.parameters.scaling = 1e6;
    DR.runIntegration();

    %% Plots

    z = ModelFD.listNodes{end}.position(3,:);

    zR = ModelRD.listNodes{end}.position(3,:);

    figure
    hold on
    plot(z, '-o','Linewidth',2, 'Color','r')
    plot(zR,'--', 'Linewidth',2, 'Color','b')
    plot(trajz, 'Linewidth',2, 'Color','k')
    legend('With u','With u_{rigid}','Prescribed')
    zlabel('Z [m]','Fontsize',16)
    grid on

    figure
    hold on
    plot(timeVector,z)
    plot(timeVector,trajz,'--')
    legend('Z','Zd')
    grid on

    joint1_init = ModelRD.listElementVariables{1};

    joint1 = ModelFD.listElementVariables{1};

    figure
    hold on
    plot(timeVector,joint1.relCoo,'Linewidth',2)
    plot(timeVector,joint1_init.relCoo,'--','Linewidth',2)
    xlabel('Time (s)','Fontsize',16)
    ylabel('Joints pos (m)','Fontsize',16)
    legend('Joint 1','Joint 1 rigid','Location', 'Best')
    grid on

    figure
    hold on
    plot(timeVector,u1,'Linewidth',2)
    plot(timeVector,u1_init(:),'--','Linewidth',2)
    xlabel('Time [s]','Fontsize',16)
    ylabel('Commands [N]','Fontsize',16)
    legend('u_1','u_{1,rigid}','Location','Best')
    grid on

    RelError = zeros(size(timeVector));
    RelErrorR = zeros(size(timeVector));
    for i = 1:length(RelError)
        RelError(i) = 100*((norm([trajz(i)-z(i)]))/(norm([trajz(i)])));
        RelErrorR(i) = 100*((norm([trajz(i)-zR(i)]))/(norm([trajz(i)])));
    end

    RMSRelError = sqrt(mean(RelError.^2))
    MaxRelError = max(RelError)
    RMSRelErrorR = sqrt(mean(RelErrorR.^2))
    MaxRelErrorR = max(RelErrorR)
    
end

if testGrad == true
    S.parameters.InitialComputation();

    for i = S.model.listNumberDof
        S.model.listDof{i}.InitializeD(D.parameters.nstep);
    end
    for i = S.model.listNumberElementVariables
        S.model.listElementVariables{i}.InitializeD(D.parameters.nstep);
    end
    for i = S.model.listNumberElements
        S.model.listElements{i}.InitializeD(D.parameters.nstep);
    end

    % Selection of the discrete time steps according to the number of npts
    S.timeValues = zeros(1,S.npts);
    S.timesteps = zeros(1,S.npts);
    for k = 1:S.npts
        S.timeValues(k) = (k-1)*S.parameters.finaltime/(S.npts-1);
        S.timesteps(k) = find(D.parameters.time>=S.timeValues(k),1);
        if k~= 1 && abs(S.timeValues(k)-D.parameters.time(S.timesteps(k)-1))< 1e-6
            S.timesteps(k) = S.timesteps(k)-1;
        end
    end

    S.LocMotionDof = S.model.listFreeDof(1:S.nCoord);

    % Saves the constant (over time and iteration) mass matrix (not like K,C
    % that are can be constant over iter but not along time).
    M = S.model.getTangentMatrix_Opti('Mass',S.timesteps(1),S.parameters.scaling);
    S.M_Opti = M(S.LocMotionDof,S.LocMotionDof);

    % Copying every trajectory information into a vector that has
    % the same size in the first guess and in the optimization
    % (inserting 0s between trajectory values)
    for nE = 1:length(S.model.listElements)
        if isa(S.model.listElements{nE},'TrajectoryConstraint')
            T = zeros(size(S.model.listElements{nE}.T,1),length(D.parameters.time));
            T(:,S.timesteps) = S.model.listElements{nE}.T(:,1:length(S.timesteps));
    %                     T(:,S.timesteps) = S.model.listElements{nE}.T;
            S.model.listElements{nE}.T = T;
        end
    end

    % Making a list of flexible elements that will be needed in the
    % Sective function.
    S.listFlexBeamElem = [];
    for nE = 1:length(S.model.listElements)
        if isa(S.model.listElements{nE},'FlexibleBeamElement')
            S.listFlexBeamElem = [S.listFlexBeamElem nE];
        end
    end            

    x_0 = zeros(S.nVar*S.npts,1);
    nfixDof = length(S.model.listFixedDof);
    for k = 1:S.npts
        for var = S.model.listNodes
            if isempty(find(S.model.listFixedNodes==var{1}.numberNode))
                locV = S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
                locVdot = 2*S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
                x_0(locV) = var{1}.velocity_InitOpti(:,S.timesteps(k));
                x_0(locVdot) = var{1}.acceleration_InitOpti(:,S.timesteps(k));
            end
        end
        for var = S.model.listElementVariables
            if strcmp(var{1}.DofType,'MotionDof')
                locV = S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
                locVdot = 2*S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
                x_0(locV) = var{1}.velocity_InitOpti(:,S.timesteps(k));
                x_0(locVdot) = var{1}.acceleration_InitOpti(:,S.timesteps(k));
            else
                locValue = 3*S.nCoord + (k-1)*S.nVar + (var{1}.listNumberDof - nfixDof);
                x_0(locValue) = var{1}.value_InitOpti(:,S.timesteps(k));
            end
        end

    end

    sizeMotionConst = S.nCoord + S.nLagrangeM + S.nControls;
    sizeIntregrationConst = 3*S.nCoord;

    delta = 1e-2; % Perturbation size
    x_i = x_0;
    Diff_gradceq = zeros(S.nVar*S.npts, S.npts*(sizeMotionConst + sizeIntregrationConst) - sizeIntregrationConst);
    [c, ceq, gradc, gradceq] = directTranscriptionOptiNLConstraints(x_i, S);
    for i = 1:length(x_i)
        x_pert = zeros(size(x_i));
        x_pert(i) = delta;
    %     [c_pertm, ceq_pertm] = NLConstraints(x_i-x_pert, S);
    %     [c_pert, ceq_pert] = NLConstraints(x_i+x_pert, S);
    %     line=(ceq_pertm-ceq_pert)'/(2*delta);
        [c_pert, ceq_pert] = NLConstraints(x_i+x_pert, S);
        line=(ceq-ceq_pert)'/delta;
        Diff_gradceq(i,:) = -line;
        if i==floor(length(x_i)/2)
            disp('halfway there...')
        end
        if i==floor(3*length(x_i)/4)
            disp('almost there...')
        end
        if i==length(x_i)
            disp('Done !')
        end
        spy(Diff_gradceq',10)
        drawnow
    end
    % figure
    % spy(Diff_gradceq,10)
    % figure
    % spy(gradceq,10)
    Diff = Diff_gradceq'-gradceq';
    tol = 1e-4;
    figure
    spy(abs(Diff)>tol,10)

    Tb = 2;
    sE = (sizeMotionConst + sizeIntregrationConst);
    bloc0 = Diff(1:sE,1:2*S.nVar);
    blocM = Diff(Tb*(sE)+1:(Tb+1)*(sE),Tb*S.nVar+1:(Tb+2)*S.nVar);

    figure
    spy(abs(bloc0)>tol,10)
    title('Gradient de contrainte au temps initial','Fontsize',14)
    [l,c] = find(abs(bloc0)>tol);
    disp('Pour le bloc au temps 0')
    for equLin = 1:length(l)
        if l(equLin)<=S.nCoord
            if c(equLin)<=S.nCoord
                disp('Problem dans K...')
            elseif c(equLin)<=2*S.nCoord && c(equLin)>S.nCoord
                disp('Problem dans C...')
            elseif c(equLin)<=3*S.nCoord && c(equLin)>2*S.nCoord
                disp('Problem dans M...')
            elseif c(equLin)<=(3*S.nCoord+S.nLagrangeM) && c(equLin)>3*S.nCoord
                disp('Problem dans LagrangeM...')
            elseif c(equLin)<=(3*S.nCoord+S.nLagrangeM+S.nControls) && c(equLin)>(3*S.nCoord+S.nLagrangeM)
                disp('Problem dans Controls...')
            else
                disp('oubli� qqch dans equ mouv...')
            end
        elseif l(equLin)<=(S.nCoord+S.nLagrangeM) && l(equLin)>S.nCoord
            disp('Problem dans contrainte cin...')
        elseif l(equLin)<=(sizeMotionConst) && l(equLin)>(S.nCoord+S.nLagrangeM)
            disp('Problem dans contrainte servo...')
        elseif l(equLin)<=(sizeMotionConst+S.nCoord) && l(equLin)>(sizeMotionConst)
            disp('Problem dans 1er equ integ...')
        elseif l(equLin)<=(sizeMotionConst+2*S.nCoord) && l(equLin)>(sizeMotionConst+S.nCoord)
            disp('Problem dans 2eme equ integ...')
        elseif l(equLin)<=(sizeMotionConst+3*S.nCoord) && l(equLin)>(sizeMotionConst+2*S.nCoord)
            disp('Problem dans 3eme equ integ...')
        else
            disp('oubli� qqch')
        end
    end
    figure
    spy(abs(blocM)>tol,10)
    title(['Gradient de contrainte au pas de temps ' num2str(Tb)],'Fontsize',14)

    [l,c] = find(abs(blocM)>tol);
    disp(['Pour le bloc au temps ',num2str(Tb)])
    for equLin = 1:length(l)
        if l(equLin)<=S.nCoord
            if c(equLin)<=S.nCoord
                disp('Problem dans K...')
            elseif c(equLin)<=2*S.nCoord && c(equLin)>S.nCoord
                disp('Problem dans C...')
            elseif c(equLin)<=3*S.nCoord && c(equLin)>2*S.nCoord
                disp('Problem dans M...')
            elseif c(equLin)<=4*S.nCoord && c(equLin)>3*S.nCoord
                disp('Problem dans alpha...')
            elseif c(equLin)<=(4*S.nCoord+S.nLagrangeM) && c(equLin)>4*S.nCoord
                disp('Problem dans LagrangeM...')
            elseif c(equLin)<=(4*S.nCoord+S.nLagrangeM+S.nControls) && c(equLin)>(4*S.nCoord+S.nLagrangeM)
                disp('Problem dans Controls...')
            else
                disp(['oubli� qqch dans equ mouv...',num2str(equLin)])
            end
        elseif l(equLin)<=(S.nCoord+S.nLagrangeM) && l(equLin)>S.nCoord
            disp('Problem dans contrainte cin...')
        elseif l(equLin)<=(sizeMotionConst) && l(equLin)>(S.nCoord+S.nLagrangeM)
            disp('Problem dans contrainte servo...')
        elseif l(equLin)<=(sizeMotionConst+S.nCoord) && l(equLin)>(sizeMotionConst)
            disp('Problem dans 1er equ integ...')
        elseif l(equLin)<=(sizeMotionConst+2*S.nCoord) && l(equLin)>(sizeMotionConst+S.nCoord)
            disp('Problem dans 2eme equ integ...')
        elseif l(equLin)<=(sizeMotionConst+3*S.nCoord) && l(equLin)>(sizeMotionConst+2*S.nCoord)
            disp('Problem dans 3eme equ integ...')
        else
            disp('oubli� qqch')
        end
    end
end
% plot(S.parameters.time,ModelF.listNodes{end}.velocity(3,:),D.parameters.time, ModelR.listNodes{end}.velocity(3,:))
% plot(S.parameters.time,ModelF.listNodes{end}.acceleration(3,:),D.parameters.time, ModelR.listNodes{end}.acceleration(3,:))
% plot(S.parameters.time,ModelF.listElementVariables{1}.acceleration,D.parameters.time, ModelR.listElementVariables{1}.acceleration)
% plot(S.parameters.time,ModelF.listElementVariables{1}.velocity,D.parameters.time, ModelR.listElementVariables{1}.velocity)
