%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute torque in the beam

load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\S13')

time = S.timeValues;
% time = D.parameters.time;
ntimeSteps = length(time);

computedVelocity = S.model.listElementVariables{2}.velocity;
computedTorque = S.model.listElementVariables{end}.value;
s = 1;
listBeam = [1 4];
Effort = cell(1,length(listBeam));
EffortNode1 = [];
select = [0 0 0 0 1 0];
% nStep = 1; % time step
for nStep = 1:ntimeSteps
    count = 1;
    for iBeam = listBeam;
        BeamElem = S.model.listElements{iBeam};
%         xA0 = BeamElem.listNodes{1}.position(:,1);
%         RA0 = dimR(BeamElem.listNodes{1}.R(:,1));
%         HA0m1 = [RA0' -RA0'*xA0; 0 0 0 1];
        HA0m1 = eye(4);
        xA = BeamElem.listNodes{1}.position(:,nStep);
        RA = dimR(BeamElem.listNodes{1}.R(:,nStep));
        HA = [RA xA; 0 0 0 1];
        HAm1 = [RA' -RA'*xA; 0 0 0 1];
        
        HB0 = eye(4);
        xB = BeamElem.listNodes{2}.position(:,nStep);
        RB = dimR(BeamElem.listNodes{2}.R(:,nStep));
        HB = [RB xB; 0 0 0 1];
        HBm1 = [RB' -RB'*xB; 0 0 0 1];

        hBeam = vectSE3(s*HAm1*HB);
        epsilon = (hBeam-BeamElem.h0)/BeamElem.L;

        T = computeTSE3inv(hBeam); 
        P = [hatSE3(hBeam)-T T];
        force = [select 0 0 0 0 0 0]*P'*BeamElem.KCS*epsilon;
%         force = -select*BeamElem.KCS*epsilon;
        Effort{count} = [Effort{count} force];
        count = count+1;
    end
end
    
figure
% plot(time,computedTorque(2,:),time,Effort{1}+Effort{2})
% plot(time,computedTorque(2,:),time,Effort{1},time,S.model.listElementVariables{5}.value(5,:))
% plot(time,computedTorque(2,:),time,Effort{1}-S.model.listElementVariables{5}.value(5,:))
% plot(time,computedTorque(2,:)+S.model.listElementVariables{5}.value(5,:),time,Effort{1})
plot(time,computedTorque(2,:),time,Effort{1}-(S.model.listElementVariables{5}.value(5,:)))
% plot(time,computedTorque(2,:),time,Effort{1})
title('Comparison of computed control 1 torque and torque in beam 1')
xlabel('Time [s]')
ylabel('Torque [Nm]')
legend('Computed', 'Measured','Lagrange')
grid on

% figure
% plot(time,computedVelocity)
% % title('Comparison of computed control 1 torque and torque in beam 1')
% xlabel('Time [s]')
% ylabel('Joint 2 velocity [rad/s]')
% grid on
    
figure
% plot(time,computedTorque(3,:),time,Effort{2})
% plot(time,computedTorque(3,:),time,Effort{2},time,S.model.listElementVariables{6}.value(5,:))
% plot(time,computedTorque(3,:)+S.model.listElementVariables{6}.value(5,:),time,Effort{2})
plot(time,computedTorque(3,:),time,Effort{2}-(S.model.listElementVariables{6}.value(5,:)))
% plot(time,Effort{1})
% plot(time,computedTorque(2,:))
title('Comparison of computed control 2 torque and torque in beam 4')
xlabel('Time [s]')
ylabel('Torque [Nm]')
legend('Computed', 'Measured')
grid on

% figure
% plot(time,computedTorque(3,:),time,Effort{3})
% % plot(time,Effort{1})
% % plot(time,computedTorque(2,:))
% title('Comparison of computed control 2 torque and torque in beam 3')
% xlabel('Time [s]')
% ylabel('Torque [Nm]')
% legend('Computed', 'Measured')
% grid on

% figure
% plot(time,computedTorque(3,:),time,Effort{4})
% % plot(time,Effort{1})
% % plot(time,computedTorque(2,:))
% title('Comparison of computed control 2 torque and torque in beam 4')
% xlabel('Time [s]')
% ylabel('Torque [Nm]')
% legend('Computed', 'Measured')
% grid on

% figure
% plot(time,computedTorque(2,:),time,EffortNode1)
% % plot(time,Effort{1}+Effort{2})
% % plot(time,computedTorque(2,:))
% title('Comparison of computed control 1 torque and torque in beam 1')
% xlabel('Time [s]')
% ylabel('Torque [Nm]')
% legend('Computed', 'Measured')
% grid on

    