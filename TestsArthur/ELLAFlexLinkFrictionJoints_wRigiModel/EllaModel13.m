%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test to try to get a control of 3 joints of the Ella robot of JKU Linz. WITH
% Visc JOINTS (ALL coulomb and viscous) and a dynamic Rigid computation at the beginning
% With line trajectory as for the actual Ella robot USING Cartesian TRAJECTORY.
% And end-effector mass is not exactly at the tip (6cm before)

% parameters comming from https://robfile.mechatronik.uni-linz.ac.at/doku.php?id=start:robots:ella:mechsetup
% close all
clear elements

finaltime = 2.4; % 2 seconds
timestepsize = 0.01;

t_i = 0.3; % time at end of pre-actuation
t_f = finaltime - 0.7; % time at begining of post-actuation

npts = round(finaltime/timestepsize + 1); % number of points for optimization
hypForPreComp = 'notConstant';
% hypForComp = 'constant';
hypForComp = 'notConstant';

% kp1 = 100;
% kd1 = 5;
% kp2 = 100;
% kd2 = 5;
% kp3 = 100;
% kd3 = 5;
kp1 = 0;
kd1 = 0;
kp2 = 0;
kd2 = 0;
kp3 = 0;
kd3 = 0;

grav = [0 0 -9.81]; % gravity in m/s�
% grav = [0 0 0]; % gravity in m/s�

endPoseInit = [1.4 0.75 -0.5]; % changed in the coordinate we have in our model
% angleLink0 = 0.4918; % rad, angle at the base (0.4918)
% angleLink1 = -0.1840; % angle of the first joint in rad (0.1840)
% angleLink2 = 0.9926; % angle of the second joint in rad (-0.9926)
angleLink0 = 0; % rad, angle at the base (0.4918)
angleLink1 = -pi/6; % angle of the first joint in rad (0.1840)
angleLink2 = pi/3; % angle of the second joint in rad (-0.9926)
% angleLink0 = 0; % rad, angle at the base (0) test when robot is facing front
% angleLink1 = -0.1840; % angle of the first joint in rad (0.1840)
% angleLink2 = 0.9926; % angle of the second joint in rad (-0.9926)

gear1 = 100;
gear2 = 100;
gear3 = 160;

% Joint damping, friction and stiffness
damp1 = 1.95*1e-3*gear1^2;
damp2 = 2.2*1e-3*gear2^2;
damp3 = 4.7699*1e-4*gear3^2;
% stif1 = 1.2*1e5;
% stif2 = 1.2*1e5;
% stif3 = 2.9*1e4;
coul1 = 0.2*gear1*0;  
coul2 = 0.14*gear2*0;
coul3 = 0.07*gear3*0;
% damp1 = 0;
% damp2 = 0;
% damp3 = 0;
% coul1 = 0;
% coul2 = 0;
% coul3 = 0;
stif1 = 0;
stif2 = 0;
stif3 = 0;
coulombParam = 100; % steepness of the tanh in coulomb friction (initially at 100)

alpha = 0.0001;  % Damping coeff proportional to mass of the beams
beta = 0.015; % Damping coeff proportional to stiffness of the beams

rho_num = 0; % 0.01 before
% listM = [1 0 0];
% listKt = [1 0 0];
% listCt = [1 0 0];
% listPhiq = [1 0 0];
listM = [1 1 1];
listKt = [1 1 1];
listCt = [1 1 1];
listPhiq = [1 1 1];
%%
link1.E = 210e9; % link modulus in N/m�
link1.l = 0.9595; % link length in m
link1.l1 = 0.03; % length in m of a short beam at start (to measure strain)
link1.rho = 7800; % mass density in kg/m�
link1.massCorr = 1; % Correction factor on the mass 1.7*
link1.inertiaCorr = 1; % Correction factor on the inertia 2.5*
link1.A = 3.01e-4; % cross section area of the link in m� previously 3.01e-4
link1.m = link1.l * link1.A * link1.rho; % mass of link in kg
link1.J = diag([5.1800e-4 0.1625 0.1625]); % inertia in kg*m�
link1.nu = 0.27; % poisson coef
link1.G = link1.E/(2*(1+link1.nu)); % compression modulus
link1.Ixx = 5.9e-8; % area moment inertia in case of beam in m^4 previously 3.85e-8 
link1.Iyy = 1.75e-8; % area moment inertia in case of beam in m^4 previously 1.75e-8
link1.Izz = 2.1e-8; % area moment inertia in case of beam in m^4 previously 2.1e-8
link1.KCS = diag([link1.E*link1.A link1.G*link1.A link1.G*link1.A link1.G*link1.Ixx link1.E*link1.Iyy link1.E*link1.Izz]);
link1.MCS = diag(link1.rho*[link1.A link1.A link1.A link1.Ixx link1.Iyy link1.Izz]);
link1.nodes = [0 0 0;...
               link1.l1*cos(angleLink1)*cos(angleLink0) link1.l1*cos(angleLink1)*sin(angleLink0) link1.l1*sin(-angleLink1);...
               link1.l*cos(angleLink1)*cos(angleLink0) link1.l*cos(angleLink1)*sin(angleLink0) link1.l*sin(-angleLink1)]; % nodes of the body, starting with bottom to top (center of mass in the middle)
link1.nElem = 2; % Number of elements composing the link
% adding necessary nodes
link1.nodes = CreateInterCoord(link1.nodes,link1.nElem,2,3);
%%
link2.E = 210e9; % link modulus in N/m�
link2.l = 0.9340; % link length in m (from motor to tip)
link2.l1 = 0.03; % length in m of a short beam at start (to measure strain)
link2.lc = 0.8740; % link length in m (from motor to center of mass at the tip)
link2.rho = 7800; % mass density in kg/m�
link2.A = 2.09e-4; % cross section area of the link in m� previously 2.09e-4
link2.m = link2.lc * link2.A * link2.rho; % mass of link in kg
link2.J = diag([2.18e-4 0.08374 0.08374]); % inertia in kg*m�
link2.nu = 0.27; % poisson coef
link2.G = link2.E/(2*(1+link2.nu)); % compression modulus
link2.Ixx = 2.85e-8; % area moment inertia in case of beam in m^4 previously 1.76e-8 
link2.Iyy = 0.75e-8; % area moment inertia in case of beam in m^4 previously 0.75e-8 
link2.Izz = 1.01e-8; % area moment inertia in case of beam in m^4 previously 1.01e-8 
link2.KCS = diag([link2.E*link2.A link2.G*link2.A link2.G*link2.A link2.G*link2.Ixx link2.E*link2.Iyy link2.E*link2.Izz]);
link2.MCS = diag(link2.rho*[link2.A link2.A link2.A link2.Ixx link2.Iyy link2.Izz]);
link2.nodes = [link1.nodes(end,1) link1.nodes(end,2) link1.nodes(end,3);...
               (link2.l1*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*cos(angleLink0) (link2.l1*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*sin(angleLink0) link1.nodes(end,3)-link2.l1*sin(angleLink2+angleLink1);...
               (link2.lc*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*cos(angleLink0) (link2.lc*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*sin(angleLink0) link1.nodes(end,3)-link2.lc*sin(angleLink2+angleLink1);...
               (link2.l*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*cos(angleLink0) (link2.l*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*sin(angleLink0) link1.nodes(end,3)-link2.l*sin(angleLink2+angleLink1)]; % nodes of the body, starting with bottom to top (center of mass in the middle)
link2.nElem = 2; % Number of elements composing the link
% adding necessary nodes
link2.nodes = CreateInterCoord(link2.nodes,link2.nElem,2,3);
%%
joint1.axis = [0 0 0 0 0 1]';
joint1.stiff = stif1;
joint1.coul = 1*coul1; % 1.4*
joint1.damp = damp1;

%%
joint2.axis = [0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]'/norm([0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]);
joint2.stiff = stif2;
joint2.coul = coul2;
joint2.damp = 1*damp2; % 0.5*

%%
joint3.axis = [0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]'/norm([0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]);
joint3.stiff = stif3;
joint3.coul = coul3;
joint3.damp = 1*damp3; % 1.3*

%% Link1 rotation
X1 = (link1.nodes(end,:)-link1.nodes(1,:))/norm(link1.nodes(end,:)-link1.nodes(1,:));
Y1 = joint2.axis(4:6)';
Z1 = cross(X1,Y1);
A1 = [X1; Y1; Z1];

% Motor - Brak - gear inertia
J_motor1 = (0.33+0.011+1.69)*10^-4;
J_motor2 = (0.33+0.011+1.69)*10^-4;
J_motor3 = (0.33+0.011+0.193)*10^-4;

% Base inertia
% J_base = 1*diag([0 0 0.3+(J_motor1)*gear1^2]); % 0.05
J_base = 1*diag([0 0 0.3]);

% Shoulder inertia
% J_shoulder = diag([0 (J_motor2)*gear2^2 0]);
J_shoulder = diag([0 0 0]);

% Elbow mass
m_elbow = 1*(3.3+3); % previously 3.3+3     0.85*
% J_elbow = 1*diag([0.0299 0.0177+(J_motor1)*gear3^2 0.0299]); % 1.5*   then 1.3
J_elbow = 1*diag([0.0299 0.0177 0.0299]); % 1.5*   then 1.3

% End effector mass
m_end = 1*2.1; % previously 2.1    then 1.4*     then 1.2
J_end = diag([2.066e-3 4.378e-3 3.3e-3]);
Xend = (link2.nodes(end,:)-link2.nodes(1,:))/norm(link2.nodes(end,:)-link2.nodes(1,:));
Yend = joint3.axis(4:6)';
Zend = cross(Xend,Yend);
Aend = [Xend; Yend; Zend];

%%
% put all nodes' coord into one matrix only     
nodes = [link1.nodes(1,:);...
         link1.nodes(1,:);...
         link1.nodes;...
         link2.nodes]; % List of nodes to build the model

% number all nodes properly so they can be used by FEMmodel
nodes = [1:size(nodes,1);nodes']';
     
% creating first flexible link with link.nElem number of elements (rotating aroud z)
count = 1;
Start1 = 4; % first node of the 1st link
% creating first rigid body (rotating aroud z)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start1+link1.nElem/2 Start1-1 Start1+[0:link1.nElem/2-1] Start1+link1.nElem/2+1+[0:link1.nElem/2-1]];
elements{count}.mass = link1.massCorr*link1.m;
% elements{count}.J = link1.J+diag([link1.m*0.475^2 0 0]);
elements{count}.J = link1.inertiaCorr*A1*link1.J*A1';
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating first flexible link with link.nElem number of elements (rotating aroud y)
count = size(elements,2)+1;
Start2 = Start1+link1.nElem + 1; % first node of the 2nd link (+2 to take into account small beam before)
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2+1+link2.nElem/2 Start2 Start2+[1:link2.nElem/2] Start2+link2.nElem/2+1+[1:link2.nElem/2] nodes(end,1)];
elements{count}.mass = link2.m;
% elements{count}.J = link2.J+diag([link2.m*0.505^2 0 0]);
elements{count}.J = Aend*link2.J*Aend';
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body joint(rotating aroud Z) Base
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [2];
% elements{count}.mass = 0.3;
elements{count}.J = J_base;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body joint(rotating aroud y) inertia of motor 2 (shoulder)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [3];
% elements{count}.mass = 0.3;
elements{count}.J = A1*J_shoulder*A1'; % Inertia in right frame
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body joint(rotating aroud y) Elbow
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2];
elements{count}.mass = m_elbow;
% elements{count}.J = A1*diag([0.0299 0.0177+((0.33+0.011+0.193)*10^-4)*gear3^2 0.0299])*A1';
elements{count}.J = Aend*J_elbow*Aend';
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body end-effector
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = m_end;
elements{count}.J = Aend*J_end*Aend';
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% Creating the kinematic constraints
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.k = joint1.stiff;
elements{count}.d = joint1.damp;
elements{count}.coulomb = joint1.coul;
elements{count}.coulombParam = coulombParam;
elements{count}.A = joint1.axis;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.k = joint2.stiff;
elements{count}.d = joint2.damp;
elements{count}.coulomb = joint2.coul;
elements{count}.coulombParam = coulombParam;
elements{count}.A = joint2.axis;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.k = joint3.stiff;
elements{count}.d = joint3.damp;
elements{count}.coulomb = joint3.coul;
elements{count}.coulombParam = coulombParam;
elements{count}.A = joint3.axis;

% Trajectory

r = 0.30;
y_end = 0;
z_end = 1.7;
% x_end = nodes(end,2);
x_end = 0.2;

timeVector = 0:timestepsize:finaltime;
trajy = lineTraj(nodes(end,3),y_end,timeVector,t_i,t_f);
trajz = lineTraj(nodes(end,4),z_end,timeVector,t_i,t_f);
trajx = lineTraj(nodes(end,2),x_end,timeVector,t_i,t_f);
% % --------- 1.4s trajectory ----------
% % load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\trajxSimulink14')
% % load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\trajySimulink14')
% % load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\trajzSimulink14')
% % load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\timeSimulink14')
% % --------- 1.05s trajectory ----------
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\trajxSimulink105')
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\trajySimulink105')
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\trajzSimulink105')
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\timeSimulink105')
% trajx = zeros(size(timeVector));
% trajy = zeros(size(timeVector));
% trajz = zeros(size(timeVector));
% indexI = find(timeVector==t_i,1);
% indexF = find(timeVector==t_f,1);
% lengthTime = length(timeVector);
% trajx(1:indexI) = trajxSimulink(1)*ones(1,indexI);
% trajz(1:indexI) = trajySimulink(1)*ones(1,indexI);
% trajy(1:indexI) = -trajzSimulink(1)*ones(1,indexI);
% trajx(indexF:lengthTime) = trajxSimulink(end)*ones(1,lengthTime-indexF+1);
% trajz(indexF:lengthTime) = trajySimulink(end)*ones(1,lengthTime-indexF+1);
% trajy(indexF:lengthTime) = -trajzSimulink(end)*ones(1,lengthTime-indexF+1);
% trajx(indexI:indexF) = interp1(timeSimulink,trajxSimulink,0:timestepsize:t_f-t_i,'linear');
% trajz(indexI:indexF) = interp1(timeSimulink,trajySimulink,0:timestepsize:t_f-t_i,'linear');
% trajy(indexI:indexF) = -interp1(timeSimulink,trajzSimulink,0:timestepsize:t_f-t_i,'linear');
% % plot(timeVector,[trajx;trajy;trajz])

count = size(elements,2)+1;
elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajy;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elements{count}.elements = [count-3 count-2 count-1];
elements{count}.active = 1;

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
% D = StaticIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();
save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\D13','D')

%% Flexible Model
clear elements

count = 0;
Start1 = 3; % first node of the 1st link
for i = 1:link1.nElem+1
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start1-1+i Start1+i];
    elements{count+i}.KCS = link1.KCS;
    elements{count+i}.MCS = diag([link1.massCorr*ones(1,3) link1.inertiaCorr*ones(1,3)])*link1.MCS;
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.listPhiq = listPhiq;
    elements{count+i}.g = grav;
end

% creating first flexible link with link.nElem number of elements (rotating aroud z)
count = size(elements,2);
Start2 = Start1+link1.nElem+2; % first node of the 2nd link
for i = 1:link2.nElem + 2 % to take into account first short beam and last beam
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start2-1+i Start2+i];
    elements{count+i}.KCS = link2.KCS;
    elements{count+i}.MCS = link2.MCS;
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;    
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.listPhiq = listPhiq;
    elements{count+i}.g = grav;
end

% creating rigid body joint(rotating aroud Z) Base
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [2];
% elements{count}.mass = 0.3;
elements{count}.J = J_base;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body joint(rotating aroud y) inertia of motor 2 (shoulder)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [3];
% elements{count}.mass = 0.3;
elements{count}.J = A1*J_shoulder*A1';
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body joint(rotating aroud y) Elbow
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2];
elements{count}.mass = m_elbow;
elements{count}.J = A1*J_elbow*A1';
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body end-effector
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [nodes(end,1)];
elements{count}.mass = m_end;
elements{count}.J = Aend*J_end*Aend';
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% Creating the kinematic constraints
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.k = joint1.stiff;
elements{count}.d = joint1.damp;
elements{count}.coulomb = joint1.coul;
elements{count}.coulombParam = coulombParam;
elements{count}.A = joint1.axis;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.k = joint2.stiff;
elements{count}.d = joint2.damp;
elements{count}.coulomb = joint2.coul;
elements{count}.coulombParam = coulombParam;
elements{count}.A = joint2.axis;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.k = joint3.stiff;
elements{count}.d = joint3.damp;
elements{count}.coulomb = joint3.coul;
elements{count}.coulombParam = coulombParam;
elements{count}.A = joint3.axis;

% Trajector

timeVector = 0:timestepsize:finaltime;

count = size(elements,2)+1;
elements{count}.type = 'TrajectoryConstraint';
elements{count}.nodes = [nodes(end,1)];
elements{count}.T = [trajx;...
                     trajy;...
                     trajz];
elements{count}.Axe = [1 0 0;...
                       0 1 0;...
                       0 0 1];
elements{count}.elements = [count-3 count-2 count-1];
elements{count}.active = 1;

% Solving
ModelF = FEModel();
ModelF.CreateFEModel(nodes,elements);
ModelF.defineBC(BC);

% Copying ElementVariables and Node variables from static solution
                           
for n = ModelF.listNumberNodes
    ModelF.listNodes{n}.R = ModelR.listNodes{n}.R;
    ModelF.listNodes{n}.position = ModelR.listNodes{n}.position;
    ModelF.listNodes{n}.velocity = ModelR.listNodes{n}.velocity;
    ModelF.listNodes{n}.acceleration = ModelR.listNodes{n}.acceleration;
%     dh = zeros(6,npts);
%     for i = 1:npts
%         dh(:,i) = logSE3([dimR(ModelF.listNodes{n}.R(:,i)) ModelF.listNodes{n}.position(:,i); 0 0 0 1]);
%     end
%     ModelF.listNodes{n}.velocity = deriveVal(dh,timestepsize);
%     ModelF.listNodes{n}.acceleration = deriveVal(dh,timestepsize);
    ModelF.listNodes{n}.InitializeD_Opti();
end
skip = 0;
for n = ModelF.listNumberElementVariables
    if strcmp(ModelF.listElementVariables{n}.DofType,'MotionDof')
        ModelF.listElementVariables{n}.R = ModelR.listElementVariables{n}.R;
        ModelF.listElementVariables{n}.x = ModelR.listElementVariables{n}.x;
        ModelF.listElementVariables{n}.relCoo = ModelR.listElementVariables{n}.relCoo;
        ModelF.listElementVariables{n}.velocity = ModelR.listElementVariables{n}.velocity;
        ModelF.listElementVariables{n}.acceleration = ModelR.listElementVariables{n}.acceleration;
    else
        while ~strcmp(int2str(ModelF.listElementVariables{n}.linkElement.listNumberNodes),int2str(ModelR.listElementVariables{n+skip}.linkElement.listNumberNodes))
            skip = skip+1;
        end
        ModelF.listElementVariables{n}.value = ModelR.listElementVariables{n+skip}.value; 
    end
    ModelF.listElementVariables{n}.InitializeD_Opti();
end

% Start second full optimization with newly computed initial guess
tic
S = DirectTranscriptionOpti(ModelF);
S.parameters.rho = rho_num;
S.npts = npts;
S.parameters.finaltime = finaltime;
S.parameters.timestepsize = finaltime/(S.npts-1);
S.parameters.scaling = 1e6;
S.ConstIter = hypForComp;
% analys = S.runPoleAnalysisOpti(D)
xSol = S.runOpti(D);
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])
save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\S13','S')
