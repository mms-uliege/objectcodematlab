%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test file to have a simple beam subjected to a force and see if we can
% compute back the strains in the beam (and so the torque inside the beam)
% base on the computed node positions. As the strains are constant over an
% element, if we want to have the torque inside the beam at the initial
% node, it is required to increase the number of elements to have a better
% approximation.








% Test example for a uniform 3D beam loaded dynamically
clear all
close all

%% Definition of time parameters
finaltime = 2;
timestepsize = 0.01;

%% Definition of the nodes of the model (as matrix)
% nodes = [1 0 0 0;
%          2 1 0 0];
     
nodes = [1 0 0 0;
         2 0 0 0
         3 0.03 0 0
         4 1 0 0];
     
% grav = [0 0 0];
grav = [0 0 -9.81];

%% Definition of the elements of the model (as structure)
% Number of beam elements to build the model
nElem = 4;
% Start = 1;
% End = 2;
Start = 3;
End = 4;
% create new nodes to have more beam elements
nodes = createInterNodes(nodes,nElem,Start,End);

% Beam parameters
a = 0.01;
b = 0.01;
rho = 2700;

E = 70e9; nu = 0.3; G = E/(2*(1+nu));
A = a*b; Ixx = a*b*(a^2+b^2)/12; Iyy = b*a^3/12;Izz = a*b^3/12;

KCS = diag([E*A G*A G*A G*Ixx E*Iyy E*Izz]);
MCS = diag(rho*[A A A Ixx Iyy Izz]);

count = 0;
type = 'FlexibleBeamElement';
for i = 1:nElem +1 % for small element at beginning
    elements{count+i}.type = type;
    elements{count+i}.nodes = [nodes(Start+i-2,1) nodes(Start+i-1,1)];
    elements{count+i}.yAxis = [0 1 0];
    elements{count+i}.KCS = KCS;
    elements{count+i}.MCS = MCS;
    elements{count+i}.alpha = 0.0001; % damping proportional to Mass 4.5896e-4
    elements{count+i}.beta = 0.01; % damping proportional to Stiffness 0.1329
    elements{count+i}.g = grav;
end

% Creating the kinematic constraints
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.A = [0 0 0 0 0 1]';

time = 0:timestepsize:finaltime;
A = 0.5;
f = A*ones(size(time));
freq = 4;
% ftip = sin(2*pi*freq*time);
ftip = f;
count = size(elements,2)+1; % 1st control element
elements{count}.type = 'ApplyForcePD_wFdbk';
elements{count}.nodes = [Start-1 Start];
elements{count}.select = [0 0 0 0 0 1];
elements{count}.KCS = KCS;
elements{count}.yAxis = [0 1 0];
elements{count}.kp = 0;
elements{count}.kd = 0;
elements{count}.kMeas = 0.5;
elements{count}.f = -0.0011*E*Iyy*ones(size(time));
elements{count}.ref = 0*f;
elements{count}.dref = 0*f;
elements{count}.elements = [count-1];

count = size(elements,2)+1;
elements{count}.type = 'punctualForce';
elements{count}.nodes = [nodes(end,1)];
% elements{count}.f = f;
% elements{count}.f = f*0.1;
elements{count}.f = ftip;
elements{count}.DOF = [6];

% count = size(elements,2)+1;
% elements{count}.type = 'ExternalForce';
% elements{count}.nodes = nodes(end,1);
% elements{count}.amplitude = 30;
% elements{count}.frequency = 0;
% elements{count}.DOF = [5]; % loaded in the y and z directions



%% Definition of the boundary conditions

BC = [1]; % Node 1 is fixed

%% Definition of the finite element model (FEModel Object)
% Based on previously defined nodes and elements
Model = FEModel();
Model.CreateFEModel(nodes,elements);
Model.defineBC(BC);

%% Definition of the solver and its parameters
tic
D = DynamicIntegration(Model);  % Creatind the Dynamic integration object
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = 0.2;
D.parameters.scaling = 1e6;
D.runIntegration(); % Run the integration of the object
elapsed = toc;
disp(['Computation lasted ',num2str(elapsed/60),' min.'])
%% Visualization and plots
% To visualize the model:(uncomment the following line)
% Model.Visu

% Plots
endNodePos = Model.listNodes{end}.position;
figure
plot(D.parameters.time,endNodePos(1,:),D.parameters.time,endNodePos(2,:),D.parameters.time,endNodePos(3,:),'Linewidth',2)
grid on
title('Tip of the beam position')
xlabel('Time [s]')
ylabel('Poistion [m]')
legend('X','Y','Z')

figure
plot(D.parameters.time,D.model.listElementVariables{end}.value(6,:),'Linewidth',2)
grid on
title('Lagrange multiplier')
xlabel('Time [s]')
ylabel('Lagrange [m]')

time = D.parameters.time;
ntimeSteps = length(time);
listBeam = [1];
Effort = cell(1,length(listBeam));
select = [0 0 0 0 0 1];
for nStep = 1:ntimeSteps
    count = 1;
    for iBeam = listBeam;
        BeamElem = D.model.listElements{iBeam};
        xA = BeamElem.listNodes{1}.position(:,nStep);
        RA = dimR(BeamElem.listNodes{1}.R(:,nStep));
        HA = [RA xA; 0 0 0 1];
        HAm1 = [RA' -RA'*xA; 0 0 0 1];
        
        xB = BeamElem.listNodes{2}.position(:,nStep);
        RB = dimR(BeamElem.listNodes{2}.R(:,nStep));
        HB = [RB xB; 0 0 0 1];
        HBm1 = [RB' -RB'*xB; 0 0 0 1];

        hBeam = vectSE3(HAm1*HB);
        epsilon = (hBeam-BeamElem.h0)/BeamElem.L;

        T = computeTSE3inv(hBeam); 
        P = [hatSE3(hBeam)-T T];
%         force = [select 0 0 0 0 0 0]*P'*BeamElem.KCS*epsilon;
        force = -select*BeamElem.KCS*epsilon;
        Effort{count} = [Effort{count} force];
        count = count+1;
    end
end

figure
plot(time,Effort{1},time,ftip)
title('Effort measured in Beam')
xlabel('Time')
ylabel('Effort')
legend('in beam','ftip')
grid on

figure
plot(D.parameters.time,D.model.listElementVariables{1}.relCoo,'Linewidth',2)
grid on
title('Joint angle')
xlabel('Time [s]')
ylabel('angle [rad]')

figure
plot(D.parameters.time,D.model.listElementVariables{1}.velocity,'Linewidth',2)
grid on
title('Joint velocity')
xlabel('Time [s]')
ylabel('velocity [rad/s]')

figure
plot(D.parameters.time,D.model.listElementVariables{1}.acceleration,'Linewidth',2)
grid on
title('Joint acceleration')
xlabel('Time [s]')
ylabel('acceleration [rad/s^2]')