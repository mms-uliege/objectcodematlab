%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tests to see the frequency response of flexible model, try to identify
% the parameters of the model compared to the real one.

% parameters comming from https://robfile.mechatronik.uni-linz.ac.at/doku.php?id=start:robots:ella:mechsetup
close all
clear elements

finaltime = 30; % 2 seconds
timestepsize = 0.01;
initialtime = 0;

time = initialtime:timestepsize:finaltime;

% grav = [0 0 -9.81]; % gravity in m/s�
grav = [0 0 -9.81]; % gravity in m/s�

% endPoseInit = [1.4 0.75 -0.5]; % changed in the coordinate we have in our model
angleLink0 = -0.4918; % rad, angle at the base (0.4918)
angleLink1 = -0.6929; % angle of the first joint in rad (0.1840)
angleLink2 = 0.2656; % angle of the second joint in rad (-0.9926)

gear1 = 100;
gear2 = 100;
gear3 = 160;

% Joint damping, friction and stiffness
damp1 = 1.95*1e-3*gear1^2;
damp2 = 2.2*1e-3*gear2^2;
damp3 = 4.76*1e-4*gear3^2;
% stif1 = 1.2*1e5;
% stif2 = 1.2*1e5;
% stif3 = 2.9*1e4;
coul1 = 0.2*gear1;  
coul2 = 0.14*gear2;
coul3 = 0.07*gear3;
% damp1 = 0;
% damp2 = 0;
% damp3 = 0;
% coul1 = 0;
% coul2 = 0;
% coul3 = 0;
stif1 = 0;
stif2 = 0;
stif3 = 0;

% kp1 = 100;
% kd1 = 5;
% kp2 = 100;
% kd2 = 5;
% kp3 = 100;
% kd3 = 5;
kp1 = 0;
kd1 = 0;
kp2 = 0;
kd2 = 0;
kp3 = 0;
kd3 = 0;

alpha = 0.0001;  % Damping coeff proportional to mass of the beams
beta = 0.005; % Damping coeff proportional to stiffness of the beams

rho_num = 0;
% listM = [1 0 0];
% listKt = [1 0 0];
% listCt = [1 0 0];
% listPhiq = [1 0 0];
listM = [1 1 1];
listKt = [1 1 1];
listCt = [1 1 1];
listPhiq = [1 1 1];
%%
link1.E = 210e9; % link modulus in N/m�
link1.l = 0.9595; % link length in m
link1.rho = 7800; % mass density in kg/m�
link1.massCorr = 1; % Correction factor on the mass 1.7*
link1.inertiaCorr = 1; % Correction factor on the inertia 2.5*
link1.A = 3.01e-4; % cross section area of the link in m�
link1.m = link1.l * link1.A * link1.rho; % mass of link in kg
link1.J = diag([5.1800e-4 0.1625 0.1625]); % inertia in kg*m�
link1.nu = 0.27; % poisson coef
link1.G = link1.E/(2*(1+link1.nu)); % compression modulus
link1.Ixx = 5.9e-8; % area moment inertia in case of beam in m^4 previously 3.85e-8 
link1.Iyy = 1.75e-8; % area moment inertia in case of beam in m^4 previously 1.75e-8
link1.Izz = 2.1e-8; % area moment inertia in case of beam in m^4 previously 2.1e-8
link1.KCS = diag([link1.E*link1.A link1.G*link1.A link1.G*link1.A link1.G*link1.Ixx link1.E*link1.Iyy link1.E*link1.Izz]);
link1.MCS = diag(link1.rho*[link1.A link1.A link1.A link1.Ixx link1.Iyy link1.Izz]);
link1.nodes = [0 0 0;...
               link1.l*cos(angleLink1)*cos(angleLink0) link1.l*cos(angleLink1)*sin(angleLink0) link1.l*sin(-angleLink1)]; % nodes of the body, starting with bottom to top (center of mass in the middle)
link1.nElem = 2; % Number of elements composing the link
% adding necessary nodes
link1.nodes = CreateInterCoord(link1.nodes,link1.nElem,1,2);
%%
link2.E = 210e9; % link modulus in N/m�
link2.l = 0.9340; % link length in m
link2.lc = 0.8740; % link length in m (from motor to center of mass at the tip) 0.8740
link2.rho = 7800; % mass density in kg/m�
link2.A = 2.09e-4; % cross section area of the link in m�
% link2.m = link2.l * link2.A * link2.rho; % mass of link in kg
link2.m = link2.lc * link2.A * link2.rho; % mass of link in kg when end mass is not exactly at tip
link2.J = diag([2.18e-4 0.08374 0.08374]); % inertia in kg*m�
link2.nu = 0.27; % poisson coef
link2.G = link2.E/(2*(1+link2.nu)); % compression modulus
link2.Ixx = 2.85e-8; % area moment inertia in case of beam in m^4 previously 1.76e-8 
link2.Iyy = 0.75e-8; % area moment inertia in case of beam in m^4 previously 0.75e-8 
link2.Izz = 1.01e-8; % area moment inertia in case of beam in m^4 previously 1.01e-8 
link2.KCS = diag([link2.E*link2.A link2.G*link2.A link2.G*link2.A link2.G*link2.Ixx link2.E*link2.Iyy link2.E*link2.Izz]);
link2.MCS = diag(link2.rho*[link2.A link2.A link2.A link2.Ixx link2.Iyy link2.Izz]);
link2.nodes = [link1.nodes(end,1) link1.nodes(end,2) link1.nodes(end,3);...
               (link2.lc*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*cos(angleLink0) (link2.lc*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*sin(angleLink0) link1.nodes(end,3)-link2.lc*sin(angleLink2+angleLink1);...
               (link2.l*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*cos(angleLink0) (link2.l*cos(angleLink1+angleLink2)+link1.l*cos(angleLink1))*sin(angleLink0) link1.nodes(end,3)-link2.l*sin(angleLink2+angleLink1)]; % nodes of the body, starting with bottom to top (center of mass in the middle)
link2.nElem = 2; % Number of elements composing the link
% adding necessary nodes
link2.nodes = CreateInterCoord(link2.nodes,link2.nElem,1,2);
%%
joint1.axis = 0*[0 0 0 0 0 1]';
joint1.stiff = stif1;
joint1.coul = coul1;
joint1.damp = damp1;

%%
joint2.axis = 0*[0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]'/norm([0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]);
joint2.stiff = stif2;
joint2.coul = coul2;
joint2.damp = damp2;

%%
joint3.axis = 0*[0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]'/norm([0 0 0 -link1.l*cos(angleLink1)*sin(angleLink0) link1.l*cos(angleLink1)*cos(angleLink0) 0]);
joint3.stiff = stif3;
joint3.coul = coul3;
joint3.damp = 1.4*damp3;

% Link1 rotation
X1 = (link1.nodes(end,:)-link1.nodes(1,:))/norm(link1.nodes(end,:)-link1.nodes(1,:));
Y1 = joint2.axis(4:6)';
Z1 = cross(X1,Y1);
A1 = [X1; Y1; Z1];

% Motor - Brak - gear inertia
J_motor1 = (0.33+0.011+1.69)*10^-4;
J_motor2 = (0.33+0.011+1.69)*10^-4;
J_motor3 = (0.33+0.011+0.193)*10^-4;

% Base inertia
% J_base = 1*diag([0 0 0.3+(J_motor1)*gear1^2]); % 0.05
% J_base = 1*diag([0 0 0.3+(J_motor1)]); % 0.05
J_baseM = diag([0 0 (J_motor1)*gear1]);
J_base = 1*diag([0 0 0.3]);

% Shoulder inertia
% J_shoulder = diag([0 (J_motor2)*gear2^2 0]);
% J_shoulder = diag([0 (J_motor2)*gear2 0]);
J_shoulderM = diag([0 (J_motor2)*gear2 0]);
J_shoulder = diag([0 0 0]);

% Elbow mass
m_elbow = 1*(3.3+3); % previously 3.3+3     0.85*
% J_elbow = 1*diag([0.0299 0.0177+(J_motor3)*gear3^2 0.0299]); % 1.5*   then 1.3
% J_elbow = 1*diag([0.0299 0.0177+(J_motor3)*gear3 0.0299]); % 1.5*   then 1.3
J_elbowM = diag([0 (J_motor3)*gear3 0]);
J_elbow = 1*diag([0.0299 0.0177 0.0299]); % 1.5*   then 1.3

% End effector mass
m_end = 1*2.1; % previously 2.1    then 1.4*     then 1.2
J_end = diag([2.066e-3 4.378e-3 3.3e-3]);
Xend = (link2.nodes(end,:)-link2.nodes(1,:))/norm(link2.nodes(end,:)-link2.nodes(1,:));
Yend = joint3.axis(4:6)';
Zend = cross(Xend,Yend);
Aend = [Xend; Yend; Zend];

%%
% put all nodes' coord into one matrix only     
nodes = [link1.nodes(1,:);...
         link1.nodes(1,:);...
         link1.nodes;...
         link2.nodes]; % List of nodes to build the model

% number all nodes properly so they can be used by FEMmodel
nodes = [1:size(nodes,1);nodes']';

%% Flexible Model

count = 0;
Start1 = 3; % first node of the 1st link
for i = 1:link1.nElem
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start1-1+i Start1+i];
    elements{count+i}.KCS = link1.KCS;
    elements{count+i}.MCS = diag([link1.massCorr*ones(1,3) link1.inertiaCorr*ones(1,3)])*link1.MCS;
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.listPhiq = listPhiq;
    elements{count+i}.g = grav;
end
% count = 1;
% elements{count}.type = 'RigidBodyElement';
% elements{count}.nodes = [Start1+link1.nElem/2 Start1+[0:link1.nElem/2-1] Start1+link1.nElem/2+1+[0:link1.nElem/2-1]];
% elements{count}.mass = link1.m;
% % elements{count}.J = link1.J+diag([link1.m*0.475^2 0 0]);
% elements{count}.J = A1*link1.J*A1';
% elements{count}.g = grav;
% elements{count}.listM = listM;
% elements{count}.listCt = listCt;
% elements{count}.listKt = listKt;
% elements{count}.listPhiq = listPhiq;

% creating first flexible link with link.nElem number of elements (rotating aroud z)
count = size(elements,2);
Start2 = Start1+link1.nElem+1; % first node of the 2nd link
for i = 1:link2.nElem + 1 % +1 is to take into account that the end mass is not actually at the end...
    elements{count+i}.type = 'FlexibleBeamElement';
    elements{count+i}.nodes = [Start2-1+i Start2+i];
    elements{count+i}.KCS = link2.KCS;
    elements{count+i}.MCS = link2.MCS;
    elements{count+i}.yAxis = [0 0 1];
    elements{count+i}.alpha = alpha;
    elements{count+i}.beta = beta;    
    elements{count+i}.listM = listM;
    elements{count+i}.listCt = listCt;
    elements{count+i}.listKt = listKt;
    elements{count+i}.listPhiq = listPhiq;
    elements{count+i}.g = grav;
end

% creating rigid body joint(rotating aroud Z) Base
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [2];
% elements{count}.mass = 0.3;
elements{count}.J = J_base;
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body joint(rotating aroud y) inertia of motor 2 (shoulder)
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [3];
% elements{count}.mass = 0.3;
elements{count}.J = A1*J_shoulder*A1';
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body joint(rotating aroud y) Elbow
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2];
elements{count}.mass = m_elbow;
elements{count}.J = A1*J_elbow*A1';
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% creating rigid body end-effector
count = size(elements,2)+1;
elements{count}.type = 'RigidBodyElement';
elements{count}.nodes = [Start2+link2.nElem/2+1];
elements{count}.mass = m_end; % To correct frequency to go a bit higher
elements{count}.J = Aend*J_end*Aend';
elements{count}.g = grav;
elements{count}.listM = listM;
elements{count}.listCt = listCt;
elements{count}.listKt = listKt;
elements{count}.listPhiq = listPhiq;

% Creating the kinematic constraints
count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [1 2];
elements{count}.k = joint1.stiff;
elements{count}.d = joint1.damp;
elements{count}.coulomb = joint1.coul;
elements{count}.A = joint1.axis;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [2 3];
elements{count}.k = joint2.stiff;
elements{count}.d = joint2.damp;
elements{count}.coulomb = joint2.coul;
elements{count}.A = joint2.axis;

count = size(elements,2)+1;
elements{count}.type = 'KinematicConstraint';
elements{count}.nodes = [Start2-1 Start2];
elements{count}.k = joint3.stiff;
elements{count}.d = joint3.damp;
elements{count}.coulomb = joint3.coul;
elements{count}.A = joint3.axis;

% count = size(elements,2)+1;
% elements{count}.type = 'ForceInKinematicConstraint'; % try with PD control
% elements{count}.elements = [count-3];
% elements{count}.ref = zeros(size(time));
% elements{count}.dref = zeros(size(time));
% elements{count}.kp = kp1;
% elements{count}.kd = kd1;
% elements{count}.f = zeros(size(time));
% 
% count = size(elements,2)+1;
% elements{count}.type = 'ForceInKinematicConstraint'; % try with PD control
% elements{count}.elements = [count-3];
% elements{count}.ref = zeros(size(time));
% elements{count}.dref = zeros(size(time));
% elements{count}.kp = kp2;
% elements{count}.kd = kd2;
% elements{count}.f = zeros(size(time));
% 
% count = size(elements,2)+1;
% elements{count}.type = 'ForceInKinematicConstraint'; % try with PD control
% elements{count}.elements = [count-3];
% elements{count}.ref = zeros(size(time));
% elements{count}.dref = zeros(size(time));
% elements{count}.kp = kp3;
% elements{count}.kd = kd3;
% elements{count}.f = zeros(size(time));

s = 1;
e = 10;
A = 5;
f = zeros(size(time));
f(s:e) = A*ones(1,e-s+1);
count = size(elements,2)+1;
elements{count}.type = 'punctualForce';
elements{count}.nodes = [nodes(end,1)];
elements{count}.f = f;
elements{count}.DOF = [2];

% Boundary Condition
BC = [1];

% Solving
ModelF = FEModel();
ModelF.CreateFEModel(nodes,elements);
ModelF.defineBC(BC);

% Start dynamic integration
tic
D = DynamicIntegration(ModelF);
% D = StaticIntegration(ModelF);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = timestepsize;
D.parameters.rho = rho_num;
% D.parameters.relTolRes = 1e-10;
D.parameters.scaling = 1e6;
D.runIntegration();
calcTime = toc;
disp(['Computation lasted ', num2str(calcTime/60),' min.'])
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\S11','S')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\S111sCoulCut','S')

%% plots
endNode = ModelF.listNodes{end};

% figure
% plot(time,[endNode.position(1,:);endNode.position(2,:);endNode.position(3,:)])
% xlabel('Time [s]')
% ylabel('Tip position [m]')
% legend('X','Y','Z','f','Location','Best')
% grid on

% figure
% plot(time,[endNode.velocity(1,:);endNode.velocity(2,:);endNode.velocity(3,:)])
% xlabel('Time [s]')
% ylabel('Tip velocity [m]')
% legend('X','Y','Z','f','Location','Best')
% grid on

figure
% plot(time,[endNode.acceleration(1,:);endNode.acceleration(2,:);endNode.acceleration(3,:);f])
plot(time,[endNode.acceleration(1,:);endNode.acceleration(2,:);endNode.acceleration(3,:)])
xlabel('Time [s]')
ylabel('Tip acceleration [m]')
% legend('X','Y','Z','f','Location','Best')
legend('X','Y','Z','Location','Best')
grid on

