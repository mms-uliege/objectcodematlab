%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Comparison of Rigid and Flexible assumptions on Simulink model for 1.05s
% trajectories
%
% -------     Flexible   -------
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingTorqueFlex105','resultingTorque')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingMotorAnglesFlex105','resultingMotorAngles')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAnglesFlex105','resultingLinkAngles')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAcc1Flex105','resultingLinkAcc1')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAcc2Flex105','resultingLinkAcc2')
% -------      Rigid     -------
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingTorqueRigid105','resultingTorque')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingMotorAnglesRigid105','resultingMotorAngles')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAnglesRigid105','resultingLinkAngles')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAcc1Rigid105','resultingLinkAcc1')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAcc2Rigid105','resultingLinkAcc2')
% -------   Flexible FF  -------
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingTorqueFlexFF105','resultingTorque')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingMotorAnglesFlexFF105','resultingMotorAngles')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAnglesFlexFF105','resultingLinkAngles')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAcc1FlexFF105','resultingLinkAcc1')
% save('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAcc2FlexFF105','resultingLinkAcc2')

%% Loading resulting behavior from simulink or actual

% -------     Flexible   -------

load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingTorqueFlex105')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingMotorAnglesFlex105')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAnglesFlex105')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAcc1Flex105')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAcc2Flex105')

torqueFlex = resultingTorque;
motorAnglesFlex = resultingMotorAngles;
linkAnglesFlex = resultingLinkAngles;
linkAcc1Flex = resultingLinkAcc1;
linkAcc2Flex = resultingLinkAcc2;

% -------      Rigid     -------

load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingTorqueRigid105')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingMotorAnglesRigid105')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAnglesRigid105')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAcc1Rigid105')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAcc2Rigid105')

torqueRigid = resultingTorque;
motorAnglesRigid = resultingMotorAngles;
linkAnglesRigid = resultingLinkAngles;
linkAcc1Rigid = resultingLinkAcc1;
linkAcc2Rigid = resultingLinkAcc2;

% -------   Flexible FF  -------

load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingTorqueFlexFF105')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingMotorAnglesFlexFF105')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAnglesFlexFF105')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAcc1FlexFF105')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\resultingLinkAcc2FlexFF105')

torqueFlexFF = resultingTorque;
motorAnglesFlexFF = resultingMotorAngles;
linkAnglesFlexFF = resultingLinkAngles;
linkAcc1FlexFF = resultingLinkAcc1;
linkAcc2FlexFF = resultingLinkAcc2;

% -------  Reference qA  -------

load('C:\ObjectCodeMatlab\TestsArthur\ELLA_ComputedInputs\Measurements\measurement_15_05_18\Traj105s_reference_qA.mat')

virtualExtraTimeRef = 10.2;
tRef = TARGET_DATA____measuredData_MMot_0_(1,:)+virtualExtraTimeRef;
torqueRef(1,:) = TARGET_DATA____measuredData_MMot_0_(2,:);
torqueRef(2,:) = TARGET_DATA____measuredData_MMot_1_(2,:);
torqueRef(3,:) = TARGET_DATA____measuredData_MMot_2_(2,:);
linkAcc2Ref(1,:) = TARGET_DATA____measuredData_aIMU2_0_(2,:);
linkAcc2Ref(2,:) = TARGET_DATA____measuredData_aIMU2_1_(2,:);
linkAcc2Ref(3,:) = TARGET_DATA____measuredData_aIMU2_2_(2,:);

% -------  Reference qM  -------

load('C:\ObjectCodeMatlab\TestsArthur\ELLA_ComputedInputs\Measurements\measurement_15_05_18\Traj105s_reference_qM.mat')

virtualExtraTimeRef = 10.2;
tRefM = TARGET_DATA____measuredData_MMot_0_(1,:)+virtualExtraTimeRef;
torqueRefM(1,:) = TARGET_DATA____measuredData_MMot_0_(2,:);
torqueRefM(2,:) = TARGET_DATA____measuredData_MMot_1_(2,:);
torqueRefM(3,:) = TARGET_DATA____measuredData_MMot_2_(2,:);
linkAcc2RefM(1,:) = TARGET_DATA____measuredData_aIMU2_0_(2,:);
linkAcc2RefM(2,:) = TARGET_DATA____measuredData_aIMU2_1_(2,:);
linkAcc2RefM(3,:) = TARGET_DATA____measuredData_aIMU2_2_(2,:);

% -------    FE actual   -------

load('C:\ObjectCodeMatlab\TestsArthur\ELLA_ComputedInputs\Measurements\measurement_28_05_18\105s_2018_05_28_fe.mat')

virtualExtraTimeActual = 10;
tActual = TARGET_DATA____measuredData_MMot_0_(1,:)+virtualExtraTimeActual;
torqueActual(1,:) = TARGET_DATA____measuredData_MMot_0_(2,:);
torqueActual(2,:) = TARGET_DATA____measuredData_MMot_1_(2,:);
torqueActual(3,:) = TARGET_DATA____measuredData_MMot_2_(2,:);
linkAcc2Actual(1,:) = TARGET_DATA____measuredData_aIMU2_0_(2,:);
linkAcc2Actual(2,:) = TARGET_DATA____measuredData_aIMU2_1_(2,:);
linkAcc2Actual(3,:) = TARGET_DATA____measuredData_aIMU2_2_(2,:);

%% Loading FE computed results

% -------     FE Flex    -------
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPose1sCoulCutActual105.mat')
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPose1sCoulCutActual105.mat')
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorque1sCoulCutActual105.mat')
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVel1sCoulCutActual105.mat')
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\Time1sCoulCutActual105.mat')

load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPose1sCoulCutActualMend105.mat')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPose1sCoulCutActualMend105.mat')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorque1sCoulCutActualMend105.mat')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVel1sCoulCutActualMend105.mat')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\Time1sCoulCutActualMend105.mat')
FlexTime = Time+virtualExtraTimeActual;
FlexTorques = JointTorque;

% -------    FE Rigid    -------
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPoseR1sCoulCutActual105.mat')
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPoseR1sCoulCutActual105.mat')
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorqueR1sCoulCutActual105.mat')
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVelR1sCoulCutActual105.mat')
% load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\TimeR1sCoulCutActual105.mat')

load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\EndPoseR1sCoulCutActualMend105.mat')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointPoseR1sCoulCutActualMend105.mat')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointTorqueR1sCoulCutActualMend105.mat')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\JointVelR1sCoulCutActualMend105.mat')
load('C:\ObjectCodeMatlab\TestsArthur\ELLAFlexLinkFrictionJoints_wRigiModel\TimeR1sCoulCutActualMend105.mat')
RigidTime = Time+virtualExtraTimeActual;
RigidTorques = JointTorque;

%% Plotting graphs
% close all
% Torques
% figure
% subplot(3,1,1) % first plot
% hold on
% plot(torqueRigid.Time,torqueRigid.Data(:,1))
% plot(torqueFlex.Time,torqueFlex.Data(:,1))
% plot(torqueFlexFF.Time,torqueFlexFF.Data(:,1))
% plot(tRef,torqueRef(1,:))
% plot(tRefM,torqueRefM(1,:))
% % plot(tActual,torqueActual(1,:))
% title('Torque comparison from simulink')
% ylabel('Torque 1 [Nm]')
% axis([10 15 -inf inf])
% grid on
% subplot(3,1,2) % second plot
% hold on
% plot(torqueRigid.Time,torqueRigid.Data(:,2))
% plot(torqueFlex.Time,torqueFlex.Data(:,2))
% plot(torqueFlexFF.Time,torqueFlexFF.Data(:,2))
% plot(tRef,torqueRef(2,:))
% plot(tRefM,torqueRefM(2,:))
% % plot(tActual,torqueActual(2,:))
% ylabel('Torque 2 [Nm]')
% axis([10 15 -inf inf])
% grid on
% subplot(3,1,3) % third plot
% hold on
% plot(torqueRigid.Time,torqueRigid.Data(:,3))
% plot(torqueFlex.Time,torqueFlex.Data(:,3))
% plot(torqueFlexFF.Time,torqueFlexFF.Data(:,3))
% plot(tRef,torqueRef(3,:))
% plot(tRefM,torqueRefM(3,:))
% % plot(tActual,torqueActual(3,:))
% xlabel('Time [s]')
% ylabel('Torque 3 [Nm]')
% axis([10 15 -inf inf])
% % legend('u_{rigid}','u_{flex}','u_{FF}','u_{ref}','u_{refM}','u_{actual}')
% legend('u_{rigid}','u_{flex}','u_{FF}','u_{ref}','u_{refM}')
% grid on

figure % first plot
hold on
plot(torqueRigid.Time,torqueRigid.Data(:,1),'Linewidth',2)
plot(torqueFlex.Time,torqueFlex.Data(:,1),'Linewidth',2)
plot(torqueFlexFF.Time,torqueFlexFF.Data(:,1),'Linewidth',2)
plot(tRef,torqueRef(1,:),'Linewidth',2)
plot(tRefM,torqueRefM(1,:),'Linewidth',2)
plot(tActual,torqueActual(1,:),'Linewidth',2)
plot(FlexTime,FlexTorques(1,:),'Linewidth',2)
plot(RigidTime,RigidTorques(1,:),'Linewidth',2)
if exist('TrajTorqueMotor')
    plot(TrajTorqueMotor.Time+virtualExtraTimeRef,TrajTorqueMotor.Data(:,1),'Linewidth',2)
end
title('Torque 1 comparison from simulink')
xlabel('Time [s]')
ylabel('Torque 1 [Nm]')
axis([10 12 -2 1])
legend('u_{rigid}','u_{flex}','u_{FF}','u_{ref}','u_{refM}','u_{actual}','u_{FE_F}','u_{FE_R}')
% legend('u_{rigid}','u_{flex}','u_{FF}','u_{ref}','u_{refM}','u_{FE_F}','u_{FE_R}')
grid on

figure % second plot
hold on
plot(torqueRigid.Time,torqueRigid.Data(:,2),'Linewidth',2)
plot(torqueFlex.Time,torqueFlex.Data(:,2),'Linewidth',2)
plot(torqueFlexFF.Time,torqueFlexFF.Data(:,2),'Linewidth',2)
plot(tRef,torqueRef(2,:),'Linewidth',2)
plot(tRefM,torqueRefM(2,:),'Linewidth',2)
plot(tActual,torqueActual(2,:),'Linewidth',2)
plot(FlexTime,FlexTorques(2,:),'Linewidth',2)
plot(RigidTime,RigidTorques(2,:),'Linewidth',2)
if exist('TrajTorqueMotor')
    plot(TrajTorqueMotor.Time+virtualExtraTimeRef,TrajTorqueMotor.Data(:,2),'Linewidth',2)
end
title('Torque 2 comparison from simulink')
xlabel('Time [s]')
ylabel('Torque 2 [Nm]')
axis([10 12 -3.2 4])
legend('u_{rigid}','u_{flex}','u_{FF}','u_{ref}','u_{refM}','u_{actual}','u_{FE_F}','u_{FE_R}')
% legend('u_{rigid}','u_{flex}','u_{FF}','u_{ref}','u_{refM}','u_{FE_F}','u_{FE_R}')
grid on

figure % third plot
hold on
plot(torqueRigid.Time,torqueRigid.Data(:,3),'Linewidth',2)
plot(torqueFlex.Time,torqueFlex.Data(:,3),'Linewidth',2)
plot(torqueFlexFF.Time,torqueFlexFF.Data(:,3),'Linewidth',2)
plot(tRef,torqueRef(3,:),'Linewidth',2)
plot(tRefM,torqueRefM(3,:),'Linewidth',2)
plot(tActual,torqueActual(3,:),'Linewidth',2)
plot(FlexTime,FlexTorques(3,:),'Linewidth',2)
plot(RigidTime,RigidTorques(3,:),'Linewidth',2)
if exist('TrajTorqueMotor')
    plot(TrajTorqueMotor.Time+virtualExtraTimeRef,TrajTorqueMotor.Data(:,3),'Linewidth',2)
end
title('Torque 3 comparison from simulink')
xlabel('Time [s]')
ylabel('Torque 3 [Nm]')
axis([10 12 -0.3 1.3])
legend('u_{rigid}','u_{flex}','u_{FF}','u_{ref}','u_{refM}','u_{actual}','u_{FE_F}','u_{FE_R}')
% legend('u_{rigid}','u_{flex}','u_{FF}','u_{ref}','u_{refM}','u_{FE_F}','u_{FE_R}')
grid on

% Motor angles
figure
hold on
plot(motorAnglesRigid.Time,motorAnglesRigid.Data,'--','Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(motorAnglesFlex.Time,motorAnglesFlex.Data,'Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(motorAnglesFlexFF.Time,motorAnglesFlexFF.Data,':','Linewidth',2)
xlabel('Time [s]')
ylabel('Angles [rad]')
title('Motor Angles comparison from simulink')
% legend('qM1_{rigid}','qM2_{rigid}','qM3_{rigid}','qM1_{flex}','qM2_{flex}','qM3_{flex}')
legend('qM1_{rigid}','qM2_{rigid}','qM3_{rigid}','qM1_{flex}','qM2_{flex}','qM3_{flex}','qM1_{FF}','qM2_{FF}','qM3_{FF}','Location','Best')
grid on


% Link angles
figure
hold on
plot(linkAnglesRigid.Time,linkAnglesRigid.Data,'--','Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(linkAnglesFlex.Time,linkAnglesFlex.Data,'Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(linkAnglesFlexFF.Time,linkAnglesFlexFF.Data,':','Linewidth',2)
xlabel('Time [s]')
ylabel('Angles [rad]')
title('Link Angles comparison from simulink')
% legend('qA1_{rigid}','qA2_{rigid}','qA3_{rigid}','qA1_{flex}','qA2_{flex}','qA3_{flex}')
legend('qA1_{rigid}','qA2_{rigid}','qA3_{rigid}','qA1_{flex}','qA2_{flex}','qA3_{flex}','qA1_{FF}','qA2_{FF}','qA3_{FF}','Location','Best')
grid on


% Link acceleration IMU 1
figure
hold on
plot(linkAcc1Rigid.Time,linkAcc1Rigid.Data,'--','Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(linkAcc1Flex.Time,linkAcc1Flex.Data,'Linewidth',2)
set(gca,'ColorOrderIndex',1)
plot(linkAcc1FlexFF.Time,linkAcc1FlexFF.Data,':','Linewidth',2)
xlabel('Time [s]')
ylabel('Acceleration [m/s^2]')
title('Link acceleration comparison from simulink IMU 1')
% legend('x_{rigid}','y_{rigid}','z_{rigid}','x_{flex}','y_{flex}','z_{flex}')
legend('x_{rigid}','y_{rigid}','z_{rigid}','x_{flex}','y_{flex}','z_{flex}','x_{FF}','y_{FF}','z_{FF}','Location','Best')
grid on


% Link acceleration IMU 2

% figure
% subplot(3,1,1) % first plot
% hold on
% plot(linkAcc2Rigid.Time,linkAcc2Rigid.Data(:,1))
% plot(linkAcc2Flex.Time,linkAcc2Flex.Data(:,1))
% plot(linkAcc2FlexFF.Time,linkAcc2FlexFF.Data(:,1))
% plot(tRef,linkAcc2Ref(1,:))
% plot(tRefM,linkAcc2RefM(1,:))
% % plot(tActual,linkAcc2Actual(1,:))
% title('Acceleration comparison from simulink')
% ylabel('Acc 2 x [m/s^2]')
% axis([10 15 -inf inf])
% grid on
% subplot(3,1,2) % second plot
% hold on
% plot(linkAcc2Rigid.Time,linkAcc2Rigid.Data(:,2))
% plot(linkAcc2Flex.Time,linkAcc2Flex.Data(:,2))
% plot(linkAcc2FlexFF.Time,linkAcc2FlexFF.Data(:,2))
% plot(tRef,linkAcc2Ref(2,:))
% plot(tRefM,linkAcc2RefM(2,:))
% % plot(tActual,linkAcc2Actual(2,:))
% ylabel('Acc 2 y [m/s^2]')
% axis([10 15 -inf inf])
% grid on
% subplot(3,1,3) % third plot
% hold on
% plot(linkAcc2Rigid.Time,linkAcc2Rigid.Data(:,3))
% plot(linkAcc2Flex.Time,linkAcc2Flex.Data(:,3))
% plot(linkAcc2FlexFF.Time,linkAcc2FlexFF.Data(:,3))
% plot(tRef,linkAcc2Ref(3,:))
% plot(tRefM,linkAcc2RefM(3,:))
% % plot(tActual,linkAcc2Actual(3,:))
% xlabel('Time [s]')
% ylabel('Acc 2 z [m/s^2]')
% axis([10 15 -inf inf])
% % legend('acc_{rigid}','acc_{flex}','acc_{FF}','acc_{ref}','acc_{refM}','acc_{actual}')
% legend('acc_{rigid}','acc_{flex}','acc_{FF}','acc_{ref}','acc_{refM}')
% grid on

figure
hold on
plot(linkAcc2Rigid.Time,linkAcc2Rigid.Data(:,1),'Linewidth',2)
plot(linkAcc2Flex.Time,linkAcc2Flex.Data(:,1),'Linewidth',2)
plot(linkAcc2FlexFF.Time,linkAcc2FlexFF.Data(:,1),'Linewidth',2)
plot(tRef,linkAcc2Ref(1,:))
plot(tActual,linkAcc2Actual(1,:))
title('Acceleration comparison from simulink')
xlabel('Time [s]')
ylabel('Acc 2 x [m/s^2]')
axis([10 12 -inf inf])
legend('acc_{rigid}','acc_{flex}','acc_{FF}','acc_{ref}','acc_{actual}','Location','Best')
% legend('acc_{rigid}','acc_{flex}','acc_{FF}','acc_{ref}','Location','Best')
grid on

figure
hold on
plot(linkAcc2Rigid.Time,linkAcc2Rigid.Data(:,2),'Linewidth',2)
plot(linkAcc2Flex.Time,linkAcc2Flex.Data(:,2),'Linewidth',2)
plot(linkAcc2FlexFF.Time,linkAcc2FlexFF.Data(:,2),'Linewidth',2)
plot(tRef,linkAcc2Ref(2,:))
plot(tActual,linkAcc2Actual(2,:))
xlabel('Time [s]')
ylabel('Acc 2 y [m/s^2]')
axis([10 12 -19 20])
% legend('acc_{rigid}','acc_{flex}','acc_{FF}','acc_{ref}','Location','Best')
legend('acc_{rigid}','acc_{flex}','acc_{FF}','acc_{ref}','acc_{actual}','Location','Best')
grid on

figure
hold on
plot(linkAcc2Rigid.Time,linkAcc2Rigid.Data(:,3),'Linewidth',2)
plot(linkAcc2Flex.Time,linkAcc2Flex.Data(:,3),'Linewidth',2)
plot(linkAcc2FlexFF.Time,linkAcc2FlexFF.Data(:,3),'Linewidth',2)
plot(tRef,linkAcc2Ref(3,:))
plot(tActual,linkAcc2Actual(3,:))
xlabel('Time [s]')
ylabel('Acc 2 z [m/s^2]')
axis([10 12 -inf inf])
% legend('acc_{rigid}','acc_{flex}','acc_{FF}','acc_{ref}','Location','Best')
legend('acc_{rigid}','acc_{flex}','acc_{FF}','acc_{ref}','acc_{actual}','Location','Best')
grid on