%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (C) 2011-2019 University of Liege
% <GECOS (GEometric toolbox for COnstrained mechanical Systems) is a MATLAB
% software for the simulation, control and optimization of flexible multibody
% systems.>
% Author: Olivier Bruls (ULiege, Multibody & Mechatronic Systems Lab)
%    Contact: o.bruls@uliege.be
% Author: Valentin Sonneville (ULiege, Multibody & Mechatronic Systems Lab)
%
% Licensed under the Apache License, Version 2.0 (the "License");
%    you may not use this file except in compliance with the License.
%    You may obtain a copy of the License at
%
%        http://www.apache.org/licenses/LICENSE-2.0
%
%    Unless required by applicable law or agreed to in writing, software
%    distributed under the License is distributed on an "AS IS" BASIS,
%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%    See the License for the specific language governing permissions and
%    limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spring mass system to analyse its frequency


clear all
close all

finaltime = 4; % [s]
h = 0.01; % [s]
timeVector = 0:h:finaltime;

rho_num = 0.0;
scaling = 1e6;
tol_num = 1e-6;

grav = 0*[0 9.81 0];

% Masses properties
J = 0.0*eye(3);
mas = [0 1];

n_mass = length(mas)-1; % Number of masses to be connected
distance = 0.5; % Distance between each masses (natural lenght of each spring)
dir = [0 1 0]; % direction of the masses ([1 0 0] corresponds to x)
joint_axis = [0 1 0 0 0 0]';
travelDist = 1;

% Spring damper properties
k = 100;
d = 0;

nodes = [1 0 0 0]; % initializing list of nodes
for i = 1:n_mass
    nodes = [nodes; size(nodes,1)+1 nodes(end,2:4)+dir*distance];
end

%% Rigid model

for i = 1:n_mass+1
    elements{i}.type = 'RigidBodyElement';
    elements{i}.nodes = [nodes(i,1)];
    elements{i}.mass = mas(i);
    elements{i}.J = J;
    elements{i}.g = grav;
end

count = size(elements,2);
for i = 1:n_mass
    elements{count+i}.type = 'KinematicConstraint';
    elements{count+i}.nodes = [nodes(i,1) nodes(i+1,1)];
    elements{count+i}.A = joint_axis;
    elements{count+i}.k = k;
    elements{count+i}.d = d;
end

count = size(elements,2) + 1;
s = round(finaltime*0.05/h);
e = round(s+10);
A = 1;
f = zeros(size(timeVector));
f(s:e) = A*ones(1,e-s+1);
elements{count}.type = 'punctualForce';
elements{count}.nodes = [nodes(end,1)];
elements{count}.f = f;
elements{count}.DOF = [2];

% Boundary Condition
BC = [1];

% Solving
ModelR = FEModel();
ModelR.CreateFEModel(nodes,elements);
ModelR.defineBC(BC);

D = DynamicIntegration(ModelR);
D.parameters.finaltime = finaltime;
D.parameters.timestepsize = h;
D.parameters.rho = rho_num;
D.parameters.itMax = 30;
D.parameters.relTolRes = tol_num;
D.parameters.scaling = scaling;
D.runIntegration();

% Plots
figure
subplot(311)
plot(timeVector,ModelR.listNodes{end}.position(1,:),'Linewidth',2)
grid on
ylabel('x')
subplot(312)
plot(timeVector,ModelR.listNodes{end}.position(2,:),'Linewidth',2)
grid on
ylabel('y')
subplot(313)
plot(timeVector,ModelR.listNodes{end}.position(3,:),'Linewidth',2)
grid on
ylabel('z')
xlabel('Time [s]')

analysis = freqAnalysis(D,1,0);
disp('Natural frequencies are :')
analysis.sNatFreq
