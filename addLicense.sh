#!/bin/bash

lic=license.txt

for i in $(find ./ -name "*.m"); 
do 
	echo $i
	tmp=${i}.tmp
	echo $i $tmp
	cat $lic > $tmp
	cat $i >> $tmp
	mv $tmp $i
done
