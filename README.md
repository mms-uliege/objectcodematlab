# Structure of the finite element code
---

## Model

Models  are created using the FEModel class which contains all the elements, nodes and parameters.\
Its main properties are:
* listNodes: contains all the nodes of the model which, in turn, contains all info on position, velocity, acceleration and corresponding dofs.
* listElementVariables: contains all the Lagrange multipliers, relative coordinates (of joints), command variables, ...
* listElements: contains all elements used in the model (beams, rigid bodies, joints,...) which in turn shows which nodes and elements variables are involved in it.

Models have methods that allow them to compute the residues and tangent matrices needed for various analysis. Each of these methods usually go through all the elements of the model and adds the contribution of each of them.
To vizualize the model, once an analysis (see available Solvers) has been done, use the Visu() method of the class.

## Elements

Several elements are available but may still be modified. Each element uses some nodes and/or element varaibles to compute a contribution to the residue and the tangent matrices.
Once their contribution is computed, they transfer it back to the model (FEModel class). If some methods do not exist for a given element, check the general Element class that has all defaults methods.

## Solvers

Solvers are objects that are created from a model FEModel. The following solver are available:
* DynamicIntegration: Dynamic analysis of the model, run using the runIntegration() method that can take initial condition (optional). More infos on [[1](http://hdl.handle.net/2268/167744)] and [[2](http://hdl.handle.net/2268/180964)]
* StaticIntegration: Static analysis of the model, run using the runIntegration() method that needs no additional input. More infos on [[1](http://hdl.handle.net/2268/167744)] and [[2](http://hdl.handle.net/2268/180964)].
* DirectTranscriptionOpti: Optimization solver used to solve the inverse dynamics of the model , run using the runOpti() method that needs an initial guess (dynamic analysis of a model with same nodes and joints). More infos on [[3](http://hdl.handle.net/2268/239153)].
* SensiDynamicIntegration: Dynamic sensitivity analysis of a model, run using runSensiIntegration() method that needs no additional input.
* SensiStaticIntegration: Static sensitivity analysis of a model, run using runSensiIntegration() method that needs no additional input.

## References

[1] [V. Sonneville and O. Brüls, A formulation on the special Euclidean group for dynamic analysis of multibody systems, Journal of Computational and Nonlinear Dynamics, 2014](http://hdl.handle.net/2268/167744).\
[2] [V. Sonneville, A geometric local frame approach for flexible multibody systems, PhD Thesis, 2015](http://hdl.handle.net/2268/180964).\
[3] [A. Lismonde, V. Sonneville and O. Brüls, A geometric optimization method for the trajectory planning of flexible manipulators, Multibody System Dynamics, 2019](http://hdl.handle.net/2268/239153).